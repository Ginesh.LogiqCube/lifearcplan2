﻿using System.Reflection.Metadata;

namespace LAP.DomainModels
{
    public static class Message
    {
        public const string LOGIN_SUCCESS = "Login successful";
        public const string FETCH_SUCCESS = "Record fetch successfully";
        public const string SAVE_SUCCESS = "Saved successfully";
        public const string VALIDATION_ERROR = "Validation error";
        public const string INTERNAL_SERVER_ERROR = "Internal server error";
        public const string NO_RECORD_FOUND = "No record found";
        public const string NO_RECORD_UPDATE = "No records were update";
        public const string RECORD_ADD = "Record added successfully";
        public const string RECORD_DELETE = "Record deleted successfully";
        public const string INVALID_USERNAME_PASSWORD = "Invalid Username or Password!";
        public const string RESET_PASSWORDLINK_SENT = "Reset password link has been sent to your email address. Your link will expire in 60 minutes.";
        public const string CLIENTADVISER_ADDED = "{0} is added successfully and an invitation is sent to the registered email address.";
        public const string RESET_PASSWORD_SUCCESSFUL = "You have successfully reset your password. You will login within 5 seconds.";
        public const string CREATE_PASSWORD_SUCCESSFUL = "You have successfully created your password. You will login within 5 seconds.";
        public const string LINK_EXPIRED = "Your link has expired. Please start process again";
        public const string UPLOAD_SUCCESSFUL = "File uploaded successfully";
        public const string ARCHIVE_SUCCESSFUL = "File {0} successfully";
        public const string SELECT_ONE = "Provide any one option";
        public const string RESEND_INVITATION = "Invitation is re-sent to the registered email address!";
        public const string VERIFICATION_CODE_SENT = "The verification code has been sent to {0}.";
        public const string INVALID_VERIFICATION_CODE = "The verification code is invalid.";
        public const string VARIFIED_SUCCESS = "The email address has changed successfully.";
        public const string ALREADY_ASSOCIATED = "This {0} is already associated with {1}.";
        public const string DEACTIVE_USER_ACCOUNT = "Your account is inactive, please contact the administrator";
        public const string COFIGURE_INTEGRATION_SUCCESS = "{0} Configured successfully";
        public const string DEACTIVE_INTEGRATION_SUCCESS = "{0} deactiveted successfully";
        public const string CFS_STUDENT_SAVED = "Student details integrated successfully";
        public const string TWO_FACTOR_AUTH = "2-Step Verification {0} successfully";
        public const string ADV2BSAVED = "The latest copy of the ADV2B has been saved and email regarding the same has been sent to you.";
        public const string PREAPPOINTMENTSENT = "Pre-Appointment invitation is sent successfully to the registered email address.";
        public const string FOREIGNKEYDELETE = "You can not delete this {0} as {1} {2} is/are associated with it";
        public const string NOTAVAILABLE = "You are not available in this duration, still you want to continue?";
        public const string TAXPREPSTATUSUPDATE = "Tax Prep Status Updated";
    }
    public static class EmailNotification
    {
        public const string CLIENTLOGGEDIN = "This is to notify you that your client, {0}, has logged into their LifeArcPlan Client Portal.";
        public const string SURVEYCOMLETION = "Congratulations! You have completed your {0} Survey and your financial professional has been notified.";
        public const string EMAILCHANGE = "Your email has been changed successfully. Please use {0} email address for future actions.";
        public const string MIGRATEDCLIENT = "This is to notify you that below listed clients have been {0} in your account.{1}";
    }
    public static class ValidationMessage
    {
        public const string REQUIRED_VALIDATION_GENERAL = "Provide {0}";
        public const string REQUIRED_TERM = "Please read the Terms of Use Agreement. You must agree with the LifeArcPlan terms in order to proceed.";
        public const string INVALID = "Provide valid {0}";
        public const string ALREADYEXIST = "{0} already exists";
        public const string CONFIRM_PASSWORD_MISMATCH = "{0} and {1} doesn't match";
        public const string RANGE_VALIDATION_GENERAL = "Provide Grade between {0} - {1}";
    }
    public static class EmailSubject
    {
        public const string ResetPasswordRequest = "Reset Password Request";
        public const string ResetPasswordAcknowledgement = "Reset Password Acknowledgement";
        public const string CreatePassword = "Create Password";
        public const string ComplianceNotification = "Compliance Notification";
        public const string ComplianceDocumentUpload = "Compliance Document Upload";
        public const string ComplianceDocumentReview = "Compliance Document Review";
        public const string EmailChangeRequest = "Email Change Request";
        public const string EmailChangeNotification = "Email Change Notification";
        public const string NewAdviserAssign = "New Adviser Assigned";
        public const string ClientRevoke = "Client Revoked";
        public const string NewClientAdded = "New Client Added";
        public const string LifeArcPlanSurveyCompletion = "LifeArcPlan Survey Completion";
        public const string RiskArcSurveyCompletion = "RiskArc Survey Completion";
        public const string ClientSurveyLogin = "Client Login";
        public const string ReportArchive = "New Client Report(s) Are Available For Your Review";
        public const string LoggedInClient = "New Personal Canvas Report Is Available For Your Review";
        public const string TwoFactorAuth = "Two Factor Authentication";
        public const string TwoFactorAuthCodeRequest = "2FA Code Request";
        public const string TaxDocumentNotification = "Tax Document Notification";
        public const string TaxPrepStatusUpdate = "Tax Prep Status Update";
    }
    public static class ValidationRegEx
    {
        public const string Email = "^[a-z0-9_\\.-]+@([a-z0-9-]+\\.)+[a-z]{2,6}$";
        public const string Password = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{12,}$";
        public const string Url = @"((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)";
    }
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string SuperAdmin = "Super Admin";
        public const string FirmPrincipal = "Firm Principal";
        public const string FirmAdmin = "Firm Admin";
        public const string Adviser = "Adviser";
        public const string Assistant = "Assistant";
        public const string Client = "Client";
        public const string SuperAdminFirmUser = "Super Admin,Firm Principal,Firm Admin,Adviser";
        public const string SuperAdminFirmUserClient = "Super Admin,Firm Principal,Firm Admin,Adviser,Client";
    }
    public static class AuthenticationSchemes
    {
        public const string SuperAdminAuth = "SuperAdminAuth";
        public const string UserAuth = "UserAuth";
        public const string AdviserAuth = "AdviserAuth";
        public const string ClientAuth = "ClientAuth";
        public const string SuperAdminFirmUserAuth = "SuperAdminAuth,UserAuth,AdviserAuth";
        public const string SuperAdminFirmUserClientAuth = "SuperAdminAuth,UserAuth,AdviserAuth,ClientAuth";
    }
}