﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace LAP.DomainModels
{
    public enum MaritalStatusEnum
    {
        [Display(Description = "m", Name = "Married")]
        Married = 1,
        [Display(Description = "d", Name = "Divorced")]
        Divorced = 2,
        [Display(Description = "w", Name = "Widowed")]
        Widowed = 3,
        [Display(Description = "s", Name = "Legally Separated")]
        Separated = 4,
        [Display(Description = "n", Name = "Never Married")]
        Unmarried = 5
    }
    public enum GenderEnum
    {
        [Display(Description = "m", Name = "Male")]
        Male = 1,
        [Display(Description = "f", Name = "Female")]
        Female = 2
    }
    public enum TaxFilingTypeEnum
    {
        [Display(Description = "joint", Name = "Married Filing Jointly")]
        Joint = 1,
        [Display(Description = "separate", Name = "Married Filing Separately")]
        Separate = 2,
        [Display(Description = "s", Name = "Single")]
        Single = 3,
        [Display(Description = "hoh", Name = "Head of Household")]
        HOH = 4,
        [Display(Description = "qualchild", Name = "Qualifying Widow(er)")]
        QualifyChild = 5
    }
    public enum ClientTypeEnum
    {
        [Display(Description = "client", Name = "Self")]
        Client = 1,
        [Display(Description = "spouse", Name = "Spouse")]
        Spouse = 2,
        [Display(Name = "Previous Spouse")]
        XSpouse = 3,
        [Display(Description = "trust", Name = "Trust")]
        Trust = 4,
        [Display(Description = "other", Name = "Other")]
        Other = 5
    }
    public enum RelationshipEnum
    {
        Spouse = 1,
        Son = 2,
        Daughter = 3,
        [Display(Name = "Sibling Male")]
        SiblingMale = 4,
        [Display(Name = "Sibling Female")]
        SiblingFemale = 5,
        [Display(Name = "Aunt/Uncle")]
        AuntUncle = 6,
        [Display(Name = "Niece/Nephew")]
        NieceNephew = 7,
        [Display(Name = "Cousin Male")]
        CousinMale = 8,
        [Display(Name = "Cousin Female")]
        CousinFemale = 9,
        Mother = 10,
        Father = 11,
        [Display(Name = "Other Relative Male")]
        OtherRelativeMale = 12,
        [Display(Name = "Other Relative Female")]
        OtherRelativeFemale = 13,
        [Display(Name = "Friend Significant Other Male")]
        FriendSignificantOtherMale = 14,
        [Display(Name = "Friend Significant Other Female")]
        FriendSignificantOtherFemale = 15,
        Trust = 16,
        Charity = 17,
        Business = 18
    }
    public enum ImportanceEnum
    {
        High = 1,
        Moderate = 2,
        Low = 3
    }
    public enum EmploymentStatusEnum
    {
        [Display(Description = "Full-time Employee", Name = "Full-time Employee")]
        FullTimeEmployee = 1,
        [Display(Description = "Part-time Employee", Name = "Part-time Employee")]
        PartTimeEmployee = 2,
        [Display(Description = "Self-Employed", Name = "Self-Employed")]
        SelfEmployed = 3,
        [Display(Description = "Homemaker", Name = "Homemaker")]
        HomeMaker = 4,
        [Display(Description = "Retired", Name = "Retired")]
        Retired = 5
    }
    public enum EmploymentTypeEnum
    {
        [Display(Name = "1099")]
        A1099 = 1,
        W2 = 2,
        K1 = 3
    }
    public enum TenYearExpectedAnnualIncomeEnum
    {
        [Display(Description = "Decrease substantially", Name = "Decrease substantially")]
        DecreaseSubstantially = 1,
        [Display(Description = "Decrease moderately", Name = "Decrease moderately")]
        DecreaseModerately = 2,
        [Display(Description = "Stay about the same", Name = "Stay about the same")]
        StayAboutTheSame = 3,
        [Display(Description = "Increase moderately", Name = "Increase moderately")]
        IncreaseModerately = 4,
        [Display(Description = " Increase substantially", Name = " Increase substantially")]
        IncreaseSubstantially = 5
    }
    public enum EmployerOfferEnum
    {
        [Display(Name = "401(K)")]
        K401 = 1,
        [Display(Name = "403(B)")]
        B403 = 2,
        [Display(Description = "457", Name = "457")]
        Plan457 = 3,
        [Display(Name = "SIMPLE IRA")]
        SIMPLEIRA = 4,
        [Display(Name = "Keough")]
        KeoughPlan = 5,
        [Display(Name = "SEP IRA")]
        SEPIRA = 6,
        [Display(Name = "Solo 401(K)")]
        Solo401K = 7,
    }
    public enum PropertyTypeEnum
    {
        [Display(Description = "primary", Name = "Primary Residence")]
        Primary = 1,
        [Display(Description = "secondary", Name = "Secondary Residence")]
        Secondary = 2,
        [Display(Description = "vacation", Name = "Vacation Property")]
        Vacation = 3,
        [Display(Description = "realestate", Name = "Real Estate (Buildings)")]
        RealEstate = 4,
        [Display(Description = "land", Name = "Undeveloped Land")]
        Land = 5,
        [Display(Description = "livestock", Name = "Livestock")]
        Livestock = 6,
        [Display(Description = "metals", Name = "Precious Metals")]
        Metals = 7,
        [Display(Description = "automobile", Name = "Automobile")]
        Automobile = 8,
        [Display(Description = "equipment", Name = "Equipment")]
        Equipment = 9,
        [Display(Description = "recreationalveh", Name = "Recreational Vehicle")]
        RecreationalVehicle = 10,
        [Display(Description = "collectibles", Name = "Collectibles")]
        Collectibles = 11,
        [Display(Description = "jewelry", Name = "Jewelry")]
        Jewelry = 12,
        [Display(Description = "Other", Name = "Other")]
        Other = 13

    }
    public enum ProprtyPlanEnum
    {
        [Display(Description = "transfer", Name = "Transfer to heirs")]
        TransferToHeir = 1,
        [Display(Description = "sell", Name = "Sell to fund retirement needs")]
        SellToFund = 2,
        [Display(Description = "rental", Name = "Rental Income")]
        RentalIncome = 3
    }
    public enum EntityFirmEnum
    {
        [Display(Name = "C Corp")]
        Corp_C = 1,
        [Display(Name = "S Corp")]
        Corp_S = 2,
        LLC = 3,
        Partnership = 4,
        Proprietor = 5
    }
    public enum LastReviewedEnum
    {
        [Display(Description = "1 Year", Name = "1 Year")]
        Year1 = 1,
        [Display(Description = "2-3 Years", Name = "2-3 Years")]
        Years2_3 = 2,
        [Display(Description = "4 or more", Name = "4 or more")]
        more4 = 3,
    }
    public enum CloselyHeldBusinessEnum
    {
        [Display(Name = "Maintain Ownership Throughout Retirement")]
        MaintainOwnershipThroughoutRetirement = 1,
        [Display(Name = "Sell to Fund Retirement Needs")]
        SelltoFundRetirementNeeds = 2,
        [Display(Name = "Sell at Death")]
        SellatDeath = 3,
        [Display(Name = "Transfer to Trust")]
        TransfertoTrust = 4,
        [Display(Name = "Transfer to Heirs Equally")]
        TransfertoHeirsEqually = 5,
        [Display(Description = "Transfer to Specific Heir", Name = "Transfer to Specific Heir")]
        TransfertoSpecificHeir = 6
    }
    public enum TypeOfPlanEnum
    {
        [Display(Description = "Defined Benefit Pension", Name = "Defined Benefit Pension")]
        DefinedBenefitPension = 1,
        [Display(Description = "Defined Contribution Pension", Name = "Defined Contribution Pension")]
        DefinedContributionPension = 2,
        [Display(Description = "Profit Sharing Plan", Name = "Profit Sharing Plan")]
        ProfitSharingPlan = 3,
        [Display(Description = "401k Plan", Name = "401k Plan")]
        Plan401k = 4,
        [Display(Description = "SIMPLE Plan", Name = "SIMPLE Plan")]
        SIMPLEPlan = 5
    }
    public enum SourceofIncomeEnum
    {
        [Display(Description = "Trust", Name = "Trust")]
        Trust = 1,
        [Display(Description = "Child Support", Name = "Child Support")]
        ChildSupport = 2,
        [Display(Description = "Alimony", Name = "Alimony")]
        Alimony = 3,
        [Display(Description = "Gift", Name = "Gift")]
        Gift = 4,
        [Display(Description = "Structured Settlement", Name = "Structured Settlement")]
        StructuredSettlement = 5,
        [Display(Description = "Disability", Name = "Disability")]
        Disability = 6,
        [Display(Description = "Other", Name = "Other")]
        Other = 7
    }
    public enum SSexemptionEnum
    {
        [Display(Name = "Clergy Exemption (Self-Employment tax)", Description = "Clergy Exemption: Clergy can request an exemption from SE tax if they are a member (minister, member of a religions order, or Christian Science practitioner or reader) or a member of a recognized religious sect.")]
        ClergyExemption = 1,
        [Display(Name = "Qualifying religious exemption (FICA tax)", Description = "Qualifying Religions Exemptions: Members of certain religious groups qualify for the exemption, but it must be a recognized religious sect opposed to accepting Social Security benefits. Some of those benefits include payments during retirement and payments in the event of a disability or at death. In addition, the religious group must have existed as of the end of 1950 and must have continuously provided its dependent members with a reasonable standard of living since that time. The exemption isn’t automatic; you must apply for it by completing Form 4029. The exemption is unavailable if you’ve ever been eligible to receive benefits under the Social Security program regardless of whether you actually received the benefit or not.")]
        Qualifyingreligiousexemption = 2,
        [Display(Name = "Nonresident aliens (FICA tax)", Description = "Nonresident Aliens Exemptions:  Individuals who don’t possess United States citizenship, or aren’t legal residents are classified as nonresident aliens. A nonresident alien working in the U.S. usually pays Social Security tax on any income made here, even if he/she works for a foreign company. There are some exceptions. Foreign students and educational professionals in the U.S. on a temporary basis don't have to pay Social Security taxes. Nonresidents working in the U.S. for a foreign government are exempt from paying Social Security taxes on their salaries. Their families and domestic workers can also qualify for the exemption. Many other categories of nonresidents are eligible for the exemption, but, in all cases, the determining factor is the type of visa the nonresident possesses.")]
        Nonresidentaliens = 3,
        [Display(Name = "Temporary student exemption (FICA tax) ", Description = "Temporary Student Exemptions:  Students working for the same school they’re enrolled at may be temporarily exempt from paying Social Security taxes. However, only students who obtain employment because of their enrollment qualify. In other words, if you work full-time in the registrar’s office of a university, and take advantage of the tuition-free enrollment the university offers its employees, you don’t qualify. If you attend school full-time, and the university offers you a part-time job that’s contingent on your continued enrollment, you do qualify. That only applies to the wages you earn at the university, not wages you earn from other employers.")]
        Temporarystudentexemption = 4,
        [Display(Name = "Foreign government exemption (FICA tax)", Description = "Foreign Government Exemptions: Employees of foreign governments are generally exempt from paying Social Security taxes on income paid to them as a result of their official responsibilities. As long as the foreign government employee is working in an official capacity on official business related to his/her employment, he/she does not have to pay Social Security on his pay. The Social Security exemption does not apply to servants of employees of a foreign government or the official’s children or spouse unless they are also employed by the foreign government.")]
        Foreigngovernmentexemption = 5
    }
    public enum BankCreditUnionAccountTypeEnum
    {
        [Display(Name = "Savings")]
        Savings = 1,
        [Display(Name = "Personal Checking")]
        PersonalChecking = 2,
        [Display(Name = "Business Checking")]
        BusinessChecking = 3,
        [Display(Name = "Certificate Of Deposit")]
        CertificateOfDeposit = 4,
        [Display(Name = "Money Market")]
        MoneyMarket = 5
    }
    public enum BrokerageAdvisoryAccountTypeEnum
    {
        Retirement = 1,
        Education = 2,
        [Display(Name = "Single-Use")]
        SingleUse = 3
    }
    public enum TaxClassificationEnum
    {
        [Display(Name = "Qualified IRA")]
        QualifiedIRA = 1,
        [Display(Name = "Qualified Roth IRA")]
        QualifiedRothIRA = 2,
        [Display(Name = "Non-Qualified")]
        NonQualified = 3
    }
    public enum CharitableEstateEnum
    {
        [Display(Name = "5% to 25%")]
        N5To25 = 1,
        [Display(Name = "26% to 50%")]
        N26To50 = 2,
        [Display(Name = "51% to 75%")]
        N51To75 = 3,
        [Display(Name = "76% to 100%")]
        N76To100 = 4
    }
    public enum CharitableDonationEnum
    {
        [Display(Name = "Provide the donor with immediate tax savings?")]
        ImmediateTaxSavings = 1,
        [Display(Name = "Provide the donor with immediate income? (guaranteed income)?")]
        GuaranteedIncome = 2,
        [Display(Name = "Provide the donor with immediate income? (variable income)")]
        VariableIncome = 3,
        [Display(Name = "Provide the donor's beneficiaries with future income?")]
        FutureIncome = 4
    }
    public enum CharitableGiftEnum
    {
        [Display(Name = "Immediately?")]
        Immediately = 1,
        [Display(Name = "At the time of the donor's death?")]
        AtDeath = 2
    }
    public enum ContractTypeEnum
    {
        Life = 1,
        Annuity = 2
    }
    public enum LifeAnnuityContractTypeEnum
    {
        Term = 1,
        [Display(Name = "Whole Life")]
        WholeLife = 2,
        [Display(Name = "Universal Life")]
        UniversalLife = 3,
        [Display(Name = "Indexed Universal Life")]
        IndexedUniversalLife = 4,
        [Display(Name = "Variable Universal Life")]
        VariableUniversalLife = 5,
        [Display(Name = "Fixed Rate Annuity")]
        FixedRateAnnuity = 6,
        [Display(Name = "Fixed Indexed Annuity")]
        FixedIndexedAnnuity = 7,
        [Display(Name = "Variable Annuity")]
        VariableAnnuity = 8,
        [Display(Name = "I Don't Know")]
        IDontKnow2 = 9
    }
    public enum PersonalCareRelationShipEnum
    {
        [Display(Name = "Child")]
        Child = 1,
        [Display(Name = "Parent")]
        Parent = 2,
        [Display(Name = "Aunt/Uncle")]
        AuntUncle = 3,
        [Display(Name = "Sibling")]
        Sibling = 4,
        [Display(Name = "Niece/Nephew")]
        NieceNephew = 5,
        [Display(Name = "Cousin")]
        Cousin = 6,
        [Display(Name = "Friend/Significant Other")]
        FriendSignificantOther = 7
    }
    public enum PersonalCareEnum
    {
        [Display(Name = "My primary caregiver will reside/move in with me to provide care")]
        ProviceCare = 1,
        [Display(Name = "I will move in with my primary caregiver")]
        PrimaryCare = 2,
        [Display(Name = "I will move into a facility")]
        Facility = 3
    }
    public enum TypeOfEducationProgramEnum
    {
        [Display(Name = "Private Elementary")]
        PrivateElementary = 1,
        [Display(Name = " Private High School")]
        PrivateHighSchool = 2,
        [Display(Name = "College")]
        College = 3
    }
    public enum SavingForEducationEnum
    {
        [Display(Name = "529 Plan")]
        plan529 = 1,
        [Display(Name = "Coverdell IRA")]
        CoverdellIRA = 2,
        [Display(Name = "Roth IRA")]
        RothIRA = 3,
        [Display(Name = "Permanent Life Insurance")]
        PermanentLifeInsurance = 4,
        [Display(Name = "Other")]
        Other = 5
    }
    public enum YesNoEnum
    {
        [Display(Name = "Yes")]
        Yes = 1,
        [Display(Name = "No")]
        No = 2,
        [Display(Name = "Don't Know")]
        DontKnow = 3
    }
    public enum WaitingPeriodEnum
    {
        [Display(Name = "1 month")]
        month1 = 1,
        [Display(Name = "2 months")]
        month2 = 2,
        [Display(Name = "3 months")]
        month3 = 3,
        [Display(Name = "4 months")]
        month4 = 4,
        [Display(Name = "5 months")]
        month5 = 5,
        [Display(Name = "6 months")]
        month6 = 6,
        [Display(Name = "7 months")]
        month7 = 7,
        [Display(Name = "8 months")]
        month8 = 8,
        [Display(Name = "9 months")]
        month9 = 9,
        [Display(Name = "10 months")]
        month10 = 10,
        [Display(Name = "11 months")]
        month11 = 11,
        [Display(Name = "12 months")]
        month12 = 12
    }
    public enum LTCIbenifitsEnum
    {
        [Display(Name = "Lifetime")]
        lifetime = 1,
        [Display(Name = "1 months")]
        month1 = 2,
        [Display(Name = "2 months")]
        month2 = 3,
        [Display(Name = "3 months")]
        month3 = 4,
        [Display(Name = "4 months")]
        month4 = 5,
        [Display(Name = "5 months")]
        month5 = 6,
        [Display(Name = "6 months")]
        month6 = 7,
        [Display(Name = "7 months")]
        month7 = 8,
        [Display(Name = "8 months")]
        month8 = 9,
        [Display(Name = "9 months")]
        month9 = 10,
        [Display(Name = "10 months")]
        month10 = 11,
        [Display(Name = "11 months")]
        month11 = 12,
        [Display(Name = "12 months")]
        month12 = 13,
        [Display(Name = "1 year")]
        year1 = 14,
        [Display(Name = "2 years")]
        year2 = 15,
        [Display(Name = "3 years")]
        year3 = 16,
        [Display(Name = "4 years")]
        year4 = 17,
        [Display(Name = "5 years")]
        year5 = 18,
        [Display(Name = "6 years")]
        year6 = 19,
        [Display(Name = "7 years")]
        year7 = 20,
        [Display(Name = "8 years")]
        year8 = 21,
        [Display(Name = "9 years")]
        year9 = 22,
        [Display(Name = "10 years")]
        year10 = 23,
        [Display(Name = "11 years")]
        year11 = 24,
        [Display(Name = "12 years")]
        year12 = 25,
        [Display(Name = "13 years")]
        year13 = 26,
        [Display(Name = "14 years")]
        year14 = 27,
        [Display(Name = "15 years")]
        year15 = 28,
        [Display(Name = "16 years")]
        year16 = 29,
        [Display(Name = "17 years")]
        year17 = 30,
        [Display(Name = "18 years")]
        year18 = 31,
        [Display(Name = "19 years")]
        year19 = 32,
        [Display(Name = "20 years")]
        year20 = 33,
    }
    public enum PolicyProvideCareEnum
    {
        [Display(Name = "Home")]
        Home = 1,
        [Display(Name = "Assisted Living Facility")]
        AssistedLivingFacility = 2,
        [Display(Name = "Nursing Home")]
        NursingHome = 3,
        [Display(Name = "I don't know")]
        Idontknow = 4
    }
    public enum PolicyBeginBenefits
    {
        [Display(Name = "Loss of 2 or more specific'Activities of Daily Living'")]
        Lossof2ormorespecific = 1,
        [Display(Name = "Cognitive Impairment")]
        CognitiveImpairment = 2,
        [Display(Name = "Medical Necessity/Doctor Certification of Need")]
        MedicalNecessity = 3,
        [Display(Name = "I don't know")]
        Idontknow = 4
    }
    public enum LTCIbenifirPayouttEnum
    {
        [Display(Name = "Indeminity")]
        Indeminity = 1,
        [Display(Name = "Reimbursement basis")]
        Reimbursementbasis = 2,
        [Display(Name = "  I don't know")]
        Idontknow = 3
    }
    public enum LTCIPolicyProvideBenifitsEnum
    {
        [Display(Name = "Guaranteed Renewable")]
        GuaranteedRenewable = 1,
        [Display(Name = "Waiver of premium")]
        Waiverofpremium = 2,
        [Display(Name = "Nonforfeiture benefits")]
        Nonforfeiturebenefits = 3,
        [Display(Name = "Restoration of Benefits")]
        RestorationofBenefits = 4,
        [Display(Name = "I don't know")]
        Idontknow = 5,
    }
    public enum DescritpionTableNameEnum
    {
        [Display(Name = "Work Expectations")]
        WorkExpectations = 1,
        [Display(Name = "Residence Expectations")]
        ResidenceExpectations = 2,
        [Display(Name = "LifeStyle Expectations")]
        LifeStyleExpectations = 3,
        [Display(Name = "Community Expectations")]
        CommunityExpectations = 4,
        [Display(Name = "Dependent Care Support")]
        DependentCare = 5,
        [Display(Name = "Expense Concern")]
        ExpenseConcerns = 6,
        [Display(Name = "Income Concern")]
        IncomeConcerns = 7,
        [Display(Name = "Health")]
        Health = 8,
        [Display(Name = "Estate")]
        Estate = 9

    }
    public enum RadioListEnum
    {
        [Display(Name = "None")]
        None = 0,
        [Display(Name = "Low")]
        Low = 1,
        [Display(Name = "Medium")]
        Medium = 2,
        [Display(Name = "High")]
        High = 3,
    }
    public enum AdminRoleEnum
    {
        [Display(Name = "Super Admin")]
        SuperAdmin = 1,
        [Display(Name = "Firm Principal")]
        FirmPrincipal = 2,
        [Display(Name = "Firm Admin")]
        FirmAdmin = 3,
        Adviser = 4,
        Compliance = 5
    }
    public enum AssistantPermissionEnum
    {
        [Display(Name = "Client")]
        AssistantC = 1,
        [Display(Name = "Compliance Queue")]
        AssistantCQ = 2,
        [Display(Name = "Insurance Queue")]
        AssistantIQ = 3,
        [Display(Name = "Tax Prep Queue")]
        AssistantTQ = 4
    }
    public enum ComplianceDocumentStatusEnum
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3
    }
    public enum InsuranceDocumentStatusEnum
    {
        Pending = 1,
        Approved = 2,
        Issued = 3,
        Rejected = 4
    }
    public enum TaxFilingPrepStatusEnum
    {
        [Display(Name = "Initial Tax Document(s) Uploaded")]
        InitialTaxDocumentUploaded = 1,
        [Display(Name = "Initial Appointment Scheduled")]
        InitialAppointmentScheduled = 2,
        [Display(Name = "Tax Forms Provided to Tax Preparer")]
        TaxFormsProvidedToTaxPreparer = 3,
        [Display(Name = "Preparation in Process")]
        PreparationInProcess = 4,
        [Display(Name = "Tax Forms Completed")]
        TaxFormsCompleted = 5,
        [Display(Name = "Filing Review Appointment Scheduled")]
        FilingReviewAppointmentScheduled = 6,
        [Display(Name = "Tax Filing Submitted to IRS")]
        TaxFilingSubmittedToIRS = 7
    }
    public enum PermissionTypeEnum
    {
        Grant = 1,
        Revoke = 2,
    }
    public enum SalutationEnum
    {
        [Display(Description = "mr", Name = "Mr.")]
        Mr = 1,
        [Display(Description = "mrs", Name = "Mrs.")]
        Mrs = 2,
        [Display(Description = "ms", Name = "Ms.")]
        Ms = 3
    }
    public enum AdviserTitleEnum
    {
        Owner = 1,
        Partner = 2,
        Member = 3,
        Founder = 4,
        President = 5,
        [Display(Name = "Vice President")]
        VicePresident = 6,
        Associate = 7,
        [Display(Name = "Chief Executive Officer")]
        ChiefExecutiveOfficer = 8,
        [Display(Name = "Chief Operations Officer")]
        ChiefOperationsOfficer = 9,
        [Display(Name = "Chief Financial Officer")]
        ChiefFinancialOfficer = 10,
        [Display(Name = "Chief Compliance Officer")]
        ChiefComplianceOfficer = 11,
        [Display(Name = "Chief Investment Officer")]
        ChiefInvestmentOfficer = 12
    }
    public enum DegreeDesinationEnum
    {
        [Display(Name = "Juris Doctorate (JD)", Description = "jd")]
        JD = 1,
        [Display(Name = "Master in Law (LLM)", Description = "llm")]
        LLM = 2,
        [Display(Name = "Master of Business Administration (MBA)", Description = "mba")]
        MBA = 3,
        [Display(Name = "Certified Public Accountant (CPA)", Description = "cpa")]
        CPA = 4,
        [Display(Name = "Charter Financial Analyst (CFA)", Description = "cfa")]
        CFA = 5,
        [Display(Name = "PhD in Finance (PhD)", Description = "phd")]
        PHDF = 6,
        [Display(Name = "Master of Science Financial Services (MSFS)", Description = "msfs")]
        MSFS = 7,
        [Display(Name = "Certified Financial Planner (CFP)", Description = "cfp")]
        CFP = 8,
        [Display(Name = "Charter Financial Consultant (ChFC®)", Description = "chfc")]
        ChFC = 9,
        [Display(Name = "Chartered Life Underwriter (CLU®)", Description = "clu")]
        CLU = 10,
        [Display(Name = "Retirement Income Consultant Professional (RICP®)", Description = "ricp")]
        RICP = 11,
        [Display(Name = "Charter Advisor for Senior Living (CASL®)", Description = "casl")]
        CASL = 12,
        [Display(Name = "Life Underwriters Training Council Fellowship (LUTCF)", Description = "lutcf")]
        LUTCF = 13,
        [Display(Name = "Certified Private Wealth Advisor (CPWA®)", Description = "cpwa")]
        CPWA = 14,
        [Display(Name = "Certified Funds Specialist (CFS)", Description = "cfs")]
        CFS = 15,
        [Display(Name = "Personal Financial Specialist (PFS)", Description = "pfs")]
        PFS = 16,
        [Display(Name = "Registered Health Underwriter (RHU)", Description = "rhu")]
        RHU = 17,
        [Display(Name = "Registered Employee Benefits Consultant (REBC)", Description = "rebc")]
        REBC = 18
    }
    public enum LicenseEnum
    {
        [Display(Name = "Life", Description = "life")]
        Life = 1,
        [Display(Name = "Health", Description = "health")]
        Health = 2,
        [Display(Name = "Annuity", Description = "ann")]
        Annuity = 3,
        [Display(Name = "Property & Casualty", Description = "prop")]
        PropertyCasualty = 4,
        [Display(Name = "Series 6", Description = "series6")]
        Series6 = 5,
        [Display(Name = "Series 7", Description = "series7")]
        Series7 = 6,
        [Display(Name = "Series 62", Description = "series62")]
        Series62 = 7,
        [Display(Name = "Series 63", Description = "series63")]
        Series63 = 8,
        [Display(Name = "Series 65", Description = "series65")]
        Series65 = 9,
        [Display(Name = "Series 66", Description = "series66")]
        Series66 = 10
    }
    public enum DegreeTypeEnum
    {
        [Display(Name = "Associate Degree", Description = "assoc")]
        Associate = 1,
        [Display(Name = "Bachelors Degree", Description = "bach")]
        Bachelor = 2,
        [Display(Name = "Masters Degree", Description = "master")]
        Master = 3,
        [Display(Name = "Doctoral Degree", Description = "doc")]
        Doctorate = 4,
        [Display(Name = "Professional Degree", Description = "prof")]
        Professional = 5
    }
    public enum DegreeEnum
    {
        [Display(Name = "Associate of Arts (AA)", Description = "aa")]
        AA = 1,
        [Display(Name = "Associate of Science (AS)", Description = "as")]
        AS = 2,
        [Display(Name = "Associate of Applied Arts (AAA)", Description = "aaa")]
        AAA = 3,
        [Display(Name = "Associate of Applied Science (AAS)", Description = "aas")]
        AAS = 4,
        [Display(Name = "Associate - Other", Description = "assocother")]
        AssociateOther = 5,

        [Display(Name = "Bachelor of Business Administration (BBA)", Description = "bba")]
        BBA = 6,
        [Display(Name = "Bachelor of Liberal Arts (BLA)", Description = "bla")]
        BLA = 7,
        [Display(Name = "Bachelor of Arts (BA)", Description = "ba")]
        BA = 8,
        [Display(Name = "Bachelor of Science (BS)", Description = "bs")]
        BS = 9,
        [Display(Name = "Bachelor - Other", Description = "bother")]
        BachelorOther = 10,

        [Display(Name = "Master of Arts (MA)", Description = "ma")]
        MA = 11,
        [Display(Name = "Master of Liberal Arts (MLA)", Description = "mla")]
        MLA = 12,
        [Display(Name = "Master of Business Administration (MBA)", Description = "mba")]
        MBA = 13,
        [Display(Name = "Master of Science (MS)", Description = "ms")]
        MS = 14,
        [Display(Name = "Master of Accountancy (MAcc)", Description = "macc")]
        MAcc = 15,
        [Display(Name = "Master of Financial Management (MFM)", Description = "mfm")]
        MFM = 16,
        [Display(Name = "Master - Other", Description = "mother")]
        MasterOther = 17,

        [Display(Name = "Doctor of Arts (DA)", Description = "da")]
        DA = 18,
        [Display(Name = "Doctor of Business Administration (DBA)", Description = "dba")]
        DBA = 19,
        [Display(Name = "Doctor of Management (DMgt)", Description = "dmgt")]
        DMgt = 20,
        [Display(Name = "Doctor of Philosophy (PhD)", Description = "phd")]
        PhD = 21,
        [Display(Name = "Doctor of Theology (ThD)", Description = "thd")]
        ThD = 22,
        [Display(Name = "Doctorate - Other", Description = "dother")]
        DoctorateOther = 23,

        [Display(Name = "Juris Doctor (JD)", Description = "jd")]
        JD = 24,
        [Display(Name = "Master of Laws (LLM)", Description = "llm")]
        LLM = 25,
        [Display(Name = "Professional - Other", Description = "pother")]
        ProfessionalOther = 26
    }
    public enum AffiliationTypeEnum
    {
        [Display(Name = "Field Marketing Organization", Description = "FMO")]
        FieldMarketingOrganization = 1,
        [Display(Name = "Broker/Dealer", Description = "BD")]
        BrokerDealer = 2,
        [Display(Name = "Registered Investment Advisor", Description = "RIA")]
        RegisteredInvestmentAdvisor = 3,
    }
    public enum RiskArcBitEnum
    {
        Preserver = 1,
        Follower = 2,
        Independent = 3,
        Accumulator = 4
    }
    public enum RiskProfileEnum
    {
        [Display(Name = "Below Conservative Score")]
        BelowConservative = 1,
        Conservative = 2,
        [Display(Name = "Moderate Conservative")]
        ModerateConservative = 3,
        Moderate = 4,
        [Display(Name = "Moderate Aggressive")]
        ModerateAggressive = 5,
        Aggressive = 6
    }
    public enum RiskCapReturnRangeEnum
    {
        [Display(Name = "2.5 to 5.5")]
        Range2_5To5_5 = 1,
        [Display(Name = "5.6 to 8.5")]
        Range5_6To8_5 = 2,
        [Display(Name = "8.6")]
        Range8_6 = 3,
    }
    public enum InsuranceTypeEnum
    {
        Life = 1,
        Annuity = 2,
        [Display(Name = "Long Term Care")]
        LongTermCare = 3,
        Disability = 4
    }
    public enum CompensationTypeEnum
    {
        [Display(Name = "Fee - Only")]
        Fee = 1,
        [Display(Name = "Commission - Only")]
        Commission = 2,
        [Display(Name = "Fee and Commission")]
        FeeCommission = 3,
    }
    public enum RegisteredEnum
    {
        [Display(Name = "SEC")]
        SECRegistered = 1,
        [Display(Name = "State")]
        StateRegistered = 2,
    }
    public enum CFSRelationshipEnum
    {
        Self = 1,
        Parent = 2,
        Grandparent = 3,
        Sibling = 4,
        Uncle = 5,
        Aunt = 6,
        [Display(Name = "Niece/Nephew")]
        NieceNephew = 7,
        Cousin = 8,
        Friend = 9
    }
    public enum AddressTypeEnum
    {
        Resident = 1,
        Business = 2
    }
    public enum ClergyLivingArrangementEnum
    {
        [Display(Name = "Living in the Personage")]
        Personage = 1,
        [Display(Name = "Own my home")]
        Home = 2,
        [Display(Name = "Rent")]
        Rent = 3
    }
    public enum ActivityTypeEnum
    {
        [Display(Name = "Event (Pipeline)", Description = "#26B99A")]
        Event = 1,
        [Display(Name = "Client Appointment (Operations)", Description = "#8950FC")]
        ClientAppointment = 2,
        [Display(Name = "Prospect Appointment (Operations)", Description = "#F64E60")]
        ProspectAppointment = 3,
        [Display(Description = "#FF9933")]
        Occasion = 4,
    }
    public enum EventTypeEnum
    {
        [Display(Name = "Webinar (Live)")]
        WebinarLive = 1,
        [Display(Name = "Webinar (Pre-Recorded)")]
        WebinarPreRecorded = 2,
        [Display(Name = "Client Appreciation Event")]
        ClientAppreciationEvent = 3,
        [Display(Name = "Seminar (Live In-Person)")]
        SeminarLiveInPerson = 4,
        [Display(Name = "Online Paid Ad Campaign")]
        OnlinePaidAdCampaign = 5,
        [Display(Name = "Livestream Event (Social Media)")]
        LivestreamEventSocialMedia = 6,
        [Display(Name = "Non-Client Advocate")]
        NonClientAdvocate = 7,
        [Display(Name = "Other")]
        Other = 8
    }
    public enum PriorityEnum
    {
        Low = 1,
        Medium = 2,
        High = 3
    }
    public enum TaskStatusEnum
    {
        Open = 1,
        InProgress = 2,
        Hold = 3,
        Completed = 4
    }
    public enum ProspectAppointmentStageEnum
    {
        [Display(Name = "First Appointment", Description = "primary")]
        FirstAppointment = 1,
        [Display(Name = "Second Appointment", Description = "primary")]
        SecondAppointment = 2,
        [Display(Name = "Third Appointment", Description = "primary")]
        ThirdAppointment = 3,
        [Display(Name = "New Client", Description = "primary")]
        NewClient = 4,
        [Display(Description = "warning")]
        Referral = 5,
        Lead = 6,
        [Display(Name = "Pre-Appointment", Description = "primary")]
        PreAppointment = 7
    }
    public enum FalloutEnum
    {
        FirstFallout = 1,
        SecondFallout = 2,
        ThirdFallout = 3
    }
    public enum ClientAppointmentPurposeEnum
    {
        [Display(Name = "New Investment Business")]
        NewInvestmentBusiness = 1,
        [Display(Name = "New Insurance Business")]
        NewInsuranceBusiness = 2,
        [Display(Name = "Strategy and Tactical")]
        StrategyTactical = 3,
        [Display(Name = "Account Administration")]
        AccountAdministration = 4,
        [Display(Name = "Client Advocate")]
        ClientAdvocate = 5,
        [Display(Name = "Ancillary Services")]
        AncillaryServices = 6
    }
    public enum AppointmentStatusEnum
    {
        Unknown = 0,
        Scheduled = 1,
        Rescheduled = 2,
        Canceled = 3
    }
    public enum ActionEnum
    {
        [Display(Name = "Added")]
        Insert = 1,
        [Display(Name = "Updated")]
        Update = 2,
        [Display(Name = "Deleted")]
        Delete = 3
    }
    public enum OccasionTypeEnum
    {
        [Display(Name = "Personal Time (Out of Office)")]
        PersonalTimeOutOfOffice = 1,
        [Display(Name = "Account/Case Preparation (In office)")]
        UnavailableCasePreparation = 2,
        [Display(Name = "Staff/Team Meeting")]
        StaffTeamMeeting = 3,
        [Display(Name = "Training/Coaching (Offsite)")]
        TrainingCoachingOffsite = 4,
        [Display(Name = "Training/Coaching (Onsite)")]
        TrainingCoachingOnsite = 5,
        [Display(Name = "Community Marketing/Networking (Offsite)")]
        CommunityMarketing = 6,
        [Display(Name = "Holiday/Vacation")]
        HolidayVacation = 7,
        Other = 8
    }
    public enum RetirementPlanTypeEnum
    {
        [Display(Name = "401(k)")]
        K401 = 1,
        [Display(Name = "403(b)")]
        B403 = 2,
        [Display(Name = "457(b)")]
        B457 = 3,
        [Display(Name = "457(f)")]
        F457 = 4,
        [Display(Name = "Employer Stock Option Plan (ESOP)")]
        EmployerStockOptionPlan = 5,
        [Display(Name = "Pension Plan - Defined Benefit")]
        PensionPlanDefinedBenefit = 6,
        [Display(Name = "Pension Plan - Defined Contribution")]
        PensionPlanDefinedContribution = 7,
        [Display(Name = "Pension Plan - Cash Balance")]
        PensionPlanCashBalance = 8,
        [Display(Name = "Pension Plan - Money Purchase")]
        PensionPlanMoneyPurchase = 9,
        [Display(Name = "Pension Plan - Target Benefit")]
        PensionPlanTargetBenefit = 10,
        [Display(Name = "Pension - 412(i)")]
        Pension412I = 11,
        [Display(Name = "Profit-Sharing")]
        ProfitSharing = 12,
        [Display(Name = "SIMPLE IRA")]
        SIMPLEIRA = 13,
        [Display(Name = "Simplified Employee Pension Plan (SEP)")]
        SimplifiedEmployeePensionPlan = 14,
        [Display(Name = "Thrift Savings")]
        ThriftSavings = 15,
        Other = 16
    }
    public static class EnumExtension
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            string result = string.Empty;
            if (enumValue.ToString() != "0")
            {
                result = enumValue.GetType()?.GetMember(enumValue.ToString())?.First()?.GetCustomAttribute<DisplayAttribute>()?.GetName();
                result ??= enumValue.ToString();
            }
            return result;
        }
        public static string GetDescription(this Enum enumValue)
        {
            string result = string.Empty;
            if (enumValue.ToString() != "0")
            {
                result = enumValue.GetType()?.GetMember(enumValue.ToString())?.First()?.GetCustomAttribute<DisplayAttribute>()?.GetDescription();
                result ??= enumValue.ToString();
            }
            return result;
        }
    }
}