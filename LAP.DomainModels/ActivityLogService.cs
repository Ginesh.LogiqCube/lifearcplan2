﻿using LAP.DomainModels.BackgroundTask;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Admin;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LAP.DomainModels
{
    public class ActivityLogService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IBackgroundTaskQueue _queue;
        public ActivityLogService(IServiceScopeFactory serviceScopeFactory, IBackgroundTaskQueue queue)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _queue = queue;
        }
        public void SaveActivityLog(List<ClientActivityLog> clientActivityLogs)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;
            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            _queue.QueueBackgroundWorkItem(async token =>
            {
                await _context2.ClientActivityLogs.AddRangeAsync(clientActivityLogs, token);
                _context2.SaveChanges();
            });
        }
        public int GetContributionCalculated(int id)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;
            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var esp = _context2.ClientEsps.Where(m => m.Id == id).Include(m => m.ClientOccupation).FirstOrDefault();
            decimal contribution = 0;
            if (esp != null)
                contribution = esp.EspContribution > 0 ? esp.EspContribution : Convert.ToDecimal(esp.ClientOccupation?.AnnualEarnings) * esp.EspContributionPercentage / 100;
            return Convert.ToInt32(contribution);
        }
    }
}