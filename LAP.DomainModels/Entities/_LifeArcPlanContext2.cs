﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Entities.Master;
using LAP.DomainModels.Entities.Admin;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using LAP.DomainModels.Models;
using Newtonsoft.Json;
using System.Data;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace LAP.DomainModels.Entities
{
    public partial class LifeArcPlanContext2 : DbContext
    {
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly string hostUrl;
        protected readonly ActivityLogService _activityLogService;
        public LifeArcPlanContext2(DbContextOptions<LifeArcPlanContext2> options, IHttpContextAccessor httpContextAccessor, ActivityLogService activityLogService) : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
            hostUrl = _httpContextAccessor.HttpContext?.Request?.Host.Value;
            _activityLogService = activityLogService;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserLogin>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<UserLogin>().Property(b => b.TwoFactorAuth).HasDefaultValue(false);
            modelBuilder.Entity<UserLogin>().Property(b => b.VerificationCode).HasDefaultValue(string.Empty);

            modelBuilder.Entity<BudgetItem>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<PlanningItem>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<DescriptionItem>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<RiskArcItem>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");

            modelBuilder.Entity<ClientLogin>().Property(b => b.InitialProfileSetupCompleted).HasDefaultValue(false);
            modelBuilder.Entity<ClientLogin>().Property(b => b.TwoFactorAuth).HasDefaultValue(false);
            modelBuilder.Entity<ClientLogin>().Property(b => b.VerificationCode).HasDefaultValue(string.Empty);

            modelBuilder.Entity<ClientData>().Property(b => b.BudgetVersion).HasDefaultValue(false);
            modelBuilder.Entity<ClientData>().Property(b => b.InvestmentVersion).HasDefaultValue(false);
            modelBuilder.Entity<ClientData>().Property(b => b.RiskArcVersion).HasDefaultValue(false);

            modelBuilder.Entity<ClientDocument>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");

            modelBuilder.Entity<ComplianceDocument>().Property(x => x.Guid).HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<ComplianceDocument>().Property(b => b.Status).HasDefaultValue(ComplianceDocumentStatusEnum.Pending);
            modelBuilder.Entity<ComplianceDocument>().Property(b => b.ReqiuredChange).HasDefaultValue(false);
            modelBuilder.Entity<ComplianceDocument>().Property(b => b.UploadedDate).HasDefaultValueSql("GETDATE()");
            modelBuilder.Entity<ComplianceDocument>().Property(b => b.IsFromV1).HasDefaultValue(false);
            modelBuilder.Entity<ComplianceDocument>().Property(b => b.ReqiuredChange).HasDefaultValue(false);
            modelBuilder.Entity<ComplianceDocument>().Property(b => b.IsFromV1).HasDefaultValue(false);

            modelBuilder.Entity<InsuranceDocument>().Property(b => b.Status).HasDefaultValue(InsuranceDocumentStatusEnum.Pending);
            modelBuilder.Entity<InsuranceDocument>().Property(b => b.ReqiuredChange).HasDefaultValue(false);
            modelBuilder.Entity<InsuranceDocument>().Property(b => b.UploadedDate).HasDefaultValueSql("GETDATE()");

            modelBuilder.Entity<UserData>().Property(x => x.DateOfBirth).IsRequired(false);
            modelBuilder.Entity<UserData>().Property(x => x.BusinessName).IsRequired(false);

            modelBuilder.Entity<Work>().Property(x => x.ToYear).IsRequired(false);

            modelBuilder.Entity<ProspectAppointment>().Property(b => b.StartDateTime).IsRequired(false);
            modelBuilder.Entity<ProspectAppointment>().Property(b => b.EndDateTime).IsRequired(false);

            modelBuilder.Entity<OccasionEvent>().Property(b => b.OtherType).IsRequired(false);
            modelBuilder.Entity<ClientPrevEsp>().Property(b => b.OtherType).IsRequired(false);

            modelBuilder.Entity<ClientActivityLog>().Property(m => m.Changes).HasConversion(value => JsonConvert.SerializeObject(value), serializedValue => JsonConvert.DeserializeObject<Dictionary<string, FieldChangeModel>>(serializedValue));
            modelBuilder.Entity<RiskArcItem>().Property(m => m.Options).HasConversion(new ValueConverter<string[], string>(v => string.Join(",", v), v => v.Split(",", StringSplitOptions.RemoveEmptyEntries).ToArray()));
            modelBuilder.Entity<UserData>().Property(m => m.WorkingHourConfig).HasConversion(value => JsonConvert.SerializeObject(value), serializedValue => JsonConvert.DeserializeObject<List<WorkingHourModel>>(serializedValue));
            modelBuilder.Entity<UserData>().Property(m => m.DashboardConfig).HasConversion(value => JsonConvert.SerializeObject(value), serializedValue => JsonConvert.DeserializeObject<List<DashboardConfigModel>>(serializedValue));
            modelBuilder.Entity<ClientData>().Property(m => m.DashboardConfig).HasConversion(value => JsonConvert.SerializeObject(value), serializedValue => JsonConvert.DeserializeObject<List<DashboardConfigModel>>(serializedValue));
            modelBuilder.Entity<TaxPrepHistory>().Property(m => m.FileNames).HasConversion(value => JsonConvert.SerializeObject(value), serializedValue => JsonConvert.DeserializeObject<List<string>>(serializedValue));
            modelBuilder.Entity<SqlResultModel>().HasNoKey();
            modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);
        }
        #region Master Table
        public virtual DbSet<PlanningItem> PlanningItems { get; set; }
        public virtual DbSet<BudgetItem> BudgetItems { get; set; }
        public virtual DbSet<DescriptionItem> DescriptionItems { get; set; }
        public virtual DbSet<RiskArcItem> RiskArcItems { get; set; }
        #endregion
        #region Client Table
        public virtual DbSet<ClientLogin> ClientLogins { get; set; }
        public virtual DbSet<ClientData> ClientDatas { get; set; }
        public virtual DbSet<SpouseData> SpouseDatas { get; set; }
        public virtual DbSet<XSpouseData> XSpouseDatas { get; set; }
        public virtual DbSet<ClientContactInfo> ClientContactInfos { get; set; }
        public virtual DbSet<ClientBudget> ClientBudgets { get; set; }
        public virtual DbSet<ClientOccupation> ClientOccupations { get; set; }
        public virtual DbSet<Heir> Heirs { get; set; }
        public virtual DbSet<ClientPlanning> ClientPlannings { get; set; }
        public virtual DbSet<ClientProperty> ClientProperties { get; set; }
        public virtual DbSet<ClientEsp> ClientEsps { get; set; }
        public virtual DbSet<ClientPrevEsp> ClientPrevEsps { get; set; }
        public virtual DbSet<BusinessInterest> BusinessInterests { get; set; }
        public virtual DbSet<OtherIncome> OtherIncomes { get; set; }
        public virtual DbSet<SocialSecurity> SocialSecurities { get; set; }
        public virtual DbSet<InvestmentLiteData> InvestmentLiteDatas { get; set; }
        public virtual DbSet<BankCreditUnionAccount> BankCreditUnionAccounts { get; set; }
        public virtual DbSet<BrokerageAdvisoryAccount> BrokerageAdvisoryAccounts { get; set; }
        public virtual DbSet<ClientEstateItem> ClientEstateItems { get; set; }
        public virtual DbSet<CharitablePlanning> CharitablePlannings { get; set; }
        public virtual DbSet<PersonalCare> PersonalCares { get; set; }
        public virtual DbSet<EducationPlanning> EducationPlannings { get; set; }
        public virtual DbSet<LifeAnnuityContract> LifeAnnuityContracts { get; set; }
        public virtual DbSet<ClientRiskArc> ClientRiskArcs { get; set; }
        public virtual DbSet<ClientHealthItem> ClientHealthItems { get; set; }
        public virtual DbSet<HealthCarePlanning> HealthCarePlannings { get; set; }
        public virtual DbSet<RetirementStatus> RetirementStatuses { get; set; }
        public virtual DbSet<ClientIncomeConcern> ClientIncomeConcerns { get; set; }
        public virtual DbSet<ClientExpenseConcern> ClientExpenseConcerns { get; set; }
        public virtual DbSet<ClientExpectation> ClientExpectations { get; set; }
        public virtual DbSet<ClientDocument> ClientDocuments { get; set; }
        public virtual DbSet<InsuranceDocument> InsuranceDocuments { get; set; }
        public virtual DbSet<TaxDocument> TaxDocuments { get; set; }
        public virtual DbSet<TaxPrepStatus> TaxPrepStatuses { get; set; }
        public virtual DbSet<TaxPrepHistory> TaxPrepHistories { get; set; }
        #endregion
        #region Adviser Table       
        public virtual DbSet<ComplianceDocument> ComplianceDocuments { get; set; }
        public virtual DbSet<AdviserContactInfo> AdviserContactInfos { get; set; }
        public virtual DbSet<PostHighSchoolEducation> PostHighSchoolEducations { get; set; }
        public virtual DbSet<Work> Works { get; set; }
        public virtual DbSet<Regulatory> Regulatories { get; set; }
        public virtual DbSet<Affiliation> Affiliations { get; set; }
        public virtual DbSet<ActivityEvent> Events { get; set; }
        public virtual DbSet<ProspectAppointment> ProspectAppointments { get; set; }
        public virtual DbSet<ClientAppointment> ClientAppointments { get; set; }
        public virtual DbSet<OccasionEvent> OccasionEvents { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }
        public virtual DbSet<LeadHistory> LeadHistories { get; set; }
        #endregion
        #region Admin Table
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<UserData> UserDatas { get; set; }
        public virtual DbSet<RegulatoryDocument> RegulatoryDocuments { get; set; }
        public virtual DbSet<Integration> Integrations { get; set; }
        public virtual DbSet<ClientActivityLog> ClientActivityLogs { get; set; }
        #endregion
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<AssistantPermission> AssistantPermissions { get; set; }
        public virtual DbSet<Firm> Firms { get; set; }
        public virtual DbSet<ReleaseNote> ReleaseNotes { get; set; }
        public virtual DbSet<SqlResultModel> SqlResultModels { get; set; }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            int ClientId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId")?.Value);
            int UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value);
            AuditBaseEntity();
            if (UserId > 0)
                await AuditUserHasChangesAsync();
            await AuditClientHasChangesAsync();
            var clientActivityLogs = GetClientActivityLog();
            bool hasMultipleEntries = false;
            if (clientActivityLogs.Any())
            {
                string[] multipleEntriesTables = { "ClientEstateItem", "ClientIncomeConcern", "ClientExpenseConcern", "ClientHealthItem", "ClientExpectation", "ClientRiskArc" };
                hasMultipleEntries = multipleEntriesTables.Contains(clientActivityLogs.FirstOrDefault().EntityName);
            }
            var result = await base.SaveChangesAsync(cancellationToken);
            if (clientActivityLogs.Any())
                OnAfterSaveChangesAsync(clientActivityLogs, hasMultipleEntries);
            return result;
        }
        public void AuditBaseEntity()
        {
            int ClientId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId")?.Value);
            int UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value);
            int OwnerUserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value);
            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            var role = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value;
            bool isAdmin = role == Roles.FirmPrincipal || role == Roles.FirmAdmin || role == Roles.SuperAdmin || role == Roles.Adviser;
            int Id = role == Roles.Client ? ClientId : role == Roles.Adviser || isAdmin ? UId : 0;
            var entries = ChangeTracker.Entries();
            foreach (var entityEntry in entries)
            {
                if (entityEntry.State == EntityState.Detached || entityEntry.State == EntityState.Unchanged || entityEntry.Entity is not BaseEntity)
                    continue;
                var entityId = entityEntry.Properties.Single(p => p.Metadata.IsPrimaryKey()).CurrentValue;
                if (entityEntry.State == EntityState.Added || (int)entityId == 0)
                {
                    var createdBy = entityEntry.CurrentValues["CreatedBy"];
                    var createdDate = Convert.ToDateTime(entityEntry.CurrentValues["CreatedDate"]);
                    ((BaseEntity)entityEntry.Entity).Guid = Guid.NewGuid();
                    ((BaseEntity)entityEntry.Entity).CreatedBy = createdBy is not null ? (int)createdBy : Id;
                    ((BaseEntity)entityEntry.Entity).CreatedDate = createdDate != DateTime.MinValue && !createdDate.ToString("dd/MM/yyyy").Contains("01/01/1900") ? createdDate : DateTime.UtcNow;
                }
                else if (entityEntry.State == EntityState.Modified || (int)entityId > 0)
                {
                    if (!hostUrl.Contains("support") && !hostUrl.Contains("localhost"))
                    {
                        var modifiedBy = entityEntry.CurrentValues["ModifiedBy"];
                        var modifiedDate = Convert.ToDateTime(entityEntry.CurrentValues["ModifiedDate"]);
                        ((BaseEntity)entityEntry.Entity).ModifiedBy = modifiedBy is not null ? (int)modifiedBy : Id;
                        ((BaseEntity)entityEntry.Entity).ModifiedDate = modifiedDate != DateTime.MinValue && !modifiedDate.ToString("dd/MM/yyyy").Contains("01/01/1900") ? modifiedDate : DateTime.UtcNow;
                    }
                }
            }
        }
        public async Task AuditUserHasChangesAsync()
        {
            int UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value);
            bool hasUpdate = false;
            string[] includeTables = { "UserData", "AdviserContactInfo", "PostHighSchoolEducation", "Work", "Regulatory", "Affiliation" };
            string[] excludeTableFields = { "LastLogin", "LastAccessPage", "CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate" };
            var entries = ChangeTracker.Entries();
            foreach (var entityEntry in entries.Where(x => x.State == EntityState.Modified))
            {
                var entityName = entityEntry.Metadata.ClrType.Name;
                if (includeTables.Contains(entityName))
                {
                    var properties = entityEntry.Properties.Where(x => x.IsModified && x.OriginalValue?.ToString() != x.CurrentValue?.ToString() && !excludeTableFields.Contains(x.Metadata.Name));
                    bool hasChangesExist = properties.Any();
                    if (hasChangesExist)
                    {
                        hasUpdate = hasChangesExist;
                        break;
                    }
                }
            }
            bool hasAddOrDelete = entries.Any(x => x.State == EntityState.Added || x.State == EntityState.Deleted);
            if (hasUpdate || hasAddOrDelete)
            {
                var user = await UserLogins.FirstOrDefaultAsync(m => m.Id == UserId);
                user.HasChanges = true;
            }
        }
        public async Task AuditClientHasChangesAsync()
        {
            int ClientId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId")?.Value);
            bool hasUpdateAol = false;
            bool hasUpdateCanvas = false;
            string[] includeAolTables = { "ClientData","SpouseData",  "ClientContactInfo", "Heir", "ClientProperty",
                    "ClientOccupation", "ClientEsp", "OtherIncome", "BusinessInterest","SocialSecurity","ClientBudget",
                    "InvestmentLiteData", "BankCreditUnionAccount","BrokerageAdvisoryAccount","LifeAnnuityContract","ClientEstateItem"};
            string[] includeAdditionalTables = { "RetirementStatus", "ClientIncomeConcern", "ClientExpenseConcern", "HealthCarePlanning", "PersonalCare", "EducationPlanning", "CharitablePlanning", "ClientHealthItem", "ClientExpectation" };
            string[] includeCanvasTables = includeAolTables.Concat(includeAdditionalTables).ToArray();
            string[] excludeTableFields = { "LastLogin", "LastAccessPage", "CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate", "OmniContactId", "RedtailContactId", "Guid", "HasChangesClientRiskArc", "HasChangesSpouseRiskArc", "HasChangesAol", "HasChangesCanvas", "HasChangesAol", "HasChangesCanvas", "InitialProfileSetupCompleted" };
            var entries = ChangeTracker.Entries();
            foreach (var entityEntry in entries.Where(x => x.State == EntityState.Modified))
            {
                var properties = entityEntry.Properties.Where(x => !excludeTableFields.Contains(x.Metadata.Name));
                var table = entityEntry.Metadata.GetTableName();
                var entityId = entityEntry.Properties.Single(p => p.Metadata.IsPrimaryKey()).CurrentValue;
                if (properties.Any())
                {
                    if (includeAolTables.Contains(table))
                    {
                        bool hasChangesExist = properties.Any();
                        if (hasChangesExist)
                            hasUpdateAol = hasChangesExist;
                    }
                    if (includeCanvasTables.Contains(table))
                    {
                        bool hasChangesExist = properties.Any();
                        if (hasChangesExist)
                            hasUpdateCanvas = hasChangesExist;
                    }
                }
            }
            if (ClientId > 0)
            {
                var client = await ClientLogins.FirstOrDefaultAsync(m => m.Id == ClientId);

                bool hasAddOrDeleteAol = entries.Where(m => includeAolTables.Contains(m.Metadata.GetTableName())).Any(x => x.State == EntityState.Added || x.State == EntityState.Deleted);
                if (hasUpdateAol || hasAddOrDeleteAol)
                    client.HasChangesAol = true;
                var AddOrDeleteCanvasEntries = entries.Where(m => includeCanvasTables.Contains(m.Metadata.GetTableName())).Where(x => x.State == EntityState.Added || x.State == EntityState.Deleted);
                bool hasAddCanvas = AddOrDeleteCanvasEntries.Any(m => m.State == EntityState.Added);
                bool hasDeleteCanvas = AddOrDeleteCanvasEntries.Any(m => m.State == EntityState.Deleted);
                if (hasUpdateCanvas || hasAddCanvas || hasDeleteCanvas)
                    client.HasChangesCanvas = true;
            }
        }
        private List<ClientActivityLog> GetClientActivityLog()
        {
            int ClientId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId")?.Value);
            int UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value);
            if (UserId == 0)
                UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "CreatedByUserId")?.Value);
            List<ClientActivityLog> clientActivityLogs = new();
            var entries = ChangeTracker.Entries();
            if (ClientId > 0)
            {
                string[] excludeTableFields = { "Id", "ClientId", "LastLogin", "LastAccessPage", "CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate", "OmniContactId", "RedtailContactId", "Guid", "HasChangesClientRiskArc", "HasChangesSpouseRiskArc", "HasChangesAol", "HasChangesCanvas", "HasChangesAol", "HasChangesCanvas", "InitialProfileSetupCompleted", "TwoFactorAuth", "businessInterestof", "BudgetItemId", "employeeOf", "DescriptionItemId", "SectionId", "ClientType", "IsDeleted", "HasPrevSpouse", "OccupationId", "ClergyIsMember", "OriginalInvestmentAmount", "Annuitant", "Password" };
                string[] excludeTables = { "ClientPlanning", "ClientActivityLog", "TaxPrepHistory", "TaxDocument", "TaxPrepStatus" };
                foreach (var entityEntry in entries)
                {
                    var allProperties = entityEntry.Properties.Where(m => !excludeTableFields.Contains(m.Metadata.Name));
                    var updatedProperties = entityEntry.State == EntityState.Modified ? allProperties.Where(m => m.CurrentValue?.ToString() != m.OriginalValue?.ToString()) : allProperties;
                    if ((entityEntry.State == EntityState.Modified && updatedProperties.Any()) || entityEntry.State == EntityState.Added || entityEntry.State == EntityState.Deleted)
                    {
                        var entityId = entityEntry.Properties.Single(p => p.Metadata.IsPrimaryKey()).CurrentValue.ToString();
                        var entityName = entityEntry.Metadata.ClrType.Name;
                        if (!excludeTables.Contains(entityName))
                        {
                            var moduleName = GetModuleName(updatedProperties, entityName);
                            var title = GetTitle(Convert.ToInt32(entityId), entityName);
                            string[] CurrencyFields = {"PricePaid","CurrentValue","AmountOwed","ProjectedFairMarketValueRetirement",
                                    "AnnualEarnings","ClergyAllowance","EspCurrentBalance", "EspContribution", "EspEmployerMatch" , "PreEmpCurrentBalance", "PreEmpAnnualAmount",
                                    "FMVofBusiness","PBValueatRetirement","AnnualIncome","ssmonthly","ssexpexting","ssChildBenefits","ProjectedAnnualBudget","InflationaryBudget","NonInflationaryBudget",
                                    "Amount","BalanceOwn","BankInflationaryInvestmentAmount","BankNonInflationaryInvestmentAmount","BrokerageInflationaryInvestmentAmount","BrokerageNonInflationaryInvestmentAmount",
                                    "AnnuityInflationaryInvestmentAmount","AnnuityNonInflationaryInvestmentAmount","PurchasePrice","CurrentBalance",
                                    "AnnualContribtion","AnnualPremiumAmount","ContractFaceAmount","LongTermCareRate","currentAmount","CashValue","PensionBenefit"};

                            string[] EnumStringFields = { "HeirOf", "Ownership", "FirmOwnershipEnum", "Owner", "EmploymentOf" };
                            string[] PercentageFields = { "PercentageOfOwnership", "EspAvgRateReturn", "PensionSurvivorBenefit", "EspAvgRateReturn", "AmountOfOwnership",
                                "AssumedInflationRate", "AvgRateReturn", "RateOfReturn", "inflationprovision", "BankInflationaryRateOfReturn",
                                "BankNonInflationaryRateOfReturn", "BrokerageInflationaryRateOfReturn", "BrokerageNonInflationaryRateOfReturn", "AnnuityInflationaryRateOfReturn",
                                "AnnuityNonInflationaryRateOfReturn", "EspContributionPercentage", "EspEmployerMatchPercentage","PreEmpAvgRateReturn","" };

                            string[] YearFields = { "YearsPayOff", "AgeToRetire", "PensionExpectedAge", "LastReviewedEnum", "Years", "sSageBenifitsEnum", "CurrentAge", "NumberOfUears", "numberOfYearsEnum" };
                            string[] MultiSelectEnumFields = { "EmploymentTypes", "EmployerOffer", "charitableDonations", "policyProvide", "LTCIPolicyProvideBenifitsEnum", "policyPayingBenifits" };
                            var changes = updatedProperties.Select(p => new { p.Metadata.Name, p.OriginalValue, p.CurrentValue }).ToDictionary(i => i.Name, i => new FieldChangeModel
                            {
                                OldValue = i.OriginalValue,
                                NewValue = i.CurrentValue,
                                TypeName = CurrencyFields.Contains(i.Name) ? "Currency" :
                                           EnumStringFields.Contains(i.Name) ? "EnumString" :
                                           PercentageFields.Contains(i.Name) ? "Percentage" :
                                           YearFields.Contains(i.Name) ? "Year" :
                                           MultiSelectEnumFields.Contains(i.Name) ? $"MultiSelectEnum:{GetMultiSelctType(i.Name)}" :
                                           i.CurrentValue != null ? i.CurrentValue?.GetType()?.FullName : i.OriginalValue?.GetType()?.FullName
                            });
                            var actionType = entityEntry.State == EntityState.Added ? ActionEnum.Insert.ToString() : entityEntry.State == EntityState.Modified ? ActionEnum.Update.ToString() : ActionEnum.Delete.ToString();
                            string role = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value;
                            var tempProperties = entityEntry.Properties.Where(p => p.IsTemporary).ToList();
                            var activityLog = new ClientActivityLog
                            {
                                OwnerId = ClientId,
                                CreatedByUserId = UserId > 0 ? UserId : null,
                                ActionType = actionType,
                                EntityModuleName = moduleName,
                                TimeStamp = DateTime.UtcNow,
                                Role = role,
                                Changes = changes,
                                EntityId = entityId,
                                EntityName = entityName,
                                Title = title,
                                TempProperties = tempProperties
                            };
                            clientActivityLogs.Add(activityLog);
                        }
                    }
                }
            }
            return clientActivityLogs;
        }
        private void OnAfterSaveChangesAsync(List<ClientActivityLog> clientActivityLogs, bool hasMultipleEntries)
        {
            foreach (var entry in clientActivityLogs)
            {
                foreach (var prop in entry.TempProperties)
                {
                    if (prop.Metadata.IsPrimaryKey())
                        entry.EntityId = prop.CurrentValue.ToString();
                    if (entry.Changes != null)
                    {
                        entry.Changes[prop.Metadata.Name] = new FieldChangeModel
                        {
                            OldValue = prop.OriginalValue,
                            NewValue = prop.CurrentValue,
                            TypeName = prop.CurrentValue?.GetType()?.FullName
                        };
                    }
                }
                var title = GetTitle(Convert.ToInt32(entry.EntityId), entry.EntityName);
                entry.Title = !string.IsNullOrEmpty(title) ? title : entry.Title;
                CustomChangedField(entry.Changes, Convert.ToInt32(entry.EntityId));
            }

            if (hasMultipleEntries)
            {
                var activityLog = clientActivityLogs.FirstOrDefault();
                clientActivityLogs = new List<ClientActivityLog>
                {
                    new ClientActivityLog
                    {
                        OwnerId = activityLog.OwnerId,
                        CreatedByUserId = activityLog.CreatedByUserId,
                        ActionType = ActionEnum.Update.ToString(),
                        EntityModuleName = activityLog.EntityModuleName,
                        TimeStamp = activityLog.TimeStamp,
                        Role = activityLog.Role,
                        EntityName = activityLog.EntityName,
                        Children= clientActivityLogs,
                        TempProperties = activityLog.TempProperties
                    }
                };
            }
            _activityLogService.SaveActivityLog(clientActivityLogs);
        }
        private static string GetModuleName(IEnumerable<PropertyEntry> properties, string entityName)
        {
            string moduleName = entityName;
            string[] maritalInfoFields = { "DateOfCurrentMarriage", "CareForSurvivingSpouse", "Prenup" };
            string[] budgetLiteFields = { "InflationaryBudget", "NonInflationaryBudget", "ProjectedAnnualBudget" };
            moduleName = properties.Any(m => maritalInfoFields.Contains(m.Metadata.Name)) ? "Marital Info" :
                         properties.Any(m => m.Metadata.Name.Contains("SpecialNeedsTrust")) ? "Family Info" :
                         properties.Any(m => budgetLiteFields.Contains(m.Metadata.Name) || m.Metadata.Name.Contains("AssumedInflationRate")) ? "Budget Info(Express)" :
                         properties.Any(m => m.Metadata.Name.Contains("FinalThought")) ? "Final Thought" : moduleName;

            var tableMap = GeTableModuleMappingDataAsync().FirstOrDefault(m => m.TableName == moduleName);
            if (tableMap != null)
                moduleName = moduleName.Replace(moduleName, tableMap.ModuleName);
            return moduleName;
        }
        public string GetTitle(int entityId, string entityName)
        {
            string title = string.Empty;
            if (entityId > 0)
            {
                int ClientId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId")?.Value);
                var client = ClientLogins.Where(m => m.Id == ClientId).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefault();
                string clientName = string.Empty, spouseName = string.Empty;
                if (client != null)
                {
                    clientName = client.ClientData?.FirstName;
                    spouseName = client.SpouseData?.FirstName;
                }
                if (entityName == "Heir")
                {
                    var heir = Heirs.FirstOrDefault(m => m.Id == entityId);
                    if (heir != null)
                        title = heir.Name;
                }
                else if (entityName == "ClientProperty")
                {
                    var property = ClientProperties.FirstOrDefault(m => m.Id == entityId);
                    if (property != null)
                    {
                        var nickname = !string.IsNullOrEmpty(property.Nickname) ? $" - {property.Nickname}" : string.Empty;
                        title = $"{property.Type.GetDisplayName()}{nickname}";
                    }
                }
                else if (entityName == "ClientOccupation" || entityName == "ClientEsp")
                {
                    var occupation = ClientOccupations.Where(m => (entityName == "ClientOccupation" && m.Id == entityId) || (entityName == "ClientEsp" && m.ClientEsp.Id == entityId)).Include(m => m.ClientLogin).Include(m => m.ClientLogin.ClientData)
                        .Include(m => m.ClientLogin.SpouseData).FirstOrDefault();
                    if (occupation != null)
                    {
                        var name = occupation.EmploymentOf == ClientTypeEnum.Client ? clientName : spouseName;
                        var type = entityName == "ClientOccupation" ? "Occupation" : "Retirement Plan";
                        title = $"{name}'s {type} - {occupation.Employer}";
                    }
                }
                else if (entityName == "ClientPrevEsp")
                {
                    var preRetirementPlan = ClientPrevEsps.Where(m => m.Id == entityId).FirstOrDefault();
                    if (preRetirementPlan != null)
                    {
                        var name = preRetirementPlan.EmploymentOf == ClientTypeEnum.Client ? clientName : spouseName;
                        title = $"{name}'s Previous Retirement Plan - {preRetirementPlan.EmployerName}";
                    }
                }
                else if (entityName == "BusinessInterest")
                {
                    var business = BusinessInterests.Where(m => m.Id == entityId).FirstOrDefault();
                    if (business != null)
                    {
                        var name = business.businessInterestOf == ClientTypeEnum.Client ? clientName : spouseName;
                        title = $"{business.BusinessName} - {name}";
                    }
                }
                else if (entityName == "OtherIncome")
                {
                    var personalActivity = OtherIncomes.Where(m => m.Id == entityId).FirstOrDefault();
                    if (personalActivity != null)
                    {
                        var name = personalActivity.businessInterestof == ClientTypeEnum.Client ? clientName : spouseName;
                        title = $"{personalActivity.sourceofIncome.GetDisplayName()} - {name}";
                    }
                }
                else if (entityName == "SocialSecurity")
                {
                    var socialSecurity = SocialSecurities.Where(m => m.Id == entityId).FirstOrDefault();
                    if (socialSecurity != null)
                    {
                        var name = socialSecurity.EmploymentOf == ClientTypeEnum.Client ? clientName : spouseName;
                        title = $"{name}'s Social Security";
                    }
                }
                else if (entityName == "ClientBudget")
                {
                    var budget = ClientBudgets.Where(m => m.Id == entityId).Include(m => m.BudgetItem).Include(m => m.BudgetItem.Parent).FirstOrDefault();
                    if (budget != null)
                        title = $"{budget.BudgetItem.ItemName}({budget.BudgetItem.Parent.ItemName})";
                }
                else if (entityName == "BankCreditUnionAccount")
                {
                    var bankCreditUnionAccount = BankCreditUnionAccounts.Where(m => m.Id == entityId).FirstOrDefault();
                    if (bankCreditUnionAccount != null)
                    {
                        var nickname = !string.IsNullOrEmpty(bankCreditUnionAccount.Nickname) ? $" - {bankCreditUnionAccount.Nickname}" : string.Empty;
                        title = $"{bankCreditUnionAccount.AccountTypeString}{nickname}";
                    }
                }
                else if (entityName == "BrokerageAdvisoryAccount")
                {
                    var brokerageAdvisoryAccount = BrokerageAdvisoryAccounts.Where(m => m.Id == entityId).FirstOrDefault();
                    if (brokerageAdvisoryAccount != null)
                    {
                        var nickname = !string.IsNullOrEmpty(brokerageAdvisoryAccount.Nickname) ? $" - {brokerageAdvisoryAccount.Nickname}" : string.Empty;
                        title = $"{brokerageAdvisoryAccount.AccountTypeString}{nickname}";
                    }
                }
                else if (entityName == "LifeAnnuityContract")
                {
                    var lifeAnnuityContract = LifeAnnuityContracts.Where(m => m.Id == entityId).FirstOrDefault();
                    if (lifeAnnuityContract != null)
                    {
                        var nickname = !string.IsNullOrEmpty(lifeAnnuityContract.Nickname) ? $" - {lifeAnnuityContract.Nickname}" : string.Empty;
                        title = $"{lifeAnnuityContract.AccountName}{nickname}";
                    }
                }
                else if (entityName == "RetirementStatus")
                {
                    var retirementStatus = RetirementStatuses.Where(m => m.Id == entityId).FirstOrDefault();
                    if (retirementStatus != null)
                    {
                        var name = retirementStatus.employeeOf == ClientTypeEnum.Client ? clientName : spouseName;
                        title = $"{name}'s Status";
                    }
                }
                else if (entityName == "ClientIncomeConcern")
                {
                    var incomeConcern = ClientIncomeConcerns.Where(m => m.Id == entityId).Include(m => m.DescriptionItems).FirstOrDefault();
                    if (incomeConcern != null)
                        title = incomeConcern.DescriptionItems.itemDesc;
                }
                else if (entityName == "ClientExpenseConcern")
                {
                    var expenseConcern = ClientExpenseConcerns.Where(m => m.Id == entityId).Include(m => m.DescriptionItems).FirstOrDefault();
                    if (expenseConcern != null)
                        title = expenseConcern.DescriptionItems.itemDesc;
                }
                else if (entityName == "PersonalCare")
                {
                    var personalCare = PersonalCares.Where(m => m.Id == entityId).FirstOrDefault();
                    if (personalCare != null)
                    {
                        var name = personalCare.ClientType == ClientTypeEnum.Client ? clientName : spouseName;
                        title = $"{name}'s Personal Care";
                    }
                }
                else if (entityName == "EducationPlanning")
                {
                    var student = EducationPlannings.Where(m => m.Id == entityId).FirstOrDefault();
                    if (student != null)
                        title = student.StudentName;
                }
                else if (entityName == "ClientEstateItem")
                {
                    var estateItem = ClientEstateItems.Where(m => m.Id == entityId).Include(m => m.DescriptionItem).FirstOrDefault();
                    if (estateItem != null)
                        title = estateItem.DescriptionItem.itemDesc;
                }
                else if (entityName == "ClientHealthItem")
                {
                    var healthItem = ClientHealthItems.Where(m => m.Id == entityId).Include(m => m.DescriptionItems).FirstOrDefault();
                    if (healthItem != null)
                        title = healthItem.DescriptionItems.itemDesc;
                }
                else if (entityName == "ClientExpectation")
                {
                    var expectation = ClientExpectations.Where(m => m.Id == entityId).Include(m => m.DescriptionItems).FirstOrDefault();
                    if (expectation != null)
                        title = $"{expectation.DescriptionItems.itemDesc}({expectation.DescriptionItems.tabletype.GetDisplayName()})";
                }
                else if (entityName == "ClientRiskArc")
                {
                    var riskArc = ClientRiskArcs.Where(m => m.Id == entityId).Include(m => m.RiskArcItem).FirstOrDefault();
                    if (riskArc != null)
                        title = riskArc.RiskArcItem.Question;
                }
            }
            return title;
        }
        public static IEnumerable<TableModuleModel> GeTableModuleMappingDataAsync()
        {
            var data = new List<TableModuleModel>
            {
                new TableModuleModel
                {
                    TableName="ClientLogin",
                    ModuleName="Client"
                },
                new TableModuleModel
                {
                    TableName="ClientData",
                    ModuleName="Client Info"
                },
                new TableModuleModel
                {
                    TableName="SpouseData",
                    ModuleName="Spouse Info"
                },
                new TableModuleModel
                {
                    TableName="ClientContactInfo",
                    ModuleName="Contact Info"
                },
                new TableModuleModel
                {
                    TableName="Heir",
                    ModuleName="Heir"
                },
                new TableModuleModel
                {
                    TableName="ClientProperty",
                    ModuleName="Property"
                },
                new TableModuleModel
                {
                    TableName="ClientOccupation",
                    ModuleName="Employment"
                },
                new TableModuleModel
                {
                    TableName="ClientEsp",
                    ModuleName="Retirement Plan"
                },
                new TableModuleModel
                {
                    TableName="ClientPrevEsp",
                    ModuleName="Previous Retirement Plan"
                },
                new TableModuleModel
                {
                    TableName="OtherIncome",
                    ModuleName="Personal Activity"
                },
                new TableModuleModel
                {
                    TableName="BusinessInterest",
                    ModuleName="Business Interest"
                },
                new TableModuleModel
                {
                    TableName="SocialSecurity",
                    ModuleName="Social Security"
                },
                new TableModuleModel
                {
                    TableName="ClientBudget",
                    ModuleName="Budget Info"
                },
                new TableModuleModel
                {
                    TableName="InvestmentLiteData",
                    ModuleName="Investment Planning(Express)"
                },
                new TableModuleModel
                {
                    TableName="BankCreditUnionAccount",
                    ModuleName="Bank & Credit Union Account"
                },
                new TableModuleModel
                {
                    TableName="BrokerageAdvisoryAccount",
                    ModuleName="Brokerage & Advisory Account"
                },
                new TableModuleModel
                {
                    TableName="LifeAnnuityContract",
                    ModuleName="Life/Annuity Contract"
                },
                new TableModuleModel
                {
                    TableName="ClientEstateItem",
                    ModuleName="Estate Planning"
                },
                new TableModuleModel
                {
                    TableName="RetirementStatus",
                    ModuleName="Retirement Income Status"
                },
                new TableModuleModel
                {
                    TableName="ClientIncomeConcern",
                    ModuleName="Income Concerns"
                },
                new TableModuleModel
                {
                    TableName="ClientExpenseConcern",
                    ModuleName="Expense Concerns"
                },
                new TableModuleModel
                {
                    TableName="HealthCarePlanning",
                    ModuleName="Health Care Planning"
                },
                new TableModuleModel
                {
                    TableName="PersonalCare",
                    ModuleName="Personal Care Planning"
                },
                new TableModuleModel
                {
                    TableName="EducationPlanning",
                    ModuleName="Student in Education Planning"
                },
                new TableModuleModel
                {
                    TableName="CharitablePlanning",
                    ModuleName="Charitable Planning"
                },
                new TableModuleModel
                {
                    TableName="ClientHealthItem",
                    ModuleName="Health Assessment"
                },
                new TableModuleModel
                {
                    TableName="ClientExpectation",
                    ModuleName="Health Expectation"
                },
                new TableModuleModel
                {
                    TableName="ClientRiskArc",
                    ModuleName="RiskArc"
                }
            };
            return data;
        }
        public static string GetMultiSelctType(string fieldName)
        {
            Dictionary<string, string> MultiSelectList = new()
            {
                { "EmploymentTypes", "EmploymentTypeEnum" },
                { "EmployerOffer", "EmployerOfferEnum" },
                { "charitableDonations", "CharitableDonationEnum" },
                { "policyProvide", "PolicyProvideCareEnum" },
                { "LTCIPolicyProvideBenifitsEnum", "LTCIPolicyProvideBenifitsEnum" },
                { "policyPayingBenifits", "PolicyBeginBenefits" }
            };
            var result = MultiSelectList.FirstOrDefault(m => m.Key == fieldName);
            return result.Value;
        }
        public void CustomChangedField(Dictionary<string, FieldChangeModel> changes, int entityId)
        {
            if (changes.Any(m => m.Key == "Answer"))
            {
                var risk = ClientRiskArcs.Where(m => m.Id == entityId).Include(m => m.RiskArcItem).FirstOrDefault();
                var options = risk.RiskArcItem.Options;
                foreach (var change in changes.Where(m => m.Key == "Answer"))
                {
                    change.Value.OldValue = options[Convert.ToInt16(change.Value.OldValue) - 1];
                    change.Value.NewValue = options[Convert.ToInt16(change.Value.NewValue) - 1];
                }
            }
            if (changes.Any(m => m.Key == "CareGiverId"))
            {
                List<Heir> careGiverHeirs = new();
                foreach (var change in changes.Where(m => m.Key == "CareGiverId"))
                {
                    int oldValue = Convert.ToInt32(change.Value.OldValue);
                    int newValue = Convert.ToInt32(change.Value.NewValue);
                    int[] careGiverIds = { oldValue, newValue };
                    careGiverHeirs = Heirs.Where(m => careGiverIds.Contains(m.Id)).ToList();
                    change.Value.OldValue = oldValue > 0 ? careGiverHeirs.FirstOrDefault(m => m.Id == oldValue).FirstName :
                                           oldValue == -1 ? "Other" : oldValue == -2 ? "{clientName}" : oldValue == -3 ? "{spouseName}" : string.Empty;
                    change.Value.NewValue = newValue > 0 ? careGiverHeirs.FirstOrDefault(m => m.Id == newValue).FirstName :
                                           newValue == -1 ? "Other"
                                           : newValue == -2 ? "{clientName}" : newValue == -3 ? "{spouseName}" : string.Empty;
                }
            }
        }
    }
}