﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Master
{
    [Table(name: "BudgetItem", Schema = "Master")]
    public partial class BudgetItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string ItemName { get; set; }
        public int SortOrder { get; set; }
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public virtual BudgetItem Parent { get; set; }
        public bool IsRevolvingDebt { get; set; }
        public virtual List<BudgetItem> Children { get; set; }

    }
}
