﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Master
{
    [Table(name: "PlanningItem", Schema = "Master")]
    public partial class PlanningItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string ItemName { get; set; }
        public int SortOrder { get; set; }
    }
}