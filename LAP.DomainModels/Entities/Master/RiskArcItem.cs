﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Master
{
    [Table(name: "RiskArcItem", Schema = "Master")]
    public partial class RiskArcItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Question { get; set; }
        public string[] Options { get; set; }
    }
}