﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Master
{
    [Table(name: "DescriptionItem", Schema = "Master")]
    public class DescriptionItem
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string itemDesc { get; set; }
        public int sortOrder { get; set; }
        public DescritpionTableNameEnum tabletype { get; set; }
    }
}
