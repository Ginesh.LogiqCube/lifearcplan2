﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "HealthCarePlanning", Schema = "Client")]
    public class HealthCarePlanning : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public bool underESP { get; set; }
        public bool underME { get; set; }
        public bool uninsured { get; set; }
        public bool longTermCare { get; set; }
        public int LongTermCareRate { get; set; }
        public string policyProvide { get; set; }
        public string policyPayingBenifits { get; set; }
        public YesNoEnum waitingPeriodYesNOEnum { get; set; }
        public WaitingPeriodEnum waitingPeriodEnum { get; set; }
        public YesNoEnum LTCIBenifitsEnumYesNO { get; set; }
        public LTCIbenifitsEnum LTCIbenifitsEnum { get; set; }
        public LTCIbenifirPayouttEnum lTCIbenifirPayouttEnum { get; set; }
        public YesNoEnum inflationProtectionEnum { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal inflationprovision { get; set; }
        public string LTCIPolicyProvideBenifitsEnum { get; set; }

    }
}
