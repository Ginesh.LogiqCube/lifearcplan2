﻿using LAP.DomainModels.Entities.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientRiskArc", Schema = "Client")]
    public class ClientRiskArc : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public int SectionId { get; set; }
        public int Answer { get; set; }
        public ClientTypeEnum ClientType { get; set; }

        [ForeignKey("SectionId")]
        public virtual RiskArcItem RiskArcItem { get; set; }
    }
}
