﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "SocialSecurity", Schema = "Client")]
    public class SocialSecurity : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public ClientTypeEnum EmploymentOf { get; set; }
        [NotMapped]
        public string EmploymentOfWithData { get; set; }
        public bool ssreceiving { get; set; }
        [DisplayName("What is your monthly Social Security Benefit?")]
        public int ssmonthly { get; set; }
        public bool ssGovPension { get; set; }
        public bool ssretirementbenefit { get; set; }
        public bool ssLegally { get; set; }
        public string ssexemptionStatus { get; set; }
        public int sSageBenifitsEnum { get; set; }
        public int ssexpexting { get; set; }
        public int ssChildBenefits { get; set; }
        public SSexemptionEnum sSexemptionEnum { get; set; }
        [NotMapped]
        public string EmployerTitle { get; set; }
    }
}
