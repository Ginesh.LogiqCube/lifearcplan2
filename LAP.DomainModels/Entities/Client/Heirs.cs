﻿using LAP.DomainModels.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "Heir", Schema = "Client")]
    public partial class Heir : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "First Name", Prompt = "First Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name", Prompt = "Middle Name")]
        public string MiddleInitial { get; set; }
        [Display(Name = "Last Name", Prompt = "Last Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string LastName { get; set; }
        [Display(Name = "Date Of Birth", Prompt = "Date Of Birth")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Heir Of")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string HeirOf { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public RelationshipEnum Relationship { get; set; }
        [NotMapped]
        public string RelationshipString { get { return Relationship.GetDisplayName(); } }
        public bool Inheritance { get; set; }
        [Display(Name = "Special Needs")]
        public bool SpecialNeeds { get; set; }
        [NotMapped]
        public string DateOfBirthFormated
        {
            get { return DateOfBirth.ToString("MMM dd, yyyy"); }
        }
        [NotMapped]
        public string HeirOfWithData { get; set; }
        [NotMapped]
        public string Name
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
        }
    }
}
