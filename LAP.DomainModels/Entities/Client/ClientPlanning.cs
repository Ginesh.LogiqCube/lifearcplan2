﻿using LAP.DomainModels.Entities.Master;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientPlanning", Schema = "Client")]
    public partial class ClientPlanning : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }

        [ForeignKey("PlanningItem")]
        public int PlanningItemId { get; set; }
        public virtual PlanningItem PlanningItem { get; set; }
        public ImportanceEnum Importance { get; set; }
        [NotMapped]
        public bool Active { get; set; }
    }
}
