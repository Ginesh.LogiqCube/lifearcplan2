﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientProperty", Schema = "Client")]
    public class ClientProperty : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "Property Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public PropertyTypeEnum Type { get; set; }
        [Display(Name = "Purchase Price", Prompt = "Purchase Price")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int PricePaid { get; set; }
        [Display(Name = "Current Value", Prompt = "Current Value")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int CurrentValue { get; set; }
        [Display(Name = "Balance owed", Prompt = "Balance owed")]
        public int? AmountOwed { get; set; }
        [Display(Name = "Years Till Pay Off", Prompt = "Years Till Pay Off")]
        public int? YearsPayOff { get; set; }
        [Display(Name = "Own By")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Ownership { get; set; }
        [Display(Name = "Plans for Property")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ProprtyPlanEnum PropertyPlan { get; set; }
        [NotMapped]
        public string PropertyPlanString { get { return PropertyPlan.GetDisplayName(); } }
        [NotMapped]
        public string PropertyTypeString { get { return Type.GetDisplayName(); } }
        [Display(Name = "Projected Value at Retirement", Prompt = "Projected Value at Retirement")]
        public int? ProjectedFairMarketValueRetirement { get; set; }
        [Display(Name = "Ownership(%)", Prompt = "Ownership(%)")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal PercentageOfOwnership { get; set; }
        [NotMapped]
        public string OwnByWithData { get; set; }
        [Display(Name = "Amount of My Annual Income")]
        [Required(ErrorMessage = "Provide My Annual Income")]
        public int AnnualIncome { get; set; }
        [Display(Name = "Duration(Years)")]
        public int Years { get; set; }
        [Display(Prompt = "Nickname")]
        public string Nickname { get; set; }
        [NotMapped]
        public bool Incomegenerated
        {
            get
            {
                return AnnualIncome > 0;
            }
        }

    }
}
