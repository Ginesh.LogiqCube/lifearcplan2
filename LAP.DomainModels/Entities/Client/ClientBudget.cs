﻿using LAP.DomainModels.Entities.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientBudget", Schema = "Client")]
    public partial class ClientBudget : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [ForeignKey("BudgetItem")]
        [Display(Name = "Budget Item")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int BudgetItemId { get; set; }
        public virtual BudgetItem BudgetItem { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int Amount { get; set; }
        public int? BalanceOwn { get; set; }
        public int YearsPayOff { get; set; }
        public bool Inflationary { get; set; }
        [NotMapped]
        [Display(Name = "Budget Category")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int BudgetType { get; set; }
        [NotMapped]
        public string AmountFormated
        {
            get { return string.Format("{0:C0}", Amount); }
        }
        [NotMapped]
        public string BalanceOwnFormated
        {
            get { return string.Format("{0:C0}", BalanceOwn); }
        }
    }
}
