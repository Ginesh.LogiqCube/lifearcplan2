﻿using LAP.DomainModels.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientData", Schema = "Client")]
    public partial class ClientData : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public GenderEnum Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public MaritalStatusEnum MaritalStatus { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateOfCurrentMarriage { get; set; }
        public TaxFilingTypeEnum TaxFilingType { get; set; }
        public bool Prenup { get; set; }
        public bool CareForSurvivingSpouse { get; set; }
        public bool IsCitizen { get; set; }
        public bool IsPermanantCitizen { get; set; }
        public bool MarriedTenYears { get; set; }
        public bool IsMilitary { get; set; }
        public bool ActiveService { get; set; }
        public bool Discharge { get; set; }
        public bool IsAdviserAssisted { get; set; }
        public bool SpecialNeedsTrust { get; set; }
        public int InflationaryBudget { get; set; }
        public int NonInflationaryBudget { get; set; }
        public int ProjectedAnnualBudget { get; set; }
        public bool? HasPrevSpouse { get; set; }
        public bool BudgetVersion { get; set; }
        public bool InvestmentVersion { get; set; }
        public bool RiskArcVersion { get; set; }
        public string LastAccessPage { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal AssumedInflationRate { get; set; }
        [Display(Prompt = "Final Thought")]
        public string FinalThought { get; set; }
        public int RedtailContactId { get; set; }
        public string OmniContactId { get; set; }
        public List<DashboardConfigModel> DashboardConfig { get; set; }
        [NotMapped]
        public string Name
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
        }
        [NotMapped]
        public string Initial
        {
            get
            {
                string initial = string.Empty;
                if (!string.IsNullOrEmpty(FirstName))
                    initial = $"{FirstName[..1]}{LastName[..1]}";
                return initial;
            }
        }
        [NotMapped]
        public bool SurveyCompleted
        {
            get { return !string.IsNullOrEmpty(FinalThought); }
        }
    }
}