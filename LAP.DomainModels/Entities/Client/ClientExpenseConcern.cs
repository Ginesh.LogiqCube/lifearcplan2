﻿using LAP.DomainModels.Entities.Master;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientExpenseConcern", Schema = "Client")]
    public class ClientExpenseConcern : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [ForeignKey("DescriptionItem")]
        public int DescriptionItemId { get; set; }
        public virtual DescriptionItem DescriptionItems { get; set; }
        public RadioListEnum radioListforClientEnum { get; set; }
        public RadioListEnum radioListforSpouseEnum { get; set; }
    }
}
