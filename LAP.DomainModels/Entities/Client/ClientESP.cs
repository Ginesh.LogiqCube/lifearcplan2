﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientEsp", Schema = "Client")]
    public class ClientEsp : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [ForeignKey("ClientOccupation")]
        public int? OccupationId { get; set; }
        public virtual ClientOccupation ClientOccupation { get; set; }
        public string EmployerOffer { get; set; }
        public int EspContribution { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal EspContributionPercentage { get; set; }
        [NotMapped]
        public int EspContributionCalculated
        {
            get
            {
                decimal result = EspContribution > 0 ? EspContribution : Convert.ToDecimal(ClientOccupation?.AnnualEarnings) * EspContributionPercentage / 100;
                return Convert.ToInt32(result);
            }
        }
        public int EspEmployerMatch { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal EspEmployerMatchPercentage { get; set; }
        [NotMapped]
        public int EspEmployerMatchCalculated
        {
            get
            {
                decimal result = EspEmployerMatch > 0 ? EspEmployerMatch : Convert.ToDecimal(ClientOccupation?.AnnualEarnings) * EspEmployerMatchPercentage / 100;
                return Convert.ToInt32(result);
            }
        }

        public int EspCurrentBalance { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal EspAvgRateReturn { get; set; }

        public int PensionExpectedAge { get; set; }
        public int PensionBenefit { get; set; }
        [NotMapped]
        public bool HasPensionPlan
        {
            get
            {
                return PensionSurvivorBenefit > 0 || PensionExpectedAge > 0 || PensionBenefit > 0;
            }
        }
        [Column(TypeName = "decimal(18,2)")]
        public decimal PensionSurvivorBenefit { get; set; }
        [NotMapped]
        public string EmployerOfferString
        {
            get
            {
                string result = string.Empty;
                if (!string.IsNullOrEmpty(EmployerOffer))
                {
                    var split = EmployerOffer.Split(",");
                    foreach (var item in split)
                    {
                        EmployerOfferEnum employmentType = (EmployerOfferEnum)Enum.Parse(typeof(EmployerOfferEnum), item);
                        result += employmentType.GetDisplayName() + ", ";
                    }
                    result = result[..result.LastIndexOf(",")];
                }
                return result;
            }
        }       
    }
}