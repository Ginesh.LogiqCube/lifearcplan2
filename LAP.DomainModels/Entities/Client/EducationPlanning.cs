﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "EducationPlanning", Schema = "Client")]
    public class EducationPlanning : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "Name of Student")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string StudentName { get; set; }
        [Display(Name = "Current Age of Student")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int CurrentAge { get; set; }
        [Display(Name = "Number of years before student will begin education program")]
        [Required(ErrorMessage = "Provide Number of years")]
        public int NumberOfUears { get; set; }
        [Display(Name = "Type of education program")]
        [Required(ErrorMessage = "Provide Type of education")]
        public TypeOfEducationProgramEnum typeOfEducationProgramEnum { get; set; }
        [Display(Name = "Expected number of years to complete education")]
        [Required(ErrorMessage = "Provide Expected number of years")]
        public int numberOfYearsEnum { get; set; }
        [Display(Name = "How are you saving for education")]
        [Required(ErrorMessage = "Provide Saving plan")]
        public SavingForEducationEnum savingForEducationEnum { get; set; }
        [Display(Name = "Current amount saved for education")]
        [Required(ErrorMessage = "Provide saved amount")]
        public int currentAmount { get; set; }
        public string CFSStudentId { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Student's Resident State")]
        public string State { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Relationship to Student")]
        public CFSRelationshipEnum RelationshipToStudent { get; set; }
        [Display(Name = "Custodial Parent")]
        public bool LegalGuardian { get; set; }
        [NotMapped]
        public string RelationshipToStudentString { get { return RelationshipToStudent.GetDisplayName(); } }
        [NotMapped]
        public string typeOfEducationProgramEnumString { get { return typeOfEducationProgramEnum.GetDisplayName(); } }
        [NotMapped]
        public string savingForEducationEnumString { get { return savingForEducationEnum.GetDisplayName(); } }

    }
}
