﻿using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientExpectation", Schema = "Client")]
    public class ClientExpectation : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [ForeignKey("DescriptionItem")]
        public int DescriptionItemId { get; set; }
        public virtual DescriptionItem DescriptionItems { get; set; }
        public bool isClientActive { get; set; }
        public bool isSpouseActive { get; set; }
    }
}
