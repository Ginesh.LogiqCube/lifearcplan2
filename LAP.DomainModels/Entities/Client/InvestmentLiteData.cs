﻿using LAP.DomainModels.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "InvestmentLiteData", Schema = "Client")]
    public class InvestmentLiteData : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public int BankInflationaryInvestmentAmount { get; set; }
        public int BankNonInflationaryInvestmentAmount { get; set; }
        public int BrokerageInflationaryInvestmentAmount { get; set; }
        public int BrokerageNonInflationaryInvestmentAmount { get; set; }
        public int AnnuityInflationaryInvestmentAmount { get; set; }
        public int AnnuityNonInflationaryInvestmentAmount { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? BankInflationaryRateOfReturn { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? BankNonInflationaryRateOfReturn { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? BrokerageInflationaryRateOfReturn { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? BrokerageNonInflationaryRateOfReturn { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? AnnuityInflationaryRateOfReturn { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? AnnuityNonInflationaryRateOfReturn { get; set; }
    }
}
