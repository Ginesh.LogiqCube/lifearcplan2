﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientContactInfo", Schema = "Client")]
    public partial class ClientContactInfo : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string POBox { get; set; }
        public string Phone { get; set; }
        public string CellNumber { get; set; }
        public string SpouseCellNumber { get; set; }
        public string SpouseEmail { get; set; }
        public string Country { get; set; }
    }
}