﻿using LAP.DomainModels.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "LifeAnnuityContract", Schema = "Client")]
    public class LifeAnnuityContract : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "Contract Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ContractTypeEnum ContractType { get; set; }
        [Display(Name = "Account Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string AccountName { get; set; }
        [Display(Name = "Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public LifeAnnuityContractTypeEnum LifeAnnuityContractType { get; set; }
        [Display(Name = "Date of Issue")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime IssueDate { get; set; }
        [Display(Name = "Owner")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Owner { get; set; }
        [Display(Name = "Insured")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Insured { get; set; }
        [Display(Name = "Contract Face Amount")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int ContractFaceAmount { get; set; }
        [Display(Name = "Annual Premium Amount")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int AnnualPremiumAmount { get; set; }
        [Display(Name = "Cash Value")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int CashValue { get; set; }
        [Display(Name = "Duration(Years)")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int Years { get; set; }
        [Display(Name = "Annual Income")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int AnnualIncome { get; set; }
        public int Annuitant { get; set; }
        [Display(Name = "Tax Classification")]
        public TaxClassificationEnum TaxClassification { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        [Display(Name = "Rate Of Return")]
        public decimal? RateOfReturn { get; set; }
        public int OriginalInvestmentAmount { get; set; }
        [Display(Name = "Annual Contribution")]
        public int AnnualContribution { get; set; }
        [Display(Name = "Current Balance")]
        public int CurrentBalance { get; set; }
        [NotMapped]
        public string OwnByWithData { get; set; }
        [NotMapped]
        public string TypeString { get { return ContractType.ToString(); } }
        [NotMapped]
        public string TaxClassificationString { get { return TaxClassification.GetDisplayName(); } }
        [NotMapped]
        public string LifeAnnuityContractTypeString { get { return LifeAnnuityContractType.GetDisplayName(); } }
        [Display(Prompt = "Nickname")]
        public string Nickname { get; set; }
        [NotMapped]
        public bool HasAnnualIncome
        {
            get
            {
                return AnnualIncome > 0;
            }
        }
        [NotMapped]
        public string IssueDateFormated
        {
            get { return IssueDate.ToString("MMM dd, yyyy"); }
        }
    }
}
