﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "PersonalCare", Schema = "Client")]
    public class PersonalCare : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public ClientTypeEnum ClientType { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Required(ErrorMessage = Message.SELECT_ONE)]
        public PersonalCareEnum PersonalCareType { get; set; }
        public int? CareGiverId { get; set; }
        [Required(ErrorMessage = "Enter Caregiver Name")]
        [MaxLength(30)]
        public string CaregiverName { get; set; }
        [Required(ErrorMessage = "Please Select Relationship")]
        public PersonalCareRelationShipEnum Relationship { get; set; }
        [NotMapped]
        public string Name { get; set; }

    }
}
