﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "TaxPrepStatus", Schema = "Client")]
    public class TaxPrepStatus : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public TaxFilingPrepStatusEnum Status { get; set; }
        public string TaxPrepStatusString
        {
            get
            {
                return Status.GetDisplayName();
            }
        }
        public int Year { get; set; }
    }
}