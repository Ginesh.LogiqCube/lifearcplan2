﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "BankCreditUnionAccount", Schema = "Client")]
    public class BankCreditUnionAccount : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "Account Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public BankCreditUnionAccountTypeEnum AccountType { get; set; }
        [Display(Name = "Owner")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Owner { get; set; }
        [Display(Name = "Tax Classification")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public TaxClassificationEnum TaxClassification { get; set; }
        [Display(Name = "Initial Contribution")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int PurchasePrice { get; set; }
        [Display(Name = "Current Balance")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int CurrentBalance { get; set; }
        [Display(Name = "Annual Contribution")]
        public int? AnnualContribtion { get; set; }
        [Display(Name = "Rate Of Return")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal? AvgRateReturn { get; set; }
        [Display(Name = "Duration(Years)")]
        public int? Years { get; set; }
        [Display(Name = "Annual Income")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int AnnualIncome { get; set; }
        [NotMapped]
        public string OwnByWithData { get; set; }
        [NotMapped]
        public string AccountTypeString { get { return AccountType.GetDisplayName(); } }
        [NotMapped]
        public string TaxClassificationString { get { return TaxClassification.GetDisplayName(); } }
        [Display(Prompt = "Nickname")]
        public string Nickname { get; set; }
        [NotMapped]
        public bool HasAnnualIncome
        {
            get
            {
                return AnnualIncome > 0;
            }
        }
    }
}
