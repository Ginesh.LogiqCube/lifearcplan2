﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "SpouseData", Schema = "Client")]
    public partial class SpouseData : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public GenderEnum Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsCitizen { get; set; }
        public bool IsMilitary { get; set; }
        public bool ActiveService { get; set; }
        public bool Discharge { get; set; }
        public int RedtailContactId { get; set; }
        [NotMapped]
        public string Name
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
        }
        [NotMapped]
        public string Initial
        {
            get
            {
                string initial = string.Empty;
                if (!string.IsNullOrEmpty(FirstName))
                    initial = $"{FirstName[..1]}{LastName[..1]}";
                return initial;
            }
        }
    }
}