﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "CharitablePlanning", Schema = "Client")]
    public class CharitablePlanning : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public CharitableEstateEnum charitableEstateEnum { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string charitableDonations { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public CharitableGiftEnum charitableGiftEnum { get; set; }
    }
}
