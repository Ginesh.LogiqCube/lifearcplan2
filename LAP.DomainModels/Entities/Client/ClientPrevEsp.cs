﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientPrevEsp", Schema = "Client")]
    public class ClientPrevEsp : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "Employer")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string EmployerName { get; set; }
        public ClientTypeEnum EmploymentOf { get; set; }
        [NotMapped]
        public string EmploymentOfWithData { get; set; }
        [Display(Name = "Current Balance")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int PreEmpCurrentBalance { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        [Display(Name = "Rate of Return")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public decimal PreEmpAvgRateReturn { get; set; }
        [NotMapped]
        [Display(Name = "Income?")]
        public bool HasPreEmpIncome
        {
            get
            {
                return PreEmpAnnualAmount > 0;
            }
        }
        [Display(Name = "Annual Amount")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int PreEmpAnnualAmount { get; set; }
        [Display(Name = "Type of Retirement Plan")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public RetirementPlanTypeEnum Type { get; set; }
        public string TypeString
        {
            get { return Type.GetDisplayName(); }
        }

        [Display(Name = "Other Description")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string OtherType { get; set; }
    }
}
