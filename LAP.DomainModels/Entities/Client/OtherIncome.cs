﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "OtherIncome", Schema = "Client")]
    public class OtherIncome : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [DisplayName("Personal Activity Of")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ClientTypeEnum businessInterestof { get; set; }
        [DisplayName("Source of Income")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public SourceofIncomeEnum sourceofIncome { get; set; }
        [DisplayName("Amount of Annual Income")]
        [Required(ErrorMessage = "Provide Annual Income")]
        public int AnnualIncome { get; set; }
        [DisplayName("Duration(Years)")]
        [Required(ErrorMessage = "Provide number of years")]
        public int Years { get; set; }
        [NotMapped]
        public string sourceofIncomeString { get { return sourceofIncome.GetDisplayName(); } }
        [NotMapped]
        public string businessInterestofString { get; set; }
    }
}
