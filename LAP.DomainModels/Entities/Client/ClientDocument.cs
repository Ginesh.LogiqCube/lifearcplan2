﻿using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientDocument", Schema = "Client")]
    public class ClientDocument : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public string FileName { get; set; }
        [NotMapped]
        public string Extension
        {
            get { return Path.GetExtension(FileName).Replace(".", ""); }
        }
        public string ContentType { get; set; }
        public long FileSize { get; set; }
        public string VersionId { get; set; }
        public bool IsAccesibleClient { get; set; } = false;
    }
}
