﻿using LAP.DomainModels.Entities.Master;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientEstateItem", Schema = "Client")]
    public class ClientEstateItem : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [ForeignKey("DescriptionItem")]
        public int DescriptionItemId { get; set; }
        public virtual DescriptionItem DescriptionItem { get; set; }
        public bool isClientActive { get; set; }
        public bool isSpouseActive { get; set; }
    }
}
