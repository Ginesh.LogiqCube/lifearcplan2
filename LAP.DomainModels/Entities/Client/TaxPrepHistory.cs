﻿using LAP.DomainModels.Entities.Admin;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "TaxPrepHistory", Schema = "Client")]
    public class TaxPrepHistory : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public ClientLogin ClientLogin { get; set; }
        public string ActionType { get; set; }
        public TaxFilingPrepStatusEnum Status { get; set; }
        public int Year { get; set; }
        [ForeignKey("UserLogin")]
        public int? ActionByUserId { get; set; }
        public UserLogin ActionByUser { get; set; }
        public IEnumerable<string> FileNames { get; set; }
        public string Message
        {
            get
            {
                var suffix = ActionType == "Update" ? "d" : "ed";
                var additionalMessage = ActionType == "Update" ? $"@CLIENTNAME to <b>{Status.GetDisplayName()}</b>" : "";
                var type = ActionType == "Update" ? "status" : "document(s) @CLIENTNAME";
                return $"{ActionType.ToLower()}{suffix} the {type}{additionalMessage} for Year <b>{Year}</b>";
            }
        }
    }
}