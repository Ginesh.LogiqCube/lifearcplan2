﻿using LAP.DomainModels.Entities.Admin;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientLogin", Schema = "Client")]
    public partial class ClientLogin : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? Active { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? SetupCompletedDate { get; set; }
        public bool InitialProfileSetupCompleted { get; set; } = false;
        [StringLength(4)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string VerificationCode { get; set; }
        public bool TwoFactorAuth { get; set; }
        public bool HasChangesClientRiskArc { get; set; }
        public bool HasChangesSpouseRiskArc { get; set; }
        public bool HasChangesAol { get; set; }
        public bool HasChangesCanvas { get; set; }
        [NotMapped]
        public string EncryptedGuid { get; set; }

        #region Relationship
        public virtual UserLogin UserLogin { get; set; }
        public virtual ClientData ClientData { get; set; }
        public virtual SpouseData SpouseData { get; set; }
        public virtual XSpouseData XSpouseData { get; set; }
        public virtual ClientContactInfo ClientContactInfo { get; set; }
        public virtual InvestmentLiteData InvestmentLiteData { get; set; }
        public virtual CharitablePlanning CharitablePlanning { get; set; }
        public virtual HealthCarePlanning HealthCarePlanning { get; set; }
        public virtual List<Heir> Heirs { get; set; }
        public virtual List<ClientProperty> ClientProperties { get; set; }
        public virtual List<ClientBudget> ClientBudgets { get; set; }
        public virtual List<ClientOccupation> ClientOccupations { get; set; }
        public virtual List<ClientEsp> ClientEsps { get; set; }
        public virtual List<ClientPrevEsp> ClientPrevEsps { get; set; }
        public virtual List<BusinessInterest> BusinessInterests { get; set; }
        public virtual List<OtherIncome> OtherIncomes { get; set; }
        public virtual List<SocialSecurity> SocialSecurities { get; set; }
        public virtual List<BankCreditUnionAccount> BankCreditUnionAccounts { get; set; }
        public virtual List<BrokerageAdvisoryAccount> BrokerageAdvisoryAccounts { get; set; }
        public virtual List<ClientEstateItem> ClientEstate { get; set; }
        public virtual List<LifeAnnuityContract> LifeAnnuityContracts { get; set; }
        public virtual List<PersonalCare> PersonalCares { get; set; }
        public virtual List<EducationPlanning> EducationPlannings { get; set; }
        public virtual List<ClientRiskArc> ClientRiskArcs { get; set; }
        public virtual List<ClientHealthItem> ClientHealth { get; set; }
        public virtual List<RetirementStatus> RetirementStatuses { get; set; }
        public virtual List<ClientIncomeConcern> ClientIncomeConcerns { get; set; }
        public virtual List<ClientExpenseConcern> ClientExpenseConcern { get; set; }
        public virtual List<ClientDocument> ClientDocuments { get; set; }
        public virtual List<TaxDocument> TaxDocuments { get; set; }
        public virtual List<ClientExpectation> ClientExpectations { get; set; }
        public virtual List<TaxPrepStatus> TaxPrepStatuses { get; set; }
        public virtual List<ClientActivityLog> ClientActivityLogs { get; set; }
        public virtual List<TaxPrepHistory> TaxPrepHistories { get; set; }
        #endregion
    }
}