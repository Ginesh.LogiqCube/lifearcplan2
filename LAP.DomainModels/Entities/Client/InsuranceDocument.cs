﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "InsuranceDocument", Schema = "Client")]
    public class InsuranceDocument : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Client Name")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "File Url")]
        [RegularExpression(ValidationRegEx.Url, ErrorMessage = ValidationMessage.INVALID)]
        [NotMapped]
        public string FileUrl { get; set; }
        [NotMapped]
        public string Extension
        {
            get { return !string.IsNullOrEmpty(FileName) ? Path.GetExtension(FileName).Replace(".", "") : string.Empty; }
        }
        public string ContentType { get; set; }
        public long FileSize { get; set; }
        public string VersionId { get; set; }
        public DateTime UploadedDate { get; set; }
        [Display(Name = "Adviser Note")]
        [MaxLength(500)]
        public string AdviserNote { get; set; }
        [MaxLength(500)]
        [Display(Name = "Reviewer Note")]
        public string ReviewerNote { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public InsuranceDocumentStatusEnum Status { get; set; }
        [ForeignKey("UserLogin")]
        public int? ReviewedById { get; set; }
        public virtual UserLogin ReviewedBy { get; set; }
        public DateTime? ReviewedDate { get; set; }
        public bool ReqiuredChange { get; set; }
        [Display(Name = "Insurance Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public InsuranceTypeEnum InsuranceType { get; set; }
        [NotMapped]
        public string InsuranceTypeString { get { return InsuranceType.GetDisplayName(); } }
    }
}