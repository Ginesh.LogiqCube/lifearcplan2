﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "RetirementStatus", Schema = "Client")]
    public class RetirementStatus : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [DisplayName("Personal Activity Of")]
        public ClientTypeEnum employeeOf { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public string EmployerTitle
        {
            get; set;
        }
    }
}
