﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "BusinessInterest", Schema = "Client")]
    public class BusinessInterest : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "Business Name", Prompt = "Business Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(50)]
        public string BusinessName { get; set; }

        [Display(Name = "Entity Firm", Prompt = "Entity Firm")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public EntityFirmEnum EntityFirmEnum { get; set; }

        [Display(Name = "Firm Ownership", Prompt = "Firm Ownership")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string FirmOwnershipEnum { get; set; }

        [Display(Name = "Amount of Ownership(%)", Prompt = "Amount of Ownership(%)")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal AmountOfOwnership { get; set; }

        [Display(Name = "Fair Market Value of Business", Prompt = "Fair Market Value of Business")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Range(1, int.MaxValue, ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int FMVofBusiness { get; set; } //FMV-Fair Market Value

        [Display(Name = "Projected Value at Retirement", Prompt = "Projected Value at Retirement")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]

        public int PBValueatRetirement { get; set; } //Projected Business Value at Retirement

        public bool BSContractCurrently { get; set; } //Buy/Sell Contract Currently in Place?

        public bool FundedLifeIns { get; set; }

        [Display(Name = "Last Reviewed", Prompt = "Last Reviewed")]
        [Required(ErrorMessage = "Provide Last reviewed year")]
        public LastReviewedEnum LastReviewedEnum { get; set; }

        [Display(Name = "Plans for Closely Held Business?", Prompt = "Plans for closely held business?")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public CloselyHeldBusinessEnum closelyheldbusinessEnum { get; set; }

        [Display(Name = "Business Interest Of", Prompt = "Select Client or Spouse")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ClientTypeEnum businessInterestOf { get; set; }

        [NotMapped]
        public string entityFirmString { get { return EntityFirmEnum.GetDisplayName(); } }

        [NotMapped]
        public string FirmOwnerShipString { get; set; }

        [NotMapped]
        public string lastReviewedEnumString { get { return LastReviewedEnum.GetDisplayName(); } }

        [NotMapped]
        public string closelyheldbusinessEnumString { get { return closelyheldbusinessEnum.GetDisplayName(); } }

        [NotMapped]
        public string businessInterestOfEnumString { get; set; }



    }
}
