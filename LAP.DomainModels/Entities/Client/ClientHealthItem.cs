﻿using LAP.DomainModels.Entities.Master;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientHealthItem", Schema = "Client")]
    public class ClientHealthItem : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [ForeignKey("DescriptionItem")]
        public int DescriptionItemId { get; set; }
        public virtual DescriptionItem DescriptionItems { get; set; }
        public bool isClientActive { get; set; }
        public bool isSpouseActive { get; set; }
    }
}
