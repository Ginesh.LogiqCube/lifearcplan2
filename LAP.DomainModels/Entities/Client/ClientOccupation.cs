﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "ClientOccupation", Schema = "Client")]
    public class ClientOccupation : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        public string EmploymentTypes { get; set; }
        public int AgeToRetire { get; set; }
        public string Occupation { get; set; }
        public string Industry { get; set; }
        public string Employer { get; set; }
        public string Country { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int? AnnualEarnings { get; set; }
        public bool ClergyIsMember { get; set; }
        public bool ClergyIsExcludeFromRetirement { get; set; }
        public ClergyLivingArrangementEnum ClergyLivingIn { get; set; }
        public int ClergyAllowance { get; set; }
        public TenYearExpectedAnnualIncomeEnum TenYearExpectedAnnualIncome { get; set; }
        public EmploymentStatusEnum EmploymentStatus { get; set; }
        public ClientTypeEnum EmploymentOf { get; set; }
        public ClientEsp ClientEsp { get; set; }
        [NotMapped]
        public DateTime DOB { get; set; }
        [NotMapped]
        public string EmployerTitle { get; set; }
        [NotMapped]
        public int YearsToRetire { get; set; }
        [NotMapped]
        public int MonthToRetire { get; set; }
        [NotMapped]
        public string EmploymentTypesString
        {
            get
            {
                string result = string.Empty;
                if (!string.IsNullOrEmpty(EmploymentTypes))
                {
                    var split = EmploymentTypes.Split(",");
                    foreach (var item in split)
                    {
                        EmploymentTypeEnum employmentType = (EmploymentTypeEnum)Enum.Parse(typeof(EmploymentTypeEnum), item);
                        result += employmentType.GetDisplayName() + ", ";
                    }
                    result = result[..result.LastIndexOf(",")];
                }
                return result;
            }
        }
        [NotMapped]
        public string EmploymentStatusString { get { return EmploymentStatus.GetDisplayName(); } }
        [NotMapped]
        public string EmploymentOfString { get { return EmploymentOf.GetDisplayName(); } }
        [NotMapped]
        public string TenYearExpectedAnnualIncomeString { get { return TenYearExpectedAnnualIncome.GetDisplayName(); } }
    }
}