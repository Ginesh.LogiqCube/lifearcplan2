﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace LAP.DomainModels.Entities.Client
{
    [Table(name: "TaxDocument", Schema = "Client")]
    public class TaxDocument : BaseEntity
    {
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        public int Year { get; set; }
        public long FileSize { get; set; }
        public string VersionId { get; set; }
    }
}