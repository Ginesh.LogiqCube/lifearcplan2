﻿using LAP.DomainModels.Entities.Admin;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "AssistantPermission", Schema = "Adviser")]
    public class AssistantPermission : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserInfoId { get; set; }
        public virtual UserLogin UserInfo { get; set; }
        public AssistantPermissionEnum Permission { get; set; }
        public string PermissionString
        {
            get { return Permission.GetDisplayName(); }
        }
    }
}