﻿using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Admin;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Regulatory", Schema = "Adviser")]
    public class Regulatory : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        public int SectionId { get; set; }
        public bool Answer { get; set; }
        [NotMapped]
        public int ParentId { get; set; }
    }
}
