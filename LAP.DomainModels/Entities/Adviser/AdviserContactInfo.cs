﻿using LAP.DomainModels.Entities.Admin;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "AdviserContactInfo", Schema = "Adviser")]
    public partial class AdviserContactInfo : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        [Display(Name = "Street Address")]
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string POBox { get; set; }
        public string Phone { get; set; }
        public string CellNumber { get; set; }
        public AddressTypeEnum AddressType { get; set; }
    }
}