﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Event", Schema = "Adviser")]
    public partial class ActivityEvent : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        [MaxLength(30)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Title { get; set; }
        [Display(Name = "Estimated Cost")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int EstimatedCost { get; set; }
        [Display(Name = "Estimated # of Attendees")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int EstimatedAtendees { get; set; }

        [Display(Name = "Estimated # of Leads")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int EstimatedLeads { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public EventTypeEnum Type { get; set; }
        public string Tag { get; set; }
        [Display(Name = "Actual Cost")]
        public int? ActualCost { get; set; }
        [Display(Name = "Actual # Of Attendees")]
        public int? ActualNoOfAttendees { get; set; }
        [Display(Name = "# of Leads Generated")]
        public virtual List<Lead> Leads { get; set; }
    }
}