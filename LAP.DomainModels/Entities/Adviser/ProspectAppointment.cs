﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "ProspectAppointment", Schema = "Adviser")]
    public partial class ProspectAppointment : BaseEntity
    {
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Subject { get; set; }
        [Display(Name = "Start Date & Time")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime? StartDateTime { get; set; }
        [Display(Name = "End Date & Time")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime? EndDateTime { get; set; }

        [NotMapped]
        public DateTime Date { get; set; }
        [NotMapped]
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }
        [NotMapped]
        [Display(Name = "End Time")]
        public DateTime EndTime { get; set; }


        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ProspectAppointmentStageEnum Stage { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Location { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public AppointmentStatusEnum Status { get; set; }
        public bool FallOut { get; set; }
        [ForeignKey("Lead")]
        public int LeadId { get; set; }
        public virtual Lead Lead { get; set; }
    }
}