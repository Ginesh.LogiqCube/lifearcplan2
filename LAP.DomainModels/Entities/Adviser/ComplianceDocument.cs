﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "ComplianceDocument", Schema = "Adviser")]
    public class ComplianceDocument : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin User { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [Display(Name = "File Url")]
        [RegularExpression(ValidationRegEx.Url, ErrorMessage = ValidationMessage.INVALID)]
        [NotMapped]
        public string FileUrl { get; set; }
        [NotMapped]
        public string Extension
        {
            get { return !string.IsNullOrEmpty(FileName) ? Path.GetExtension(FileName).Replace(".", "") : string.Empty; }
        }
        public string ContentType { get; set; }
        public long FileSize { get; set; }
        public string VersionId { get; set; }
        public DateTime UploadedDate { get; set; }
        [Display(Name = "Adviser Note")]
        [MaxLength(500)]
        public string AdviserNote { get; set; }
        [Display(Name = "Compliance Note")]
        public string ComplianceNote { get; set; }
        public ComplianceDocumentStatusEnum Status { get; set; }
        [ForeignKey("UserLogin")]
        public int? ReviewedById { get; set; }
        public virtual UserLogin ReviewedBy { get; set; }
        public DateTime? ReviewedDate { get; set; }
        public string Allegation { get; set; }
        [Display(Name = "Termination Type")]
        [MaxLength(30)]
        public string TerminationType { get; set; }
        public bool ReqiuredChange { get; set; }
        public bool IsFromV1 { get; set; }
    }
}