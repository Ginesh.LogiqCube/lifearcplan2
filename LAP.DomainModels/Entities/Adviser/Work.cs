﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Work", Schema = "Adviser")]
    public partial class Work : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(75)]
        public string Company { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(60)]
        public string Position { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string State { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Prompt = "City")]
        public string City { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Duration", Prompt = "mm/yyyy")]
        public string FromYear { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "To Year", Prompt = "mm/yyyy")]
        public string ToYear { get; set; }
        public bool IsPresent { get; set; }
        [NotMapped]
        public string FromYearString
        {
            get
            {
                if (!string.IsNullOrEmpty(FromYear))
                {
                    var sp = FromYear.Split("/");
                    DateTime dt = new(Convert.ToInt32(sp[1]), Convert.ToInt32(sp[0]), 1);
                    return string.Format("{0:MMM yyyy}", dt);
                }
                return null;
            }
        }
        [NotMapped]
        public string ToYearString
        {
            get
            {
                if (!string.IsNullOrEmpty(ToYear))
                {
                    var sp = ToYear.Split("/");
                    DateTime dt = new(Convert.ToInt32(sp[1]), Convert.ToInt32(sp[0]), 1);
                    return string.Format("{0:MMM yyyy}", dt);
                }
                return "Present";
            }
        }
    }
}