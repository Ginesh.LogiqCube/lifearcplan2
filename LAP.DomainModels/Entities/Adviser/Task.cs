﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Task", Schema = "Adviser")]
    public partial class AcivityTask : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }

        [MaxLength(30)]
        public string Subject { get; set; }
        public PriorityEnum Priority { get; set; }
        public string Status { get; set; }
        //public string AssignTo { get; set; }
        [MaxLength(300)]
        public string Description { get; set; }
        //public string RelatedContact { get; set; }
        //public string Link { get; set; }
        public DateTime DueDate { get; set; }
        //public string Category { get; set; }
    }
}