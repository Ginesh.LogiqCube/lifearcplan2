﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "LeadHistory", Schema = "Adviser")]
    public partial class LeadHistory : BaseEntity
    {
        [Display(Name = "Start Date & Time")]
        public DateTime? StartDateTime { get; set; }
        [Display(Name = "End Date & Time")]
        public DateTime? EndDateTime { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ProspectAppointmentStageEnum Stage { get; set; }
        public bool FallOut { get; set; }
        public AppointmentStatusEnum Status { get; set; }
        [ForeignKey("Lead")]
        public int LeadId { get; set; }
        public virtual Lead Lead { get; set; }
        public bool Default { get; set; }
    }
}