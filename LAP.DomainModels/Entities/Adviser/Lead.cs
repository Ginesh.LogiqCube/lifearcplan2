﻿using LAP.DomainModels.Entities.Admin;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Lead", Schema = "Adviser")]
    public partial class Lead : BaseEntity
    {
        [ForeignKey("ClientAppointment")]
        public int? ClientAppointmentId { get; set; }
        public virtual ClientAppointment ClientAppointment { get; set; }
        [ForeignKey("ActivityEvent")]
        public int? EventId { get; set; }
        public virtual ActivityEvent ActivityEvent { get; set; }
        [MaxLength(30)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(30)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [MaxLength(30)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        [Display(Name = "Email address", Prompt = "Email address")]
        public string Email { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [NotMapped]
        public string Name
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
        }
        public bool Active { get; set; }
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        public virtual ProspectAppointment ProspectAppointment { get; set; }
        public virtual List<LeadHistory> LeadHistories { get; set; }
    }
}