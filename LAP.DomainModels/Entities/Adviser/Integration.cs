﻿using LAP.DomainModels.Entities.Admin;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Integration", Schema = "Adviser")]
    public partial class Integration : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        public int? RedtailUserId { get; set; }
        public Guid? RedtailUserKey { get; set; }
        public int? RedtailDatabaseId { get; set; }
        public string CFSUserId { get; set; }
        public string CFSUserApiKey { get; set; }
        public string OmniUserId { get; set; }
        public string OmniUsername { get; set; }
        public string OmniAccessKey { get; set; }
        public string FusionUserId { get; set; }
    }
}