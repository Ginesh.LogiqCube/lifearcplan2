﻿using LAP.DomainModels.Entities.Admin;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "ClientAppointment", Schema = "Adviser")]
    public partial class ClientAppointment : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        [MaxLength(30)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(30)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [MaxLength(30)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [StringLength(50)]
        public string Email { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [NotMapped]
        public string Name
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
        }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Subject { get; set; }
        [Display(Name = "Start Date & Time")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime? StartDateTime { get; set; }

        [NotMapped]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime Date { get; set; }
        [NotMapped]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime Time { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ClientAppointmentPurposeEnum Purpose { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Location { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public AppointmentStatusEnum Status { get; set; }
        public virtual List<Lead> Leads { get; set; }
    }
}