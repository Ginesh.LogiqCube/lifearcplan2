﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Adviser
{
    [Table(name: "Affiliation", Schema = "Adviser")]
    public partial class Affiliation : BaseEntity
    {
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Remote("CheckAffiliationExist", "Home", AdditionalFields = "Guid,Type")]
        [Display(Name = "Affiliation Name", Prompt = "Affiliation Name")]
        [MaxLength(150)]
        public string Name { get; set; }
        [Display(Name = "Affiliation Type", Prompt = "Affiliation Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public AffiliationTypeEnum Type { get; set; }
        [Display(Name = "Compliance Officer/Firm Principal", Prompt = "Compliance Officer/Firm Principal")]
        [MaxLength(150)]
        public string ComplianceOfficerName { get; set; }
        [Display(Name = "Street Address", Prompt = "Street Address")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(60)]
        public string Address { get; set; }
        [Display(Name = "Suite/Apt #", Prompt = "Suite/Apt #")]
        [MaxLength(60)]
        public string Address2 { get; set; }
        [Display(Name = "Zip Code", Prompt = "Zip Code")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string ZipCode { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "State", Prompt = "State")]
        public string State { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "City", Prompt = "City")]
        public string City { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Prompt = "(___) ___-____")]
        public string Phone { get; set; }
        [Display(Name = "Email Address", Prompt = "Email Address")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        public string Email { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Url, ErrorMessage = ValidationMessage.INVALID)]
        [Display(Name = "Website Url", Prompt = "www.xyz.com")]
        [MaxLength(150)]
        public string WebsiteUrl { get; set; }
        [NotMapped]
        public string TypeString { get { return Type.GetDisplayName(); } }
        [Display(Name = "Registered With")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public RegisteredEnum RegisteredWith { get; set; }
    }
}