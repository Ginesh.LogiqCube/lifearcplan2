﻿using LAP;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "RegulatoryDocument", Schema = "Admin")]
    public class RegulatoryDocument : BaseEntity
    {
        [ForeignKey("Firm")]
        public int FirmId { get; set; }
        public virtual Firm Firm { get; set; }
        [Display(Name = "File Name")]
        public string FileName { get; set; }
        [NotMapped]
        public string Extension
        {
            get { return !string.IsNullOrEmpty(FileName) ? Path.GetExtension(FileName).Replace(".", "") : string.Empty; }
        }
        public string ContentType { get; set; }
        public long FileSize { get; set; }
        public DateTime UploadedDate { get; set; }
    }
}
