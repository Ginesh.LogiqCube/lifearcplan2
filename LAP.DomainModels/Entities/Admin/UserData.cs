﻿using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "UserData", Schema = "Admin")]
    public class UserData : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserId { get; set; }
        public virtual UserLogin UserLogin { get; set; }
        [Display(Name = "First Name", Prompt = "First Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(30)]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name", Prompt = "Middle Name")]
        [MaxLength(30)]
        public string MiddleInitial { get; set; }
        [Display(Name = "Last Name", Prompt = "Last Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(30)]
        public string LastName { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public SalutationEnum Salutation { get; set; }
        [Display(Name = "Date Of Birth", Prompt = "mm/dd/yyyy")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime? DateOfBirth { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public AdviserTitleEnum RoleTitle { get; set; }
        [Display(Name = "Business Name", Prompt = "Business Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(50)]
        public string BusinessName { get; set; }
        public string AvatarFileName { get; set; }
        public string BusinessLogoFileName { get; set; }

        public string DegreeDesignation { get; set; }
        [NotMapped]
        public string DegreeDesignationWithData { get; set; }
        public string License { get; set; }
        [MaxLength(7)]
        public int? CRD { get; set; }
        [MaxLength(7)]
        public int? NPN { get; set; }

        [ForeignKey("Affiliation")]
        public int? FMOId { get; set; }
        public virtual Affiliation FMO { get; set; }

        [ForeignKey("Affiliation")]
        public int? BDId { get; set; }
        public virtual Affiliation BD { get; set; }

        [ForeignKey("Affiliation")]
        public int? RIAId { get; set; }
        public virtual Affiliation RIA { get; set; }
        public string BDDisclosure { get; set; }
        public string RIADisclosure { get; set; }
        public CompensationTypeEnum CompensationType { get; set; }
        [Display(Name = "Business Tagline")]
        public string Tagline { get; set; }
        [Display(Name = "Notes")]
        public string ClientBoardMessage { get; set; }
        public List<WorkingHourModel> WorkingHourConfig { get; set; }
        public List<DashboardConfigModel> DashboardConfig { get; set; }
        [NotMapped]
        public string Name
        {
            get { return $"{FirstName.Trim()} {LastName.Trim()}"; }
        }
        [NotMapped]
        public string AvatarFileBase64String { get; set; }
        [NotMapped]
        public IFormFile BusinessLogoFile { get; set; }
    }
}