﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "Firm", Schema = "Admin")]
    public class Firm : BaseEntity
    {
        public string FirmNo => $"F{Id.ToString().PadLeft(5, '0')}";
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Firm Name", Prompt = "Firm Name")]
        [MaxLength(40)]
        public string Name { get; set; }
        [Display(Name = "Notes")]
        public string UserBoardMessage { get; set; }
        public virtual List<UserLogin> UserLogins { get; set; }
        public virtual List<RegulatoryDocument> RegulatoryDocuments { get; set; }
    }
}
