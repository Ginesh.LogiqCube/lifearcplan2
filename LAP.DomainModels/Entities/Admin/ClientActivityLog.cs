﻿using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "ClientActivityLog", Schema = "Admin")]
    public class ClientActivityLog
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string EntityId { get; set; }
        public string EntityName { get; set; }
        public string ActionType { get; set; }
        public string EntityModuleName { get; set; }
        public string Title { get; set; }
        public string Role { get; set; }
        public DateTime TimeStamp { get; set; }
        public Dictionary<string, FieldChangeModel> Changes { get; set; }

        public int? CreatedByUserId { get; set; }
        [ForeignKey("CreatedByUserId")]
        public virtual UserLogin CreatedByUser { get; set; }

        [ForeignKey("OwnerId")]
        public virtual ClientLogin Owner { get; set; }
        public int OwnerId { get; set; }

        [ForeignKey("ParentId")]
        public virtual ClientActivityLog Parent { get; set; }
        public int? ParentId { get; set; }
        public virtual List<ClientActivityLog> Children { get; set; }

        [NotMapped]
        public List<PropertyEntry> TempProperties { get; set; }
    }
}
