﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "ReleaseNote", Schema = "Admin")]
    public class ReleaseNote : BaseEntity
    {
        [Display(Name = "Release Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(50)]
        public string ReleaseVersion { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Note { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Release Date")]
        public DateTime ReleaseDate { get; set; }
        [NotMapped]
        public string ReleaseDateFormated => ReleaseDate.ToString("MMM dd, yyyy");
    }
}