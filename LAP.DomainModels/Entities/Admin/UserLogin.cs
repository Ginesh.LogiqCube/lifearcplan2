﻿using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Entities.Client;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "UserLogin", Schema = "Admin")]
    public class UserLogin : BaseEntity
    {
        public string UserNo
        {
            get
            {
                string prefix = AdminRole.GetDisplayName() == Roles.SuperAdmin ? "SA"
                    : AdminRole.GetDisplayName() == Roles.FirmPrincipal ? "FP"
                    : AdminRole.GetDisplayName() == Roles.FirmAdmin ? "FA" : "AD";
                return $"{prefix}{Id.ToString().PadLeft(4, '0')}";
            }
        }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        [Display(Name = "Email address", Prompt = "Email address")]
        [Remote("CheckEmailExist", "Home", AdditionalFields = "Guid")]
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public bool IsAssistant { get; set; }
        [ForeignKey("OwnerUser")]
        public int? OwnerUserId { get; set; }
        public virtual UserLogin OwnerUser { get; set; }

        public AdminRoleEnum AdminRole { get; set; }
        public string AdminRoleString
        {
            get { return AdminRole.GetDisplayName(); }
        }
        public DateTime? LastLogin { get; set; }
        public DateTime? SetupCompletedDate { get; set; }
        [ForeignKey("Firm")]
        [Display(Name = "Firm")]
        public int? FirmId { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Verification Code", Prompt = "XXXX")]
        [StringLength(4)]
        public string VerificationCode { get; set; }
        public bool TwoFactorAuth { get; set; }
        public bool HasChanges { get; set; }
        public Firm Firm { get; set; }
        [NotMapped]
        public string EncryptedGuid { get; set; }
        public virtual List<Permission> Permissions { get; set; }
        [NotMapped]
        [Display(Name = "Additional Roles")]
        public virtual List<int> Permission { get; set; }
        public virtual List<AssistantPermission> AssistantPermissions { get; set; }
        [NotMapped]
        [Display(Name = "Permissions")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public virtual List<int> AssistantPermission { get; set; }
        public virtual List<ClientLogin> ClientLogins { get; set; }
        public virtual UserData UserData { get; set; }
        public virtual List<AdviserContactInfo> AdviserContactInfos { get; set; }
        public virtual List<PostHighSchoolEducation> PostHighSchoolEducations { get; set; }
        public virtual List<Work> Works { get; set; }
        public virtual List<Regulatory> Regulatories { get; set; }
        public virtual Integration Integration { get; set; }
    }
}