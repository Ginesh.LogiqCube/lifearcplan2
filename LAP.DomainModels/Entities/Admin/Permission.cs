﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Entities.Admin
{
    [Table(name: "Permission", Schema = "Admin")]
    public class Permission : BaseEntity
    {
        [ForeignKey("UserLogin")]
        public int UserInfoId { get; set; }
        public virtual UserLogin UserInfo { get; set; }
        public AdminRoleEnum RoleType { get; set; }
        public string RoleTypeString
        {
            get { return RoleType.GetDisplayName(); }
        }
    }
}