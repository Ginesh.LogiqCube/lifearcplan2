﻿using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Entities.Master;
using LAP.DomainModels.Models;
using Microsoft.EntityFrameworkCore;

namespace LAP.DomainModels
{
    public static class SeedExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            #region Admin Login
            modelBuilder.Entity<UserLogin>().HasData(new UserLogin
            {
                Id = 1,
                Username = "admin@lifearcplan.info",
                Password = "x/SbbPhUa4jbPzs9nRz7i37dQlp6PyfyqabHTQl739U=", //Admin@2021#
                Active = true,
                AdminRole = AdminRoleEnum.SuperAdmin,
                VerificationCode = string.Empty
            });
            modelBuilder.Entity<UserData>().HasData(new UserData
            {
                Id = 1,
                FirstName = "Super Admin",
                LastName = string.Empty,
                UserId = 1
            });
            #endregion
            #region WorkExpectations 
            modelBuilder.Entity<DescriptionItem>().HasData(
            new DescriptionItem
            {
                Id = 1,
                itemDesc = "Work by necessity",
                sortOrder = 1,
                tabletype = DescritpionTableNameEnum.WorkExpectations
            },
            new DescriptionItem
            {
                Id = 2,
                itemDesc = "Work by choice",
                sortOrder = 2,
                tabletype = DescritpionTableNameEnum.WorkExpectations
            },
            new DescriptionItem
            {
                Id = 3,
                itemDesc = "Start a business",
                sortOrder = 3,
                tabletype = DescritpionTableNameEnum.WorkExpectations
            },
            new DescriptionItem
            {
                Id = 4,
                itemDesc = "Stay at home to care for loved ones",
                sortOrder = 4,
                tabletype = DescritpionTableNameEnum.WorkExpectations
            },
            new DescriptionItem
            {
                Id = 5,
                itemDesc = "Do not work",
                sortOrder = 5,
                tabletype = DescritpionTableNameEnum.WorkExpectations
            },
            #endregion
            #region WorkExpectations
              new DescriptionItem
              {
                  Id = 6,
                  itemDesc = "Rent",
                  sortOrder = 1,
                  tabletype = DescritpionTableNameEnum.WorkExpectations
              },
             new DescriptionItem
             {
                 Id = 7,
                 itemDesc = "Stay in current home",
                 sortOrder = 2,
                 tabletype = DescritpionTableNameEnum.WorkExpectations
             },
              new DescriptionItem
              {
                  Id = 8,
                  itemDesc = "Build an addition on/renovate current home",
                  sortOrder = 3,
                  tabletype = DescritpionTableNameEnum.WorkExpectations
              },
              new DescriptionItem
              {
                  Id = 9,
                  itemDesc = "Move to a larger home",
                  sortOrder = 4,
                  tabletype = DescritpionTableNameEnum.WorkExpectations
              },
               new DescriptionItem
               {
                   Id = 10,
                   itemDesc = "Downsize to a smaller home/condo/apartment",
                   sortOrder = 5,
                   tabletype = DescritpionTableNameEnum.WorkExpectations
               },
               new DescriptionItem
               {
                   Id = 11,

                   itemDesc = "Purchase a 2nd residence/vacation home",
                   sortOrder = 6,
                   tabletype = DescritpionTableNameEnum.WorkExpectations
               },
               new DescriptionItem
               {
                   Id = 12,
                   itemDesc = "Relocate: city/state",
                   sortOrder = 7,
                   tabletype = DescritpionTableNameEnum.WorkExpectations
               },
            #endregion
            #region LifeStyleExpectations
                new DescriptionItem
                {
                    Id = 13,
                    itemDesc = "Active lifestyle",
                    sortOrder = 1,
                    tabletype = DescritpionTableNameEnum.LifeStyleExpectations
                },
              new DescriptionItem
              {
                  Id = 14,
                  itemDesc = "Quiet lifestyle",
                  sortOrder = 2,
                  tabletype = DescritpionTableNameEnum.LifeStyleExpectations
              },
              new DescriptionItem
              {
                  Id = 15,
                  itemDesc = "Travel to visit family",
                  sortOrder = 3,
                  tabletype = DescritpionTableNameEnum.LifeStyleExpectations
              },
              new DescriptionItem
              {
                  Id = 16,
                  itemDesc = "Frequent leisure travel",
                  sortOrder = 4,
                  tabletype = DescritpionTableNameEnum.LifeStyleExpectations
              },
              new DescriptionItem
              {
                  Id = 17,
                  itemDesc = "Occasional leisure travel",
                  sortOrder = 5,
                  tabletype = DescritpionTableNameEnum.LifeStyleExpectations
              },
              new DescriptionItem
              {
                  Id = 18,
                  itemDesc = "Rare leisure travel",
                  sortOrder = 6,
                  tabletype = DescritpionTableNameEnum.LifeStyleExpectations
              },
            #endregion
            #region CommunityExpectationsItems
                new DescriptionItem
                {
                    Id = 19,
                    itemDesc = "None",
                    sortOrder = 1,
                    tabletype = DescritpionTableNameEnum.CommunityExpectations
                },
               new DescriptionItem
               {
                   Id = 20,
                   itemDesc = "Volunteer regularly",
                   sortOrder = 2,
                   tabletype = DescritpionTableNameEnum.CommunityExpectations
               },
               new DescriptionItem
               {
                   Id = 21,
                   itemDesc = "Volunteer occasionally",
                   sortOrder = 3,
                   tabletype = DescritpionTableNameEnum.CommunityExpectations
               },
               new DescriptionItem
               {
                   Id = 22,
                   itemDesc = "Charitable donations",
                   sortOrder = 4,
                   tabletype = DescritpionTableNameEnum.CommunityExpectations
               },
              new DescriptionItem
              {
                  Id = 23,
                  itemDesc = "Mission trip(s)",
                  sortOrder = 5,
                  tabletype = DescritpionTableNameEnum.CommunityExpectations
              },
            #endregion
            #region DependentCareItems
               new DescriptionItem
               {
                   Id = 24,
                   itemDesc = "Provide support for parent(s)",
                   sortOrder = 1,
                   tabletype = DescritpionTableNameEnum.DependentCare
               },
              new DescriptionItem
              {
                  Id = 25,
                  itemDesc = "Provide support for child(ren)",
                  sortOrder = 2,
                  tabletype = DescritpionTableNameEnum.DependentCare
              },
              new DescriptionItem
              {
                  Id = 26,
                  itemDesc = "Provide support for grandchild(ren)",
                  sortOrder = 3,
                  tabletype = DescritpionTableNameEnum.DependentCare
              },
              new DescriptionItem
              {
                  Id = 27,
                  itemDesc = "Adoption/Foster care",
                  sortOrder = 4,
                  tabletype = DescritpionTableNameEnum.DependentCare
              },
            #endregion
            #region IncomeConcerns
                new DescriptionItem
                {
                    Id = 28,
                    itemDesc = "Having sufficient income to meet budgetary needs",
                    sortOrder = 1,
                    tabletype = DescritpionTableNameEnum.IncomeConcerns
                },
               new DescriptionItem
               {
                   Id = 29,
                   itemDesc = "Disability resulting in a loss of income",
                   sortOrder = 2,
                   tabletype = DescritpionTableNameEnum.IncomeConcerns
               },
               new DescriptionItem
               {
                   Id = 30,
                   itemDesc = "Not having a paycheck",
                   sortOrder = 3,
                   tabletype = DescritpionTableNameEnum.IncomeConcerns
               },
               new DescriptionItem
               {
                   Id = 31,
                   itemDesc = "Being dependant on Social Security retirement income",
                   sortOrder = 4,
                   tabletype = DescritpionTableNameEnum.IncomeConcerns
               },
               new DescriptionItem
               {
                   Id = 32,
                   itemDesc = "Adverse market correction resulting in loss of investment assests",
                   sortOrder = 5,
                   tabletype = DescritpionTableNameEnum.IncomeConcerns
               },
               new DescriptionItem
               {
                   Id = 33,
                   itemDesc = "Running out of money/outliving your savings",
                   sortOrder = 6,
                   tabletype = DescritpionTableNameEnum.IncomeConcerns
               },
               new DescriptionItem
               {
                   Id = 34,
                   itemDesc = "Spouse dying early",
                   sortOrder = 7,
                   tabletype = DescritpionTableNameEnum.IncomeConcerns
               },
           new DescriptionItem
           {
               Id = 35,
               itemDesc = "Loss of pension due to death of spouse",
               sortOrder = 8,
               tabletype = DescritpionTableNameEnum.IncomeConcerns
           },
           new DescriptionItem
           {
               Id = 36,
               itemDesc = "Loss of Social Security income due to death of spouse",
               sortOrder = 9,
               tabletype = DescritpionTableNameEnum.IncomeConcerns
           },
            #endregion
            #region ExpenseConcerns
             new DescriptionItem
             {
                 Id = 37,
                 itemDesc = "Paying higher taxation on income and/or investments",
                 sortOrder = 1,
                 tabletype = DescritpionTableNameEnum.ExpenseConcerns
             },
            new DescriptionItem
            {
                Id = 38,
                itemDesc = "Inflation: Increasing cost of goods and services",
                sortOrder = 2,
                tabletype = DescritpionTableNameEnum.ExpenseConcerns
            },
             new DescriptionItem
             {
                 Id = 39,
                 itemDesc = "Renovating residence for handicap accessibility",
                 sortOrder = 9,
                 tabletype = DescritpionTableNameEnum.ExpenseConcerns
             },
             new DescriptionItem
             {
                 Id = 40,
                 itemDesc = "Cost of future health care",
                 sortOrder = 3,
                 tabletype = DescritpionTableNameEnum.ExpenseConcerns
             },
              new DescriptionItem
              {
                  Id = 41,
                  itemDesc = "Cost of long term care",
                  sortOrder = 4,
                  tabletype = DescritpionTableNameEnum.ExpenseConcerns
              },
              new DescriptionItem
              {
                  Id = 42,
                  itemDesc = "Parents needing financial assistance",
                  sortOrder = 5,
                  tabletype = DescritpionTableNameEnum.ExpenseConcerns
              },
              new DescriptionItem
              {
                  Id = 43,
                  itemDesc = "Children needing financial assistance",
                  sortOrder = 6,
                  tabletype = DescritpionTableNameEnum.ExpenseConcerns
              },
              new DescriptionItem
              {
                  Id = 44,
                  itemDesc = "Grandchildren needing financial assistance",
                  sortOrder = 7,
                  tabletype = DescritpionTableNameEnum.ExpenseConcerns
              },
              new DescriptionItem
              {
                  Id = 45,
                  itemDesc = "Maintaining residence (HVAC, roof, windows, applicances, flooring, paint etc.)",
                  sortOrder = 8,
                  tabletype = DescritpionTableNameEnum.ExpenseConcerns
              },
              new DescriptionItem
              {
                  Id = 46,
                  itemDesc = "Purchase of vehicles",
                  sortOrder = 10,
                  tabletype = DescritpionTableNameEnum.ExpenseConcerns
              },
            #endregion
            #region Health
                new DescriptionItem
                {
                    Id = 47,
                    itemDesc = "Heart or cardiovascular problems?",
                    sortOrder = 1,
                    tabletype = DescritpionTableNameEnum.Health
                },
               new DescriptionItem
               {
                   Id = 48,
                   itemDesc = "Lung or respiratory problems?",
                   sortOrder = 2,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 49,
                   itemDesc = "High blood pressure?",
                   sortOrder = 3,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 50,
                   itemDesc = "Abnormal cholesterol levels?",
                   sortOrder = 4,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 51,
                   itemDesc = "Stroke/aneurysm?",
                   sortOrder = 5,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 52,
                   itemDesc = "Auto-immune disorder?",
                   sortOrder = 6,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 53,
                   itemDesc = "Diabetes or irregular blood sugar?",
                   sortOrder = 7,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 54,
                   itemDesc = "Digestive system problems?",
                   sortOrder = 8,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 55,
                   itemDesc = "Cancer/tumors formed from abnormal cell growth?",
                   sortOrder = 9,
                   tabletype = DescritpionTableNameEnum.Health
               },
               new DescriptionItem
               {
                   Id = 56,
                   itemDesc = "Overweight by more than 30 lbs?",
                   sortOrder = 10,
                   tabletype = DescritpionTableNameEnum.Health
               },
            #endregion
            #region Estate
                new DescriptionItem
                {
                    Id = 57,
                    itemDesc = "Do you have a Will?",
                    sortOrder = 1,
                    tabletype = DescritpionTableNameEnum.Estate
                },
               new DescriptionItem
               {
                   Id = 58,
                   itemDesc = "Do you have a Revocable Trust?",
                   sortOrder = 2,
                   tabletype = DescritpionTableNameEnum.Estate
               },
               new DescriptionItem
               {
                   Id = 59,
                   itemDesc = "Do you have an Irrevocable Trust?",
                   sortOrder = 3,
                   tabletype = DescritpionTableNameEnum.Estate
               },
               new DescriptionItem
               {
                   Id = 60,
                   itemDesc = "Have there been marriages, divorces, births, deaths, or adoptions in your family since your Will or Trusts were last updated?",
                   sortOrder = 4,
                   tabletype = DescritpionTableNameEnum.Estate
               },
                new DescriptionItem
                {
                    Id = 61,
                    itemDesc = "Do you have a Durable Power of Attorney for financial?",
                    sortOrder = 5,
                    tabletype = DescritpionTableNameEnum.Estate
                },
                 new DescriptionItem
                 {
                     Id = 62,
                     itemDesc = "Do you have a Durable Power of Attorney for medical?",
                     sortOrder = 6,
                     tabletype = DescritpionTableNameEnum.Estate
                 });
            #endregion

            #region parent
            modelBuilder.Entity<BudgetItem>().HasData(

                 new BudgetItem
                 {
                     Id = 61,
                     ItemName = "Residence",
                     SortOrder = 2,
                     IsRevolvingDebt = false
                 },
                new BudgetItem
                {
                    Id = 62,
                    ItemName = "Transportation",
                    SortOrder = 8,
                    IsRevolvingDebt = false
                },
              new BudgetItem
              {
                  Id = 63,
                  ItemName = "Utilities",
                  SortOrder = 13,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 64,
                  ItemName = "Daily Living",
                  SortOrder = 21,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 65,
                  ItemName = "Personal Care",
                  SortOrder = 35,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 66,
                  ItemName = "Insurance",
                  SortOrder = 38,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 67,
                  ItemName = "Family",
                  SortOrder = 43,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 68,
                  ItemName = "Other",
                  SortOrder = 52,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 69,
                  ItemName = "Revolving Debt/Savings",
                  SortOrder = 31,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 70,
                  ItemName = "Benevolence",
                  SortOrder = 1,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 71,
                  ItemName = "Taxes",
                  SortOrder = 48,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 72,
                  ItemName = "Education",
                  SortOrder = 50,
                  IsRevolvingDebt = false
              },
              new BudgetItem
              {
                  Id = 73,
                  ItemName = "Personal Savings",
                  SortOrder = 26,
                  IsRevolvingDebt = false
              });

            #endregion
            #region child            
            modelBuilder.Entity<BudgetItem>().HasData(
            new BudgetItem
            {
                Id = 1,
                ItemName = "Mortgage/Rent",
                SortOrder = 2,
                ParentId = 61,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 2,
                ItemName = "HOA Fee/Condo Fee",
                SortOrder = 3,
                ParentId = 61,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 3,
                ItemName = "Homeowners/Renters Insurance",
                SortOrder = 4,
                ParentId = 61,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 4,
                ItemName = "Home Repairs/Improvements",
                SortOrder = 5,
                ParentId = 61,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 5,
                ItemName = "Lawncare/Gardening",
                SortOrder = 6,
                ParentId = 61,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 6,
                ItemName = "Housecleaning services",
                SortOrder = 7,
                ParentId = 61,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 7,
                ItemName = "Automobile Payment",
                SortOrder = 8,
                ParentId = 62,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 8,
                ItemName = "Automobile Insurance",
                SortOrder = 9,
                ParentId = 62,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 9,
                ItemName = "Automobile Maintenance",
                SortOrder = 10,
                ParentId = 62,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 10,
                ItemName = "Public Transportation",
                SortOrder = 11,
                ParentId = 62,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 11,
                ItemName = "Water/Sewer",
                SortOrder = 13,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 12,
                ItemName = "Garbage Service",
                SortOrder = 14,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 13,
                ItemName = "Electricity",
                SortOrder = 15,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 14,
                ItemName = "Gas/Fuel/Coal",
                SortOrder = 16,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 15,
                ItemName = "Home Telephone",
                SortOrder = 17,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 16,
                ItemName = "Cellular Phone",
                SortOrder = 18,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 17,
                ItemName = "Home Security",
                SortOrder = 19,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 18,
                ItemName = "Cable/Satellite/Internet",
                SortOrder = 20,
                ParentId = 63,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 19,
                ItemName = "Groceries/Household goods",
                SortOrder = 21,
                ParentId = 64,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 20,
                ItemName = "Childcare",
                SortOrder = 22,
                ParentId = 64,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 21,
                ItemName = "Dry Cleaning",
                SortOrder = 23,
                ParentId = 64,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 22,
                ItemName = "Dining Out",
                SortOrder = 24,
                ParentId = 64,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 23,
                ItemName = "Entertainment/Recreation",
                SortOrder = 25,
                ParentId = 64,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 24,

                ItemName = "Hair Care",
                SortOrder = 35,
                ParentId = 65,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 25,
                ItemName = "Massages/Chiropractic",
                SortOrder = 36,
                ParentId = 65,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 26,
                ItemName = "Clothing/Apparel",
                SortOrder = 37,
                ParentId = 65,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 27,
                ItemName = "Health: Medical/Dental,Supplemental",
                SortOrder = 38,
                ParentId = 66,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 28,
                ItemName = "Life",
                SortOrder = 39,
                ParentId = 66,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 29,
                ItemName = "Vacation/Trips",
                SortOrder = 43,
                ParentId = 67,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 30,
                ItemName = "Holiday/Birthday gifts",
                SortOrder = 44,
                ParentId = 67,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 31,
                ItemName = "Child Support",
                SortOrder = 45,
                ParentId = 67,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 32,
                ItemName = "Alimony",
                SortOrder = 46,
                ParentId = 67,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 33,
                ItemName = "Pets",
                SortOrder = 47,
                ParentId = 67,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 34,
                ItemName = "Other",
                SortOrder = 52,
                ParentId = 68,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 35,
                ItemName = "Fuel cost",
                SortOrder = 12,
                ParentId = 62,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 36,

                ItemName = "Credit Card(s)",
                SortOrder = 31,
                ParentId = 69,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 37,
                ItemName = "Home Equity Line of Credit",
                SortOrder = 32,
                ParentId = 69,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 38,
                ItemName = "Savings Acct",
                SortOrder = 27,
                ParentId = 73,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 39,
                ItemName = "Disability ",
                SortOrder = 40,
                ParentId = 66,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 40,
                ItemName = "Long Term Care",
                SortOrder = 41,
                ParentId = 66,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 41,
                ItemName = "Liability / Umbrella Coverage",
                SortOrder = 42,
                ParentId = 66,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 42,
                ItemName = "Installment Note",
                SortOrder = 34,
                ParentId = 69,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 43,
                ItemName = "Tithing/Charity/Giving",
                SortOrder = 1,
                ParentId = 70,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 44,
                ItemName = "Income Taxes",
                SortOrder = 48,
                ParentId = 71,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 45,
                ItemName = "Property Taxes",
                SortOrder = 49,
                ParentId = 71,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 46,
                ItemName = "Education Savings",
                SortOrder = 50,
                ParentId = 72,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 47,
                ItemName = "Education Loans",
                SortOrder = 51,
                ParentId = 72,
                IsRevolvingDebt = true
            },
            new BudgetItem
            {
                Id = 48,
                ItemName = "Retirement Plan Contributions",
                SortOrder = 26,
                ParentId = 73,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 49,
                ItemName = "Investment Contributions",
                SortOrder = 28,
                ParentId = 73,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 50,
                ItemName = "Annuity Contributions",
                SortOrder = 29,
                ParentId = 73,
                IsRevolvingDebt = false
            },
            new BudgetItem
            {
                Id = 51,
                ItemName = "Life Insurance Contributions",
                SortOrder = 30,
                ParentId = 73,
                IsRevolvingDebt = false
            });
            #endregion
            #region Planning
            modelBuilder.Entity<PlanningItem>().HasData(

             new PlanningItem
             {
                 Id = 1,
                 ItemName = "Investment Planning",
                 SortOrder = 2
             },
             new PlanningItem
             {
                 Id = 2,
                 ItemName = "Social Security Planning",
                 SortOrder = 1
             },
             new PlanningItem
             {
                 Id = 3,
                 ItemName = "Retirement Income Planning",
                 SortOrder = 3
             },
             new PlanningItem
             {
                 Id = 5,
                 ItemName = "Healthcare Planning",
                 SortOrder = 4
             },
             new PlanningItem
             {
                 Id = 6,
                 ItemName = "Personal Care Planning",
                 SortOrder = 5
             },
             new PlanningItem
             {
                 Id = 7,
                 ItemName = "Education Planning",
                 SortOrder = 6
             },
             new PlanningItem
             {
                 Id = 8,
                 ItemName = "Charitable Planning",
                 SortOrder = 7
             },
             new PlanningItem
             {
                 Id = 9,
                 ItemName = "Estate Planning",
                 SortOrder = 8
             });
            #endregion
            #region RiskArc
            modelBuilder.Entity<RiskArcItem>().HasData(

                new RiskArcItem
                {
                    Id = 1,
                    Question = "How would you define yourself as an investor?",
                    Options = new string[5] { "Conservative", "Moderate Conservative", "Moderate", "Moderate Aggressive", "Aggressive" }
                },
                new RiskArcItem
                {
                    Id = 8,
                    Question = "Other than your spouse, for how many years will you be financially providing for dependent?",
                    Options = new string[5] { "15 years or more", "11 to 15 years", "5 to 10 years", "1 to 4 years", "0 years/Question Not Applicable" }
                },
                new RiskArcItem
                {
                    Id = 9,
                    Question = "When do you expect to begin withdrawing money from your investment account on a regular basis?",
                    Options = new string[5] { "1 year or less", "2 to 5 years", "6 to 10 years", "11 to 20 years", "20 years or more" }
                },
                new RiskArcItem
                {
                    Id = 10,
                    Question = "How many years do you expect to be dependent on your investment account for income?",
                    Options = new string[5] { "More than 25 years", "15 years to 25 years", "5 years to 14 years", "1 year to 4 years", "0 years" }
                },
                new RiskArcItem
                {
                    Id = 12,
                    Question = "Knowing that higher returns typically require higher risk exposure, over the next 10 years, I want my investment account to average the following annual rate of return:",
                    Options = new string[5] { "1% to 3%", "4% to 5%", "6% to 7%", "8% to 9%", "10% or more" }
                },
                new RiskArcItem
                {
                    Id = 13,
                    Question = "Which statement best describes your attitude toward a 10% loss in your investment account?",
                    Options = new string[5] { "I would be concerned with the loss lasting 1 to 3 months.", "I would be concerned with the loss lasting 4 to 6 months.", "I would be concerned with the loss lasting 7 to 9 months.", "I would be concerned with the loss lasting 10 months to 1 year.", "I would be concerned with the loss lasting longer than 1 year." }
                },
                new RiskArcItem
                {
                    Id = 14,
                    Question = "Which statement best describes the action you would take if you experienced a 10% loss in your investment account?",
                    Options = new string[5] { "I would abandon the current investment strategy.", "I would make significant changes to the current investment strategy.", "I would make minor changes to the current investment strategy.", "I would not make any changes to the current investment strategy.", "I would take advantage of the downturn and invest more into the current strategy." }
                },
                new RiskArcItem
                {
                    Id = 15,
                    Question = "The portfolio we recommend will fluctuate in value. Hypothetically, after one year, if the value of your $100,000 investment account fell somewhere between the Lower boundary and the Upper boundary graph, indicate which portfolio represents the maximum risk/return trade-off you would be willing to accept.",
                    Options = new string[5] { "Portfolio 1", "Portfolio 2", "Portfolio 3", "Portfolio 4", "Portfolio 5" }
                },
                new RiskArcItem
                {
                    Id = 16,
                    Question = "When thinking about selling an investment, the price I paid is a big factor I consider before taking any action.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 17,
                    Question = "The pain of financial loss is at least two times stronger than the pleasure of financial gain.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 18,
                    Question = "I will buy things I want even if they are not the best financial choices.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 19,
                    Question = "Poor financial decisions have caused me to change my current investing decisions.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 20,
                    Question = "I sometimes get attached to certain investments I've made, which may prevent me from making a change to them.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 21,
                    Question = "I often take action on a new investment right away, if it makes sense to me.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 22,
                    Question = "I often find that many of my successful investments can be attributed to my decisions, while those that did not work out were based on the guidance of others.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 23,
                    Question = "When considering changing my portfolio, I spend time thinking about options but often end up changing little or sometimes nothing.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 24,
                    Question = "I am confident that my investment knowledge is above average.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 25,
                    Question = " I trust more advice from nationally advertised firms than from smaller, local firms on my investment choices.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 26,
                    Question = "I don’t easily change my views about investments once they are made.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 27,
                    Question = "I invest in companies that make products I like or companies that reflect my personal values.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 28,
                    Question = "I tend to categorize my investments into various accounts, such as leisure, bill paying, college funding, and so on.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 29,
                    Question = "When reflecting on past investment mistakes, I see that many could have been easily avoided.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 30,
                    Question = "Many investment choices I make are based on my knowledge of how similar past investments have performed.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 31,
                    Question = " What’s most important is that my investments make money; I’m not very concerned with following a structured plan.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 32,
                    Question = "When making investment decisions, I tend to focus on the positive aspects of an investment rather than on what might go wrong with the investment. ",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 33,
                    Question = "I am more likely to have a better outcome if I make my own investment choices rather than relying on others.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 34,
                    Question = " When an investment is not going well, I usually seek information that confirms I made the right decision about it.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                },
                new RiskArcItem
                {
                    Id = 35,
                    Question = "When considering the track record of an investment, I put more weight on how it has performed recently rather than on how it has performed historically.",
                    Options = new string[5] { "Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree" }
                });
            #endregion           
        }
    }
}
