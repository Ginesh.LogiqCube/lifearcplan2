﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace LAP.DomainModels.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        [Display(Name = "Email address", Prompt = "Email")]
        public string Username { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Prompt = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = ValidationMessage.REQUIRED_TERM)]
        public bool TermAccepted { get; set; }
        public int TimeZone { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string VerificationCode { get; set; }
    }
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        [Display(Name = "Email address", Prompt = "Email")]
        public string Email { get; set; }
    }
    public class ResetPasswordModel
    {
        [Display(Prompt = "Email")]
        public string Email { get; set; }
        [Display(Prompt = "Password")]
        [DataType(DataType.Password)]
        [RegularExpression(ValidationRegEx.Password, ErrorMessage = ValidationMessage.INVALID)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = ValidationMessage.CONFIRM_PASSWORD_MISMATCH)]
        [Display(Name = "Re-type password", Prompt = "Re-type password to confirm")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        [HiddenInput]
        public string Token { get; set; }
        public int TimeZone { get; set; }
    }
    public class ClientUserDataModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ClientId { get; set; }
        public string AdviserServiceOption { get; set; }
        public bool RememberMe { get; set; }
    }
    public class IntegrationAuthModel
    {
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Prompt = "Username")]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]
        public string Username { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Password/Access Key", Prompt = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}