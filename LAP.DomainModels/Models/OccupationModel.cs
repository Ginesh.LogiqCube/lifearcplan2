﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LAP.DomainModels.Models
{
    public class OccupationModel
    {
        [HiddenInput]
        public Guid Guid { get; set; }
        public int ClientId { get; set; }
        [Display(Name = "Employment Type", Prompt = "Employment Type")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public List<int> EmploymentTypes { get; set; }
        [Display(Name = "Retirement Age", Prompt = "Retirement Age")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Range(0, 100, ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Remote("ValidateRetireAge", "Income", AdditionalFields = "EmploymentStatus,DOB")]
        public int? AgeToRetire { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(50)]
        public string Occupation { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(30)]
        public string Industry { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(50)]
        public string Employer { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Street Address", Prompt = "Street Address")]
        [MaxLength(50)]
        public string Address1 { get; set; }
        [Display(Name = "Suite/Appt #", Prompt = "Suite/Appt #")]
        [MaxLength(50)]
        public string Address2 { get; set; }
        public string Country { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string City { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string State { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Zip { get; set; }
        [Display(Name = "Annual Earnings", Prompt = "Annual Earnings")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Range(1, int.MaxValue, ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int AnnualEarnings { get; set; }
        [Display(Name = "Over the next 10 years, my income will", Prompt = "Over the next 10 years,I expect my annual income to")]
        [Required(ErrorMessage = "Provide Income Status")]
        public TenYearExpectedAnnualIncomeEnum TenYearExpectedAnnualIncome { get; set; }
        [Display(Name = "Status", Prompt = "Status")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public EmploymentStatusEnum EmploymentStatus { get; set; }
        [Display(Name = "Employment Of")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public ClientTypeEnum EmploymentOf { get; set; }
        [Display(Name = "Years To Retirement")]
        public int? YearsToRetire { get; set; }
        [HiddenInput]
        public DateTime DOB { get; set; }
        public bool ClergyIsMember { get; set; }
        public bool ClergyIsExcludeFromRetirement { get; set; }
        public ClergyLivingArrangementEnum ClergyLivingIn { get; set; }
        public int ClergyAllowance { get; set; }
    }
    public class EspModel
    {
        public Guid Guid { get; set; }
        [Display(Name = "Annual Employee Contribution")]
        public int? EspContribution { get; set; }
        [Display(Name = "Annual Employee Contribution")]
        public decimal? EspContributionPercentage { get; set; }
        [Display(Name = "Annual Employer Match")]
        public int? EspEmployerMatch { get; set; }
        [Display(Name = "Annual Employer Match")]
        public decimal? EspEmployerMatchPercentage { get; set; }
        [Display(Name = "Current Balance")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int EspCurrentBalance { get; set; }
        [Display(Name = "Average Rate Of Return")]
        public decimal? EspAvgRateReturn { get; set; }
        [Display(Name = "Pension Expected Age")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Remote("ValidatePensionExpectedAge", "Income", AdditionalFields = "DOB")]
        public int PensionExpectedAge { get; set; }
        [Display(Name = "Pension Benefit")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int PensionBenefit { get; set; }
        [Display(Name = "Pension Survivor Benefit")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public decimal PensionSurvivorBenefit { get; set; }
        public bool HasPensionPlan { get; set; }
        [Display(Name = "Employer Offer")]
        public List<int> EmployerOffer { get; set; }
        [HiddenInput]
        public DateTime DOB { get; set; }
    }
    public class EmploymentInfoModel
    {
        public OccupationModel Occupation { get; set; }
        public EspModel Esp { get; set; }
    }

    public class BusinessInterestModel
    {
        public int ClientId { get; set; }
        [Display(Name = "Business Name", Prompt = "Business Name")]
        public string BusinessName { get; set; }
        [Display(Name = "Entity Firm", Prompt = "Entity Firm")]
        public EntityFirmEnum EntityFirmEnum { get; set; }
        [Display(Name = "Firm Ownership", Prompt = "Firm Ownership")]
        public string FirmOwnershipEnum { get; set; }
        [Display(Name = "Amount of Ownership(%)", Prompt = "Amount of Ownership(%)")]
        public decimal AmountOfOwnership { get; set; }
        [Display(Name = "Fair Market Value of Business", Prompt = "Fair Market Value of Business")]
        public int FMVofBusiness { get; set; } //FMV-Fair Market Value        
        [Display(Name = "Projected Value at Retirement", Prompt = "Projected Value at Retirement")]
        public int PBValueatRetirement { get; set; } //Projected Business Value at Retirement
        public bool BSContractCurrently { get; set; } //Buy/Sell Contract Currently in Place?
        public bool FundedLifeIns { get; set; }
        [Display(Name = "Last Reviewed", Prompt = "Last Reviewed")]
        public LastReviewedEnum LastReviewedEnum { get; set; }
        [Display(Name = "Plans for Closely Held Business?")]
        public CloselyHeldBusinessEnum CloselyheldbusinessEnum { get; set; }
    }
}
