﻿namespace LAP.DomainModels.Models
{
    public class DatatableMetaModel
    {
        public DatatableMetaModel()
        {
            sort = new Sort();
            pagination = new Pagination();
        }
        public Pagination pagination { get; set; }
        public Sort sort { get; set; }
    }
    public class Pagination
    {
        public int page { get; set; }
        public int pages { get; set; }
        public int perpage { get; set; }
        public int total { get; set; }
    }
    public class Sort
    {
        public string sort { get; set; }
        public string field { get; set; }
    }
}
