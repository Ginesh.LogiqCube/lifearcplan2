﻿using System.Linq;

namespace LAP.DomainModels.Models
{
    public class Meta
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public int Perpage { get; set; }
        public int Total { get; set; }
        public int RowsTotal { get; set; }
        public string Sort { get; set; }
        public string Field { get; set; }
        public object AdditionalData { get; set; }
    }
    public class KTDataTableResult<T>
    {
        public Meta Meta { get; set; }
        public IQueryable<T> Data { get; set; }

        public KTDataTableResult()
        {
            Meta = new Meta();
            Data = Enumerable.Empty<T>().AsQueryable();
        }
    }
}