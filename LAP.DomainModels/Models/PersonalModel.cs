﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LAP.DomainModels.Models
{
    public class ClientInfo
    {
        [Display(Name = "Marital Status", Prompt = "Marital Status")]
        public MaritalStatusEnum MaritalStatus { get; set; }
        [Display(Name = "First Name", Prompt = "First Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name", Prompt = "Middle Name")]
        public string MiddleInitial { get; set; }
        [Display(Name = "Last Name", Prompt = "Last Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string LastName { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public GenderEnum Gender { get; set; }
        [Display(Name = "Date Of Birth", Prompt = "Date Of Birth")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime? DateOfBirth { get; set; }
        [Display(Name = "Tax Filing Type")]
        public TaxFilingTypeEnum TaxFilingType { get; set; }
        [Display(Name = "US Citizen/Permanent Resident")]
        public bool IsCitizen { get; set; }
        [Display(Name = "Military Service")]
        public bool IsMilitary { get; set; }
        public bool ActiveService { get; set; }
        public bool Discharge { get; set; }
        public bool IsPermanantCitizen { get; set; }
    }
    public class MaritalInfo
    {
        [Display(Name = "Spouse First Name", Prompt = "Spouse First Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string SpouseFirstName { get; set; }
        [Display(Name = "Spouse Middle Name", Prompt = "Spouse Middle Name")]
        public string SpouseMiddleInitial { get; set; }
        [Display(Name = "Spouse Last Name", Prompt = "Spouse Last Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string SpouseLastName { get; set; }

        [Display(Name = "Former Spouse First Name", Prompt = "Former Spouse First Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string XSpouseFirstName { get; set; }
        [Display(Name = "Former Spouse Middle Name", Prompt = "Former Spouse Middle Name")]
        public string XSpouseMiddleInitial { get; set; }
        [Display(Name = "Former Spouse Last Name", Prompt = "Former Spouse Last Name")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string XSpouseLastName { get; set; }
        [Display(Name = "Date Of Current Marriage", Prompt = "Date Of Current Marriage")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public DateTime? DateOfCurrentMarriage { get; set; }
        public bool Prenup { get; set; }
        public bool CareForSurvivingSpouse { get; set; }
        public bool CurrentMarriedTenYears { get; set; }
    }
    public class ContactInfoViewModel
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        [Display(Name = "Street Address", Prompt = "Street Address")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [MaxLength(30)]
        public string Address { get; set; }
        [Display(Name = "Suite/Apt #", Prompt = "Suite/Apt #")]
        [MaxLength(30)]
        public string Address2 { get; set; }
        [Display(Name = "P.O. Box", Prompt = "P.O. Box")]
        public string POBox { get; set; }
        [Display(Name = "Zip Code", Prompt = "Zip Code")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string ZipCode { get; set; }
        [Display(Name = "Country", Prompt = "Country")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Country { get; set; }
        [Display(Name = "State", Prompt = "State")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string State { get; set; }
        [Display(Name = "City", Prompt = "City")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string City { get; set; }
        [Display(Name = "Phone", Prompt = "(___) ___-____")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string Phone { get; set; }
        [Display(Name = "Mobile/Cell Phone", Prompt = "Cell Phone")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string CellNumber { get; set; }
        [Display(Name = "Email Address", Prompt = "Email Address")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]

        [StringLength(50)]
        public string Email { get; set; }
        [Display(Name = "Spouse's Cell Phone", Prompt = "Spouse's Cell Phone")]
        public string SpouseCellNumber { get; set; }
        [Display(Name = "Spouse's Email Address", Prompt = "Spouse's Email Address")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]

        [StringLength(50)]
        public string SpouseEmail { get; set; }
        public AddressTypeEnum AddressType { get; set; }
    }

    public class BudgetLiteInfo
    {
        [Display(Name = "Projected Annual Budget", Prompt = "Projected Annual Budget")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int ProjectedAnnualBudget { get; set; }
        [Display(Name = "Inflationary Budget", Prompt = "Inflationary Budget")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int InflationaryBudget { get; set; }
        [Display(Name = "Non-Inflationary Budget", Prompt = "Non-Inflationary Budget")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int NonInflationaryBudget { get; set; }
        [Display(Name = "Assumed Inflation Rate")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal AssumedInflationRate { get; set; }
    }
    public class InvestmentLiteInfo
    {
        [Display(Name = "Bank Inflationary Investment Amount")]
        public int? BankInflationaryInvestmentAmount { get; set; }
        public double? BankInflationaryRateOfReturn { get; set; }

        [Display(Name = "Bank Non-Inflationary Investment Amount")]
        public int? BankNonInflationaryInvestmentAmount { get; set; }
        public decimal? BankNonInflationaryRateOfReturn { get; set; }
        [Display(Name = "Brokerage Inflationary Investment Amount")]
        public int? BrokerageInflationaryInvestmentAmount { get; set; }
        public decimal? BrokerageInflationaryRateOfReturn { get; set; }
        [Display(Name = "Brokerage Non-Inflationary Investment Amount")]
        public int? BrokerageNonInflationaryInvestmentAmount { get; set; }
        public decimal? BrokerageNonInflationaryRateOfReturn { get; set; }
        [Display(Name = "Annuity Inflationary Investment Amount")]
        public int? AnnuityInflationaryInvestmentAmount { get; set; }
        public decimal? AnnuityInflationaryRateOfReturn { get; set; }
        [Display(Name = "Annuity Non-Inflationary Investment Amount")]
        public int? AnnuityNonInflationaryInvestmentAmount { get; set; }
        public decimal? AnnuityNonInflationaryRateOfReturn { get; set; }
    }
    public class EmailVerification
    {
        [HiddenInput]
        public Guid Guid { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50, ErrorMessage = ValidationMessage.INVALID)]

        [Display(Name = "Email address", Prompt = "Email address")]
        [Remote("CheckEmailExist", "Home")]
        public string Email { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Verification Code", Prompt = "XXXX")]
        [StringLength(4)]
        public string VerificationCode { get; set; }
    }
}