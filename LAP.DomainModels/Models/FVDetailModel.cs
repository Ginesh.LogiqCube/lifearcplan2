﻿using System;

namespace LAP.DomainModels.Models
{
    public class FVDetailModel
    {
        public int PresentValue { get; set; }
        public int FutureValue { get; set; }
        public int Year28FutureValue { get; set; }
        public int Income { get; set; }
        public int IncomeAtRetirement { get; set; }
        public int Contribution { get; set; }
        public int PmtValue
        {
            get => Income - Contribution;
        }
        //public int ContributionFutureValue { get; set; }
        //public int MatchFutureValue { get; set; }
        //public int Year28ContributionFutureValue { get; set; }
        //public int Year28MatchFutureValue { get; set; }

        public double RateOfInterest { get; set; }
        public int ValueOfReturn
        {
            get => Convert.ToInt32(PresentValue * (RateOfInterest / 100));
        }
    }
    public class FVTotalModel
    {
        public int PresentValue { get; set; }
        public int FutureValue { get; set; }
        public int Year28FutureValue { get; set; }
        public int Income { get; set; }
        public int IncomeAtRetirement { get; set; }
        public int PmtValue { get; set; }
        public int ValueOfReturn { get; set; }
    }
}
