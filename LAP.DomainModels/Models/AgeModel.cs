﻿using System;

namespace LAP.DomainModels.Models
{
    public class AgeModel
    {
        public double? YearToRetirement { get; set; }
        public AgeDataModel Client { get; set; }
        public AgeDataModel Spouse { get; set; }
    }
    public class AgeDataModel
    {
        public bool IsRetired { get; set; }
        public double Age { get; set; }
        public int? RetirementAge { get; set; }
        public int? ActualRetirementAge { get; set; }
        public int Age28YearAtRetirement
        {
            get => Convert.ToInt32(Age + 28);
        }
        public int? YearToRetirement { get; set; }
    }
}
