﻿using LAP.DomainModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace LAP.DomainModels.Models
{
    public class HealthCarePlanningModel
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool UnderESP { get; set; }
        public bool UnderME { get; set; }
        public bool Uninsured { get; set; }
        public bool LongTermCare { get; set; }
        public int LongTermCareRate { get; set; }
        public List<int> LstpolicyProvide { get; set; }
        public List<int> LstpolicyPayingBenifits { get; set; }
        public List<int> ListLTCIPolicyProvideBenifits { get; set; }
        public YesNoEnum WaitingPeriodYesNOEnum { get; set; }
        public WaitingPeriodEnum WaitingPeriodEnum { get; set; }
        public YesNoEnum LTCIBenifitsEnumYesNO { get; set; }
        public LTCIbenifitsEnum LTCIbenifitsEnum { get; set; }
        public LTCIbenifirPayouttEnum LTCIbenifirPayouttEnum { get; set; }
        public YesNoEnum InflationProtectionEnum { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Inflationprovision { get; set; }
        public List<SelectListItem> ListpolicyProvide { get; set; }
        public List<SelectListItem> ListpolicyPayingBenifits { get; set; }
        public List<SelectListItem> ListLTCIPolicyProvideBenifitsEnum { get; set; }
    }
}
