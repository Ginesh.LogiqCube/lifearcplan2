﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LAP.DomainModels.Models
{
    public class CharitableModel
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        [Required(ErrorMessage = Message.SELECT_ONE)]
        public CharitableEstateEnum CharitableEstateEnum { get; set; }
        [Required(ErrorMessage = Message.SELECT_ONE)]
        public List<int> CharitableDonations { get; set; }
        [BindProperty, Required(ErrorMessage = Message.SELECT_ONE)]
        public CharitableGiftEnum CharitableGiftEnum { get; set; }
        public List<SelectListItem> CharitablerList { get; set; }
    }
}
