﻿namespace LAP.DomainModels.Models
{
    public class ExplicitVueModel
    {
        public bool InvestmentVersion { get; set; }
        public bool BudgetVersion { get; set; }
        public Income Income { get; set; }
        public Expense Expense { get; set; }
        public CashFlow CashFlow { get; set; }
        public PvModel TotalPv { get; set; }
        public NeededAtRetirement NeededAtRetirement { get; set; }
        public AnnualAvgRateOfReturn AnnualAvgRateOfReturn { get; set; }
    }
    public class Income
    {
        public int ClientEmployment { get; set; }
        public int SpouseEmployment { get; set; }
        public int ClientPreviousEmployment { get; set; }
        public int SpousePreviousEmployment { get; set; }
        public int ClientPersonalActivity { get; set; }
        public int SpousePersonalActivity { get; set; }
        public int ClientPersonalAsset { get; set; }
        public int SpousePersonalAsset { get; set; }
        public int ClientInvestment { get; set; }
        public int SpouseInvestment { get; set; }
        public int ClientSocialSecurity { get; set; }
        public int SpouseSocialSecurity { get; set; }
        public int ClientSocialSecurityAtRetirement { get; set; }
        public int SpouseSocialSecurityAtRetirement { get; set; }
        public int ClientPension { get; set; }
        public int SpousePension { get; set; }
        public int PensionAtRetirement { get; set; }
        public int Total
        {
            get => ClientEmployment + SpouseEmployment +
                   ClientPreviousEmployment + SpousePreviousEmployment +
                   ClientPersonalActivity + SpousePersonalActivity +
                   ClientPersonalAsset + SpousePersonalAsset +
                   ClientInvestment + SpouseInvestment +
                   ClientSocialSecurity + SpouseSocialSecurity +
                   ClientPension + SpousePension;
        }
    }
    public class Expense
    {
        public int Inflationary { get; set; }
        public int InflationaryProjectedAtRetirement { get; set; }
        public int NonInflationary { get; set; }
        public int Total
        {
            get => Inflationary + NonInflationary;
        }
    }
    public class CashFlow
    {
        public int TotalIncome { get; set; }
        public int TotalExpense { get; set; }
        public int CurrentCashFlow
        {
            get => TotalIncome - TotalExpense;
        }
    }
    public class PvModel
    {
        public int ClientEmploymentPlan { get; set; }
        public int ClientEmploymentPlanProjectedAtRetirement { get; set; }
        public int SpouseEmploymentPlan { get; set; }
        public int SpouseEmploymentPlanProjectedAtRetirement { get; set; }
        public int ClientPreEmploymentPlan { get; set; }
        public int ClientPreEmploymentPlanProjectedAtRetirement { get; set; }
        public int SpousePreEmploymentPlan { get; set; }
        public int SpousePreEmploymentPlanProjectedAtRetirement { get; set; }
        public int ClientBankingAccount { get; set; }
        public int ClientBankingAccountProjectedAtRetirement { get; set; }
        public int SpouseBankingAccount { get; set; }
        public int SpouseBankingAccountProjectedAtRetirement { get; set; }
        public int ClientBrokerageAccount { get; set; }
        public int ClientBrokerageAccountProjectedAtRetirement { get; set; }
        public int SpouseBrokerageAccount { get; set; }
        public int SpouseBrokerageAccountProjectedAtRetirement { get; set; }
        public int ClientInsuranceAccount { get; set; }
        public int ClientInsuranceAccountProjectedAtRetirement { get; set; }
        public int SpouseInsuranceAccount { get; set; }
        public int SpouseInsuranceAccountProjectedAtRetirement { get; set; }
        public int Total
        {
            get => ClientEmploymentPlan + SpouseEmploymentPlan
                + ClientPreEmploymentPlan + SpousePreEmploymentPlan
                + ClientBankingAccount + SpouseBankingAccount
                + ClientBrokerageAccount + SpouseBrokerageAccount
                + ClientInsuranceAccount + SpouseInsuranceAccount;
        }
        public int TotalProjectedAtRetirement
        {
            get => ClientEmploymentPlanProjectedAtRetirement + SpouseEmploymentPlanProjectedAtRetirement +
                   ClientPreEmploymentPlanProjectedAtRetirement + SpousePreEmploymentPlanProjectedAtRetirement +
                   ClientBankingAccountProjectedAtRetirement + SpouseBankingAccountProjectedAtRetirement +
                   ClientBrokerageAccountProjectedAtRetirement + SpouseBrokerageAccountProjectedAtRetirement +
                   ClientInsuranceAccountProjectedAtRetirement + SpouseInsuranceAccountProjectedAtRetirement;
        }
        public int TotalPmtValue { get; set; }
    }
    public class NeededAtRetirement
    {
        public int Income { get; set; }
        public long InvestmentAccount { get; set; }
    }
    public class AnnualAvgRateOfReturn
    {
        public double Current { get; set; }
        public double Required { get; set; }
    }
}