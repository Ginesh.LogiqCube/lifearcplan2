﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.DomainModels.Models
{
    public class PrevNextMenuModel
    {
        public string PrevTitle { get; set; }
        public string PrevAction { get; set; }
        public string NextTitle { get; set; }
        public string NextAction { get; set; }
    }
}
