﻿namespace LAP.DomainModels.Models
{
    public class HeirListModel
    {
        public int HeirsId { get; set; }
        public string HeirsName { get; set; }
    }
}
