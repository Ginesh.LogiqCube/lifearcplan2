﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace LAP.DomainModels.Models
{
    public class ProcessViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Desc { get; set; }
        public string Img { get; set; }
        public string Color { get; set; }
    }
    public class MenuModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Action2 { get; set; }
        public int ParentId { get; set; }
        public int PlanningItemId { get; set; }
    }
    public class RiskArcQuestionModel
    {
        public int Id { get; set; }
        public int SectionId { get; set; }
        public Guid Guid { get; set; }
        public string Question { get; set; }
        public string[] Options { get; set; }
        public int Answer { get; set; }
    }
    public class RegulatoryQuestionModel
    {
        public int RegulatoryId { get; set; }
        public int SectionId { get; set; }
        public Guid Guid { get; set; }
        public string Question { get; set; }
        public bool Answer { get; set; }
        public int ParentId { get; set; }
    }
    public class Select2Model
    {
        public IEnumerable<Select2Item> Items { get; set; }
        public int TotalCount { get; set; }
    }
    public class Select2Item
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Text { get; set; }
    }
    public class IntegrationItemModel
    {
        public int Id { get; set; }
        public string PartnerName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string LogoFileName { get; set; }
        public string BorderColor { get; set; }
        public bool Connected { get; set; }
    }
    public class TableModuleModel
    {
        public string TableName { get; set; }
        public string ModuleName { get; set; }
    }
    public class FieldMapModel
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string Title { get; set; }
    }
    public class SqlResultModel
    {
        public int Id { get; set; }
        public string OldValue { get; set; }
        public int Count { get; set; }
    }
    public class WorkingHourModel
    {
        public string Day { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool Active { get; set; }
    }
    public class DashboardConfigModel
    {
        public int x { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int h { get; set; }
        public string id { get; set; }
        public bool active { get; set; }
    }
}
public class FieldChangeModel
{
    public object OldValue { get; set; }
    public object NewValue { get; set; }
    public string TypeName { get; set; }
    public string OldValueFormatted
    {
        get
        {
            return ChangeValueFormat(OldValue, TypeName);
        }
    }
    public string NewValueFormatted
    {
        get
        {
            return ChangeValueFormat(NewValue, TypeName);
        }
    }
    public static string ChangeValueFormat(object obj, string type)
    {
        string result = string.Empty;
        if (obj != null || !string.IsNullOrEmpty(type))
        {
            var typeName = !string.IsNullOrEmpty(type) ? type : obj?.GetType().FullName;
            if (typeName.Contains("DateTime"))
            {
                var dt = Convert.ToDateTime(obj);
                if (dt.Year != 1)
                    result = dt.ToString("MMM dd, yyyy");
            }
            else if (typeName.Contains("Boolean"))
                result = Convert.ToBoolean(obj) ? "Yes" : "No";
            else if (typeName.Contains("Percentage"))
            {
                var percentage = Convert.ToDecimal(obj);
                if (percentage > 0)
                    result = $"{percentage:0.00}%";
            }
            else if (typeName.Contains("Currency"))
            {
                var amount = Convert.ToInt32(obj);
                if (amount > 0)
                    result = string.Format("{0:C0}", obj);
            }
            else if (typeName.Contains("Year"))
            {
                var amount = Convert.ToInt32(obj);
                if (amount > 0)
                    result = $"{obj} Year(s)";
            }
            else if (typeName.Contains("EnumString"))
            {
                if (obj.ToString().Contains('1') && obj.ToString().Contains('2'))
                    result = "{clientName} & {spouseName}";
                else if (obj.ToString().Contains('1'))
                    result = "{clientName}";
                else
                    result = "{spouseName}";
            }
            else if (typeName.Contains("MultiSelectEnum"))
            {
                var enumTypeNameSplit = typeName.Split(":");
                var enumType = enumTypeNameSplit.Length > 1 ? Type.GetType($"LAP.DomainModels.{enumTypeNameSplit[1]}") : null;
                if (enumType != null && obj != null)
                {
                    string htmlList = string.Empty;
                    foreach (var item in obj.ToString().Split(","))
                    {
                        var enumName = Enum.GetName(enumType, Convert.ToInt16(item));
                        if (enumName != null)
                        {
                            var enumDisplayName = enumType.GetMember(enumName)?.First()?.GetCustomAttribute<DisplayAttribute>()?.GetName();
                            string enumString = !string.IsNullOrEmpty(enumDisplayName) ? enumDisplayName : enumName;
                            htmlList += $"<li>{enumString}</li>";
                        }
                    }
                    result = $"<ul>{htmlList}</ul>";
                }
            }
            else if (typeName.Contains("Enum"))
            {
                var enumType = Type.GetType(typeName);
                var enumName = Enum.GetName(enumType, obj);
                if (enumName != null)
                {
                    var enumDisplayName = enumType.GetMember(enumName)?.First()?.GetCustomAttribute<DisplayAttribute>()?.GetName();
                    result = !string.IsNullOrEmpty(enumDisplayName) ? enumDisplayName : enumName;
                }
            }
            else
                result = obj?.ToString();
        }
        return result;
    }
}