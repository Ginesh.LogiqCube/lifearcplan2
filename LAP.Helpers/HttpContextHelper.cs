﻿using LAP.DomainModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LAP.Helpers
{
    public static class HttpContextHelper
    {
        public static async Task LogoutEveryone(this HttpContext httpContext)
        {
            await httpContext.SignOutAsync(AuthenticationSchemes.ClientAuth);
            await httpContext.SignOutAsync(AuthenticationSchemes.UserAuth);
            await httpContext.SignOutAsync(AuthenticationSchemes.SuperAdminAuth);
        }
        public static string GetClientIp(this HttpContext httpContext)
        {
            var remoteIp = httpContext.Request.Headers["X-forwarded-for"].FirstOrDefault();
            var localIp = httpContext.Connection.RemoteIpAddress.ToString();
            return !string.IsNullOrEmpty(remoteIp) ? remoteIp : localIp;
        }
        public static string GetClaimValueByKey(this HttpContext httpContext, string key)
        {
            return httpContext.User.Identity.IsAuthenticated ? httpContext.User.FindFirst(key)?.Value : string.Empty;
        }
        public static string GetClaimValueByKeyFromAuth(this HttpContext httpContext, string key, string authScheme)
        {
            var auth = httpContext.AuthenticateAsync(authScheme).Result;
            string result = string.Empty;
            return auth.Succeeded ? auth.Principal.Claims.FirstOrDefault(c => c.Type == key)?.Value : string.Empty;
        }
        public static string GetCurrentAuthScheme(this HttpContext httpContext)
        {
            string result = string.Empty;
            var clientAuth = httpContext.AuthenticateAsync(AuthenticationSchemes.ClientAuth).Result;
            var userAuth = httpContext.AuthenticateAsync(AuthenticationSchemes.UserAuth).Result;
            var superAdminAuth = httpContext.AuthenticateAsync(AuthenticationSchemes.SuperAdminAuth).Result;
            var isDisplayOnly = httpContext.User.Claims.FirstOrDefault(c => c.Type == "DisplayOnlyClient")?.Value;
            if (clientAuth.Succeeded && !Convert.ToBoolean(isDisplayOnly))
                result = AuthenticationSchemes.ClientAuth;
            else if (userAuth.Succeeded)
                result = AuthenticationSchemes.UserAuth;
            else if (superAdminAuth.Succeeded)
                result = AuthenticationSchemes.SuperAdminAuth;
            return result;
        }
        public static async Task<string> ReadBodyFromRequest(this HttpRequest request)
        {
            request.EnableBuffering();

            using var streamReader = new StreamReader(request.Body, leaveOpen: true);
            var requestBody = await streamReader.ReadToEndAsync();

            request.Body.Position = 0;
            return HttpUtility.UrlDecode(requestBody);
        }
        public static string FormatHeaders(this IHeaderDictionary headers) => string.Join(", ", headers.Select(kvp => $"{{{kvp.Key}: {string.Join(", ", kvp.Value)}}}"));
        public static bool IsLocalOrSupport(this HttpContext httpContext)
        {
            string hostUrl = httpContext.Request.Host.Value;
            return hostUrl.Contains("support") || hostUrl.Contains("localhost");
        }
        public static string GetAbsoluteUrl(this HttpContext httpContext,string path)
        {
            var request = httpContext.Request;
            return $"{request.Scheme}://{request.Host}/{path}";
        }
    }
}
