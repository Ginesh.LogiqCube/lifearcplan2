﻿using LAP.DomainModels.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LAP.Helpers
{
    public static class MenuDataHelper
    {
        public static async Task<IEnumerable<ProcessViewModel>> GetProcessDataAsync()
        {
            var data = new List<ProcessViewModel>
            {
                new ProcessViewModel
                {
                    Id = 1,
                    Title = "LifeArcPlan&trade; Survey",
                    SubTitle = "GETTING TO KNOW YOU",
                    Desc = "You, the client, take an active role in sharing with us what is important to you.",
                    Img = "LAP-6-Steps-Process-1.jpg",
                    Color = "#549e3e"
                },
                new ProcessViewModel
                {
                    Id = 2,
                    Title = "Initial Review Meeting",
                    SubTitle = "GETTING YOUR INFO RIGHT",
                    Desc = "We take a deeper dive with you into the information you provided us in the LifeArcPlan&trade; Survey to identify the opportunities as well as conflicting goals and expectations.",
                    Img = "LAP-6-Steps-Process-2.jpg",
                    Color = "#39a1db"
                },
                new ProcessViewModel
                {
                    Id = 3,
                    Title = "Analyze Information",
                    SubTitle = "PROFESSIONAL ANALYSIS",
                    Desc = "Our seasoned financial, legal and/or tax professionals can now analyze the information you provided to determine how the pieces of your plan fit together.",
                    Img = "LAP-6-Steps-Process-3.jpg",
                    Color = "#275d8e"
                },
                new ProcessViewModel
                {
                    Id = 4,
                    Title = "Create a Sustainable Solution",
                    SubTitle = "360&deg; PERSPECTIVE",
                    Desc = "We align all aspects of your personal life and goals into multi-faceted solutions that seamlessly work together to achieve your plan.",
                    Img = "LAP-6-Steps-Process-4.jpg",
                    Color = "#011e56"
                },
                new ProcessViewModel
                {
                    Id = 5,
                    Title = "Strategy Session to Implement Your Plan",
                    SubTitle = "TAKING CONTROL OF YOUR FUTURE",
                    Desc = "This is an exciting day! This is the day we implement a plan to make your dreams a reality. From the universe of possible solutions, we narrow the list down to the ones that will give you the highest probability of success.",
                    Img = "LAP-6-Steps-Process-5.jpg",
                    Color = "#4e1e52"
                },
                new ProcessViewModel
                {
                    Id = 6,
                    Title = "On-going Monitoring and Measuring",
                    SubTitle = "MEASURING WHAT MATTERS",
                    Desc = "As life events change and circumstances create pivot points in your life, we will adapt your plan to those events by making necessary changes. Our goal is to bring certainty during uncertain times.",
                    Img = "LAP-6-Steps-Process-6.jpg",
                    Color = "#a11923"
                }
            };
            return await Task.FromResult(data);
        }
        public static async Task<IEnumerable<MenuModel>> GetMenuDataAsync()
        {
            var data = new List<MenuModel>
            {
                new MenuModel
                {
                    Id = 1,
                    Title = "Personal Info",
                    Action = "client-info",
                    Controller = "personal"
                },
                new MenuModel
                {
                    Id = 2,
                    Title = "Income",
                    Action = "employment-info",
                    Controller = "income"
                },
                new MenuModel
                {
                    Id = 3,
                    Title = "Expense",
                    Action = "budget-info",
                    Controller = "expense"
                },
                new MenuModel
                {
                    Id = 4,
                    Title = "Planning",
                    Action = "investment-planning-intro",
                    Controller = "Income",
                    PlanningItemId = 2
                },
                new MenuModel
                {
                    Id = 5,
                    Title = "LifeStyle",
                    Action = "life-style-intro",
                    Controller = "LifeStyle",
                },
                new MenuModel
                {
                    Id = 6,
                    Title = "Client Info",
                    Action = "client-info",
                    Controller = "personal",
                    Desc = "Tell us about yourself",
                    ParentId = 1
                },
                new MenuModel
                {
                    Id = 7,
                    Title = "Marital Info",
                    Action = "marital-info",
                    Controller = "personal",
                    Desc = "Tell about your marital status",
                    ParentId = 1,
                },
                new MenuModel
                {
                    Id = 8,
                    Title = "Spouse Info",
                    Action = "spouse-info",
                    Controller = "personal",
                    Desc = "Tell us about yourself",
                    ParentId = 1,
                },
                new MenuModel
                {
                    Id = 9,
                    Title = "Contact Info",
                    Action = "contact-info",
                    Controller = "personal",
                    Desc = "How/ where can we reach you",
                    ParentId = 1
                },
                new MenuModel
                {
                    Id = 10,
                    Title = "Family Info",
                    Action = "family-info",
                    Controller = "personal",
                    Desc = "Help us determine your family needs",
                    ParentId = 1
                },
                new MenuModel
                {
                    Id = 11,
                    Title = "Personal Property",
                    Action = "personal-property",
                    Controller = "personal",
                    Desc = "How do you plan on using your assets",
                    ParentId = 1
                },
                new MenuModel
                {
                    Id = 12,
                    Title = "Employment Info",
                    Action = "employment-info",
                    Controller = "income",
                    Desc = "How are you earning your income",
                    ParentId = 2
                },
                new MenuModel
                {
                    Id = 13,
                    Title = "Retirement Plan",
                    Action = "retirement-plan",
                    Controller = "income",
                    Desc = "Does your employer offer you a retirement plan",
                    ParentId = 2
                },
                new MenuModel
                {
                    Id = 14,
                    Title = "Business Interests",
                    Action = "business-interests",
                    Controller = "income",
                    Desc = "Do you have an ownership in a business",
                    ParentId = 2
                },
                new MenuModel
                {
                    Id = 15,
                    Title = "Personal Activities",
                    Action = "personal-activities",
                    Controller = "income",
                    Desc = "Are you receiving any income outside of your employment",
                    ParentId = 2
                },
                new MenuModel
                {
                    Id = 16,
                    Title = "Personal Assets",
                    Action = "personal-assets",
                    Controller = "income",
                    Desc = "How should you evaluate your personal assets?",
                    ParentId = 2
                },
                new MenuModel
                {
                    Id = 17,
                    Title = "Social Security",
                    Action = "social-security-intro",
                    Controller = "income",
                    Desc = "How should you optimize your benefits",
                    ParentId = 2
                },
                new MenuModel
                {
                    Id = 18,
                    Title = "Investment Income",
                    Action = "investment-income",
                    Controller = "income",
                    Desc = "How should you evaluate your personal income?",
                    ParentId = 2
                },
                  new MenuModel
                {
                    Id = 19,
                    Title = "Intro.",
                    Action = "social-security-intro",
                    Controller = "income",
                    Desc = "How should you optimize your benefits",
                    ParentId = 17
                },
                 new MenuModel
                {
                    Id = 20,
                    Title = "Estimate",
                    Action = "social-security-estimate",
                    Controller = "income",
                    ParentId = 17
                },
               new MenuModel
                {
                    Id = 21,
                    Title = "Budget Info",
                    Action = "budget-info",
                    Controller = "expense",
                    Desc = "What are your lifestyle needs for retirement",
                    ParentId = 3
                },
                new MenuModel
                {
                    Id = 22,
                    Title = "Investment",
                    Action = "investment-planning-intro",
                    Controller = "InvestmentPlanning",
                    Desc = "Understand where you should invest your money",
                    ParentId = 4,
                    PlanningItemId = 1
                },
                new MenuModel
                {
                    Id = 23,
                    Title = "Retirement Income",
                    Action = "retirement-income-planning-intro",
                    Desc = "What and when are your income needs",
                    Controller = "RetirementIncomePlanning",
                    ParentId = 4,
                    PlanningItemId = 3
                },
                new MenuModel
                {
                    Id = 24,
                    Title = "Healthcare",
                    Action = "healthcare-planning-intro",
                    Controller = "HealthcarePlanning",
                    Desc = "Safeguard your assets from expensive medical costs",
                    ParentId = 4,
                    PlanningItemId = 5
                },
                new MenuModel
                {
                    Id = 25,
                    Title = "Personal Care",
                    Action = "personal-care-planning-intro",
                    Controller = "PersonalCarePlanning",
                    Desc = "What if your health deteriorates or you're incapacitated",
                    ParentId = 4,
                    PlanningItemId = 6
                },
                new MenuModel
                {
                    Id = 26,
                    Title = "Education",
                    Action = "education-planning-intro",
                    Controller = "EducationPlanning",
                    Desc = "Identify the optimal strategy to pay for education costs",
                    ParentId = 4,
                    PlanningItemId = 7
                },
                new MenuModel
                {
                    Id = 27,
                    Title = "Charitable",
                    Action = "charitable-planning-intro",
                    Controller = "CharitablePlanning",
                    Desc = "Address your compassion to helps others",
                    ParentId = 4,
                    PlanningItemId = 8
                },
                new MenuModel
                {
                    Id = 28,
                    Title = "Estate",
                    Controller = "EstatePlanning",
                    Action = "estate-planning-intro",
                    Desc = "Manage your assets, wealth transfer, and asset protection",
                    ParentId = 4,
                    PlanningItemId = 9
                },
                new MenuModel
                {
                    Id = 29,
                    Title = "Intro.",
                    Action = "investment-planning-intro",
                    Controller = "InvestmentPlanning",
                    ParentId = 22,
                    PlanningItemId = 1
                },
                new MenuModel
                {
                    Id = 30,
                    Title = "Bank/Credit Union Accounts",
                    Action = "bank-credit-union-accounts",
                    Controller = "InvestmentPlanning",
                    ParentId = 22,
                    PlanningItemId = 1
                },
                new MenuModel
                {
                    Id = 31,
                    Title = "Brokerage/Advisory Accounts",
                    Action = "brokerage-advisory-accounts",
                    Controller = "InvestmentPlanning",
                    ParentId = 22,
                    PlanningItemId = 1
                },
                new MenuModel
                {
                    Id = 32,
                    Title = "Life/Annuity Contracts",
                    Action = "life-annuity-contracts",
                    Controller = "InvestmentPlanning",
                    ParentId = 22,
                    PlanningItemId = 1
                },
                new MenuModel
                {
                    Id = 33,
                    Title = "Intro.",
                    Action = "retirement-income-planning-intro",
                    Controller = "RetirementIncomePlanning",
                    ParentId = 23,
                    PlanningItemId = 3
                },
                new MenuModel
                {
                    Id = 34,
                    Title = "Status",
                    Action = "retirement-income-planning-status",
                    Controller = "RetirementIncomePlanning",
                    ParentId = 23,
                    PlanningItemId = 3
                },
                new MenuModel
                {
                    Id = 35,
                    Title = "Income Concerns",
                    Action = "retirement-income-planning-income-concern",
                    Controller = "RetirementIncomePlanning",
                    ParentId = 23,
                    PlanningItemId = 3
                },
                new MenuModel
                {
                    Id = 36,
                    Title = "Expense Concerns",
                    Action = "retirement-income-planning-expense-concern",
                    Controller = "RetirementIncomePlanning",
                    ParentId = 23,
                    PlanningItemId = 3
                },
                new MenuModel
                {
                    Id = 37,
                    Title = "Intro.",
                    Action = "healthcare-planning-intro",
                    Controller = "HealthcarePlanning",
                    ParentId = 24,
                    PlanningItemId = 5
                },
                new MenuModel
                {
                    Id = 38,
                    Title = "Assessment",
                    Action = "healthcare-planning-assessment",
                    Controller = "HealthcarePlanning",
                    ParentId = 24,
                    PlanningItemId = 5
                },
                new MenuModel
                {
                    Id = 39,
                    Title = "Intro.",
                    Action = "personal-care-planning-intro",
                    Controller = "PersonalCarePlanning",
                    ParentId = 25,
                    PlanningItemId = 6
                },
                new MenuModel
                {
                    Id = 40,
                    Title = "Care Plan",
                    Action = "personal-care-planning-plan",
                    Controller = "PersonalCarePlanning",
                    ParentId = 25,
                    PlanningItemId = 6
                },
                new MenuModel
                {
                    Id = 41,
                    Title = "Intro.",
                    Action = "education-planning-intro",
                    Controller = "EducationPlanning",
                    ParentId = 26,
                    PlanningItemId = 7
                },
                new MenuModel
                {
                    Id = 42,
                    Title = "Assessment",
                    Action = "education-planning-assessment",
                    Controller = "EducationPlanning",
                    ParentId =26,
                    PlanningItemId = 7
                },
                new MenuModel
                {
                    Id = 43,
                    Title = "Intro.",
                    Action = "charitable-planning-intro",
                    Controller = "CharitablePlanning",
                    ParentId = 27,
                    PlanningItemId = 8
                },
                new MenuModel
                {
                    Id = 44,
                    Title = "Charitable Ambitions",
                    Action = "charitable-planning-ambitions",
                    Controller = "CharitablePlanning",
                    ParentId = 27,
                    PlanningItemId = 8
                },
                new MenuModel
                {
                    Id = 45,
                    Title = "Intro.",
                    Action = "estate-planning-intro",
                    Controller = "EstatePlanning",
                    ParentId =28,
                    PlanningItemId = 9
                },
                new MenuModel
                {
                    Id = 46,
                    Title = "General Assessment",
                    Action = "estate-planning-assessment",
                    Controller = "EstatePlanning",
                    ParentId = 28,
                    PlanningItemId = 9
                },
                new MenuModel
                {
                    Id = 47,
                    Title = "Intro.",
                    Action = "life-style-intro",
                    Controller = "LifeStyle",
                    Desc = "General description",
                    ParentId = 5
                },
                new MenuModel
                {
                    Id = 48,
                    Title = "Health Assessment",
                    Action = "health-assessment",
                    Desc = "Give us a high level idea of your overall health",
                    Controller = "LifeStyle",
                    ParentId = 5
                },
                new MenuModel
                {
                    Id = 49,
                    Title = "Expectations",
                    Action = "health-expectations",
                    Controller = "LifeStyle",
                    Desc = "What are your expectations for retirement",
                    ParentId = 5
                }
            };
            return await Task.FromResult(data);
        }
        public static async Task<IEnumerable<MenuModel>> GetAdviserWizardItemDataAsync()
        {
            var data = new List<MenuModel>
            {
                new MenuModel
                {
                    Id = 1,
                    Title = "Adviser Info",
                    Action = "adviser/info",
                    Controller = "Profile",
                    Desc = "Tell us about yourself"
                },
                new MenuModel
                {
                    Id = 2,
                    Title = "Contact Info",
                    Action = "adviser/contact-info",
                    Controller = "Profile",
                    Desc = "How/where can we reach you",
                },
                new MenuModel
                {
                    Id = 3,
                    Title = "Education Info",
                    Action = "adviser/education-info",
                    Controller = "Profile",
                    Desc = "Tell us about your educational qualifications"
                },
                new MenuModel
                {
                    Id = 4,
                    Title = "Work History",
                    Action = "adviser/work-history",
                    Controller = "Profile",
                    Desc = "Tell us about 10 years of your work history"
                },
                new MenuModel
                {
                    Id = 5,
                    Title = "My Business",
                    Action = "adviser/business",
                    Controller = "Profile",
                    Desc = "Share the details about your Business"
                },
                new MenuModel
                {
                    Id = 6,
                    Title = "Regulatory",
                    Action = "adviser/regulatory",
                    Controller = "personal",
                    Desc = "Tell us about your Regulatory track record"
                }
            };
            return await Task.FromResult(data);
        }
        public static async Task<IEnumerable<RegulatoryQuestionModel>> GetRegulatoryQuestionDataAsync()
        {
            var data = new List<RegulatoryQuestionModel>
            {
                #region Parent
                new RegulatoryQuestionModel
                {
                    SectionId = 1,
                    Question = "Have you ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 2,
                    Question = "Based upon activities that occurred while you exercised control over it, has an organization ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 3,
                    Question = "Have you ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 4,
                    Question = "Based upon activities that occurred while you exercised control over it, has an organization ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 5,
                    Question = "Has the U.S. Securities and Exchange Commission or the Commodity Futures Trading Commission ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 6,
                    Question = "Has any other Federal regulatory agency or any state regulatory agency or foreign financial regulatory authority ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 7,
                    Question = "Have you been subject to any final order of a state securities commission (or any agency or office performing like functions), state authority that supervises or examines banks, savings associations, or credit unions, state insurance commission (or any agency or office performing like functions), an appropriate federal banking agency, or the National Credit Union Administration, that:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 8,
                    Question = "Has any self-regulatory organization ever:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 9,
                    Question = "Have you ever had an authorization to act as an attorney, accountant or federal contractor that was revoked or suspended?<br>Have you been notified, in writing, that you are now the subject of any:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 10,
                    Question = "Have you ever been named as a respondent/defendant in an investment-related, consumer-initiated, arbitration or civil litigation which alleged that you were involved in one or more sales practice violations and which:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 11,
                    Question = "Have you ever been the subject of an investment-related, consumer-initiated (written or oral) complaint, which alleged that you were involved in one or more sales practice violations, and which:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 12,
                    Question = "Within the past twenty four (24) months, have you been the subject of an investment- related, consumer-initiated, written complaint, not otherwise reported under question 14I(2) above, which:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 13,
                    Question = "Have you ever been the subject of an investment-related, consumer-initiated arbitration claim or civil litigation which alleged that you were involved in one or more sales practice violations, and which:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 14,
                    Question = "Within the past twenty four (24) months, have you been the subject of an investment- related, consumer-initiated arbitration claim or civil litigation not otherwise reported under question 14I(4) above, which:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 15,
                    Question = "Have you ever voluntarily resigned, been discharged or permitted to resign after allegations were made that accused you of:",
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 16,
                    Question = "Within the past 10 years:",
                },
                #endregion
                #region Child
                new RegulatoryQuestionModel
                {
                    SectionId = 17,
                    Question = @"been convicted of or pled guilty or nolo contendere (""no contest"") in a domestic, foreign, or military court to any felony?",
                    ParentId = 1
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 18,
                    Question = "been charged with any felony?",
                    ParentId = 1
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 19,
                    Question = @"been convicted of or pled guilty or nolo contendere (""no contest"") in a domestic or foreign court to any felony?",
                    ParentId = 2
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 20,
                    Question = "been charged with any felony?",
                    ParentId = 2
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 21,
                    Question = @"been convicted of or pled guilty or nolo contendere (""no contest"") in a domestic, foreign or military court to a misdemeanor involving: investments or an investment-related business or any fraud, false statements or omissions, wrongful taking of property, bribery, perjury, forgery, counterfeiting, extortion, or a conspiracy to commit any of these offenses?",
                    ParentId = 3
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 24,
                    Question = "been charged with any felony?",
                    ParentId = 3
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 22,
                    Question = @"been convicted of or pled guilty or nolo contendere (""no contest"") in a domestic or foreign court to a misdemeanor specified in 14B(1)(a)?",
                    ParentId = 4
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 23,
                    Question = "been charged with a misdemeanor specified in 14B(1)(a)?",
                    ParentId = 4
                },


                new RegulatoryQuestionModel
                {
                    SectionId = 25,
                    Question = "found you to have made a false statement or omission?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 26,
                    Question = "found you to have been involved in a violation of its regulations or statutes?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 27,
                    Question = "found you to have been a cause of an investment-related business having its authorization to do business denied, suspended, revoked, or restricted?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 28,
                    Question = "entered an order against you in connection with investment-related activity?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 29,
                    Question = "imposed a civil money penalty on you, or ordered you to cease and desist from any activity?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 30,
                    Question = "found you to have willfully violated any provision of the Securities Act of 1933, the Securities Exchange Act of 1934, the Investment Advisers Act of 1940, the Investment Company Act of 1940, the Commodity Exchange Act, or any rule or regulation under any of such Acts, or any of the rules of the Municipal Securities Rulemaking Board, or found you to have been unable to comply with any provision of such Act, rule or regulation?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 31,
                    Question = "found you to have willfully aided, abetted, counseled, commanded, induced, or procured the violation by any person of any provision of the Securities Act of 1933, the Securities Exchange Act of 1934, the Investment Advisers Act of 1940, the Investment Company Act of 1940, the Commodity Exchange Act, or any rule or regulation under any of such Acts, or any of the rules of the Municipal Securities Rulemaking Board?",
                    ParentId = 5
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 32,
                    Question = "found you to have failed reasonably to supervise another person subject to your supervision, with a view to preventing the violation of any provision of the Securities Act of 1933, the Securities Exchange Act of 1934, the Investment Advisers Act of 1940, the Investment Company Act of 1940, the Commodity Exchange Act, or any rule or regulation under any of such Acts, or any of the rules of the Municipal Securities Rulemaking Board?",
                    ParentId = 5
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 33,
                    Question = "found you to have made a false statement or omission or been dishonest, unfair or unethical?",
                    ParentId = 6
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 34,
                    Question = "found you to have been involved in a violation of investment-related regulation(s) or statute(s)?",
                    ParentId = 6
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 35,
                    Question = "found you to have been a cause of an investment-related business having its authorization to do business denied, suspended, revoked or restricted?",
                    ParentId = 6
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 36,
                    Question = "entered an order against you in connection with an investment-related activity?",
                    ParentId = 6
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 37,
                    Question = "denied, suspended, or revoked your registration or license or otherwise, by order, prevented you from associating with an investment-related business or restricted your activities?",
                    ParentId = 6
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 38,
                    Question = "bars you from association with an entity regulated by such commission, authority, agency, or officer, or from engaging in the business of securities, insurance, banking, savings association activities, or credit union activities; or constitutes a final order based on violations of any laws or regulations that prohibit fraudulent, manipulative, or deceptive conduct?",
                    ParentId = 7
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 39,
                    Question = "found you to have made a false statement or omission?",
                    ParentId = 8
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 40,
                    Question = @"found you to have been involved in a violation of its rules (other than a violation designated as a ""minor rule violation"" under a plan approved by the U.S. Securities and Exchange Commission)?",
                    ParentId = 8
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 41,
                    Question = "found you to have been the cause of an investment-related business having its authorization to do business denied, suspended, revoked or restricted?",
                    ParentId = 8
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 42,
                    Question = "disciplined you by expelling or suspending you from membership, barring or suspending your association with its members, or restricting your activities?",
                    ParentId = 8
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 43,
                    Question = "found you to have willfully violated any provision of the Securities Act of 1933, the Securities Exchange Act of 1934, the Investment Advisers Act of 1940, the Investment Company Act of 1940, the Commodity Exchange Act, or any rule or regulation under any of such Acts, or any of the rules of the Municipal Securities Rulemaking Board, or found you to have been unable to comply with any provision of such Act, rule or regulation?",
                    ParentId = 8
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 44,
                    Question = "found you to have willfully aided, abetted, counseled, commanded, induced, or procured the violation by any person of any provision of the Securities Act of 1933, the Securities Exchange Act of 1934, the Investment Advisers Act of 1940, the Investment Company Act of 1940, the Commodity Exchange Act, or any rule or regulation under any of such Acts, or any of the rules of the Municipal Securities Rulemaking Board?",
                    ParentId = 8
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 45,
                    Question = "found you to have failed reasonably to supervise another person subject to your supervision, with a view to preventing the violation of any provision of the Securities Act of 1933, the Securities Exchange Act of 1934, the Investment Advisers Act of 1940, the Investment Company Act of 1940, the Commodity Exchange Act, or any rule or regulation under any of such Acts, or any of the rules of the Municipal Securities Rulemaking Board?",
                    ParentId = 8
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 46,
                    Question = @"regulatory complaint or proceeding that could result in a ""yes"" answer to any part of 14C, D or E? (If ""yes"", complete the Regulatory Action Disclosure Reporting Page.)",
                    ParentId = 9
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 47,
                    Question = @"investigation that could result in a ""yes"" answer to any part of 14A, B, C, D or E? (If ""yes"", complete the Investigation Disclosure Reporting Page.)",
                    ParentId = 9
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 48,
                    Question = "is still pending, or; resulted in an arbitration award or civil judgment against you, regardless of amount, or; was settled, prior to 05/18/2009, for an amount of $10,000 or more, or; was settled, on or after 05/18/2009, for an amount of $15,000 or more?",
                    ParentId = 10
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 49,
                    Question = "was settled, prior to 05/18/2009, for an amount of $10,000 or more, or; was settled, on or after 05/18/2009, for an amount of $15,000 or more?",
                    ParentId = 11
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 50,
                    Question = "alleged that you were involved in one or more sales practice violations and contained a claim for compensatory damages of $5,000 or more (if no damage amount is alleged, the complaint must be reported unless the firm has made a good faith determination that the damages from the alleged conduct would be less than $5,000), or; alleged that you were involved in forgery, theft, misappropriation or conversion of funds or securities?",
                    ParentId = 12
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 51,
                    Question = "was settled for an amount of $15,000 or more, or; resulted in an arbitration award or civil judgment against any named respondent(s)/defendant(s), regardless of amount?",
                    ParentId = 13
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 52,
                    Question = "alleged that you were involved in one or more sales practice violations and contained a claim for compensatory damages of $5,000 or more (if no damage amount is alleged, the arbitration claim or civil litigation must be reported unless the firm has made a good faith determination that the damages from the alleged conduct would be less than $5,000), or; alleged that you were involved in forgery, theft, misappropriation or conversion of funds or securities?",
                    ParentId = 14
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 53,
                    Question = "violating investment-related statutes, regulations, rules, or industry standards of conduct?",
                    ParentId = 15
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 54,
                    Question = "fraud or the wrongful taking of property?",
                    ParentId = 15
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 55,
                    Question = "failure to supervise in connection with investment-related statutes, regulations, rules or industry standards of conduct?",
                    ParentId = 15
                },

                new RegulatoryQuestionModel
                {
                    SectionId = 56,
                    Question = "have you made a compromise with creditors, filed a bankruptcy petition or been the subject of an involuntary bankruptcy petition?",
                    ParentId = 16
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 57,
                    Question = "based upon events that occurred while you exercised control over it, has an organization made a compromise with creditors, filed a bankruptcy petition or been the subject of an involuntary bankruptcy petition?",
                    ParentId = 16
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 58,
                    Question = "based upon events that occurred while you exercised control over it, has a broker or dealer been the subject of an involuntary bankruptcy petition, or had a trustee appointed, or had a direct payment procedure initiated under the Securities Investor Protection Act?",
                    ParentId = 16
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 59,
                    Question = "Has a bonding company ever denied, paid out on, or revoked a bond for you?",
                    ParentId = 16
                },
                new RegulatoryQuestionModel
                {
                    SectionId = 60,
                    Question = "Do you have any unsatisfied judgments or liens against you?",
                    ParentId = 16
                }
#endregion
            };
            return await Task.FromResult(data);
        }
        public static async Task<IEnumerable<IntegrationItemModel>> GeIntegrationPartnerDataAsync()
        {
            var data = new List<IntegrationItemModel>
            {
                new IntegrationItemModel
                {
                   Id=1,
                   PartnerName="Redtail",
                   Title="<span style='color:#bd2d31'>REDTAIL</span>",
                   Description="We were the FIRST cloud-based CRM for financial advisors with automated workflows, an intuitive user interface, and integrations galore. That’s working smarter.",
                   LogoFileName="redtail-logo 360x360.png",
                   BorderColor="#bd2d31"
                },
                new IntegrationItemModel
                {
                   Id=2,
                   PartnerName="CollegiateFundingSolutions",
                   Title="<span style='color:#11689c'>Collegiate Funding Solutions</span>",
                   Description="With Collegiate Funding Solutions, help clients save on college costs, avoid student loan debt, and improve retirement savings outlook!",
                   LogoFileName="cfs-logo 360x360.png",
                   BorderColor="#11689c"
                },
                new IntegrationItemModel
                {
                   Id=3,
                   PartnerName="Omniscient",
                   Title="<span style='letter-spacing: 1px;'><span class='lap-primary-text'>OMNI</span><span class='lap-secondary-text'>SCIENT</span></span>",
                   Description="Contact your Omni CRM directly with fusion Elements in order to allow elements to pull existing information directly from your CRM to easily generate paperwork, run reports, and more.",
                   LogoFileName="omni-logo 360x360.png",
                   BorderColor="#0098cf"
                },
                new IntegrationItemModel
                {
                   Id=4,
                   PartnerName="FusionElements",
                   Title="<span class='lap-secondary-text mr-1'>Fusion</span><span class='lap-primary-text'>Elements</span>",
                   Description="Fusion Elements provides advisors the end to end support needed to attain true operational alpha. Owning your process means less time, training, support, and more consistency and efficiency, no matter what else changes.",
                   LogoFileName="fusion-elements 360x360.png",
                   BorderColor="#f4100e"
                },
            };
            return await Task.FromResult(data);
        }
        public static async Task<IEnumerable<WorkingHourModel>> GetDefaultWorkingHourDataAsync()
        {
            List<WorkingHourModel> data = new()
            {
                new WorkingHourModel
                {
                    Day = "Sunday",
                },
                new WorkingHourModel
                {
                    Day = "Monday",
                    StartTime = "9:00 AM",
                    EndTime = "5:00 PM",
                    Active = true
                },
                new WorkingHourModel
                {
                    Day = "Tuesday",
                    StartTime = "9:00 AM",
                    EndTime = "5:00 PM",
                    Active = true
                },
                new WorkingHourModel
                {
                    Day = "Wednesday",
                    StartTime = "9:00 AM",
                    EndTime = "5:00 PM",
                    Active = true
                },
                new WorkingHourModel
                {
                    Day = "Thursday",
                    StartTime = "9:00 AM",
                    EndTime = "5:00 PM",
                    Active = true
                },
                new WorkingHourModel
                {
                    Day = "Friday",
                    StartTime = "9:00 AM",
                    EndTime = "5:00 PM",
                    Active = true
                },
                new WorkingHourModel
                {
                    Day = "Saturday",
                }
            };
            return await Task.FromResult(data);
        }
        public static IEnumerable<FieldMapModel> GeFieldMappingDataAsync()
        {
            var data = new List<FieldMapModel>
            {
                new FieldMapModel
                {
                    FieldName="FirstName",
                    Title="First Name"
                },
                new FieldMapModel
                {
                    FieldName="MiddleInitial",
                    Title="Middle Name"
                },
                new FieldMapModel
                {
                    FieldName="LastName",
                    Title="Last Name"
                },
                new FieldMapModel
                {
                    FieldName="DateOfBirth",
                    Title="Date Of Birth"
                },
                new FieldMapModel
                {
                    FieldName="TaxFilingType",
                    Title="Tax Filing Type"
                },
                new FieldMapModel
                {
                    FieldName="IsCitizen",
                    Title="US Citizen/Permanent Resident"
                },
                new FieldMapModel
                {
                    FieldName="ActiveService",
                    Title="90 days of active Military service"
                },
                new FieldMapModel
                {
                    FieldName="Prenup",
                    Title="We have a prenuptial agreement in place"
                },
                new FieldMapModel
                {
                    FieldName="Discharge",
                    Title="Military discharge from Service"
                },
                new FieldMapModel
                {
                    FieldName="DateOfCurrentMarriage",
                    Title="Date Of Current Marriage"
                },
                 new FieldMapModel
                {
                    FieldName="Address",
                    Title="Street Address"
                },
                new FieldMapModel
                {
                    FieldName="Address2",
                    Title="Suite/Apt #"
                },
                new FieldMapModel
                {
                    FieldName="POBox",
                    Title="P.O.Box"
                },
                new FieldMapModel
                {
                    FieldName="ZipCode",
                    Title="Zip Code"
                },
                new FieldMapModel
                {
                    FieldName="CellNumber",
                    Title="Client Mobile/Cell Phone"
                }, new FieldMapModel
                {
                    FieldName="ClientEmail",
                    Title="Client Email Address"
                },
                new FieldMapModel
                {
                    FieldName="SpouseCellNumber",
                    Title="Spouse's Mobile/Cell phone"
                },
                new FieldMapModel
                {
                    FieldName="SpouseEmail",
                    Title="Spouse Email Address"
                },
                new FieldMapModel
                {
                    FieldName="HeirOf",
                    Title="Heir Of"
                },
                new FieldMapModel
                {
                    FieldName="SpecialNeeds",
                    Title="Special Needs"
                },
                new FieldMapModel
                {
                    FieldName="SpecialNeedsTrust",
                    Title="If there is/ are special needs child(ren), has a Special Needs Trust been established for each heir with special needs ?"
                },
                new FieldMapModel
                {
                    FieldName="Nickname",
                    Title="Nick Name"
                },
                new FieldMapModel
                {
                    FieldName="Type",
                    Title="Property Type"
                },
                new FieldMapModel
                {
                    FieldName="PricePaid",
                    Title="Purchase Price"
                },
                new FieldMapModel
                {
                    FieldName="CurrentValue",
                    Title="Current Value"
                },
                new FieldMapModel
                {
                    FieldName="AmountOwed",
                    Title="Amount owned"
                },
                new FieldMapModel
                {
                    FieldName="ProjectedFairMarketValueRetirement",
                    Title="Projected Value at Retirement"
                },
                new FieldMapModel
                {
                    FieldName="Ownership",
                    Title="Own By"
                },
                new FieldMapModel
                {
                    FieldName="PercentageOfOwnership",
                    Title="Ownership(%)"
                },
                new FieldMapModel
                {
                    FieldName="PropertyPlan",
                    Title="Plans for Property"
                },
                new FieldMapModel
                {
                    TableName="ClientProperty",
                    FieldName="AnnualIncome",
                    Title="Amount of My Annual Income"
                },
                new FieldMapModel
                {
                    TableName="OtherIncome",
                    FieldName="AnnualIncome",
                    Title="Annual amount"
                },
                new FieldMapModel
                {
                    TableName="ClientProperty",
                    FieldName="Years",
                    Title="Duration(Years)"
                },
                new FieldMapModel
                {
                    FieldName="EmployerName",
                    Title="Employer"
                },
                new FieldMapModel
                {
                    FieldName="EmploymentOf",
                    Title="Employment Of"
                },
                new FieldMapModel
                {
                    FieldName="EmploymentStatus",
                    Title="Employment Status"
                },
                new FieldMapModel
                {
                    FieldName="AgeToRetire",
                    Title="Age To Retire"
                },
                 new FieldMapModel
                {
                    FieldName="EmploymentTypes",
                    Title="Employment Types"
                },
                new FieldMapModel
                {
                    FieldName="AnnualEarnings",
                    Title="Annual Earning"
                },
                new FieldMapModel
                {
                    FieldName="TenYearExpectedAnnualIncome",
                    Title="My Income Over The 10 Years Will"
                },
                new FieldMapModel
                {
                    FieldName="ClergyLivingIn",
                    Title="Current living arrangements"
                },
                new FieldMapModel
                {
                    FieldName="PreEmpAnnualAmount",
                    Title="Annual Amount"
                },
                new FieldMapModel
                {
                    FieldName="PreEmpAvgRateReturn",
                    Title="Rate of Return"
                },
                new FieldMapModel
                {
                    FieldName="PreEmpCurrentBalance",
                    Title="Current Balance"
                },
                new FieldMapModel
                {
                    FieldName="EspCurrentBalance",
                    Title="Current Balance"
                },
                new FieldMapModel
                {
                    FieldName="PensionExpectedAge",
                    Title="Pension Expected Age"
                },
                new FieldMapModel
                {
                    FieldName="EspAvgRateReturn",
                    Title="Average Rate Of Return"
                },
                new FieldMapModel
                {
                    FieldName="AmountOfOwnership",
                    Title="Amount of Ownership(%)"
                },
                new FieldMapModel
                {
                    FieldName="BSContractCurrently",
                    Title="Buy/Sell Contract Currently in Place?"
                },
                new FieldMapModel
                {
                    FieldName="BusinessName",
                    Title="Business Name"
                },
                new FieldMapModel
                {
                    FieldName="EntityFirmEnum",
                    Title="Entity Firm"
                },
                new FieldMapModel
                {
                    FieldName="FMVofBusiness",
                    Title="Fair Market Value of Business"
                },
                new FieldMapModel
                {
                    FieldName="FirmOwnershipEnum",
                    Title="Firm Ownership"
                },
                new FieldMapModel
                {
                    FieldName="FundedLifeIns",
                    Title="Funded Life Insurance?"
                },
                new FieldMapModel
                {
                    FieldName="LastReviewedEnum",
                    Title="Last Reviewed"
                },
                new FieldMapModel
                {
                    FieldName="PBValueatRetirement",
                    Title="Projected Value at Retirement"
                },
                new FieldMapModel
                {
                    TableName="BusinessInterest",
                    FieldName="businessInterestOf",
                    Title="Business Interest Of"
                },
                new FieldMapModel
                {
                    FieldName="closelyheldbusinessEnum",
                    Title="Plans for Closely Held Business?"
                },
                new FieldMapModel
                {
                    FieldName="sourceofIncome",
                    Title="Source of Income"
                },
                new FieldMapModel
                {
                    FieldName="sSageBenifitsEnum",
                    Title="At what age do you plan to start receiving your Social Security benefit?"
                },
                new FieldMapModel
                {
                    FieldName="ssexpexting",
                    Title="How much Social Security benefit are you expecting to receive monthly?"
                },
                new FieldMapModel
                {
                    FieldName="ssChildBenefits",
                    Title="Surviving Child Benefits Estimate"
                },
                 new FieldMapModel
                {
                    FieldName="BalanceOwn",
                    Title="Balance Owed"
                },
                new FieldMapModel
                {
                    FieldName="YearsPayOff",
                    Title="Years till pay off"
                },
                new FieldMapModel
                {
                    FieldName="Inflationary",
                    Title="Inflationary?"
                },
                new FieldMapModel
                {
                    FieldName="AccountType",
                    Title="Account Type"
                },
                new FieldMapModel
                {
                    FieldName="AnnualContribtion",
                    Title="Annual Contribution"
                },
                new FieldMapModel
                {
                    FieldName="AvgRateReturn",
                    Title="Rate Of Return"
                },
                new FieldMapModel
                {
                    FieldName="CurrentBalance",
                    Title="Current Balance"
                },
                new FieldMapModel
                {
                    FieldName="PurchasePrice",
                    Title="Purchase Price"
                },
                new FieldMapModel
                {
                    FieldName="TaxClassification",
                    Title="Tax Classification"
                },
                new FieldMapModel
                {
                    FieldName="LifeAnnuityContractType",
                    Title="ContractType"
                },
                new FieldMapModel
                {
                    FieldName="ContractFaceAmount",
                    Title="Contract Face Amount"
                },
                new FieldMapModel
                {
                    FieldName="CashValue",
                    Title="Cash Value"
                },
                new FieldMapModel
                {
                    FieldName="AnnualPremiumAmount",
                    Title="Annual Premium Amount"
                },
                new FieldMapModel
                {
                    FieldName="IsActive",
                    Title="Active"
                },
                new FieldMapModel
                {
                    FieldName="radioListforClientEnum",
                    Title="{clientName}"
                },
                new FieldMapModel
                {
                    FieldName="radioListforSpouseEnum",
                    Title="{spouseName}"
                },
                new FieldMapModel
                {
                    FieldName="policyProvide",
                    Title="Does your policy provide for care at?"
                },
                new FieldMapModel
                {
                    FieldName="policyPayingBenifits",
                    Title="Under what conditions will the policy begin paying benefits?"
                },
                new FieldMapModel
                {
                    FieldName="LTCIPolicyProvideBenifitsEnum",
                    Title="Does your LTCI policy provide any of the following benefits?"
                },
                new FieldMapModel
                {
                    FieldName="CareGiverId",
                    Title="Caregiver"
                },
                new FieldMapModel
                {
                    FieldName="CaregiverName",
                    Title="Name"
                },
                new FieldMapModel
                {
                    FieldName="CurrentAge",
                    Title="Current Age of Student"
                },
                new FieldMapModel
                {
                    FieldName="LegalGuardian",
                    Title="Custodial Parent"
                },
                new FieldMapModel
                {
                    FieldName="NumberOfUears",
                    Title="Number of years before student will begin education program"
                },
                new FieldMapModel
                {
                    FieldName="RelationshipToStudent",
                    Title="Relationship to Student"
                },new FieldMapModel
                {
                    FieldName="StudentName",
                    Title="Name of Student"
                },
                new FieldMapModel
                {
                    FieldName="currentAmount",
                    Title="Current amount saved for education"
                },
                new FieldMapModel
                {
                    FieldName="numberOfYearsEnum",
                    Title="Expected number of years to complete education"
                },
                new FieldMapModel
                {
                    FieldName="savingForEducationEnum",
                    Title="How are you saving for education"
                },
                new FieldMapModel
                {
                    FieldName="typeOfEducationProgramEnum",
                    Title="Type of education program"
                },
                 new FieldMapModel
                {
                    FieldName="charitableDonations",
                    Title="What is the intent of the donation?"
                },
                new FieldMapModel
                {
                    FieldName="charitableEstateEnum",
                    Title="How much of your estate do you want to give?"
                },
                new FieldMapModel
                {
                    FieldName="charitableGiftEnum",
                    Title="When do you plan for the organization, charity, or private foundation to receive the gift?"
                },
                new FieldMapModel
                {
                    FieldName="isClientActive",
                    Title="{clientName}"
                },
                new FieldMapModel
                {
                    FieldName="isSpouseActive",
                    Title="{spouseName}"
                },
                new FieldMapModel
                {
                    FieldName="IsMilitary",
                    Title="Military Service"
                },
                new FieldMapModel
                {
                    FieldName="CareForSurvivingSpouse",
                    Title="In the event I predecease my spouse/partner, it is important to me to financially provide for him/her during their lifetime."
                },
                new FieldMapModel
                {
                    FieldName="BudgetVersion",
                    Title="Budget Version"
                },
                new FieldMapModel
                {
                    FieldName="InvestmentVersion",
                    Title="Investment Version"
                },
                new FieldMapModel
                {
                    FieldName="RiskArcVersion",
                    Title="RiskArc Version"
                },
                new FieldMapModel
                {
                    FieldName="PersonalCareType",
                    Title="Personal Care Type"
                },
                new FieldMapModel
                {
                    FieldName="LTCIBenifitsEnumYesNO",
                    Title="Do you know how long your LTCI benefits last?"
                },
                new FieldMapModel
                {
                    FieldName="LTCIbenifitsEnum",
                    Title="Personal Care Type"
                },
                new FieldMapModel
                {
                    FieldName="LongTermCareRate",
                    Title="What does your LTC plan pay at a daily rate for coverage?"
                },
                new FieldMapModel
                {
                    FieldName="inflationProtectionEnum",
                    Title="Does your LTCI policy provide Inflation protection?"
                },
                new FieldMapModel
                {
                    FieldName="inflationprovision",
                    Title="What is the inflation provision in your policy?"
                },
                new FieldMapModel
                {
                    FieldName="lTCIbenifirPayouttEnum",
                    Title="Does your LTCI benefits payout on an"
                },
                new FieldMapModel
                {
                    FieldName="longTermCare",
                    Title="Personal Care Type"
                },
                new FieldMapModel
                {
                    FieldName="underESP",
                    Title="Are you covered under an employer sponsored healthcare plan?"
                },
                new FieldMapModel
                {
                    FieldName="underME",
                    Title="Are you covered under a marketplace exchange healthcare plan?"
                },
                new FieldMapModel
                {
                    FieldName="uninsured",
                    Title="Are you currently uninsured for healthcare coverage?"
                },
                new FieldMapModel
                {
                    FieldName="waitingPeriodEnum",
                    Title="How long is the waiting period or elimination period?"
                },
                new FieldMapModel
                {
                    FieldName="waitingPeriodYesNOEnum",
                    Title="Does your LTCI policy have a 'waiting period' or 'elimination period'?"
                },

                new FieldMapModel
                {
                    FieldName="sSexemptionEnum",
                    Title="Does your LTCI policy have a 'waiting period' or 'elimination period'?"
                },
                new FieldMapModel
                {
                    FieldName="ssGovPension",
                    Title="Do you have a Federal, State, or Municipal pension with a Government Pension Offset(GPO)?"
                },
                new FieldMapModel
                {
                    FieldName="ssLegally",
                    Title="Are you legally exempt based upon one of the following exemptions?"
                },
                new FieldMapModel
                {
                    FieldName="ssmonthly",
                    Title="What is your monthly Social Security Benefit?"
                },
                new FieldMapModel
                {
                    FieldName="ssreceiving",
                    Title="Are you currently receiving Social Security?"
                },
                new FieldMapModel
                {
                    FieldName="ssretirementbenefit",
                    Title="Are you eligible to receive Social Security retirement benefits?"
                },
                new FieldMapModel
                {
                    FieldName="Amount",
                    Title="Annual amount",
                    TableName="ClientBudget"
                },
                new FieldMapModel
                {
                    FieldName="BankInflationaryInvestmentAmount",
                    Title="Balance(Banking) - Qualified Accounts",
                },
                new FieldMapModel
                {
                    FieldName="BankNonInflationaryInvestmentAmount",
                    Title="Balance(Banking) - Non-Qualified Accounts",
                },
                new FieldMapModel
                {
                    FieldName="BrokerageInflationaryInvestmentAmount",
                    Title="Balance(Investment) Qualified Accounts",
                },
                new FieldMapModel
                {
                    FieldName="BrokerageNonInflationaryInvestmentAmount",
                    Title="Balance(Investment) Non-Qualified Accounts",
                },
                new FieldMapModel
                {
                    FieldName="AnnuityInflationaryInvestmentAmount",
                    Title="Balance(Insurance) - Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="AnnuityNonInflationaryInvestmentAmount",
                    Title="Balance(Insurance) - Non-Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="BankInflationaryRateOfReturn",
                    Title="Rate of Return(Banking) - Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="BankNonInflationaryRateOfReturn",
                    Title="Rate of Return(Banking) - Non-Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="BrokerageInflationaryRateOfReturn",
                    Title="Rate of Return(Investment) - Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="BrokerageNonInflationaryRateOfReturn",
                    Title="Rate of Return(Investment) - Non-Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="AnnuityInflationaryRateOfReturn",
                    Title="Rate of Return(Insurance) - Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="AnnuityNonInflationaryRateOfReturn",
                    Title="Rate of Return(Insurance) - Non-Qualified Accounts"
                },
                new FieldMapModel
                {
                    FieldName="AssumedInflationRate",
                    Title="Assumed Inflation Rate"
                },
                new FieldMapModel
                {
                    FieldName="InflationaryBudget",
                    Title="How much of my projected budget is inflationary expenses"
                },
                new FieldMapModel
                {
                    FieldName="NonInflationaryBudget",
                    Title="How much of my projected budget is non-inflationary expenses"
                },
                new FieldMapModel
                {
                    FieldName="ProjectedAnnualBudget",
                    Title="Projected Annual Budget"
                },
                new FieldMapModel
                {
                    FieldName="EmployerOffer",
                    Title="Employer Offer"
                },
                new FieldMapModel
                {
                    FieldName="PensionBenefit",
                    Title="Pension Expected Monthly Benefit"
                },
                new FieldMapModel
                {
                    FieldName="PensionSurvivorBenefit",
                    Title="Survivor Benefit"
                },
                new FieldMapModel
                {
                    FieldName="ClergyAllowance",
                    Title="Your housing allowance designated by the church is"
                },
                new FieldMapModel
                {
                    FieldName="ClergyIsExcludeFromRetirement",
                    Title="Is it important to you to maintain your Housing Allowance exclusion throughout your retirement years?"
                },
                new FieldMapModel
                {
                    FieldName="AccountName",
                    Title="Account Name"
                },
                new FieldMapModel
                {
                    FieldName="AnnualContribution",
                    Title="Annual Contribution"
                },
                new FieldMapModel
                {
                    TableName="LifeAnnuityContract",
                    FieldName="AnnualIncome",
                    Title="Annual Income"
                },
                new FieldMapModel
                {
                    TableName="BankCreditUnionAccount",
                    FieldName="AnnualIncome",
                    Title="Annual Income"
                },
                new FieldMapModel
                {
                    TableName="BrokerageAdvisoryAccount",
                    FieldName="AnnualIncome",
                    Title="Annual Income"
                },
                new FieldMapModel
                {
                    FieldName="ContractType",
                    Title="Type"
                },
                new FieldMapModel
                {
                    FieldName="IssueDate",
                    Title="Issue Date"
                },
                new FieldMapModel
                {
                    FieldName="RateOfReturn",
                    Title="Rate Of Return"
                },
                new FieldMapModel
                {
                    FieldName="ispermanantcitizen",
                    Title="US Citizen/Permanent Resident"
                },
                new FieldMapModel
                {
                    FieldName="EspContribution",
                    Title="Annual Contribution"
                },
                new FieldMapModel
                {
                    FieldName="EspContributionPercentage",
                    Title="Annual Contribution Percentage"
                },
                new FieldMapModel
                {
                    FieldName="EspEmployerMatch",
                    Title="Annual Employer Match"
                },
                new FieldMapModel
                {
                    FieldName="EspEmployerMatchPercentage",
                    Title="Annual Employer Match Percentage"
                },
                new FieldMapModel
                {
                    FieldName="FinalThought",
                    Title="Final Thought"
                },
                new FieldMapModel
                {
                    FieldName="ssexemptionStatus",
                    Title="Explanation for your exemption status"
                },
            };
            return data;
        }
    }
}