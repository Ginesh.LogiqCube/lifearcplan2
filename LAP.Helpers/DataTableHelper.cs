﻿using LAP.DomainModels.Entities.Client;
using System;
using System.Linq;
using System.Reflection;

namespace LAP.Helpers
{
    public static class DataTableHelper
    {
        public static string GetSearchQueryFromSearchModel(object searchModel)
        {
            string query = string.Empty;
            string[] includeType = { "image", "audio", "video", "link" };
            string[] excludeField = { "Roles", "RolesList", "ComplianceName" };
            var properties = searchModel?.GetType().GetProperties().Where(m => m.GetValue(searchModel) != null && !excludeField.Contains(m.Name.Replace("_", ".")));
            foreach (PropertyInfo property in properties)
            {
                string propValue = property.GetValue(searchModel).ToString().ToLower();
                string prop = property.Name.Replace("_", ".");
                var returnType = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                var typeCode = Type.GetTypeCode(returnType);

                if (prop == "FileType")
                    prop = includeType.Contains(propValue) ? "ContentType" : "FileName";
                switch (typeCode)
                {
                    case TypeCode.Int32:
                        query += $"{prop} == {propValue} and ";
                        break;
                    case TypeCode.String:
                        if (prop.Contains(".Name") || prop.Contains("ClientName") || prop.Contains("AdviserName"))
                        {
                            var userType = prop.Contains("ClientName") ? "Client" : "User";
                            var firstNameField = prop.Replace(".name", ".FirstName");
                            var lastNameField = prop.Replace(".name", ".LastName");
                            var usernameCondition = !prop.Contains("Only") ? $" or Username.ToLower().Contains(\"{propValue}\")" : "";
                            query += $"(({userType}Data.FirstName.ToLower() + \" \" + {userType}Data.LastName.ToLower()).Contains(\"{propValue}\"){usernameCondition}) and ";
                        }
                        else
                            query += $"{prop}.ToLower().Contains(\"{propValue}\") and ";
                        break;
                    default:
                        query += $"{prop}.ToString().Contains(\"{propValue}\") and ";
                        break;
                }
            }
            query = !string.IsNullOrEmpty(query) ? query[..query.LastIndexOf(" ", query.LastIndexOf(" ") - 1)].TrimEnd() : string.Empty;
            return query;
        }
        public static string GetSortString(string field, string order)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(order))
            {
                var firstNameField = field.Replace(".name", ".FirstName");
                var lastNameField = field.Replace(".name", ".LastName");
                result = field.Contains(".name", StringComparison.OrdinalIgnoreCase) ? $"{firstNameField} {order},{lastNameField} {order}" : $"{field} {order}";
            }
            return result;
        }
    }
}