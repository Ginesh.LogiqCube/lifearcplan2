﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LAP.Helpers
{
    public class DropdownDataHelper : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly Guid ClientLoginGuid;
        protected readonly Guid UserLoginGuid;
        private bool disposed;

        public DropdownDataHelper(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            ClientLoginGuid = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid() ?? Guid.Empty;
            UserLoginGuid = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid() ?? Guid.Empty;
        }
        public static async Task<IEnumerable<SelectListItem>> GetMaritaStatusTypeAsync()
        {
            var result = EnumHelper.GetEnumSelectList<MaritalStatusEnum>();
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetTaxFilingTypeAsync(string maritalStatus)
        {
            var status = EnumHelper.ToEnum<MaritalStatusEnum>(maritalStatus);
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<TaxFilingTypeEnum>();
            result = status switch
            {
                MaritalStatusEnum.Married or MaritalStatusEnum.Separated => result.Where(item => Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.Joint || Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.Separate),
                MaritalStatusEnum.Divorced => result.Where(item => Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.Single || Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.HOH),
                MaritalStatusEnum.Widowed => result.Where(item => Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.Single || Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.HOH || Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.QualifyChild),
                _ => result.Where(item => Convert.ToInt32(item.Value) == (int)TaxFilingTypeEnum.Single),
            };
            if (result.Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; });
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetBudgetCategoriesAsync(int? parentId)
        {
            var budgetItems = _context2.BudgetItems.Where(m => m.ParentId == parentId).OrderBy(m => m.SortOrder);
            var result = budgetItems.Where(m => m.ParentId == parentId).Select(m => new SelectListItem
            {
                Text = m.ItemName,
                Value = m.Id.ToString(),
                Selected = budgetItems.Count() == 1,
            });
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetHeirOfAsync(string maritalStatus, Guid guid = default)
        {
            Guid gid = guid == Guid.Empty ? ClientLoginGuid : guid;
            var client = await _context2.ClientLogins.Where(m => m.Guid == gid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData).FirstOrDefaultAsync();
            string clientName = client.ClientData.FirstName;
            var status = EnumHelper.ToEnum<MaritalStatusEnum>(maritalStatus);
            List<SelectListItem> result = new()
            {
                new SelectListItem
                {
                    Text = clientName,
                    Group = new SelectListGroup() { Name = !string.IsNullOrEmpty(clientName) ? "Client" : "" },
                    Value = ((int)ClientTypeEnum.Client).ToString()
                }
            };
            switch (status)
            {
                case MaritalStatusEnum.Married:
                case MaritalStatusEnum.Separated:
                    string SpouseName = client.SpouseData?.FirstName;
                    string grp = !string.IsNullOrEmpty(SpouseName) ? "Spouse" : "";
                    bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                    if (IsMarried)
                    {
                        result.Add(new SelectListItem
                        {
                            Text = $"{SpouseName}",
                            Group = new SelectListGroup() { Name = grp },
                            Value = $"{(int)ClientTypeEnum.Spouse}"
                        });
                        result.Add(new SelectListItem
                        {
                            Text = IsMarried ? $"{clientName} & {SpouseName}" : clientName,
                            Value = $"{(int)ClientTypeEnum.Client},{(int)ClientTypeEnum.Spouse}"
                        });
                    }
                    break;
            }
            if (result.AsEnumerable().Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; }).ToList();
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetOwnershipAsync(string maritalStatus, Guid guid = default)
        {
            Guid gid = guid == Guid.Empty ? ClientLoginGuid : guid;
            var client = await _context2.ClientLogins.Where(m => m.Guid == gid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData).FirstOrDefaultAsync();
            string clientName = client.ClientData.FirstName;
            var status = EnumHelper.ToEnum<MaritalStatusEnum>(maritalStatus);
            List<SelectListItem> result = new()
            {
                new SelectListItem
                {
                    Text = clientName,
                    Group = new SelectListGroup() { Name = !string.IsNullOrEmpty(clientName) ? "Client" : "" },
                    Value = ((int)ClientTypeEnum.Client).ToString()
                }
            };
            switch (status)
            {
                case MaritalStatusEnum.Married:
                case MaritalStatusEnum.Separated:
                    string SpouseName = client.SpouseData?.FirstName;
                    string grp = !string.IsNullOrEmpty(SpouseName) ? "Spouse" : "";
                    bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                    if (IsMarried)
                    {
                        result.Add(new SelectListItem
                        {
                            Text = $"{SpouseName}",
                            Group = new SelectListGroup() { Name = grp },
                            Value = $"{(int)ClientTypeEnum.Spouse}"
                        });
                        result.Add(new SelectListItem
                        {
                            Text = IsMarried ? $"{clientName} & {SpouseName}" : clientName,
                            Value = $"{(int)ClientTypeEnum.Client},{(int)ClientTypeEnum.Spouse}"
                        });
                    }
                    break;
            }
            result.Add(new SelectListItem
            {
                Text = "Trust/Legal Entity",
                Value = ((int)ClientTypeEnum.Other).ToString()
            });
            if (result.AsEnumerable().Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; }).ToList();
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetEmploymentOfAsync(string maritalStatus, Guid guid = default)
        {
            Guid gid = guid == Guid.Empty ? ClientLoginGuid : guid;
            var client = await _context2.ClientLogins.Where(m => m.Guid == gid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData).FirstOrDefaultAsync();
            string clientName = client.ClientData.FirstName;
            string SpouseName = client.SpouseData?.FirstName;
            var spouseDob = client.SpouseData?.DateOfBirth;
            var status = EnumHelper.ToEnum<MaritalStatusEnum>(maritalStatus);
            List<SelectListItem> result = new()
            {
                new SelectListItem
                {
                    Text = clientName,
                    Group = new SelectListGroup() { Name = !string.IsNullOrEmpty(clientName) ? "Client" : "" },
                    Value = ((int)ClientTypeEnum.Client).ToString(),
                }
            };
            switch (status)
            {
                case MaritalStatusEnum.Married:
                case MaritalStatusEnum.Separated:
                    string grp = !string.IsNullOrEmpty(SpouseName) ? "Spouse" : "";
                    bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                    if (IsMarried)
                        result.Add(new SelectListItem
                        {
                            Text = $"{SpouseName}",
                            Group = new SelectListGroup() { Name = grp },
                            Value = $"{(int)ClientTypeEnum.Spouse}",
                            Disabled = spouseDob == null || spouseDob == DateTime.MinValue
                        });
                    break;
            }
            if (result.AsEnumerable().Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; }).ToList();
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetEmploymentStatusAsync()
        {
            var result = EnumHelper.GetEnumSelectList<EmploymentStatusEnum>();
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetEmploymentTypeAsync(string employmentStatus)
        {
            var status = EnumHelper.ToEnum<EmploymentStatusEnum>(employmentStatus);
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<EmploymentTypeEnum>();
            switch (status)
            {
                case EmploymentStatusEnum.FullTimeEmployee:
                case EmploymentStatusEnum.PartTimeEmployee:
                    result = result.Where(item => Convert.ToInt32(item.Value) != (int)EmploymentTypeEnum.K1);
                    break;
            }
            if (result.Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; });
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetEmployerOfferTypeAsync(string employmentStatus)
        {
            var status = EnumHelper.ToEnum<EmploymentStatusEnum>(employmentStatus);
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<EmployerOfferEnum>();
            switch (status)
            {
                case EmploymentStatusEnum.FullTimeEmployee:
                case EmploymentStatusEnum.PartTimeEmployee:
                    result = result.Where(item =>
                    Convert.ToInt32(item.Value) != (int)EmployerOfferEnum.SEPIRA && Convert.ToInt32(item.Value) != (int)EmployerOfferEnum.Solo401K);
                    break;
                case EmploymentStatusEnum.SelfEmployed:
                    result = result.Where(item =>
                    Convert.ToInt32(item.Value) == (int)EmployerOfferEnum.SEPIRA ||
                    Convert.ToInt32(item.Value) == (int)EmployerOfferEnum.Solo401K || Convert.ToInt32(item.Value) == (int)EmployerOfferEnum.KeoughPlan);
                    break;
            }
            if (result.Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; });
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetEmployersAsync(int EmploymentOf)
        {
            var Employers = _context2.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid && (int)m.EmploymentOf == EmploymentOf).Select(m =>
                    new SelectListItem { Value = m.Id.ToString(), Text = m.Employer }).ToList();
            if (Employers.AsEnumerable().Count() == 1)
                Employers = Employers.Select(m => { m.Selected = true; return m; }).ToList();
            return await Task.FromResult(Employers);
        }
        public async Task<IEnumerable<SelectListItem>> GetFirmOwnershipOfAsync(string maritalStatus)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData).FirstOrDefaultAsync();
            string clientName = client.ClientData.FirstName;
            var spouseDob = client.SpouseData?.DateOfBirth;
            var status = EnumHelper.ToEnum<MaritalStatusEnum>(maritalStatus);
            List<SelectListItem> result = new()
            {
                new SelectListItem
                {
                    Text = clientName,
                    Group = new SelectListGroup() { Name = !string.IsNullOrEmpty(clientName) ? "Client" : "" },
                    Value = ((int)ClientTypeEnum.Client).ToString(),
                }
            };
            switch (status)
            {
                case MaritalStatusEnum.Married:
                case MaritalStatusEnum.Separated:
                    string SpouseName = client.SpouseData?.FirstName;
                    string grp = !string.IsNullOrEmpty(SpouseName) ? "Spouse" : "";
                    bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                    if (IsMarried)
                    {
                        result.Add(new SelectListItem
                        {
                            Text = $"{SpouseName}",
                            Group = new SelectListGroup() { Name = grp },
                            Value = $"{(int)ClientTypeEnum.Spouse}"
                        });
                        result.Add(new SelectListItem
                        {
                            Text = IsMarried ? $"{clientName} & {SpouseName}" : clientName,
                            Value = $"{(int)ClientTypeEnum.Client},{(int)ClientTypeEnum.Spouse}"
                        });
                    }
                    break;
            }
            result.Add(new SelectListItem
            {
                Text = "Trust",
                Value = ((int)ClientTypeEnum.Trust).ToString(),
            });
            result.Add(new SelectListItem
            {
                Text = "Other",
                Value = ((int)ClientTypeEnum.Other).ToString(),
            });
            if (result.AsEnumerable().Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; }).ToList();
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetLifeAnnuityContractTypeAsync(string contractType)
        {
            var status = EnumHelper.ToEnum<ContractTypeEnum>(contractType);
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<LifeAnnuityContractTypeEnum>();
            switch (status)
            {
                case ContractTypeEnum.Life:
                    result = result.Where(item => Convert.ToInt32(item.Value) <= 5 || Convert.ToInt32(item.Value) == 9);
                    break;
                case ContractTypeEnum.Annuity:
                    result = result.Where(item => Convert.ToInt32(item.Value) > 5);
                    break;
            }
            if (result.Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; });
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetCareGiverAsync()
        {
            var heirs = _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            var result = heirs.Select(s => new SelectListItem
            {
                Text = s.FirstName,
                Value = s.Id.ToString()
            }).ToList();
            result.Add(new SelectListItem() { Text = "Other", Value = "-1" });
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetPermissionTypesAsync(int roleId)
        {
            var role = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value.Replace(" ", "");
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<AdminRoleEnum>();
            result = result.Where(item => Convert.ToInt32(item.Value) > roleId);
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetFieldOfStudyAsync(string degree)
        {
            var degreeVal = EnumHelper.ToEnum<DegreeTypeEnum>(degree);
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<DegreeEnum>();
            switch (degreeVal)
            {
                case DegreeTypeEnum.Associate:
                    result = result.Where(item => Convert.ToInt32(item.Value) >= 1 && Convert.ToInt32(item.Value) <= 5);
                    break;
                case DegreeTypeEnum.Bachelor:
                    result = result.Where(item => Convert.ToInt32(item.Value) >= 6 && Convert.ToInt32(item.Value) <= 10);
                    break;
                case DegreeTypeEnum.Master:
                    result = result.Where(item => Convert.ToInt32(item.Value) >= 11 && Convert.ToInt32(item.Value) <= 17);
                    break;
                case DegreeTypeEnum.Doctorate:
                    result = result.Where(item => Convert.ToInt32(item.Value) >= 18 && Convert.ToInt32(item.Value) <= 23);
                    break;
                case DegreeTypeEnum.Professional:
                    result = result.Where(item => Convert.ToInt32(item.Value) >= 24 && Convert.ToInt32(item.Value) <= 26);
                    break;
            }
            if (result.Count() == 1)
                result = result.Select(i => { i.Selected = true; return i; });
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetAdviserClientsAsync(int UserId)
        {
            var clients = _context2.ClientLogins.Where(m => m.UserLogin.Id == UserId && m.IsDeleted != true).Include(m => m.ClientData).OrderBy(m => m.ClientData.FirstName).ThenBy(m => m.ClientData.LastName);
            var result = clients.Select(s => new SelectListItem
            {
                Text = s.ClientData.Name,
                Value = s.Id.ToString()
            }).ToList();
            return await Task.FromResult(result);
        }
        public static async Task<IEnumerable<SelectListItem>> GetPropertyTypeAsync(bool disabledPrimary = false)
        {
            var result = EnumHelper.GetEnumSelectList<PropertyTypeEnum>();
            if (disabledPrimary)
                result = result.Select(m =>
                {
                    m.Disabled = disabledPrimary && Convert.ToInt32(m.Value) == (int)PropertyTypeEnum.Primary; return m;
                });
            return await Task.FromResult(result);
        }
        public async Task<Select2Model> GetFirmsAsync(string term, int page)
        {
            IEnumerable<Firm> firms = _context2.Firms;
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                firms = firms.Where(m => m.Name != "LQC");
            if (!string.IsNullOrEmpty(term))
                firms = firms.Where(m => m.Name.Contains(term, StringComparison.OrdinalIgnoreCase));
            var skip = page > 0 ? (page - 1) * 10 : 0;
            var items = firms.OrderBy(m => m.Name).Select(s => new Select2Item
            {
                Guid = s.Guid,
                Text = s.Name,
                Id = s.Id
            }).Skip(skip).Take(10);
            var result = new Select2Model
            {
                Items = items,
                TotalCount = firms.Count()
            };
            return await Task.FromResult(result);
        }
        public async Task<Select2Model> GetAdvisersAsync(string term, int page, int FirmId, int ownerId)
        {
            IEnumerable<UserLogin> advisers = _context2.UserLogins.Where(m => m.FirmId == FirmId && !m.IsAssistant).Include(m => m.UserData);
            if (ownerId > 0)
                advisers = advisers.Where(m => m.Id != ownerId);
            if (!string.IsNullOrEmpty(term))
                advisers = advisers.Where(m => m.Id.ToString().Contains(term) || m.UserData.Name.Contains(term, StringComparison.OrdinalIgnoreCase));
            var skip = page > 0 ? (page - 1) * 10 : 0;
            var items = advisers.OrderBy(m => m.UserData.FirstName).ThenBy(m => m.UserData.LastName).Select(s => new Select2Item
            {
                Text = $"{s.UserData.Name} - {s.Id}",
                Id = s.Id,
                Guid = s.Guid
            }).Skip(skip).Take(10);
            var result = new Select2Model
            {
                Items = items,
                TotalCount = advisers.Count()
            };
            return await Task.FromResult(result);
        }
        public async Task<Select2Model> GetAffiliationsAsync(AffiliationTypeEnum type, string term, int page)
        {
            IEnumerable<Affiliation> affiliations = _context2.Affiliations.Where(m => m.Type == type);
            if (!string.IsNullOrEmpty(term))
                affiliations = affiliations.Where(m => m.Name.Contains(term, StringComparison.OrdinalIgnoreCase));
            var skip = page > 0 ? (page - 1) * 10 : 0;
            var emptyItem = new List<Select2Item> { new Select2Item { Text = "new Item" } };
            var items = affiliations.OrderBy(m => m.Name).Select(s => new Select2Item
            {
                Id = s.Id,
                Guid = s.Guid,
                Text = s.Name,
            }).Skip(skip).Take(10);
            var result = new Select2Model
            {
                Items = emptyItem.Concat(items),
                TotalCount = affiliations.Count()
            };
            return await Task.FromResult(result);
        }
        public async Task<Select2Model> GetComplianceUserAsync(string term, int FirmId, int page)
        {
            IEnumerable<UserLogin> advisers = _context2.UserLogins.Where(m => m.FirmId == FirmId && m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Compliance))
                .Include(m => m.UserData).Include(m => m.Permissions);
            if (!string.IsNullOrEmpty(term))
                advisers = advisers.Where(m => m.UserData.Name.Contains(term, StringComparison.OrdinalIgnoreCase));
            var skip = page > 0 ? (page - 1) * 10 : 0;
            var items = advisers.OrderBy(m => m.UserData.FirstName).ThenBy(m => m.UserData.LastName).Select(s => new Select2Item
            {
                Text = s.UserData.Name,
                Id = s.Id,
                Guid = s.Guid
            }).Skip(skip).Take(10);
            var result = new Select2Model
            {
                Items = items,
                TotalCount = advisers.Count()
            };
            return await Task.FromResult(result);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~DropdownDataHelper()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}