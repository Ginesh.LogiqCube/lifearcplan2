﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Extensions.Configuration;

namespace LAP.Helpers
{
    public class FileHelper : IDisposable
    {
        protected readonly IConfiguration _config;
        protected readonly IAmazonS3 _s3Client;
        protected readonly TransferUtility _transferUtility;
        private bool disposed;

        public FileHelper(IConfiguration config)
        {
            _config = config;
            _s3Client = new AmazonS3Client(_config["S3:AccessKey"], _config["S3:SecretKey"], RegionEndpoint.USEast1);
            _transferUtility = new TransferUtility(_s3Client);
        }
        public async Task CreateS3BucketFolder(string bucketPath)
        {
            var sp = bucketPath.Split("/");
            var bucketName = sp[0];
            var folderPath = $"{bucketPath.Replace($"{bucketName}/", string.Empty)}/";

            ListObjectsRequest listRequest = new()
            {
                BucketName = bucketName,
                Prefix = folderPath
            };
            ListObjectsResponse response = await _s3Client.ListObjectsAsync(listRequest);
            if (response.S3Objects.Count == 0)
            {
                PutObjectRequest request = new()
                {
                    BucketName = bucketName,
                    Key = folderPath
                };
                await _s3Client.PutObjectAsync(request);
            }
        }
        public static string ByteToBase64(byte[] fileBytes, string contentType)
        {
            string base64String = Convert.ToBase64String(fileBytes, 0, fileBytes.Length);
            return $"data:{contentType};base64,{base64String}";
        }
        public async Task<string> GetS3FileBase64String(string bucketPath, string fileName, string contentType)
        {
            S3FileResult file = await DownloadFileFromS3Bucket(bucketPath, fileName);
            string base64String = ByteToBase64(file.FileContent, contentType);
            return base64String;
        }
        public async Task<GetObjectResponse> GetS3FileObjectAsync(GetObjectRequest request)
        {
            return await _s3Client.GetObjectAsync(request);
        }
        public async Task<GetObjectResponse> UploadFileToS3Bucket(Stream stream, string bucketPath, string FileName)
        {
            FileName = HttpUtility.UrlDecode(FileName);
            await CreateS3BucketFolder(bucketPath);
            var uploadRequest = new TransferUtilityUploadRequest
            {
                InputStream = stream,
                BucketName = bucketPath,
                Key = FileName,
                PartSize = 6291456 //6 MB.
            };
            if (bucketPath.Contains("public"))
                uploadRequest.CannedACL = S3CannedACL.PublicRead;
            await _transferUtility.UploadAsync(uploadRequest);
            GetObjectRequest request = new()
            {
                BucketName = bucketPath,
                Key = FileName,
            };
            return await GetS3FileObjectAsync(request);
        }
        public async Task<S3FileResult> DownloadFileFromS3Bucket(string bucketPath, string fileName, string versionId = null)
        {
            GetObjectRequest request = new()
            {
                BucketName = bucketPath,
                Key = fileName,
                VersionId = versionId
            };
            byte[] bytes;
            string contentType;
            using (GetObjectResponse response = await _s3Client.GetObjectAsync(request))
            {

                using var ms = new MemoryStream();
                response.ResponseStream.CopyTo(ms);
                bytes = ms.ToArray();
                contentType = response.Headers.ContentType;
            }
            return new S3FileResult
            {
                FileContent = bytes,
                ContentType = contentType,
                FileName = fileName
            };
        }

        public async Task<string> GetLatestVersionId(string bucketPath, string fileName)
        {
            GetObjectRequest request = new()
            {
                BucketName = bucketPath,
                Key = fileName,
            };
            GetObjectResponse response = await _s3Client.GetObjectAsync(request);
            return response.VersionId;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _transferUtility.Dispose();
                    _s3Client.Dispose();
                }
                disposed = true;
            }
        }
        ~FileHelper()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    public class S3FileResult
    {
        public byte[] FileContent { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}
