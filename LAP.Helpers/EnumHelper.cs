﻿using LAP.DomainModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LAP.Helpers
{
    public static class EnumHelper
    {
        public static IEnumerable<SelectListItem> GetEnumSelectList<T>()
        {
            return (Enum.GetValues(typeof(T)).Cast<T>().Select(
                enu => new SelectListItem()
                {
                    Text = (enu as Enum).GetDisplayName(),
                    Value = Convert.ToInt32(enu).ToString()
                })).ToList();
        }
        public static T ToEnum<T>(string value)
        {
            return !string.IsNullOrEmpty(value) ? (T)Enum.Parse(typeof(T), value, true) : default;
        }
    }
}