﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace LAP.Helpers
{
    public class EmailHelper : IDisposable
    {
        protected readonly IConfiguration _config;
        private bool disposed;

        public EmailHelper(IConfiguration config)
        {
            _config = config;
        }
        public async Task SendEmailAsync(string to, string cc, string bcc, string subject, string htmlBody, string envWebRootPath, List<LinkedResource> linkedResources = null)
        {
            MailMessage message = new()
            {
                IsBodyHtml = true,
            };
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            List<LinkedResource> resources = new()
            {
                new LinkedResource(Path.Combine(envWebRootPath, "images/brand_logo.png"))
                {
                    ContentId = "LogoImage"
                }
            };
            if (linkedResources != null)
                resources.AddRange(linkedResources);
            foreach (var image in resources)
                alternateView.LinkedResources.Add(image);
            message.AlternateViews.Add(alternateView);
            string fromEmail = _config["Email:FromEmail"];
            message.From = new MailAddress(fromEmail, "LifeArcPlan");
            foreach (var item in to.TrimEnd(',').Split(','))
                message.To.Add(new MailAddress(item));

            if (!string.IsNullOrEmpty(cc))
                foreach (var item in cc.Split(','))
                    message.CC.Add(new MailAddress(item));

            if (!string.IsNullOrEmpty(bcc))
                foreach (var item in bcc.Split(','))
                    message.Bcc.Add(new MailAddress(item));

            message.Subject = subject;
            message.Body = htmlBody;
            SmtpClient smtpClient = new(_config["Smtp:Host"], Convert.ToInt32(_config["Smtp:Port"]));
            System.Net.NetworkCredential credentials = new(_config["Smtp:Username"], _config["Smtp:Password"]);
            smtpClient.Credentials = credentials;
            smtpClient.EnableSsl = true;
            smtpClient.Timeout = 20000;
            await smtpClient.SendMailAsync(message);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }
        ~EmailHelper()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
