﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Excel.FinancialFunctions;
using System;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace LAP.Helpers
{
    public static class ExtensionMethod
    {
        public static dynamic StringToJsonObject<T>(this object jsonString)
        {
            return JsonSerializer.Deserialize<T>(jsonString.ToString());
        }
        public static string JsonObjectToString(this object jsonObject)
        {
            return jsonObject is not null ? JsonSerializer.Serialize(jsonObject) : string.Empty;
        }
        const string EncryptionKey = "L1O2G3I4Q5C6U7B8E9";
        public static async Task<string> EncryptAsync(this string plainText)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(plainText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using MemoryStream ms = new();
                using (CryptoStream cs = new(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                plainText = Convert.ToBase64String(ms.ToArray());
            }
            return await Task.FromResult(plainText);
        }
        public static async Task<string> DecryptAsync(this string cipherText)
        {
            try
            {
                cipherText = cipherText.Replace(" ", "+");
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using MemoryStream ms = new();
                    using (CryptoStream cs = new(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
                return await Task.FromResult(cipherText);
            }
            catch (Exception)
            {
                return "InvalidToken";
            }
        }
        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (request.Headers["X-Requested-With"] == "XMLHttpRequest")
                return true;
            if (request.Headers is not null)
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            return false;
        }
        public static DateTime? ToDefaultDate(this DateTime? date)
        {
            return date != default(DateTime) ? date : null;
        }
        public static int StringToInt(this string str)
        {
            string result = null;
            if (!string.IsNullOrEmpty(str))
                result = Regex.Replace(str, @"[^0-9.]", "").Trim();
            return Convert.ToInt32(result);
        }
        public static decimal StringToDecimal(this string str)
        {
            string result = null;
            if (!string.IsNullOrEmpty(str))
                result = Regex.Replace(str, @"[^0-9.]", "").Trim();
            return Convert.ToDecimal(result);
        }
        public static bool IsNotEmptyDate(this DateTime date)
        {
            if (date != DateTime.MinValue && date.Year != 1900 && date.Year != 0001)
                return true;
            else
                return false;
        }
        public static bool Between(this int num, int lower, int upper, bool inclusive = true)
        {
            return inclusive
                ? lower <= num && num <= upper
                : lower < num && num < upper;
        }
        public static bool Between(this double num, int lower, int upper, bool inclusive = true)
        {
            return inclusive
                ? lower <= num && num <= upper
                : lower < num && num < upper;
        }
        public static bool Between(this long num, int lower, int upper, bool inclusive = true)
        {
            return inclusive
                ? lower <= num && num <= upper
                : lower < num && num < upper;
        }
        public static bool BetweenDate(this DateTime date, DateTime start, DateTime end)
        {
            return start <= date && date <= end;
        }
        public static bool IsBetweenTime(this DateTime date, TimeSpan start, TimeSpan end)
        {
            var time = date.TimeOfDay;
            return start <= time && time <= end;
        }
        public static int? GetRemainYearToRetirement(this DateTime birthdate, int? retirementAge)
        {
            int? yearToRetirement = null;
            if (retirementAge != null)
            {
                int y = Convert.ToInt32(retirementAge);
                int retirementYear = birthdate.AddYears(y).Year;
                int currentYear = DateTime.UtcNow.Year;
                yearToRetirement = retirementYear > currentYear ? retirementYear - currentYear : 0;
            }
            return yearToRetirement;
        }
        public static int? GetRemainMonthToRetirement(this DateTime birthdate, int? retirementAge)
        {
            double? monthToRetirement = null;
            if (retirementAge != null)
            {
                int y = Convert.ToInt32(retirementAge);
                DateTime retirementDate = birthdate.AddYears(y);
                DateTime currentDate = DateTime.UtcNow;
                monthToRetirement = retirementDate > currentDate ? retirementDate.Subtract(currentDate).Days / (365.25 / 12) : 0;
            }
            return Convert.ToInt32(monthToRetirement % 12);
        }
        public static double CalculateAge(this DateTime dateOfBirth)
        {
            double age = 0;
            if (dateOfBirth.IsNotEmptyDate())
                age = (DateTime.UtcNow.Date - dateOfBirth.Date).TotalDays / 365;
            return age;
        }
        public static int NagativeToZero(this int value)
        {
            return value > 0 ? value : 0;
        }
        public static double NagativeToZero(this double value)
        {
            return value > 0 ? value : 0;
        }
        public static long NagativeToZero(this long value)
        {
            return value > 0 ? value : 0;
        }
        public static int CalculateFutureValue(double rate, double? yearToRetire, int? presentValue = 0, int? income = 0, int? contribution = 0)
        {
            yearToRetire = yearToRetire != null ? yearToRetire : 27;
            double result = Financial.Fv(rate / 100, Convert.ToInt32(yearToRetire), Convert.ToInt32(income - contribution), -Convert.ToDouble(presentValue), PaymentDue.BeginningOfPeriod);
            bool isInRange = result.Between(int.MinValue, int.MaxValue);
            return isInRange ? Convert.ToInt32(result).NagativeToZero() : 0;
        }
        public static double CalculateRate(double? clientYearToRetirement, int totalPmt, int totalPrsentValue, long totalFutureValue)
        {
            double result = 0;
            clientYearToRetirement = clientYearToRetirement > 0 ? clientYearToRetirement : 1;
            try
            {
                result = Financial.Rate(Convert.ToInt32(clientYearToRetirement), totalPmt, totalPrsentValue, -totalFutureValue, PaymentDue.BeginningOfPeriod) * 100;
            }
            catch { }
            return result.NagativeToZero();
        }
        public static Guid ToGuid(this string stringValue)
        {
            return stringValue != null ? new Guid(stringValue) : Guid.Empty;
        }
        public static Guid ClaimToGuid(this Claim claimValue)
        {
            return claimValue != null ? new Guid(claimValue.Value) : Guid.Empty;
        }
        public static int ClaimToInt(this Claim claimValue)
        {
            return claimValue != null ? Convert.ToInt32(claimValue.Value) : 0;
        }
        public static string ToCurrency(this int amount)
        {
            int multiplier = amount > 0 ? 1 : -1;
            string prefix = amount < 0 ? "-" : string.Empty;
            return $"{prefix}{string.Format("{0:C0}", amount * multiplier)}";
        }
        public static string FormatNagativeAmount(this int amount)
        {
            string sign = amount > 0 ? "" : "-";
            string result = string.Format("{0:C0}", amount);
            if (amount < 0)
                result = $"{sign}{string.Format("{0:C0}", Math.Abs(amount))}";
            return result;
        }
        public static string BoolToYesNo(this bool value)
        {
            if (value)
                return "Yes";
            else
                return "No";
        }
        public static ActionContext GetActionContext(string areaName, string controllerName, string actionName, string host, IServiceProvider serviceProvider)
        {
            RouteData routeData = new();
            var actionDescriptor = new ControllerActionDescriptor();
            var httpContext = new DefaultHttpContext { RequestServices = serviceProvider };
            if (!string.IsNullOrEmpty(areaName))
            {
                routeData.Values["area"] = areaName;
                actionDescriptor.RouteValues["area"] = areaName;
            }
            routeData.Values["controller"] = controllerName;
            actionDescriptor.RouteValues["controller"] = controllerName;
            actionDescriptor.ControllerName = controllerName;

            actionDescriptor.ActionName = actionName;
            routeData.Values["action"] = actionName;
            actionDescriptor.RouteValues["action"] = actionName;

            httpContext.Request.Path = $"/{areaName}/{controllerName}/{actionName}";
            httpContext.Request.Scheme = "http";
            httpContext.Request.Host = new HostString(host);
            var actionContext = new ActionContext(httpContext, routeData, actionDescriptor);
            return actionContext;
        }
        public static string GetMd5Hash(this string input)
        {
            using MD5 md5Hash = MD5.Create();
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new();
            for (int i = 0; i < data.Length; i++)
                sBuilder.Append(data[i].ToString("x2"));
            return sBuilder.ToString();
        }
        public static string StringShortn(this string str, int? length)
        {
            if (length > 0)
            {
                string suffix = str.Length > length ? "..." : string.Empty;
                string result = !string.IsNullOrEmpty(suffix) ?
                    $"{str[..Convert.ToInt32(length)]}{suffix}" : str;
                return result;
            }
            return str;
        }
        public static string GetInitials(this string name)
        {
            string[] nameSplit = name.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

            string initials = "";

            foreach (string item in nameSplit)
            {
                initials += item[..1].ToUpper();
            }

            return initials[..2];
        }
    }
}