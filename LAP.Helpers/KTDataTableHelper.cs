﻿using LAP.DomainModels.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace LAP.Helpers
{
    public static class KTDataTableHelper
    {
        public static async Task<KTDataTableResult<T>> PagedListAsync<T>(this IQueryable<T> query, DatatableMetaModel meta, string includeProperties = null, string sortQueryString = null, string searchQueryString = null) where T : class
        {
            if (!string.IsNullOrEmpty(includeProperties))
                foreach (var include in includeProperties.Split(","))
                    query = query.Include(include);

            if (!string.IsNullOrEmpty(sortQueryString))
                query = query.OrderBy(sortQueryString);

            if (!string.IsNullOrEmpty(searchQueryString))
                query = query.Where(searchQueryString);

            var result = new KTDataTableResult<T>();
            int perpage = meta.pagination.perpage;
            int page = meta.pagination.page;
            result.Meta.Page = page;
            result.Meta.Perpage = perpage;
            result.Meta.Total = query.Count();
            var pages = (double)result.Meta.Total / perpage;
            result.Meta.Pages = (int)Math.Ceiling(pages);
            var skip = (page - 1) * perpage;
            result.Data = perpage > 0 ? query.Skip(skip).Take(perpage) : query;
            return await Task.FromResult(result);
        }
    }
}
