﻿var showHideStatus = function (employmentstatus) {
    if (employmentstatus == EmploymentStatus.FULLTIME || employmentstatus == EmploymentStatus.PARTTIME || employmentstatus == EmploymentStatus.SELFEMPLOYED) {
        $("[data-target='#step-1']").parent().removeClass("d-none");
        $("[data-target='#step-2']").parent().removeClass("d-none");
        $("[data-target='#step-3']").parent().removeClass("d-none");
        var occupation = $("#Occupation_Occupation").val();

        var clergyOccupations = ["pastor", "preacher", "priest", "minister", "music minister", "youth minister", "youth pastor", "rabbi", "evangelist", "cantor", "cardinal", "bishop", "clergy", "chaplain", "missionary"];
        if (!clergyOccupations.includes(occupation.toLowerCase())) {
            $("[data-target='#step-3']").parent().addClass("d-none");
            var step3 = $("#step-3");
            step3.find("input[type='text']").val(null);
            step3.find("select").val(null).change();
            step3.find("[type='checkbox']").prop("checked", false);
            step3.find(".alert").remove();
        }
        $("[data-target='#step-4']").parent().removeClass("d-none");
        $("[data-target='#step-5']").parent().removeClass("d-none");
        $(".full").removeClass("d-none");
        $(".stepwizard").removeClass("d-none");
        $("#Occupation_AgeToRetire").removeClass("disabled");
        $("#Occupation_AgeToRetire").parent().removeClass("disabled-div");
    }
    else if (employmentstatus == EmploymentStatus.HOMEMAKER || employmentstatus == EmploymentStatus.RETIRED) {
        $("[data-target='#step-1']").parent().removeClass("d-none");
        $("[data-target='#step-2']").parent().addClass("d-none");
        $("[data-target='#step-3']").parent().addClass("d-none");
        $("[data-target='#step-4']").parent().addClass("d-none");
        $("[data-target='#step-5']").parent().addClass("d-none");
        $(".stepwizard").addClass("d-none");
        $(".full").addClass("d-none");
        $("#Occupation_AgeToRetire").val(0);
        if (employmentstatus == EmploymentStatus.RETIRED) {
            $(".retired").removeClass("d-none");
            var age = $("#Occupation_EmploymentOf").val() == ClientType.CLIENT ? $("#ClientAge").val() : $("#SpouseAge").val();
            $("#Occupation_AgeToRetire").val(age);
            $("#Occupation_AgeToRetire").addClass("disabled");
            $("#Occupation_AgeToRetire").parent().addClass("disabled-div");
            $("[data-target='#step-1']").parent().addClass("d-none");
        }
    }
}
var showHideStatusEdit = function (offer, hasPension) {
    if (hasPension && offer.length > 0) {
        $("[data-target='#step-4']").parent().removeClass("d-none");
        $("[data-target='#step-5']").parent().removeClass("d-none");
        showHideWizard(4);
    }
    else {
        if (offer.length > 0) {
            $("[data-target='#step-4']").parent().removeClass("d-none");
            $("[data-target='#step-5']").parent().addClass("d-none");
            showHideWizard(4);
        }
        else if (hasPension) {
            $("[data-target='#step-4']").parent().addClass("d-none");
            $("[data-target='#step-5']").parent().removeClass("d-none");
            showHideWizard(5);
        }
    }
}
var setWizardOrder = function () {
    $(".stepwizard-step:visible").each(function (i, item) {
        $(this).find("button").html(i + 1);
        $(this).find("p").html(`Step ${i + 1}`);
    });
}
var showHideEsp = function (hasEsp, target) {
    if (hasEsp == true)
        $(`.${target}`).removeClass("d-none");
    else {
        $(`.${target} .form-control`).val("");
        $(`.${target}`).addClass("d-none");
    }
}
var showHideEspOffer = function (offer) {
    if (offer && offer.length > 0) {
        $(".esp").removeClass("d-none");
        $("#espmatch").removeClass("d-none");
        var status = $("#Occupation_EmploymentStatus").val();
        if (status == EmploymentStatus.SELFEMPLOYED) {
            if (!offer.includes(EmployerOfferType.KEOUGHPLAN.toString()))
                $("#espmatch").addClass("d-none");
            else
                $("#espmatch").removeClass("d-none");
        }
    }
    else
        $(".esp").addClass("d-none");
}
var showHideWizard = function (currentWizard) {
    var nextToWizard = $(`.stepwizard-step [data-order="${currentWizard}"]`).parent().nextAll(".stepwizard-step:visible").find("button").attr("data-order");
    if (currentWizard) {
        $("[id^=step]").addClass("d-none");
        $(`#step-${Number(currentWizard)}`).removeClass("d-none");
        $(".btn-default").removeClass("active");
        $(`[data-order="${currentWizard}"]`).addClass("active");
        $(`[data-order="${currentWizard}"]`).removeAttr("disabled");
    }
    if (nextToWizard) {
        $("#btn_next").removeClass("d-none");
        $("#btn_submit_modal").addClass("d-none");
    }
    else {
        $("#btn_next").addClass("d-none");
        $("#btn_submit_modal").removeClass("d-none");
    }
    if ($(".stepwizard-step:visible").length == 1)
        $(".stepwizard").addClass("d-none");
}
var bindEmploymentType = function (employmentstatus) {
    $.get(`income/GetEmploymentType?employmentStatus=${employmentstatus}`, function (response) {
        var html = "";
        $.each(response, function (i, item) {
            var s = item.selected == true ? "selected" : "";
            html += `<option value="${item.value}" ${s}>${item.text}</option>`
        });
        $("#Occupation_EmploymentTypes").html(html);
        $('#Occupation_EmploymentTypes').selectpicker('refresh');
    });
}
var bindEmployerOfferType = function (employmentstatus) {
    $.get(`income/getemployeroffertype?employmentStatus=${employmentstatus}`, function (response) {
        var html = "";
        $.each(response, function (i, item) {
            var s = item.selected == true ? "selected" : "";
            html += `<option value="${item.value}" ${s}>${item.text}</option>`
        });
        $("#Esp_EmployerOffer").html(html);
        $('#Esp_EmployerOffer').selectpicker('refresh');
    });
}

$(`#espcontri .none-zero,#espmatch .none-zero`).on('input', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode != 9) {
        var id = $(this).attr("id").toLowerCase();
        if (id.includes("match"))
            $("#espmatch .none-zero").not(this).val("");
        else
            $("#espcontri .none-zero").not(this).val("");
    }
});
$(`#Occupation_Occupation`).on('input', function (e) {
    var employmentStatus = $(`#Occupation_EmploymentStatus`).val();
    showHideStatus(employmentStatus);
    setWizardOrder();
    var currentWizard = $('.stepwizard-step .active').parent().find("button").attr("data-order");
    showHideWizard(currentWizard);
});
$(`#Occupation_Zip`).on('input', function (e) {
    var zip = $(this).val();
    showHideZip(zip)
});
$(`#Occupation_EmploymentStatus`).change(function (e) {
    var employmentstatus = Number(e.target.value);
    bindEmploymentType(employmentstatus);
    bindEmployerOfferType(employmentstatus);
    showHideStatus(employmentstatus);
    setWizardOrder();
    var currentWizard = $('.stepwizard-step .active').parent().find("button").attr("data-order");
    showHideWizard(currentWizard);
});
$(`#Esp_EmployerOffer`).change(function (e) {
    var offer = $("#Esp_EmployerOffer").val();
    showHideEspOffer(offer);
    $("#step-4 input").val("");
});
$(`#Esp_HasPensionPlan`).change(function (e) {
    var hasPension = $(this).is(":checked");
    showHideEsp(hasPension, "pensionplan");
});
$(`#Occupation_EmploymentOf`).change(function (e) {
    setDOB();
    var employmentstatus = $("#Occupation_EmploymentStatus").val();
    showHideStatus(employmentstatus);
});
$(`.stepwizard-step button`).click(function (e) {
    e.preventDefault();
    var targetid = $(this).attr("data-target");
    $("[id^=step]").addClass("d-none");
    $(targetid).removeClass("d-none");
    $(".btn-default").removeClass("active");
    $(this).addClass("active");
    var currentWizard = $('.stepwizard-step .active').parent().find("button").attr("data-order");
    showHideWizard(currentWizard);
})
$(`#Occupation_AgeToRetire`).on('input', function (e) {
    var dob = $("#Occupation_DOB").val();
    var yearToRetire = getYearToRetirement(dob, e.target.value);
    if (yearToRetire <= 99)
        $("#Occupation_YearsToRetire").val(yearToRetire);
});
$(`#btn_next`).click(function (e) {
    e.preventDefault();
    if ($(this).closest("form").valid()) {
        var currentWizard = $('.stepwizard-step .active').parent().find("button").attr("data-order");
        var nextToWizard = $(`.stepwizard-step [data-order="${currentWizard}"]`).parent().nextAll(".stepwizard-step:visible").find("button").attr("data-order");
        showHideWizard(nextToWizard);
    }
});