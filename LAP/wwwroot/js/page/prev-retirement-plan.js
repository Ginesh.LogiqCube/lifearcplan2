﻿$(document).on('input', '.required', function (e) {
    var val = e.target.value;
    if (val.length == 0)
        $(this).addClass("input-validation-error");
    else
        $(this).removeClass("input-validation-error");
});
$(document).on('click', '.btn-prevesp-edit', function (e) {
    $(".edit-view").addClass("d-none");
    $(".display-view").removeClass("d-none");
    var tr = $(this).closest("tr");
    tr.find(".input-validation-error").removeClass("input-validation-error");
    tr.find(".edit-view").removeClass("d-none");
    tr.find(".display-view").addClass("d-none");
    var trEdit = tr.find(".edit-view");
    var employername = tr.find(".display-view.employername").text().trim();
    trEdit.find(".employername").val(employername);
    var currentbalance = tr.find(".display-view.currentbalance").text().replace(/[^0-9]/gi, '');
    trEdit.find(".currentbalance").val(currentbalance);
    var ror = tr.find(".display-view.ror").text().replace(/[^0-9.]/gi, '');
    trEdit.find(".ror").val(ror);   
    var hasIncome = tr.find(".display-view.hasincome").text().trim() == "Yes";
    if (!hasIncome)
        trEdit.find(".annualamount").addClass("d-none");
    else {
        var annualamount = tr.find(".display-view.annualamount").text().replace(/[^0-9]/gi, '');
        trEdit.find(".annualamount").val(annualamount);
    }    
})
$(document).on('click', '.btn-prevesp-cancel', function () {
    var tr = $(this).closest("tr");
    tr.find(".edit-view").addClass("d-none");
    tr.find(".display-view").removeClass("d-none");
})
$(document).off('click', '.btn-prevesp-save');
$(document).on('click', '.btn-prevesp-save', function () {
    $(".zero-not-allowed").trigger("input");
    if ($(".input-validation-error:visible").length == 0) {
        var tr = $(this).closest("tr");
        tr.find(".btn-prevesp-save").prop("disabled", true);
        tr.find(".btn-prevesp-cancel i").addClass("d-none");
        tr.find(".btn-prevesp-cancel .spinner").removeClass("d-none");
        var guid = tr.attr("data-guid");
        var employername = tr.find(".edit-view .employername").val();
        var employmentof = tr.attr("data-employmentof");
        var preempcurrentbalance = tr.find(".edit-view .currentbalance").val();
        var preempavgratereturn = tr.find(".edit-view .ror").val();
        var preempannualamount = tr.find(".edit-view .annualamount").val();
        $.post(`income/updateprevespinfo`,
            {
                guid: guid,
                employername: employername,
                employmentof: employmentof,
                preempcurrentbalance: preempcurrentbalance,
                preempavgratereturn: preempavgratereturn,
                preempannualamount, preempannualamount,
            }, function (response) {
                if (response) {
                    if (response.success) {
                        tr.find(".btn-prevesp-save").prop("disabled", false);
                        tr.find(".btn-prevesp-cancel i").removeClass("d-none");
                        tr.find(".btn-prevesp-cancel .spinner").addClass("d-none");
                        var responseObj = response.data.clientPrevEspInfo;
                        tr.find(".currentbalance").text(currencyFormat.format(responseObj.preEmpCurrentBalance));
                        var hasincome = responseObj.hasPreEmpIncome == true ? "Yes" : "No";
                        tr.find(".display-view.hasincome").text(hasincome);
                        tr.find(".display-view.ror").text(`${Number(responseObj.preEmpAvgRateReturn).toFixed(2)}%`);
                        var preEmpAnnualAmount = responseObj.preEmpAnnualAmount > 0 ? currencyFormat.format(responseObj.preEmpAnnualAmount) :"-"
                        tr.find(".display-view.annualamount").parent().removeClass("text-right").removeClass("text-center").addClass(`text-right`);
                        tr.find(".display-view.annualamount").text(preEmpAnnualAmount);
                        tr.find(".display-view.employername").text(responseObj.employerName);
                        $(".edit-view").addClass("d-none");
                        $(".display-view").removeClass("d-none");
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                        LoadExplicitVue();
                    }
                    else {
                        toastr.options = toastrOptions;
                        toastr.error(response.message);
                    }
                }
                else {
                    toastr.options = toastrOptions;
                    toastr.error("Error Occur");
                }
            })
    }
    else
        Swal.fire("", "Please enter valid data in highlighted field!", "error");
});
$(document).on('change', '.hasincome', function () {
    var tr = $(this).closest("tr");
    if ($(this).is(":checked"))
        tr.find(".edit-view .annualamount").removeClass("d-none");
    else {
        tr.find(".edit-view .annualamount").val("").addClass("d-none");
    }
});
$(document).on('click', '.btn-prevesp-delete', function () {
    var cur = this;
    var tr = $(this).closest("tr");
    var swalOptions = confimationSwalOptions;
    Swal.fire(swalOptions)
        .then(function (result) {
            var guid = tr.attr("data-guid");
            if (result.value) {
                deleteRecord(`income/deleteprevesp?guid=${guid}`, function (response) {
                    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
                    if (response.success) {
                        var employmentof = tr.attr("data-employmentof");
                        $(cur).closest("tr").remove();
                        var count = $(`#pre-esp-table-${employmentof} >tbody >tr`).length;
                        if (count == 0)
                            $(`#prev-esp-${employmentof}`).html("");
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                        LoadExplicitVue();
                    }
                    setTimeout(function () { KTApp.unblockPage(); }, 1000);
                });
            }
        });
});