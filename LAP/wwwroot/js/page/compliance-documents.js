﻿var bindComplianceDocumentStatusDropdown = function () {
    $("#status-filter").empty();
    var drpBody = "";
    for (let [key, item] of Object.entries(ComplianceDocumentStatus))
        drpBody += `<option value='${item.Value}' data-content="<i class='fa fa-circle fa-xs text-${item.ColorClass} mr-2'></i>${key}"/>`;
    $("#status-filter").html(drpBody);
}
var bindInsuranceDocumentTypeDropdown = function () {
    $("#insuranceType-filter").empty();
    var drpBody = "";
    for (let [key, item] of Object.entries(InsuranceDocumentTypeStatus))
        drpBody += `<option value='${item.Value}'>${item.Title ? item.Title : key}</option>`;
    $("#insuranceType-filter").html(drpBody);
}
var bindInsuranceDocumentStatusDropdown = function () {
    $("#status-filter").empty();
    var drpBody = "";
    for (let [key, item] of Object.entries(InsuranceDocumentStatus))
        drpBody += `<option value='${item.Value}' data-content="<i class='fa fa-circle fa-xs text-${item.ColorClass} mr-2'></i>${key}"/>`;
    $("#status-filter").html(drpBody);
}
var bindComplianceDropdown = function () {
    $("#reviewedById-filter").select2({
        ajax: {
            url: 'admin/adviser/GetComplianceUsers',
            data: function (params) {
                return { term: params.term, page: params.page || 1 };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 10) < data.totalCount
                    }
                };
            }
        }
    });
}