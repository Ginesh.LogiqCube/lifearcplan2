﻿$("#btn-submit").addClass("btn-next");
$("#wizard-main-div").on("click", ".btn-heir-add,.btn-heir-edit", function () {
    var id = $(this).attr("data-guid");
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    loadPartialView(`personal/heirinfo?guid=${id}`, "HeirModal");
    setTimeout(function () { KTApp.unblockPage(); }, 1000);
    $("#HeirModal").modal({ backdrop: 'static' });
});
$("#wizard-main-div").on("click", ".btn-heir-delete", function () {
    var cur = this;
    var swalOptions = confimationSwalOptions;
    Swal.fire(swalOptions)
        .then(function (result) {
            if (result.value) {
                var guid = $(cur).attr("data-guid");
                deleteRecord(`personal/deleteheir?guid=${guid}`, function () {
                    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
                    $(cur).closest(".heir-info-div").remove();
                    setTimeout(function () { KTApp.unblockPage(); }, 1000);
                    showHideSpecialNeed();
                });
            }
        });
});
$('#SpecialNeedsTrust').change(function (e) {
    var el = e.target;
    if (el != null)
        updatesinglevalue(el.dataset.targetTable, el.name, $(this).prop('checked'));
});
var showHideSpecialNeed = function () {
    $(".specialneed").addClass("d-none");
    $(".heirspecialneed").each(function (i, item) {
        if (item.innerHTML.trim() == "Yes")
            $(".specialneed").removeClass("d-none");
    });
}
var onFormCompleteHeir = function (response) {
    var id = $(this).find(":submit").attr('id');
    id = id == undefined ? $(frm).find(":submit").attr('id') : id;
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    frm_id = frm_id == undefined ? $(frm).attr('id') : frm_id;
    var responseObj = JSON.parse(response.responseText);
    var heirinfo = responseObj.data.heirInfo;
    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            var div = $(`#heirinfo-${heirinfo.guid}`)
            if (div.length > 0)
                $(`#heirinfo-${heirinfo.guid}`).html($("#heir-template").tmpl(responseObj.data));
            else {
                responseObj.data.action = "add";
                $("#heir-main-div").append($("#heir-template").tmpl(responseObj.data));
            }
            toastr.clear();
            toastr.options = toastrOptions;
            toastr.success(responseObj.message);
            showHideSpecialNeed();
        }
        else
            _showFormMessage(`${frm_id}`, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage(`${frm_id}`, "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
};