﻿$('#DateOfCurrentMarriage').on('change input', function (e) {
    var date = e.target.value
    showHideSpouseInfo(date);
});
var showHide = function () {
    var maritalStatus = $("#ClientMaritalStatus").val();
    if (maritalStatus == MaritalStatus.MARRIED || maritalStatus == MaritalStatus.SEPARATED) {
        $(".d").addClass("d-none");
        $(".n").addClass("d-none");
        $("#XSpouse").addClass("d-none");
        $(".m").removeClass("d-none");
    }
    else if (maritalStatus == MaritalStatus.DIVORCED || maritalStatus == MaritalStatus.WIDOWED) {
        $(".m").addClass("d-none");
        $(".n").addClass("d-none");
        $(".d").removeClass("d-none");

        if ($("#CurrentMarriedTenYears").is(":checked"))
            $("#XSpouse").removeClass("d-none");
        else
            $("#XSpouse").addClass("d-none");

        $("#CurrentMarriedTenYears").change(function () {
            if ($(this).is(":checked"))
                $("#XSpouse").removeClass("d-none");
            else
                $("#XSpouse").addClass("d-none");
        })
    }
    else if (maritalStatus == MaritalStatus.UNMARRIED) {
        $(".m").addClass("d-none");
        $(".d").addClass("d-none");
        $(".n").removeClass("d-none");
    }
}
var showHideSpouseInfo = function (date) {
    if (new Date(date) != "Invalid Date") {
        $(".spouseInfo").removeClass("d-none");
        $("#XSpouse").addClass("d-none");
    }
    else
        $(".spouseInfo").addClass("d-none");
}
var onFormCompleteMaritalStatus = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
            gotoNextPrevAction();
            $("#spouse-riskarc .navi-text").text(`${responseObj.data.spouseFirstName}'s RiskArc`);
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
};