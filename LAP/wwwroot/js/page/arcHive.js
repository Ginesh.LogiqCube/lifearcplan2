﻿localStorage.setItem("adviserLastPageHref", 'user/client-arc-hive');
var documentTable = function (e, elm) {
    var ktDataTablOptions = ktDataTableDefaultConfigOptions;
    ktDataTablOptions.source = `home/fetchdocuments?clientId=${e.data.id}`;
    var el = elm != null ? elm : $('<div/>').attr('id', `client_list_${e.data.id}`).appendTo(e.detailCell);
    var variable = elm != undefined && elm.length > 0 ? 'datatable' : `client_sub_datatable_${e.data.id}`;
    window[variable] = el.KTDatatable({
        data: ktDataTablOptions,
        columnFilter: true,
        columns: [
            { field: 'id', title: '#', width: 85, filter: { maxlength: 5, additionalClasses: "numericOnly-no-placeholder" } },
            { field: 'fileName', title: 'File Name', width: 500, filter: {} },
            {
                field: 'extension', title: 'File Type', sortable: false, sortable: false, textAlign: 'center', width: 120, filter: { filterType: "select", container: "fileType", size: 5 },
                template: function (row) {
                    var extension = row.extension;
                    row.fileName = row.fileName.replace("'", "/");
                    var type = extension.includes("doc") ? "word" : extension.includes("xls") ? "excel" : extension.includes("csv") ? "csv" : extension.includes("ppt") ? "powerpoint" : extension.includes("pdf") ? "pdf" : row.contentType.includes("image") ? "image" : row.contentType.includes("audio") ? "audio" : row.contentType.includes("video") ? "video" : row.contentType.includes("text") ? "alt" : "";
                    var color = extension.includes("doc") ? "295497" : extension.includes("xls") || extension.includes("csv") ? "1f7244" : extension.includes("ppt") ? "d14424" : extension.includes("pdf") ? "cb0606" : row.contentType.includes("image") ? "26b99a" : row.contentType.includes("audio") ? "8950FC" : row.contentType.includes("video") ? "F64E60" : row.contentType.includes("text") ? "3699FF" : "008080";
                    var knownHtml = `<i class="fas fa-file${type.length > 0 ? "-" : ""}${type}" style="color:#${color};font-size: 30px;" data-toggle="tooltip" data-title="${extension.toUpperCase()}" id="icon-${row.id}"></i>`;
                    var unknownHtml = `<span class="fa-stack fa-1x" style="font-size:1.2em !important" data-toggle="tooltip" data-title="${extension.toUpperCase()}" id="icon-${row.id}">
                                                                                                                                                                                                                                        <i class="fa fa-file fa-stack-2x" style="color:#${color}"></i>
                                                                                                                                                                                                                                          <span class="fa fa-stack-1x">
                                                                                                                                                                                                                                              <span style="font-size:10px;color:white;font-family: 'Poppins';font-weight: normal;">
                                                                                                                                                                                                                                                  ${extension.toUpperCase()}
                                                                                                                                                                                                                                              </span>
                                                                                                                                                                                                                                        </span>
                                                                                                                                                                                                                                    </span>	`;
                    return `<a href="javascript:" onclick="clientFileDownload('documents/client', '${row.fileName}', '${row.versionId}',${row.id})">
                                                                                                                                                                                                                            ${type.length > 0 ? knownHtml : unknownHtml}
                                                                                                                                                                                                                            <i class="fas fa-arrow-down animated faa-falling ml-2 text-primary d-none" id="loader-${row.id}"></i>
                                                                                                                                                                                                                        </a>`;
                }
            },
            {
                field: 'fileSize', title: 'File Size', width: 80,
                template: function (row) {
                    return `${humanFileSize(row.fileSize)}`
                }
            },
            {
                field: 'createdDate', title: 'Uploaded Date', sortable: 'desc', width: 130,
                template: function (row) {
                    return getDataTableDateWithHtml(row.createdDate)
                },
            },
            {
                field: '', title: 'Actions', sortable: false, textAlign: 'center', width: 100,
                template: function (row) {
                    var action = !row.isDeleted ? "archive" : "restore";
                    return `<a href="javascript:;" class="btn btn-sm btn-icon" data-toggle="tooltip" title="Click here to ${action}" onclick="archiveFile('${row.guid}','${row.clientId}')">
                                        <i id="archive-${row.guid}" class="fas fas fa-archive text-primary icon-lg"></i>
                                        <span id="spinner-${row.guid}" class="spinner spinner-primary d-none" style="left: -20px;"></span>
                                    </a>`
                }
            },
        ],
    })
}
var adminId = $("#hdnAdminId").val();
var ownerUserId = $("#hdnOwnerUserId").val();
var adviserId = ownerUserId > 0 ? ownerUserId : $("#hdnAdviserId").val();
var clientId = $("#hdnClientId").val();
if (!(adminId > 0 || adviserId > 0)) {
    var el = $('#client_list');
    var e = {
        data: {
            id: clientId
        }
    };
    documentTable(e, el);
}
else {
    var ktDataTablOptions2 = ktDataTableDefaultConfigOptions;
    ktDataTablOptions2.source = `adviser/home/fetchclientswithcount?userId=${adviserId}`;

    window['datatable'] = $('#client_list').KTDatatable({
        data: ktDataTablOptions2,
        columnFilter: true,
        columns: [
            { field: 'id', title: '', sortable: false, width: 0 },
            {
                field: 'name', title: 'Client Name', sortable: 'asc', filter: { container: "ClientName" },
                template: function (row) {
                    return `<div class="font-size-lg">${row.name}</div>`
                }
            },
            {
                field: 'username', title: 'Username', filter: {},
                template: function (row) {
                    return `<div class="font-size-lg">${row.username}</div>`
                }
            },
            {
                field: 'total', title: '', textAlign: 'right', sortable: false, width: 50,
                template: function (row) {
                    return `<span class="label label-lg label-info" data-id=${row.id}>${row.totalDocument}</span>`
                }
            },
            {
                field: '', title: 'Actions', sortable: false, textAlign: 'center', width: 50,
                template: function (row) {
                    return `<span data-toggle="tooltip" data-original-title="Click here to upload" role="button" onclick="$('#ArcUploadModal').modal('show');$('#hdnClientId').val('${row.id}')">
                                                                                                                                                                                                                                            <i class="fas fa-upload text-primary icon-lg"></i>
                                                                                                                                                                                                                                        </span>`
                }
            },
        ],
        detail: { content: documentTable }
    })
}
$('#client_list').on('datatable-on-init', function (e, data) {
    bindfileTypeDropdown();
});
var archiveFile = function (guid, clientId) {
    $(`#archive-${guid}`).addClass("d-none");
    $(`#spinner-${guid}`).removeClass("d-none");
    $.post('home/archivefile', { guid: guid }, function (response) {
        if (adviserId > 0) {
            if (window['client_sub_datatable_' + clientId] != undefined)
                window['client_sub_datatable_' + clientId].reload();
        }
        else
            window['datatable'] && window['datatable'].reload();
        if (response) {
            toastr.options = toastrOptions;
            toastr.success(response.message);
        }
    })
}