﻿var flag = false;
$("#btn-back").addClass("invisible");
$("#MaritalStatus").change(function (e) {
    var maritalStatus = Number(e.target.value);
    if (maritalStatus == MaritalStatus.DIVORCED || maritalStatus == MaritalStatus.WIDOWED) {
        setTimeout(function () {
            var msg = "Spouse data will be lost if the marital status is changed to <b>Divorced</b> or <b>Widowed</b>.";
            _showFormMessage("kt_clientinfo_form", "warning", msg, "fas fa-exclamation-triangle")
        }, 500);
    }
    $.get(`personal/gettaxfilingtype?maritalStatus=${maritalStatus}`, function (response) {
        var html = "";
        $.each(response, function (i, item) {
            var s = item.selected == true ? "selected" : "";
            html += `<option value="${item.value}" ${s}>${item.text}</option>`
        });
        $("#TaxFilingType").html(html);
        setSelectedOptionOne("TaxFilingType", response.length);
    });
    
    $("#ClientMaritalStatus").val(maritalStatus);
    hideUnhideWizardItem();
    var nextText = maritalStatus != MaritalStatus.MARRIED && maritalStatus != MaritalStatus.SEPARATED ? "Contact Info" : "Marital Info";
    var nextAction = maritalStatus != MaritalStatus.MARRIED && maritalStatus != MaritalStatus.SEPARATED ? "contact-info" : "marital-info";
    $("#btn-next,#btn-submit").attr("data-original-title", `Save & Next to ${nextText}`).attr("data-next-action", `personal/${nextAction}`);
});
$('#IsMilitary').change(function () {
    if ($(this).is(':checked'))
        $('#clientMilitaryModal').modal({ backdrop: 'static' });
});

$("#btn-submit").click(function (e) {
    e.preventDefault();
    var isuscitizen = $("#IsCitizen").is(":checked");
    if (!flag && !isuscitizen)
        $('#clientUsCitizenModal').modal({ backdrop: 'static' });
    else
        $("#kt_clientinfo_form").submit();
});
$("#IsPermanantCitizen").change(function () {
    var val = $(this).is(":checked");
    if (val) {
        var msg = "Due to Compliance Issues with the General Data Protection Regulation (GDPR) of the European Union, LifeArcPlan™ is not available to any person who is a citizen or permanent resident of a member state within the European Union. We regret to inform you that you may not proceed to complete LifeArcPlan™.";
        _showFormMessage("msg-citizen", "danger", msg, "fas fa-exclamation-triangle");
        $("#btn-citizen-submit").addClass("d-none");
        $("#btn-citizen-close").removeClass("d-none");
    }
    else {
        $(".alert").remove();
        $("#btn-citizen-submit").removeClass("d-none");
        $("#btn-citizen-close").addClass("d-none");
    }
});
$("#btn-citizen-submit").click(function (e) {
    e.preventDefault();
    var ispermanantcitizen = $("#IsPermanantCitizen").is(":checked");
    if (!ispermanantcitizen) {
        flag = true;
        updatesinglevalue("ClientData", "ispermanantcitizen", ispermanantcitizen);
        $('#clientUsCitizenModal').modal('hide');
        $("#btn-submit").click();
    }
});

$("#DateOfBirth").on('change input', function (e) {
    $("#ClientDOB").val(e.target.value);
});
$('#ActiveService').change(function (e) {
    var el = e.target;
    if (el != null)
        updatesinglevalue(el.dataset.targetTable, el.name, $(this).prop('checked'));
});

$('#Discharge').change(function (e) {
    var el = e.target;
    if (el != null)
        updatesinglevalue(el.dataset.targetTable, el.name, $(this).prop('checked'));
});
var onFormCompleteClientInfo = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
            gotoNextPrevAction();
            var fullname = `${responseObj.data.clientFirstName} ${responseObj.data.clientLastName}`
            $(".client_fullname").text(fullname);
            $("#client-riskarc .navi-text").text(`${responseObj.data.clientFirstName}'s RiskArc`);
            $(".client_initial").text(responseObj.data.clientFirstName.charAt(0));
            LoadExplicitVue();
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};