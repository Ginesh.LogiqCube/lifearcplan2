﻿$("#btn-submit").addClass("btn-next");
$("#wizard-main-div").on("click", ".btn-property-add,.btn-property-edit", function () {
    var id = $(this).attr("data-guid");
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    loadPartialView(`personal/propertyinfo?guid=${id}`, "PropertyModal");
});
$("#wizard-main-div").on("click", ".btn-property-delete", function () {
    var cur = this;
    var swalOptions = confimationSwalOptions;
    Swal.fire(swalOptions)
        .then(function (result) {
            if (result.value) {
                var guid = $(cur).attr("data-guid");
                deleteRecord(`personal/deleteproperty?guid=${guid}`, function (response) {
                    var hasPersonalAssets = response.data.hasPersonalAssets == true ? 1 : 0
                    $("#HasPersonalAssets").val(hasPersonalAssets);
                    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
                    $(cur).closest(".property-info-div").remove();
                    setTimeout(function () { KTApp.unblockPage(); }, 1000);
                    LoadExplicitVue();
                });
            }
        });
});
$(document).on('change', "#Incomegenerated", function () {
    var chk = $(this).is(":checked");
    showHideIncomeYear(chk, "incomeYears")
});
var onFormCompleteProperty = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    var propertyinfo = responseObj.data.propertyInfo;
    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            var div = $(`#property-card-${propertyinfo.guid}`)
            if (div.length > 0)
                $(`#property-card-${propertyinfo.guid}`).html($("#property-template").tmpl(responseObj.data));
            else {
                responseObj.data.action = "add";
                $("#property-main-div").prepend($("#property-template").tmpl(responseObj.data));
            }
            var hasPersonalAssets = responseObj.data.hasPersonalAssets == true ? 1 : 0
            $("#HasPersonalAssets").val(hasPersonalAssets);
            toastr.clear();
            toastr.options = toastrOptions;
            toastr.success(responseObj.message);
            LoadExplicitVue();
        }
        else
            _showFormMessage(`${frm_id}`, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage(`${frm_id}`, "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
};
var showHideIncomeYear = function (incomeYears, target) {
    if (incomeYears) {
        $(`.${target}`).removeClass("d-none");
        $("#Incomegenerated").attr("checked", true)
    }
    else {
        $(`.${target} .form-control`).val("");
        $(`.${target}`).addClass("d-none");
        $("#AnnualIncome").val("");
        $("#Years").val("");
    }
}