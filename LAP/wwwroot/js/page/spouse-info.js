﻿var flag = false;
$('#IsMilitary').change(function () {
    if ($(this).is(':checked'))
        $('#clientMilitaryModal').modal({ backdrop: 'static' });
});
$("#btn-submit").click(function (e) {
    e.preventDefault();
    var isuscitizen = $("#IsCitizen").is(":checked");
    if (!flag && !isuscitizen)
        $('#clientUsCitizenModal').modal({ backdrop: 'static' });
    else
        $("#kt_clientinfo_form").submit();
});
$("#IsPermanantCitizen").change(function () {
    var val = $(this).is(":checked");
    if (val) {
        var msg = "Due to Compliance Issues with the General Data Protection Regulation (GDPR) of the European Union, LifeArcPlan™ is not available to any person who is a citizen or permanent resident of a member state within the European Union. We regret to inform you that you may not proceed to complete LifeArcPlan™.";
        _showFormMessage("msg-citizen", "danger", msg, "fas fa-exclamation-triangle");
        $("#btn-citizen-submit").addClass("d-none");
        $("#btn-citizen-close").removeClass("d-none");
    }
    else {
        $(".alert").remove();
        $("#btn-citizen-submit").removeClass("d-none");
        $("#btn-citizen-close").addClass("d-none");
    }
});
$("#btn-citizen-submit").click(function (e) {
    e.preventDefault();
    var ispermanantcitizen = $("#IsPermanantCitizen").is(":checked");
    if (!ispermanantcitizen) {
        flag = true;
        updatesinglevalue("ClientData", "ispermanantcitizen", ispermanantcitizen);
        $('#clientUsCitizenModal').modal('hide');
        $("#btn-submit").click();
    }
});
$('#ActiveService').change(function (e) {
    var el = e.target;
    if (el != null)
        updatesinglevalue(el.dataset.targetTable, el.name, $(this).prop('checked'));
});

$('#Discharge').change(function (e) {
    var el = e.target;
    if (el != null)
        updatesinglevalue(el.dataset.targetTable, el.name, $(this).prop('checked'));
})
var onFormCompleteSpouseInfo = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
            gotoNextPrevAction();
            LoadExplicitVue();
            $("#spouse-riskarc .navi-text").text(`${responseObj.data.spouseFirstName}'s RiskArc`);
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
};