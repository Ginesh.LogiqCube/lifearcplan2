﻿var firmId = $("#hdnFirmId").val();
var ownerUserId = $("#hdnOwnerUserId").val();
var adviserId = ownerUserId > 0 ? ownerUserId : $("#hdnAdviserId").val();
var clientListColumns = [
    { field: 'id', title: '#', width: 85, filter: { maxlength: 5, additionalClasses: "numericOnly-no-placeholder" } },
    {
        field: 'clientData.name', title: 'Client Name/Email', sortable: 'asc', width: 400, filter: { container: "ClientName" },
        template: function (row) {
            var isSetupCompleted = row.setupCompletedDate != null && new Date(row.setupCompletedDate).getFullYear() != 1;
            var isClientDOBSetupCompleted = row.clientData.dateOfBirth != null && new Date(row.clientData.dateOfBirth).getFullYear() != 1;
            var ocupations = row.clientOccupations
            var clientOcupations = ocupations.filter(m => m.employmentOf == ClientType.CLIENT);
            var isMarried = row.clientData.dateOfCurrentMarriage != null && new Date(row.clientData.dateOfCurrentMarriage).getFullYear() != 1 && row.clientData.maritalStatus == MaritalStatus.MARRIED;
            var isSpouseDOBSetupCompleted = row.spouseData != null && row.spouseData.dateOfBirth != null && new Date(row.spouseData.dateOfBirth).getFullYear() != 1;
            var isCompletedProfile = row.initialProfileSetupCompleted && isSetupCompleted && row.clientData.surveyCompleted && isClientDOBSetupCompleted && clientOcupations.length > 0;
            if (isMarried)
                isCompletedProfile = isCompletedProfile && isSpouseDOBSetupCompleted;
            var initial = [row.clientData.firstName, row.clientData.lastName].map((n) => n[0]).join("").substring(0, 2);
            var color = getRandomColor();
            var tooltipHtml = "Pending Items To Complete";
            tooltipHtml += "<ul>";
            if (!isSetupCompleted)
                tooltipHtml += `<li>Complete the Password setup process</li>`;
            if (!row.initialProfileSetupCompleted)
                tooltipHtml += `<li>Mention at least one Planning Area</li>`;
            if (!isClientDOBSetupCompleted)
                tooltipHtml += `<li>Mention Client's Date of Birth</li>`;
            if (isMarried && !isSpouseDOBSetupCompleted)
                tooltipHtml += `<li>Mention Spouse's Date of Birth</li>`;
            if (row.clientOccupations) {
                var spouseOcupations = ocupations.filter(m => m.employmentOf == ClientType.SPOUSE);
                if (clientOcupations == 0)
                    tooltipHtml += `<li>Mention at least one Client's employment (if not Retired or Homemaker)</li>`;
                if (isMarried && spouseOcupations == 0)
                    tooltipHtml += `<li>Mention at least one Spouse's employment (if not Retired or Homemaker)</li>`;
            }
            if (!row.clientData.surveyCompleted)
                tooltipHtml += `<li>Complete the survey</li>`;
            var warningHtml = !isCompletedProfile ?
                `<span class="fa-stack pulse pulse-warning" data-toggle="tooltip" data-html="true" data-original-title="${tooltipHtml}" data-placement="right" role="button">
                            <i class="fas fa-play fa-stack-1x text-dark icon-nm" style="transform: rotate(30deg);left:1px;top:3px"></i>
                            <i class="fas fa-exclamation-triangle fa-stack-1x text-warning"></i>
                            <span class="pulse-ring" style="margin-left:-4px;margin-top:-5px"></span>
                         </span>` : '';
            var cfsBtnHtml = row.userLogin.integration && row.userLogin.integration.cfsUserId && row.educationPlannings.length > 0 ?
                `<button class="btn btn-sm btn-icon cfs-btn" data-toggle="tooltip" data-original-title="Collegiate Funding Solutions" onclick='$("#CFSStudentModal").modal({ backdrop: "static" });' data-guid="${row.guid}">
                           <i class="icon-xl fas fa-graduation-cap text-dark-65" style="transform: scaleX(-1);"></i>
                         </button>` : "";
            var fusionBtnHtml = row.userLogin.integration && row.userLogin.integration.fusionId ?
                `<button class="btn btn-sm btn-icon" data-toggle="tooltip" data-original-title="Fusion Elements">
                            <img src="/images/fusion-elements.png" height="20">
                         </button>` : "";
            return `<div class="d-flex align-items-center">
                                <div class="symbol symbol-40 flex-shrink-0">
                                <div class="symbol-label text-white" style="background:${color}">${initial}</div>
                            </div>
                            <div class="ml-2">
                                <div class="text-dark-75 font-weight-bold line-height-sm">${row.clientData.name}</div>
                                <div class="text-primary">${row.username}</div>
                            </div>
                            ${warningHtml}
                            ${cfsBtnHtml}
                            ${fusionBtnHtml}
                            </div>`;
        }
    },
    {
        field: 'setupCompletedDate', title: 'Password&nbsp;Setup', sortable: false, width: 110,
        template: function (row) {
            return getDataTableDateWithHtml(row.setupCompletedDate)
        },
    },
    {
        field: 'lastLogin', title: 'Last Login', sortable: false, width: 80,
        template: function (row) {
            return getDataTableDateWithHtml(row.lastLogin)
        }
    },
    {
        field: 'actions', title: 'Actions', width: 160, sortable: false, overflow: 'visible',
        template: function (row) {
            var isSetupCompleted = row.setupCompletedDate != null && new Date(row.setupCompletedDate).getFullYear() != 1;
            var isClientDOBSetupCompleted = row.clientData.dateOfBirth != null && new Date(row.clientData.dateOfBirth).getFullYear() != 1;
            var ocupations = row.clientOccupations
            var clientOcupations = ocupations.filter(m => m.employmentOf == ClientType.CLIENT);
            var isCompletedProfile = isSetupCompleted && isClientDOBSetupCompleted;
            var tz = new Date().getTimezoneOffset();
            var viewProfileHtml = isSetupCompleted ?
                `<a class="ml-1" href="admin/account/clientlogin?guid=${row.guid}" target="_blank" data-toggle="tooltip" data-original-title="View Profile">
                            <img src="/images/view-profile.png" height="30" />
                         </a>` : "";
            var spouseRiskArcHtml = row.clientData.dateOfCurrentMarriage != null && new Date(row.clientData.dateOfCurrentMarriage).getFullYear() != 1 && row.clientData.maritalStatus == MaritalStatus.MARRIED && row.spouseData != null ?
                `<li class="navi-item">
                           <a href="report/risk-arc?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}&clientType=${ClientType.SPOUSE}" data-id="${row.id}" class="navi-link" target="_blank"><span class="navi-text">Spouse's RiskArc</span></a>
                        </li>` : "";
            var resendHtml = !isSetupCompleted ?
                `<button class="btn btn-sm btn-icon resend-invitation-button" data-toggle="tooltip" data-original-title="Resend Invitation" onclick="reinvite(this,${AdminRoleEnum.CLIENT},'${row.guid}')">
                            <img src="/images/email-resend-icon.png" height="25" />
                            <i class="fa fa-redo text-warning icon-sm resend-invitation-icon" aria-hidden="true" style="left:21px;top:29px;"></i>
                         </button>` : "";
            var arcOfLifeHtml = row.initialProfileSetupCompleted && isCompletedProfile && clientOcupations.length > 0 ?
                `<li class="navi-item">
                            <a href="report/arc-of-life?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}" data-id="${row.id}" class="navi-link" target="_blank"><span class="navi-text">Arc Of Life</span></a>
                         </li>
                         <li class="navi-item">
                            <a href="report/balance-sheet?token=${encodeURIComponent(row.encryptedGuid)}" class="navi-link" target="_blank"><span class="navi-text">Balance Sheet</span></a>
                         </li>` : "";
            var reportHtml = isCompletedProfile ?
                `<div class="dropdown dropdown-inline" data-toggle="tooltip" data-original-title="View Reports">
                            <button class="btn btn-sm btn-icon action-menu" data-toggle="dropdown" aria-expanded="false" id="report-icon-${row.id}" style="margin-left: -3px">
                                <i class="fas fa-file-alt icon-2x text-primary mr-0" aria-hidden="true"></i>
                            </button>
                            <span class="fa-stack fa-1x mt-2 d-none" id="loader-icon-${row.id}">
                                <i class="fas fa-box-open fa-stack-2x text-primary icon-xl"></i>
                                <i class="fas fa-arrow-down fa-stack-1x icon-nm text-warning faa-falling animated" style="top:-15px;"></i>
                            </span>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right report-menu">
                                <ul class="navi flex-column navi-hover py-2">
                                    <li class="navi-item">
                                        <a href="report/personal-canvas?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}" data-id="${row.id}" class="navi-link" target="_blank"><span class="navi-text">My Personal Canvas</span></a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="report/risk-arc?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}&clientType=${ClientType.CLIENT}" data-id="${row.id}" class="navi-link" target="_blank"><span class="navi-text">Client's RiskArc</span></a>
                                    </li>
                                    ${spouseRiskArcHtml}
                                    ${arcOfLifeHtml}
                                </ul>
                            </div>
                         </div>`: "";
            return `${resendHtml}
                            ${viewProfileHtml}
                            <a href="engagement-letter?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}" target="_blank" class="ml-2" data-toggle="tooltip" data-original-title="Engagement Letter" data-guid="${row.guid}">
                                <img src="/images/agreement-icon.png" height="27" />
                            </a>
                            ${reportHtml}                           
                            <a class="ml-1" href="client/activity-log?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}" target="_blank" data-toggle="tooltip" data-original-title="Activity Log">
                               <img src="/images/activity-log.png" height="30" />
                            </a>`;
        },
    }
]
var clientTable = function (e, elm) {
    var ktDataTablOptions = ktDataTableDefaultConfigOptions;
    ktDataTablOptions.source = `admin/adviser/fetchclients?userId=${e.data.id}`;
    var el = elm != null ? elm : $('<div/>').attr('id', `adviser_list_${e.data.id}`).appendTo(e.detailCell);
    var variable = elm != undefined && elm.length > 0 ? 'datatable' : `adviser_list_${e.data.id}`;
    window[variable] = el.KTDatatable({
        columnFilter: true,
        data: ktDataTablOptions,
        columns: clientListColumns,
    })
}

if (adviserId > 0) {
    var el = $('#adviser_list');
    var e = {
        data: {
            id: adviserId
        }
    };
    clientTable(e, el);
}
else {
    var ktDataTablOptions2 = ktDataTableDefaultConfigOptions;
    ktDataTablOptions2.source = `admin/adviser/fetchadviserswithcount?firmId=${firmId}`;
    window['datatable'] = $('#adviser_list').KTDatatable({
        columnFilter: true,
        data: ktDataTablOptions2,
        columns: [
            { field: 'id', title: '', sortable: false, width: 0 },
            {
                field: 'name', title: 'Adviser Name', sortable: 'asc', filter: { container: "AdviserName" },
                template: function (row) {
                    return `<div class="font-size-lg">${row.name}</div>`
                }
            },
            {
                field: 'username', title: 'Username', width: 300, filter: {},
                template: function (row) {
                    return `<div class="font-size-lg">${row.username}</div>`
                }
            },
            {
                field: 'totalClient', title: '', textAlign: 'right', sortable: false,
                template: function (row) {
                    return `<span class="label label-lg label-danger">${row.totalClient}</span>`
                }
            },
        ],
        detail: { content: clientTable }
    });
}

var onFormCompleteCFSSubmit = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
            openFileOrLinkNewWindow(`${window.location.origin}/report/cfs/${responseObj.data.cfsStudentId}`)
        }
        else
            _showFormMessage(`${frm_id}`, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage(`${frm_id}`, "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
};

var bindSearchClientList = function () {
    if ($('#client-list').html() != '')
        window['datatable-client-list'].destroy();
    toggleDiv(true);
    var ktDataTablOptions2 = ktDataTableDefaultConfigOptions;
    ktDataTablOptions2.source = {
        read: {
            url: `admin/adviser/fetchclients`,
            params: {
                query:
                {
                    ClientName: $("#Search-Filter").val(),
                }
            }
        }
    };

    window["datatable-client-list"] = $('#client-list').KTDatatable({
        data: ktDataTablOptions2,
        columns: clientListColumns,
    })
}

var enableDisableSerachBtn = function () {
    var searchText = $("#Search-Filter").val();
    $("#btn-search").prop("disabled", true);
    if (searchText && searchText.length)
        $("#btn-search").prop("disabled", false);
}

var toggleDiv = function (fromFilter) {
    $("#firm-client-list").addClass("d-none")
    $("#filtered-client-list").addClass("d-none");
    if (fromFilter)
        $("#filtered-client-list").removeClass("d-none");
    else
        $("#firm-client-list").removeClass("d-none")
    setTimeout(function () { enableDisableSerachBtn(); }, 0);
}

var clearFilter = function () {
    toggleDiv();
    window['datatable'].reload();
    $('#Search-Filter').val('');
}

enableDisableSerachBtn();

$(document).on("click", ".btn-client-add,.btn-client-edit", function () {
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    var guid = $(this).attr("data-guid");
    loadPartialView(`adviser/home/addclient?guid=${guid}`, "ClientModal");
    setTimeout(function () { KTApp.unblockPage(); }, 1000);
    $("#ClientModal").modal({ backdrop: 'static' });
});

$(document).on("click", ".cfs-btn", function () {
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    var guid = $(this).attr("data-guid");
    loadPartialView(`adviser/home/CFSStudents?guid=${guid}`, "CFSStudentModal");
    setTimeout(function () { KTApp.unblockPage(); }, 1000);
    $("#CFSStudentModal").modal({ backdrop: 'static' });
});

$(document).on("click", ".btn-cfs-student", function () {
    $("#cfs-frm").html("");
    $(this).removeClass("btn-light-primary").addClass("btn-primary");
    $(".btn-cfs-student").not(this).removeClass("btn-primary").addClass("btn-light-primary");
    var guid = $(this).attr("data-guid");
    loadPartialView(`adviser/home/cfsform?StudentGuid=${guid}`, "cfs-frm", "cfs-frm", function () {
        $("#btn_submit_cfs_student").prop("disabled", false);
    });
});

$(document).on("click", "#btn-search", function () {
    bindSearchClientList();
    $(`#FirmAdviserAdvanceFilterModal`).modal('hide');
    $("#btn-nav-back").removeClass("d-none");
    $(".nav-item .nav-link").eq(1).click();
});

$("#Search-Filter").on("input", function (e) {
    if (e.target.value.length)
        enableDisableSerachBtn();
    else
        clearFilter();
});

$(document).on("click", "#client-filter-container [data-allow-clear='true'] + .close", function (e) {
    clearFilter();
});