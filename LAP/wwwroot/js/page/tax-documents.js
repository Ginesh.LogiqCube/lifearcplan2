﻿localStorage.setItem("adviserLastPageHref", 'user/tax-documents');
var clientId = $("#hdnClientId").val();

var opnUploadModal = function (e, CID) {
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    var guid = $(e).attr("data-guid");
    loadPartialView(`home/taxdocumentmodal?guid=${guid}`, "TaxDocumentUploadModal", null, function () {
        $('#tax-document-upload-form #ClientId').val(CID);
        var year = clientId > 0 ? $("#Year-Filter").val() : $(`#year-filter-${CID}`).text().trim();
        $('#tax-document-upload-form #Year').val(year);
        $('#tax-document-upload-form #Year').prop("disabled", true);
        KTApp.unblockPage();
        $("#TaxDocumentUploadModal").modal({ backdrop: 'static' });
    });
}
$(document).on("click", ".btn-upload-document", function () {
    opnUploadModal(this, clientId);
});

var documentTable = function (e, elm) {
    var firmId = $("#hdnFirmId").val();
    var ktDataTablOptions = ktDataTableDefaultConfigOptions;
    var CID = e.data.id;
    var year = clientId > 0 ? $("#Year-Filter").val() : $(`#year-filter-${CID}`).text().trim();
    ktDataTablOptions.source = `home/fetchtaxdocuments?firmId=${firmId}&clientId=${CID}&year=${year}`;
    var el = elm != null ? elm : $('<div/>').attr('id', `client_list_${CID}`).appendTo(e.detailCell);
    var variable = elm != undefined && elm.length > 0 ? 'datatable' : `client_sub_datatable_${CID}`;
    window[variable] = el.KTDatatable({
        data: ktDataTablOptions,
        columnFilter: true,
        columns: [
            { field: 'id', title: '#', width: 85, filter: { maxlength: 5, additionalClasses: "numericOnly-no-placeholder" } },
            { field: 'fileName', title: 'File Name', width: 500, filter: {} },
            {
                field: 'extension', title: 'File Type', sortable: false,  textAlign: 'center', width: 120, sortable: false,
                template: function (row) {
                    row.fileName = row.fileName.replace("'", "/");
                    return `<a href="javascript:" onclick="clientFileDownload('documents/tax/${row.year}', '${row.fileName}', '${row.versionId}',${row.id})">
                                    <i class="fas fa-file-pdf" style="color:#cb0606;font-size: 30px;" data-toggle="tooltip" data-title="PDF" id="icon-${row.id}" data-original-title=""></i>
                                    <i class="fas fa-arrow-down animated faa-falling ml-2 text-primary d-none" id="loader-${row.id}"></i>
                            </a>`;
                }
            },
            {
                field: 'fileSize', title: 'File Size', width: 80,
                template: function (row) {
                    return `${humanFileSize(row.fileSize)}`
                }
            },
            {
                field: 'createdDate', title: 'Uploaded Date', sortable: 'desc', width: 130,
                template: function (row) {
                    return getDataTableDateWithHtml(row.createdDate)
                },
            },
        ],
    })

    el.on('datatable-on-layout-updated', function (e) {
        var total = window[variable].getTotalRows();
        $(`.datatable-row .doc-count[data-id="${CID}"]`).text(total);
        $(`#status-${CID}`).addClass("d-none");
        if (total) $(`#status-${CID}`).removeClass("d-none");
        var statusData = window[variable].lastResponse.meta.additionalData;
        var el2 = !clientId ? $(`#status-btn-${CID}`) : $(`#status-btn`);
        el2.addClass("d-none");
        if (statusData.taxPrepStatusString && total) {
            el2.text(statusData.taxPrepStatusString);
            el2.removeClass("d-none");
        }
    });
}

var adminId = $("#hdnAdminId").val();
var ownerUserId = $("#hdnOwnerUserId").val();
var adviserId = ownerUserId > 0 ? ownerUserId : $("#hdnAdviserId").val();

if (!(adminId > 0 || adviserId > 0)) {
    var el = $('#client_list');
    var e = {
        data: {
            id: clientId
        }
    };
    documentTable(e, el);
}
else {
    var ktDataTablOptions2 = ktDataTableDefaultConfigOptions;
    var lastYear = new Date().getFullYear() - 1;
    ktDataTablOptions2.source = `adviser/home/FetchTaxDocumentClientsWithCount?userId=${adviserId}&year=${lastYear}`;

    window['datatable'] = $('#client_list').KTDatatable({
        data: ktDataTablOptions2,
        columnFilter: true,
        columns: [
            { field: 'id', title: '', sortable: false, width: 0 },
            {
                field: 'name', title: 'Client Name', width: 200, sortable: 'asc', filter: { container: "ClientName" },
                template: function (row) {
                    return `<div class="font-size-lg">${row.name}</div>`
                }
            },
            {
                field: 'taxPrepStatus', title: 'Tax Prep Status', width: 300, sortable: false, filter: { filterType: "select" },
                template: function (row) {
                    var drpBody = "";
                    for (let [key, item] of Object.entries(TaxDocumentStatus)) {
                        if (item.Value > 0) {
                            var activeClass = row.taxPrepStatusString == item.Title ? "active" : "";
                            drpBody += `<span class="dropdown-item ${activeClass}" data-value="${item.Value}">${item.Title}</span>`;
                        }
                    }
                    row.taxPrepStatusString = row.taxPrepStatusString ? row.taxPrepStatusString : "No Status";
                    var cls = !row.totalTaxDocument ? "d-none" : "";
                    return `<div class="btn-group ${cls}" role="group" id="status-${row.id}">
                                        <button id="status-btn-${row.id}" type="button" class="btn btn-sm dropdown-toggle border-dark status-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    ${row.taxPrepStatusString}
                                </button>
                                <div class="dropdown-menu dropdown-menu-status" aria-labelledby="status-btn-${row.id}">
                                    ${drpBody}
                                </div>
                            </div>`
                }
            },
            {
                field: 'adviserName', title: 'Adviser Name', width: 200, sortable: 'asc', filter: { filterType: "select", additionalClasses: "select2", container: "UserId" },
                template: function (row) {
                    return `<div class="font-size-lg">${row.adviserName}</div>`
                }
            },
            {
                field: 'year', title: 'Tax Year', width: 80, sortable: false,
                template: function (row) {
                    var y = new Date().getFullYear();
                    var drpBody = "";
                    for (let i = 1; i <= 3; i++) {
                        var activeClass = lastYear == y - i ? "active" : "";
                        drpBody += `<span class="dropdown-item ${activeClass}" data-value="${y - i}">${y - i}</span>`;
                    }

                    return `<div class="btn-group" role="group"  data-id="${row.id}">
                                <button id="year-filter-${row.id}" type="button" class="text-left year-filter btn btn-sm dropdown-toggle border-dark" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ${y - 1}
                                </button>
                                <div class="dropdown-menu dropdown-menu-year" aria-labelledby="year-filter-${row.id}">
                                    ${drpBody}
                                </div>
                            </div>`
                }
            },
            {
                field: 'total', title: '# of Docs', textAlign: 'center', sortable: false, width: 70,
                template: function (row) {
                    return `<span class="label label-lg label-info doc-count" data-id=${row.id}>${row.totalTaxDocument}</span>`
                }
            },
            {
                field: '', title: 'Actions', sortable: false, textAlign: 'center', width: 80,
                template: function (row) {
                    var tz = new Date().getTimezoneOffset();
                    return `<span data-toggle="tooltip" data-original-title="Click here to upload" role="button" onclick="opnUploadModal(this,${row.id})">
                                <i class="fas fa-upload text-primary icon-lg"></i>
                            </span>
                             <a class="ml-1" href="client/tax-prep-queue-activity-log?token=${encodeURIComponent(row.encryptedGuid)}&tz=${tz}" target="_blank" data-toggle="tooltip" data-original-title="Tax Prep Queue Activity Log">
                               <img src="/images/activity-log.png" height="30" />
                            </a>`
                }
            },
        ],
        detail: { content: documentTable }
    })
}

$("#Year-Filter").change(function (e) {
    window["datatable"].setDataSourceParam('year', e.target.value);
    window["datatable"].load()
});

var bindAdviserDropdown = function () {
    $("#UserId-filter").select2({
        ajax: {
            url: `admin/adviser/GetAdvisers`,
            data: function (params) {
                return { term: params.term, page: params.page || 1 };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 10) < data.totalCount
                    }
                };
            }
        }
    });
};

var bindTaxPrepStatusDropdown = function () {
    var optionsLength = $("#taxPrepStatus-filter option").length;
    if (optionsLength == 0) {
        $("#taxPrepStatus-filter").empty();
        var drpBody = "";
        for (let [key, item] of Object.entries(TaxDocumentStatus))
            drpBody += `<option value='${item.Value}'>${item.Title ? item.Title : key}</option>`;
        $("#taxPrepStatus-filter").html(drpBody);
    }
};

$('#client_list').on('datatable-on-layout-updated', function (e, data) {
    var el = $('.dropdown-menu-year .dropdown-item');
    el.off('click');
    el.on('click', function () {
        if (!$(this).hasClass("active")) {
            var CID = $(this).parent().attr("aria-labelledby").replace("year-filter-", "");
            var year = $(this).text();
            $(`#year-filter-${CID}`).text(year);
            $(`[aria-labelledby="year-filter-${CID}"] .dropdown-item`).removeClass("active");
            $(`[aria-labelledby="year-filter-${CID}"] .dropdown-item[data-value="${year}"]`).addClass("active");
            var collapse = $(`.datatable-toggle-subtable[data-value="${CID}"] .fa-caret-right`).length;
            if (collapse)
                $(`.datatable-toggle-subtable[data-value="${CID}"]`).click();
            window[`client_sub_datatable_${CID}`].setDataSourceParam('year', year);
            window[`client_sub_datatable_${CID}`].load()
        }
    });
    var el2 = $('.dropdown-menu-status .dropdown-item');
    el2.off('click');
    el2.on('click', function (el) {
        if (!$(this).hasClass("active")) {
            var statusValue = $(this).attr("data-value");
            var statusText = $(this).text();
            var CID = $(this).parent().attr("aria-labelledby").replace("status-btn-", "");
            var year = $(`#year-filter-${CID}`).text();
            $(".status-btn").prop("disabled", true);
            $.post('adviser/home/UpdateTaxPrepStatus', { clientId: CID, status: statusValue, year: year }, function (response) {
                toastr.options = toastrOptions;
                if (response.success) {
                    var swalOptions = successSwalOptions;
                    swalOptions.title = response.message;
                    Swal.fire(swalOptions);
                    $(`#status-btn-${CID}`).text(statusText);
                    $(`[aria-labelledby="status-btn-${CID}"] .dropdown-item`).removeClass("active");
                    $(`[aria-labelledby="status-btn-${CID}"] .dropdown-item[data-value="${statusValue}"]`).addClass("active");
                }
                else
                    toastr.error(response.message);
                $(".status-btn").prop("disabled", false);
            })
        }
    })
});

$('#client_list').on('datatable-on-init', function (e, data) {
    bindAdviserDropdown();
    bindTaxPrepStatusDropdown();
    var elm = $("#client_list");
    var dt = elm.KTDatatable();
    var visibility = adminId > 0 ? true : false;
    window['datatable'].columns('adviserName') && window['datatable'].columns('adviserName').visible(visibility);
    setTimeout(function () {
        $.each(dt.options.columns, function (i, item) {
            elm.find(`tr th:eq(${i}) span:first`).css("width", `${item.width}px`);
            elm.find(`tr td:eq(${i}) span:first`).css("width", `${item.width}px`);
        });
    }, 0);
});