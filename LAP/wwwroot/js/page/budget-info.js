﻿$("#btn-submit").addClass("btn-next");
$(document).on('click', '.btn-budget-edit', function () {
    $(".edit-view").addClass("d-none");
    $(".display-view").removeClass("d-none");
    var tr = $(this).closest("tr");
    tr.find(".edit-view").removeClass("d-none");
    tr.find(".display-view").addClass("d-none");
    tr.find(".currency").removeClass("input-validation-error");
    var amount = tr.find(".display-view.amount").text().replace(/[^0-9.]/gi, '');
    var currencyObj = tr.find(".edit-view").find(".amount");
    currencyObj.val(amount);
    var balanceown = tr.find(".display-view.balanceown").text().replace(/[^0-9.]/gi, '');
    var balanceowncurrencyObj = tr.find(".edit-view").find(".balanceown");
    balanceowncurrencyObj.val(balanceown);
    var yearspayoff = tr.find(".display-view.yearspayoff").text().replace(/[^0-9.]/gi, '');
    var yearspayoffcurrencyObj = tr.find(".edit-view").find(".yearspayoff");
    yearspayoffcurrencyObj.val(yearspayoff);

    var length = currencyObj.val().length * 8;
    var minlength = currencyObj.attr("data-minlength");
    currencyObj.css("width", length > minlength ? length : minlength);
})
$(document).on('click', '.btn-budget-cancel', function () {
    var tr = $(this).closest("tr");
    tr.find(".edit-view").addClass("d-none");
    tr.find(".display-view").removeClass("d-none");
})
$(document).off('click', '.btn-budget-save');
$(document).on('click', '.btn-budget-save', function () {
    if ($(".input-validation-error").length == 0) {
        var tr = $(this).closest("tr");
        tr.find(".btn-budget-save").prop("disabled", true);
        tr.find(".btn-budget-cancel i").addClass("d-none");
        tr.find(".btn-budget-cancel .spinner").removeClass("d-none");
        var guid = tr.attr("data-guid");
        var budgettype = tr.attr("data-budget-type");
        var amount = tr.find(".edit-view .currency").val().replace(/[^0-9]/gi, '');
        var inflationary = tr.find(".edit-view .inflationary").is(":checked");
        var balanceown = tr.find(".edit-view .balanceown").val();
        balanceown = balanceown != undefined ? balanceown.replace(/[^0-9]/gi, '') : 0;
        var yearspayoff = tr.find(".edit-view .yearspayoff").val();
        $.post(`expense/updatebudgetinfo`,
            {
                guid: guid,
                budgettype: budgettype,
                amount, amount,
                inflationary: inflationary,
                balanceown, balanceown,
                yearspayoff: yearspayoff
            }, function (response) {
                if (response) {
                    if (response.success) {
                        tr.find(".btn-budget-save").prop("disabled", false);
                        tr.find(".btn-budget-cancel i").removeClass("d-none");
                        tr.find(".btn-budget-cancel .spinner").addClass("d-none");
                        var responseObj = response.data.clientBudgetInfo;
                        tr.find(".amount").text(responseObj.amountFormated);
                        var inflationaryValue = responseObj.inflationary == true ? "Yes" : "No";
                        tr.find(".display-view.inflationary").text(inflationaryValue);
                        tr.find(".display-view.balanceown").text(responseObj.balanceOwnFormated);
                        tr.find(".display-view.yearspayoff").text(responseObj.yearsPayOff);
                        $(`#total-${responseObj.budgetType}`).text(currencyFormat.format(response.data.total));
                        $(".edit-view").addClass("d-none");
                        $(".display-view").removeClass("d-none");
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                        LoadExplicitVue();
                    }
                    else {
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                    }
                }
                else {
                    toastr.options = toastrOptions;
                    toastr.success("Error Occur");
                }
            })
    }
    else
        Swal.fire("", "Please enter valid amount!", "error");
});
$(document).on('click', '.btn-budget-delete', function () {
    var cur = this;
    var tr = $(this).closest("tr");
    var parent_id = tr.attr("data-budget-type");
    var swalOptions = confimationSwalOptions;
    Swal.fire(swalOptions)
        .then(function (result) {
            var guid = tr.attr("data-guid");
            if (result.value) {
                deleteRecord(`expense/deletebudget?guid=${guid}&parentId=${parent_id}`, function (response) {
                    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
                    if (response.success) {
                        $(cur).closest("tr").remove();
                        $(`#total-${parent_id}`).text(currencyFormat.format(response.data.total));
                        var totalBalanceOwn = response.data.totalBalanceOwn > 0 ? currencyFormat.format(response.data.totalBalanceOwn) : "-";
                        $(`#total-balanceown-${parent_id}`).text(totalBalanceOwn);
                        $(`#total-balanceown-${parent_id}`).removeClass("text-right").removeClass("text-center");
                        if (response.data.isRevolvingDebt == false)
                            $(`#budget-card-${parent_id} .term-revolve`).remove();
                        var side = response.data.totalBalanceOwn > 0 ? "right" : "center";
                        $(`#total-balanceown-${parent_id}`).addClass(`text-${side}`);
                        var count = $(`[data-budget-type=${parent_id}]`).length;
                        if (count == 0)
                            $(`#budget-card-${parent_id}`).parent().remove();
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                        LoadExplicitVue();
                    }
                    setTimeout(function () { KTApp.unblockPage(); }, 1000);
                });
            }
        });
});
$(document).on('click', '.btn-budget-add', function () {
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    loadPartialView(`expense/addbudgetinfo`, "AddBudgetModal");
    KTApp.unblockPage();
    $("#AddBudgetModal").modal({ backdrop: 'static' });
})
$(document).on('change', ".budgetinfo", function (e) {
    _hideErrorMessage();
    var key = $(this).attr("data-key");
    var value = key == "inflationary" ? $(this).is(":checked") : $(this).val().replace(/[^0-9]/gi, '');

    var id = $(this).closest("tr").attr("data-id");
    items.filter(m => m.budgetItemId == id).forEach(function (item) {
        item[key] = value;
    });
});
$(document).on('change', "#BudgetType", function (e) {
    var parentId = e.target.value;
    $.get(`expense/getbudgetitems?parentId=${parentId}`, function (response) {
        items = response;
        $("#table-budget tbody").empty();
        if (items.length > 0) {
            $("#table-budget").removeClass("d-none");
            $(".msg").addClass("d-none");
            var itm = items.filter(m => m.budgetItem.isRevolvingDebt == true);
            if (itm.length == 0) {
                $("#table-budget thead :nth-child(4)").addClass("d-none");
                $("#table-budget thead :nth-child(5)").addClass("d-none");
            }
            else {
                $("#table-budget thead :nth-child(4)").removeClass("d-none");
                $("#table-budget thead :nth-child(5)").removeClass("d-none");
            }
            $("#table-budget tbody").append($("#budget-template").tmpl(items));
            $(".budgetinfo").change(function (e) {
                _hideErrorMessage();
                var key = $(this).attr("data-key");
                var value = key == "inflationary" ? $(this).is(":checked") : $(this).val().replace(/[^0-9]/gi, '');

                var id = $(this).closest("tr").attr("data-id");
                items.filter(m => m.budgetItemId == id).forEach(function (item) {
                    item[key] = value;
                });
            });
        }
        else {
            $("#table-budget").addClass("d-none");
            $(".msg").removeClass("d-none");
        }
    });
});
$("#AssumedInflationRate").change(function (e) {
    var el = e.target;
    if (el != null)
        updatesinglevalue("ClientData", el.name, $(this).val(), function () {
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success("Assumed Inflation Rate is saved successfully");
        });
});
var onFormComplete = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            toastr.options = toastrOptions;
            toastr.success(responseObj.message);
            LoadExplicitVue();
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType");
};
var bindBudgetItem = function (selector, parentId) {
    $.get(`expense/getbudgetcategories?parentId=${parentId}`, function (response) {
        $(`#${selector}`).empty();
        var html = "";
        $.each(response, function (i, item) {
            var s = item.selected == true ? "selected" : "";
            var d = item.disabled == true ? "disabled" : "";
            html += `<option value="${item.value}" ${s} ${d}>${item.text}</option>`
        });
        $(`#${selector}`).html(html);
        $(`#${selector}`).selectpicker('render');
        $(`#${selector}`).selectpicker('refresh');
    });
}