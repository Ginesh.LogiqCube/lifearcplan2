﻿var grid = GridStack.init();
grid.on('added removed change', function (e, items) {
    var data = grid.save();
    $.post('home/updatedashboardconfig', { config: data }, function () {
    });
});
var married = $("#ClientIsMarried").val() == 1;
var estate = $("#HasEstatePlanning").val() == 1;
var hasEstatePlanning = estate ? true : false;
let chartsJsonData = [
    { x: 0, y: 0, w: 4, h: 3, id: 'retirement_assets', title: "Retirement Assets", actionName: "RetirementAssets" },
    { x: 4, y: 0, w: 4, h: 3, id: 'cash_flow', title: "Cash Flow", actionName: "CashFlow" },
    { x: 8, y: 0, w: 4, h: 3, id: 'net_worth', title: "Net Worth", actionName: "NetWorth" },
    { x: 0, y: 3, w: 6, h: 3, id: 'risk_profile', title: "Risk Profile", actionName: "RiskProfile" },
    { x: 6, y: 3, w: 6, h: 2, id: 'goal_tracker', title: "Goal Tracker", actionName: "GoalTracker" },
    { x: 0, y: 6, w: 4, h: 2, id: 'estate', title: "Your Estate", actionName: "Estate", hidden: hasEstatePlanning },
    { x: 0, y: 8, w: 4, h: 2, id: 'tax_prep_status', title: "Your Tax Prep Status", actionName: "TaxPrepStatus" },
    { x: 4, y: 6, w: 8, h: 4, id: 'tax_prep_queue_activity_log', title: "Tax Prep Queue Activity Log", actionName: "TaxPrepQueueActivityLog" },
];

chartsJsonData.forEach((n, i) =>
    n.content = `<div id="${n.id}"></div>`);

function bindWidgetDropdown() {
    $.each(chartsJsonData.filter(m => m.hidden != false), function (index, item) {
        var d = chartsJsonData.filter(m => m.id == item.id && m.active).length;
        var disabled = d ? "disabled" : "";
        $("#drp-widgets").append(`<a id='${item.id}_drp_item' class="dropdown-item ${disabled}" href="javascript:" onclick="addWidget('${item.id}')">${item.title}</a>`);
    });
}
function bindDashboard() {
    $.get('/home/FetchDashboardConfig', function (response) {
        if (response)
            chartsJsonData.filter(m => m.hidden != false).forEach((n, i) => {
                var res = response.filter(m => m.id == n.id)[0];
                if (res) {
                    n.active = true;
                    n.x = res.x;
                    n.y = res.y;
                    n.w = res.w;
                    n.h = res.h;
                }
            });
        bindWidgetDropdown();
        var activeChartsJsonData = chartsJsonData.filter(m => m.active);
        grid.load(activeChartsJsonData, true);
        $.each(activeChartsJsonData, function (index, item) {
            loadChart(item.id);
        })
    });
}
function addWidget(id) {
    $(`#${id}_drp_item`).addClass("disabled");
    var widgetData = chartsJsonData.filter(m => m.id == id)[0];
    var wigetHtml = `<div class="grid-stack-item" gs-id="${widgetData.id}" gs-x="${widgetData.x}" gs-y="${widgetData.y}" gs-w="${widgetData.w}" gs-h="${widgetData.h}">
                        <div class="grid-stack-item-content">${widgetData.content}</div>
                     </div>`;
    grid.addWidget(wigetHtml, 0, 0, widgetData.w, widgetData.h);
    loadChart(widgetData.id);
}
function removeWidget(el, sel) {
    var id = $(sel).attr("data-chart-container");
    $(`#${id}_drp_item`).removeClass("disabled");
    grid.removeWidget(el, true);
    el.remove();
    $('[data-toggle="tooltip"], .tooltip').tooltip("hide");
}

function refreshChart(el) {
    var container = $(el).attr("data-chart-container");
    loadChart(container);
}
function loadChart(id) {
    var res = chartsJsonData.filter(m => m.id == id)[0];
    if (res) {
        KTApp.block(`[gs-id='${res.id}'] .grid-stack-item-content`, {
            state: 'primary',
            message: 'Please wait...'
        });
        loadPartialView(`home/${res.actionName}`, res.id, null, function () {
            KTApp.unblock(`[gs-id='${res.id}'] .grid-stack-item-content`);
        });
    }
}
bindDashboard();