﻿var onFormCompleteOtherIncome = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    responseObj.data.otherIncomeInfo.color = responseObj.data.otherIncomeInfo.businessInterestof == ClientType.CLIENT ? "#b00022" : "#008080";
    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            $("#otherIncome-info-div").removeClass("d-none");
            var div = $("#other-card");
            if (div.length > 0) {
                $("#tblotherincome > tbody").append($("#otherincome-append-template").tmpl(responseObj.data));
                $("#total-property").text(currencyFormat.format(responseObj.data.total));
            }
            else {
                $.get('template/OtherIncome-card-template.html', function (template) {
                    $("#otherIncome-info-div").append($.tmpl(template, responseObj.data));
                });
            }
            toastr.clear();
            toastr.options = toastrOptions;
            toastr.success(responseObj.message);
            LoadExplicitVue();
        }
        else
            _showFormMessage(`${frm_id}`, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage(`${frm_id}`, "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
};
$("#wizard-main-div").on("click", ".btn-OtherIncome-add,.btn-OtherIncome-edit", function () {
    var id = $(this).attr("data-guid");
    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
    loadPartialView(`income/otherincomeInfo?guid=${id}`, "OtherIncmoeModal");
    setTimeout(function () { KTApp.unblockPage(); }, 1000);
    $("#OtherIncmoeModal").modal({ backdrop: 'static' });
});
$(document).on('click', '.btn-other-edit', function () {
    $(".edit-view").addClass("d-none");
    $(".display-view").removeClass("d-none");
    var tr = $(this).closest("tr");
    tr.find(".edit-view").removeClass("d-none");
    tr.find(".display-view").addClass("d-none");
    tr.find(".currency").removeClass("input-validation-error");
    tr.find(".years").removeClass("input-validation-error");
    var amount = tr.find(".display-view.amount").text().trim();;
    var currencyObj = tr.find(".edit-view").find(".amount");
    currencyObj.val(amount);
    var years = tr.find(".display-view.years").text().trim();
    var yearsObj = tr.find(".edit-view").find(".years");
    yearsObj.val(years);
    var length = currencyObj.val().length * 8;
    var minlength = currencyObj.attr("data-minlength");
    currencyObj.css("width", length > minlength ? length : minlength);
});
$(document).on('click', '.btn-other-cancel', function () {
    var tr = $(this).closest("tr");
    tr.find(".edit-view").addClass("d-none");
    tr.find(".display-view").removeClass("d-none");
})
$(document).off('click', '.btn-other-save');
$(document).on('click', '.btn-other-save', function () {
    if ($(".input-validation-error:visible").length == 0) {
        var tr = $(this).closest("tr");
        tr.find(".btn-other-save").prop("disabled", true);
        tr.find(".btn-other-cancel > i").addClass("d-none");
        tr.find(".btn-other-cancel .spinner").removeClass("d-none");
        var guid = tr.attr("data-guid");
        var AnnualIncome = tr.find(".edit-view .amount").val().replace(/[^0-9]/gi, '').replace("$", "");
        var years = tr.find(".edit-view .years").val();
        $.post(`income/UpdateOtherInfo`, {
            guid: guid,
            AnnualIncome: AnnualIncome,
            Years: years
        }, function (response) {
            if (response) {
                if (response.success) {
                    tr.find(".btn-other-save").prop("disabled", false);
                    tr.find(".btn-other-cancel i").removeClass("d-none");
                    tr.find(".btn-other-cancel .spinner").addClass("d-none");
                    var responseObj = response.data.otherIncomeInfo;
                    tr.find(".amount").text(currencyFormat.format(responseObj.annualIncome));
                    tr.find(".display-view.years").text(responseObj.years);
                    $(".edit-view").addClass("d-none");
                    $(".display-view").removeClass("d-none");
                    $("#total-property").text(currencyFormat.format(response.data.total));
                    toastr.options = toastrOptions;
                    toastr.success(response.message);
                    LoadExplicitVue();
                }
                else {
                    toastr.options = toastrOptions;
                    toastr.success(response.message);
                }
            }
            else {
                toastr.options = toastrOptions;
                toastr.success("Error Occur");
            }
        })
    }
    else
        Swal.fire("", "Please enter valid data in highlighted field!", "error");
});
$(document).on('click', '.btn-other-delete', function () {
    var cur = this;
    var tr = $(this).closest("tr");
    var swalOptions = confimationSwalOptions;
    Swal.fire(swalOptions)
        .then(function (result) {
            var guid = tr.attr("data-guid");
            if (result.value) {
                deleteRecord(`income/DeleteOtherIncome?guid=${guid}`, function (response) {
                    KTApp.blockPage({ overlayColor: '#000000', state: 'primary', message: 'Please wait...' });
                    if (response.success) {
                        $(cur).closest("tr").remove();
                        $("#total-property").text(currencyFormat.format(response.data.total));
                        if ($('#tblotherincome > tbody > tr').length == 0) {
                            $("#otherIncome-info-div").addClass("d-none");
                        }
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                        LoadExplicitVue();
                    }
                    setTimeout(function () { KTApp.unblockPage(); }, 1000);
                });
            }
        });
});