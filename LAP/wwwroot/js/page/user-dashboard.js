﻿localStorage.setItem("adviserLastPageHref", 'user/dashboard');
let page = 1;
let lastPage = 0;
let loading = false;
let appointmentChartOptions = {
    chart: {
        type: 'bar',
    },
    plotOptions: {
        bar: {
            columnWidth: '50%',
        },
    },
    dataLabels: {
        enabled: false
    },
};
let grid = GridStack.init();
grid.on('added removed change', function (e, items) {
    var data = grid.save();
    $.post('adviser/dashboard/updatedashboardconfig', { config: data }, function () {
    });
});
let chartsJsonData = [
    { x: 0, y: 0, w: 6, h: 5, id: 'event_chart', title: "Event Chart", functionName: "bindEventChart" },
    { x: 6, y: 0, w: 6, h: 5, id: 'client_appointment_chart', title: "Client Appointment Chart", functionName: "bindClientAppointmentChart" },
    { x: 0, y: 6, w: 6, h: 5, id: 'prospect_appointment_chart', title: "Prospect Appointment Chart", functionName: "bindProspectAppointmentChart" },
    { x: 0, y: 10, w: 7, h: 4, id: 'tax_prep_queue_activity_log', title: "Tax Prep Status", functionName: "bindActivityLog" },
];
chartsJsonData.forEach((n, i) =>
    n.content = `<div class="card card-custom">
                    <div class="card-header border-0">
                        <div class="card-title">
                            <h3 class="card-label text-primary font-size-h3">
                                ${n.title}
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <button class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-toggle="tooltip" data-original-title="Reload Card" data-chart-container="${n.id}" data-variable="${n.id}_data" data-modal-title="${n.title}" onclick="refreshChart(this)">
                                <i class="fas fa-sync-alt icon-md text-primary"></i>
                            </button>
                            <button class="btn btn-icon btn-sm btn-hover-light-primary ${n.id.includes("tax") ? 'd-none' : ''}" data-toggle="tooltip" data-original-title="Expand Card" data-chart-container="${n.id}_preview" data-modal-title="${n.title}" onclick="showChartPreview(this)">
                                <i class="fas fa-expand icon-md text-primary"></i>
                            </button>
                            <button class="btn btn-icon btn-sm btn-hover-light-danger" data-toggle="tooltip" data-original-title="Remove Card" data-chart-container="${n.id}" onClick="removeWidget(this.parentElement.parentElement.parentElement.parentElement.parentElement,this)">
                                <i class="fas fa-times icon-md text-danger"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="${n.id.includes("tax") ? 'timeline timeline-5' : ''}" id="${n.id.includes("tax") ? 'tax_prep_queue_activity_log_timeline' : ''}">
                            <div id="${n.id}"></div>
                        </div>
                    </div>
                </div>`);

function bindWidgetDropdown() {
    $.each(chartsJsonData, function (index, item) {
        var d = chartsJsonData.filter(m => m.id == item.id && m.active).length;
        var disabled = d ? "disabled" : "";
        $("#drp-widgets").append(`<a id='${item.id}_drp_item' class="dropdown-item ${disabled}" href="javascript:" onclick="addWidget('${item.id}')">${item.title}</a>`);
    });
}
function bindEventChart(elSelector) {
    if (!elSelector.includes("preview"))
        KTApp.block(`[gs-id='${elSelector}'] .grid-stack-item-content`, {
            state: 'primary',
            message: 'Please wait...'
        });
    else
        $("#ChartPreviewModal .modal-title .spinner").removeClass("d-none");
    $.get('adviser/dashboard/fetcheventchartdata', function (response) {
        var eventChartOptions = {
            series: [{
                name: "Events",
                data: response
            }],
            chart: {
                type: 'bar',
            },
            colors: ['#26b99a'],
            plotOptions: {
                bar: {
                    columnWidth: '50%',
                }
            },
            dataLabels: {
                enabled: false
            },
            legend: {
                show: false
            },
            xaxis: {
                categories: [
                    ['Webinar', '(Live)'],
                    ['Webinar', '(Pre-Recorded)'],
                    ['Client Appreciation', 'Event'],
                    ['Seminar', '(Live In-Person)'],
                    ['Online Paid', 'Ad Campaign'],
                    ['Livestream Event', '(Social Media)'],
                    ['Non-Client', 'Advocate'],
                    ['Other'],
                ],
                labels: {
                    style: {
                        fontSize: '12px'
                    }
                }
            },
            yaxis: {
                title: {
                    text: "# of Events",
                    style: {
                        fontSize: '12px',
                        fontWeight: '400'
                    },
                },
            },
        };
        const eventChartEl = `#${elSelector}`;
        window["event_chart_data"] = new ApexCharts(document.querySelector(eventChartEl), eventChartOptions);
        event_chart_data.render();
        $("#event_chart_footer").removeClass("d-none");
        KTApp.unblock(`[gs-id='${elSelector}'] .grid-stack-item-content`);
        $("#ChartPreviewModal .modal-title .spinner").addClass("d-none");
    })
}
function bindProspectAppointmentChart(elSelector) {
    if (!elSelector.includes("preview"))
        KTApp.block(`[gs-id='${elSelector}'] .grid-stack-item-content`, {
            state: 'primary',
            message: 'Please wait...'
        });
    else
        $("#ChartPreviewModal .modal-title .spinner").removeClass("d-none");
    $.get('adviser/dashboard/fetchprospectappointmentchartdata', function (response) {
        var prospectAppointmentChartOptions = appointmentChartOptions;
        prospectAppointmentChartOptions.colors = ['#F64E60'];
        prospectAppointmentChartOptions.xaxis = {
            categories: [
                ['First', 'Appointment'],
                ['Second', 'Appointment'],
                ['Third', 'Appointment'],
                ['New', 'Client'],
                ['Referral'],
            ],
            labels: {
                style: {
                    fontSize: '12px'
                }
            }
        }
        prospectAppointmentChartOptions.yaxis = {
            title: {
                text: "# of Appoinments",
                style: {
                    fontSize: '12px',
                    fontWeight: '400'
                },
            },
        };
        prospectAppointmentChartOptions.series = [{
            name: 'Prospect Appointment',
            data: response
        }];
        var prospectAppointmentChartEl = `#${elSelector}`;
        window["prospect_appointment_chart_data"] = new ApexCharts(document.querySelector(prospectAppointmentChartEl), prospectAppointmentChartOptions);
        prospect_appointment_chart_data.render();
        KTApp.unblock(`[gs-id='${elSelector}'] .grid-stack-item-content`);
        $("#ChartPreviewModal .modal-title .spinner").addClass("d-none");
    });
}
function bindClientAppointmentChart(elSelector) {
    if (!elSelector.includes("preview"))
        KTApp.block(`[gs-id='${elSelector}'] .grid-stack-item-content`, {
            state: 'primary',
            message: 'Please wait...'
        });
    else
        $("#ChartPreviewModal .modal-title .spinner").removeClass("d-none");
    $.get('adviser/dashboard/fetchclientappointmentchartdata', function (response) {
        var clientAppointmentChartOptions = appointmentChartOptions;
        clientAppointmentChartOptions.colors = ['#8950FC'];
        clientAppointmentChartOptions.xaxis = {
            categories: [
                ['New Investment', 'Business'],
                ['New Insurance', 'Business'],
                ['Strategy and', 'Tactical'],
                ['Account', 'Administration'],
                ['Client', 'Advocate'],
                ['Ancillary', 'Services'],
            ],
            labels: {
                style: {
                    fontSize: '12px'
                }
            }
        }

        clientAppointmentChartOptions.series = [{
            name: 'Client Appointment',
            data: response
        }];
        var clientAppointmentChartEl = `#${elSelector}`;
        window["client_appointment_chart_data"] = new ApexCharts(document.querySelector(clientAppointmentChartEl), clientAppointmentChartOptions);
        client_appointment_chart_data.render();
        KTApp.unblock(`[gs-id='${elSelector}'] .grid-stack-item-content`);
        $("#ChartPreviewModal .modal-title .spinner").addClass("d-none");
    });
}
function bindActivityLog(pageNumber) {
    if (pageNumber == 1) {
        page = 1;
        $("#tax_prep_queue_activity_log").html("");
        $('#tax_prep_queue_activity_log').scroll(function (e) {
            var scrollPos = $(this).scrollTop() + $(this).innerHeight();
            if (scrollPos >= this.scrollHeight && !loading && page - 1 < lastPage)
                bindActivityLog(page);
        });
    }
    KTApp.block(`[gs-id='tax_prep_queue_activity_log'] .grid-stack-item-content`, {
        state: 'primary',
        message: 'Please wait...'
    });
    loading = true;
    page++;
    $.get(`adviser/dashboard/fetchtaxprepqueueacitvities?page=${pageNumber}`, function (res) {
        var el = $("#tax_prep_queue_activity_log");
        el.append($("#tax-prep-queue-activity-template").tmpl(res.data, { convertStringToHTML: convertStringToHTML }));
        loading = false;
        lastPage = res.meta.pages;
        KTApp.unblock(`[gs-id='tax_prep_queue_activity_log'] .grid-stack-item-content`);
        var rowsLength = $(".timeline-item").length;
        if (rowsLength > 6) {
            el.addClass("tax-prep-queue-activity");
            el.css("height", "270px");
        }
    })
}
function convertStringToHTML(htmlString, username, name, userid) {
    if (!userid) return htmlString.replace("@CLIENTNAME", "");
    return htmlString.replace("@CLIENTNAME", ` of <span class="text-maroon font-weight-bold" role="button" data-toggle="tooltip" data-original-title="${name}">${username}</span>`)
}
function showChartPreview(el) {
    var container = $(el).attr("data-chart-container");
    var title = $(el).attr("data-modal-title");
    $("#ChartPreviewModal .modal-body").html("");
    $("#ChartPreviewModal .modal-body").append(`<div id='${container}'></div>`);
    if (container.includes("event")) bindEventChart(container);
    else if (container.includes("prospect")) bindProspectAppointmentChart(container);
    else if (container.includes("client")) bindClientAppointmentChart(container);
    $("#ChartPreviewModal .chart-title").text(title);
    $("#ChartPreviewModal").modal({ backdrop: 'static' });
}
function refreshChart(el) {
    var variableName = $(el).attr("data-variable");
    if (variableName.includes("tax")) bindActivityLog(1);
    else {
        var title = $(el).attr("data-modal-title");
        var container = $(el).attr("data-chart-container");
        KTApp.block(`[gs-id='${container}'] .grid-stack-item-content`, {
            state: 'primary',
            message: 'Please wait...'
        });
        var actionType = variableName.includes("event") ? "event" : variableName.includes("prospect") ? "prospectappointment" : "clientappointment";
        $.get(`adviser/dashboard/fetch${actionType}chartdata`, function (response) {
            window[variableName].updateOptions({
                series: [{
                    name: title,
                    data: response
                }],
            })
            KTApp.unblock(`[gs-id='${container}'] .grid-stack-item-content`);
        });
    }
}

function bindDashboard() {
    $.get('adviser/dashboard/FetchDashboardConfig', function (response) {
        if (response)
            chartsJsonData.forEach((n, i) => {
                var res = response.filter(m => m.id == n.id)[0];
                if (res) {
                    n.active = true;
                    n.x = res.x;
                    n.y = res.y;
                    n.w = res.w;
                    n.h = res.h;
                }
            });
        bindWidgetDropdown();
        var activeChartsJsonData = chartsJsonData.filter(m => m.active);
        grid.load(activeChartsJsonData, true);

        $.each(activeChartsJsonData, function (index, item) {
            if (activeChartsJsonData.filter(m => m.id == item.id).length)
                item.id.includes("tax") ? window[`${item.functionName}`](1) : window[`${item.functionName}`](`${item.id}`);
        })
    });
}
function addWidget(id) {
    $(`#${id}_drp_item`).addClass("disabled");
    var widgetData = chartsJsonData.filter(m => m.id == id && !m.active)[0];
    var wigetHtml = `<div class="grid-stack-item" gs-id="${widgetData.id}" gs-x="${widgetData.x}" gs-y="${widgetData.y}" gs-w="${widgetData.w}" gs-h="${widgetData.h}">
                        <div class="grid-stack-item-content">${widgetData.content}</div>
                     </div>`;
    grid.addWidget(wigetHtml, 0, 0, widgetData.w, widgetData.h);
    widgetData.id.includes("tax") ? window[`${widgetData.functionName}`](1) : window[`${widgetData.functionName}`](`${widgetData.id}`);
}
function removeWidget(el, sel) {
    var id = $(sel).attr("data-chart-container");
    $(`#${id}_drp_item`).removeClass("disabled");
    grid.removeWidget(el, true);
    el.remove();
    $('[data-toggle="tooltip"], .tooltip').tooltip("hide");
}

bindDashboard();