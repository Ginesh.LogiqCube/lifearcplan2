﻿$("#ZipCode").on('input', function () {
    var zipcode = $(this).val();
    showHideZip(zipcode);
});
$(".form-control").attr("data-lpignore", true);
$("#change-email-btn").click(function () {
    KTApp.blockPage();
    loadPartialView(`personal/ChangeEmailModal`, "ChangeEmailModal", null, function () {
        KTApp.unblockPage();
        $("#ChangeEmailModal").modal({ backdrop: 'static' });
    });
});
var showHideZip = function (zipcode) {
    zipcode = zipcode ? zipcode.replace(/_/g, '') : zipcode;
    $(".address").addClass("d-none");
    if (zipcode && zipcode.length > 4) {
        $(".address").removeClass("d-none");
        $("#State").val("");
        $("#City").val("");
        $.get(`https://zip.getziptastic.com/v2/US/${zipcode}`, function (response) {
            $("#State").val(response.state);
            $("#City").val(response.city);
        });
    }
    else
        $(".address").addClass("d-none");
}