﻿const AdminRoleEnum = {
    SUPERADMIN: 1,
    FIRMPRINCIPAL: 2,
    FIRMADMIN: 3,
    ADVISER: 4,
    COMPLIANCE: 5,
}
const AssistantPermissionEnum = {
    AssistantC: 1,
    AssistantCQ: 2,
    AssistantIQ: 3,
    AssistantTQ: 3,
}
const MaritalStatus = {
    MARRIED: 1,
    DIVORCED: 2,
    WIDOWED: 3,
    SEPARATED: 4,
    UNMARRIED: 5
};
const ClientType = {
    CLIENT: 1,
    SPOUSE: 2,
    XSPOUSE: 3,
    OTHER: 4
}
const EmploymentStatus = {
    FULLTIME: 1,
    PARTTIME: 2,
    SELFEMPLOYED: 3,
    HOMEMAKER: 4,
    RETIRED: 5
}
const EmployerOfferType = {
    K401: 1,
    B403: 2,
    PLAN457: 3,
    SIMPLEIRA: 4,
    KEOUGHPLAN: 5,
    SEPIRA: 6,
    SOLO401K: 7
}
const ContractType = {
    LIFE: 1,
    ANNUITY: 2
}
const LifeAnnuityContractType = {
    TERM: 1
}
const PreviousEmployerRetirementPlanType = {
    OTHER: 16
}
const BankCreditUnionAccountType = {
    SAVINGS: 1,
    PERSONALCHECKING: 2,
    BUSINESSCHECKING: 3,
    CERTIFICATEOFDEPOSIT: 4,
    MONEYMARKET: 5
}
const BrokerageAdisoryAccountType = {
    Cash: 1,
    MutualFund: 2,
    ExchangeTradedFund: 3,
    IndividualStock: 4,
    IndividualCorporateBond: 5,
    IndividualMunicipalBond: 6,
    IndividualUSGovtBond: 7,
    Other: 8
}
const PersonalCareType = {
    ProviceCare: 1,
    PrimaryCare: 2,
    Facility: 3
}
const YesNoEnum = {
    Yes: 1,
    No: 2,
    DontKnow: 3
}
const RelationshipEnum = {
    Self: 1,
    Parent: 2
}
const EventTypeEnum = {
    WebinarLive: 1,
    WebinarPreRecorded: 2,
    ClientAppreciationEvent: 3,
    SeminarLiveInPerson: 4,
    OnlinePaidAdCampaign: 5,
    LivestreamEventSocialMedia: 6,
    NonClientAdvocate: 7,
    Other: 8
}
const OccasionTypeEnum = {
    PersonalTimeOutOfOffice: 1,
    StaffTeamMeeting: 2,
    TrainingCoachingOffsite: 3,
    TrainingCoachingOffsite: 4,
    TrainingCoachingOnsite: 5,
    CommunityMarketing: 6,
    HolidayVacation: 7,
    Other: 8
}
const Roles = {
    SUPERADMIN: { Value: 1, Title: "Super Admin" },
    FIRMPRINCIPAL: { Value: 2, Title: "Firm Principal" },
    FIRMADMIN: { Value: 3, Title: "Firm Admin" },
    ADVISER: { Value: 4, Title: "Adviser" },
    COMPLIANCE: { Value: 5, Title: "Compliance" },
}
const ComplianceDocumentStatus = {
    Pending: { Value: 1, ColorClass: "warning" },
    Approved: { Value: 2, ColorClass: "success" },
    Rejected: { Value: 3, ColorClass: "danger" }
}
const InsuranceDocumentStatus = {
    Pending: { Value: 1, ColorClass: "warning" },
    Approved: { Value: 2, ColorClass: "success" },
    Issued: { Value: 3, ColorClass: "info" },
    Rejected: { Value: 4, ColorClass: "danger" }
}
const InsuranceDocumentTypeStatus = {
    Life: { Value: 1 },
    Annuity: { Value: 2 },
    LongTermCare: { Value: 3, Title: "Long Term Care" },
    Disability: { Value: 4 }
}
const TaxDocumentStatus = {
    NoStatus: { Value: 0, Title: "No Status" },
    InitialTaxDocumentUploaded: { Value: 1, Title: "Initial Tax Document(s) Uploaded" },
    InitialAppointmentScheduled: { Value: 2, Title: "Initial Appointment Scheduled" },
    TaxFormsProvidedToTaxPreparer: { Value: 3, Title: "Tax Forms Provided to Tax Preparer" },
    PreparationInProcess: { Value: 4, Title: "Preparation in Process" },
    TaxFormsCompleted: { Value: 5, Title: "Tax Forms Completed" },
    FilingReviewAppointmentScheduled: { Value: 6, Title: "Filing Review Appointment Scheduled" },
    TaxFilingSubmittedToIRS: { Value: 7, Title: "Tax Filing Submitted to IRS" }
}

const FileTypes = {
    Word: { type: "doc", icon: "fa-file-word", color: "#295497" },
    CSV: { type: "csv", icon: "fa-file-csv", color: "#1f7244" },
    Excel: { type: "xls", icon: "fa-file-excel", color: "#1f7244" },
    PPT: { type: "ppt", icon: "fa-file-powerpoint", color: "#d14424" },
    PDF: { type: "pdf", icon: "fa-file-pdf", color: "#cb0606" },
    Text: { type: "txt", icon: "fa-file-alt", color: "#3699FF" },
    Link: { type: "link", icon: "fa-globe font-size-12", color: "#0073e9" },
    EPS: { type: "eps" },
    Image: { type: "image", icon: "fa-file-image", color: "#26b99a" },
    Audio: { type: "audio", icon: "fa-file-audio", color: "#8950FC" },
    Video: { type: "video", icon: "fa-file-video", color: "#F64E60" }
}
const ActivityTypes = {
    Event: { Value: 1, Title: "Event (Pipeline)", color: "#26b99a" },
    ClientAppointment: { Value: 2, Title: "Client Appointment (Operations)", color: "#8950FC" },
    ProspectAppointment: { Value: 3, Title: "Prospect Appointment (Operations)", color: "#F64E60" },
    Occasion: { Value: 4, Title: "Occasion", color: "#FF9933" }
}
const ClientAppoinmentPurpose = {
    NewInvestmentBusiness: { Value: 1, Title: "New Investment Business" },
    NewInsuranceBusiness: { Value: 2, Title: "New Insurance Business" },
    StrategyTactical: { Value: 3, Title: "Strategy and Tactical" },
    AccountAdministration: { Value: 4, Title: "Account Administration" },
    ClientAdvocate: { Value: 5, Title: "Client Advocate" },
    AncillaryServices: { Value: 6, Title: "Ancillary Services" }
}
const License = {
    Life: 1,
    Health: 2,
    Annuity: 3,
    PropertyCasualty: 4,
    Series6: 5,
    Series7: 6,
    Series62: 7,
    Series63: 8,
    Series65: 9,
    Series66: 10
}
const AffiliationType = {
    FieldMarketingOrganization: 1,
    BrokerDealer: 2,
    RegisteredInvestmentAdvisor: 3
}
const PropertyType = {
    Primary: 1
}
const currencyFormat = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
});

const KTAppSettings = {
    "breakpoints": {
        "sm": 576,
        "md": 768,
        "lg": 992,
        "xl": 1200,
        "xxl": 1400
    },
    "colors": {
        "theme": {
            "base": {
                "white": "#ffffff",
                "primary": "#3699FF",
                "secondary": "#E5EAEE",
                "success": "#1BC5BD",
                "info": "#8950FC",
                "warning": "#FFA800",
                "danger": "#F64E60",
                "light": "#E4E6EF",
                "dark": "#181C32"
            },
            "light": {
                "white": "#ffffff",
                "primary": "#E1F0FF",
                "secondary": "#EBEDF3",
                "success": "#C9F7F5",
                "info": "#EEE5FF",
                "warning": "#FFF4DE",
                "danger": "#FFE2E5",
                "light": "#F3F6F9",
                "dark": "#D6D6E0"
            },
            "inverse": {
                "white": "#ffffff",
                "primary": "#ffffff",
                "secondary": "#3F4254",
                "success": "#ffffff",
                "info": "#ffffff",
                "warning": "#ffffff",
                "danger": "#ffffff",
                "light": "#464E5F",
                "dark": "#ffffff"
            }
        },
        "gray": {
            "gray-100": "#F3F6F9",
            "gray-200": "#EBEDF3",
            "gray-300": "#E4E6EF",
            "gray-400": "#D1D3E0",
            "gray-500": "#B5B5C3",
            "gray-600": "#7E8299",
            "gray-700": "#5E6278",
            "gray-800": "#3F4254",
            "gray-900": "#181C32"
        }
    },
    "font-family": "Poppins"
};

const CurrencyMaskOptions = {
    alias: "currency",
    digits: 0,
    allowMinus: false,
    autoUnmask: true,
    clearMaskOnLostFocus: false,
}
const PercentageMaskOption = {
    alias: "percentage",
    allowMinus: false,
    autoUnmask: true,
    clearMaskOnLostFocus: false
}
const NumericMaskOption = {
    alias: "integer",
    placeholder: '0',
    allowMinus: false,
    autoUnmask: true,
    clearMaskOnLostFocus: false,
}
const YearMaskOption = {
    alias: "integer",
    suffix: " year(s)",
    max: 99,
    allowMinus: false,
    autoUnmask: true,
    clearMaskOnLostFocus: false
}
const GPAMaskOption = {
    alias: "decimal",
    digits: 1,
    digitsOptional: false,
    placeholder: '0',
    max: 10,
    allowMinus: false,
    autoUnmask: true,
    clearMaskOnLostFocus: false
}

const Message = {
    AreUSure: "Are you sure?",
    UWontRevert: "You won't be able to undo this!",
    UWontResume: "You won't be able to resume file upload!",
    DeleteIt: "Yes, delete it!",
    MoveIt: "Yes, move it!",
    StopIt: "Yes, stop it!",
    FileUploadStopped: "File upload has been stopped",
    QRCodeSent: "QR Code has been sent to your registered email address"
}
const toastrOptions = {
    closeButton: true,
    preventDuplicates: true
};
const progressToastrOptions = {
    timeOut: 0,
    timeOut: 0,
    extendedTimeOut: 0,
    tapToDismiss: false,
    preventDuplicates: true
};
const dropzoneDefaultOptions = {
    autoProcessQueue: false,
    paramName: "file",
    maxFiles: 1,
    maxFilesize: 50,
    timeout: 0,
    acceptedFiles: `application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,
                    application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,
                    application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation,
                    image/*,application/pdf,audio/*,video/*,text/plain,text/csv,.eps`,
    addRemoveLinks: true,
    dictInvalidFileType: "File format of this type are not accepted.",
    dictFileTooBig: "This file size exceeds the limit of {{maxFilesize}} MB.",
    dictMaxFilesExceeded: "You can not upload more than {{maxFiles}} files.",
    init: function () {
        this.on("addedfile", function (file) {
            if ($("#FileUrl").length > 0) {
                $("#FileUrl").val("");
                $("#FileUrl").prop("disabled", true);
            }
        });
        this.on("removedfile", function (file) {
            if ($("#FileUrl").length > 0) {
                $("#FileUrl").prop("disabled", false);
                var modalId = $('.modal:visible').attr("id");
                $(`#${modalId}`).find(':submit').prop("disabled", true);
            }
        });
        this.on("complete", function (file) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).find(':submit').prop("disabled", true);
            var dropzone = this;
            setTimeout(function () { dropzone.removeAllFiles(); }, 0)
        });
    },
    accept: function (file, done) {
        $(".alert").remove();
        var modalId = $('.modal:visible').attr("id");
        $(`#${modalId}`).find(':submit').prop("disabled", false);
        done();
    },
    error: function (file, errorMsg) {
        var modalId = $('.modal:visible').attr("id");
        var frm_id = $(`#${modalId}`).attr("id");
        _showFormMessage(frm_id, "danger", errorMsg, "fas fa-exclamation-triangle");
    },
    uploadprogress: function (file, progress) {
        setToastProgress(progress);
    },
    complete: function (file) {
        if (file.status != "error") {
            this.removeFile(file);
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            toastr.clear();
            if (file.status == "canceled") {
                toastr.options = toastrOptions;
                toastr.error(Message.FileUploadStopped);
            }
        }
    },
}

const successSwalOptions = {
    position: "top-right",
    timer: 5000,
    icon: "success",
    showConfirmButton: false,
    showCloseButton: true
}

const confimationSwalOptions = {
    title: Message.AreUSure,
    text: Message.UWontRevert,
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: Message.DeleteIt
}
const confimationSwalOptions2 = {
    title: Message.AreUSure,
    text: Message.UWontRevert,
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Yes"
}

const ktDataTableDefaultConfigOptions = {
    type: 'remote',
    pageSize: 10,
    serverSorting: true,
    serverFiltering: true,
    serverPaging: true,
    saveState: false
}