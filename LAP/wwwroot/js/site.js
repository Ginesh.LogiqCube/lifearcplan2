﻿String.prototype.startsWith || (String.prototype.startsWith = function (word) {
    return this.lastIndexOf(word, 0) === 0;
});

var showMaintenanceToastr = function (msg) {
    toastr.options = toastrOptions;
    $.fn.noop = function () { return this; };
    toastr.options.hideMethod = 'noop';
    toastr.options.showDuration = 0;
    toastr.options.hideDuration = 0;
    toastr.options.timeOut = 0;
    toastr.options.extendedTimeout = 0;
    toastr.options.tapToDismiss = false;
    toastr.options.closeButton = false;
    toastr.options.positionClass = "toast-top-full-width w-600px";
    toastr.warning(`<div class="d-flex align-items-center"><div class="mr-3">${msg}</div>`);
}
var setActiveWizard = function () {
    $('.wizard-steps .wizard-step').removeAttr("data-wizard-state");
    var action = `${window.location.pathname.replace(/^\/|\/$/g, '')}${window.location.search}`;
    $('.wizard-steps .wizard-step').filter(function () {
        var dataAction = $(this).attr("data-subactions");
        dataAction = dataAction != undefined ? dataAction : "";
        var href = $(this).attr("data-action");
        return action.includes(href) || dataAction.includes(action);
    }).attr('data-wizard-state', "current");
    var currentEl = $('.wizard-steps .wizard-step[data-wizard-state=current]');
    var title = currentEl.find(".wizard-title").text().trim();
    document.title = `LifeArcPlan™ | ${title}`;
}
var getActiveOrNot = function (value) {
    var ret = "";
    if (value == true)
        ret += "Yes";
    else
        ret += "No";
    return ret;
}
var _showFormMessage = function (form, type, msg, iconClass) {
    $(".alert").remove();
    var html = `<div class="alert alert-custom alert-${type}" role="alert">
                    <div class="alert-icon"><i class="${iconClass}"></i></div>
                    <div class="alert-text">${msg}</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="ki ki-close"></i></span>
                        </button>
                    </div>
                </div>`;

    var modalId = $('.modal:visible').attr("id");
    var el = $(`#${form}`);
    var hasModalForm = $(`#${form} .modal-body`).length;
    if (modalId && hasModalForm)
        el = $(`#${form} .modal-body`);
    $(html).hide().prependTo(el).fadeIn();
    $("#kt_scrolltop").click();
}
var _showErrorMessage = function (selector, msg) {
    $(".text-msg").remove();
    var html = `<span class="text-danger text-msg">${msg}</span>`;
    $(`#${selector}`).prepend(html);
}
var _hideErrorMessage = function () {
    $(".text-msg").remove();
}
var showHidePasswordAsterisk = function (id, curEl) {
    var x = document.getElementById(id);
    if (x.type === "password") {
        x.type = "text";
        $(curEl).find("i").removeClass("fa-eye").addClass("fa-eye-slash");
    }
    else {
        x.type = "password";
        $(curEl).find("i").removeClass("fa-eye-slash").addClass("fa-eye");
    }
    $(":submit").click(function (e) {
        x.type = "password";
        $(curEl).find("i").removeClass("fa-eye-slash").addClass("fa-eye");
    });
}
var onFormBegin = function () {
    $(".alert").remove();
    var buttonType = localStorage.getItem("buttonType");
    var id;
    if (buttonType)
        id = `btn${buttonType}`;
    else
        id = $(this).find(":submit:visible").not("#btn-save-exit").attr('id')
    id = id ? id : $("#btnSubmitId").val();
    var btn = KTUtil.getById(id);
    var fromExitButton = $("#FromExitButton").val() == "1";
    if (fromExitButton) {
        btn = KTUtil.getById("btn-save-exit");
        KTUtil.btnWait(btn, `spinner spinner-right spinner-dark pr-15`, "Please wait");
    }
    else
        KTUtil.btnWait(btn, `spinner spinner-right spinner-white pr-15`, "Please wait");
};
var onFormCompleteWithRedirect = function (response) {
    $(".alert").remove();
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj.success == false)
        _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    else {
        toastr.options = toastrOptions;
        toastr.success(responseObj.message);
        if (responseObj.redirectUrl)
            setTimeout(function () {
                window.location.href = responseObj.redirectUrl;
            }, 3000);
    };
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};
var onFormCompleteWith2FactorAuth = function (response) {
    $(".alert").remove();
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj.success == false)
        _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    else {
        if (responseObj.redirectUrl) {
            toastr.options = toastrOptions;
            toastr.success(responseObj.message);
            setTimeout(function () {
                window.location.href = responseObj.redirectUrl;
            }, 3000);
        }
        else {
            var loginData = responseObj.data.login;
            $("#kt_login_verification_form #VerificationUsername").val(loginData.username);
            $("#kt_login_verification_form #VerificationTimeZone").val(loginData.timeZone);
            $("#kt_login_verification_form #VerificationRememberMe").val(loginData.rememberMe);
            $("#kt_login_verification_form #VerificationFirstName").val(responseObj.data.firstname);
            _showForm('verification');
            $("title").html("LifeArcPlan | Sign In");
        }
    };
    KTUtil.btnRelease(btn);
};
var onFormCompleteWithSwal = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            $(`#${frm_id} div`).hide();
            _showFormMessage(frm_id, "success", responseObj.message, "flaticon2-check-mark");
            var swalOptions = successSwalOptions;
            swalOptions.title = responseObj.message;
            Swal.fire(swalOptions);
            if (responseObj.redirectUrl)
                setTimeout(function () {
                    window.location.href = responseObj.redirectUrl;
                }, 3000);
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};
var onFormCompleteWithModal = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            localStorage.setItem(modalId, true);
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
        }
        else
            _showFormMessage(`${frm_id}`, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage(`${frm_id}`, "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};
var onFormCompleteWithWizard = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
            gotoNextPrevAction();
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};
var onFormCompleteWithWizardRealodExplicitVue = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);
    if (responseObj) {
        if (responseObj.success) {
            toastr.options = toastrOptions;
            toastr.clear();
            toastr.success(responseObj.message);
            gotoNextPrevAction();
            LoadExplicitVue();
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};
var onFormCompleteDataTable = function (response) {
    var id = $(this).find(":submit").attr('id');
    var btn = KTUtil.getById(id);
    var frm_id = $(this).attr('id');
    var responseObj = JSON.parse(response.responseText);

    if (responseObj) {
        if (responseObj.success) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            $(`#${frm_id} div`).hide();
            if (responseObj.actionType == "add") {
                var swalOptions = successSwalOptions;
                swalOptions.title = responseObj.message;
                Swal.fire(swalOptions);
            }
            else {
                toastr.options = toastrOptions;
                toastr.success(responseObj.message);
            }
            if (window["datatable"] != undefined) {
                var id = window["datatable"][0].id;
                if ($(`#${id}:visible`).length > 0)
                    datatable.reload();
            }
            if (window['datatable-firm-principal-list'] != undefined)
                window['datatable-firm-principal-list'].reload();

            if (window['datatable-firm-adviser-list'] != undefined)
                window['datatable-firm-adviser-list'].reload();

            setTimeout(function () {
                var tbl = $("#hdnParentTable").val();
                var length = $(`#${tbl}`).length;
                if (length > 0)
                    $(`#${tbl} .datatable-toggle-subtable[data-value=${responseObj.parentId}]`).click();
            }, 2000);
        }
        else
            _showFormMessage(frm_id, "danger", responseObj.message, "fas fa-exclamation-triangle");
    }
    else
        _showFormMessage("kt_login_signin_form", "danger", "Error Occur", "fas fa-exclamation-triangle")
    KTUtil.btnRelease(btn);
    localStorage.removeItem("buttonType")
};
var updatesinglevalue = function (table, key, value, successCallback) {
    $.post("home/updatesinglevalue", { table: table, key: key, value: value }, function () {
        if (successCallback) {
            successCallback();
        }
    });
};
var loadPartialView = function (action, selector, loaderDiv, callback) {
    KTApp.block(`#${loaderDiv}`, { state: 'primary', message: 'Please wait...' });
    $(`#${selector}`).load(`${action}`, function () {
        $("form").each(function () {
            $.validator.unobtrusive.parse(this);
        })

        KTApp.unblock(`#${loaderDiv}`);
        if (callback != undefined)
            callback();
    });
}
var deleteRecord = function (url, callback) {
    $.ajax({
        url: url,
        type: 'DELETE',
        success: callback,
    })
}
var validateNonZero = function () {
    $(`.zero-not-allowed`).filter(function () {
        var a = $.trim($(this).val()).replace(/[^\w\s]/gi, '').trim();
        return a == '' || a == 0;
    }).addClass("input-validation-error");
}
var getEmptyTextBoxCount = function (parentDiv) {
    return $(`#${parentDiv} .none-zero`).filter(function () {
        var a = $.trim($(this).val()).replace(/[^\w\s]/gi, '').trim();
        return a == '' || a == 0;
    }).length;
}
var getRandomNumber = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var getLocalDateTime = function (date) {
    var utcDate = moment.utc(date).toDate();
    var dt = moment(utcDate).local().format('MMM D, YYYY');
    var time = moment(utcDate).local().format('hh:mm A').toUpperCase();
    return [dt, time];
}

var getLocalDateTimeString = function (date) {
    var utcDate = moment.utc(date).toDate();
    return moment(utcDate).local().format('MMM D, YYYY hh:mm A');
}

var reinvite = function (currentEl, role, guid) {
    var principalUrl = `admin/firmprincipal/reinvite?UserLoginGuid=${guid}`;
    var adminUrl = `admin/firmadmin/reinvite?UserLoginGuid=${guid}`;
    var adviserUrl = `admin/adviser/reinvite?UserLoginGuid=${guid}`;
    var clientUrl = `adviser/home/reinvite?ClientLoginGuid=${guid}`;
    var url = role == AdminRoleEnum.FIRMPRINCIPAL ? principalUrl : role == AdminRoleEnum.FIRMADMIN ? adminUrl : role == AdminRoleEnum.ADVISER ? adviserUrl : role == AdminRoleEnum.CLIENT ? clientUrl : '';
    $(currentEl).prop("disabled", true);
    $(".tooltip").tooltip("hide");
    $(currentEl).find("i").addClass("fa-spin");
    $.post(url, function (response) {
        if (response) {
            if (response.success) {
                var swalOptions = successSwalOptions;
                swalOptions.title = response.message;
                Swal.fire(swalOptions);
            }
            else {
                toastr.options = toastrOptions;
                toastr.error(response.message);
            }
        }
        $(currentEl).find("i").removeClass("fa-spin");
        $(currentEl).prop("disabled", false);
    });
}
var bindFirmsGrid = function (type, callback) {
    var firmKtDataTablOptions = ktDataTableDefaultConfigOptions;
    firmKtDataTablOptions.source = `admin/superadmin/fetchfirms`;
    window['datatable'] = $('#firm_list').KTDatatable({
        data: firmKtDataTablOptions,
        columnFilter: true,
        columns: [
            { field: 'id', title: '', sortable: false, width: 0 },
            {
                field: 'firmName', title: 'Firm Name', sortable: 'asc', filter: { container: "FirmName" },
                template: function (row) {
                    return `<div class="font-size-lg">${row.firmName}</div>`
                }
            },
            {
                field: `total${type}`, title: '', textAlign: 'right', sortable: false, width: 50,
                template: function (row) {
                    return `<span class="label label-lg label-info" data-id="${row.id}">${row[`total${type}`]}</span>`
                }
            },
        ],
        detail: { content: callback }
    })
}
var getRandomColor = function () {
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += Math.floor(Math.random() * 10);
    }
    return color;
}
var exportReport = function (el) {
    var actionUrl = $(el).attr("data-url");
    var filename = $(el).attr("data-filename");
    var id = $(el).attr("data-id");
    $(`#report-icon-${id}`).addClass("d-none");
    $(`#loader-icon-${id}`).removeClass("d-none");
    $.ajax({
        url: actionUrl,
        method: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = filename;
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
            $(`#loader-icon-${id}`).addClass("d-none");
            $(`#report-icon-${id}`).removeClass("d-none");
        }
    });
}
var enableDisableUpload = function (loading) {
    var el = $("#btn_submit_upload");
    el.prop("disabled", true);
    loading ? el.find("i").addClass("d-none") : el.find("i").removeClass("d-none");
    loading ? el.addClass("spinner") : el.removeClass("spinner");
    loading ? el.find("#upload-text").text("Uploading") : el.find("#upload-text").text("Upload");
    $("#progress-container .progress-bar").css("width", `0%`);
    $("#progress-container").addClass("d-none");
}
var humanFileSize = function (size) {
    if (size < 1000) return size + ' B'
    let i = Math.floor(Math.log(size) / Math.log(1000))
    let num = (size / Math.pow(1000, i))
    let round = Math.round(num)
    num = round < 10 ? num.toFixed(2) : round < 100 ? num.toFixed(1) : round
    return `${num} ${'KMGTPEZY'[i - 1]}B`
}
var getElapseTime = function (miliSeconds) {
    var seconds = (miliSeconds / 1000).toFixed(2);
    return `${seconds} sec`;
}
var fileDownload = function (pathName, fileName, versionId, complete) {
    fileName = encodeURIComponent(fileName.replace("/", "'"));
    var url = `home/FileDownload?pathName=${pathName}&fileName=${fileName}`;
    url = versionId != 0 ? `${url}&versionId=${versionId}` : url;
    $.ajax({
        url: url,
        method: 'GET',
        xhrFields: { responseType: 'blob' },
        beforeSend: function () {
            initToastProgress();
        },
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var progress = parseInt((evt.loaded / evt.total) * 100);
                    var rem = parseInt(progress / 5);
                    $("#progress-container .progress-bar").css("width", `${progress}%`);
                    $("#toast-container .toast-message").html(`<span style="font-size: 15px;">Downloading... ${progress}%</span>`);
                    $("#toast-container .toast .toast-custom-progress").css("box-shadow", `inset ${rem}rem 0 0 black`);
                    if (progress == 100)
                        toastr.clear();
                }
            }, false);
            return xhr;
        },
        success: function (data) {
            var url = window.URL.createObjectURL(data);
            var extension = fileName.split('.').pop();
            if (extension == "pdf")
                openFileOrLinkNewWindow(url);
            else {
                var a = document.createElement('a');
                a.href = url;
                a.download = fileName;
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
            }
        },
        complete: complete
    });
}
var clientFileDownload = function (pathName, fileName, versionId, id, isModal) {
    var parent = "modal";
    isModal != undefined ? $(`.${parent}`).find(`#loader-${id}`).removeClass("d-none") : $(`#loader-${id}`).removeClass("d-none");
    isModal != undefined ? $(`.${parent}`).find(`#icon-${id}`).addClass("d-none") : $(`#icon-${id}`).addClass("d-none");
    fileName = fileName.replace("/", "'");
    fileDownload(pathName, fileName, versionId, function () {
        isModal != undefined ? $(`.${parent}`).find(`#loader-${id}`).addClass("d-none") : $(`#loader-${id}`).addClass("d-none");
        isModal != undefined ? $(`.${parent}`).find(`#icon-${id}`).removeClass("d-none") : $(`#icon-${id}`).removeClass("d-none");
    });
}
var openFileOrLinkNewWindow = function (url) {
    url = url.includes("http") ? url : `http://${url}`;
    window.open(url);
}
var substringMatcher = function (strs) {
    return function findMatches(query, filterData) {
        var matches = strs.filter(m => m.toLowerCase().startsWith(query.toLowerCase()));
        filterData(matches);
    };
};
var UrlToLink = function (text) {
    var url = text.replace("http://", "").replace("https://", "").replace("www.", "");
    return `http://www.${url}`;
}
var getSerialNo = function (n, pageData) {
    var prevPage = pageData.page != 1 ? pageData.page - 1 : 0;
    return n + (prevPage * pageData.perpage);
}
var setToastProgress = function (progress) {
    progress = parseInt(progress);
    var rem = parseInt(progress / 5);
    $("#progress-container .progress-bar").css("width", `${progress}%`);
    $("#toast-container .toast-message").html(`<span style="font-size: 15px;">Uploading... ${progress}%</span>`);
    $(".dz-progress .dz-upload").css("width", `${progress}%`);
    $("#toast-container .toast .toast-custom-progress").css("box-shadow", `inset ${rem}rem 0 0 black`);
}
var initToastProgress = function () {
    toastr.options = progressToastrOptions;
    toastr.warning('<span style="font-size: 15px;">Please wait...</span>');
    $("#toast-container .toast").addClass("opacity-100");
    $("#toast-container .toast").append('<div class="toast-custom-progress"></div>');
}
var clearDropzone = function () {
    enableDisableUpload();
    $('.dropzone').each(function () {
        let dropzoneControl = $(this)[0].dropzone;
        if (dropzoneControl)
            dropzoneControl.destroy();
    });
}
var setSelectedOptionOne = function (selectorId, optionCount) {
    if (optionCount == 1)
        setTimeout(function () { $(`#${selectorId}`).selectpicker('val', $(`#${selectorId} option:last-child`).val()); }, 0);
}
var getDataTableDateWithHtml = function (date) {
    if (date != null && date.length > 2) {
        var localDateTime = getLocalDateTime(date);
        return `<div>${localDateTime[0]}</div><div class="text-primary">${localDateTime[1]}</div>`;
    }
    return "-";
}
var isValidURL = function (string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
};
var bindfileTypeDropdown = function (firmId) {
    var el = firmId ? $(`.filetype-filter-dropdown-${firmId}`) : $("#fileType-filter");
    el.empty();
    var drpBody = "";
    var unknownHtml = `<span class='eps-icon-container position-relative mr-2'>
                <span class='icon-text'>EPS</span>
                <i class='fa fa-file font-size-20' style='color:#008080'></i>
            </span>`;
    for (let [key, item] of Object.entries(FileTypes)) {
        icon = key.includes("EPS") ? unknownHtml : `<i class='fa ${item.icon} font-size-20 mr-2' style='color:${item.color}'></i>`;
        drpBody += `<option value='${item.type}' data-content="${icon}${key}"/>`;
    }
    el.html(drpBody);
}
var bindRolesDropdown = function (firmId) {
    $(`.roles-filter-dropdown-${firmId}`).empty();
    var drpBody = "";
    for (let [key, item] of Object.entries(Roles)) {
        if (item.Value != Roles.SUPERADMIN.Value)
            drpBody += `<option value='${item.Value}' data-content="${item.Title}" multiple/>`;
    }
    $(`.roles-filter-dropdown-${firmId}`).html(drpBody);
}
var getCheckedIds = function (dt) {
    var uncheckedIds = [];
    dt.rows('.datatable-row').nodes().find('.checkbox > [type="checkbox"]').each(function (i, chk) {
        var checked = $(this).is(":checked");
        if (checked)
            checkedIds.push($(this).val())
        else
            uncheckedIds.push($(this).val())
    });
    checkedIds = checkedIds.filter((el) => !uncheckedIds.includes(el));
    checkedIds = checkedIds ? checkedIds.concat(checkedIds) : checkedIds;
    return checkedIds.filter((v, i) => checkedIds.indexOf(v) == i);
}
var setActiveCheckBox = function (el, successCallback) {
    var totalCheckbox = el.find("td .checkbox [type='checkbox']").length;
    var checkedCheckboxOnPage = el.find("td .checkbox [type='checkbox']:checked").length;
    var allCheckedOnPage = totalCheckbox == checkedCheckboxOnPage;
    el.find("th .checkbox [type='checkbox']").prop("checked", allCheckedOnPage || checkedCheckboxOnPage > 0);
    el.find("th .checkbox [type='checkbox']").prop("indeterminate", checkedCheckboxOnPage > 0 && !allCheckedOnPage);
    setTimeout(function () {
        $("#migrate-client-container").css("width", "fit-content");
        $("#adviser-list-drp").next().css("width", "200px");
    }, 0);
    if (successCallback) successCallback();
}
var formatCurrency = function (amount) {
    var result = "-";
    if (amount > 0)
        result = currencyFormat.format(amount);
    return result;
}
var formatYear = function (year) {
    var result = "-";
    if (year > 0)
        result = `${year} year(s)`;
    return result;
}
var bindFirmDropdown = function (selectorId) {
    selectorId = selectorId ? selectorId : "FirmId";
    $(`#${selectorId}`).select2({
        ajax: {
            url: 'admin/firmprincipal/getfirms',
            data: function (params) {
                return { term: params.term, page: params.page || 1 };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data.items,
                    pagination: {
                        more: (params.page * 10) < data.totalCount
                    }
                };
            }
        }
    });
}
var bindPermissionMultiselect2 = function (selectorId) {
    var roleColors = ["#8950FC", "#1BC5BD", "#FFA800", "#1BC5BD", "#8950FC"];
    var permissionColors = ["#8950FC", "#1BC5BD", "#FFA800", "#EC0C24", "#1BC5BD"];
    var colors = selectorId && selectorId.includes("Permission") ? permissionColors : roleColors;
    selectorId = selectorId ? selectorId : "Permission";
    $(`#${selectorId}`).select2({
        templateSelection: function (data, container) {
            $(container).css("background-color", colors[data.id - 1]);
            return data.text;
        },
    });
}
var showSurveyCompletionToastr = function (msg, functionName) {
    toastr.options = toastrOptions;
    $.fn.noop = function () { return this; };
    toastr.options.hideMethod = 'noop';
    toastr.options.showDuration = 0;
    toastr.options.hideDuration = 0;
    toastr.options.timeOut = 0;
    toastr.options.extendedTimeout = 0;
    toastr.options.tapToDismiss = false;
    toastr.options.closeButton = false;
    toastr.options.positionClass = "toast-top-full-width risk-arc-toast";
    var bt = `<a class="btn btn-sm btn-white" onclick="${functionName}()">Continue</button>`;
    toastr.success(`<div class="d-flex align-items-center"><div class="mr-3">${msg}</div><div class="mr-3">${bt}</div>`);
}
Dropzone.confirm = function (question, accepted, rejected) {
    var swalOptions = confimationSwalOptions;
    swalOptions.text = Message.UWontResume;
    swalOptions.confirmButtonText = Message.StopIt;
    Swal.fire(swalOptions)
        .then(function (result) {
            if (result.value)
                accepted();
        });
};
$(document).on('click', '#btn-save-exit,#save-exit-setting,#save-exit-survey', function (e) {
    $("#FromExitButton").val("1");
    if ($("#btn-save-exit"))
        $("#btn-save-exit-hidden").click();
});
$(document).on('click', '#kt_quick_user_toggle_mobile', function (e) {
    $("#kt_quick_user_toggle").click();
});
$(document).on("click", ":submit", function () {
    setTimeout(function () {
        $(".bootstrap-select .input-validation-error").each(function () {
            $(this).parent().addClass("input-validation-error");
        });
    }, 0)
});
$(document).on('focus', ':text:not(.bootstrap-timepicker-widget input)', function () {
    $(this).attr('autocomplete', 'current-password');
});
$(document).on('click', ".reset-btn", function () {
    var $form = this.closest('form');
    $($form).formReset();
});
$(document).on('input', 'input.form-control:not(.phone,.zipcode,.numericOnly,.currency,.percentage,.gpa-mask,.year-mask,.datemask,#Password,#ConfirmPassword)', function (e) {
    var str = e.target.value;
    if (/^[\s.!@#$%^&*()+-/*_=<>\[\]}{|\\:;"',.?/`~]/.test(str))
        str = '';
    $(this).val(str);
});
$(document).on('input', 'input.numeric', function (e) {
    if (/\D/g.test(this.value)) {
        this.value = this.value.replace(/\D/g, '');
    }
});
$(document).on('input', '.capitalize', function (e) {
    var str = $(this).val();
    $(this).val(str.charAt(0).toUpperCase() + str.slice(1));
});
$(document).on('input', '[name="Email"],[name="Username"]:not(#IntegrationUsername),.tolowercase', function (e) {
    var str = $(this).val();
    $(this).val(str.toLowerCase());
});
$(document).on('input', '.autosize-textbox', function (e) {
    var length = $(this).val().length * 8;
    var minlength = $(this).attr("data-minlength");
    $(this).css("width", length > minlength ? length : minlength);
});
$(document).on('input', '.zero-not-allowed', function (e) {
    var val = e.target.value;
    if (val == 0 || val == "")
        $(this).addClass("input-validation-error");
    else
        $(this).removeClass("input-validation-error");
});
$(document).on('show.bs.modal', "#ArcUploadModal", function () {
    clearDropzone();
    var dropzoneOptions = dropzoneDefaultOptions;
    dropzoneOptions.url = "home/arcupload";
    dropzoneOptions.sending = function (file, xhr, formData) {
        var ClientId = $("#ArcUploadModal #hdnClientId").val();
        formData.append("Id", ClientId);
        $("#progress-container").removeClass("d-none");
        initToastProgress();
    };

    dropzoneOptions.success = function (file, response) {
        if (file.accepted) {
            var swalOptions = successSwalOptions;
            swalOptions.title = response.message;
            Swal.fire(swalOptions);

            var ClientId = response.parentId;
            if (ClientId > 0) {
                if ($(`#client_list_${ClientId}:visible`).length == 0)
                    $(`#client_list .datatable-toggle-subtable[data-value=${ClientId}]`).click();

                if (window['client_sub_datatable_' + ClientId] != undefined)
                    window['client_sub_datatable_' + ClientId].reload();

                var el = $(`#client_list [data-field='total'] .label-info[data-id='${ClientId}']`);
                el.text(response.data.arcHiveFileCount);
            }
            else
                window['datatable'] && window['datatable'].reload();
        }
        else {
            toastr.options = toastrOptions;
            toastr.error(response.message);
        }
    };
    $('#arc-upload').dropzone(dropzoneOptions);

    $(document).on("click", "#ArcUploadModal #btn_submit_upload", function (e) {
        e.preventDefault();
        enableDisableUpload(true);
        Dropzone.forElement(".dropzone").processQueue();
    });
});
$(document).on('hide.bs.modal', "#ArcUploadModal,#RegulatoryDocumentUploadModal,#ComplianceDocumentUploadModal", function (e) {
    Dropzone.forElement(".dropzone").removeAllFiles(true);
});
$(document).on("scroll", function () {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    scrolled = scrolled > 99 ? 100 : scrolled;
    if (scrolled > 0) {
        $("#scrollbar-progress-container").removeClass("d-none");
        $("#scrollbar-progress-container .progress-bar").attr("aria-valuenow", scrolled).css("width", scrolled + "%");
    }
    else
        $("#scrollbar-progress-container").addClass("d-none");
});
$(document).on("change", ".bootstrap-select", function () {
    $(this).find("select").removeClass("error");
});
$(document).on("click", ".btn-copy", function () {
    var target = $(this).attr("data-target");
    var val = $(`#${target}`).val();
    $(this).attr("data-original-title", "Copied");
    $(this).removeClass("btn-warning").addClass("btn-secondary");
    navigator.clipboard.writeText(val);
});
$(document).on('click', '.close-event-popover,.fc-button-group button', function (e) {
    $('.event-popover').popover('hide');
});
//$(document).on('click', '.btn-card-fullscreen', function (e) {
//    e.preventDefault();
//    var $this = $(this);

//    if ($this.children('i').hasClass('fa-expand-alt')) {
//        $(".draggable-zone").addClass("d-none");
//        $this.children('i').removeClass('fa-expand-alt');
//        $this.children('i').addClass('fa-compress-alt');
//    }
//    else if ($this.children('i').hasClass('fa-compress-alt')) {
//        $(".draggable-zone").removeClass("d-none");
//        $this.children('i').removeClass('fa-compress-alt');
//        $this.children('i').addClass('fa-expand-alt');
//    }
//    $(this).closest('.card').toggleClass('card-fullscreen');
//    $('.card-fullscreen').parent().removeClass("d-none");
//});
$(document).ajaxError(function (event, jqxhr) {
    var loginUrl = $("#hdnLoginUrl").val();
    $("form").find(":submit").each(function () {
        KTUtil.btnRelease(this);
    });
    var returnUrl = `${loginUrl}?ReturnUrl=${encodeURIComponent(window.location.pathname)}`;
    if (jqxhr.status == 401)
        window.location.href = returnUrl;
    else if (jqxhr.status == 500) {
        toastr.options = toastrOptions;
        toastr.error("Something went wrong");
    }
});