﻿/*KTDatatable*/
$(document).on('datatable-on-sort', function (e, args) {
    setTimeout(function () {
        $(e.target).find(".datatable-pager-link.datatable-pager-link-number[data-page=1]").click();
    }, 1000);
});
$(document).on('datatable-on-ajax-done', function (e, data) {
    var el = $(e.target);
    var dt = el.KTDatatable();
    var sortableColumns = dt.options.columns.filter(m => m.sortable != false);
    $.each(sortableColumns, function (i, item) {
        el.find(`.datatable-head:not(".search-bar") .datatable-row [data-field='${item.field}']`).addClass("datatable-cell-sort");
    });

    setTimeout(function () {
        var inActiveUsers = data.filter(m => m.active == false || m.isDeleted == true);
        $.each(inActiveUsers, function (key, item) {
            el.find(`.datatable-row [data-field='id'][aria-label="${item.id}"]`).closest('tr').addClass("datatable-row-inactive");
            el.find(`.btn-adviser-edit[data-guid='${item.guid}']`).closest('tr').addClass("datatable-row-inactive");
        });
        el.find("[title]").removeAttr("title");
        el.find('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        el.find('[data-toggle="popover"]').popover();
        $(".datatable-table").scroll(function () {
            $("[data-toggle='popover']").popover('hide');
            $(".bs-container .dropdown-menu:not(.inner).show").removeClass("show");
        });
    }, 0);
});
$(document).on('datatable-on-layout-updated', function (e, args) {
    $('[data-toggle="tooltip"], .tooltip').tooltip("hide");
    $(".report-menu").hide();
    var el = $(e.target);
    var dt = el.KTDatatable();
    setTimeout(function () {
        $.each(dt.options.columns, function (i, item) {
            el.find(`tr th:eq(${i}) span:first`).css("width", `${item.width}px`);
            el.find(`tr td:eq(${i}) span:first`).css("width", `${item.width}px`);
        });
    }, 0);
});
$(document).on('datatable-on-check datatable-on-uncheck', function (e) {
    var el = $(e.target);
    setActiveCheckBox(el);
});
$(document).on('change input keyup', "[id$=filter]", function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    var column = e.target.id.replace("-filter", "").replace(".", "_");
    var value = $(e.target).val();
    value = value ? value.toString() : "";
    var dt = $(e.target).closest(".datatable").KTDatatable();
    var query = dt.getDataSourceParam("query");
    isSearched = query && query[`${column}`];
    if ((keycode == 13 && e.type == "keyup") || (e.target.nodeName.toLowerCase() != "input" && e.type == "change") || (value.length == 0 && isSearched)) {
        var datatableid = dt.attr("id");
        KTApp.block(`#${datatableid}`, {
            state: 'primary',
            message: 'Please wait...',
        });
        dt.search(value, column);
    }
});
/*Select2*/
$(document).on("select2:open", function (e) {
    $(this).find('.select2-search__field').attr('placeholder', e.target.dataset.searchPlaceholder);
})

/*Clear Button*/
$(document).on("input change keyup", "[data-allow-clear='true']:not(.select2)", function (e) {
    currentVal = $(this).val().replace(/^[\s.!@#$%^&*()+-/*_=<>\[\]}{|\\:;"',.?/`~]/, "");
    tagName = e.target.nodeName.toLowerCase();
    El = tagName == "select" ? $(this).next() : $(this);
    sibling = El.next(".close");
    if (currentVal.length > 0 && sibling.length == 0) {
        closeButtonHtml = `<button type="button" class="close" data-target-type="${tagName}">
                                <i aria-hidden="true" class="ki ki-close icon-xs text-dark"></i>
                           </button>`;
        El.after(closeButtonHtml);
    }
    else if (currentVal.length == 0)
        sibling.remove();
});

$(document).on("click", "[data-allow-clear='true'] + .close,.bootstrap-select .close", function (e) {
    tagName = $(this).data("target-type");
    if (tagName == "input")
        $(this).prev("input").val("").trigger("input");
    else if (tagName == "select")
        $(this).prevAll("select").val('').trigger("change");
    $(this).remove();
});

$(document).on("input", ".numericOnly-no-placeholder", function () {
    if (/\D/g.test(this.value))
        this.value = this.value.replace(/\D/g, '');
    if ($(this).val().length == 0)
        $(".form-control-sm.numericOnly-no-placeholder[data-allow-clear='true']").css("padding-right", "0.75rem");
    else
        $(".form-control-sm.numericOnly-no-placeholder[data-allow-clear='true']").css("padding-right", "1.5rem");
});

$(document).on("paste", ".form-control", function () {
    $(this).trigger("input");
});

/*Dropdown menu*/

$(document).on('show.bs.dropdown', function () {
    $(".tooltip").tooltip("hide");
});

/*Reset validation*/

$(document).on('change', '.form-control,.form-group input', function (e) {
    var $form = $(this).closest('form');
    if (e.target.id.length > 0) {
        $form.find(`.form-control#${e.target.id}`).removeClass("input-validation-error");
        $form.find(`.form-control#${e.target.id}`).parent().removeClass("input-validation-error");
        $form.find(`[data-valmsg-for='${e.target.id}']`).removeClass("field-validation-error").addClass("field-validation-valid").empty();
        $form.find(`[data-valmsg-for='${e.target.id.replace("_", ".")}']`).removeClass("field-validation-error").addClass("field-validation-valid").empty();
    }
});

/*Popover*/
$(document).on('click', '[data-toggle="popover"]', function () {
    $('[data-toggle="popover"]').not(this).popover('hide');
});

$(document).on('blur', '.input-group.datepicker-control input, input.datetimerange,#daterangepicker input', function (e) {
    var invalidDate = new Date(e.target.value) == "Invalid Date";
    if (invalidDate || ($(this).hasClass("datetimemask") && $(this).val() < 18))
        $(this).val("");
});
$('body').on('click', function (e) {
    $('[data-toggle=popover]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
            $('.event-popover').popover('hide');
        }
    });
});
$(".outline-radio-list [type='radio']").each(function () {
    if ($(this).is(":checked"))
        $(this).closest(".radio").addClass("active-radio");
});
$(document).on("change", ".outline-radio-list [type='radio']", function (e) {
    $(".outline-radio-list .active-radio").removeClass("active-radio");
    $(this).closest(".radio").addClass("active-radio");
});