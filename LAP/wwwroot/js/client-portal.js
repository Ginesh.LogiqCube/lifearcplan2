﻿var basePath;
var riskArcType = "";

var el = $('.menu-link[href="/investment-planning-intro"]');
el.attr("href", "javascript:").removeAttr("data-ajax").addClass("planning-disabled");
var InitialProfileSetupCompleted = $("#InitialProfileSetupCompleted").val() == 1;
//if (!InitialProfileSetupCompleted) {
//    el.parent().addClass("disabled-div");
//    el.addClass("disabled");
//}

var el2 = $('.menu-link[href="/budget-info"]');
el2.attr("href", "javascript:").removeAttr("data-ajax").addClass("budget-disabled");

var setDOB = function (employmentOf) {
    var employmentOf = $("#Occupation_EmploymentOf").val();
    var dob = $(`#DOB_${employmentOf}`).val();
    $("#Occupation_DOB").val(dob);
    $("#Esp_DOB").val(dob);
}
var setYearToRetire = function (currentYearToRetire, newYearToRetire) {
    if (newYearToRetire > currentYearToRetire)
        $("#ClientYearToRetire").val(newYearToRetire);
}
var setActiveMainMenu = function () {
    $("#riskarc-menu,.riskarc-link").removeClass("active");
    $(".menu-nav .menu-item.menu-item-active").removeClass("menu-item-active");
    $(".menu-subnav .menu-item.menu-item-active").removeClass("menu-item-active");
    $('.menu-nav .menu-link,.menu-subnav .menu-link').filter(function () {
        var action = window.location.pathname.replace(basePath, "");
        var dataAction = $(this).attr("data-action");
        dataAction = dataAction != undefined ? dataAction : "";
        var href = $(this).attr("href").replace(basePath, "").replace("/", "");
        return href == action || dataAction.includes(action);
    }).parent().addClass('menu-item-active');
    $(".menu-nav .menu-item").css("pointer-events", "");
    $(".menu-nav .menu-item.menu-item-active").css("pointer-events", "none");
}
var setActiveSubWizard = function () {
    $('#wizard-3 .nav.nav-tabs .nav-item .nav-link').removeClass("active");
    var el = $('#wizard-3 .nav.nav-tabs .nav-item .nav-link').filter(function () {
        var action = window.location.pathname.replace(basePath, "");
        var dataAction = $(this).attr("href").replace(basePath, "").replace("/", "");
        return dataAction == action;
    }).addClass("active");
    if (el.length == 0) {
        $("#wizard-3").remove();
        $("#wizard-submenu").html("");
    }
}
var setActiveRiskArcMenu = function () {
    $('.navi .navi-item .riskarc-link').filter(function () {
        var action = window.location.pathname.replace(basePath, "");
        var dataAction = $(this).attr("data-action");
        dataAction = dataAction != undefined ? dataAction : "";
        return dataAction.includes(action);
    }).addClass('active');
    $("#riskarc-menu").removeClass("active");
    var length = $('.navi .navi-item .riskarc-link.active').length;
    if (length > 0)
        $("#riskarc-menu").addClass("active");
}
var showVersionPopup = function (type, clientType) {
    var selectedVersion = $(`#${type}Version`).val();
    var isInDepth = selectedVersion == "1";
    $(`input[name='${type}Version'][value=${selectedVersion}]`).prop('checked', true);
    var btn = KTUtil.getById("btn-submit");
    KTUtil.btnRelease(btn);
    if (!isInDepth) {
        if (!window.location.pathname.includes("riskarc"))
            localStorage.setItem("last-action", window.location.pathname);
        $(`#${type}VersionModal`).modal({ backdrop: 'static', keyboard: false });
        $(document).on("change", `[name="${type}Version"]`, function (e) {
            var isInDepthVersion = $(e.target).val();
            if (isInDepthVersion == 1) {
                var msg = "You will not be allowed to revert back to Express Version.";
                setTimeout(function () {
                    _showFormMessage(`${type}VersionModalFrm`, "warning", msg, "fas fa-exclamation-triangle");
                }, 0)
            }
            else
                $(".alert").remove();
        })
    }
    else {
        var url = type == "Budget" ? "budget-info" : type == "Investment" ? "investment-planning-intro" : "client-riskarc-indepth";
        if (clientType == "spouse")
            url = "spouse-riskarc-indepth";
        $(`#${type}VersionModal`).modal("hide");
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        var hasWizardDiv = $("#wizard-content-div").length;
        if (url.includes("budget")) {
            var action = isInDepth ? "budget-info" : "budget-info-express";
            if (hasWizardDiv) {
                loadPartialView(`expense/${action.replace(/-/g, '')}`, "wizard-content-div", "wizard-main-div");
                loadPartialView(`home/wizardnav?parentId=3`, "wizard-nav", "wizard-nav", function () {
                    window.history.pushState("data", "title", action);
                    setActiveWizard();
                    $("#back-text").text(`Back to Family Info`);
                    $("#btn-submit,#btn-next").attr("data-original-title", `Save & Next to Personal Property`);
                    LoadExplicitVue();
                    $("#wizard-3").remove();
                    $("#wizard-submenu").html("");
                });
            }
            else
                window.location.href = action;
        }
        else if (url.includes("investment")) {
            var action = isInDepth ? "investment-planning-intro" : "investment-planning-express";
            if (hasWizardDiv) {
                loadPartialView(`investmentplanning/${action.replace(/-/g, '')}`, "wizard-content-div", "wizard-main-div");
                loadPartialView(`home/wizardnav?parentId=4`, "wizard-nav", "wizard-nav", function () {
                    window.history.pushState("data", "title", action);
                    setActiveWizard();
                    if (isInDepth)
                        BindSubWizard();
                    LoadExplicitVue();
                });
            }
            else
                window.location.href = action;
        }
        else
            window.location.href = url;
    }
}
var hideUnhideWizardItem = function () {
    $(".wizard-steps .wizard-step[data-id=7]").removeClass("d-none");
    $(".wizard-steps .wizard-step[data-id=8]").removeClass("d-none");
    $(".wizard-steps .wizard-step[data-id=14]").removeClass("d-none");
    $(".wizard-steps .wizard-step[data-id=16]").removeClass("d-none");
    $(".wizard-steps .wizard-step[data-id=18]").removeClass("d-none");
    $("#spouse-riskarc").removeClass("d-none");
    var maritalstatus = $("#ClientMaritalStatus").val();
    if (maritalstatus != MaritalStatus.MARRIED && maritalstatus != MaritalStatus.SEPARATED) {
        $(".wizard-steps .wizard-step[data-id=7]").addClass("d-none");
        $(".wizard-steps .wizard-step[data-id=8]").addClass("d-none");
        $("#spouse-riskarc").addClass("d-none");
    }

    var employmentStatus = $("#ClientSpouseOccupations").val();
    if (!employmentStatus.includes(EmploymentStatus.SELFEMPLOYED))
        $(".wizard-steps .wizard-step[data-id=14]").addClass("d-none");

    var hasPersonalAssets = $("#HasPersonalAssets").val() == 1;
    if (!hasPersonalAssets)
        $(".wizard-steps .wizard-step[data-id=16]").addClass("d-none");

    var hasInvestmentIncome = $("#HasInvestmentIncome").val() == 1;
    if (!hasInvestmentIncome)
        $(".wizard-steps .wizard-step[data-id=18]").addClass("d-none");

    $(".wizard-step:visible").each(function (i) {
        $(this).find(".wizard-order").html(`${i + 1}.`)
    });
}
var getYearToRetirement = function (birthdate, years) {
    var dobYear = new Date(birthdate).getFullYear();
    var retirementyears = (Number(dobYear) + Number(years)) - new Date().getFullYear();
    return retirementyears > 0 ? retirementyears : 0;
}
var setInvestmentExpressNextPrevTitleAction = function () {
    var currentItemEl = $("#kt_wizard_v2 .wizard-steps .wizard-step[data-wizard-state='current']");
    var nextItemEl = currentItemEl.nextAll(':visible').first();
    var arrow = "<i class='flaticon2-right-arrow icon-nm'></i>";

    var isIndepthBudgetVersion = $("#BudgetVersion").val() == 1;
    var budgetText = !isIndepthBudgetVersion ? " Express" : "";
    var budgetAction = !isIndepthBudgetVersion ? "-express" : "";
    $("#back-text").html(`Back to Budget Info${budgetText}`);
    $("#btn-back").attr("data-prev-action", `expense/budget-info${budgetAction}`);

    if (nextItemEl.length > 0) {
        var controller = nextItemEl.attr("data-controller");
        var action = nextItemEl.attr("data-action");
        var title = nextItemEl.find(".wizard-title").text().trim();
        $("#btn-submit,#btn-next").attr("data-original-title", `Save & Next to ${title} ${arrow} Intro`);
        $("#btn-submit,#btn-next").attr("data-next-action", `${controller}/${action}`);
    }
    else {
        $("#btn-submit,#btn-next").attr("data-original-title", "Save & Next to LifeStyle Intro");
        $("#btn-submit,#btn-next").attr("data-next-action", "lifestyle/life-style-intro");
    }
}
var gotoNextPrevAction = function (prev) {
    var fromExitButton = $("#FromExitButton").val() == "1";
    if (fromExitButton) {
        $.post("home/saveclientreports", function (response) {
            if (response.success) {
                window.location.href = "/welcome";
            }
        });
    }
    else {
        var action = prev ? $("#btn-back").attr("data-prev-action") : $("#btn-next").attr("data-next-action");
        var sp = action.split("/");
        var controller = sp[0];
        var targetAction = sp[1];
        var currentMenuEl = $("#kt_header_menu .menu-nav .menu-item.menu-item-active");
        var targetMenuEl = !prev ? currentMenuEl.next().find(".menu-link") : currentMenuEl.prev().find(".menu-link");

        var currentItemEl = $("#kt_wizard_v2 .wizard-steps .wizard-step[data-wizard-state='current']");
        var targetItemEl = !prev ? currentItemEl.nextAll(':visible').first() : currentItemEl.prevAll(':visible').first();

        var currentSubWizardEl = $("#wizard-3 .nav-item .nav-link.active").parent();
        var targetSubWizardEl = !prev ? currentSubWizardEl.nextAll(':visible').first() : currentSubWizardEl.prevAll(':visible').first();
        if ((targetAction.includes("riskarc") && prev) || targetAction == "health-expectations" || targetAction == "final-thought")
            window.location.href = targetAction;

        if (!prev && targetItemEl.length == 0 && targetSubWizardEl.length == 0) {
            targetMenuEl.click();
            if (targetAction == "life-style-intro" || targetAction == "investment-planning-intro") return false;
        }

        if (targetAction.includes("budget") && prev)
            $('.budget-disabled').click();
        else if (targetAction.includes("budget") && !prev)
            showVersionPopup("Budget");
        else if (targetAction == "" && !targetAction.includes("income") && !prev)
            targetMenuEl.click();
        else if (targetAction.includes("riskarc") && !prev) {
            if (!DisplayReadOnly)
                showVersionPopup("RiskArc");
            else {
                var indepth = $("#RiskArcVersion").val() == 1 ? true : false;
                if (targetAction.includes("client"))
                    window.location.href = `client-riskarc-${indepth ? "indepth" : "express"}`;
                else if (targetAction.includes("spouse")) {
                    var isMarried = $("#ClientIsMarried").val() == 1;
                    if (isMarried)
                        window.location.href = `spouse-riskarc-${indepth ? "indepth" : "express"}`;
                    else
                        $(`#CongratulationModal`).modal({ backdrop: 'static', keyboard: false });
                }
                else
                    $(`#CongratulationModal`).modal({ backdrop: 'static', keyboard: false });
            }
        }
        else {
            var activeMenuId = Number(currentItemEl.attr("data-id"));
            if (prev && targetItemEl.length == 0 && targetSubWizardEl.length == 0 && activeMenuId > 0)
                loadPartialView(`home/wizardnextprevnav?actionId=${activeMenuId}&prev=true`, "wizard-nav", "wizard-nav");

            loadPartialView(`${controller}/${targetAction.replace(/\/|-/g, '')}`, "wizard-content-div", "wizard-main-div", function () {
                window.history.pushState("data", "title", targetAction);
                setActiveWizard();
                if ((controller.toLowerCase().includes("planning") || action.toLowerCase().includes("social-security")) && !targetAction.includes("express"))
                    BindSubWizard();
                else {
                    $("#wizard-3").remove();
                    $("#wizard-submenu").html("");
                }
            });
        }
    }
}
var BindSubWizard = function () {
    var currentObj = $("#kt_wizard_v2 .wizard-steps .wizard-step[data-wizard-state='current']");
    var act = currentObj.attr("data-subactions");
    var parentId = currentObj.attr("data-id");
    if (act && (act.includes('planning') || act.includes('social-security')))
        loadPartialView(`Home/WizardSubNav?parentId=${parentId}`, "wizard-submenu");
}
var setWizardHeight = function () {
    var length = $(".wizard-steps .wizard-step").length;
    if (length > 6) {
        var height = 70;
        $(".wizard-content-height").css("min-height", `${height * length - 40}px`);
    }
}
var AjaxLinkStart = function () {
    var blockId = $(this).attr('data-ajax-update');
    KTApp.block(`${blockId}`, { state: 'primary', message: 'Please wait...' });
}
var AjaxLinkComplete = function () {
    var blockId = $(this).attr('data-ajax-update');
    KTApp.unblock(`${blockId}`);
    window.history.pushState("data", "title", this.href);
    $('.menu-item').removeClass('menu-item-active');
    setActiveMainMenu();
    var title = $('.menu-nav .menu-item.menu-item-active').find(".menu-text").text().trim();
    document.title = `LifeArcPlan™ | ${title}`;
}
var DisableClientInputControl = function () {
    $(".wizard-content-height input,.wizard-content-height select,.wizard-content-height textarea,.modal-body input").prop("disabled", true);
    $(".wizard-content-height select").selectpicker('refresh');
    $(".wizard-content-height select").closest(".form-group").addClass("disabled-div");
    $(".wizard-content-height label.radio,.modal-body label.radio").addClass("radio-disabled");
    $(".wizard-content-height label.checkbox,.modal-body label.checkbox").addClass("checkbox-disabled");
    $(".btn-heir-add,.btn-budget-add,.btn-property-add,.btn-employment-add,.btn-pre-esp-add,.btn-OtherIncome-add,.btn-businessInterest-add,.btn-bankcreditunionaccounts-add,.btn-brokerageadvisoryaccounts-add,.btn-lifeannuities-add,.btn-EducationPlanning-add").remove();
    $(".card-toolbar,.table-budget td:last-child").remove();
    $(".form-control-file").closest(".form-group").remove();
    $(".radio-pen").css("pointer-events", "none").css("opacity", ".5");
    $(".radio-pen").parent().addClass("disabled-div");
    $(".riskarc-survey input").prop("disabled", true);
    $(".riskarc-survey label.radio,.modal-body label.radio").addClass("radio-disabled");
    $("#btn-submit").remove();
    $("#change-email-btn").remove();
    $("#btn-next").removeClass("d-none");
    var url = window.location.pathname;
    if (url.includes("client-riskarc")) {
        var maritalStatus = $("#ClientMaritalStatus").val();
        if (maritalStatus != MaritalStatus.MARRIED)
            $("#btn-next").addClass("d-none");
    }
    else if (url.includes("spouse-riskarc"))
        $("#btn-next").addClass("d-none");
}
var LoadExplicitVue = function () {
    $("#explicitvue-spinner").removeClass("d-none");
    var yearToRetire = $("#ClientYearToRetire").val();
    if (yearToRetire)
        loadPartialView('home/ExPlicItVue', 'ex-plic-it-vue', null, function () {
            $("#explicitvue-spinner").addClass("d-none");
            $("#ex-plic-it-vue").removeClass("d-none");
            $("#exp-msg").addClass("d-none");
        });
    else {
        $("#ex-plic-it-vue").addClass("d-none");
        $("#exp-msg").removeClass("d-none");
        setTimeout(function () { $("#explicitvue-spinner").addClass("d-none"); }, 1000);
    }
}
var goBack = function () {
    if (history.length > 1 && !window.location.search.includes("back=true"))
        history.go(-1);
    else
        window.location.href = "/client-info";
}
$(document).on('click', '#btn-back', function (e) {
    var href = $(e.target).parent().attr("data-prev-action");
    if (href != '/') {
        var btn = KTUtil.getById(e.target.id);
        KTUtil.btnWait(btn, "spinner spinner-right spinner-darker-primary pr-15", "Please wait");
        gotoNextPrevAction("back");
        setTimeout(function () { KTUtil.btnRelease(btn); }, 500);
    }
});
$(document).on('click', '.btn-next', function (e) {
    var href = $(e.target).parent().attr("data-next-action");
    if (href != '/') {
        var btn = KTUtil.getById(e.target.id);
        var spinnerClass = e.target.id == "btn-save-exit" ? "dark" : "white";
        KTUtil.btnWait(btn, `spinner spinner-right spinner-${spinnerClass} pr-15`, "Please wait");
        gotoNextPrevAction();
        setTimeout(function () { KTUtil.btnRelease(btn); }, 500);
    }
});
$(document).on('click', '.riskarc-link', function (e) {
    riskArcType = $(this).attr("data-type");
    showVersionPopup("RiskArc", riskArcType);
});
$(document).on('click', "#RiskArcVersionModal #btn_submit_riskarc", function (e) {
    e.preventDefault();
    var isInDepth = $('[name="RiskArcVersion"]:checked').val() == 1;
    riskArcType = riskArcType.length > 0 ? riskArcType : "client";
    updatesinglevalue("ClientData", "RiskArcVersion", isInDepth, function () {
        window.location.href = `${riskArcType}-riskarc-${isInDepth ? "indepth" : "express"}`;
    });
});
$(document).on('click', "#RiskArcVersionModal [name='RiskArcVersion']", function (e) {
    var version = $(this).val();
    $("#RiskArcVersion").val(version);
});

$(document).on('click', '.planning-disabled', function (e) {
    var InitialProfileSetupCompleted = $("#InitialProfileSetupCompleted").val();
    if (InitialProfileSetupCompleted == 0) {
        $('#planningModal').modal({
            backdrop: 'static', keyboard: false
        });
        $("select.form-control").selectpicker();
    }
    else
        showVersionPopup("Investment");
});
$(document).on('click', '.budget-disabled', function (e) {
    showVersionPopup("Budget");
});
$(document).on('click', "#BudgetVersionModal #btn_submit_budget", function (e) {
    e.preventDefault();
    var isInDepth = $("input[name='BudgetVersion']:checked").val() == 1;
    updatesinglevalue("ClientData", "BudgetVersion", isInDepth, function () {
        var action = isInDepth ? "budget-info" : "budget-info-express";
        if ($(".wizard-content-div").length) {
            $('#BudgetVersionModal').modal("hide");
            loadPartialView(`expense/${action.replace(/-/g, '')}`, "wizard-content-div", "wizard-main-div");
            loadPartialView(`home/wizardnav?parentId=3`, "wizard-nav", "wizard-nav", function () {
                window.history.pushState("data", "title", action);
                setActiveWizard();
                $("#back-text").text(`Back to Family Info`);
                $("#btn-submit,#btn-next").attr("data-original-title", `Save & Next to Personal Property`);
                LoadExplicitVue();
                $("#wizard-3").remove();
                $("#wizard-submenu").html("");
            });
        }
        else
            window.location.href = action;
    });

});
$(document).on('click', "#InvestmentVersionModal #btn_submit_investment", function (e) {
    e.preventDefault();
    var isInDepth = $("input[name='InvestmentVersion']:checked").val() == "1";
    updatesinglevalue("ClientData", "InvestmentVersion", isInDepth, function () {
        var action = isInDepth ? "investment-planning-intro" : "investment-planning-express";
        if ($(".wizard-content-div").length) {
            $('#InvestmentVersionModal').modal("hide");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            loadPartialView(`investmentplanning/${action.replace(/-/g, '')}`, "wizard-content-div", "wizard-main-div");
            loadPartialView(`home/wizardnav?parentId=4`, "wizard-nav", "wizard-nav", function () {
                window.history.pushState("data", "title", action);
                setActiveWizard();
                if (isInDepth == 1)
                    BindSubWizard();
                LoadExplicitVue();
            });
        }
        else
            window.location.href = action;
    });
});
$(document).on("change", "#BudgetVersionModal [name='BudgetVersion']", function () {
    var version = $(this).val();
    $("#BudgetVersion").val(version);
});
$(document).on("change", "#InvestmentVersionModal [name='InvestmentVersion']", function () {
    var version = $(this).val();
    $("#InvestmentVersion").val(version);
});
$(document).on('click', '#btn-modal-submit', function () {
    if ($("#budgetFrm").valid()) {
        var btn = this;
        var parentDiv = "AddBudgetModal";
        var totalCount = $(`#${parentDiv} .none-zero`).length;
        var emptyTextBoxCount = getEmptyTextBoxCount(parentDiv);
        if (totalCount > 0) {
            if (totalCount != emptyTextBoxCount) {
                KTUtil.btnWait(btn, "spinner spinner-right spinner-white pr-15", "Please wait");
                var parentId = $("#BudgetType").val();
                $.post("expense/addbudgets", { clientBudgets: items, parentId: parentId }, function (response) {
                    $("#AddBudgetModal").modal("hide");
                    if (response.success) {
                        toastr.options = toastrOptions;
                        toastr.success(response.message);
                        if (response.data) {
                            response.data.totalFormated = currencyFormat.format(response.data.total);
                            response.data.totalBalanceOwnFormated = currencyFormat.format(response.data.totalBalanceOwn);
                            var div = $(`#budget-card-${parentId}`)
                            if (div.length > 0) {
                                var color = $(`#budget-card-${parentId}`).attr("data-color");
                                response.data.Cardcolor = color;
                                $.get('template/budget-card-template.html', function (template) {
                                    div.parent().html($.tmpl(template, response.data));
                                });
                            }
                            else {
                                var existingcolors = getExistingColor();
                                var nextColor = allcolors.filter(x => !existingcolors.includes(x));
                                response.data.Cardcolor = nextColor[0];
                                response.data.action = "add";
                                $.get('template/budget-card-template.html', function (template) {
                                    $("#budget-main-div").prepend($.tmpl(template, response.data));
                                });
                            }
                        }
                        LoadExplicitVue();
                    }
                    else {
                        toastr.options = toastrOptions;
                        toastr.warning(response.message);
                    }
                    KTUtil.btnRelease(btn);
                });
            }
            else
                _showFormMessage("main-div", "danger", "Please insert annual amount in one row at least.", "fas fa-exclamation-triangle");
        }
    }
});
$(document).on("change", ".is-active", function () {
    $(".alert").remove();
    var tr = $(this).closest("tr");
    if ($(this).is(":checked")) {
        tr.find(".importance").removeClass("d-none");
        tr.find(".dash").addClass("d-none");
    }
    else {
        tr.find(".importance").addClass("d-none");
        tr.find(".dash").removeClass("d-none");
    }
    var checkedCount = $('.is-active').filter(':checked').length;
    if (checkedCount > 0)
        $("#planningModal .close").removeClass("d-none");
    else
        $("#planningModal .close").addClass("d-none");
});
$(document).on("submit", "#frm-planning-items", function (e) {
    e.preventDefault();
    var cur = this;
    var checkedCount = $('.is-active').filter(':checked').length;
    if (checkedCount > 0) {
        var id = $(cur).find(":submit").attr('id');
        var btn = KTUtil.getById(id);
        KTUtil.btnWait(btn, "spinner spinner-right spinner-white pr-15", "Please wait");
        _hideErrorMessage();
        $.post("home/saveplanningitems", $(cur).serialize(), function (response) {
            var modalId = $('.modal:visible').attr("id");
            $(`#${modalId}`).modal('hide');
            localStorage.setItem("planningModal", true);
            var firstPlanningAction = $(".planning-table tbody tr td .switch [type='checkbox']:checked:first").attr("data-action");
            if (!firstPlanningAction.includes("investment"))
                window.location.href = firstPlanningAction;
            else {
                var isInDepth = $(`#InvestmentVersion`).val();
                $(`input[name='InvestmentVersion'][value=${isInDepth}]`).prop('checked', true);
                $(`#InvestmentVersionModal`).modal({ backdrop: 'static' });
                localStorage.setItem("last-action", window.location.pathname);
            }
            KTUtil.btnRelease(btn);
        });
    }
    else
        _showFormMessage("frm-planning-items", "danger", "Please select at least one of the areas of planning.", "fas fa-exclamation-triangle");
});
$(document).ajaxComplete(function () {
    setActiveMainMenu();
    $('.input-group.datepicker-control').datepicker({
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        format: 'mm/dd/yyyy',
        startDate: '-100y',
        endDate: '+100y'
    });
    $(".datemask").inputmask("99/99/9999", { "placeholder": "mm/dd/yyyy" });
    $(".phone").inputmask({ "mask": "(999) 999-9999" });
    $(".zipcode").inputmask({ "mask": "99999" });
    $(".currency").not(".currency-total").attr("maxlength", 12);
    $(".currency").inputmask(CurrencyMaskOptions);
    $(".percentage").inputmask(PercentageMaskOption);
    $(".numericOnly").inputmask(NumericMaskOption);
    $(".year-mask").inputmask(YearMaskOption);
    $(".currency").each(function () {
        var curVal = $(this).val();
        if (curVal.length == 0)
            $(this).val(0);
    });
    var select2El = $("select.form-control").not(".select2");
    select2El.not(".importance").attr("title", "-Select-");
    select2El.selectpicker("refresh");
    select2El.selectpicker();
    $('[data-toggle="tooltip"], .tooltip').tooltip("hide");
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    $('[data-toggle="popover"]').popover({ trigger: "click", cotainer: 'body' })

    $(".form-control:not('#Email,#Password')").attr("data-lpignore", true);
    $(".form-control").each(function () {
        var maxlength = $(this).attr("maxlength");
        if (maxlength == undefined)
            $(this).attr("maxlength", 20);
    });
    setWizardHeight();
    var DisplayReadOnly = $("#DisplayOnlyClient").val();
    if (DisplayReadOnly == 1)
        DisableClientInputControl();
});
$(document).on("input", ".year-mask", function (e) {
    if ($(this).val() > 99)
        $(this).val(99);
})
$(function () {
    basePath = $("base").attr("href");
    $(".form-control:not('#Email,#Password')").attr("data-lpignore", true);
    LoadExplicitVue();
    setActiveMainMenu();
    hideUnhideWizardItem();
    setActiveRiskArcMenu();
})