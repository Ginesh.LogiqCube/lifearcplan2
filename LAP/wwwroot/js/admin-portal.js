﻿var setActiveMenu = function () {
    var url = window.location.pathname.replace("/", "");
    $("#kt_aside_menu .menu-nav .menu-item").find(`[href='${url}']`).parent().addClass("menu-item-active")
}
var gotoNextPrevAction = function (prev) {
    var fromExitButton = $("#FromExitButton").val() == "1";
    if (fromExitButton) {
        $.post("admin/adviser/saveadviserreports", function (response) {
            if (response.success) {
                var adviserLastPageHref = localStorage.getItem("adviserLastPageHref");
                window.location.href = adviserLastPageHref;
            }
        });
    }
    else {
        var action = prev ? $("#btn-back").attr("data-prev-action") : $("#btn-next").attr("data-next-action");
        loadPartialView(`adviser/profile/${action.replace("adviser", "").replace(/\/|-/g, '')}`, "wizard-content-div", "wizard-main-div");
        window.history.pushState("data", "title", action);
        setActiveWizard();
    }
}
var UpdateAdv2BValid = function () {
    $.get("admin/adviser/isadv2bValid", function (response) {
        var isValidAdv2B = response.data.isValidAdv2B ? 1 : 0;
        $("#hdnIsAdv2bValid").val(isValidAdv2B);
    })
}
var goBackAdviser = function (role) {
    if (history.length > 1 && !window.location.search.includes("back=true"))
        history.go(-1);
    else
        window.location.href = `${role}/client-list`;
}
var DisableAdviserInputControl = function () {
    $(".wizard-content-height input,.wizard-content-height select,.wizard-content-height textarea,.modal-body input").prop("disabled", true);
    $(".wizard-content-height input").attr("placeholder", "");
    $(".wizard-content-height select").selectpicker('refresh');
    $(".wizard-content-height select").closest(".form-group").addClass("disabled-div");
    $(".wizard-content-height label.radio,.modal-body label.radio").addClass("radio-disabled");
    $(".wizard-content-height label.checkbox,.modal-body label.checkbox").addClass("checkbox-disabled");
    $(".btn-heir-add,.btn-budget-add,.btn-property-add,.btn-employment-add,.btn-pre-esp-add,.btn-OtherIncome-add,.btn-businessInterest-add,.btn-bankcreditunionaccounts-add,.btn-brokerageadvisoryaccounts-add,.btn-lifeannuities-add,.btn-EducationPlanning-add").remove();
    $(".card-toolbar:not(.integration),.table-budget td:last-child,#btn-submit,#btn-save-exit,.btn-change-avatar,.btn-remove-avatar,.btn-change-logo,.btn-remove-logo,.btn-education-add,.btn-work-add,.btn-firm-edit").remove();
    $(".wizard-content-height a").attr("href", "javascript:").attr("target", "");
    $("#btn-next").removeClass("d-none");
    $(".wizard-steps .wizard-step").each(function () {
        var action = $(this).attr("data-action");
        if (!action.includes("flag"))
            $(this).attr("data-action", `${action}?flag=true`);
    });
    var prevEl = $("#btn-back");
    var prevAction = prevEl.attr("data-prev-action");
    if (prevAction && !prevAction.includes("flag"))
        prevEl.attr("data-prev-action", `${prevAction}?flag=true`);

    var nextEl = $("#btn-next");
    var nextAction = nextEl.attr("data-next-action");
    if (nextAction && !nextAction.includes("flag"))
        nextEl.attr("data-next-action", `${nextAction}?flag=true`);
}
var showOverLappedActivity = function (start, end, guid, frm_id, msg) {
    var swalConfirmationOptions = confimationSwalOptions2;
    swalConfirmationOptions.html = `${msg}</br><div id='swal-custom-container'></div>`;
    swalConfirmationOptions.onOpen = function () {
        Swal.showLoading()
        $.get(`adviser/dashboard/OverlappedActivity?start=${start}&end=${end}&guid=${guid}`, function (response) {
            $("#swal-custom-container").html(response);
            Swal.hideLoading();
        });
    }
    Swal.fire(swalConfirmationOptions).then(function (result) {
        if (result.value) {
            $("#confirmed").val(true);
            $(`#${frm_id}`).submit();
        }
    });
}
$(document).on('click', '#btn-back', function (e) {
    var btn = KTUtil.getById(e.target.id);
    KTUtil.btnWait(btn, "spinner spinner-right spinner-darker-primary pr-15", "Please wait");
    gotoNextPrevAction("back");
    setTimeout(function () { KTUtil.btnRelease(btn); }, 500);
});
$(document).on('click', '.btn-next', function (e) {
    var btn = KTUtil.getById(e.target.id);
    var spinnerClass = e.target.id == "btn-save-exit" ? "dark" : "white";
    KTUtil.btnWait(btn, `spinner spinner-right spinner-${spinnerClass} pr-15`, "Please wait");
    gotoNextPrevAction();
    setTimeout(function () { KTUtil.btnRelease(btn); }, 500);
});
$(document).ajaxComplete(function () {
    $(".form-control:not('#Email,#Password')").attr("data-lpignore", true);
    $('[data-toggle="tooltip"], .tooltip').tooltip("hide");
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
    var select2El = $("select.form-control").not(".select2");
    select2El.attr("title", "-Select-");
    select2El.selectpicker("refresh");
    select2El.selectpicker();
    $('.input-group.datepicker-control').datepicker({
        orientation: "bottom left",
        autoclose: true,
        todayHighlight: true,
        format: 'mm/dd/yyyy',
        startDate: '-100y',
        endDate: '+100y'
    });
    $(".currency").not(".currency-total").attr("maxlength", 12);
    $(".currency").inputmask(CurrencyMaskOptions);
    $(".datemask").attr("placeholder", "mm/dd/yyyy").inputmask("99/99/9999", { "placeholder": "mm/dd/yyyy" });
    $(".yearmask").inputmask("99/9999", { "placeholder": "mm/yyyy" });
    $(".phone").inputmask({ "mask": "(999) 999-9999" });
    $(".percentage").inputmask(PercentageMaskOption);
    $(".numericOnly").inputmask(NumericMaskOption);
    $(".zipcode").inputmask({ "mask": "99999" });
    $(".year-mask").inputmask(YearMaskOption);
    $(".gpa-mask").inputmask(GPAMaskOption).attr("maxlength", 4);
    $(".form-control:not('#Email,#Password')").attr("data-lpignore", true);
    $(".form-control").not("textarea").each(function () {
        var maxlength = $(this).attr("maxlength");
        if (maxlength == undefined)
            $(this).attr("maxlength", 20);
    });
    var DisplayReadOnly = $("#DisplayOnlyAdviser").val();
    if (DisplayReadOnly == 1)
        DisableAdviserInputControl();
});
$(function () {
    setActiveMenu();
});