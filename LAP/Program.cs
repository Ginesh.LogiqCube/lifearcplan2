using LAP.Areas.Adviser.Controllers;
using LAP.Controllers;
using LAP.DomainModels;
using LAP.DomainModels.BackgroundTask;
using LAP.DomainModels.Entities;
using LAP.Filters;
using LAP.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Rotativa.AspNetCore;
using System;

namespace LAP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddAuthentication(AuthenticationSchemes.ClientAuth)
            .AddCookie(AuthenticationSchemes.ClientAuth, options =>
            {
                options.LoginPath = new PathString("/client/login");
                options.Events = new CookieAuthenticationEvents()
                {
                    OnValidatePrincipal = ArchivedUserValidator.ValidateClientAsync,
                };
            })
            .AddCookie(AuthenticationSchemes.UserAuth, options =>
            {
                options.LoginPath = new PathString("/user/login");
                options.Events = new CookieAuthenticationEvents()
                {
                    OnValidatePrincipal = ArchivedUserValidator.ValidateUserAsync,
                };
            })
            .AddCookie(AuthenticationSchemes.AdviserAuth)
            .AddCookie(AuthenticationSchemes.SuperAdminAuth, options =>
            {
                options.LoginPath = new PathString("/super-admin/login");
            });
            var configuration = builder.Configuration;
            builder.Services.AddDbContext<LifeArcPlanContext2>(o => o.UseSqlServer(configuration["ConnectionStrings:LAP2DB"]));
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            builder.Services.AddHostedService<QueuedHostedService>();
            builder.Services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            builder.Services.AddHttpContextAccessor();
            builder.Services.AddScoped<PermissionAttribute>();
            builder.Services.AddTransient<CommonService>();
            builder.Services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            builder.Services.AddTransient<Controllers.HomeController, Controllers.HomeController>();
            builder.Services.AddTransient<ReportController, ReportController>();
            builder.Services.AddTransient<RiskArcController, RiskArcController>();
            builder.Services.AddTransient<ArcHiveService, ArcHiveService>();
            builder.Services.AddTransient<ActivityLogService, ActivityLogService>();
            builder.Services.AddControllersWithViews(config => config.Filters.Add(typeof(CustomExceptionFilter)));
            var app = builder.Build();

            if (!app.Environment.IsProduction())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error-500");
                app.UseStatusCodePagesWithReExecute("/error-{0}");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            RotativaConfiguration.Setup(app.Environment.WebRootPath);
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                {
                    int maxAge = 60 * 60 * 24;
                    context.Context.Response.Headers["Cache-Control"] = $"public,max-age={maxAge}";
                }
            });
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCookiePolicy();
            app.RouteConfigution();
            app.Run();
        }
    }
}