﻿using AutoMapper;
using LAP.DomainModels.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;

namespace LAP
{
    public class BaseController : Controller, IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IWebHostEnvironment _env;
        protected readonly IConfiguration _config;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly IMapper _mapper;
        private bool disposed;

        public BaseController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper)
        {
            _context2 = context2;
            _env = env;
            _config = config;
            _httpContextAccessor = httpContextAccessor;
            _mapper = mapper;
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~BaseController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
