﻿using AutoMapper;
using LAP.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Models;
using LAP.DomainModels.Entities.Admin;

namespace LAP
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ClientData, ClientInfo>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore())
                 .ForMember(dest => dest.ActiveService, act => act.Ignore())
                 .ForMember(dest => dest.Discharge, act => act.Ignore());

            CreateMap<ClientData, MaritalInfo>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<SpouseData, ClientInfo>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore())
                 .ForMember(dest => dest.ActiveService, act => act.Ignore())
                 .ForMember(dest => dest.Discharge, act => act.Ignore());

            CreateMap<SpouseData, MaritalInfo>()
                 .ForMember(dest => dest.SpouseFirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.SpouseMiddleInitial, opt => opt.MapFrom(src => src.MiddleInitial))
                 .ForMember(dest => dest.SpouseLastName, opt => opt.MapFrom(src => src.LastName))
                 .ReverseMap();

            CreateMap<XSpouseData, MaritalInfo>()
                 .ForMember(dest => dest.XSpouseFirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.XSpouseMiddleInitial, opt => opt.MapFrom(src => src.MiddleInitial))
                 .ForMember(dest => dest.XSpouseLastName, opt => opt.MapFrom(src => src.LastName))
                 .ReverseMap();

            CreateMap<ClientContactInfo, ContactInfoViewModel>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<Heir, Heir>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientProperty, ClientProperty>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientBudget, ClientBudget>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientData, BudgetLiteInfo>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<InvestmentLiteData, InvestmentLiteInfo>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientOccupation, OccupationModel>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientEsp, EspModel>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientPrevEsp, ClientPrevEsp>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<BankCreditUnionAccount, BankCreditUnionAccount>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<BrokerageAdvisoryAccount, BrokerageAdvisoryAccount>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<LifeAnnuityContract, LifeAnnuityContract>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<BusinessInterest, BusinessInterest>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<OtherIncome, OtherIncome>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<SocialSecurity, SocialSecurity>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientEstateItem, ClientEstateItem>()
                 .ForMember(dest => dest.Id, act => act.Ignore());

            CreateMap<CharitablePlanning, CharitableModel>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<PersonalCare, PersonalCare>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<EducationPlanning, EducationPlanning>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientHealthItem, ClientHealthItem>()
                 .ForMember(dest => dest.Id, act => act.Ignore());

            CreateMap<PersonalCare, PersonalCare>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<RetirementStatus, RetirementStatus>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ClientId, act => act.Ignore());

            CreateMap<ClientIncomeConcern, ClientIncomeConcern>()
                 .ForMember(dest => dest.Id, act => act.Ignore());

            CreateMap<ClientExpenseConcern, ClientExpenseConcern>()
                 .ForMember(dest => dest.Id, act => act.Ignore());

            CreateMap<UserLogin, UserLogin>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.Password, act => act.Ignore())
                 .ForMember(dest => dest.LastLogin, act => act.Ignore())
                 .ForMember(dest => dest.SetupCompletedDate, act => act.Ignore());

            CreateMap<ComplianceDocument, ComplianceDocument>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ReviewedById, act => act.Ignore())
                 .ForMember(dest => dest.ReviewedDate, act => act.Ignore())
                 .ForMember(dest => dest.ComplianceNote, act => act.Ignore())
                 .ForMember(dest => dest.TerminationType, act => act.Ignore())
                 .ForMember(dest => dest.Allegation, act => act.Ignore());

            CreateMap<InsuranceDocument, InsuranceDocument>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.ReviewedById, act => act.Ignore())
                 .ForMember(dest => dest.ReviewedDate, act => act.Ignore())
                 .ForMember(dest => dest.ReviewerNote, act => act.Ignore());

            CreateMap<UserLogin, UserLogin>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.Password, act => act.Ignore())
                 .ForMember(dest => dest.LastLogin, act => act.Ignore())
                 .ForMember(dest => dest.SetupCompletedDate, act => act.Ignore());

            CreateMap<UserData, UserData>()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.UserId, act => act.Ignore())
                 .ForMember(dest => dest.UserLogin, act => act.Ignore())
                 .ForMember(dest => dest.Guid, act => act.Ignore())
                 .ForMember(dest => dest.DegreeDesignation, act => act.Ignore())
                 .ForMember(dest => dest.License, act => act.Ignore())
                 .ForMember(dest => dest.CRD, act => act.Ignore())
                 .ForMember(dest => dest.FMOId, act => act.Ignore())
                 .ForMember(dest => dest.BDId, act => act.Ignore())
                 .ForMember(dest => dest.RIAId, act => act.Ignore())
                 .ForMember(dest => dest.BDDisclosure, act => act.Ignore())
                 .ForMember(dest => dest.RIADisclosure, act => act.Ignore());

            CreateMap<PostHighSchoolEducation, PostHighSchoolEducation>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.UserLogin, act => act.Ignore())
                 .ForMember(dest => dest.UserId, act => act.Ignore());

            CreateMap<Work, Work>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore())
                  .ForMember(dest => dest.UserLogin, act => act.Ignore())
                 .ForMember(dest => dest.UserId, act => act.Ignore());

            CreateMap<Affiliation, Affiliation>().ReverseMap()
                 .ForMember(dest => dest.Id, act => act.Ignore());

            CreateMap<ReleaseNote, ReleaseNote>().ReverseMap()
                .ForMember(dest => dest.Id, act => act.Ignore());

            CreateMap<AdviserContactInfo, ContactInfoViewModel>().ReverseMap()
                 //.ForMember(dest => dest.Id, act => act.Ignore())
                 .ForMember(dest => dest.UserId, act => act.Ignore());

            CreateMap<ClientRiskArc, ClientRiskArc>().ReverseMap()
                ;

            CreateMap<List<int>, string>().ConvertUsing(new IntListToStringConverter());
            CreateMap<string, List<int>>().ConvertUsing(new StringToIntListConverter());
        }
    }
    public class StringToIntConverter : ITypeConverter<string, int>
    {
        public int Convert(string source, int destination, ResolutionContext context)
        {
            return source.StringToInt();
        }
    }
    public class StringToDecimalConverter : ITypeConverter<string, decimal>
    {
        public decimal Convert(string source, decimal destination, ResolutionContext context)
        {
            return source.StringToDecimal();
        }
    }
    public class IntListToStringConverter : ITypeConverter<List<int>, string>
    {
        public string Convert(List<int> source, string destination, ResolutionContext context)
        {
            return source is not null ? string.Join(",", source) : null;
        }
    }
    public class StringToIntListConverter : ITypeConverter<string, List<int>>
    {
        public List<int> Convert(string source, List<int> destination, ResolutionContext context)
        {
            return source?.Split(',').Select(Int32.Parse).ToList();
        }
    }
}