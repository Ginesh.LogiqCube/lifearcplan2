﻿using Microsoft.AspNetCore.Mvc;

namespace LAP.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult UnAuthorize()
        {
            return View("_UnAuthorize");
        }
        [Route("error-{statusCode}")]
        public IActionResult ErrorHandle(int statusCode)
        {
            switch (statusCode)
            {
                case 403:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.Title = "Access Denied";
                    ViewBag.Message = "Sorry, You don't have permission to access this page.";
                    break;
                case 404:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.Title = "Page not found";
                    ViewBag.Message = "Sorry, the page you requested could not be found";
                    break;
                case 500:
                    ViewBag.StatusCode = statusCode;
                    ViewBag.Title = "Internal server error";
                    ViewBag.Message = "Please contact your administrator";
                    break;
            }
            return View("ErrorPage");
        }
    }
}