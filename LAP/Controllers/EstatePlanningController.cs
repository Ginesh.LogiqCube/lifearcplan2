﻿using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAP.Filters;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class EstatePlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public EstatePlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 9;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "estate-planning-intro" })]
        public async Task<IActionResult> EstatePlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("estate-planning-intro", ClientLoginGuid);
            return View("EstatePlanningIntro");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "estate-planning-assessment" })]
        public async Task<IActionResult> EstatePlanningAssessment()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var clientEstateItems = await _commonService.GetEstateItemAsync(ClientLoginGuid);
            ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("estate-planning-assessment", ClientLoginGuid);
            return View("GeneralAssessment", clientEstateItems);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEsPlanning(List<ClientEstateItem> clientEstateItem)
        {
            var estateItems = _context2.ClientEstateItems.Where(m => clientEstateItem.Select(m => m.Id).Contains(m.Id));
            if (estateItems.Any())
            {
                foreach (var item in estateItems)
                {
                    item.isClientActive = clientEstateItem.FirstOrDefault(n => n.Id == item.Id).isClientActive;
                    item.isSpouseActive = clientEstateItem.FirstOrDefault(n => n.Id == item.Id).isSpouseActive;
                }
            }
            else
            {
                clientEstateItem = clientEstateItem.Select(m => { m.ClientId = ClientId; return m; }).ToList();
                _context2.ClientEstateItems.AddRange(clientEstateItem);
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = clientEstateItem is not null,
                Message = clientEstateItem is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~EstatePlanningController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}