﻿using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAP.Areas.Adviser.Models;
using System.IO;
using LAP.Filters;
using System.Text;
using LAP.DomainModels.Models;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class RiskArcController : BaseController, IDisposable
    {
        protected readonly FileHelper _fileHelper;
        protected readonly CommonService _commonService;
        protected readonly ReportService _reportService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public RiskArcController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _fileHelper = new FileHelper(_config);
            _commonService = new CommonService(_context2);
            _reportService = new ReportService(_context2, _httpContextAccessor);
            ClientLoginGuid = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid() ?? Guid.Empty;
            ClientId = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt() ?? 0;
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "client-riskarc" })]
        public async Task<IActionResult> ClientRiskArc(bool isIndepth)
        {
            ClientTypeEnum clientType = ClientTypeEnum.Client;
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            clientData.RiskArcVersion = isIndepth;
            await _context2.SaveChangesAsync();
            bool isMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.IsMarried = isMarried;
            ViewBag.Name = clientData.FirstName;
            ViewBag.IsInDepth = isIndepth;
            ViewBag.ClientType = (int)clientType;
            ViewBag.RiskArcQuestions = isIndepth ? GetRiskArcQuestionsWithAnswers(clientType, ClientLoginGuid) : (GetRiskArcQuestionsWithAnswers(clientType, ClientLoginGuid)).Where(m => m.SectionId <= 15);

            ViewBag.nextPrev = new PrevNextMenuModel
            {
                NextTitle = isMarried ? $"{client.SpouseData.FirstName}'s RiskArc" : "",
                NextAction = isMarried ? "riskarc/spouse-riskarc" : "custom-action"
            };
            return View("RiskArc");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "client-riskarc" })]
        public async Task<IActionResult> SpouseRiskArc(bool isIndepth)
        {
            ClientTypeEnum clientType = ClientTypeEnum.Spouse;
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.IsMarried = IsMarried;
            ViewBag.Name = IsMarried ? clientData.ClientLogin.SpouseData.FirstName : null;
            ViewBag.IsInDepth = isIndepth;
            ViewBag.ClientType = (int)clientType;
            ViewBag.RiskArcQuestions = isIndepth ? GetRiskArcQuestionsWithAnswers(clientType, ClientLoginGuid) : (GetRiskArcQuestionsWithAnswers(clientType, ClientLoginGuid)).Where(m => m.SectionId <= 15);
            bool isExpress = !client.ClientData.RiskArcVersion;
            var RiskArcText = isExpress ? " Express" : "";
            var RiskArcAction = isExpress ? "express" : "indepth";
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = $"{clientData.FirstName}'s RiskArc",
                PrevAction = $"riskarc/client-riskarc-{RiskArcAction}",
                NextTitle = "",
                NextAction = "/custom-action"
            };
            return View("RiskArc");
        }
        public IEnumerable<RiskArcQuestionModel> GetRiskArcQuestionsWithAnswers(ClientTypeEnum RiskArcClientType, Guid ClientLoginGuid)
        {
            var clientRiskArcAnswers = _commonService.GetRiskArcQuestionsWithAnswers(ClientLoginGuid, RiskArcClientType);
            return clientRiskArcAnswers;
        }
        [HttpPost]
        public async Task<IActionResult> SaveRiskArc(IEnumerable<ClientRiskArc> clientRiskArcs, ClientTypeEnum RiskArcClientType, bool isIndepth = false)
        {
            var riskArcQuestions = _context2.ClientRiskArcs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.ClientType == RiskArcClientType);
            if (!isIndepth)
            {
                var inDepthQuestions = riskArcQuestions.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.SectionId > 15);
                _context2.ClientRiskArcs.RemoveRange(inDepthQuestions);
                riskArcQuestions = riskArcQuestions.Where(m => m.SectionId <= 15);
            }
            bool hasChanges = false;
            if (riskArcQuestions.Any())
            {
                foreach (var item in riskArcQuestions)
                {
                    var newAnswer = clientRiskArcs.FirstOrDefault(n => n.Id == item.Id).Answer;
                    if (newAnswer != item.Answer)
                    {
                        item.Answer = newAnswer;
                        if (!hasChanges) hasChanges = true;
                    }
                }

                clientRiskArcs = clientRiskArcs.Where(m => m.Answer > 0).Select(m =>
                {
                    m.ClientType = RiskArcClientType;
                    m.ClientId = ClientId;
                    return m;
                });
            }
            if (clientRiskArcs.Count() != riskArcQuestions.Count())
            {
                hasChanges = true;
                var additionalQuestions = clientRiskArcs.Where(m => m.Answer > 0 && !riskArcQuestions.Select(n => n.SectionId).Contains(m.SectionId));
                clientRiskArcs = additionalQuestions.Select(m =>
                {
                    m.ClientType = RiskArcClientType;
                    m.ClientId = ClientId;
                    return m;
                });
                _context2.ClientRiskArcs.AddRange(clientRiskArcs);
            }
            if (hasChanges)
            {
                var clientLogin = await _context2.ClientLogins.FirstOrDefaultAsync(m => m.Guid == ClientLoginGuid);
                if (RiskArcClientType == ClientTypeEnum.Client)
                    clientLogin.HasChangesClientRiskArc = true;
                else
                    clientLogin.HasChangesSpouseRiskArc = true;
            }
            await _context2.SaveChangesAsync();
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid).Include(m => m.ClientData).Include(m => m.UserLogin).FirstOrDefaultAsync();
            bool isMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
            bool sendEmail = (isMarried && RiskArcClientType == ClientTypeEnum.Spouse) || (!isMarried && RiskArcClientType == ClientTypeEnum.Client);
            string message = string.Format(EmailNotification.SURVEYCOMLETION, "RiskArc");
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && sendEmail)
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
                var emailText = message;
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(client.Username, client.UserLogin.Username, _config["Email:BccEmail"], EmailSubject.RiskArcSurveyCompletion, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = true,
                Message = message
            };
            return Json(result);
        }
        public async Task<RiskArcSurveyModel> RiskArcSurveyData(string token, ClientTypeEnum clientType, int tz)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid ClientLoginGuid = new(token);
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid)
                .Include(m => m.ClientData).Include(m => m.SpouseData)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var riskArcQuestions = clientData.RiskArcVersion ? GetRiskArcQuestionsWithAnswers(clientType, ClientLoginGuid) : (GetRiskArcQuestionsWithAnswers(clientType, ClientLoginGuid)).Where(m => m.SectionId <= 15);
            var spouseFirstName = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate() ? client.SpouseData.FirstName : null;
            var firstname = clientType == ClientTypeEnum.Client ? clientData.FirstName : spouseFirstName;

            var spouseLastName = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate() ? client.SpouseData.LastName : null;
            var lastname = clientType == ClientTypeEnum.Client ? clientData.LastName : spouseLastName;

            var suffix = clientData.RiskArcVersion ? "Indepth" : "Express";
            string fileName = $"{firstname}'s RiskArc {suffix} Survey {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
            string businessLogoImage = string.Empty;
            var adviserData = client.UserLogin.UserData;
            if (!string.IsNullOrEmpty(adviserData.BusinessLogoFileName))
            {
                string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
                string extension = Path.GetExtension(adviserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                businessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.BusinessLogoFileName, contentType);
            }
            int[] includeAnswer = { 1, 12, 13, 14, 15 };
            var tolerances = await _reportService.GetRiskArcDataAsync(ClientLoginGuid, clientType);
            int[] PreserverData = { 16, 17, 20, 23, 28 };
            int[] FollowerData = { 19, 25, 29, 32, 35 };
            int[] IndependentData = { 21, 22, 26, 30, 34 };
            int[] AccumulatorData = { 18, 24, 27, 31, 33 };
            int totalPoint = 0, totalFpoint = 0, totalIpoint = 0, totalApoint = 0, invTypepoint = 0;
            foreach (var item in tolerances)
            {
                invTypepoint = item.Answer == 1 ? 3 : item.Answer == 2 ? 1 : 0;
                if (PreserverData.Contains(item.SectionId))
                    totalPoint += invTypepoint;
                else if (FollowerData.Contains(item.SectionId))
                    totalFpoint += invTypepoint;
                else if (IndependentData.Contains(item.SectionId))
                    totalIpoint += invTypepoint;
                else if (AccumulatorData.Contains(item.SectionId))
                    totalApoint += invTypepoint;
            }
            IEnumerable<RiskArcBitData> BitData = new List<RiskArcBitData>();
            if (client.ClientData.RiskArcVersion)
            {
                var RateCountData = new List<KeyValuePair<string, int>>
            {
                new KeyValuePair<string, int>(RiskArcBitEnum.Preserver.ToString(), totalPoint),
                new KeyValuePair<string, int>(RiskArcBitEnum.Follower.ToString(), totalFpoint),
                new KeyValuePair<string, int>(RiskArcBitEnum.Independent.ToString(), totalIpoint),
                new KeyValuePair<string, int>(RiskArcBitEnum.Accumulator.ToString(), totalApoint)
            };
                RateCountData = RateCountData.OrderByDescending(m => m.Value).ToList();
                int maxPoint = RateCountData.FirstOrDefault().Value;
                int minPoint = maxPoint - 3;
                BitData = RateCountData.Select(m => new RiskArcBitData
                {
                    BitType = EnumHelper.ToEnum<RiskArcBitEnum>(m.Key),
                    Point = m.Value,
                    Active = ((double)m.Value).Between(minPoint, maxPoint) && m.Value > 0,
                });
            }
            double point = 0, sumpoint = 0;
            string RiskProfile = sumpoint.Between(20, 25) ? RiskProfileEnum.Conservative.GetDisplayName() : sumpoint.Between(26, 45) ? RiskProfileEnum.ModerateConservative.GetDisplayName() : sumpoint.Between(46, 65) ? RiskProfileEnum.Moderate.GetDisplayName() : sumpoint.Between(66, 85) ? RiskProfileEnum.ModerateAggressive.GetDisplayName() : sumpoint.Between(86, 100) ? RiskProfileEnum.Aggressive.GetDisplayName() : "N/A";
            foreach (var item in tolerances.Where(m => includeAnswer.Contains(m.SectionId)))
            {
                point = item.Answer == 1 ? 2.5 : item.Answer == 2 ? 5 : item.Answer == 3 ? 7.5 : item.Answer == 4 ? 10 : item.Answer == 5 ? 12.5 : 0;
                sumpoint += point;
            }
            var model = new RiskArcSurveyModel
            {
                BusinessLogoImage = businessLogoImage,
                FirstName = firstname,
                LastName = lastname,
                RiskArcQuestions = riskArcQuestions,
                LocalDateTime = DateTime.UtcNow.AddMinutes(-tz),
                FileName = fileName,
                ClientId = client.Id,
                RiskCapacity = sumpoint,
                RiskProfile = RiskProfile,
                BitData = BitData,
                RIADisclosure = adviserData.RIADisclosure
            };
            return model;
        }
        [AllowAnonymous]
        public async Task<IActionResult> RiskSurvey(string token, int tz)
        {
            var model = await RiskArcSurveyData(token, ClientTypeEnum.Client, tz);
            var footerAction = _httpContextAccessor.HttpContext.GetAbsoluteUrl("Home/ReportFooter");
            string customSwitches = string.Format("--footer-html {0} --footer-spacing -10", footerAction);
            return new ViewAsPdf("RiskArcSurvey", model)
            {
                PageSize = Size.Letter,
                PageMargins = { Left = 0, Bottom = 12, Right = 0, Top = 10 },
                CustomSwitches = customSwitches,
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName
            };
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _reportService.Dispose();
                }
                disposed = true;
            }
        }
        ~RiskArcController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}