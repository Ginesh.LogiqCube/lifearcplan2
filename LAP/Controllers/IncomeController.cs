﻿using AutoMapper;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.Filters;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class IncomeController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly InvestmentService _investmentService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public IncomeController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 2;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        public async Task<IEnumerable<SelectListItem>> GetEmploymentType(string employmentStatus)
        {
            return await DropdownDataHelper.GetEmploymentTypeAsync(employmentStatus);
        }
        public async Task<IEnumerable<SelectListItem>> GetEmployerOfferType(string employmentStatus)
        {
            return await DropdownDataHelper.GetEmployerOfferTypeAsync(employmentStatus);
        }
        public async Task<IEnumerable<SelectListItem>> GetEmployers(int EmploymentOf)
        {
            return await _dropdownDataHelper.GetEmployersAsync(EmploymentOf);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "employment-info" })]
        public async Task<IActionResult> EmploymentInfo()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).FirstOrDefaultAsync();

            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var occupations = client.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.EmploymentOf > 0).ToList();
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            occupations = occupations.Select(m =>
            {
                var YearsToRetire = m.EmploymentOf == ClientTypeEnum.Client ? clientData.DateOfBirth.GetRemainYearToRetirement(m.AgeToRetire) : spouseData.DateOfBirth.GetRemainYearToRetirement(m.AgeToRetire);
                var MonthsToRetire = m.EmploymentOf == ClientTypeEnum.Client ? clientData.DateOfBirth.GetRemainMonthToRetirement(m.AgeToRetire) : spouseData.DateOfBirth.GetRemainYearToRetirement(m.AgeToRetire);
                m.YearsToRetire = m.EmploymentStatus != EmploymentStatusEnum.Retired ? YearsToRetire ?? 0 : 0;
                m.MonthToRetire = m.EmploymentStatus != EmploymentStatusEnum.Retired ? MonthsToRetire ?? 0 : 0;
                m.DOB = m.EmploymentOf == ClientTypeEnum.Spouse ? spouseData.DateOfBirth : clientData.DateOfBirth;
                var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.EmploymentOf).ToString()).Text;
                m.EmployerTitle = !string.IsNullOrEmpty(m.Employer) ? $"{name}'s Occupation - {m.Employer}" : $"{name}'s Occupation - {m.EmploymentStatus.GetDisplayName()}";
                return m;
            }).OrderByDescending(m => m.Id).ToList();
            ViewBag.ClientSpouseOccupations = string.Join(",", client.ClientOccupations.Select(m => (int)m.EmploymentStatus));
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Personal Property", PrevAction = "personal/personal-property", NextTitle = "Retirement Plan", NextAction = "income/retirement-plan" };
            return View("EmploymentInfo/_EmploymentInfo", occupations);
        }
        public async Task<IActionResult> OccupationInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var occupationinfo = client.ClientOccupations.FirstOrDefault(m => m.Guid == Guid);
            var clientesp = new ClientEsp();
            EmploymentInfoModel empInfoModel = new();
            var OccupationdataObj = _mapper.Map(occupationinfo, empInfoModel.Occupation);
            if (OccupationdataObj is not null)
            {
                var dob = OccupationdataObj.EmploymentOf == ClientTypeEnum.Spouse ? spouseData.DateOfBirth : clientData.DateOfBirth;
                var YearsToRetire = dob.GetRemainYearToRetirement(OccupationdataObj.AgeToRetire);
                OccupationdataObj.DOB = dob;
                OccupationdataObj.YearsToRetire = OccupationdataObj.EmploymentStatus != EmploymentStatusEnum.Retired ? YearsToRetire : 0;
                var ClientEspdataObj = _mapper.Map(clientesp, empInfoModel.Esp);
                empInfoModel.Occupation = OccupationdataObj;
                empInfoModel.Esp = ClientEspdataObj;
                ViewBag.EmploymentType = await DropdownDataHelper.GetEmploymentTypeAsync(OccupationdataObj.EmploymentStatus.ToString());
            }
            ViewBag.AgeData = await _commonService.GetAgeData(ClientLoginGuid);
            ViewBag.ClientDOB = client.ClientData.DateOfBirth.IsNotEmptyDate() ? client.ClientData.DateOfBirth.ToString("MMM dd, yyyy") : null;
            ViewBag.SpouseDOB = client.SpouseData is not null && client.SpouseData.DateOfBirth.IsNotEmptyDate() ? client.SpouseData.DateOfBirth.ToString("MMM dd, yyyy") : null;
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            return View("EmploymentInfo/_EmploymentModal", empInfoModel);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEmployemntInfo(EmploymentInfoModel EmploymentInfo)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).Include(m => m.ClientEsps).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var occupationdata = client.ClientOccupations.FirstOrDefault(m => m.Guid == EmploymentInfo.Occupation.Guid);
            var OccupationdataObj = _mapper.Map(EmploymentInfo.Occupation, occupationdata);

            var ClientEspdata = client.ClientEsps.FirstOrDefault(m => m.Guid == EmploymentInfo.Esp?.Guid);
            if (EmploymentInfo.Esp is not null)
            {
                if ((EmploymentInfo.Esp.HasPensionPlan || EmploymentInfo.Esp.EmployerOffer?.Count > 0))
                {
                    var clientEspdataObj = _mapper.Map(EmploymentInfo.Esp, ClientEspdata);
                    clientEspdataObj.ClientId = ClientId;
                    OccupationdataObj.ClientEsp = clientEspdataObj;
                }
                else
                    if (ClientEspdata is not null)
                    _context2.ClientEsps.Remove(ClientEspdata);
            }
            OccupationdataObj.ClientId = ClientId;
            _context2.ClientOccupations.Update(OccupationdataObj);
            await _context2.SaveChangesAsync();
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            OccupationdataObj.ClientLogin = null;
            var dob = OccupationdataObj.EmploymentOf == ClientTypeEnum.Spouse ? spouseData.DateOfBirth : clientData.DateOfBirth;
            var YearsToRetire = dob.GetRemainYearToRetirement(OccupationdataObj.AgeToRetire);
            OccupationdataObj.DOB = dob;
            OccupationdataObj.YearsToRetire = OccupationdataObj.EmploymentStatus != EmploymentStatusEnum.Retired ? YearsToRetire ?? 0 : 0;
            var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)OccupationdataObj.EmploymentOf).ToString()).Text;
            OccupationdataObj.EmployerTitle = !string.IsNullOrEmpty(OccupationdataObj.Employer) ? $"{name}'s Occupation - {OccupationdataObj.Employer}" : $"{name}'s Occupation - {OccupationdataObj.EmploymentStatus.GetDisplayName()}";
            var result = new CommonResponse
            {
                Success = client is not null,
                Message = client is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { employmentInfo = OccupationdataObj }
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEspInfo(EmploymentInfoModel EmploymentInfo, List<ClientPrevEsp> prevEsps)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).Include(m => m.ClientEsps).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var clientEspInfo = client.ClientEsps.FirstOrDefault(m => m.Guid == EmploymentInfo.Esp.Guid);

            var clientEspdataObj = new ClientEsp();
            if ((EmploymentInfo.Esp.HasPensionPlan || EmploymentInfo.Esp.EmployerOffer.Count > 0))
            {
                clientEspdataObj = _mapper.Map(EmploymentInfo.Esp, clientEspInfo);
                clientEspdataObj.ClientId = ClientId;
                _context2.ClientEsps.Update(clientEspdataObj);
            }
            else
                if (clientEspInfo is not null)
                _context2.ClientEsps.Remove(clientEspInfo);
            prevEsps = prevEsps.Select(m =>
            {
                m.EmploymentOf = clientEspInfo.ClientOccupation.EmploymentOf;
                m.ClientId = ClientId;
                return m;
            }).ToList();

            _context2.ClientPrevEsps.AddRange(prevEsps);
            await _context2.SaveChangesAsync();
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)clientEspInfo.ClientOccupation.EmploymentOf).ToString()).Text;
            clientEspdataObj.ClientOccupation.EmployerTitle = !string.IsNullOrEmpty(clientEspdataObj.ClientOccupation.Employer) ? $"{name}'s Retirement Plan - {clientEspdataObj.ClientOccupation.Employer}" : $"{name}'s Retirement Plan - {clientEspdataObj.ClientOccupation.EmploymentStatus.GetDisplayName()}";
            var result = new CommonResponse
            {
                Success = clientEspInfo is not null,
                Message = clientEspInfo is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { espInfo = clientEspdataObj }
            };
            return Json(result);
        }
        public IActionResult ValidateRetireAge(EmploymentInfoModel employmentInfo)
        {
            bool result = false;
            var dob = employmentInfo.Occupation.DOB;
            var age = Convert.ToInt32(dob.CalculateAge());
            if (age <= employmentInfo.Occupation.AgeToRetire)
                result = true;
            string msg = $"Retirement age must be more than your age - {age}";
            if (result)
                return Json(result);
            return Json(msg);
        }
        public IActionResult ValidatePensionExpectedAge(EmploymentInfoModel employmentInfo)
        {
            bool result = false;
            var dob = employmentInfo.Esp.DOB;
            var age = dob.CalculateAge();
            if (age <= employmentInfo.Esp.PensionExpectedAge)
                result = true;
            string msg = $"Pension Expected age must be more than your age - {age}";
            if (result)
                return Json(result);
            return Json(msg);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployment(Guid Guid)
        {
            var employemntInfo = await _context2.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid)
                .Include(m => m.ClientEsp).FirstOrDefaultAsync();
            if (employemntInfo is not null)
            {
                _context2.ClientOccupations.Remove(employemntInfo);
                if (employemntInfo.ClientEsp != null)
                    _context2.ClientEsps.Remove(employemntInfo.ClientEsp);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = employemntInfo is not null,
                Message = employemntInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { employmentInfo = employemntInfo }
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "retirement-plan" })]
        public async Task<IActionResult> RetirementPlan()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
               .Include(m => m.ClientData).Include(m => m.ClientOccupations).Include(m => m.ClientEsps).Include(m => m.ClientPrevEsps).FirstOrDefaultAsync();

            var ClientEsps = client.ClientEsps.Where(m => m.ClientOccupation?.EmploymentOf > 0).ToList();
            var clientData = client.ClientData;

            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ClientEsps = ClientEsps.Select(m =>
             {
                 var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.ClientOccupation.EmploymentOf).ToString()).Text;
                 m.ClientOccupation.EmployerTitle = !string.IsNullOrEmpty(m.ClientOccupation.Employer) ? $"{name}'s Retirement Plan - {m.ClientOccupation.Employer}" : $"{name}'s Retirement Plan - {m.ClientOccupation.EmploymentStatus.GetDisplayName()}";
                 return m;
             }).OrderByDescending(m => m.ClientOccupation.Id).ToList();
            ViewBag.PrevEsps = client.ClientPrevEsps;
            ViewBag.ClientTitle = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ClientTypeEnum.Client.ToString());
            ViewBag.EmploymentOf = EmploymentOf;
            bool hasSelfEmployed = client.ClientOccupations.Any(m => m.EmploymentStatus == EmploymentStatusEnum.SelfEmployed);
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Employment Info",
                PrevAction = "income/employment-info",
                NextTitle = hasSelfEmployed ? "Business Interests" : "Personal Activities",
                NextAction = hasSelfEmployed ? "income/business-interests" : "income/personal-activities"
            };
            return View("EspInfo/_EmploySponserPlanInfo", ClientEsps);
        }
        public async Task<IActionResult> EspInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).Include(m => m.ClientEsps).FirstOrDefaultAsync();
            var clientEspInfo = await _context2.ClientEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            EmploymentInfoModel empInfoModel = new()
            {
                Occupation = new OccupationModel()
            };
            var clientEspObj = _mapper.Map(clientEspInfo, empInfoModel.Esp);
            ViewBag.EmployerOfferType = await DropdownDataHelper.GetEmployerOfferTypeAsync(clientEspInfo.ClientOccupation.EmploymentStatus.ToString());
            if (clientEspObj is not null)
            {
                empInfoModel.Occupation.Guid = clientEspInfo.ClientOccupation.Guid;
                empInfoModel.Occupation.EmploymentStatus = clientEspInfo.ClientOccupation.EmploymentStatus;
                empInfoModel.Occupation.EmploymentOf = clientEspInfo.ClientOccupation.EmploymentOf;
                var dob = empInfoModel.Occupation.EmploymentOf == ClientTypeEnum.Spouse ? spouseData.DateOfBirth : clientData.DateOfBirth;
                clientEspObj.DOB = dob;
                empInfoModel.Esp = clientEspObj;
            }
            return View("EspInfo/_EspModal", empInfoModel);
        }
        public async Task<IActionResult> PrevEspInfo()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.EmploymentOf = EmploymentOf;
            return View("PrevEsp/_PrevEspModal");
        }
        [HttpPost]
        public async Task<IActionResult> AddPrevEsp(ClientPrevEsp clientPrevEsp, ClientTypeEnum ClientType)
        {
            clientPrevEsp.EmploymentOf = ClientType;
            clientPrevEsp.ClientId = ClientId;
            _context2.ClientPrevEsps.Add(clientPrevEsp);
            await _context2.SaveChangesAsync();
            var clientData = _context2.ClientDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            var name = EmploymentOf.FirstOrDefault(n => n.Value == ((int)ClientType).ToString());
            var updateditems = _context2.ClientPrevEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid && (int)m.EmploymentOf == (int)ClientType);
            var result = new CommonResponse
            {
                Success = clientPrevEsp != null,
                Message = clientPrevEsp != null ? Message.SAVE_SUCCESS : Message.NO_RECORD_UPDATE,
                Data = clientPrevEsp != null ? new { updateditems, name.Text } : null
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> UpdatePrevEspInfo(ClientPrevEsp clientPrevEsp)
        {
            var clientPrevEspInfo = await _context2.ClientPrevEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == clientPrevEsp.Guid).FirstOrDefaultAsync();
            if (clientPrevEspInfo is not null)
            {
                clientPrevEsp.Type = clientPrevEsp.Type != default ? clientPrevEsp.Type : clientPrevEspInfo.Type;
                clientPrevEsp.OtherType = !string.IsNullOrEmpty(clientPrevEsp.OtherType) ? clientPrevEsp.OtherType : clientPrevEspInfo.OtherType;
                var clientPrevEspObj = _mapper.Map(clientPrevEsp, clientPrevEspInfo);
                clientPrevEspObj.ClientId = ClientId;
                _context2.ClientPrevEsps.Update(clientPrevEspObj);
                await _context2.SaveChangesAsync();
                clientPrevEspInfo.ClientLogin = null;
            }
            var result = new CommonResponse
            {
                Success = clientPrevEspInfo is not null,
                Message = clientPrevEspInfo is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { clientPrevEspInfo }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeletePrevEsp(Guid Guid)
        {
            var clientPrevEsp = await _context2.ClientPrevEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (clientPrevEsp is not null)
            {
                _context2.ClientPrevEsps.Remove(clientPrevEsp);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = clientPrevEsp is not null,
                Message = clientPrevEsp is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "business-interests" })]
        public async Task<IActionResult> BusinessInterests()
        {
            var bussinessInterest = _context2.BusinessInterests.OrderBy(m => m.businessInterestOf == ClientTypeEnum.Client).Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetFirmOwnershipOfAsync(clientData.MaritalStatus.ToString());
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            bussinessInterest = bussinessInterest.Select(m =>
            {
                var FirmOwnerShipString = ClientType.Where(n => n.Value == m.FirmOwnershipEnum).Select(m => m.Text);
                m.FirmOwnerShipString = string.Join(" & ", FirmOwnerShipString);
                m.businessInterestOfEnumString = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.businessInterestOf).ToString()).Text;
                return m;
            }).ToList();
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Retirement Plan",
                PrevAction = "income/retirement-plan",
                NextTitle = "Personal Activities",
                NextAction = "income/personal-activities"
            };
            return View("BusinessInterest/_BusinessInterestInfo", bussinessInterest);
        }
        public async Task<IActionResult> BusinessInterestInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.BusinessInterests).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var businessInfo = client.BusinessInterests.OrderBy(m => m.businessInterestOf == ClientTypeEnum.Client).FirstOrDefault(m => m.Guid == Guid);
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.FirmOwnershipOf = await _dropdownDataHelper.GetFirmOwnershipOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.IsAdd = businessInfo is not null ? "Edit" : "Add";
            return View("BusinessInterest/_BusinessInterestModal", businessInfo);
        }
        public async Task<IActionResult> OtherIncomeInfo(Guid? Guid)
        {
            var clientLoginData = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.OtherIncomes).FirstOrDefaultAsync();
            var clientData = clientLoginData.ClientData;
            var otherIncomeInfo = clientLoginData.OtherIncomes.FirstOrDefault(m => m.Guid == Guid);
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());

            return View("OtherIncome/_OtherIncomeModal", otherIncomeInfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveBusinessInterest(BusinessInterest businessInterestModel)
        {
            var businessdata = await _context2.BusinessInterests.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == businessInterestModel.Guid).FirstOrDefaultAsync();
            var bussinessObj = _mapper.Map(businessInterestModel, businessdata);
            bussinessObj.ClientId = ClientId;
            _context2.BusinessInterests.Update(bussinessObj);
            await _context2.SaveChangesAsync();
            var clientLoginData = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.BusinessInterests).FirstOrDefaultAsync();
            var maritalStatus = clientLoginData.ClientData.MaritalStatus;
            var ClientType = await _dropdownDataHelper.GetFirmOwnershipOfAsync(maritalStatus.ToString());
            var HeirOfWithData = ClientType.Where(m => m.Value == bussinessObj.FirmOwnershipEnum).Select(m => m.Text);
            var clientData = clientLoginData.ClientData;
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            bussinessObj.businessInterestOfEnumString = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)bussinessObj.businessInterestOf).ToString()).Text;
            bussinessObj.FirmOwnerShipString = string.Join(" & ", HeirOfWithData);

            var result = new CommonResponse
            {
                Success = bussinessObj.ClientId > 0,
                Message = bussinessObj.ClientId > 0 ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { businessInterestInfo = bussinessObj }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteBusinessInterest(Guid Guid)
        {
            var businessInfo = await _context2.BusinessInterests.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid)
                .Include(m => m.ClientLogin).FirstOrDefaultAsync();
            if (businessInfo is not null)
            {
                _context2.BusinessInterests.Remove(businessInfo);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = businessInfo is not null,
                Message = businessInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "personal-activities" })]
        public async Task<IActionResult> PersonalActivities()
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            var otherincome = _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            otherincome = otherincome.Select(m =>
            {
                var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.businessInterestof).ToString()).Text;
                m.businessInterestofString = name;
                return m;
            }).ToList();
            var ocupations = _context2.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            bool hasSelfEmployed = await ocupations.AnyAsync(m => m.EmploymentStatus == EmploymentStatusEnum.SelfEmployed);
            bool hasPersonalAssets = _context2.ClientProperties.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = hasSelfEmployed ? "Business Interests" : "Retirement Plan",
                PrevAction = hasSelfEmployed ? "income/business-interests" : "income/retirement-plan",
                NextTitle = hasPersonalAssets ? "Personal Assets" : "Social Security",
                NextAction = hasPersonalAssets ? "income/personal-assets" : "income/social-security-intro",
            };
            return View("OtherIncome/_OtherIncomeInfo", otherincome);
        }
        [HttpPost]
        public async Task<IActionResult> SaveOtherIncome(OtherIncome OtherIncome)
        {
            var otherIncomedata = await _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == OtherIncome.Guid).FirstOrDefaultAsync();
            var OtherIncomeObj = _mapper.Map(OtherIncome, otherIncomedata);
            OtherIncomeObj.ClientId = ClientId;
            _context2.OtherIncomes.Update(OtherIncomeObj);
            await _context2.SaveChangesAsync();
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientProperties).Include(m => m.OtherIncomes).FirstOrDefaultAsync();
            decimal otherIncomeTotal = client.OtherIncomes.Sum(m => m.AnnualIncome);
            PropertyTypeEnum[] includePropertyType = { PropertyTypeEnum.Metals, PropertyTypeEnum.Automobile, PropertyTypeEnum.Jewelry, PropertyTypeEnum.Collectibles };
            decimal propertyTotal = client.ClientProperties.Where(m => includePropertyType.Contains(m.Type) && m.Incomegenerated).Sum(i => i.AnnualIncome);
            var total = otherIncomeTotal + propertyTotal;
            var clientData = _context2.ClientDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            OtherIncomeObj.businessInterestofString = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)OtherIncomeObj.businessInterestof).ToString()).Text;
            var result = new CommonResponse
            {
                Success = OtherIncomeObj.ClientId > 0,
                Message = OtherIncomeObj.ClientId > 0 ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { OtherIncomeInfo = OtherIncomeObj, total }
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateOtherInfo(OtherIncome OtherIncomeInfo)
        {
            var otherIncome = await _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == OtherIncomeInfo.Guid).FirstOrDefaultAsync();

            if (otherIncome is not null)
            {
                otherIncome.AnnualIncome = OtherIncomeInfo.AnnualIncome;
                otherIncome.Years = OtherIncomeInfo.Years;
                await _context2.SaveChangesAsync();
                otherIncome.ClientLogin = null;
            }
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientProperties).Include(m => m.OtherIncomes).FirstOrDefaultAsync();
            decimal otherIncomeTotal = client.OtherIncomes.Sum(m => m.AnnualIncome);
            PropertyTypeEnum[] includePropertyType = { PropertyTypeEnum.Metals, PropertyTypeEnum.Automobile, PropertyTypeEnum.Jewelry, PropertyTypeEnum.Collectibles };
            decimal propertyTotal = client.ClientProperties.Where(m => includePropertyType.Contains(m.Type) && m.Incomegenerated).Sum(i => i.AnnualIncome);
            var total = otherIncomeTotal + propertyTotal;
            var result = new CommonResponse
            {
                Success = OtherIncomeInfo is not null,
                Message = OtherIncomeInfo is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { OtherIncomeInfo, total }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteOtherIncome(Guid Guid)
        {
            var otherIncome = await _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (otherIncome is not null)
            {
                _context2.OtherIncomes.Remove(otherIncome);
                await _context2.SaveChangesAsync();
            }
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientProperties).Include(m => m.OtherIncomes).FirstOrDefaultAsync();
            decimal otherIncomeTotal = client.OtherIncomes.Sum(m => m.AnnualIncome);
            PropertyTypeEnum[] includePropertyType = { PropertyTypeEnum.Metals, PropertyTypeEnum.Automobile, PropertyTypeEnum.Jewelry, PropertyTypeEnum.Collectibles };
            decimal propertyTotal = client.ClientProperties.Where(m => includePropertyType.Contains(m.Type) && m.Incomegenerated).Sum(i => i.AnnualIncome);
            var total = otherIncomeTotal + propertyTotal;
            var result = new CommonResponse
            {
                Success = otherIncome is not null,
                Message = otherIncome is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { total }
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "personal-assets" })]
        public IActionResult PersonalAssets()
        {
            var properties = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Personal Activities", PrevAction = "income/personal-activities", NextTitle = "Social Security", NextAction = "income/social-security-intro" };
            return View("PersonalAsset/_PersonalAsset", properties);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "investment-income" })]
        public async Task<IActionResult> InvestmentIncome()
        {
            var clientData = await _context2.ClientDatas.FirstOrDefaultAsync(m => m.ClientLogin.Guid == ClientLoginGuid);
            var bankCreditUnionAccounts = await _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0).ToListAsync();
            var brokerageAdvisoryAccounts = await _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0).ToListAsync();
            var lifeAnnuityContracts = await _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0).ToListAsync();
            var ClientTypeOwner = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            bankCreditUnionAccounts = bankCreditUnionAccounts.Select(m =>
            {
                var OwnByWithData = ClientTypeOwner.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            brokerageAdvisoryAccounts = brokerageAdvisoryAccounts.Select(m =>
            {
                var OwnByWithData = ClientTypeOwner.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            lifeAnnuityContracts = lifeAnnuityContracts.Select(m =>
            {
                var OwnByWithData = ClientTypeOwner.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            ViewBag.BankCreditUnionAccounts = bankCreditUnionAccounts;
            ViewBag.BrokerageAdvisoryAccounts = brokerageAdvisoryAccounts;
            ViewBag.LifeAnnuityContracts = lifeAnnuityContracts;

            bool isExpress = !clientData.BudgetVersion;
            var budgetText = isExpress ? " Express" : "";
            var budgetAction = isExpress ? "-express" : "";
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Social Security Estimate",
                PrevAction = "income/social-security-estimate",
                NextTitle = $"Budget Info{budgetText}",
                NextAction = $"expense/budget-info{budgetAction}"
            };
            return View("InvestmentIncome/_InvestmentIncome");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "social-security-intro" })]
        public async Task<IActionResult> SocialSecurityIntro()
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            bool hasPersonalAssets = _context2.ClientProperties.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = hasPersonalAssets ? "Personal Assets" : "Personal Activities",
                PrevAction = hasPersonalAssets ? "income/personal-assets" : "income/personal-activities",
                NextTitle = "Social Security Estimate",
                NextAction = "income/social-security-estimate",
            };
            return View("SocialSecurity/SocialSecurityIntro");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "social-security-estimate" })]
        public async Task<IActionResult> SocialSecurityEstimate()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).Include(m => m.SocialSecurities).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            var SSestimate = client.SocialSecurities.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();

            List<SocialSecurity> sslist = new();
            foreach (var item in EmploymentOf)
            {
                var currentSse = SSestimate.Find(m => ((int)m.EmploymentOf).ToString() == item.Value.ToString());
                if (currentSse is not null)
                {
                    currentSse.EmployerTitle = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)currentSse.EmploymentOf).ToString()).Text;
                    sslist.Add(currentSse);
                }
                else
                {
                    sslist.Add(new SocialSecurity()
                    {
                        EmployerTitle = item.Text,
                        EmploymentOf = (ClientTypeEnum)(Convert.ToInt16(EmploymentOf.FirstOrDefault(n => n.Value == item.Value.ToString()).Value))
                    });
                }
            }
            SSestimate = sslist;
            var expectedAges = new List<SelectListItem>();
            for (int i = 60; i <= 70; i++)
            {
                expectedAges.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }
            ViewBag.expectedAges = expectedAges;
            ViewBag.EmploymentOf = EmploymentOf;
            bool isExpress = !clientData.BudgetVersion;
            var budgetText = isExpress ? " Express" : "";
            var budgetAction = isExpress ? "-express" : "";
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Social Security Intro",
                PrevAction = "income/social-security-intro",
                NextTitle = hasInvestmentIncome ? "Investment Income" : $"Budget Info{budgetText}",
                NextAction = hasInvestmentIncome ? "income/investment-income" : $"expense/budget-info{budgetAction}"
            };
            return View("SocialSecurity/SocialSecurityEstimate", SSestimate);
        }
        [HttpPost]
        public async Task<IActionResult> SaveSecurityEstimate(List<SocialSecurity> socialSecurity)
        {
            var ssData = _context2.SocialSecurities.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (socialSecurity is not null)
            {
                foreach (var item in socialSecurity)
                {
                    var SSobjex = await ssData.Where(m => m.Guid == item.Guid).FirstOrDefaultAsync();
                    if (SSobjex is not null)
                    {
                        var ssDataObj = _mapper.Map(item, SSobjex);
                        _context2.SocialSecurities.Update(ssDataObj);
                    }
                    else
                    {
                        item.ClientId = ClientId;
                        _context2.SocialSecurities.Update(item);
                    }
                }
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = ssData is not null,
                Message = ssData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~IncomeController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}