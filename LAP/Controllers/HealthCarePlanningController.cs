﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LAP.Filters;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using LAP.DomainModels.Entities.Client;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class HealthCarePlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public HealthCarePlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 5;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "healthcare-planning-intro" })]
        public async Task<IActionResult> HealthCarePlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("healthcare-planning-intro", ClientLoginGuid);
            return View();
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "healthcare-planning-assessment" })]
        public async Task<IActionResult> HealthCarePlanningAssessment()
        {
            HealthCarePlanningModel healthCarePlanningModel = new();
            var clientLoginData = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.HealthCarePlanning).FirstOrDefaultAsync();

            var healthCaredata = await _context2.HealthCarePlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();

            IEnumerable<SelectListItem> policyProvedList = EnumHelper.GetEnumSelectList<PolicyProvideCareEnum>();
            IEnumerable<SelectListItem> ListpolicyPayingBenifitsList = EnumHelper.GetEnumSelectList<PolicyBeginBenefits>();
            IEnumerable<SelectListItem> ListLTCIPolicyProvideBenifits = EnumHelper.GetEnumSelectList<LTCIPolicyProvideBenifitsEnum>();

            if (healthCaredata is not null)
            {
                healthCarePlanningModel.WaitingPeriodYesNOEnum = healthCaredata.waitingPeriodYesNOEnum;
                healthCarePlanningModel.WaitingPeriodEnum = healthCaredata.waitingPeriodEnum;
                healthCarePlanningModel.LTCIBenifitsEnumYesNO = healthCaredata.LTCIBenifitsEnumYesNO;
                healthCarePlanningModel.LTCIbenifitsEnum = healthCaredata.LTCIbenifitsEnum;
                healthCarePlanningModel.LTCIbenifirPayouttEnum = healthCaredata.lTCIbenifirPayouttEnum;
                healthCarePlanningModel.InflationProtectionEnum = healthCaredata.inflationProtectionEnum;
                healthCarePlanningModel.Inflationprovision = healthCaredata.inflationprovision;
                healthCarePlanningModel.UnderESP = healthCaredata.underESP;
                healthCarePlanningModel.UnderME = healthCaredata.underME;
                healthCarePlanningModel.Uninsured = healthCaredata.uninsured;
                healthCarePlanningModel.LongTermCare = healthCaredata.longTermCare;
                healthCarePlanningModel.LongTermCareRate = healthCaredata.LongTermCareRate;

                if (healthCaredata.policyProvide != "")
                    healthCarePlanningModel.LstpolicyProvide = healthCaredata.policyProvide.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                if (healthCaredata.policyPayingBenifits != "")
                    healthCarePlanningModel.LstpolicyPayingBenifits = healthCaredata.policyPayingBenifits.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                if (healthCaredata.LTCIPolicyProvideBenifitsEnum != "")
                    healthCarePlanningModel.ListLTCIPolicyProvideBenifits = healthCaredata.LTCIPolicyProvideBenifitsEnum.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            }
            healthCarePlanningModel.ListpolicyProvide = policyProvedList.Select(x => new SelectListItem()
            {
                Text = x.Text,
                Value = x.Value,
                Selected = healthCarePlanningModel.LstpolicyProvide != null && healthCarePlanningModel.LstpolicyProvide.Any(z => z == Convert.ToInt32(x.Value)) || x.Selected

            }).ToList();
            healthCarePlanningModel.ListpolicyPayingBenifits = ListpolicyPayingBenifitsList.Select(x => new SelectListItem()
            {
                Text = x.Text,
                Value = x.Value,
                Selected = healthCarePlanningModel.LstpolicyPayingBenifits is not null && healthCarePlanningModel.LstpolicyPayingBenifits.Any(z => z == Convert.ToInt32(x.Value)) || x.Selected

            }).ToList();
            healthCarePlanningModel.ListLTCIPolicyProvideBenifitsEnum = ListLTCIPolicyProvideBenifits.Select(x => new SelectListItem()
            {
                Text = x.Text,
                Value = x.Value,
                Selected = healthCarePlanningModel.ListLTCIPolicyProvideBenifits is not null && healthCarePlanningModel.ListLTCIPolicyProvideBenifits.Any(z => z == Convert.ToInt32(x.Value)) || x.Selected

            }).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("healthcare-planning-assessment", ClientLoginGuid);
            return View("HealthcarePlanningAssessment", healthCarePlanningModel);
        }
        [HttpPost]
        public async Task<IActionResult> SaveHealCarePlanning(HealthCarePlanningModel healthCarePlanningModel)
        {
            var healthcaredata = await _context2.HealthCarePlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var policyprovideValues = healthCarePlanningModel.ListpolicyProvide.Where(x => x.Selected == true).Select(x => x.Value).ToList();
            var policybenifitValues = healthCarePlanningModel.ListpolicyPayingBenifits.Where(x => x.Selected == true).Select(x => x.Value).ToList();
            var ListLTCIPolicyProvideBenifitsValues = healthCarePlanningModel.ListLTCIPolicyProvideBenifitsEnum.Where(x => x.Selected == true).Select(x => x.Value).ToList();
            if (healthcaredata == null)
                healthcaredata = new HealthCarePlanning();
            if (healthcaredata is not null)
            {
                var policyProvide = string.Join(',', policyprovideValues);
                var policybenifit = string.Join(',', policybenifitValues);
                var ListLTCIPolicyProvideBenifit = string.Join(',', ListLTCIPolicyProvideBenifitsValues);
                healthcaredata.policyProvide = policyProvide;
                healthcaredata.policyPayingBenifits = policybenifit;
                healthcaredata.LTCIPolicyProvideBenifitsEnum = ListLTCIPolicyProvideBenifit;

                healthcaredata.underESP = healthCarePlanningModel.UnderESP;
                healthcaredata.underME = healthCarePlanningModel.UnderME;
                healthcaredata.uninsured = healthCarePlanningModel.Uninsured;
                healthcaredata.longTermCare = healthCarePlanningModel.LongTermCare;
                healthcaredata.waitingPeriodYesNOEnum = healthCarePlanningModel.WaitingPeriodYesNOEnum;
                healthcaredata.waitingPeriodEnum = healthCarePlanningModel.WaitingPeriodEnum;
                healthcaredata.LTCIBenifitsEnumYesNO = healthCarePlanningModel.LTCIBenifitsEnumYesNO;
                healthcaredata.LTCIbenifitsEnum = healthCarePlanningModel.LTCIbenifitsEnum;
                healthcaredata.lTCIbenifirPayouttEnum = healthCarePlanningModel.LTCIbenifirPayouttEnum;
                healthcaredata.inflationProtectionEnum = healthCarePlanningModel.InflationProtectionEnum;
                healthcaredata.inflationprovision = healthCarePlanningModel.Inflationprovision;
                healthcaredata.ClientId = ClientId;
                healthcaredata.LongTermCareRate = healthCarePlanningModel.LongTermCareRate;
                _context2.HealthCarePlannings.Update(healthcaredata);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = healthcaredata is not null,
                Message = healthcaredata is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~HealthCarePlanningController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}