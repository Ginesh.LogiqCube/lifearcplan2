﻿using AutoMapper;
using LAP.Helpers;
using LAP.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LAP.Services;
using LAP.Services.Integration;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using System.Reflection;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class PersonalController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly BudgetService _budgetService;
        protected readonly RedtailService _redtailService;
        protected readonly CFSService _cfsService;
        protected readonly OmniService _omniService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public PersonalController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            _budgetService = new BudgetService(_context2, _httpContextAccessor);
            _redtailService = new RedtailService(context2, config);
            _cfsService = new CFSService(context2, config, env);
            _omniService = new OmniService(context2, config, env);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 1;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        public async Task<IEnumerable<SelectListItem>> GetTaxFilingType(string maritalStatus)
        {
            return await DropdownDataHelper.GetTaxFilingTypeAsync(maritalStatus);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "client-info" })]
        public async Task<IActionResult> ClientInfo()
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            ClientInfo clientDataMap = new();
            ViewBag.MaritalStatus = await DropdownDataHelper.GetMaritaStatusTypeAsync();
            if (clientData is not null)
            {
                ViewBag.TaxFilingStatus = clientData?.MaritalStatus > 0 ? await DropdownDataHelper.GetTaxFilingTypeAsync(clientData.MaritalStatus.ToString()) : null;
                clientDataMap = _mapper.Map<ClientInfo>(clientData);
                clientDataMap.DateOfBirth = Convert.ToDateTime(clientDataMap.DateOfBirth).IsNotEmptyDate() ? clientDataMap.DateOfBirth : null;
            }

            bool isNotMarriedSeprated = clientData.MaritalStatus != MaritalStatusEnum.Married && clientData.MaritalStatus != MaritalStatusEnum.Separated;
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                NextTitle = isNotMarriedSeprated ? "Contact Info" : "Marital Info",
                NextAction = isNotMarriedSeprated ? "personal/contact-info" : "personal/marital-info"
            };
            return View("ClientInfo/_ClientInfo", clientData is not null ? clientDataMap : null);
        }
        [HttpPost]
        public async Task<IActionResult> SaveClientInfo(ClientInfo clientInfo)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.Integration).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var xspouseData = client.XSpouseData;
            var integrationData = client.UserLogin.Integration;
            if (clientData is not null)
            {
                var clientDataObj = _mapper.Map(clientInfo, clientData);
                if (clientData.MaritalStatus == MaritalStatusEnum.Married || clientData.MaritalStatus == MaritalStatusEnum.Separated)
                {
                    clientDataObj.HasPrevSpouse = true;
                    if (xspouseData is not null)
                        _context2.XSpouseDatas.Remove(xspouseData);
                }
                else if (clientData.MaritalStatus == MaritalStatusEnum.Divorced || clientData.MaritalStatus == MaritalStatusEnum.Widowed)
                {
                    clientDataObj.HasPrevSpouse = true;
                    await _commonService.ClearSpouseData(ClientLoginGuid);
                }
                else
                    await _commonService.ClearSpouseData(ClientLoginGuid);
                _context2.ClientDatas.Update(clientDataObj);
                await _context2.SaveChangesAsync();
            }
            var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
            await _redtailService.UpdateClientContact(ClientLoginGuid, Convert.ToInt32(TimeZone));
            if (!string.IsNullOrEmpty(integrationData?.OmniUsername))
            {
                var model = new LoginModel
                {
                    Username = integrationData.OmniUsername,
                    Password = await integrationData.OmniAccessKey.DecryptAsync()
                };
                await _omniService.UpdateClientContact(model, ClientLoginGuid, clientData.OmniContactId);
            }
            var result = new CommonResponse
            {
                Success = clientData is not null,
                Message = clientData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { ClientFirstName = clientInfo.FirstName, ClientLastName = clientInfo.LastName }
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "marital-info" })]
        public async Task<IActionResult> MaritalInfo()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouse = client.SpouseData;
            var xspouse = client.XSpouseData;
            var clientDataMap = new MaritalInfo();
            ViewBag.MaritalStatus = (int)clientData.MaritalStatus;
            if (clientData is not null)
            {
                if (spouse != null)
                {
                    clientDataMap = _mapper.Map<MaritalInfo>(spouse);
                    if (Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate())
                    {
                        clientDataMap.DateOfCurrentMarriage = clientData.DateOfCurrentMarriage;
                        clientDataMap.Prenup = clientData.Prenup;
                        clientDataMap.CareForSurvivingSpouse = clientData.CareForSurvivingSpouse;
                        clientDataMap.DateOfCurrentMarriage = Convert.ToDateTime(clientDataMap.DateOfCurrentMarriage).IsNotEmptyDate() ? clientDataMap.DateOfCurrentMarriage : null;
                    }
                }
                else
                {
                    if (xspouse is not null && clientData.MarriedTenYears)
                    {
                        clientDataMap = _mapper.Map<MaritalInfo>(xspouse);
                        clientDataMap.CurrentMarriedTenYears = clientData.MarriedTenYears;
                    }
                }
            }
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Client Info",
                PrevAction = "personal/client-info",
                NextTitle = clientData.MaritalStatus == MaritalStatusEnum.Married || clientData.MaritalStatus == MaritalStatusEnum.Separated ? "Spouse Info" : "Contact Info",
                NextAction = clientData.MaritalStatus == MaritalStatusEnum.Married || clientData.MaritalStatus == MaritalStatusEnum.Separated ? "personal/spouse-info" : "personal/contact-info"
            };
            return View("_MaritalStatus", clientDataMap);
        }
        [HttpPost]
        public async Task<IActionResult> SaveMaritalInfo(MaritalInfo maritalInfo)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.XSpouseData)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.Integration).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var xspouseData = client.XSpouseData;
            var integrationData = client.UserLogin.Integration;
            if (clientData is not null)
            {
                if (clientData.MaritalStatus == MaritalStatusEnum.Married || clientData.MaritalStatus == MaritalStatusEnum.Separated)
                {
                    var clientDataObj = await _context2.ClientDatas.FirstOrDefaultAsync(m => m.Guid == clientData.Guid);
                    var updateclientDataObj = clientDataObj;
                    updateclientDataObj.DateOfCurrentMarriage = maritalInfo.DateOfCurrentMarriage;
                    updateclientDataObj.Prenup = maritalInfo.Prenup;
                    updateclientDataObj.CareForSurvivingSpouse = maritalInfo.CareForSurvivingSpouse;
                    _context2.ClientDatas.Update(updateclientDataObj);

                    var spouseDataObj = _mapper.Map(maritalInfo, spouseData);
                    spouseDataObj.ClientId = clientData.ClientId;
                    if (spouseData is not null)
                        _context2.SpouseDatas.Update(spouseDataObj);
                    else
                        _context2.SpouseDatas.Add(spouseDataObj);

                    if (xspouseData is not null)
                        _context2.XSpouseDatas.Remove(xspouseData);
                    await _redtailService.UpdateSpouseContact(client.Guid, $"{clientData.LastName} Family", clientData.RedtailContactId);
                    if (!string.IsNullOrEmpty(integrationData?.OmniUsername))
                    {
                        var model = new LoginModel
                        {
                            Username = integrationData.OmniUsername,
                            Password = await integrationData.OmniAccessKey.DecryptAsync()
                        };
                        await _omniService.UpdateClientContact(model, ClientLoginGuid, clientData.OmniContactId);
                    }
                }
                else if (clientData.MaritalStatus == MaritalStatusEnum.Divorced || clientData.MaritalStatus == MaritalStatusEnum.Widowed)
                {
                    clientData.MarriedTenYears = maritalInfo.CurrentMarriedTenYears;
                    if (maritalInfo.CurrentMarriedTenYears)
                    {
                        var xspouseDataObj = _mapper.Map(maritalInfo, xspouseData);
                        xspouseDataObj.ClientId = clientData.ClientId;
                        if (xspouseData is not null)
                            _context2.XSpouseDatas.Update(xspouseDataObj);
                        else
                            _context2.XSpouseDatas.Add(xspouseDataObj);
                    }
                    if (spouseData is not null)
                        _context2.SpouseDatas.Remove(spouseData);
                }
                else
                {
                    if (spouseData is not null)
                        _context2.SpouseDatas.Remove(spouseData);
                    if (xspouseData is not null)
                        _context2.XSpouseDatas.Remove(xspouseData);
                }
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = clientData is not null,
                Message = clientData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { maritalInfo.SpouseFirstName }
            };
            return Json(result);
        }
        [ServiceFilter(typeof(PermissionAttribute))]
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "spouse-info" })]
        public async Task<IActionResult> SpouseInfo()
        {
            var spouseData = await _context2.SpouseDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var spouseDataMap = _mapper.Map<ClientInfo>(spouseData);
            if (spouseDataMap is not null)
                spouseDataMap.DateOfBirth = Convert.ToDateTime(spouseDataMap.DateOfBirth).IsNotEmptyDate() ? spouseDataMap.DateOfBirth : null;
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Marital Info", PrevAction = "personal/marital-info", NextTitle = "Contact Info", NextAction = "personal/contact-info" };
            return View("_SpouseInfo", spouseDataMap);
        }
        [HttpPost]
        public async Task<IActionResult> SaveSpouseInfo(ClientInfo spouseInfo)
        {
            var spouseData = await _context2.SpouseDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid)
                .Include(m => m.ClientLogin).Include(m => m.ClientLogin.ClientData)
                .Include(m => m.ClientLogin.UserLogin).Include(m => m.ClientLogin.UserLogin.UserData)
                .Include(m => m.ClientLogin.UserLogin.Integration).FirstOrDefaultAsync();
            var integrationData = spouseData.ClientLogin.UserLogin.Integration;
            if (spouseData is not null)
            {
                var spouseDataObj = _mapper.Map(spouseInfo, spouseData);
                spouseDataObj.ClientId = ClientId;
                _context2.SpouseDatas.Update(spouseDataObj);
                await _context2.SaveChangesAsync();
                var clientData = spouseData.ClientLogin.ClientData;
                await _redtailService.UpdateSpouseContact(ClientLoginGuid, $"{clientData.LastName} Family", clientData.RedtailContactId);
                if (!string.IsNullOrEmpty(integrationData?.OmniUsername))
                {
                    var model = new LoginModel
                    {
                        Username = integrationData.OmniUsername,
                        Password = await integrationData.OmniAccessKey.DecryptAsync()
                    };
                    await _omniService.UpdateClientContact(model, ClientLoginGuid, clientData.OmniContactId);
                }
            }
            var result = new CommonResponse
            {
                Success = spouseData is not null,
                Message = spouseData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { SpouseFirstName = spouseInfo.FirstName }
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "contact-info" })]
        public async Task<IActionResult> ContactInfo()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientContactInfo).FirstOrDefaultAsync();
            var addressData = client.ClientContactInfo;
            var addressDataMap = _mapper.Map<ContactInfoViewModel>(addressData);
            ViewBag.Email = client.Username;
            var maritalStatus = client.ClientData.MaritalStatus;
            bool isMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.IsMarried = isMarried;
            bool isNotMarriedSeprated = client.ClientData.MaritalStatus != MaritalStatusEnum.Married && client.ClientData.MaritalStatus != MaritalStatusEnum.Separated;
            ViewBag.ClientFirstName = client.ClientData.FirstName;
            ViewBag.SpouseFirstName = client.SpouseData != null ? client.SpouseData.FirstName : string.Empty;
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = !isNotMarriedSeprated ? "Spouse Info" : "Client Info",
                PrevAction = !isNotMarriedSeprated ? "personal/spouse-info" : "personal/client-info",
                NextTitle = "Family Info",
                NextAction = "personal/family-info"
            };
            return View("_ContactInfo", addressDataMap);
        }
        [HttpPost]
        public async Task<IActionResult> SaveContactInfo(ContactInfoViewModel contactInfo)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.ClientContactInfo).FirstOrDefaultAsync();
            var contactdata = client.ClientContactInfo;
            var contactObj = _mapper.Map(contactInfo, contactdata);
            contactObj.ClientId = ClientId;
            _context2.ClientContactInfos.Update(contactObj);
            await _context2.SaveChangesAsync();
            var clientData = client.ClientData;
            var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
            await _redtailService.UpdateClientContact(ClientLoginGuid, Convert.ToInt32(TimeZone));
            if (Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate())
                await _redtailService.UpdateSpouseContact(ClientLoginGuid, $"{clientData.LastName} Family", clientData.RedtailContactId);
            var result = new CommonResponse
            {
                Success = contactObj.ClientId > 0,
                Message = contactObj.ClientId > 0 ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "family-info" })]
        public async Task<IActionResult> FamilyInfo()
        {
            var heirs = _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            ViewBag.SpecialNeedsTrust = clientData.SpecialNeedsTrust;

            var ClientType = await _dropdownDataHelper.GetHeirOfAsync(clientData.MaritalStatus.ToString());
            heirs = heirs.Select(m =>
            {
                var HeirOfWithData = ClientType.Where(n => n.Value == m.HeirOf).Select(m => m.Text);
                m.HeirOfWithData = string.Join(" & ", HeirOfWithData);
                return m;
            }).ToList();
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Contact Info", PrevAction = "personal/contact-info", NextTitle = "Personal Property", NextAction = "personal/personal-property" };
            return View("FamilyInfo/_FamilyInfo", heirs);
        }
        public async Task<IActionResult> HeirInfo(Guid? Guid)
        {
            var clientLoginData = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.Heirs).FirstOrDefaultAsync();
            var clientData = clientLoginData.ClientData;

            var ClientType = await _dropdownDataHelper.GetHeirOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var heirInfo = clientLoginData.Heirs.FirstOrDefault(m => m.Guid == Guid);
            if (heirInfo is not null)
            {
                var HeirOfWithData = ClientType.Where(m => m.Value == heirInfo.HeirOf).Select(m => m.Text);
                heirInfo.HeirOfWithData = string.Join(" & ", HeirOfWithData);
            }
            return View("FamilyInfo/_HeirModal", heirInfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveHeir(Heir heir)
        {
            var heirInfo = await _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == heir.Guid).FirstOrDefaultAsync();
            var heirInfoMap = _mapper.Map(heir, heirInfo);
            heirInfoMap.ClientId = ClientId;
            _context2.Heirs.Update(heirInfoMap);
            await _context2.SaveChangesAsync();

            var maritalStatus = (await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync()).MaritalStatus;
            var ClientType = await _dropdownDataHelper.GetHeirOfAsync(maritalStatus.ToString());
            var HeirOfWithData = ClientType.Where(m => m.Value == heirInfoMap.HeirOf).Select(m => m.Text);
            heirInfoMap.HeirOfWithData = string.Join(" & ", HeirOfWithData);
            var result = new CommonResponse
            {
                Success = heir is not null,
                Message = heir is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { heirInfo = heirInfoMap }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteHeir(Guid Guid)
        {
            var heirInfo = await _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (heirInfo is not null)
            {
                _context2.Heirs.Remove(heirInfo);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = heirInfo is not null,
                Message = heirInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "personal-property" })]
        public async Task<IActionResult> PersonalProperty()
        {
            var properties = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            properties = properties.Select(m =>
            {
                var OwnByWithData = ClientType.Where(n => n.Value == m.Ownership).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Family Info", PrevAction = "personal/family-info", NextTitle = "Employment Info", NextAction = "income/employment-info" };
            return View("PropertyInfo/_PropertyInfo", properties);
        }
        public async Task<IActionResult> PropertyInfo(Guid? Guid)
        {
            var clientLoginData = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.ClientProperties).FirstOrDefaultAsync();

            var clientData = clientLoginData.ClientData;
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var properties = clientLoginData.ClientProperties;

            var propertyInfo = properties.FirstOrDefault(m => m.Guid == Guid) ?? new ClientProperty();
            bool hasPrimaryResidence = properties.Any(m => m.Type == PropertyTypeEnum.Primary);
            ViewBag.PropertyType = await DropdownDataHelper.GetPropertyTypeAsync(hasPrimaryResidence);
            if (propertyInfo is not null)
            {
                var OwnByWithData = ClientType.Where(m => m.Value == propertyInfo.Ownership).Select(m => m.Text);
                propertyInfo.OwnByWithData = string.Join(" & ", OwnByWithData);
            }
            var mortgageRent = _context2.ClientBudgets.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid && m.BudgetItem.ItemName == "Mortgage/Rent" && m.Amount > 0);
            if (mortgageRent != null)
                ViewBag.MortgageRent = mortgageRent;
            return View("PropertyInfo/_PropertyModal", propertyInfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveProperty(ClientProperty clientProperty)
        {
            var propertyInfo = await _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == clientProperty.Guid).FirstOrDefaultAsync();
            var clientPropertyMap = _mapper.Map(clientProperty, propertyInfo);
            clientPropertyMap.ClientId = ClientId;
            _context2.ClientProperties.Update(clientPropertyMap);
            await _context2.SaveChangesAsync();
            var maritalStatus = (await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync()).MaritalStatus;
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(maritalStatus.ToString());
            var OwnByWithData = ClientType.Where(m => clientPropertyMap.Ownership == m.Value).Select(m => m.Text);
            clientPropertyMap.OwnByWithData = string.Join(" & ", OwnByWithData);
            bool hasPersonalAssets = _context2.ClientProperties.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            var result = new CommonResponse
            {
                Success = clientProperty is not null,
                Message = clientProperty is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { propertyInfo = clientPropertyMap, hasPersonalAssets }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteProperty(Guid Guid)
        {
            var propertyInfo = await _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (propertyInfo is not null)
            {
                _context2.ClientProperties.Remove(propertyInfo);
                await _context2.SaveChangesAsync();
            }
            bool hasPersonalAssets = _context2.ClientProperties.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            var result = new CommonResponse
            {
                Success = propertyInfo is not null,
                Message = propertyInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { hasPersonalAssets }
            };
            return Json(result);
        }
        public IActionResult ChangeEmailModal()
        {
            ViewBag.ClientLoginGuid = ClientLoginGuid;
            return View("_ChangeEmailModal");
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> SaveEmail(EmailVerification emailVerification)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == emailVerification.Guid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.UserLogin).FirstOrDefaultAsync();
            string initiator = ClientLoginGuid != Guid.Empty ? "you" : "Super Admin";
            bool verified = false;
            if (string.IsNullOrEmpty(emailVerification.VerificationCode))
            {
                verified = true;
                Random generator = new();
                string code = generator.Next(0, 9999).ToString("D4");
                client.VerificationCode = code;

                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
                var emailText = @$"This is to notify you that <span style='color:#0073e9'>{initiator}</span> has initiated the email change process. Please verify by applying the code in the Change Email Address pop-up.<br>
                                   Verification Code: <b style='color:#800000'>{code}</b>";
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                string receiver = _env.EnvironmentName == "Production" ? emailVerification.Email : $"{emailVerification.Email},{_config["Email:SuperAdminEmail"]}";
                await emailHelper.SendEmailAsync(receiver, null, _config["Email:BccEmail"], EmailSubject.EmailChangeRequest, emailHtmlBody, _env.WebRootPath);
            }
            else
            {
                if (emailVerification.VerificationCode == client.VerificationCode)
                {
                    client.Username = emailVerification.Email;
                    verified = true;
                    client.VerificationCode = null;
                }
            }
            await _context2.SaveChangesAsync();
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && verified && !string.IsNullOrEmpty(emailVerification.VerificationCode))
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
                emailTemplateHtml.Replace("@EMAILTEXT", string.Format(EmailNotification.EMAILCHANGE, client.Username));
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(emailVerification.Email, client.UserLogin.Username, _config["Email:BccEmail"], EmailSubject.EmailChangeNotification, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = verified,
                Message = string.IsNullOrEmpty(emailVerification.VerificationCode) ? string.Format(Message.VERIFICATION_CODE_SENT, emailVerification.Email) : verified && !string.IsNullOrEmpty(emailVerification.VerificationCode) ? Message.VARIFIED_SUCCESS : Message.INVALID_VERIFICATION_CODE,
                Data = new { email = verified && !string.IsNullOrEmpty(emailVerification.VerificationCode) ? emailVerification.Email : null, userId = client.UserId }
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _budgetService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~PersonalController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}