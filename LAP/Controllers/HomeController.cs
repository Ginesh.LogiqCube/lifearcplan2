﻿using AutoMapper;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Linq.Dynamic.Core;
using LAP.DomainModels.Models;
using LAP.Services;
using System.Security.Claims;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using LAP.Filters;
using LAP.Areas.Adviser.Models;
using LAP.Model;
using System.IO;
using System.Text;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using Google.Authenticator;
using System.Net.Mail;
using LAP.DomainModels.Entities.Admin;
using Microsoft.AspNetCore.Mvc.Rendering;
using LAP.DomainModels.Entities.Master;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class HomeController : BaseController, IDisposable
    {
        protected readonly FileHelper _fileHelper;
        protected readonly CommonService _commonService;
        protected readonly BudgetService _budgetService;
        protected readonly EmploymentService _employmentService;
        protected readonly InvestmentService _investmentService;
        protected readonly SocialSecurityService _socialSecurityService;
        protected readonly OtherIncomeService _otherIncomeService;
        protected readonly ReportService _reportService;
        protected readonly PropertyService _propertyService;
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly Guid ClientLoginGuid;
        protected readonly int ClientId;
        protected readonly int FirmId;
        private readonly ArcHiveService _arcHiveController;
        public HomeController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper, ArcHiveService arcHiveController) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _commonService = new CommonService(_context2);
            _fileHelper = new FileHelper(_config);
            _budgetService = new BudgetService(_context2, _httpContextAccessor);
            _employmentService = new EmploymentService(_context2, _httpContextAccessor);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
            _socialSecurityService = new SocialSecurityService(_context2);
            _otherIncomeService = new OtherIncomeService(_context2);
            _reportService = new ReportService(_context2, _httpContextAccessor);
            _propertyService = new PropertyService(_context2);
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            ClientLoginGuid = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid() ?? Guid.Empty;
            ClientId = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt() ?? 0;
            FirmId = _httpContextAccessor.HttpContext?.User.Claims.FirstOrDefault(c => c.Type == "FirmId").ClaimToInt() ?? 0;
            _arcHiveController = arcHiveController;
        }
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View("SignIn");
        }
        public async Task<IActionResult> SurveyLastAction()
        {
            var isDisplayOnly = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "DisplayOnlyClient")?.Value;
            if (ClientLoginGuid != Guid.Empty && !Convert.ToBoolean(isDisplayOnly))
            {
                var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                    .Include(m => m.ClientData).Include(m => m.ClientOccupations).FirstOrDefaultAsync();
                var clientData = client.ClientData;
                if (clientData != null)
                {
                    string lastAccessPage = !string.IsNullOrEmpty(clientData.LastAccessPage) ? clientData.LastAccessPage : "";
                    var url = Url.RouteUrl(lastAccessPage.TrimStart('/'));
                    var routeUrl = !string.IsNullOrEmpty(url) ? url.TrimStart('/') : "welcome";
                    return RedirectToRoute(routeUrl);
                }
            }
            return RedirectToRoute("client-info");
        }
        public async Task<IActionResult> Welcome()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                    .Include(m => m.ClientData).Include(m => m.ClientOccupations)
                    .Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            ViewBag.ClientFirstName = clientData.FirstName;
            ViewBag.LastAccessPage = clientData.LastAccessPage;
            ViewBag.PlanningItems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).ToList();
            ViewBag.IsInitialLogin = client.LastLogin == client.SetupCompletedDate;
            ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.token = await ClientLoginGuid.ToString().EncryptAsync();
            ViewBag.InvestmentVersion = clientData.InvestmentVersion;
            ViewBag.RiskArcVersion = clientData.RiskArcVersion;
            var clientOccupation = client.ClientOccupations.Where(m => m.EmploymentOf == ClientTypeEnum.Client);
            var isCompletedProfile = Convert.ToDateTime(clientData.ClientLogin.SetupCompletedDate).IsNotEmptyDate() && Convert.ToDateTime(clientData.DateOfBirth).IsNotEmptyDate();
            ViewBag.IsAolEligible = isCompletedProfile && clientOccupation.Any();
            ViewBag.IsRiskArcEligible = isCompletedProfile && (clientOccupation.Any() || !clientData.RiskArcVersion);
            var riskArcs = _context2.ClientRiskArcs.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            ViewBag.ClientBoardMessage = !string.IsNullOrEmpty(client.UserLogin.UserData.ClientBoardMessage) ? HttpUtility.UrlDecode(client.UserLogin.UserData.ClientBoardMessage).Replace("<a", "<a target='_blank'") : string.Empty;
            return View();
        }
        public async Task<IActionResult> Intro()
        {
            ViewBag.EmptyLayout = true;
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                   .Include(m => m.ClientData).Include(m => m.ClientOccupations).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            ViewBag.ClientName = $"{clientData.FirstName} {clientData.LastName}";
            ViewBag.token = await ClientLoginGuid.ToString().EncryptAsync();
            return View();
        }
        public async Task<IActionResult> Wizard(int parentId = 2, int PlanningItemId = 0)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var SpouseData = client.SpouseData;
            var planningItems = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            var wizardItems = (await MenuDataHelper.GetMenuDataAsync()).Where(m => m.ParentId == parentId);
            wizardItems = parentId == 4 ? wizardItems.Where(m => planningItems.Contains(m.PlanningItemId)) : wizardItems;
            var Controller = PlanningItemId > 0 ? wizardItems.FirstOrDefault(m => m.PlanningItemId == PlanningItemId).Controller : wizardItems.FirstOrDefault().Controller;
            ViewBag.WizardItems = wizardItems;
            ViewBag.Controller = Controller;
            return View("_Wizard");
        }
        public async Task<IActionResult> WizardNav(int parentId = 2)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var planningItems = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            var wizardItems = (await MenuDataHelper.GetMenuDataAsync()).Where(m => m.ParentId == parentId);
            wizardItems = parentId == 4 ? wizardItems.Where(m => planningItems.Contains(m.PlanningItemId)) : wizardItems;
            ViewBag.WizardItems = wizardItems;
            return View("_WizardNav");
        }
        [HttpPost]
        public async Task<IActionResult> SavePlanningItems(List<ClientPlanning> clientPlannings)
        {
            var items = clientPlannings.Where(m => m.Active).Select(m => { m.ClientId = ClientId; return m; });
            _context2.ClientPlannings.UpdateRange(items);

            var deletedItems = clientPlannings.Where(m => m.Guid != Guid.Empty && !m.Active);
            _context2.ClientPlannings.RemoveRange(deletedItems);

            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).FirstOrDefaultAsync();
            client.InitialProfileSetupCompleted = true;
            await _context2.SaveChangesAsync();
            await _commonService.ClearPlanningData(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = items.Any(),
                Message = items.Any() ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = (await MenuDataHelper.GetMenuDataAsync()).FirstOrDefault(m => m.PlanningItemId == items.Min(n => n.PlanningItemId)).Action
            };
            return Json(result);
        }
        public async Task<IActionResult> FinalThought()
        {
            ViewBag.EmptyLayout = true;
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var maritalStatus = client.ClientData.MaritalStatus;
            var ClientName = client.ClientData.FirstName;
            bool isMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
            var SpouseName = isMarried ? client.SpouseData?.FirstName : null;
            var name = isMarried ? SpouseName : ClientName;
            ViewBag.FinalThought = client.ClientData.FinalThought;
            bool isExpress = !client.ClientData.RiskArcVersion;
            var RiskArcText = isExpress ? " Express" : "";
            var RiskArcAction = isExpress ? "express" : "indepth";
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Health Expectations",
                PrevAction = "lifestyle/health-expectations",
            };
            return View();
        }
        public async Task<IActionResult> SaveFinalThought(string finalthought)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid).Include(m => m.ClientData).Include(m => m.UserLogin).FirstOrDefaultAsync();
            client.ClientData.FinalThought = finalthought;
            await _context2.SaveChangesAsync();
            string message = string.Format(EmailNotification.SURVEYCOMLETION, "LifeArcPlan");
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
                var emailText = message;
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(client.Username, client.UserLogin.Username, _config["Email:BccEmail"], EmailSubject.LifeArcPlanSurveyCompletion, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = true,
                Message = message
            };
            return Json(result);
        }
        public IActionResult FinalCompletion()
        {
            return View();
        }
        public void UpdateSingleValue(string table, string key, string value)
        {
            value = value.Any(ch => !char.IsLetterOrDigit(ch)) ? HttpUtility.UrlEncode(value) : value;
            string WhereKey = table != "clientlogin" ? "ClientId" : "Id";
            string selectQuery = $"select Id,(SELECT CAST({key} AS varchar)) [OldValue],COUNT(*)[Count] from client.{table} where {WhereKey} in ({ClientId}) group by Id,{key}";
            var result = _context2.SqlResultModels.FromSqlRaw(selectQuery).FirstOrDefault();
            Type type = key.Contains("Rate") ? typeof(decimal) : typeof(bool);
            if (type == typeof(bool))
            {
                result.OldValue = result.OldValue == "1" ? "true" : "false";
                value = value == "1" || Convert.ToBoolean(value) ? "true" : "false";
            }
            if (result.OldValue != value)
            {
                var updateQuery = $"update client.{table} set {key}='{value}' where {WhereKey} in ({ClientId})";
                _context2.Database.ExecuteSqlRaw(updateQuery);
                string role = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value;
                int UserId = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId")?.Value);


                object oldValue = type == typeof(bool) ? Convert.ToBoolean(result.OldValue) : Convert.ToDecimal(result.OldValue);
                object newValue = type == typeof(bool) ? Convert.ToBoolean(value) : Convert.ToDecimal(value);
                Dictionary<string, FieldChangeModel> changes = new()
                {
                    {
                        key,
                        new FieldChangeModel()
                        {
                            OldValue= oldValue,
                            NewValue= newValue,
                            TypeName= type.FullName
                        }
                    }
                };
                var moduleName = key.Contains("Investment") ? "Investment Planning" :
                                 key.Contains("RiskArc") ? "RiskArc" :
                                 key.Contains("Budget") || key.Contains("Inflation") ? "Budget Info(Indepth)" :
                                 table.ToLower().Contains("client") ? "Client Info" :
                                 table.ToLower().Contains("spouse") ? "Spouse Info" : table;

                var activityLog = new ClientActivityLog
                {
                    OwnerId = ClientId,
                    CreatedByUserId = UserId > 0 ? UserId : null,
                    ActionType = ActionEnum.Update.ToString(),
                    EntityModuleName = moduleName,
                    TimeStamp = DateTime.UtcNow,
                    Role = role,
                    Changes = changes,
                    EntityId = result.Id.ToString(),
                    EntityName = table,
                };
                _context2.ClientActivityLogs.Add(activityLog);
                _context2.SaveChanges();
            }
        }

        public async Task<IActionResult> WizardNextPrevNav(int actionId, bool prev = false)
        {
            var parentId = (await MenuDataHelper.GetMenuDataAsync()).FirstOrDefault(m => m.Id == actionId)?.ParentId;
            parentId = prev ? parentId - 1 : parentId + 1;
            var wizardItems = (await MenuDataHelper.GetMenuDataAsync()).Where(m => m.ParentId == parentId);
            var planningItems = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            wizardItems = parentId == 4 ? wizardItems.Where(m => planningItems.Contains(m.PlanningItemId)) : wizardItems;
            ViewBag.WizardItems = wizardItems;
            return View("_WizardNav", wizardItems);
        }
        public async Task<IActionResult> WizardSubNav(int parentId)
        {
            var menuItems = (await MenuDataHelper.GetMenuDataAsync()).Where(m => m.ParentId == parentId);
            return View("_WizardSubNav", menuItems);
        }
        [AllowAnonymous]
        public async Task<IActionResult> CheckEmailExist(Guid guid, string email, string username)
        {
            email = !string.IsNullOrEmpty(email) ? email : username;
            bool isExistAdmin = await _context2.UserLogins.AnyAsync(m => m.Username == email && m.Guid != guid && m.IsDeleted != true);
            bool isExistAdviser = await _context2.UserLogins.AnyAsync(m => m.Username == email && m.Guid != guid && m.IsDeleted != true);
            bool isExistClient = await _context2.ClientLogins.AnyAsync(m => m.Username == email && m.Guid != guid && m.IsDeleted != true);
            bool isExistLead = await _context2.Leads.AnyAsync(m => m.Email == email && m.Guid != guid && m.IsDeleted != true);
            if (isExistAdmin || isExistAdviser || isExistClient || isExistLead)
                return Json(string.Format(ValidationMessage.ALREADYEXIST, "Email Address"));
            return Json(true);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "arc-hive" })]
        public IActionResult DocumentVault()
        {
            ViewBag.EmptyLayout = true;
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> FetchDocuments(int clientId, DatatableMetaModel meta, DocumentsSearchModel query)
        {
            var role = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;
            var DisplayOnlyClient = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "DisplayOnlyClient")?.Value;

            var documents = _context2.ClientDocuments.Where(m => m.ClientId == clientId);
            if (role == Roles.Client && !Convert.ToBoolean(DisplayOnlyClient))
                documents = documents.Where(m => m.IsAccesibleClient);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            int rowsTotal = documents.Count();
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sortString = DataTableHelper.GetSortString(field, sort);
            sortString = $"IsDeleted asc,{sortString}";
            var result = await documents.PagedListAsync(meta, null, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "tax-documents" })]
        public IActionResult TaxDocuments()
        {
            ViewBag.EmptyLayout = true;
            var years = new List<SelectListItem>();
            for (int i = 1; i <= 3; i++)
                years.Add(new SelectListItem
                {
                    Text = DateTime.UtcNow.AddYears(-i).ToString("yyyy"),
                    Value = DateTime.UtcNow.AddYears(-i).ToString("yyyy"),
                    Selected = i == 1
                });
            ViewBag.Years = years;
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> FetchTaxDocuments(int clientId, int firmId, int year, DatatableMetaModel meta, DocumentsSearchModel query)
        {
            var documents = _context2.TaxDocuments.Where(m => m.ClientLogin.UserLogin.FirmId == firmId && m.Year == year);
            if (clientId > 0)
                documents = documents.Where(m => m.ClientId == clientId);

            string sort = meta.sort.sort;
            string field = meta.sort.field;
            int rowsTotal = documents.Count();
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "ClientLogin";
            var result = await documents.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            var taxPrepStatus = await _context2.TaxPrepStatuses.FirstOrDefaultAsync(m => m.ClientId == clientId && m.Year == year);
            var TaxPrepStatusData = taxPrepStatus != null ? taxPrepStatus.Status : TaxFilingPrepStatusEnum.InitialAppointmentScheduled;
            result.Meta.AdditionalData = new { TaxPrepStatus = TaxPrepStatusData, TaxPrepStatusString = TaxPrepStatusData.GetDisplayName() };
            return Json(result);
        }
        [AllowAnonymous]
        public async Task<IActionResult> TaxDocumentModal(Guid guid)
        {
            var document = await _context2.TaxDocuments.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            var years = new List<SelectListItem>();
            for (int i = 1; i <= 3; i++)
                years.Add(new SelectListItem
                {
                    Text = DateTime.UtcNow.AddYears(-i).ToString("yyyy"),
                    Value = DateTime.UtcNow.AddYears(-i).ToString("yyyy"),
                    Selected = i == 1
                });
            ViewBag.Years = years;
            return View("_TaxDocumentModal", document);
        }

        [AllowAnonymous]
        public async Task<IActionResult> TaxDocumentUpload(TaxDocument taxDocument, int actionByUserId, bool fromClient)
        {
            string bucketPath = $"{_config["BucketName"]}/documents/tax/{taxDocument.Year}";
            var files = HttpContext.Request.Form.Files;
            if (files.Any())
            {
                foreach (var file in files)
                {
                    var uploadedFile = await _fileHelper.UploadFileToS3Bucket(file.OpenReadStream(), bucketPath, file.FileName);
                    _context2.TaxDocuments.Add(new TaxDocument
                    {
                        VersionId = uploadedFile.VersionId,
                        FileSize = file.Length,
                        ClientId = taxDocument.ClientId,
                        FileName = file.FileName,
                        ModifiedDate = DateTime.UtcNow,
                        Year = taxDocument.Year
                    });
                }
                _context2.TaxPrepHistories.Add(new TaxPrepHistory
                {
                    ActionType = "Upload",
                    ClientId = taxDocument.ClientId,
                    Year = taxDocument.Year,
                    ActionByUserId = !fromClient ? actionByUserId : null,
                    FileNames = files.Select(m => m.FileName)
                });
                var documents = _context2.TaxDocuments.Where(m => m.ClientId == taxDocument.ClientId && m.Year == taxDocument.Year).Include(m => m.ClientLogin);
                if (!documents.Any())
                    _context2.TaxPrepStatuses.Add(new TaxPrepStatus
                    {
                        ClientId = taxDocument.ClientId,
                        Status = TaxFilingPrepStatusEnum.InitialTaxDocumentUploaded,
                        Year = taxDocument.Year
                    });
                if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                {
                    var fileNamesList = "<ul>";
                    foreach (var file in files)
                        fileNamesList += $"<li>{file.FileName}</li>";
                    fileNamesList += "</ul>";
                    var client = await _context2.ClientLogins.Where(m => m.Id == taxDocument.ClientId).Include(m => m.ClientData).FirstOrDefaultAsync();
                    var clientData = client.ClientData;
                    StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                    mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                    StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                    actionByUserId = actionByUserId > 0 ? actionByUserId : client.UserId;
                    var adviser = await _context2.UserLogins.Where(m => m.Id == actionByUserId).Include(m => m.UserData).FirstOrDefaultAsync();
                    var toName = fromClient ? adviser.UserData.FirstName : clientData.FirstName;
                    emailTemplateHtml.Replace("@NAME", toName);
                    var name = fromClient ? clientData.Name : adviser.UserData.Name;
                    var emailText = @$"This is to notify you that <b>{name}</b> has uploaded below mentioned document(s) of Year {taxDocument.Year} for further process<span style='color:#0073e9'>{fileNamesList}</span>";
                    emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                    mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                    EmailHelper emailHelper = new(_config);
                    string emailHtmlBody = mainEmailTemplateHtml.ToString();
                    var usersWithTQPermission = _context2.UserLogins.Where(m => m.FirmId == FirmId && (m.AdminRole == AdminRoleEnum.FirmPrincipal || m.AdminRole == AdminRoleEnum.FirmAdmin || (m.AdminRole == AdminRoleEnum.Adviser && !m.IsAssistant) || (m.IsAssistant && m.AssistantPermissions.Any(n => n.Permission == AssistantPermissionEnum.AssistantTQ))) && m.Username != adviser.Username);
                    var toEmail = fromClient ? adviser.Username : client.Username;
                    await emailHelper.SendEmailAsync($"{toEmail},{string.Join(",", usersWithTQPermission.Select(m => m.Username))}", null, _config["Email:BccEmail"], EmailSubject.TaxDocumentNotification, emailHtmlBody, _env.WebRootPath);
                }
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = files.Any(),
                Message = files.Any() ? Message.UPLOAD_SUCCESSFUL : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        [AllowAnonymous]
        public async Task<IActionResult> ArcUpload(IFormFile file, int Id)
        {
            string bucketPath = $"{_config["BucketName"]}/documents/client";
            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(file.OpenReadStream(), bucketPath, file.FileName);
            _context2.ClientDocuments.Add(new ClientDocument
            {
                ClientId = ClientId > 0 ? ClientId : Id,
                FileName = HttpUtility.UrlDecode(file.FileName),
                ContentType = file.ContentType,
                VersionId = uploadedFile.VersionId,
                FileSize = file.Length,
                IsAccesibleClient = true
            });
            await _context2.SaveChangesAsync();
            var arcHiveFileCount = _context2.ClientDocuments.Count(m => m.ClientId == Id);
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(uploadedFile.VersionId),
                Message = !string.IsNullOrEmpty(uploadedFile.VersionId) ? Message.UPLOAD_SUCCESSFUL : Message.INTERNAL_SERVER_ERROR,
                ParentId = Id,
                Data = new { arcHiveFileCount }
            };
            return Json(result);
        }
        [AllowAnonymous]
        public async Task<IActionResult> ArchiveFile(Guid guid)
        {
            var doc = await _context2.ClientDocuments.FirstOrDefaultAsync(m => m.Guid == guid);
            doc.IsDeleted = !Convert.ToBoolean(doc.IsDeleted);
            await _context2.SaveChangesAsync();
            var action = doc.IsDeleted != false ? "archived" : "restored";
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(doc.VersionId),
                Message = !string.IsNullOrEmpty(doc.VersionId) ? string.Format(Message.ARCHIVE_SUCCESSFUL, action) : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        [AllowAnonymous]
        public async Task<IActionResult> FileDownload(string pathName, string fileName, string versionId)
        {
            string bucketPath = $"{_config["BucketName"]}/{pathName}";
            S3FileResult file = await _fileHelper.DownloadFileFromS3Bucket(bucketPath, fileName, versionId);
            return File(file.FileContent, file.ContentType, file.FileName);
        }
        [AllowAnonymous]
        public async Task<IActionResult> CheckAffiliationExist(Guid guid, string name, AffiliationTypeEnum type)
        {
            bool isExist = await _context2.Affiliations.AnyAsync(m => m.Name == name && m.Type == type && m.Guid != guid);
            if (isExist)
                return Json(string.Format(ValidationMessage.ALREADYEXIST, type.GetDisplayName()));
            return Json(true);
        }
        public async Task<IActionResult> AdviserInfo()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).Include(m => m.UserLogin.Firm).Include(m => m.UserLogin.AdviserContactInfos)
                .Include(m => m.UserLogin.UserData.FMO).Include(m => m.UserLogin.UserData.BD).Include(m => m.UserLogin.UserData.RIA).FirstOrDefaultAsync();
            var adviserData = client.UserLogin.UserData;
            string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
            if (!string.IsNullOrEmpty(adviserData.AvatarFileName))
            {
                string extension = Path.GetExtension(adviserData.AvatarFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                ViewBag.avatarImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.AvatarFileName, contentType);
            }
            ViewBag.EmptyLayout = true;
            return View(client.UserLogin);
        }
        public async Task<IActionResult> ExPlicItVue()
        {
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = clientLogin.ClientData;
            var spouseData = clientLogin.SpouseData;
            var planningItems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).Where(m => m.Active);
            bool hasInvestmentPlanning = planningItems.Any(m => m.PlanningItem.ItemName.Contains("Investment"));
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();

            AgeModel ageData = await _commonService.GetAgeData(ClientLoginGuid);
            FVTotalModel clientRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel clientPrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel clientBankCreditUnionFvDetails = new();
            FVTotalModel clientBrokerageFvDetails = new();
            FVTotalModel clientAnnuityContractFvDetails = new();
            FVTotalModel spouseRetirementPlanFvDetails = new();
            FVTotalModel spousePrevRetirementPlanFvDetails = new();
            FVTotalModel spouseBankCreditUnionFvDetails = new();
            FVTotalModel spouseBrokerageFvDetails = new();
            FVTotalModel spouseAnnuityContractFvDetails = new();

            //PropertyTypeEnum[] excludePropertyIncome = new PropertyTypeEnum[] { PropertyTypeEnum.Metals, PropertyTypeEnum.Automobile, PropertyTypeEnum.Jewelry, PropertyTypeEnum.Collectibles };
            int clientPersonalAssets = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Sum(m => m.AnnualIncome) ?? 0;
            int clientPersonalAssetsAtRetirement = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

            int clientPersonalActivityIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Sum(m => m.AnnualIncome) ?? 0;
            int clientPersonalActivityIncomeAtRetirement = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Where(m => m.Years > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

            int spouseOccupationIncome = 0, spouseOccupationIncomeAtRetirement = 0,
                spousePersonalAssets = 0, spousePensionIncome = 0,
                spousePensionIncomeAtRetirement = 0, spousePersonalActivityIncome = 0,
                spousePersonalAssetsAtRetirement = 0, spousePersonalActivityIncomeAtRetirement = 0;

            int clientOccupationIncome = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
            int clientOccupationIncomeAtRetirement = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.RetirementAge)?.Sum(m => m.AnnualEarnings) ?? 0;

            var clientPensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Client);
            int clientPensionIncome = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            int clientPensionIncomeAtRetirement = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            IEnumerable<ClientEsp> spousePensionData = new List<ClientEsp>();
            if (isMarried)
            {
                spouseOccupationIncome = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseOccupationIncomeAtRetirement = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.RetirementAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);

                spousePersonalAssets = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalAssetsAtRetirement = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

                spousePersonalActivityIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalActivityIncomeAtRetirement = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Where(m => m.Years > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

                spousePensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePensionIncome = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
                spousePensionIncomeAtRetirement = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            }

            var budgetItems = await _budgetService.GetBudgetItemsAsync(ClientLoginGuid);
            FVTotalModel inflationaryFvDetails = clientData.BudgetVersion ? await _budgetService.GetInflationaryBudgetFutureValue(ClientLoginGuid) : await _budgetService.GetInfaltionaryBudgetFutureValueExpressAsync(ClientLoginGuid);
            var nonInflationary = clientData.BudgetVersion ? budgetItems.Where(m => !m.Inflationary && (m.YearsPayOff > ageData.YearToRetirement || !m.BudgetItem.IsRevolvingDebt))?.Sum(m => m.Amount) ?? 0 : clientData.NonInflationaryBudget;
            int clientSocialSecurityIncome = 0, spouseSocialSecurityIncome = 0, clientSocialSecurityIncomeAtRetirement = 0, spouseSocialSecurityIncomeAtRetirement = 0;
            int clientSocialSecurityAge = 0, spouseSocialSecurityAge = 0;
            var clientSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Client);

            if (clientSocialSecurity != null)
            {
                clientSocialSecurityAge = clientSocialSecurity.sSageBenifitsEnum;
                if (clientSocialSecurity.ssreceiving)
                {
                    clientSocialSecurityIncome = clientSocialSecurity.ssmonthly * 12;
                    clientSocialSecurityIncomeAtRetirement = clientSocialSecurity.ssmonthly * 12;
                }
                else
                {
                    clientSocialSecurityIncome = ageData.Client.Age >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                    clientSocialSecurityIncomeAtRetirement = ageData.Client.RetirementAge >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                }
            }

            if (isMarried && spouseData != null)
            {
                var spouseSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                if (spouseSocialSecurity != null)
                {
                    spouseSocialSecurityAge = spouseSocialSecurity.sSageBenifitsEnum;
                    if (spouseSocialSecurity.ssreceiving)
                    {
                        spouseSocialSecurityIncome = spouseSocialSecurity.ssmonthly * 12;
                        spouseSocialSecurityIncomeAtRetirement = spouseSocialSecurity.ssmonthly * 12;
                    }
                    else
                    {
                        spouseSocialSecurityIncome = ageData.Spouse.Age >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                        spouseSocialSecurityIncomeAtRetirement = ageData.Spouse.RetirementAge >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                    }
                }
            }

            if (clientData.InvestmentVersion)
            {
                var accountTypes = new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse };
                var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, bankingAccountTypes);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, accountTypes);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                if (isMarried)
                {
                    spouseBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, bankingAccountTypes);
                    spouseBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, accountTypes);
                    spouseAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                }
            }
            else
            {
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractExpressFutureValueDetailsAsync(ClientLoginGuid);
            }

            int totalValueOfReturn = clientRetirementPlanFvDetails.ValueOfReturn + spouseRetirementPlanFvDetails.ValueOfReturn
                + clientPrevRetirementPlanFvDetails.ValueOfReturn + spousePrevRetirementPlanFvDetails.ValueOfReturn
                + clientBankCreditUnionFvDetails.ValueOfReturn + spouseBankCreditUnionFvDetails.ValueOfReturn
                + clientBrokerageFvDetails.ValueOfReturn + spouseBrokerageFvDetails.ValueOfReturn
                + clientAnnuityContractFvDetails.ValueOfReturn + spouseAnnuityContractFvDetails.ValueOfReturn;

            int totalPresentValue = clientRetirementPlanFvDetails.PresentValue + spouseRetirementPlanFvDetails.PresentValue
                + clientPrevRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue
                + clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue
                + clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue
                + clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue;

            int clientInvestmentIncome = clientBankCreditUnionFvDetails.Income + clientBrokerageFvDetails.Income + clientAnnuityContractFvDetails.Income;
            int spouseInvestmentIncome = spouseBankCreditUnionFvDetails.Income + spouseBrokerageFvDetails.Income + spouseAnnuityContractFvDetails.Income;
            int totalPmtValue = clientRetirementPlanFvDetails.PmtValue + spouseRetirementPlanFvDetails.PmtValue +
                                clientBankCreditUnionFvDetails.PmtValue + spouseBankCreditUnionFvDetails.PmtValue +
                                clientBrokerageFvDetails.PmtValue + spouseBrokerageFvDetails.PmtValue +
                                clientAnnuityContractFvDetails.PmtValue + spouseAnnuityContractFvDetails.PmtValue;

            var totalIncomeAtRetirement = clientOccupationIncomeAtRetirement + spouseOccupationIncomeAtRetirement +
                                          clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                          clientPersonalActivityIncomeAtRetirement + spousePersonalActivityIncomeAtRetirement +
                                          clientPersonalAssetsAtRetirement + spousePersonalAssetsAtRetirement +
                                          clientInvestmentIncome + spouseInvestmentIncome +
                                          clientSocialSecurityIncomeAtRetirement + spouseSocialSecurityIncomeAtRetirement +
                                          clientPensionIncomeAtRetirement + spousePensionIncomeAtRetirement;

            ExplicitVueModel explicitVueData = new()
            {
                BudgetVersion = clientData.BudgetVersion,
                InvestmentVersion = clientData.InvestmentVersion,
                Income = new()
                {
                    ClientEmployment = clientOccupationIncome,
                    SpouseEmployment = spouseOccupationIncome,
                    ClientPreviousEmployment = clientPrevRetirementPlanFvDetails.Income,
                    SpousePreviousEmployment = spousePrevRetirementPlanFvDetails.Income,
                    ClientPersonalActivity = clientPersonalActivityIncome,
                    SpousePersonalActivity = spousePersonalActivityIncome,
                    ClientPersonalAsset = clientPersonalAssets,
                    SpousePersonalAsset = spousePersonalAssets,
                    ClientInvestment = clientInvestmentIncome,
                    SpouseInvestment = spouseInvestmentIncome,
                    ClientSocialSecurity = clientSocialSecurityIncome,
                    SpouseSocialSecurity = spouseSocialSecurityIncome,
                    ClientSocialSecurityAtRetirement = clientSocialSecurityIncomeAtRetirement,
                    SpouseSocialSecurityAtRetirement = spouseSocialSecurityIncomeAtRetirement,
                    ClientPension = clientPensionIncome,
                    SpousePension = spousePensionIncome,
                    PensionAtRetirement = clientPensionIncomeAtRetirement + spousePensionIncomeAtRetirement
                },
                Expense = new()
                {
                    Inflationary = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), 1, inflationaryFvDetails.PresentValue),
                    InflationaryProjectedAtRetirement = inflationaryFvDetails.FutureValue,
                    NonInflationary = nonInflationary
                },
                TotalPv = new()
                {
                    ClientEmploymentPlan = clientRetirementPlanFvDetails.PresentValue,
                    ClientEmploymentPlanProjectedAtRetirement = clientRetirementPlanFvDetails.FutureValue,
                    SpouseEmploymentPlan = spouseRetirementPlanFvDetails.PresentValue,
                    SpouseEmploymentPlanProjectedAtRetirement = spouseRetirementPlanFvDetails.FutureValue,

                    ClientPreEmploymentPlan = clientPrevRetirementPlanFvDetails.PresentValue,
                    ClientPreEmploymentPlanProjectedAtRetirement = clientPrevRetirementPlanFvDetails.FutureValue,
                    SpousePreEmploymentPlan = spousePrevRetirementPlanFvDetails.PresentValue,
                    SpousePreEmploymentPlanProjectedAtRetirement = spousePrevRetirementPlanFvDetails.FutureValue,

                    ClientBankingAccount = clientBankCreditUnionFvDetails.PresentValue,
                    ClientBankingAccountProjectedAtRetirement = clientBankCreditUnionFvDetails.FutureValue,
                    SpouseBankingAccount = spouseBankCreditUnionFvDetails.PresentValue,
                    SpouseBankingAccountProjectedAtRetirement = spouseBankCreditUnionFvDetails.FutureValue,

                    ClientBrokerageAccount = clientBrokerageFvDetails.PresentValue,
                    ClientBrokerageAccountProjectedAtRetirement = clientBrokerageFvDetails.FutureValue,
                    SpouseBrokerageAccount = spouseBrokerageFvDetails.PresentValue,
                    SpouseBrokerageAccountProjectedAtRetirement = spouseBrokerageFvDetails.FutureValue,

                    ClientInsuranceAccount = clientAnnuityContractFvDetails.PresentValue,
                    ClientInsuranceAccountProjectedAtRetirement = clientAnnuityContractFvDetails.FutureValue,
                    SpouseInsuranceAccount = spouseAnnuityContractFvDetails.PresentValue,
                    SpouseInsuranceAccountProjectedAtRetirement = spouseAnnuityContractFvDetails.FutureValue,
                    TotalPmtValue = totalPmtValue
                }
            };
            explicitVueData.CashFlow = new()
            {
                TotalIncome = explicitVueData.Income.Total,
                TotalExpense = explicitVueData.Expense.Total,
            };
            var incomeNeededAtRetirement = inflationaryFvDetails.FutureValue + nonInflationary;
            var investmentNeededAtRetirement = _reportService.GetInvestmentNeededAtRetirement(incomeNeededAtRetirement, totalIncomeAtRetirement);
            explicitVueData.NeededAtRetirement = new()
            {
                Income = incomeNeededAtRetirement,
                InvestmentAccount = investmentNeededAtRetirement
            };
            double requiredRoR = ExtensionMethod.CalculateRate(ageData.YearToRetirement, explicitVueData.TotalPv.TotalPmtValue, explicitVueData.TotalPv.Total, investmentNeededAtRetirement);
            explicitVueData.AnnualAvgRateOfReturn = new()
            {
                Current = _reportService.GetCurrentRoR(totalPresentValue, totalValueOfReturn),
                Required = requiredRoR
            };
            return View("_ExPlicItVue", explicitVueData);
        }
        [AllowAnonymous]
        public async Task<ClientSurveyModel> ClientSurveyDataAsync(string token, int tz)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid ClientLoginGuid = new(token);
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientContactInfo)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.UserData)
                .Include(m => m.UserLogin.AdviserContactInfos).Include(m => m.ClientContactInfo).AsNoTracking().FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var adviserData = client.UserLogin.UserData;
            string fileName = $"My Personal Canvas {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
            var EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString(), ClientLoginGuid);
            string businessLogoImage = string.Empty;
            if (!string.IsNullOrEmpty(adviserData.BusinessLogoFileName))
            {
                string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
                string extension = Path.GetExtension(adviserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                businessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.BusinessLogoFileName, contentType);
            }
            var model = new ClientSurveyModel
            {
                ClientLoginData = client,
                FileName = fileName,
                EmploymentOf = EmploymentOf,
                BusinessLogoImage = businessLogoImage
            };
            var heirs = await _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            var ClientTypes = await _dropdownDataHelper.GetHeirOfAsync(clientData.MaritalStatus.ToString(), ClientLoginGuid);
            var ClientTypeOwners = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString(), ClientLoginGuid);
            heirs = heirs.Select(m =>
            {
                var HeirOfWithData = ClientTypes.Where(n => n.Value == m.HeirOf).Select(m => m.Text);
                m.HeirOfWithData = string.Join(" & ", HeirOfWithData);
                return m;
            }).ToList();
            model.ClientLoginData.Heirs = heirs;
            var clientProperties = await _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            clientProperties = clientProperties.Select(m =>
            {
                var ownByWithData = ClientTypeOwners.Where(n => n.Value == m.Ownership).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", ownByWithData);
                return m;
            }).ToList();
            model.ClientLoginData.ClientProperties = clientProperties;
            var occupations = await _context2.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Include(m => m.ClientEsp).ToListAsync();

            occupations = occupations.Select(m =>
            {
                var YearsToRetire = m.EmploymentOf == ClientTypeEnum.Client ? clientData.DateOfBirth.GetRemainYearToRetirement(m.AgeToRetire) : spouseData.DateOfBirth.GetRemainYearToRetirement(m.AgeToRetire);
                m.YearsToRetire = m.EmploymentStatus != EmploymentStatusEnum.Retired ? YearsToRetire ?? 0 : 0;
                m.DOB = m.EmploymentOf == ClientTypeEnum.Spouse ? spouseData.DateOfBirth : clientData.DateOfBirth;
                var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.EmploymentOf).ToString()).Text;
                m.EmployerTitle = !string.IsNullOrEmpty(m.Employer) ? $"{name}'s Occupation - {m.Employer}" : $"{name}'s Occupation - {m.EmploymentStatus.GetDisplayName()}";
                return m;
            }).ToList();
            model.ClientLoginData.ClientOccupations = occupations;
            var prevRetirementPlans = await _context2.ClientPrevEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            prevRetirementPlans = prevRetirementPlans.Select(m =>
            {
                var EmploymentOfWithData = ClientTypes.Where(n => n.Value == ((int)m.EmploymentOf).ToString()).Select(m => m.Text);
                m.EmploymentOfWithData = string.Join(" & ", EmploymentOfWithData);
                return m;
            }).ToList();
            model.ClientLoginData.ClientPrevEsps = prevRetirementPlans;
            var bussinessInterests = await _context2.BusinessInterests.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            bussinessInterests = bussinessInterests.Select(m =>
            {
                var FirmOwnerShipString = ClientTypeOwners.Where(n => n.Value == m.FirmOwnershipEnum).Select(m => m.Text);
                m.FirmOwnerShipString = string.Join(" & ", FirmOwnerShipString);
                m.businessInterestOfEnumString = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.businessInterestOf).ToString()).Text;
                return m;
            }).ToList();
            model.ClientLoginData.BusinessInterests = bussinessInterests;
            var otherincome = await _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            otherincome = otherincome.Select(m =>
            {
                var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.businessInterestof).ToString()).Text;
                m.businessInterestofString = name;
                return m;
            }).ToList();
            model.ClientLoginData.OtherIncomes = otherincome;
            var socialSecurities = await _context2.SocialSecurities.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            socialSecurities = socialSecurities.Select(m =>
            {
                var name = EmploymentOf.FirstOrDefault(n => n.Value.ToString() == ((int)m.EmploymentOf).ToString()).Text;
                m.EmploymentOfWithData = name;
                return m;
            }).ToList();
            model.ClientLoginData.SocialSecurities = socialSecurities;
            var InvestmentLiteData = !clientData.InvestmentVersion ? await _context2.InvestmentLiteDatas.FirstOrDefaultAsync(m => m.ClientLogin.Guid == ClientLoginGuid) : new InvestmentLiteData();
            model.InvestmentLiteData = InvestmentLiteData;
            var bankCreditUnionAccounts = await _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            bankCreditUnionAccounts = bankCreditUnionAccounts.Select(m =>
            {
                var OwnByWithData = ClientTypeOwners.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            model.ClientLoginData.BankCreditUnionAccounts = bankCreditUnionAccounts;
            var brokerageAdvisoryAccounts = await _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            model.ClientLoginData.BrokerageAdvisoryAccounts = brokerageAdvisoryAccounts;
            brokerageAdvisoryAccounts = brokerageAdvisoryAccounts.Select(m =>
            {
                var OwnByWithData = ClientTypeOwners.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            var lifeAnnuityContracts = await _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            lifeAnnuityContracts = lifeAnnuityContracts.Select(m =>
            {
                var OwnByWithData = ClientTypeOwners.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            model.ClientLoginData.LifeAnnuityContracts = lifeAnnuityContracts;
            var clientBudget = (await _budgetService.GetBudgetItemsAsync(ClientLoginGuid)).Where(m => m.Amount > 0).ToList();
            model.ClientLoginData.ClientBudgets = clientBudget;
            var planningItems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).Where(m => m.Active).ToList();
            model.PlanningItems = planningItems;
            var retirementPlanningStatuses = await _context2.RetirementStatuses.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            model.ClientLoginData.RetirementStatuses = retirementPlanningStatuses;
            var healthcarePlanning = await _context2.HealthCarePlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            model.ClientLoginData.HealthCarePlanning = healthcarePlanning ?? new HealthCarePlanning();
            var personalCarePlannings = await _context2.PersonalCares.Where(m => m.ClientLogin.Guid == ClientLoginGuid)
                .Include(m => m.ClientLogin).Include(m => m.ClientLogin.ClientData).ToListAsync();
            personalCarePlannings = personalCarePlannings.Select(m =>
            {
                m.Name = heirs.FirstOrDefault(n => n.Id == m.CareGiverId)?.FirstName;
                return m;
            }).ToList();
            model.ClientLoginData.PersonalCares = personalCarePlannings;
            var incomeConcerns = (await _commonService.GetIncomeConcernAsync(ClientLoginGuid)).ToList();
            model.ClientLoginData.ClientIncomeConcerns = incomeConcerns;
            var expenseConcerns = (await _commonService.GetExpenseConcernAsync(ClientLoginGuid)).ToList();
            model.ClientLoginData.ClientExpenseConcern = expenseConcerns;
            var educationPlannings = await _context2.EducationPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
            model.ClientLoginData.EducationPlannings = educationPlannings;
            var charitablePlanning = await _context2.CharitablePlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            model.ClientLoginData.CharitablePlanning = charitablePlanning ?? new CharitablePlanning();
            var estatePlanningItems = (await _commonService.GetEstateItemAsync(ClientLoginGuid)).ToList();
            model.ClientLoginData.ClientEstate = estatePlanningItems;

            var clientHealth = (await _commonService.GetHealthAsync(ClientLoginGuid)).ToList();
            model.ClientLoginData.ClientHealth = clientHealth;
            var clientExpectationItems = (await _commonService.GetExpectationItemsAsync(ClientLoginGuid)).ToList();
            model.ClientLoginData.ClientExpectations = clientExpectationItems;
            return model;
        }
        [AllowAnonymous]
        public async Task<IActionResult> ClientSurvey(string token, int tz)
        {
            var model = await ClientSurveyDataAsync(token, tz);
            if (model != null)
            {
                var footerAction = _httpContextAccessor.HttpContext.GetAbsoluteUrl("Home/ReportFooter");
                string customSwitches = string.Format("--footer-html {0} --footer-spacing -10", footerAction);
                var pdfViewAction = new ViewAsPdf(model)
                {
                    PageSize = Size.Letter,
                    PageMargins = { Left = 0, Bottom = 13, Right = 0, Top = 10 },
                    CustomSwitches = customSwitches,
                    ContentDisposition = ContentDisposition.Inline,
                    FileName = model.FileName
                };
                return pdfViewAction;
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        [AllowAnonymous]
        public ActionResult ReportFooter()
        {
            return View();
        }
        public async Task<IActionResult> RiskArcBasicDetail()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            ClientViewModel clientViewModel = new();
            if (client is not null)
            {
                ClientInfo spouseInfo = new();
                bool isMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                if (isMarried)
                {
                    spouseInfo = new ClientInfo()
                    {
                        FirstName = client.SpouseData.FirstName,
                        MiddleInitial = client.SpouseData.MiddleInitial,
                        LastName = client.SpouseData.LastName,
                        DateOfBirth = client.SpouseData.DateOfBirth
                    };
                }
                clientViewModel = new ClientViewModel()
                {
                    Id = client.Id,
                    Guid = client.Guid,
                    Email = client.Username,
                    IsMarried = Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate(),
                    ClientInfo = new ClientInfo()
                    {
                        FirstName = client.ClientData.FirstName,
                        MiddleInitial = client.ClientData.MiddleInitial,
                        LastName = client.ClientData.LastName,
                        DateOfBirth = client.ClientData.DateOfBirth.IsNotEmptyDate() ? client.ClientData.DateOfBirth : null
                    },
                    SpouseInfo = spouseInfo
                };
            }
            ViewBag.MaritalStatus = await DropdownDataHelper.GetMaritaStatusTypeAsync();
            return View("_RiskArcBasicDetail", clientViewModel);
        }
        public async Task<IActionResult> AdviserRegulatoryDocuments()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                    .Include(m => m.UserLogin).FirstOrDefaultAsync();
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == client.UserLogin.Guid)
                .Include(m => m.UserData).Include(m => m.Permissions).Include(m => m.AdviserContactInfos).FirstOrDefaultAsync();
            ViewBag.isValidDateOfBirth = adviserDataObj.UserData.DateOfBirth is not null;
            ViewBag.isValidContactInfo = adviserDataObj.AdviserContactInfos is not null;
            ViewBag.hasRIA = adviserDataObj.UserData.RIAId > 0;
            ViewBag.token = await client.UserLogin.Guid.ToString().EncryptAsync();
            ViewBag.EmptyLayout = true;
            return View("RegulatoryDocuments");
        }
        public async Task<IActionResult> Settings()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).FirstOrDefaultAsync();
            ViewBag.EmptyLayout = true;
            var twoFactorAuthenticator = new TwoFactorAuthenticator();
            var accountSecretKey = $"{_config["SecretKey"]}-{client.Username}";
            var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], client.Username, Encoding.ASCII.GetBytes(accountSecretKey));
            ViewBag.SetupCode = setupCode;
            ViewBag.HasTwoFactorAuth = client.TwoFactorAuth;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SaveTwoFactorAuth(string VerificationCode, bool TwoFactorAuth)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).FirstOrDefaultAsync();

            bool isAuthenticated = false;
            if (TwoFactorAuth)
            {
                var accountSecretKey = $"{_config["SecretKey"]}-{client.Username}";
                var twoFactorAuthenticator = new TwoFactorAuthenticator();
                isAuthenticated = twoFactorAuthenticator.ValidateTwoFactorPIN(accountSecretKey, VerificationCode, TimeSpan.FromSeconds(30));
                if (isAuthenticated)
                {
                    await SendQrCodeToClientEmail(client.Username, client.ClientData.FirstName);
                    client.TwoFactorAuth = TwoFactorAuth;
                }
            }
            else
                client.TwoFactorAuth = TwoFactorAuth;
            await _context2.SaveChangesAsync();
            var action = client.TwoFactorAuth ? "activated" : "deactivated";
            var result = new CommonResponse
            {
                Success = isAuthenticated || !TwoFactorAuth,
                Message = isAuthenticated || !TwoFactorAuth ? string.Format(Message.TWO_FACTOR_AUTH, action) : Message.INVALID_VERIFICATION_CODE,
                RedirectUrl = isAuthenticated || !TwoFactorAuth ? Url.RouteUrl("welcome") : null,
                Data = new { client.TwoFactorAuth },
            };
            return Json(result);
        }
        [AllowAnonymous]
        public async Task SendQrCodeToClientEmail(string Email, string FirstName)
        {
            var twoFactorAuthenticator = new TwoFactorAuthenticator();
            var accountSecretKey = $"{_config["SecretKey"]}-{Email}";
            var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], Email, Encoding.ASCII.GetBytes(accountSecretKey));
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml.Replace("@NAME", FirstName);
                string emailText = $@"<p>Congratulations! You have successfully completed the 2-Step verification setup process.</p>
                                      <p>In the event you lose access to the LifeArcPlan account in your mobile phone's Authenticator App, please send an email to info@lifearcplan.com or call (888) 302-5571 to receive a Setup Key for account re-activation.</p>";
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                var bytes = Convert.FromBase64String(setupCode.QrCodeSetupImageUrl.Replace($"data:image/png;base64,", ""));
                var contents = new MemoryStream(bytes);
                List<LinkedResource> resources = new()
                {
                    new LinkedResource(contents)
                    {
                        ContentId = "QRCode"
                    }
                };
                await emailHelper.SendEmailAsync(Email, null, _config["Email:BccEmail"], EmailSubject.TwoFactorAuth, emailHtmlBody, _env.WebRootPath, resources);
            }
        }
        [HttpPost]
        public async Task<IActionResult> SaveClientReports()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            if (client != null &&
                (client.HasChangesAol || client.HasChangesCanvas || client.HasChangesClientRiskArc || client.HasChangesSpouseRiskArc))
            {
                var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
                bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                _arcHiveController.SaveReports(client.Guid, IsMarried, Convert.ToInt32(TimeZone));

                client.HasChangesAol = false;
                client.HasChangesCanvas = false;
                client.HasChangesClientRiskArc = false;
                client.HasChangesSpouseRiskArc = false;
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = client != null,
                Message = client != null ? Message.ADV2BSAVED : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        public async Task<IActionResult> ActivityLog(string token)
        {
            token = await token.DecryptAsync();
            if (token != "InvalidToken")
            {
                Guid guid = new(token);
                ViewBag.EmptyLayout = true;
                var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid)
                    .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
                var clientName = client.ClientData.FirstName;
                var spouseName = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null ? client.SpouseData.FirstName : null;
                string[] names = { clientName, spouseName };
                ViewBag.Names = names;
                var activities = _context2.ClientActivityLogs.Where(m => m.Owner.Guid == guid && m.ParentId == null)
                    .Include(m => m.Owner).Include(m => m.Owner.ClientData).Include(m => m.CreatedByUser).Include(m => m.CreatedByUser.UserData).OrderByDescending(m => m.TimeStamp);
                var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
                ViewBag.TimeZone = Convert.ToInt32(TimeZone);
                return View(activities);
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        [AllowAnonymous]
        public async Task<IActionResult> GetActivityLogChildren(int ParentId, Guid guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == guid)
                    .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientName = client.ClientData.FirstName;
            var spouseName = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null ? client.SpouseData.FirstName : null;
            string[] names = { clientName, spouseName };
            ViewBag.Names = names;
            var activities = _context2.ClientActivityLogs.Where(m => m.ParentId == ParentId)
                .Include(m => m.Owner).Include(m => m.Owner.ClientData).Include(m => m.CreatedByUser).Include(m => m.CreatedByUser.UserData).OrderByDescending(m => m.TimeStamp);
            string riskArcOwner = string.Empty;
            if (activities.Any(m => m.EntityName == "ClientRiskArc"))
            {
                var clientRiskArc = activities.FirstOrDefault(m => m.EntityName == "ClientRiskArc");
                var risk = _context2.ClientRiskArcs.FirstOrDefault(m => m.Id == Convert.ToInt32(clientRiskArc.EntityId));
                riskArcOwner = risk.ClientType == ClientTypeEnum.Client ? "{clientName}'s" : "{spouseName}'s";
            }
            ViewBag.RiskArcOwner = riskArcOwner;
            return View("_ActivityLogChildrenModal", activities);
        }
        public async Task<IActionResult> Dashboard()
        {
            ViewBag.EmptyLayout = true;
            var planningITems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).Where(m => m.Active);
            bool hasEstatePlanning = await planningITems.AnyAsync(m => m.PlanningItem.ItemName.Contains("Estate"));
            ViewBag.HasEstatePlanning = hasEstatePlanning;
            return View();
        }
        public async Task<IActionResult> FetchDashboardConfig()
        {
            var client = await _context2.ClientDatas.FirstOrDefaultAsync(m => m.ClientId == ClientId);
            return Json(client.DashboardConfig);
        }
        public async Task<IActionResult> UpdateDashboardConfig(List<DashboardConfigModel> config)
        {
            var client = await _context2.ClientDatas.FirstOrDefaultAsync(m => m.ClientId == ClientId);
            client.DashboardConfig = config;
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = true
            };
            return Json(result);
        }
        public async Task<IActionResult> RetirementAssets()
        {
            ViewBag.DashboardCardTitle = "Retirement Assets";
            ViewBag.DashboardCardContainer = "retirement_assets";
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
               .Include(m => m.UserLogin).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = clientLogin.ClientData;
            var spouseData = clientLogin.SpouseData;
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            var planningITems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).Where(m => m.Active);
            bool hasInvestmentPlanning = await planningITems.AnyAsync(m => m.PlanningItem.ItemName.Contains("Investment"));

            var properties = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            var personalPropertyaSellAtRetirement = properties.Where(m => m.PropertyPlan == ProprtyPlanEnum.SellToFund)?.Sum(m => m.CurrentValue) ?? 0;
            FVTotalModel clientRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel clientPrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel spouseRetirementPlanFvDetails = new();
            FVTotalModel spousePrevRetirementPlanFvDetails = new();
            FVTotalModel clientBankCreditUnionFvDetails = new();
            FVTotalModel clientBrokerageFvDetails = new();
            FVTotalModel clientLifeFvDetails = new();
            FVTotalModel clientAnnuityContractFvDetails = new();
            FVTotalModel spouseBankCreditUnionFvDetails = new();
            FVTotalModel spouseBrokerageFvDetails = new();
            FVTotalModel spouseLifeFvDetails = new();
            FVTotalModel spouseAnnuityContractFvDetails = new();
            if (isMarried)
            {
                spouseRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
            }
            if (hasInvestmentPlanning)
            {
                var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                if (clientData.InvestmentVersion)
                {
                    clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, bankingAccountTypes);
                    clientLifeFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Life });
                    clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                    clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse });
                    if (isMarried)
                    {
                        spouseBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, bankingAccountTypes);
                        spouseLifeFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Life });
                        spouseAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                        spouseBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse });
                    }
                }
                else
                {
                    clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionExpressFutureValueDetailsAsync(ClientLoginGuid);
                    clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractExpressFutureValueDetailsAsync(ClientLoginGuid);
                    clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryExpressFutureValueDetailsAsync(ClientLoginGuid);
                }
            }
            var result = new RetirementAssetModel
            {
                PersonalPropertyForSellAtRetirement = personalPropertyaSellAtRetirement,
                RetirementAccount = clientRetirementPlanFvDetails.PresentValue + clientPrevRetirementPlanFvDetails.PresentValue +
                                    spouseRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue,
                BankCreditUnionAccount = clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue,
                BrokerageAdvisoryAccount = clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue,
                LifeAnnuityAccount = clientLifeFvDetails.PresentValue + spouseLifeFvDetails.PresentValue +
                                     clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue,
            };
            return View("Dashboard/RetirementAssets", result);
        }
        public async Task<IActionResult> NetWorth()
        {
            ViewBag.DashboardCardTitle = "Net Worth";
            ViewBag.DashboardCardContainer = "net_worth";
            var properties = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            var includeRealProperty = new PropertyTypeEnum[] { PropertyTypeEnum.Primary, PropertyTypeEnum.Secondary, PropertyTypeEnum.Vacation, PropertyTypeEnum.RealEstate, PropertyTypeEnum.Land };
            var realProperties = properties.Where(m => includeRealProperty.Contains(m.Type));
            var personalProperties = properties.Where(m => !includeRealProperty.Contains(m.Type));
            var realPropertyPv = realProperties.Sum(m => m.CurrentValue);
            var personalPropertyPv = personalProperties.Sum(m => m.CurrentValue);
            var primaryResidence = properties.Where(m => m.Type == PropertyTypeEnum.Primary).Sum(m => m.CurrentValue);
            var assetsTotalPv = properties?.Sum(m => m.CurrentValue) ?? 0;
            var totalPropertyDebt = await properties?.SumAsync(m => m.AmountOwed) ?? 0;
            var budgetItems = await _budgetService.GetBudgetItemsAsync(ClientLoginGuid);
            var totalBudgetDebt = budgetItems?.Sum(m => m.BalanceOwn) ?? 0;
            var result = new NetWorthModel
            {
                RealProperty = realPropertyPv,
                PersonalProperty = personalPropertyPv,
                ClientAssetsTotal = assetsTotalPv,
                ClientDebtTotal = totalPropertyDebt + totalBudgetDebt
            };
            return View("Dashboard/NetWorth", result);
        }
        public async Task<IActionResult> CashFlow()
        {
            ViewBag.DashboardCardTitle = "Cash Flow";
            ViewBag.DashboardCardContainer = "cash_flow";
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
              .Include(m => m.UserLogin).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = clientLogin.ClientData;
            var spouseData = clientLogin.SpouseData;
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            AgeModel ageData = await _commonService.GetAgeData(ClientLoginGuid);

            FVTotalModel clientPrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel spousePrevRetirementPlanFvDetails = new();
            var clientPensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Client);
            int clientPensionIncomeAmount = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            int clientOccupationIncome = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
            int clientOtherIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Sum(m => m.AnnualIncome) ?? 0;

            FVTotalModel clientBankCreditUnionFvDetails = new();
            FVTotalModel clientBrokerageFvDetails = new();
            FVTotalModel clientAnnuityContractFvDetails = new();

            FVTotalModel spouseBankCreditUnionFvDetails = new();
            FVTotalModel spouseBrokerageFvDetails = new();
            FVTotalModel spouseAnnuityContractFvDetails = new();

            int spouseOccupationIncome = 0, spouseOtherIncome = 0, spousePensionIncomeAmount = 0,
                clientSocialSecurityIncome = 0, spouseSocialSecurityIncome = 0,
                clientSocialSecurityAge = 0, spouseSocialSecurityAge = 0; ;
            IEnumerable<ClientEsp> spousePensionData = new List<ClientEsp>();
            if (isMarried)
            {
                spouseOccupationIncome = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
                spousePrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePensionIncomeAmount = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            }
            if (clientData.InvestmentVersion)
            {
                var accountTypes = new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse };
                var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, bankingAccountTypes);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, accountTypes);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                if (isMarried)
                {
                    spouseBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, bankingAccountTypes);
                    spouseBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, accountTypes);
                    spouseAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                }
            }
            else
            {
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractExpressFutureValueDetailsAsync(ClientLoginGuid);
            }
            var clientSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Client);

            if (clientSocialSecurity != null)
            {
                clientSocialSecurityAge = clientSocialSecurity.sSageBenifitsEnum;
                if (clientSocialSecurity.ssreceiving)
                    clientSocialSecurityIncome = clientSocialSecurity.ssmonthly * 12;
                else
                    clientSocialSecurityIncome = ageData.Client.Age >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
            }
            if (isMarried && spouseData != null)
            {
                var spouseSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                if (spouseSocialSecurity != null)
                {
                    spouseSocialSecurityAge = spouseSocialSecurity.sSageBenifitsEnum;
                    if (spouseSocialSecurity.ssreceiving)
                        spouseSocialSecurityIncome = spouseSocialSecurity.ssmonthly * 12;
                    else
                        spouseSocialSecurityIncome = ageData.Spouse.Age >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                }
            }
            var budgetItems = await _budgetService.GetBudgetItemsAsync(ClientLoginGuid);
            FVTotalModel inflationaryFvDetails = clientData.BudgetVersion ? await _budgetService.GetInflationaryBudgetFutureValue(ClientLoginGuid) : await _budgetService.GetInfaltionaryBudgetFutureValueExpressAsync(ClientLoginGuid);
            var inflationary = clientData.BudgetVersion ? inflationaryFvDetails.PresentValue : clientData.InflationaryBudget;
            var nonInflationary = clientData.BudgetVersion ? budgetItems.Where(m => !m.Inflationary && (m.YearsPayOff > ageData.YearToRetirement || !m.BudgetItem.IsRevolvingDebt))?.Sum(m => m.Amount) ?? 0 : clientData.NonInflationaryBudget;

            int clientInvestmentIncome = clientBankCreditUnionFvDetails.Income + clientBrokerageFvDetails.Income + clientAnnuityContractFvDetails.Income;
            int spouseInvestmentIncome = spouseBankCreditUnionFvDetails.Income + spouseBrokerageFvDetails.Income + spouseAnnuityContractFvDetails.Income;

            var result = new CashFlowModel
            {
                EarnedIncome = clientOccupationIncome + spouseOccupationIncome +
                               clientPrevRetirementPlanFvDetails.Income +
                               spousePrevRetirementPlanFvDetails.Income +
                               clientPensionIncomeAmount + spousePensionIncomeAmount,
                OtherIncome = clientOtherIncome + spouseOtherIncome +
                              clientInvestmentIncome + spouseInvestmentIncome +
                              clientSocialSecurityIncome + spouseSocialSecurityIncome,
                InflationaryExpenses = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), 1, inflationary),
                NonInflationaryExpenses = nonInflationary,
            };
            return View("Dashboard/CashFlow", result);
        }
        public async Task<IActionResult> RiskProfile()
        {
            ViewBag.DashboardCardTitle = "Risk Profile";
            ViewBag.DashboardCardContainer = "risk_profile";
            var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
               .Include(m => m.UserLogin).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = clientLogin.ClientData;
            var spouseData = clientLogin.SpouseData;
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            AgeModel ageData = await _commonService.GetAgeData(ClientLoginGuid);

            string token = await ClientLoginGuid.ToString().EncryptAsync();
            var clientRiskArcData = await _reportService.RiskArcReportData(token, ClientTypeEnum.Client, Convert.ToInt32(TimeZone));
            FVTotalModel clientRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel clientPrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            int clientPersonalActivityIncomeAtRetirement = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Where(m => m.Years > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;
            int clientPersonalAssetsAtRetirement = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;
            var clientPensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Client);
            int clientPensionIncomeAtRetirement = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;

            FVTotalModel spouseRetirementPlanFvDetails = new();
            FVTotalModel spousePrevRetirementPlanFvDetails = new();
            FVTotalModel clientBankCreditUnionFvDetails = new();
            FVTotalModel clientBrokerageFvDetails = new();
            FVTotalModel clientAnnuityContractFvDetails = new();
            FVTotalModel spouseBankCreditUnionFvDetails = new();
            FVTotalModel spouseBrokerageFvDetails = new();
            FVTotalModel spouseAnnuityContractFvDetails = new();

            var spouseRiskArcData = new RiskArcReportModel();
            int spouseOccupationIncomeAtRetirement = 0, spousePensionIncomeAtRetirement = 0,
               spousePersonalAssetsAtRetirement = 0, spousePersonalActivityIncomeAtRetirement = 0;
            if (isMarried)
            {
                spouseRiskArcData = await _reportService.RiskArcReportData(token, ClientTypeEnum.Spouse, Convert.ToInt32(TimeZone));
                spouseRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spouseOccupationIncomeAtRetirement = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.RetirementAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                spousePersonalAssetsAtRetirement = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalActivityIncomeAtRetirement = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Where(m => m.Years > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;
            }
            if (clientData.InvestmentVersion)
            {
                var accountTypes = new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse };
                var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, bankingAccountTypes);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, accountTypes);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                if (isMarried)
                {
                    spouseBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, bankingAccountTypes);
                    spouseBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, accountTypes);
                    spouseAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                }
            }
            else
            {
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractExpressFutureValueDetailsAsync(ClientLoginGuid);
            }
            var budgetItems = await _budgetService.GetBudgetItemsAsync(ClientLoginGuid);
            FVTotalModel inflationaryFvDetails = clientData.BudgetVersion ? await _budgetService.GetInflationaryBudgetFutureValue(ClientLoginGuid) : await _budgetService.GetInfaltionaryBudgetFutureValueExpressAsync(ClientLoginGuid);
            var nonInflationary = clientData.BudgetVersion ? budgetItems.Where(m => !m.Inflationary && (m.YearsPayOff > ageData.YearToRetirement || !m.BudgetItem.IsRevolvingDebt))?.Sum(m => m.Amount) ?? 0 : clientData.NonInflationaryBudget;
            int totalPv = clientRetirementPlanFvDetails.PresentValue + spouseRetirementPlanFvDetails.PresentValue +
                                  clientPrevRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue +
                                  clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue +
                                  clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue +
                                  clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue;
            var incomeNeededAtRetirement = inflationaryFvDetails.FutureValue + nonInflationary;
            int clientOccupationIncomeAtRetirement = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.RetirementAge)?.Sum(m => m.AnnualEarnings) ?? 0;
            int clientInvestmentIncome = clientBankCreditUnionFvDetails.Income + clientBrokerageFvDetails.Income + clientAnnuityContractFvDetails.Income;
            int spouseInvestmentIncome = spouseBankCreditUnionFvDetails.Income + spouseBrokerageFvDetails.Income + spouseAnnuityContractFvDetails.Income;
            int clientSocialSecurityIncomeAtRetirement = 0, spouseSocialSecurityIncomeAtRetirement = 0;
            int clientSocialSecurityAge = 0, spouseSocialSecurityAge = 0;
            var clientSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Client);

            if (clientSocialSecurity != null)
            {
                clientSocialSecurityAge = clientSocialSecurity.sSageBenifitsEnum;
                if (clientSocialSecurity.ssreceiving)
                    clientSocialSecurityIncomeAtRetirement = clientSocialSecurity.ssmonthly * 12;
                else
                    clientSocialSecurityIncomeAtRetirement = ageData.Client.RetirementAge >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
            }

            if (isMarried && spouseData != null)
            {
                var spouseSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                if (spouseSocialSecurity != null)
                {
                    spouseSocialSecurityAge = spouseSocialSecurity.sSageBenifitsEnum;
                    if (spouseSocialSecurity.ssreceiving)
                        spouseSocialSecurityIncomeAtRetirement = spouseSocialSecurity.ssmonthly * 12;
                    else
                        spouseSocialSecurityIncomeAtRetirement = ageData.Spouse.RetirementAge >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                }
            }
            var totalIncomeAtRetirement = clientOccupationIncomeAtRetirement + spouseOccupationIncomeAtRetirement +
                                          clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                          clientPersonalActivityIncomeAtRetirement + spousePersonalActivityIncomeAtRetirement +
                                          clientPersonalAssetsAtRetirement + spousePersonalAssetsAtRetirement +
                                          clientInvestmentIncome + spouseInvestmentIncome +
                                          clientSocialSecurityIncomeAtRetirement + spouseSocialSecurityIncomeAtRetirement +
                                          clientPensionIncomeAtRetirement + spousePensionIncomeAtRetirement;
            var investmentNeededAtRetirement = _reportService.GetInvestmentNeededAtRetirement(incomeNeededAtRetirement, totalIncomeAtRetirement);
            double Fis = _reportService.GetFIS(totalPv, investmentNeededAtRetirement);
            var result = new RiskProfileModel
            {
                FinancialIndependentScore = Fis,
                ClientRiskData = new RiskArcDataModel
                {
                    RiskCapacityScore = clientRiskArcData.RiskCapacity,
                    BehavioralInvestmentType = clientRiskArcData.BitData.Any() ? clientRiskArcData.BitData.Where(m => m.Active).Select(m => m.BitType.GetDisplayName()) : null,
                    RiskProfile = clientRiskArcData.RiskProfile
                },
                SpouseRiskData = isMarried ? new RiskArcDataModel
                {
                    RiskCapacityScore = spouseRiskArcData.RiskCapacity,
                    BehavioralInvestmentType = spouseRiskArcData.BitData.Any() ? spouseRiskArcData.BitData.Where(m => m.Active).Select(m => m.BitType.GetDisplayName()) : null,
                    RiskProfile = spouseRiskArcData.RiskProfile
                } : new RiskArcDataModel { },
                IsMarried = isMarried,
                ClientName = clientData.FirstName,
                SpouseName = isMarried ? spouseData.FirstName : string.Empty,
            };
            return View("Dashboard/RiskProfile", result);
        }
        public async Task<IActionResult> GoalTracker()
        {
            ViewBag.DashboardCardTitle = "Goal Tracker";
            ViewBag.DashboardCardContainer = "goal_tracker";
            AgeModel ageData = await _commonService.GetAgeData(ClientLoginGuid);
            var summaryData = await _reportService.GetClientReportSummaryAsync(ClientLoginGuid);
            var investmentNeededAtRetirement = _reportService.GetInvestmentNeededAtRetirement(summaryData.IncomeNeededAtRetirement, summaryData.IncomeNeededAtRetirement);
            double currentRoR = _reportService.GetCurrentRoR(summaryData.PresentValue, summaryData.ValueOfReturn);
            double requiredRoR = ExtensionMethod.CalculateRate(ageData.YearToRetirement, summaryData.PmtValue, summaryData.PresentValue, investmentNeededAtRetirement);
            var result = new GoalTrackerModel
            {
                CurrentAnnualAvgRateOfReturn = currentRoR,
                RequiredAnnualAvgRateOfReturn = requiredRoR,
                TotalInvestmentNeededAtRetirement = investmentNeededAtRetirement,
                ProjectedValueOfInvestmentAtRetirement = summaryData.FutureValue
            };
            return View("Dashboard/GoalTracker", result);
        }
        public IActionResult Estate()
        {
            ViewBag.DashboardCardTitle = "Your Estate";
            ViewBag.DashboardCardContainer = "estate";
            var Estate = _context2.ClientEstateItems.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            var result = new EstatePlanningModel
            {
                Will = Estate.Any() && (Estate.FirstOrDefault(m => m.DescriptionItemId == 57).isClientActive || Estate.FirstOrDefault(m => m.DescriptionItemId == 57).isSpouseActive),
                Trust = Estate.Any() && (Estate.FirstOrDefault(m => m.DescriptionItemId == 58 || m.DescriptionItemId == 59).isClientActive || Estate.FirstOrDefault(m => m.DescriptionItemId == 58 || m.DescriptionItemId == 59).isSpouseActive),
                PowerOfAttorney = Estate.Any() && (Estate.FirstOrDefault(m => m.DescriptionItemId == 61 || m.DescriptionItemId == 62).isClientActive || Estate.FirstOrDefault(m => m.DescriptionItemId == 61 || m.DescriptionItemId == 62).isSpouseActive)
            };
            return View("Dashboard/Estate", result);
        }
        public IActionResult TaxPrepStatus()
        {
            ViewBag.DashboardCardTitle = "Your Tax Prep Status";
            ViewBag.DashboardCardContainer = "tax_prep_status";
            var taxPrepStatuses = _context2.TaxPrepStatuses.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            List<TaxPrepStatusModel> statusesYearWise = new();
            for (int i = 1; i <= 3; i++)
            {
                int year = Convert.ToInt32(DateTime.UtcNow.AddYears(-i).ToString("yyyy"));
                var status = taxPrepStatuses.FirstOrDefault(m => m.Year == year)?.TaxPrepStatusString;
                statusesYearWise.Add(new TaxPrepStatusModel
                {
                    Year = year,
                    Status = status
                });
            }
            return View("Dashboard/TaxPrepStatus", statusesYearWise);
        }
        public async Task<IActionResult> TaxPrepQueueActivityLog()
        {
            ViewBag.DashboardCardTitle = "Tax Prep Queue Activity Log";
            ViewBag.DashboardCardContainer = "tax_prep_queue_activity_log";
            var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
            ViewBag.TimeZone = Convert.ToInt32(TimeZone);
            var teaxPrepHistory = await _context2.TaxPrepHistories.Where(m => m.ClientLogin.Guid == ClientLoginGuid)
                .Include(m => m.ClientLogin).Include(m => m.ClientLogin.ClientData).Include(m => m.ActionByUser).Include(m => m.ActionByUser.UserData).OrderByDescending(m => m.CreatedDate).ToListAsync();
            return View("Dashboard/TaxPrepQueueActivityLog", teaxPrepHistory);
        }
        [AllowAnonymous]
        public IActionResult CustomHtml()
        {
            return View("custom-html");
        }
        [AllowAnonymous]
        public IActionResult Demo()
        {
            return View();
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _fileHelper.Dispose();
                    _commonService.Dispose();
                    _budgetService.Dispose();
                    _employmentService.Dispose();
                    _investmentService.Dispose();
                    _otherIncomeService.Dispose();
                    _socialSecurityService.Dispose();
                    _reportService.Dispose();
                }
                disposed = true;
            }
        }
        ~HomeController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}