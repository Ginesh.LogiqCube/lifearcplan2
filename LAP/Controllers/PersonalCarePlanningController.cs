﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using LAP.Filters;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class PersonalCarePlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public PersonalCarePlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 6;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "personal-care-planning-intro" })]
        public async Task<IActionResult> PersonalCarePlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("personal-care-planning-intro", ClientLoginGuid);
            return View();
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "personal-care-planning-plan" })]
        public async Task<IActionResult> PersonalCarePlanningPlan()
        {
            var personalCares = _context2.PersonalCares.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            if (client.ClientData.MaritalStatus != MaritalStatusEnum.Married)
                personalCares = personalCares.Where(m => m.ClientType != ClientTypeEnum.Spouse).ToList();
            string clientName = client.ClientData.FirstName;
            string spouseName = Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate() ? client.SpouseData.FirstName : null;
            if (!personalCares.Any(m => m.ClientType == ClientTypeEnum.Client))
                personalCares.Add(new PersonalCare
                {
                    ClientType = ClientTypeEnum.Client,
                });
            if (!personalCares.Any(m => m.ClientType == ClientTypeEnum.Spouse) && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate())
                personalCares.Add(new PersonalCare
                {
                    ClientType = ClientTypeEnum.Spouse
                });
            personalCares = personalCares.Select(m => { m.Name = m.ClientType == ClientTypeEnum.Client ? clientName : spouseName; return m; }).ToList();
            var careGivers = await _dropdownDataHelper.GetCareGiverAsync();

            ViewBag.clientCareGivers = client.ClientData.MaritalStatus == MaritalStatusEnum.Unmarried ? careGivers : careGivers.Append(new SelectListItem
            {
                Text = $"{spouseName}",
                Value = "-3",
                Group = new SelectListGroup() { Name = !string.IsNullOrEmpty(spouseName) ? ClientTypeEnum.Spouse.ToString() : "" },
            }).OrderBy(m => Convert.ToInt32(m.Value));
            ViewBag.spouseCareGivers = careGivers.Append(new SelectListItem
            {
                Text = $"{clientName}",
                Value = "-2",
                Group = new SelectListGroup() { Name = !string.IsNullOrEmpty(clientName) ? ClientTypeEnum.Client.ToString() : "" },
            }).OrderBy(m => Convert.ToInt32(m.Value));
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("personal-care-planning-plan", ClientLoginGuid);
            return View(personalCares);
        }
        public async Task<IActionResult> SavePersonalCare(List<PersonalCare> personalCares)
        {
            var personalData = _context2.PersonalCares.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (personalCares is not null)
            {
                int i = 0;
                foreach (var item in personalCares)
                {
                    item.CaregiverName = !string.IsNullOrEmpty(item.CaregiverName) ? item.CaregiverName : string.Empty;
                    if (item.Guid != Guid.Empty)
                    {
                        var PersonalCareObject = await personalData.Where(m => m.Guid == item.Guid).FirstOrDefaultAsync();
                        var PersonalCareObjectDataObj = _mapper.Map(item, PersonalCareObject);
                        _context2.PersonalCares.Update(PersonalCareObjectDataObj);
                    }
                    else
                    {
                        item.ClientId = ClientId;
                        _context2.PersonalCares.Update(item);
                    }
                    i++;
                }
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = personalCares is not null,
                Message = personalCares is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~PersonalCarePlanningController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}