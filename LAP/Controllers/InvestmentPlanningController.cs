﻿using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAP.Filters;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class InvestmentPlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly InvestmentService _investmentService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public InvestmentPlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 1;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "investment-planning-intro" })]
        public async Task<IActionResult> InvestmentPlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("investment-planning-intro", ClientLoginGuid);
            return View("Intro");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "bank-credit-union-accounts" })]
        public async Task<IActionResult> BankCreditUnionAccounts()
        {
            var bankcreditunionaccounts = _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            bankcreditunionaccounts = bankcreditunionaccounts.Select(m =>
            {
                var OwnByWithData = ClientType.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("bank-credit-union-accounts", ClientLoginGuid);
            return View("BankCreditUnionAccount/BankCreditUnionAccounts", bankcreditunionaccounts);
        }
        public async Task<IActionResult> BankCreditUnionAccountInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.BankCreditUnionAccounts).Include(m => m.ClientData).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(client.ClientData.MaritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var backcreditInfo = client.BankCreditUnionAccounts.FirstOrDefault(m => m.Guid == Guid);
            if (backcreditInfo is not null)
            {
                var OwnByWithData = ClientType.Where(m => m.Value == backcreditInfo.Owner).Select(m => m.Text);
                backcreditInfo.OwnByWithData = string.Join(" & ", OwnByWithData);
            }
            return View("BankCreditUnionAccount/BankCreditUnionAccountInfoModal", backcreditInfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveBankCreditUnionAccount(BankCreditUnionAccount bankCreditUnionAccount)
        {
            var bankCreditUnionAccountdata = await _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == bankCreditUnionAccount.Guid).FirstOrDefaultAsync();
            var bankCreditUnionAccountObj = _mapper.Map(bankCreditUnionAccount, bankCreditUnionAccountdata);
            bankCreditUnionAccountObj.ClientId = ClientId;
            _context2.BankCreditUnionAccounts.Update(bankCreditUnionAccountObj);
            await _context2.SaveChangesAsync();

            var maritalStatus = (await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync()).MaritalStatus;
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(maritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var OwnByWithData = ClientType.Where(m => m.Value == bankCreditUnionAccountObj.Owner).Select(m => m.Text);
            bankCreditUnionAccountObj.OwnByWithData = string.Join(" & ", OwnByWithData);
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = bankCreditUnionAccountObj is not null,
                Message = bankCreditUnionAccountObj is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { bankCreditUnionAccountInfo = bankCreditUnionAccountObj, hasInvestmentIncome }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteBankCreditUnionAccount(Guid Guid)
        {
            var bankCreditUnionAccount = await _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (bankCreditUnionAccount is not null)
            {
                _context2.BankCreditUnionAccounts.Remove(bankCreditUnionAccount);
                await _context2.SaveChangesAsync();
            }
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = bankCreditUnionAccount is not null,
                Message = bankCreditUnionAccount is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { hasInvestmentIncome }
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "brokerage-advisory-accounts" })]
        public async Task<IActionResult> BrokerageAdvisoryAccounts()
        {
            var brokerageadvisoryaccounts = _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            brokerageadvisoryaccounts = brokerageadvisoryaccounts.Select(m =>
            {
                var OwnByWithData = ClientType.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("brokerage-advisory-accounts", ClientLoginGuid);
            return View("BrokerageAdvisoryAccount/BrokerageAdvisoryAccounts", brokerageadvisoryaccounts);
        }
        public async Task<IActionResult> BrokerageAdvisoryAccountInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.BrokerageAdvisoryAccounts).Include(m => m.ClientData).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(client.ClientData.MaritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var brokerageadvisoryaccountInfo = client.BrokerageAdvisoryAccounts.FirstOrDefault(m => m.Guid == Guid);
            if (brokerageadvisoryaccountInfo is not null)
            {
                var OwnByWithData = ClientType.Where(m => m.Value == brokerageadvisoryaccountInfo.Owner).Select(m => m.Text);
                brokerageadvisoryaccountInfo.OwnByWithData = string.Join(" & ", OwnByWithData);
            }
            return View("BrokerageAdvisoryAccount/BrokerageAdvisoryAccountInfoModal", brokerageadvisoryaccountInfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveBrokerageAdvisoryAccount(BrokerageAdvisoryAccount brokerageAdvisoryAccount)
        {
            var brokerageadvisoryaccountdata = await _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == brokerageAdvisoryAccount.Guid).FirstOrDefaultAsync();
            var brokerageadvisoryObj = _mapper.Map(brokerageAdvisoryAccount, brokerageadvisoryaccountdata);
            brokerageadvisoryObj.ClientId = ClientId;
            _context2.BrokerageAdvisoryAccounts.Update(brokerageadvisoryObj);
            await _context2.SaveChangesAsync();

            var maritalStatus = (await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync()).MaritalStatus;
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(maritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var OwnByWithData = ClientType.Where(m => m.Value == brokerageadvisoryObj.Owner).Select(m => m.Text);
            brokerageadvisoryObj.OwnByWithData = string.Join(" & ", OwnByWithData);
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = brokerageadvisoryObj is not null,
                Message = brokerageadvisoryObj is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { BrokerageAdvisoryAccountInfo = brokerageadvisoryObj, hasInvestmentIncome }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteBrokerageAdvisoryAccount(Guid Guid)
        {
            var brokerageAdvisoryAccount = await _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (brokerageAdvisoryAccount is not null)
            {
                _context2.BrokerageAdvisoryAccounts.Remove(brokerageAdvisoryAccount);
                await _context2.SaveChangesAsync();
            }
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = brokerageAdvisoryAccount is not null,
                Message = brokerageAdvisoryAccount is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { hasInvestmentIncome }
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "life-annuity-contracts" })]
        public async Task<IActionResult> LifeAnnuityContracts()
        {
            var lifeannuitycontracts = _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(clientData.MaritalStatus.ToString());
            lifeannuitycontracts = lifeannuitycontracts.Select(m =>
            {
                var OwnByWithData = ClientType.Where(n => n.Value == m.Owner).Select(m => m.Text);
                m.OwnByWithData = string.Join(" & ", OwnByWithData);
                return m;
            }).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("life-annuity-contracts", ClientLoginGuid);
            return View("LifeAnnuityContract/LifeAnnuityContracts", lifeannuitycontracts);
        }
        public async Task<IActionResult> LifeAnnuityContractInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.LifeAnnuityContracts).Include(m => m.ClientData).FirstOrDefaultAsync();
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(client.ClientData.MaritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var lifeannuitycontractinfo = client.LifeAnnuityContracts.FirstOrDefault(m => m.Guid == Guid);
            if (lifeannuitycontractinfo is not null)
            {
                ViewBag.ContractType = await DropdownDataHelper.GetLifeAnnuityContractTypeAsync(lifeannuitycontractinfo.ContractType.ToString());
                var OwnByWithData = ClientType.Where(m => m.Value == lifeannuitycontractinfo.Owner).Select(m => m.Text);
                lifeannuitycontractinfo.OwnByWithData = string.Join(" & ", OwnByWithData);
            }
            return View("LifeAnnuityContract/LifeAnnuityContractInfoModal", lifeannuitycontractinfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveLifeAnnuityContract(LifeAnnuityContract lifeAnnuityContract)
        {
            var lifeannuitycontractdata = await _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == lifeAnnuityContract.Guid).FirstOrDefaultAsync();
            var lifeAnnuityContractObj = _mapper.Map(lifeAnnuityContract, lifeannuitycontractdata);
            lifeAnnuityContractObj.ClientId = ClientId;
            _context2.LifeAnnuityContracts.Update(lifeAnnuityContractObj);
            await _context2.SaveChangesAsync();

            var maritalStatus = (await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync()).MaritalStatus;
            var ClientType = await _dropdownDataHelper.GetOwnershipAsync(maritalStatus.ToString());
            ViewBag.ClientType = ClientType;
            var OwnByWithData = ClientType.Where(m => m.Value == lifeAnnuityContractObj.Owner).Select(m => m.Text);
            lifeAnnuityContractObj.OwnByWithData = string.Join(" & ", OwnByWithData);
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = lifeAnnuityContractObj is not null,
                Message = lifeAnnuityContractObj is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { LifeAnnuityContractInfo = lifeAnnuityContractObj, hasInvestmentIncome }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteLifeAnnuityContract(Guid Guid)
        {
            var lifeAnnuityContract = await _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (lifeAnnuityContract is not null)
            {
                _context2.LifeAnnuityContracts.Remove(lifeAnnuityContract);
                await _context2.SaveChangesAsync();
            }
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            var result = new CommonResponse
            {
                Success = lifeAnnuityContract is not null,
                Message = lifeAnnuityContract is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { hasInvestmentIncome }
            };
            return Json(result);
        }
        public async Task<IEnumerable<SelectListItem>> GetLifeAnnuityContractType(string contractType)
        {
            return await DropdownDataHelper.GetLifeAnnuityContractTypeAsync(contractType);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "investment-planning-express" })]
        public async Task<IActionResult> InvestmentPlanningExpress()
        {
            var clientData = await _context2.InvestmentLiteDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var investmentLiteInfo = _mapper.Map<InvestmentLiteInfo>(clientData);
            return View("InvestmentPlanningLite", investmentLiteInfo);
        }
        [HttpPost]
        public async Task<IActionResult> SaveInvestmentLite(InvestmentLiteInfo investmentLiteInfo)
        {
            var InvestmentData = await _context2.InvestmentLiteDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var InvestmentDataObj = _mapper.Map(investmentLiteInfo, InvestmentData);
            InvestmentDataObj.ClientId = ClientId;
            _context2.InvestmentLiteDatas.Update(InvestmentDataObj);
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = investmentLiteInfo is not null,
                Message = investmentLiteInfo is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~InvestmentPlanningController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}