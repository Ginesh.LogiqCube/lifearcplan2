﻿using AutoMapper;
using LAP.Helpers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using Google.Authenticator;
using System.Net.Mail;
using Microsoft.Extensions.Logging;

namespace LAP.Controllers
{
    public class AccountController : BaseController, IDisposable
    {
        private bool disposed;
        private readonly TwoFactorAuthenticator twoFactorAuthenticator;
        private readonly ILogger<AccountController> _logger;
        public AccountController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper, ILogger<AccountController> logger) : base(context2, env, config, httpContextAccessor, mapper)
        {
            twoFactorAuthenticator = new TwoFactorAuthenticator();
            _logger = logger;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel login, string returnUrl)
        {
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            login.Password = await login.Password.EncryptAsync();
            var client = await _context2.ClientLogins.Where(m => m.Username == login.Username && m.Password == login.Password && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            string url = string.Empty;
            if (client is not null)
            {
                if (!client.TwoFactorAuth || _httpContextAccessor.HttpContext.IsLocalOrSupport())
                {
                    if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                    {
                        client.SetupCompletedDate = Convert.ToDateTime(client.SetupCompletedDate).IsNotEmptyDate() ? client.SetupCompletedDate : client.LastLogin;
                        client.LastLogin = DateTime.UtcNow;
                        await _context2.SaveChangesAsync();

                        bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();

                        string clientName = client.ClientData.FirstName;
                        string version = client.ClientData.RiskArcVersion ? "Indepth" : "Express";
                        string reportListHtml = $@"<li>Personal Canvas</li>
                                           <li>{clientName}'s RiskArc {version} Survey</li>
                                           <li>{clientName}'s RiskArc {version} Report</li>";
                        if (IsMarried)
                        {
                            string spouseName = client.SpouseData.FirstName;
                            reportListHtml += $@"<li>{spouseName}'s RiskArc {version} Survey</li>
                                             <li>{spouseName}'s RiskArc {version} Report</li>";
                        }
                        reportListHtml += $@"<li>Arc of Life Report</li>";
                        reportListHtml = $"<ul>{reportListHtml}</ul>";
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", client.UserLogin.UserData.FirstName);
                        string href = _httpContextAccessor.HttpContext.GetAbsoluteUrl("user/client-arc-hive");
                        string clientSpouseName = Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate() ? $"{client.ClientData.Name} and {client.SpouseData.Name}" : client.ClientData.Name;
                        string emailText = $@"The LifeArcPlan team is notifying you that your client(s), {clientSpouseName} logged in again for a subsequent visit to their LifeArcPlan Survey.<br/><br/>
                                          Each time a client logs into LifeArcPlan, the system date stamps and archives all saved data fields, generating one or more of the following new report(s) for your review:{reportListHtml}";
                        emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        List<LinkedResource> resources = new()
                        {
                            new LinkedResource(Path.Combine(_env.WebRootPath, "images/brand_logo.png"))
                            {
                                ContentId = "LogoImage"
                            }
                        };
                        await emailHelper.SendEmailAsync(client.UserLogin.Username, null, _config["Email:BccEmail"], EmailSubject.LoggedInClient, emailHtmlBody, _env.WebRootPath);
                    }
                    var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Email, client.Username),
                        new Claim(ClaimTypes.Role,Roles.Client),
                        new Claim("ClientId", client.Id.ToString()),
                        new Claim("ClientLoginGuid", client.Guid.ToString()),
                        new Claim("FirmId", client.UserLogin.FirmId.ToString()),
                        new Claim("TimeZone", login.TimeZone.ToString()),
                    };
                    var claimsIdentity = new ClaimsIdentity(claims, AuthenticationSchemes.ClientAuth);
                    var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                    var prop = new AuthenticationProperties();
                    if (login.RememberMe)
                    {
                        prop.IsPersistent = login.RememberMe;
                        prop.ExpiresUtc = DateTime.UtcNow.AddMonths(1);
                    }
                    await _httpContextAccessor.HttpContext.SignInAsync(claimsPrincipal, prop);
                    url = !string.IsNullOrEmpty(returnUrl) ? returnUrl : Url.RouteUrl("welcome");
                }
            }
            login.Password = string.Empty;
            var result = new CommonResponse
            {
                Success = client is not null,
                Message = client is not null ? Message.LOGIN_SUCCESS : Message.INVALID_USERNAME_PASSWORD,
                RedirectUrl = client is not null && (!client.TwoFactorAuth || _httpContextAccessor.HttpContext.IsLocalOrSupport()) ? url : null,
                Data = client is not null ? new { login, firstname = client.ClientData.FirstName } : null
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> Send2FACode(string email)
        {
            var client = await _context2.ClientLogins.Where(m => m.Username == email && m.IsDeleted != true)
                .Include(m => m.ClientData).FirstOrDefaultAsync();
            var accountSecretKey = $"{_config["SecretKey"]}-{client.Username}";
            var code = twoFactorAuthenticator.GetCurrentPIN(accountSecretKey);
            StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
            mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
            StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
            emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
            var emailText = @$"Please find the verification code mentioned below as requested by you.<br>
                                   Verification Code: <b style='color:#800000'>{code}</b><br><br>
                                   <b>Note:</b> Verification code will be valid for 5 minutes only";
            emailTemplateHtml.Replace("@EMAILTEXT", emailText);
            mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
            EmailHelper emailHelper = new(_config);
            string emailHtmlBody = mainEmailTemplateHtml.ToString();
            await emailHelper.SendEmailAsync(client.Username, null, _config["Email:BccEmail"], EmailSubject.TwoFactorAuthCodeRequest, emailHtmlBody, _env.WebRootPath);

            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(code),
                Message = !string.IsNullOrEmpty(code) ? string.Format(Message.VERIFICATION_CODE_SENT, client.Username) : Message.INVALID_VERIFICATION_CODE,
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> CodeVerification(LoginModel model, string returnUrl, int EmailSent)
        {
            var client = await _context2.ClientLogins.Where(m => m.Username == model.Username && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            string url = string.Empty;
            bool isAuthenticated = false;
            if (client is not null)
            {
                var accountSecretKey = $"{_config["SecretKey"]}-{client.Username}";
                var CodeValidity = EmailSent == 1 ? TimeSpan.FromMinutes(5) : TimeSpan.FromSeconds(30);
                isAuthenticated = twoFactorAuthenticator.ValidateTwoFactorPIN(accountSecretKey, model.VerificationCode, CodeValidity);
                if (isAuthenticated)
                {
                    if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                    {
                        client.SetupCompletedDate = Convert.ToDateTime(client.SetupCompletedDate).IsNotEmptyDate() ? client.SetupCompletedDate : client.LastLogin;
                        client.LastLogin = DateTime.UtcNow;
                        await _context2.SaveChangesAsync();
                        bool IsMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                        string clientName = client.ClientData.FirstName;
                        string version = client.ClientData.RiskArcVersion ? "Indepth" : "Express";
                        string reportListHtml = $@"<li>Personal Canvas</li>
                                           <li>{clientName}'s RiskArc {version} Survey</li>
                                           <li>{clientName}'s RiskArc {version} Report</li>";
                        if (IsMarried)
                        {
                            string spouseName = client.SpouseData.FirstName;
                            reportListHtml += $@"<li>{spouseName}'s RiskArc {version} Survey</li>
                                             <li>{spouseName}'s RiskArc {version} Report</li>";
                        }
                        reportListHtml += $@"<li>Arc of Life Report</li>";
                        reportListHtml = $"<ul>{reportListHtml}</ul>";
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", client.UserLogin.UserData.FirstName);
                        string href = _httpContextAccessor.HttpContext.GetAbsoluteUrl("user/client-arc-hive");
                        string clientSpouseName = Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate() ? $"{client.ClientData.Name} and {client.SpouseData.Name}" : client.ClientData.Name;
                        string emailText = $@"The LifeArcPlan team is notifying you that your client(s), {clientSpouseName} logged in again for a subsequent visit to their LifeArcPlan Survey.<br/><br/>
                                          Each time a client logs into LifeArcPlan, the system date stamps and archives all saved data fields, generating one or more of the following new report(s) for your review:{reportListHtml}";
                        emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        List<LinkedResource> resources = new()
                        {
                            new LinkedResource(Path.Combine(_env.WebRootPath, "images/brand_logo.png"))
                            {
                                ContentId = "LogoImage"
                            }
                        };
                        await emailHelper.SendEmailAsync(client.UserLogin.Username, null, _config["Email:BccEmail"], EmailSubject.LoggedInClient, emailHtmlBody, _env.WebRootPath);
                    }
                    var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Email, client.Username),
                        new Claim(ClaimTypes.Role,Roles.Client),
                        new Claim("ClientId", client.Id.ToString()),
                        new Claim("ClientLoginGuid", client.Guid.ToString()),
                        new Claim("FirmId", client.UserLogin.FirmId.ToString()),
                        new Claim("TimeZone", model.TimeZone.ToString()),
                    };
                    var claimsIdentity = new ClaimsIdentity(claims, AuthenticationSchemes.ClientAuth);
                    var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                    var prop = new AuthenticationProperties();
                    if (model.RememberMe)
                    {
                        prop.IsPersistent = model.RememberMe;
                        prop.ExpiresUtc = DateTime.UtcNow.AddMonths(1);
                    }
                    await _httpContextAccessor.HttpContext.SignInAsync(claimsPrincipal, prop);
                    url = !string.IsNullOrEmpty(returnUrl) ? returnUrl : Url.RouteUrl("welcome");
                }
            }
            var result = new CommonResponse
            {
                Success = client is not null && isAuthenticated,
                Message = client is not null && isAuthenticated ? Message.LOGIN_SUCCESS : Message.INVALID_VERIFICATION_CODE,
                RedirectUrl = client is not null && isAuthenticated ? url : null,
            };
            return Json(result);
        }
        public async Task<IActionResult> LogOut()
        {
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            return RedirectToRoute("client/login");
        }
        [HttpPost]
        public async Task<IActionResult> SendResetPasswordLink(ForgotPasswordModel model)
        {
            var client = await _context2.ClientLogins.Where(m => m.Username == model.Email && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            string resetPasswordLink = string.Empty;
            if (client is not null)
            {
                string name = $"{client.ClientData.FirstName} {client.ClientData.LastName}";
                string token = await ($"{client.Username}#{DateTime.UtcNow.AddHours(1)}").EncryptAsync();
                resetPasswordLink = Url.RouteUrl("reset-password", new { token }, Request.Scheme);
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());

                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/reset-password.html")));
                emailTemplateHtml.Replace("@RESETPASSWORDLINK", resetPasswordLink);
                emailTemplateHtml.Replace("@NAME", name);

                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                List<LinkedResource> resources = new()
                {
                    new LinkedResource(Path.Combine(_env.WebRootPath, "images/brand_logo.png"))
                    {
                        ContentId = "LogoImage"
                    }
                };
                await emailHelper.SendEmailAsync(client.Username, null, _config["Email:BccEmail"], EmailSubject.ResetPasswordRequest, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = client is not null,
                Message = client is not null ? Message.RESET_PASSWORDLINK_SENT : string.Format(ValidationMessage.INVALID, "Email address"),
            };
            return Json(result);
        }
        public async Task<IActionResult> ResetPassword(string token)
        {
            ResetPasswordModel model = new();
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
            {
                ViewBag.StatusCode = 403;
                ViewBag.Title = "Access Denied";
                ViewBag.Message = "Sorry, You don't have permission to access this page.";
                return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
            }
            var sp = token.Split("#");
            model.Email = sp[0];
            model.Token = token;
            if (sp.Length > 1 && Convert.ToDateTime(sp[1]) < DateTime.UtcNow)
                ViewData["msg"] = Message.LINK_EXPIRED;
            return View("ResetPassword", model);
        }
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            string token = await model.Token.DecryptAsync();
            var sp = token.Split("#");
            model.Email = sp[0];
            var client = await _context2.ClientLogins.Where(m => m.Username == model.Email && m.IsDeleted != true).Include(m => m.ClientData).Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            string redirectUrl = string.Empty;
            if (client is not null)
            {
                client.Password = await model.Password.EncryptAsync();
                client.SetupCompletedDate = DateTime.UtcNow;
                client.LastLogin = DateTime.UtcNow;
                await _context2.SaveChangesAsync();
                var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Email, client.Username),
                        new Claim(ClaimTypes.Role,Roles.Client),
                        new Claim("ClientId", client.Id.ToString()),
                        new Claim("ClientLoginGuid", client.Guid.ToString()),
                        new Claim("FirmId", client.UserLogin.FirmId.ToString()),
                        new Claim("TimeZone", model.TimeZone.ToString()),
                    };

                var claimsIdentity = new ClaimsIdentity(claims, AuthenticationSchemes.ClientAuth);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                var prop = new AuthenticationProperties();
                await _httpContextAccessor.HttpContext.SignInAsync(claimsPrincipal, prop);
                redirectUrl = !client.TwoFactorAuth ? $"settings?back=true" : Url.RouteUrl("welcome");
            }
            var msg = sp.Length > 1 ? Message.RESET_PASSWORD_SUCCESSFUL : Message.CREATE_PASSWORD_SUCCESSFUL;
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
                var action = sp.Length > 1 ? "reset" : "setup";
                var subject = sp.Length > 1 ? EmailSubject.ResetPasswordAcknowledgement : EmailSubject.CreatePassword;
                var emailText = $@"<p>Your password {action} request has been processed.</p>
                                   <p>In the event you did not request to have your password reset, please notify LifeArcPlan's help desk staff immediately at <a href='mailto:info@lifearcplan.com' style='color:#0073e9'>info@lifearcplan.com</a>.</p>";
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                string OwnerEmail = client.UserLogin.IsAssistant ? client.UserLogin.OwnerUser.Username : client.UserLogin.Username;
                await emailHelper.SendEmailAsync(client.Username, OwnerEmail, _config["Email:BccEmail"], subject, emailHtmlBody, _env.WebRootPath);

                StringBuilder mainEmailTemplateHtml2 = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml2.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml2 = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                emailTemplateHtml2.Replace("@NAME", client.UserLogin.UserData.FirstName);
                emailTemplateHtml2.Replace("@EMAILTEXT", string.Format(EmailNotification.CLIENTLOGGEDIN, client.ClientData.Name));
                mainEmailTemplateHtml2.Replace("@CONTENT", emailTemplateHtml2.ToString());
                string emailHtmlBody2 = mainEmailTemplateHtml2.ToString();
                await emailHelper.SendEmailAsync(OwnerEmail, null, _config["Email:BccEmail"], EmailSubject.ClientSurveyLogin, emailHtmlBody2, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = client is not null,
                Message = client is not null ? msg : string.Format(ValidationMessage.INVALID, "Email address"),
                RedirectUrl = redirectUrl
            };
            return Json(result);
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~AccountController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}