﻿using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAP.Filters;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class CharitablePlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        protected readonly int ClientId;
        private bool disposed;
        public CharitablePlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 8;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "charitable-planning-intro" })]
        public async Task<IActionResult> CharitablePlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("charitable-planning-intro", ClientLoginGuid);
            return View();
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "charitable-planning-ambitions" })]
        public async Task<IActionResult> CharitablePlanningAmbitions()
        {
            CharitableModel charitable = new();
            var clientLoginData = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.CharitablePlanning).FirstOrDefaultAsync();
            var charitabledata = _context2.CharitablePlannings.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            IEnumerable<SelectListItem> result = EnumHelper.GetEnumSelectList<CharitableDonationEnum>();
            if (charitabledata is not null)
            {
                charitable.CharitableEstateEnum = charitabledata.charitableEstateEnum;
                if (!string.IsNullOrEmpty(charitabledata.charitableDonations))
                    charitable.CharitableDonations = charitabledata.charitableDonations.Split(',').Select(x => Convert.ToInt32(x)).ToList();
                charitable.CharitableGiftEnum = charitabledata.charitableGiftEnum;
                ViewBag.charitableItems = charitable.CharitableDonations;
            }
            charitable.CharitablerList = result.Select(x => new SelectListItem()
            {
                Text = x.Text,
                Value = x.Value,
                Selected = charitable.CharitableDonations is not null && charitable.CharitableDonations.Any(z => z == Convert.ToInt32(x.Value)) || x.Selected
            }).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("charitable-planning-ambitions", ClientLoginGuid);
            return View("CharitablePlanningAmbitions", charitable);
        }
        public async Task<IActionResult> SaveCharitableAmbitions(CharitableModel charitablePlanning)
        {
            var charitabledata = await _context2.CharitablePlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var charitableValues = charitablePlanning.CharitablerList.Where(x => x.Selected == true).Select(x => x.Value).ToList();
            charitabledata ??= new CharitablePlanning();
            if (charitabledata is not null)
            {
                var donation = string.Join(',', charitableValues);
                charitabledata.charitableDonations = donation;
                charitabledata.charitableEstateEnum = charitablePlanning.CharitableEstateEnum;
                charitabledata.charitableGiftEnum = charitablePlanning.CharitableGiftEnum;
                charitabledata.ClientId = ClientId;
                _context2.CharitablePlannings.Update(charitabledata);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = charitabledata is not null,
                Message = charitabledata is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~CharitablePlanningController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
