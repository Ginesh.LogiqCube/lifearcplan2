﻿using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;
using LAP.Filters;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class EducationPlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        private bool disposed;
        public EducationPlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 7;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "education-planning-intro" })]
        public async Task<IActionResult> EducationPlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("education-planning-intro", ClientLoginGuid);
            return View();
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "education-planning-assessment" })]
        public async Task<IActionResult> EducationPlanningAssessment()
        {
            var educationplanning = _context2.EducationPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("education-planning-assessment", ClientLoginGuid);
            return View("EducationPlanningAssessment", educationplanning);
        }
        public async Task<IActionResult> EducationPlanningInfo(Guid? Guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.EducationPlannings).Include(m => m.ClientData).FirstOrDefaultAsync();
            var educationtInfo = client.EducationPlannings.FirstOrDefault(m => m.Guid == Guid);
            return View("EducationPlanningInfoModal", educationtInfo);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteEducationPlanning(Guid Guid)
        {
            var EducationInfo = _context2.EducationPlannings.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid);
            if (EducationInfo is not null)
            {
                _context2.EducationPlannings.Remove(EducationInfo);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = EducationInfo is not null,
                Message = EducationInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEducationPlanning(EducationPlanning educationPlanning)
        {
            var educationPlanningdata = await _context2.EducationPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == educationPlanning.Guid).FirstOrDefaultAsync();
            var EducationPlanningObj = _mapper.Map(educationPlanning, educationPlanningdata);
            EducationPlanningObj.ClientId = ClientId;
            _context2.EducationPlannings.Update(EducationPlanningObj);
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = EducationPlanningObj is not null,
                Message = EducationPlanningObj is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { EducationPlanningInfo = EducationPlanningObj }
            };
            return Json(result);
        }
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~EducationPlanningController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}