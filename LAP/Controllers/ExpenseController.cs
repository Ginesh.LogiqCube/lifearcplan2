﻿using AutoMapper;
using LAP.Helpers;
using LAP.Filters;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAP.Services;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;

namespace LAP.Controllers
{
    public class ExpenseController : BaseController
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly BudgetService _budgetService;
        protected readonly InvestmentService _investmentService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public ExpenseController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            _budgetService = new BudgetService(_context2, _httpContextAccessor);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 3;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        public async Task<IEnumerable<SelectListItem>> GetBudgetCategories(int? parentId)
        {
            return await _dropdownDataHelper.GetBudgetCategoriesAsync(parentId);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "budget-info" })]
        public async Task<IActionResult> BudgetInfo()
        {
            var clientBudget = await _budgetService.GetBudgetItemsAsync(ClientLoginGuid);
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            ViewBag.AssumedInflationRate = clientData.AssumedInflationRate;
            var planningItems = await _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid)
                .Include(m => m.PlanningItem).OrderBy(m => m.PlanningItem.SortOrder).FirstOrDefaultAsync();
            var clientPlanningItems = await _commonService.GetPlanningItemsAsync(ClientLoginGuid);
            bool hasInvestmentPlanning = clientPlanningItems.Any(m => m.Active && m.PlanningItem.ItemName.Contains("Investment"));
            var arrow = "<i class='flaticon2-right-arrow icon-nm'></i>";
            string nextTitle = "Planning";
            string nextAction = "investmentplanning/investment-planning-intro";
            if (planningItems != null)
            {
                nextTitle = $"{planningItems.PlanningItem.ItemName.Replace("Planning", "")} {arrow} Intro";
                nextAction = $"{planningItems.PlanningItem.ItemName.Replace(" ", "").ToLower()}/{planningItems.PlanningItem.ItemName.Replace(" ", "-").ToLower().Replace("-care", "care")}-intro";
            }
            bool isExpress = nextAction.Contains("investment") && !clientData.InvestmentVersion;
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = hasInvestmentIncome ? "Investment Income" : "Social Security Estimate",
                PrevAction = hasInvestmentIncome ? "income/investment-income" : "income/social-security-estimate",
                NextTitle = !isExpress ? nextTitle : hasInvestmentPlanning ? "Investment Planning Express" : "LifeStyle Intro",
                NextAction = !isExpress ? nextAction : hasInvestmentPlanning ? "investmentplanning/investment-planning-intro" : "lifestyle/life-style-intro"
            };
            return View("BudgetInfo/_BudgetInfo", clientBudget);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "budget-info-express" })]
        public async Task<IActionResult> BudgetInfoExpress()
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            var budgetLiteInfo = _mapper.Map<BudgetLiteInfo>(clientData);
            var planningItems = await _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid)
                .Include(m => m.PlanningItem).OrderBy(m => m.PlanningItem.SortOrder).FirstOrDefaultAsync();
            var clientPlanningItems = await _commonService.GetPlanningItemsAsync(ClientLoginGuid);
            bool hasInvestmentPlanning = clientPlanningItems.Any(m => m.Active && m.PlanningItem.ItemName.Contains("Investment"));
            var arrow = "<i class='flaticon2-right-arrow icon-nm'></i>";
            string nextTitle = "Planning";
            string nextAction = "investmentplanning/investment-planning-intro";
            if (planningItems != null)
            {
                nextTitle = $"{planningItems.PlanningItem.ItemName.Replace("Planning", "")} {arrow} Intro";
                nextAction = $"{planningItems.PlanningItem.ItemName.Replace(" ", "").ToLower()}/{planningItems.PlanningItem.ItemName.Replace(" ", "-").ToLower().Replace("-care", "care")}-intro";
            }
            bool isExpress = nextAction.Contains("investment") && !clientData.InvestmentVersion;
            bool hasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = hasInvestmentIncome ? "Investment Income" : "Social Security Estimate",
                PrevAction = hasInvestmentIncome ? "income/investment-income" : "income/social-security-estimate",
                NextTitle = !isExpress ? nextTitle : hasInvestmentPlanning ? "Investment Planning Express" : "LifeStyle Intro",
                NextAction = !isExpress ? nextAction : hasInvestmentPlanning ? "investmentplanning/investment-planning-intro" : "lifestyle/life-style-intro"
            };
            return View("BudgetInfo/_BudgetInfoLite", budgetLiteInfo);
        }
        public IActionResult AddBudgetInfo()
        {
            return View("BudgetInfo/_AddBudgetModal");
        }
        public async Task<IEnumerable<ClientBudget>> GetBudgetItems(int? parentId = null)
        {
            var clientPrimaryProperty = _context2.ClientProperties.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid && m.Type == PropertyTypeEnum.Primary);
            return (await _budgetService.GetBudgetItemsAsync(ClientLoginGuid, parentId)).Where(m => m.Amount == 0)
                .Select(m =>
                {
                    m.Amount = m.BudgetItem.ItemName == "Mortgage/Rent" && clientPrimaryProperty?.CurrentValue > 0 ? clientPrimaryProperty.CurrentValue : 0;
                    m.BalanceOwn = m.BudgetItem.ItemName == "Mortgage/Rent" && clientPrimaryProperty?.AmountOwed > 0 ? clientPrimaryProperty.AmountOwed : 0;
                    m.YearsPayOff = m.BudgetItem.ItemName == "Mortgage/Rent" && clientPrimaryProperty?.YearsPayOff > 0 ? Convert.ToInt32(clientPrimaryProperty.YearsPayOff) : 0;
                    return m;
                });
        }
        [HttpPost]
        public async Task<IActionResult> AddBudgets(List<ClientBudget> clientBudgets, int parentId)
        {
            var items = clientBudgets.Where(m => m.Amount > 0);
            if (items.Any())
            {
                items = items.Select(i => { i.BudgetItem = null; i.ClientId = ClientId; return i; });
                _context2.ClientBudgets.AddRange(items);
                await _context2.SaveChangesAsync();
            }
            var updatedItems = (await _budgetService.GetBudgetItemsAsync(ClientLoginGuid)).Where(m => m.BudgetItem.ParentId == parentId && m.Amount > 0);
            var total = updatedItems.Sum(m => m.Amount);
            var totalBalanceOwn = updatedItems.Sum(m => m.BalanceOwn);
            bool isRevolvingDebt = updatedItems.Any(m => m.BudgetItem.IsRevolvingDebt);
            var result = new CommonResponse
            {
                Success = items.Any(),
                Message = items.Any() ? Message.SAVE_SUCCESS : Message.NO_RECORD_UPDATE,
                Data = items.Any() ? new { updatedItems, isRevolvingDebt, total, totalBalanceOwn } : null
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateBudgetInfo(ClientBudget clientBudgetInfo)
        {
            var clientBudget = await _context2.ClientBudgets.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == clientBudgetInfo.Guid).FirstOrDefaultAsync();

            decimal total = 0;
            if (clientBudget is not null)
            {
                clientBudget.Amount = clientBudgetInfo.Amount;
                clientBudget.Inflationary = clientBudgetInfo.Inflationary;
                await _context2.SaveChangesAsync();
                total = _context2.ClientBudgets.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.BudgetItem.ParentId == clientBudgetInfo.BudgetType).Sum(m => m.Amount);
                clientBudget.ClientLogin = null;
            }
            var result = new CommonResponse
            {
                Success = clientBudgetInfo is not null,
                Message = clientBudgetInfo is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { clientBudgetInfo, total }
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveBudgetLiteInfo(BudgetLiteInfo budgetLiteInfo)
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();

            if (clientData is not null)
            {
                var clientDataObj = _mapper.Map(budgetLiteInfo, clientData);
                clientDataObj.ClientId = ClientId;
                _context2.ClientDatas.Update(clientDataObj);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = clientData is not null,
                Message = clientData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteBudget(Guid Guid, int? parentId)
        {
            var budget = await _context2.ClientBudgets.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            decimal total = 0;
            int? totalBalanceOwn = 0;
            bool isRevolvingDebt = false;
            if (budget is not null)
            {
                _context2.ClientBudgets.Remove(budget);
                await _context2.SaveChangesAsync();
                var budgetItems = _context2.ClientBudgets.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.BudgetItem.ParentId == parentId);
                total = budgetItems.Sum(m => m.Amount);
                totalBalanceOwn = await budgetItems.SumAsync(m => m.BalanceOwn);
                isRevolvingDebt = budgetItems.Any(m => m.BudgetItem.IsRevolvingDebt);
            }
            var result = new CommonResponse
            {
                Success = budget is not null,
                Message = budget is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                Data = new { total, totalBalanceOwn, isRevolvingDebt }
            };
            return Json(result);
        }
    }
}
