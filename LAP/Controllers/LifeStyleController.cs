﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LAP.Filters;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using System.Reflection.Metadata;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class LifeStyleController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public LifeStyleController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 5;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "life-style-intro" })]
        public async Task<IActionResult> LifeStyleIntro()
        {
            var clientData = await _context2.ClientDatas.FirstOrDefaultAsync(m => m.ClientLogin.Guid == ClientLoginGuid);
            var planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).OrderBy(m => m.PlanningItem.SortOrder).Select(m => m.PlanningItemId);
            var menuItems = await MenuDataHelper.GetMenuDataAsync();
            var Ids = menuItems.Where(m => m.ParentId == 4).Select(m => m.Id);
            var lastItem = menuItems.LastOrDefault(m => Ids.Contains(m.ParentId) && planningItemIds.Contains(m.PlanningItemId)) ?? new MenuModel() { Controller = string.Empty };
            var arrow = !string.IsNullOrEmpty(lastItem.Action) ? "<i class='flaticon2-right-arrow icon-nm'></i>" : "";
            bool isExpress = !clientData.BudgetVersion;
            var budgetText = isExpress ? " Express" : "";
            var budgetAction = isExpress ? "-express" : "";
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = !clientData.InvestmentVersion && lastItem.Controller.Contains("Investment") ? "Investment Planning Express" : !string.IsNullOrEmpty(lastItem.Action) ? $"{lastItem.Controller.Replace("Planning", "")} {arrow} {lastItem.Title}" : $"Budget Info{budgetText}",
                PrevAction = !clientData.InvestmentVersion && lastItem.Controller.Contains("Investment") ? "investmentplanning/investment-planning-express" : !string.IsNullOrEmpty(lastItem.Action) ? $"{lastItem.Controller}/{lastItem.Action}" : $"expense/budget-info{budgetAction}",
                NextTitle = "Health Assessment",
                NextAction = "lifestyle/health-assessment"
            };
            return View("Intro");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "health-assessment" })]
        public async Task<IActionResult> HealthAssessment()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var clientHealth = await _commonService.GetHealthAsync(ClientLoginGuid);
            ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "LifeStyle Intro", PrevAction = "lifestyle/life-style-intro", NextTitle = "LifeStyle Expectations", NextAction = "lifestyle/health-expectations" };
            return View("HealthAssessment", clientHealth);
        }
        [HttpPost]
        public async Task<IActionResult> SaveHealthAssesment(List<ClientHealthItem> clientHealthItem)
        {
            var healthItems = _context2.ClientHealthItems.Where(m => clientHealthItem.Select(m => m.Id).Contains(m.Id));
            if (healthItems.Any())
            {
                foreach (var item in healthItems)
                {
                    item.isClientActive = clientHealthItem.FirstOrDefault(n => n.Id == item.Id).isClientActive;
                    item.isSpouseActive = clientHealthItem.FirstOrDefault(n => n.Id == item.Id).isSpouseActive;
                }
            }
            else
            {
                clientHealthItem = clientHealthItem.Select(m => { m.ClientId = ClientId; return m; }).ToList();
                _context2.ClientHealthItems.AddRange(clientHealthItem);
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = clientHealthItem is not null,
                Message = clientHealthItem is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "health-expectations" })]
        public async Task<IActionResult> HealthExpectations()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var clientEpectationItems = await _commonService.GetExpectationItemsAsync(ClientLoginGuid);
            ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            bool isExpress = !clientData.RiskArcVersion;
            var riskArcText = isExpress ? " Express" : "";
            var riskArcAction = isExpress ? "-express" : "";
            ViewBag.nextPrev = new PrevNextMenuModel
            {
                PrevTitle = "Health Assessment",
                PrevAction = "lifestyle/health-assessment",
                NextTitle = $"Final Thought",
                NextAction = $"/final-thought"
            };
            return View("HealthExpectations", clientEpectationItems);
        }
        [HttpPost]
        public async Task<IActionResult> SaveExpectation(List<ClientExpectation> clientExpectations)
        {
            var expectationItems = _context2.ClientExpectations.Where(m => clientExpectations.Select(m => m.Id).Contains(m.Id));
            if (expectationItems.Any())
            {
                foreach (var item in expectationItems)
                {
                    item.isClientActive = clientExpectations.FirstOrDefault(n => n.Id == item.Id).isClientActive;
                    item.isSpouseActive = clientExpectations.FirstOrDefault(n => n.Id == item.Id).isSpouseActive;
                }
            }
            else
            {
                clientExpectations = clientExpectations.Select(m => { m.ClientId = ClientId; return m; }).ToList();
                _context2.ClientExpectations.AddRange(clientExpectations);
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = clientExpectations is not null,
                Message = clientExpectations is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~LifeStyleController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}