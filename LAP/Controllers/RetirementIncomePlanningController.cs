﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using LAP.Filters;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace LAP.Controllers
{
    [Authorize(Roles = Roles.Client, AuthenticationSchemes = AuthenticationSchemes.ClientAuth)]
    public class RetirementIncomePlanningController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CommonService _commonService;
        protected readonly Guid ClientLoginGuid;
        private readonly int ClientId;
        public RetirementIncomePlanningController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _commonService = new CommonService(_context2);
            ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            ClientId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientId").ClaimToInt();
        }
        public IActionResult Index()
        {
            ViewBag.ParentId = 4;
            ViewBag.PlanningItemId = 3;
            ViewBag.planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            return View("WizardContainer");
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "retirement-income-planning-intro" })]
        public async Task<IActionResult> RetirementIncomePlanningIntro()
        {
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("retirement-income-planning-intro", ClientLoginGuid);
            return View();
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "retirement-income-planning-status" })]
        public async Task<IActionResult> RetirementIncomePlanningStatus()
        {
            var retrimentStatuses = _context2.RetirementStatuses.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToList();
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            if (client.ClientData.MaritalStatus != MaritalStatusEnum.Married)
                retrimentStatuses = retrimentStatuses.Where(m => m.employeeOf != ClientTypeEnum.Spouse).ToList();
            string clientName = client.ClientData.FirstName;
            string spouseName = Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate() ? client.SpouseData.FirstName : null;
            if (!retrimentStatuses.Any(m => m.employeeOf == ClientTypeEnum.Client))
                retrimentStatuses.Add(new RetirementStatus
                {
                    employeeOf = ClientTypeEnum.Client,
                });
            if (!retrimentStatuses.Any(m => m.employeeOf == ClientTypeEnum.Spouse) && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate())
                retrimentStatuses.Add(new RetirementStatus
                {
                    employeeOf = ClientTypeEnum.Spouse
                });
            retrimentStatuses = retrimentStatuses.Select(m => { m.EmployerTitle = m.employeeOf == ClientTypeEnum.Client ? clientName : spouseName; return m; }).ToList();
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("retirement-income-planning-status", ClientLoginGuid);
            return View("RetirementIncomePlanningStatus", retrimentStatuses);
        }
        public async Task<IActionResult> SaveStatusPlanning(List<RetirementStatus> RetirementStatus)
        {
            var rsData = _context2.RetirementStatuses.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (RetirementStatus is not null)
            {
                foreach (var item in RetirementStatus)
                {
                    var rsobjex = await rsData.Where(m => m.Guid == item.Guid).FirstOrDefaultAsync();
                    if (rsobjex is not null)
                    {
                        var ssDataObj = _mapper.Map(item, rsobjex);
                        _context2.RetirementStatuses.Update(ssDataObj);
                    }
                    else
                    {
                        item.ClientId = ClientId;
                        _context2.RetirementStatuses.Add(item);
                    }
                }
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = rsData is not null,
                Message = rsData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "retirement-income-planning-income-concern" })]
        public async Task<IActionResult> RetirementIncomePlanningIncomeConcern()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var clientIncomeConcerns = await _commonService.GetIncomeConcernAsync(ClientLoginGuid);
            ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("retirement-income-planning-income-concern", ClientLoginGuid);
            return View("RetirePlanningIncomeConcern", clientIncomeConcerns);
        }
        [TypeFilter(typeof(LastActionLogFilterAttribute), Arguments = new object[] { "retirement-income-planning-expense-concern" })]
        public async Task<IActionResult> RetirementIncomePlanningExpenseConcern()
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var clientExpenseConcerns = await _commonService.GetExpenseConcernAsync(ClientLoginGuid);
            ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
            ViewBag.EmploymentOf = await _dropdownDataHelper.GetEmploymentOfAsync(clientData.MaritalStatus.ToString());
            ViewBag.nextPrev = await _commonService.GetPlanningPrevNextAsync("retirement-income-planning-expense-concern", ClientLoginGuid);
            return View("RetirePlanningExpenseConcern", clientExpenseConcerns);
        }
        [HttpPost]
        public async Task<IActionResult> SaveRetirementIncome(List<ClientIncomeConcern> clientIncomeConcerns)
        {
            var incomeConcernItems = _context2.ClientIncomeConcerns.Where(m => clientIncomeConcerns.Select(m => m.Id).Contains(m.Id));
            if (incomeConcernItems.Any())
            {
                foreach (var item in incomeConcernItems)
                {
                    item.radioListforClientEnum = clientIncomeConcerns.FirstOrDefault(n => n.Id == item.Id).radioListforClientEnum;
                    item.radioListforSpouseEnum = clientIncomeConcerns.FirstOrDefault(n => n.Id == item.Id).radioListforSpouseEnum;
                }
            }
            else
            {
                clientIncomeConcerns = clientIncomeConcerns.Select(m => { m.ClientId = ClientId; return m; }).ToList();
                _context2.ClientIncomeConcerns.AddRange(clientIncomeConcerns);
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = clientIncomeConcerns is not null,
                Message = clientIncomeConcerns is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveRetirementExpense(List<ClientExpenseConcern> clientExpenseConcerns)
        {
            var expenseConcernItems = _context2.ClientExpenseConcerns.Where(m => clientExpenseConcerns.Select(m => m.Id).Contains(m.Id));
            if (expenseConcernItems.Any())
            {
                foreach (var item in expenseConcernItems)
                {
                    item.radioListforClientEnum = clientExpenseConcerns.FirstOrDefault(n => n.Id == item.Id).radioListforClientEnum;
                    item.radioListforSpouseEnum = clientExpenseConcerns.FirstOrDefault(n => n.Id == item.Id).radioListforSpouseEnum;
                }
            }
            else
            {
                clientExpenseConcerns = clientExpenseConcerns.Select(m => { m.ClientId = ClientId; return m; }).ToList();
                _context2.ClientExpenseConcerns.AddRange(clientExpenseConcerns);
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = clientExpenseConcerns is not null,
                Message = clientExpenseConcerns is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~RetirementIncomePlanningController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}