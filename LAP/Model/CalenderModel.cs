﻿using LAP.DomainModels;
using System;
using System.Diagnostics;

namespace LAP.Model
{
    public class CalenderModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public ActivityTypeEnum EventType { get; set; }
        public string Color { get; set; }
        public bool AllDay { get; set; }
    }
}
