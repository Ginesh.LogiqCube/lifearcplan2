﻿using LAP.DomainModels.Entities.Client;
using System;

namespace LAP.Model
{
    public class TaxDocumentClientListModel
    {
        public Guid Guid { get; set; }
        public string AdviserName { get; set; }
        public string EncryptedGuid { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public ClientData ClientData { get; set; }
        public int TaxPrepStatus { get; set; }
        public string TaxPrepStatusString { get; set; }
        public int TotalTaxDocument { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
