﻿namespace LAP.Model
{
    public class DocumentsSearchModel
    {
        public int? Id { get; set; }
        public int? ClientId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public int? Status { get; set; }
        public int? ReviewedById { get; set; }
        public int? UserId { get; set; }
        public int? InsuranceType { get; set; }
        public string ClientNameOnly { get; set; }
    }
}