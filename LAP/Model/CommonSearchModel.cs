﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LAP.Model
{
    public class CommonSearchModel
    {
        public int? Id { get; set; }
        public string ClientName { get; set; }
        public string AdviserName { get; set; }
        public string ComplianceName { get; set; }
        public string AffliationName { get; set; }
        public string Username { get; set; }
        public int? FirmId { get; set; }
        public string FirmName { get; set; }
        public string Roles { get; set; }
        public int? UserId { get; set; }
        public int? TaxPrepStatus { get; set; }
        public List<int> RolesList
        {
            get
            {
                return !string.IsNullOrEmpty(Roles) ? Roles.Split(',').Select(int.Parse).ToList() : new List<int>();
            }
        }
    }
}