﻿using LAP.Areas.Adviser.Models;
using LAP.DomainModels.Entities.Client;
using System.Collections.Generic;

namespace LAP.Model
{
    public class RetirementAssetModel
    {
        public int PersonalPropertyForSellAtRetirement { get; set; }
        public int RetirementAccount { get; set; }
        public int BankCreditUnionAccount { get; set; }
        public int BrokerageAdvisoryAccount { get; set; }
        public int LifeAnnuityAccount { get; set; }
        public int Total
        {
            get => PersonalPropertyForSellAtRetirement + RetirementAccount + BankCreditUnionAccount + BrokerageAdvisoryAccount + LifeAnnuityAccount;
        }
    }
    public class CashFlowModel
    {
        public int CurrentHouseholdIncome
        {
            get => EarnedIncome + OtherIncome;
        }
        public int EarnedIncome { get; set; }
        public int OtherIncome { get; set; }
        public int NonInflationaryExpenses { get; set; }
        public int InflationaryExpenses { get; set; }
        public int NetYearlyIncome
        {
            get => CurrentHouseholdIncome - NonInflationaryExpenses - InflationaryExpenses;
        }
    }
    public class NetWorthModel
    {
        public int RealProperty { get; set; }
        public int PersonalProperty { get; set; }
        public int PropetyAnalysisTotal
        {
            get => RealProperty + PersonalProperty;
        }
        public int ClientAssetsTotal { get; set; }
        public int ClientDebtTotal { get; set; }
        public int NetWorth
        {
            get => ClientAssetsTotal - ClientDebtTotal;
        }
    }
    public class RiskArcDataModel
    {
        public double RiskCapacityScore { get; set; }
        public IEnumerable<string> BehavioralInvestmentType { get; set; }
        public string RiskProfile { get; set; }
    }
    public class RiskProfileModel
    {
        public double FinancialIndependentScore { get; set; }
        public RiskArcDataModel ClientRiskData { get; set; }
        public RiskArcDataModel SpouseRiskData { get; set; }
        public bool RiskArcVersion { get; set; }
        public bool IsMarried { get; set; }
        public string ClientName { get; set; }
        public string SpouseName { get; set; }
    }
    public class GoalTrackerModel
    {
        public double CurrentAnnualAvgRateOfReturn { get; set; }
        public double RequiredAnnualAvgRateOfReturn { get; set; }
        public long TotalInvestmentNeededAtRetirement { get; set; }
        public int ProjectedValueOfInvestmentAtRetirement { get; set; }
    }
    public class TaxPrepStatusModel
    {
        public int Year { get; set; }
        public string Status { get; set; }
    }
    public class ClientDashboardModel
    {
        public RetirementAssetModel Asset { get; set; }
        public CashFlowModel CashFlow { get; set; }
        public RiskProfileModel RiskProfile { get; set; }
        public NetWorthModel NetWorth { get; set; }
        public bool HasEstatePlanning { get; set; }
        public EstatePlanningModel Estate { get; set; }
        public GoalTrackerModel GoalTracker { get; set; }
        public IEnumerable<TaxPrepStatusModel> TaxPrepStatuses { get; set; }
        public IEnumerable<TaxPrepHistory> TaxPrepHistories { get; set; }
    }
}