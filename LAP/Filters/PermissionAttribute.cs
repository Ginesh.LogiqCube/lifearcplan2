﻿using LAP.DomainModels.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LAP.Filters
{
    public class PermissionAttribute : ActionFilterAttribute
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public PermissionAttribute(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            //var routeValues = context.ActionDescriptor.RouteValues;
            //if (routeValues.Count() > 0)
            //{
            //    var action = routeValues["action"].ToLower();
            //    var menuitems = MenuDataHelper.GetMenuData().Where(m => m.ControllerAction?.ToLower() == action && m.ParentId > 0 && !string.IsNullOrEmpty(m.Condition));
            //    if (menuitems.Count() > 0)
            //    {
            //        var ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid");
            //        var clientData = _context2.ClientData.FirstOrDefault(m => m.ClientLogin.Guid.ToString() == ClientLoginGuid.Value);
            //        var maritalStatus = (int)clientData?.MaritalStatus;
            //        bool isExist = menuitems.Any(m => m.Condition.Contains(maritalStatus.ToString()));
            //        if (isExist)
            //            context.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Error" }, { "Action", "UnAuthorize" } });
            //        base.OnActionExecuting(context);
            //    }
            //}
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
