﻿using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Filters
{
    public class LastActionLogFilterAttribute : IAsyncActionFilter
    {
        protected readonly string _lastAccessPage;
        public LastActionLogFilterAttribute(string lastAccessPage)
        {
            _lastAccessPage = lastAccessPage;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context,ActionExecutionDelegate next)
        {
            var httpContext = context.HttpContext;
            string lastPageAction = _lastAccessPage;
            if(lastPageAction.Contains("riskarc"))
                lastPageAction = httpContext.Request.Path.Value.TrimStart('/');
            var commonService = context.HttpContext.RequestServices.GetRequiredService<CommonService>();
            Guid ClientLoginGuid = httpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            await commonService.UpdateLastAccessPage(ClientLoginGuid,lastPageAction);
            await next();
        }
    }
}
