﻿using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Entities.Master;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class BudgetService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly CommonService _commonService;
        private bool disposed;

        public BudgetService(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _commonService = new CommonService(_context2);
        }

        public async Task<IEnumerable<ClientBudget>> GetBudgetItemsAsync(Guid ClientLoginGuid, int? parentId = null)
        {
            var clientBudget = _context2.BudgetItems.Include(m => m.Parent).AsNoTracking().SelectMany
            (
                clientBudget => _context2.ClientBudgets.Where(budgetItem => clientBudget.Id == budgetItem.BudgetItemId && budgetItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty().AsNoTracking(),
                (budgetItem, clientBudget) => new ClientBudget
                {
                    Guid = clientBudget != null ? clientBudget.Guid : Guid.Empty,
                    ClientId = clientBudget != null ? clientBudget.ClientId : 0,
                    Inflationary = clientBudget != null && clientBudget.Inflationary,
                    BalanceOwn = clientBudget != null ? clientBudget.BalanceOwn : 0,
                    YearsPayOff = clientBudget != null ? clientBudget.YearsPayOff : 0,
                    Amount = clientBudget != null ? clientBudget.Amount : 0,
                    BudgetItemId = budgetItem.Id,
                    BudgetItem = new BudgetItem
                    {
                        Guid = budgetItem.Guid,
                        ParentId = budgetItem.ParentId,
                        ItemName = budgetItem.ItemName,
                        IsRevolvingDebt = budgetItem.IsRevolvingDebt,
                        Parent = new BudgetItem
                        {
                            ItemName = budgetItem.Parent != null ? budgetItem.Parent.ItemName : string.Empty,
                            SortOrder = budgetItem.Parent != null ? budgetItem.Parent.SortOrder : 0
                        }
                    }
                }
            );
            if (parentId > 0)
                clientBudget = clientBudget.Where(m => m.BudgetItem.ParentId == parentId);
            return await Task.FromResult(clientBudget.OrderBy(m => m.BudgetItem.Parent.SortOrder));
        }
        public async Task<FVTotalModel> GetInfaltionaryBudgetFutureValueExpressAsync(Guid ClientLoginGuid)
        {
            var clientData = _context2.ClientDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            int infaltionaryFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), yearToRetire, clientData?.InflationaryBudget);
            int result = Convert.ToInt32(infaltionaryFV).NagativeToZero();
            return new FVTotalModel
            {
                PresentValue = clientData.InflationaryBudget,
                FutureValue = infaltionaryFV
            };
        }
        public async Task<FVTotalModel> GetInflationaryBudgetFutureValue(Guid ClientLoginGuid)
        {
            var clientData = _context2.ClientDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var infationaryBudgetData = (await GetBudgetItemsAsync(ClientLoginGuid)).Where(m => m.Inflationary).Select(m => new
            {
                m.BudgetItem.IsRevolvingDebt,
                m.Inflationary,
                PresentValue = m.Amount,
                m.YearsPayOff,
                FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), yearToRetire, m.Amount)
            });
            int inflationaryFv = infationaryBudgetData.Where(m => m.YearsPayOff >= yearToRetire || !m.IsRevolvingDebt).Sum(m => m.FutureValue);

            return new FVTotalModel
            {
                PresentValue = infationaryBudgetData.Sum(m => m.PresentValue),
                FutureValue = inflationaryFv
            };
        }
        public async Task<IEnumerable<ClientBudget>> GetNonDiscretionaryBudgetDataAsync(Guid ClientLoginGuid)
        {
            string[] expenses = { "Mortgage/Rent", "HOA Fee/Condo Fee", "Homeowners/Renters Insurance","Automobile Payment","Automobile Insurance","Automobile Maintenance","Public Transportation","Fuel Cost","Water/Sewer","Garbage Service",
                               "Electricity","Gas/Fuel/Coal","Groceries/Household goods","Credit Card(s)","Home Equity Line of Credit","Installment Note","Hair","Health: Medical/Dental, Supplemental","Life","Disability",
                               "Long Term Care","Liability/Umbrella Coverage","Child Support","Alimony","Income Taxes","Property Taxes" };
            var budgetItes = (await GetBudgetItemsAsync(ClientLoginGuid)).Where(m => m.Amount > 0 && expenses.Contains(m.BudgetItem.ItemName));
            return budgetItes;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                }
                disposed = true;
            }
        }
        ~BudgetService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
