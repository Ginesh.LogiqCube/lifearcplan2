﻿using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LAP.Services.Integration
{
    public class CFSService
    {
        private readonly LifeArcPlanContext2 _context2;
        protected readonly IConfiguration _config;
        private readonly RestClient _restClient;
        protected readonly CommonService _commonService;
        protected readonly IWebHostEnvironment _env;
        public CFSService(LifeArcPlanContext2 context2, IConfiguration config, IWebHostEnvironment env)
        {
            _context2 = context2;
            _config = config;
            _env = env;
            _restClient = new RestClient(_config["Integration:CFS:Url"]);
            _commonService = new CommonService(_context2);
        }
        public async Task<bool> Authenticate(LoginModel model, Guid UserLoginGuid, int UserId, bool active = true)
        {
            var integration = await _context2.Integrations.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync() ?? new DomainModels.Entities.Adviser.Integration();
            bool isAuthenticated = false;
            if (active)
            {
                var request = new RestRequest($"cfsexpert/api/advisor/list", Method.Post);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("userid", _config["Integration:CFS:MasterUserId"]);
                request.AddParameter("apikey", _config["Integration:CFS:MasterApiKey"]);
                request.AddParameter("filter_username", model.Username);
                request.AddParameter("filter_password", model.Password);
                var response = await _restClient.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.OK && response.Content != "[]")
                {
                    var cfsUser = JArray.Parse(response.Content).FirstOrDefault();
                    integration.UserId = UserId;
                    integration.CFSUserId = cfsUser["userid"].ToString();
                    integration.CFSUserApiKey = cfsUser["apikey"].ToString();
                    isAuthenticated = true;
                }
            }
            else
            {
                integration.CFSUserId = null;
                integration.CFSUserApiKey = null;
            }
            _context2.Integrations.Update(integration);
            await _context2.SaveChangesAsync();
            return isAuthenticated;
        }       
        public async Task<CFSModel> GetCFSStudentAsync(Guid UserLoginGuid, string CFSStudentId)
        {
            var integration = await _context2.Integrations.FirstOrDefaultAsync(m => m.UserLogin.Guid == UserLoginGuid);
            var request = new RestRequest($"cfsexpert/api/client/{CFSStudentId}", Method.Get);
            request.AddParameter("userid", integration.CFSUserId);
            request.AddParameter("apikey", integration.CFSUserApiKey);
            var response = await _restClient.ExecuteAsync(request);
            CFSModel CFSModel = response.StatusCode == HttpStatusCode.OK ? response.Content.StringToJsonObject<CFSModel>() : new CFSModel();
            CFSModel.CFSStudentId = CFSStudentId;
            return CFSModel;
        }
        public async Task UpdateCFSStudentAsync(Guid UserLoginGuid, CFSModel model)
        {
            var student = await _context2.EducationPlannings.FirstOrDefaultAsync(m => m.Guid == model.StudentGuid);
            var integration = await _context2.Integrations.FirstOrDefaultAsync(m => m.UserLogin.Guid == UserLoginGuid);
            if (!string.IsNullOrEmpty(integration?.CFSUserId))
            {
                var request = new RestRequest($"cfsexpert/api/client/{model.CFSStudentId}", Method.Post);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                model.userid = integration.CFSUserId;
                model.apikey = integration.CFSUserApiKey;
                request.AddObject(model);
                var response = await _restClient.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string CFSStudentIdResult = JObject.Parse(response.Content)["id"].ToString();
                    student.CFSStudentId = CFSStudentIdResult;
                    await _context2.SaveChangesAsync();
                    var requestReport = new RestRequest($"cfsexpert/api/reports/{CFSStudentIdResult}", Method.Get);
                    requestReport.AddParameter("userid", integration.CFSUserId);
                    requestReport.AddParameter("apikey", integration.CFSUserApiKey);
                    await _restClient.ExecuteAsync(requestReport);
                }
            }
        }
    }
}