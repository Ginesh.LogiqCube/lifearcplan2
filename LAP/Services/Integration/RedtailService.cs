﻿using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LAP.Services.Integration
{
    public class RedtailService
    {
        private readonly LifeArcPlanContext2 _context2;
        protected readonly IConfiguration _config;
        private readonly RestClient _restClient;
        public RedtailService(LifeArcPlanContext2 context2, IConfiguration config)
        {
            _context2 = context2;
            _config = config;
            _restClient = new RestClient(_config["Integration:Redtail:Url"]);
        }
        public async Task<bool> Authenticate(LoginModel model, Guid UserLoginGuid, bool active = true)
        {
            var integration = await _context2.Integrations.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync() ?? new DomainModels.Entities.Adviser.Integration();
            var userLogin = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).FirstOrDefaultAsync();
            bool isAuthenticated = false;
            if (active)
            {
                string token = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes($"{_config["Integration:Redtail:ApiKey"]}:{model.Username}:{model.Password}"));
                var request = new RestRequest("authentication", Method.Get);
                request.AddHeader("Authorization", $"Basic {token}");
                var response = await _restClient.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var redtailResultObj = JObject.Parse(response.Content)["authenticated_user"];
                    RedtailUser redtailResult = redtailResultObj.StringToJsonObject<RedtailUser>();
                    integration.RedtailUserId = redtailResult.UserId;
                    integration.RedtailUserKey = new Guid(redtailResult.UserKey);
                    integration.RedtailDatabaseId = redtailResult.DatabaseId;
                    isAuthenticated = true;
                }
            }
            else
            {
                integration.RedtailUserId = null;
                integration.RedtailUserKey = null;
                integration.RedtailDatabaseId = null;
            }
            integration.UserId = userLogin.Id;
            _context2.Integrations.Update(integration);
            await _context2.SaveChangesAsync();
            return isAuthenticated;
        }
        public async Task<bool> IsExistContactId(int ContactId, string token)
        {
            var request = new RestRequest($"/contacts/{ContactId}", Method.Get);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            var response = await _restClient.ExecuteAsync(request);
            return response.StatusCode == HttpStatusCode.OK;
        }
        public async Task AddNote(int ContactId, string token, int tz)
        {
            var request = new RestRequest($"/contacts/{ContactId}/notes", Method.Post);
            var dateTime = DateTime.UtcNow.AddMinutes(-tz);
            var note = new RedtailNotes
            {
                CategoryId = RedtailCategoryEnum.GeneralInformation,
                NoteType = RedtailNoteTypeEnum.Note,
                Body = $"This contact record was created by LifeArcPlan on {dateTime:MMM dd, yyyy} at {dateTime:hh:mm tt}"
            };
            request.AddBody(note);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            await _restClient.ExecuteAsync(request);
        }
        public async Task UpdateSpouseContact(Guid ClientLoginGuid, string FamilyName, int ClientContactId)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid)
                .Include(m => m.SpouseData).Include(m => m.UserLogin).Include(m => m.UserLogin.Integration)
                .Include(m => m.ClientContactInfo)
                .FirstOrDefaultAsync();
            var spouseData = client.SpouseData;
            if (client.UserLogin.Integration?.RedtailUserId > 0)
            {
                var userIntegrationData = client.UserLogin.Integration;
                string token = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes($"{_config["Integration:Redtail:ApiKey"]}:{userIntegrationData.RedtailUserKey}"));
                bool IsRedtailContactIdExist = await IsExistContactId(spouseData.RedtailContactId, token);
                var clientData = client.ClientData;
                var maritalStatus = clientData.MaritalStatus;
                var marital_status_id = maritalStatus == MaritalStatusEnum.Unmarried ? RedtailMaritalStatusEnum.Single :
                                        maritalStatus == MaritalStatusEnum.Married ? RedtailMaritalStatusEnum.Married :
                                        maritalStatus == MaritalStatusEnum.Divorced ? RedtailMaritalStatusEnum.Divorced :
                                        maritalStatus == MaritalStatusEnum.Widowed ? RedtailMaritalStatusEnum.Widowed :
                                        maritalStatus == MaritalStatusEnum.Separated ? RedtailMaritalStatusEnum.Separated : default;
                var phoneIds = await GetPhoneIds(spouseData.RedtailContactId, token);
                var redtailContact = new RedtailContact
                {
                    Id = spouseData.RedtailContactId,
                    FirstName = spouseData.FirstName,
                    MiddleName = spouseData.MiddleInitial,
                    LastName = spouseData.LastName,
                    Type = "Individual",
                    MaritalStatusId = marital_status_id,
                    StatusId = !IsRedtailContactIdExist ? RedtailStatusEnum.SpouseOfClient : null,
                    DateOfBirth = spouseData.DateOfBirth.IsNotEmptyDate() ? spouseData.DateOfBirth.ToString("yyyy-MM-dd") : null,
                    Gender = spouseData.Gender,
                    MarriageDate = clientData.DateOfCurrentMarriage.HasValue ? clientData.DateOfCurrentMarriage.Value.ToString("yyyy-MM-dd") : null,
                    Phones = client.ClientContactInfo != null && !string.IsNullOrEmpty(client.ClientContactInfo.SpouseCellNumber) ? new List<RedtailPhone>
                    {
                        new RedtailPhone
                        {
                            Id=phoneIds.Length>0? phoneIds[0]:null,
                            CountryCode= 1,
                            Number = Regex.Replace(client.ClientContactInfo.SpouseCellNumber,@"[^0-9]+",string.Empty),
                            PhoneType = RedtailPhoneTypeEnum.Mobile,
                        }
                    } : null,
                    Emails = client.ClientContactInfo != null && !string.IsNullOrEmpty(client.ClientContactInfo.SpouseEmail) ? new List<RedtailEmail>
                    {
                        new RedtailEmail
                        {
                            Id=await GetEmailId(spouseData.RedtailContactId,token),
                            Address = client.ClientContactInfo.SpouseEmail,
                            EmailType = RedtailEmailTypeEnum.Home,
                            IsPrimary = true
                        }
                    } : null,
                };
                var method = IsRedtailContactIdExist ? Method.Put : Method.Post;
                var url = IsRedtailContactIdExist ? $"/contacts/{spouseData.RedtailContactId}" : "/contacts";
                var request = new RestRequest(url, method);
                request.AddHeader("Authorization", $"Userkeyauth {token}");
                request.AddBody(redtailContact);
                var response = await _restClient.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.OK)
                {
                    if (client.ClientContactInfo != null && string.IsNullOrEmpty(client.ClientContactInfo.SpouseCellNumber) && phoneIds.Length > 0)
                        await RemovePhone(spouseData.RedtailContactId, Convert.ToInt32(phoneIds[0]), token);
                    var redtailResultObj = JObject.Parse(response.Content)["contact"];
                    RedtailContact redtailResult = redtailResultObj.StringToJsonObject<RedtailContact>();
                    spouseData.RedtailContactId = redtailResult.Id;
                    await _context2.SaveChangesAsync();
                    await AddFamily(FamilyName, ClientContactId, spouseData.RedtailContactId, token);
                }
            }
        }
        public async Task AddFamily(string FamilyName, int ClientContactId, int SpouseContactId, string token)
        {
            var request = new RestRequest($"/families", Method.Post);
            var family = new RedtailFamily
            {
                Name = FamilyName,
                Members = new List<RedtailFamilyMember>
                {
                    new RedtailFamilyMember
                    {
                        ContactId = ClientContactId,
                        Hoh = true
                    },
                    new RedtailFamilyMember
                    {
                        ContactId = SpouseContactId,
                        Hoh = false,
                        Relationship = (int)RedtailRelationshipEnum.Spouse
                    }
                }
            };
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            request.AddBody(family);
            await _restClient.ExecuteAsync(request);
        }
        public async Task UpdateClientContact(Guid ClientLoginGuid, int tz)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid)
                .Include(m => m.ClientData).Include(m => m.UserLogin).Include(m => m.UserLogin.Integration)
                .Include(m => m.ClientContactInfo)
                .FirstOrDefaultAsync();
            if (client.UserLogin.Integration?.RedtailUserId > 0)
            {
                var clientData = client.ClientData;
                var clientAddress = client.ClientContactInfo;
                var userIntegrationData = client.UserLogin.Integration;
                string token = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes($"{_config["Integration:Redtail:ApiKey"]}:{userIntegrationData.RedtailUserKey}"));
                bool IsRedtailContactIdExist = await IsExistContactId(clientData.RedtailContactId, token);
                var maritalStatus = clientData.MaritalStatus;
                var marital_status_id = maritalStatus == MaritalStatusEnum.Unmarried ? RedtailMaritalStatusEnum.Single :
                                        maritalStatus == MaritalStatusEnum.Married ? RedtailMaritalStatusEnum.Married :
                                        maritalStatus == MaritalStatusEnum.Divorced ? RedtailMaritalStatusEnum.Divorced :
                                        maritalStatus == MaritalStatusEnum.Widowed ? RedtailMaritalStatusEnum.Widowed :
                                        maritalStatus == MaritalStatusEnum.Separated ? RedtailMaritalStatusEnum.Separated : default;
                var phoneIds = await GetPhoneIds(clientData.RedtailContactId, token);
                var redtailContact = new RedtailContact
                {
                    Id = clientData.RedtailContactId,
                    FirstName = clientData.FirstName,
                    MiddleName = clientData.MiddleInitial,
                    LastName = clientData.LastName,
                    Type = "Individual",
                    StatusId = !IsRedtailContactIdExist ? RedtailStatusEnum.Prospect : null,
                    MaritalStatusId = marital_status_id,
                    DateOfBirth = clientData.DateOfBirth.IsNotEmptyDate() ? clientData.DateOfBirth.ToString("yyyy-MM-dd") : null,
                    Gender = clientData.Gender,
                    MarriageDate = clientData.DateOfCurrentMarriage.HasValue && clientData.MaritalStatus == MaritalStatusEnum.Married ? clientData.DateOfCurrentMarriage.Value.ToString("yyyy-MM-dd") : null,
                    Addresses = client.ClientContactInfo != null ? new List<RedtailAddress>
                    {
                        new RedtailAddress
                        {
                             Id=await GetAddressId(clientData.RedtailContactId,token),
                             AddressType=RedtailAddressTypeEnum.Home,
                             StreetAddress=clientAddress.Address,
                             SuiteApt=!string.IsNullOrEmpty(clientAddress.POBox)? $"{clientAddress.Address2}, {clientAddress.POBox}" : clientAddress.Address2 ,
                             City=clientAddress.City,
                             State=clientAddress.State,
                             Zip=clientAddress.ZipCode,
                             IsPrimary = true,
                        }
                    } : null,
                    Phones = client.ClientContactInfo != null ? new List<RedtailPhone>
                    {
                        !string.IsNullOrEmpty(client.ClientContactInfo.Phone) ? new RedtailPhone
                        {
                            Id=phoneIds.Length>0? phoneIds[0]:null,
                            CountryCode= 1,
                            Number = Regex.Replace(client.ClientContactInfo.Phone,@"[^0-9]+",string.Empty),
                            PhoneType = RedtailPhoneTypeEnum.Work,
                            IsPrimary=true,
                        } : null,
                        !string.IsNullOrEmpty(client.ClientContactInfo.CellNumber) ? new RedtailPhone
                        {
                            Id=phoneIds.Length>1? phoneIds[1]:null,
                            CountryCode= 1,
                            Number = Regex.Replace(client.ClientContactInfo.CellNumber,@"[^0-9]+",string.Empty),
                            PhoneType = RedtailPhoneTypeEnum.Mobile,
                            IsPrimary=false
                        } : null
                    } : null,
                    Emails = new List<RedtailEmail>
                    {
                        new RedtailEmail
                        {
                            Id=await GetEmailId(clientData.RedtailContactId,token),
                            Address = client.Username,
                            EmailType = RedtailEmailTypeEnum.Work,
                            IsPrimary = true
                        }
                    },
                };
                var method = IsRedtailContactIdExist ? Method.Put : Method.Post;
                var url = IsRedtailContactIdExist ? $"/contacts/{clientData.RedtailContactId}" : "/contacts";
                var request = new RestRequest(url, method);
                request.AddHeader("Authorization", $"Userkeyauth {token}");
                request.AddBody(redtailContact);
                var response = await _restClient.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.OK)
                {
                    if (clientData.MaritalStatus != MaritalStatusEnum.Married)
                        await RemoveFamily(clientData.RedtailContactId, token);
                    if (client.ClientContactInfo != null && string.IsNullOrEmpty(client.ClientContactInfo.CellNumber) && phoneIds.Length > 1)
                        await RemovePhone(clientData.RedtailContactId, Convert.ToInt32(phoneIds[1]), token);
                    var redtailResultObj = JObject.Parse(response.Content)["contact"];
                    RedtailContact redtailResult = redtailResultObj.StringToJsonObject<RedtailContact>();
                    clientData.RedtailContactId = redtailResult.Id;
                    await AddNote(clientData.RedtailContactId, token, tz);
                    await _context2.SaveChangesAsync();
                }
            }
        }
        public async Task<int?> GetAddressId(int ClientContactId, string token)
        {
            var request = new RestRequest($"/contacts/{ClientContactId}/addresses", Method.Get);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            var response = await _restClient.ExecuteAsync(request);
            int? addressId = null;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var redtailAddressesResultObj = JObject.Parse(response.Content)["addresses"];
                List<RedtailAddress> redtailAddressesResult = redtailAddressesResultObj.StringToJsonObject<List<RedtailAddress>>();
                addressId = redtailAddressesResult.FirstOrDefault()?.Id;
            }
            return addressId;
        }
        public async Task<int?[]> GetPhoneIds(int ClientContactId, string token)
        {
            var request = new RestRequest($"/contacts/{ClientContactId}/phones", Method.Get);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            var response = await _restClient.ExecuteAsync(request);
            int?[] phoneIds = Array.Empty<int?>();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var redtailPhonesResultObj = JObject.Parse(response.Content)["phones"];
                List<RedtailPhone> redtailPhonesResult = redtailPhonesResultObj.StringToJsonObject<List<RedtailPhone>>();
                phoneIds = redtailPhonesResult.Select(m => m.Id).ToArray();
            }
            return phoneIds;
        }
        public async Task<int?> GetEmailId(int ClientContactId, string token)
        {
            var request = new RestRequest($"/contacts/{ClientContactId}/emails", Method.Get);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            var response = await _restClient.ExecuteAsync(request);
            int? emailId = null;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var redtailEmailsResultObj = JObject.Parse(response.Content)["emails"];
                List<RedtailEmail> redtailEmailsResult = redtailEmailsResultObj.StringToJsonObject<List<RedtailEmail>>();
                emailId = redtailEmailsResult.FirstOrDefault()?.Id;
            }
            return emailId;
        }
        public async Task RemovePhone(int ClientContactId, int PhoneId, string token)
        {
            var request = new RestRequest($"/contacts/{ClientContactId}/phones/{PhoneId}", Method.Delete);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            await _restClient.ExecuteAsync(request);
        }
        public async Task RemoveFamily(int ClientContactId, string token)
        {
            var request = new RestRequest($"/contacts/{ClientContactId}/family", Method.Get);
            request.AddHeader("Authorization", $"Userkeyauth {token}");
            var response = await _restClient.ExecuteAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var redtailFamilyResultObj = JObject.Parse(response.Content)["contact_family"];
                RedtailFamily redtailFamilyResult = redtailFamilyResultObj.StringToJsonObject<RedtailFamily>();
                var deleteRequest = new RestRequest($"/families/{redtailFamilyResult.Id}", Method.Delete);
                deleteRequest.AddHeader("Authorization", $"Userkeyauth {token}");
                await _restClient.ExecuteAsync(deleteRequest);
            }
        }
    }
}