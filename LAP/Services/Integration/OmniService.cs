﻿using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LAP.Services.Integration
{
    public class OmniService
    {
        private readonly LifeArcPlanContext2 _context2;
        protected readonly IConfiguration _config;
        private readonly RestClient _restClient;
        protected readonly CommonService _commonService;
        protected readonly IWebHostEnvironment _env;
        public OmniService(LifeArcPlanContext2 context2, IConfiguration config, IWebHostEnvironment env)
        {
            _context2 = context2;
            _config = config;
            _env = env;
            _restClient = new RestClient(_config["Integration:Omni:Url"]);
            _commonService = new CommonService(_context2);
        }
        public async Task<bool> Authenticate(LoginModel model, Guid UserLoginGuid, bool active = true)
        {
            bool isAuthenticated = false;
            var integration = await _context2.Integrations.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync() ?? new DomainModels.Entities.Adviser.Integration();
            if (active)
            {
                var (SessionName, UserId) = await GetSessionNameWithUserIdAsync(model);
                if (!string.IsNullOrEmpty(SessionName) && !string.IsNullOrEmpty(UserId))
                {
                    integration.OmniUserId = UserId;
                    integration.OmniUsername = model.Username;
                    integration.OmniAccessKey = await model.Password.EncryptAsync();
                    isAuthenticated = true;
                }
            }
            else
                integration.OmniUserId = null;
            await _context2.SaveChangesAsync();
            return isAuthenticated;
        }
        public async Task<(string SessionName, string UserId)> GetSessionNameWithUserIdAsync(LoginModel model)
        {
            string SessionName = string.Empty;
            string UserId = string.Empty;
            var request = new RestRequest($"webservice.php?operation=getchallenge&username={model.Username}", Method.Get);
            var response = await _restClient.ExecuteAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var omniResultObj = JObject.Parse(response.Content);
                OmniSessionModel omniResult = omniResultObj.StringToJsonObject<OmniSessionModel>();
                string token = omniResult.success ? omniResult.result.token : string.Empty;
                if (!string.IsNullOrEmpty(token))
                {
                    var prccessedAccessKey = $"{token}{model.Password}".GetMd5Hash();
                    var requestUser = new RestRequest($"webservice.php", Method.Post);
                    requestUser.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    requestUser.AddParameter("operation", "login");
                    requestUser.AddParameter("username", model.Username);
                    requestUser.AddParameter("accessKey", prccessedAccessKey);
                    var responseUser = await _restClient.ExecuteAsync(requestUser);
                    if (responseUser.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(response.Content))
                    {
                        var omniUserResultObj = JObject.Parse(responseUser.Content);
                        OmniUserModel omniUserResult = omniUserResultObj.StringToJsonObject<OmniUserModel>();
                        if (omniUserResult.success)
                        {
                            SessionName = omniUserResult.result.sessionName;
                            UserId = omniUserResult.result.userId;
                        }
                    }
                }
            }
            return (SessionName, UserId);
        }
        public async Task<bool> IsClientExist(string SessionName, string Id)
        {
            var request = new RestRequest($"webservice.php?operation=retrieve&sessionName={SessionName}&id={Id}", Method.Get);
            var response = await _restClient.ExecuteAsync(request);
            bool exist = false;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var omniResultObj = JObject.Parse(response.Content);
                OmniUserModel omniResult = omniResultObj.StringToJsonObject<OmniUserModel>();
                if (omniResult.success) exist = true;
            }
            return exist;
        }
        public async Task UpdateClientContact(LoginModel model, Guid ClientLoginGuid, string OmniContactId)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid)
                .Include(m => m.ClientData).Include(m => m.UserLogin).Include(m => m.UserLogin.Integration)
                .Include(m => m.SpouseData)
                .FirstOrDefaultAsync();
            if (!string.IsNullOrEmpty(client.UserLogin.Integration?.OmniUserId))
            {
                var clientData = client.ClientData;
                var spouseData = client.SpouseData;

                var (SessionName, UserId) = await GetSessionNameWithUserIdAsync(model);
                var Exist = await IsClientExist(SessionName, OmniContactId);
                var request = new RestRequest($"webservice.php", Method.Post);
                string action = Exist ? "update" : "create";
                OmniUserDetailModel omniUserDetail = new()
                {
                    id = !string.IsNullOrEmpty(OmniContactId) ? OmniContactId : string.Empty,
                    email = client.Username,
                    firstname = clientData.FirstName,
                    lastname = clientData.LastName,
                    cf_1500 = spouseData?.Name,
                    assigned_user_id = UserId
                };
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("operation", action);
                request.AddParameter("sessionName", SessionName);
                request.AddParameter("element", omniUserDetail.JsonObjectToString());
                if (!Exist)
                    request.AddParameter("elementType", "Contacts");
                var response = await _restClient.ExecuteAsync(request);
                if (response.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(response.Content))
                {
                    var omniUserResultObj = JObject.Parse(response.Content);
                    var ContactId = omniUserResultObj["result"]["id"];
                    clientData.OmniContactId = Convert.ToString(ContactId);
                    await _context2.SaveChangesAsync();
                }
            }
        }
    }
}