﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class OtherIncomeService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        private bool disposed;

        public OtherIncomeService(LifeArcPlanContext2 context2)
        {
            _context2 = context2;
        }
        public async Task<IEnumerable<OtherIncome>> GetOtherIncomeDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType)
        {
            var clientOtherIncomeData = _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
                clientOtherIncomeData = clientOtherIncomeData.Where(m => m.businessInterestof == clientType);
            return await Task.FromResult(clientOtherIncomeData);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~OtherIncomeService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
