﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class BusinessInterestService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        private bool disposed;

        public BusinessInterestService(LifeArcPlanContext2 context2)
        {
            _context2 = context2;
        }
        public async Task<IEnumerable<BusinessInterest>> GetBusinessInterestDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType)
        {
            var clientBusinessInterestData = _context2.BusinessInterests.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.businessInterestOf == clientType);
            return await Task.FromResult(clientBusinessInterestData);
        }
        public async Task<int> GetBusinessInterestFutureValueAsync(Guid ClientLoginGuid, ClientTypeEnum clientType)
        {
            var clientBusinessInterestData = (await GetBusinessInterestDataAsync(ClientLoginGuid, clientType))
                .Select(m => new
                {
                    FutureValue = m.PBValueatRetirement * (m.AmountOfOwnership / 100)
                });
            decimal result = clientBusinessInterestData.Sum(m => m.FutureValue);
            return Convert.ToInt32(result).NagativeToZero();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~BusinessInterestService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
