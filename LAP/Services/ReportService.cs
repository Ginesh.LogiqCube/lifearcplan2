﻿using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class ReportService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly CommonService _commonService;
        protected readonly BudgetService _budgetService;
        protected readonly EmploymentService _employmentService;
        protected readonly InvestmentService _investmentService;
        protected readonly SocialSecurityService _socialSecurityService;
        protected readonly PropertyService _propertyService;
        protected readonly OtherIncomeService _otherIncomeService;
        protected readonly BusinessInterestService _businessInterestService;
        private bool disposed;
        public ReportService(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _commonService = new CommonService(_context2);
            _budgetService = new BudgetService(_context2, _httpContextAccessor);
            _employmentService = new EmploymentService(_context2, _httpContextAccessor);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
            _socialSecurityService = new SocialSecurityService(_context2);
            _propertyService = new PropertyService(_context2);
            _otherIncomeService = new OtherIncomeService(_context2);
            _businessInterestService = new BusinessInterestService(_context2);
        }
        public async Task<IEnumerable<ClientRiskArc>> GetRiskArcDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType)
        {
            var clientRiskArcData = _context2.ClientRiskArcs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.ClientType == clientType);
            return await Task.FromResult(clientRiskArcData);
        }
        public long GetInvestmentNeededAtRetirement(int incomeNeededAtRetirement, int expectedTotalIncome)
        {
            double safeMoneyWithdrawalRate = 3f / 100;
            var result = Convert.ToInt64((incomeNeededAtRetirement - expectedTotalIncome) / safeMoneyWithdrawalRate).NagativeToZero();
            return result;
        }
        public double GetCurrentRoR(int totalPresentValue, int totalValueOfReturn)
        {
            var result = (Convert.ToDouble(totalValueOfReturn) / totalPresentValue) * 100;
            return !double.IsNaN(result) && !double.IsInfinity(result) ? result : 0;
        }
        public double GetFIS(int totalPresentValue, long investmentNeededAtRetirement)
        {
            double result;
            if (investmentNeededAtRetirement != 0)
                result = Convert.ToDouble(totalPresentValue) / investmentNeededAtRetirement * 100;
            else
                result = 100;
            return result;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        public string GetBitTypeDescription(RiskArcBitEnum riskArcBit)
        {
            string html = string.Empty;
            if (riskArcBit == RiskArcBitEnum.Accumulator)
                html = @$"<div style='margin-bottom:2px'>
                            <i>
                                Interested and engaged in wealth accumulation and confident in investing ability.
                            </i>
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Primary Decision-making process:</b> Emotional, sometimes appearing overconfident, with a desire to 
                                 heavily influence investment process.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Biases (Tendencies):</b> Overconfidence and Illusion of Control.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Profile:</b> Aggressive - Whether real or perceived, believe their active involvement in 
                                 investment decision-making puts them in control;deeply engaged in decision - making.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Tolerance:</b> High to very high.
                          </div>";
            else if (riskArcBit == RiskArcBitEnum.Independent)
                html = @$"<div style='margin-bottom:2px'>
                            <i>
                                Engaged in the investment process and opinionated on investment decisions.
                            </i>
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Primary Decision-making process:</b>
                              Analytical,understands potential disadvantages associated with doing one's own research,but secure in 
                              their conclusions.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Biases (Tendencies):</b>
                              Confirmation and Availability.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Profile:</b>
                              Growth-Gravitates toward evidence to support their beliefs while often ignoring conflicting information;
                              examples of things that come readily to mind are more representative than reality.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Tolerance:</b>
                              Generally above average but not as high aggressive investors.
                           </div>";
            else if (riskArcBit == RiskArcBitEnum.Follower)
                html = @$"<div style='margin-bottom:2px'>
                            <i>
                                General lack of interest in money and investing; typically desires guidance regarding financial 
                                decisions.
                            </i>
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Primary Decision-making process:</b>
                              Analytical, relating to following behavior (“herd mentality”).
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Biases (Tendencies):</b>
                              Recency and Framing.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Profile:</b>
                              Moderate - Makes decisions based on recent events and observations rather than considering the broader
                              context of their strategy.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Tolerance:</b>
                              Generally lower than average but often thinks risk tolerance is higher than it is.
                          </div>";
            else if (riskArcBit == RiskArcBitEnum.Preserver)
                html = @$"<div style='margin-bottom:2px'>
                            <i>
                                Loss averse and deliberate in decision-making.
                            </i>
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Primary Decision-making process:</b>
                              Emotional, relating to fear of losses and inability to make decisions/take action.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Biases (Tendencies):</b>
                              Loss Aversion and Status Quo.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Profile:</b>
                              Conservative - Wealth preservation first, growth second. Risk Tolerance: Generally lower than average.
                          </div>
                          <div style='margin-bottom:2px'>
                              <b>Risk Tolerance:</b>
                              High to very high.
                          </div>";
            return html;
        }
        public async Task<RiskArcReportModel> RiskArcReportData(string token, ClientTypeEnum clientType, int tz)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid ClientLoginGuid = new(token);
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            var clientData = client.ClientData;
            var userData = client.UserLogin.UserData;
            double point = 0, sumpoint = 0;
            string RiskProfile = string.Empty;
            string RiskCapReturnRange = string.Empty;
            int totalPoint = 0, totalFpoint = 0, totalIpoint = 0, totalApoint = 0, invTypepoint = 0;

            var tolerances = await GetRiskArcDataAsync(ClientLoginGuid, clientType);

            int[] PreserverData = { 16, 17, 20, 23, 28 };
            int[] FollowerData = { 19, 25, 29, 32, 35 };
            int[] IndependentData = { 21, 22, 26, 30, 34 };
            int[] AccumulatorData = { 18, 24, 27, 31, 33 };
            foreach (var item in tolerances)
            {
                invTypepoint = item.Answer == 1 ? 3 : item.Answer == 2 ? 1 : 0;
                if (PreserverData.Contains(item.SectionId))
                    totalPoint += invTypepoint;
                else if (FollowerData.Contains(item.SectionId))
                    totalFpoint += invTypepoint;
                else if (IndependentData.Contains(item.SectionId))
                    totalIpoint += invTypepoint;
                else if (AccumulatorData.Contains(item.SectionId))
                    totalApoint += invTypepoint;
            }
            IEnumerable<RiskArcBitData> BitData = new List<RiskArcBitData>();
            if (client.ClientData.RiskArcVersion)
            {
                var RateCountData = new List<KeyValuePair<string, int>>
            {
                new KeyValuePair<string, int>(RiskArcBitEnum.Preserver.ToString(), totalPoint),
                new KeyValuePair<string, int>(RiskArcBitEnum.Follower.ToString(), totalFpoint),
                new KeyValuePair<string, int>(RiskArcBitEnum.Independent.ToString(), totalIpoint),
                new KeyValuePair<string, int>(RiskArcBitEnum.Accumulator.ToString(), totalApoint)
            };
                RateCountData = RateCountData.OrderByDescending(m => m.Value).ToList();
                int maxPoint = RateCountData.FirstOrDefault().Value;
                int minPoint = maxPoint - 3;
                BitData = RateCountData.Select(m => new RiskArcBitData
                {
                    BitType = EnumHelper.ToEnum<RiskArcBitEnum>(m.Key),
                    Point = m.Value,
                    Active = ((double)m.Value).Between(minPoint, maxPoint) && m.Value > 0,
                    Description = GetBitTypeDescription(EnumHelper.ToEnum<RiskArcBitEnum>(m.Key))
                });

                RiskCapReturnRange = sumpoint > 19 && sumpoint < 47 ? RiskCapReturnRangeEnum.Range2_5To5_5.GetDisplayName() : sumpoint > 46 && sumpoint < 74 ? RiskCapReturnRangeEnum.Range5_6To8_5.GetDisplayName() : sumpoint > 73 && sumpoint < 101 ? RiskCapReturnRangeEnum.Range8_6.GetDisplayName() : string.Empty;
                var RCRStart = RiskCapReturnRange.Split(" to ")[0];
            }
            int[] includeAnswer = { 1, 12, 13, 14, 15 };
            foreach (var item in tolerances.Where(m => includeAnswer.Contains(m.SectionId)))
            {
                point = item.Answer == 1 ? 2.5 : item.Answer == 2 ? 5 : item.Answer == 3 ? 7.5 : item.Answer == 4 ? 10 : item.Answer == 5 ? 12.5 : 0;
                sumpoint += point;
            }
            RiskProfile = sumpoint.Between(20, 25) ? RiskProfileEnum.Conservative.GetDisplayName() : sumpoint.Between(26, 45) ? RiskProfileEnum.ModerateConservative.GetDisplayName() : sumpoint.Between(46, 65) ? RiskProfileEnum.Moderate.GetDisplayName() : sumpoint.Between(66, 85) ? RiskProfileEnum.ModerateAggressive.GetDisplayName() : sumpoint.Between(86, 100) ? RiskProfileEnum.Aggressive.GetDisplayName() : RiskProfileEnum.BelowConservative.GetDisplayName();

            var spouseFirstName = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate() && client.SpouseData != null ? client.SpouseData.FirstName : null;
            var firstName = clientType == ClientTypeEnum.Client ? clientData.FirstName : spouseFirstName;
            var spouseLastName = client.SpouseData is not null ? client.SpouseData.LastName : "";

            var suffix = clientData.RiskArcVersion ? "with Behavioral Tendencies" : "";
            string fileName = $"{firstName}'s RiskArc Report {suffix} {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
            var model = new RiskArcReportModel
            {
                LocalDateTime = DateTime.UtcNow.AddMinutes(-tz),
                FirstName = firstName,
                LastName = clientType == ClientTypeEnum.Client ? clientData.LastName : spouseLastName,
                RiskCapacity = sumpoint,
                RiskProfile = RiskProfile,
                BitData = BitData,
                FileName = fileName,
                RiskArcVersion = clientData.RiskArcVersion,
                ClientId = client.Id,
                RIADisclosure = userData.RIADisclosure
            };
            return model;
        }
        public async Task<ClientReportSummaryData> GetClientReportSummaryAsync(Guid ClientLoginGuid)
        {
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientData = clientLogin.ClientData;
            var spouseData = clientLogin.SpouseData;
            var planningItems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).Where(m => m.Active);
            bool hasInvestmentPlanning = planningItems.Any(m => m.PlanningItem.ItemName.Contains("Investment"));
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();

            AgeModel ageData = await _commonService.GetAgeData(ClientLoginGuid);
            FVTotalModel clientRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel clientPrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client);
            FVTotalModel clientBankCreditUnionFvDetails = new();
            FVTotalModel clientBrokerageFvDetails = new();
            FVTotalModel clientAnnuityContractFvDetails = new();
            FVTotalModel spouseRetirementPlanFvDetails = new();
            FVTotalModel spousePrevRetirementPlanFvDetails = new();
            FVTotalModel spouseBankCreditUnionFvDetails = new();
            FVTotalModel spouseBrokerageFvDetails = new();
            FVTotalModel spouseAnnuityContractFvDetails = new();

            int clientPersonalAssets = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Sum(m => m.AnnualIncome) ?? 0;
            int clientPersonalAssetsAtRetirement = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

            int clientPersonalActivityIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Sum(m => m.AnnualIncome) ?? 0;
            int clientPersonalActivityIncomeAtRetirement = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Client))?.Where(m => m.Years > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

            int spouseOccupationIncome = 0, spouseOccupationIncomeAtRetirement = 0,
                spousePersonalAssets = 0, spousePensionIncome = 0,
                spousePensionIncomeAtRetirement = 0, spousePersonalActivityIncome = 0,
                spousePersonalAssetsAtRetirement = 0, spousePersonalActivityIncomeAtRetirement = 0;

            int clientOccupationIncome = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
            int clientOccupationIncomeAtRetirement = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.RetirementAge)?.Sum(m => m.AnnualEarnings) ?? 0;

            var clientPensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Client);
            int clientPensionIncome = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            int clientPensionIncomeAtRetirement = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            IEnumerable<ClientEsp> spousePensionData = new List<ClientEsp>();
            if (isMarried)
            {
                spouseOccupationIncome = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseOccupationIncomeAtRetirement = (await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.RetirementAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse);

                spousePersonalAssets = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalAssetsAtRetirement = (await _propertyService.GetPropertyDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

                spousePersonalActivityIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalActivityIncomeAtRetirement = (await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse))?.Where(m => m.Years > ageData.YearToRetirement)?.Sum(m => m.AnnualIncome) ?? 0;

                spousePensionData = await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                spousePensionIncome = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
                spousePensionIncomeAtRetirement = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            }

            var budgetItems = await _budgetService.GetBudgetItemsAsync(ClientLoginGuid);
            FVTotalModel inflationaryFvDetails = clientData.BudgetVersion ? await _budgetService.GetInflationaryBudgetFutureValue(ClientLoginGuid) : await _budgetService.GetInfaltionaryBudgetFutureValueExpressAsync(ClientLoginGuid);
            var nonInflationary = clientData.BudgetVersion ? budgetItems.Where(m => !m.Inflationary && (m.YearsPayOff > ageData.YearToRetirement || !m.BudgetItem.IsRevolvingDebt))?.Sum(m => m.Amount) ?? 0 : clientData.NonInflationaryBudget;
            int clientSocialSecurityIncome = 0, spouseSocialSecurityIncome = 0, clientSocialSecurityIncomeAtRetirement = 0, spouseSocialSecurityIncomeAtRetirement = 0;
            int clientSocialSecurityAge = 0, spouseSocialSecurityAge = 0;
            var clientSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Client);

            if (clientSocialSecurity != null)
            {
                clientSocialSecurityAge = clientSocialSecurity.sSageBenifitsEnum;
                if (clientSocialSecurity.ssreceiving)
                {
                    clientSocialSecurityIncome = clientSocialSecurity.ssmonthly * 12;
                    clientSocialSecurityIncomeAtRetirement = clientSocialSecurity.ssmonthly * 12;
                }
                else
                {
                    clientSocialSecurityIncome = ageData.Client.Age >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                    clientSocialSecurityIncomeAtRetirement = ageData.Client.RetirementAge >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                }
            }

            if (isMarried && spouseData != null)
            {
                var spouseSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                if (spouseSocialSecurity != null)
                {
                    spouseSocialSecurityAge = spouseSocialSecurity.sSageBenifitsEnum;
                    if (spouseSocialSecurity.ssreceiving)
                    {
                        spouseSocialSecurityIncome = spouseSocialSecurity.ssmonthly * 12;
                        spouseSocialSecurityIncomeAtRetirement = spouseSocialSecurity.ssmonthly * 12;
                    }
                    else
                    {
                        spouseSocialSecurityIncome = ageData.Spouse.Age >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                        spouseSocialSecurityIncomeAtRetirement = ageData.Spouse.RetirementAge >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                    }
                }
            }

            if (clientData.InvestmentVersion)
            {
                var accountTypes = new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse };
                var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, bankingAccountTypes);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, accountTypes);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                if (isMarried)
                {
                    spouseBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, bankingAccountTypes);
                    spouseBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, accountTypes);
                    spouseAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(ClientLoginGuid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                }
            }
            else
            {
                clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryExpressFutureValueDetailsAsync(ClientLoginGuid);
                clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractExpressFutureValueDetailsAsync(ClientLoginGuid);
            }

            int totalValueOfReturn = clientRetirementPlanFvDetails.ValueOfReturn + spouseRetirementPlanFvDetails.ValueOfReturn
                + clientPrevRetirementPlanFvDetails.ValueOfReturn + spousePrevRetirementPlanFvDetails.ValueOfReturn
                + clientBankCreditUnionFvDetails.ValueOfReturn + spouseBankCreditUnionFvDetails.ValueOfReturn
                + clientBrokerageFvDetails.ValueOfReturn + spouseBrokerageFvDetails.ValueOfReturn
                + clientAnnuityContractFvDetails.ValueOfReturn + spouseAnnuityContractFvDetails.ValueOfReturn;

            int totalPresentValue = clientRetirementPlanFvDetails.PresentValue + spouseRetirementPlanFvDetails.PresentValue
                + clientPrevRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue
                + clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue
                + clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue
                + clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue;

            int clientInvestmentIncome = clientBankCreditUnionFvDetails.Income + clientBrokerageFvDetails.Income + clientAnnuityContractFvDetails.Income;
            int spouseInvestmentIncome = spouseBankCreditUnionFvDetails.Income + spouseBrokerageFvDetails.Income + spouseAnnuityContractFvDetails.Income;
            int totalPmtValue = clientRetirementPlanFvDetails.PmtValue + spouseRetirementPlanFvDetails.PmtValue +
                                clientBankCreditUnionFvDetails.PmtValue + spouseBankCreditUnionFvDetails.PmtValue +
                                clientBrokerageFvDetails.PmtValue + spouseBrokerageFvDetails.PmtValue +
                                clientAnnuityContractFvDetails.PmtValue + spouseAnnuityContractFvDetails.PmtValue;

            var totalIncomeAtRetirement = clientOccupationIncomeAtRetirement + spouseOccupationIncomeAtRetirement +
                                          clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                          clientPersonalActivityIncomeAtRetirement + spousePersonalActivityIncomeAtRetirement +
                                          clientPersonalAssetsAtRetirement + spousePersonalAssetsAtRetirement +
                                          clientInvestmentIncome + spouseInvestmentIncome +
                                          clientSocialSecurityIncomeAtRetirement + spouseSocialSecurityIncomeAtRetirement +
                                          clientPensionIncomeAtRetirement + spousePensionIncomeAtRetirement;
            var totalFutureValue = clientRetirementPlanFvDetails.FutureValue + spouseRetirementPlanFvDetails.FutureValue +
                                   clientPrevRetirementPlanFvDetails.FutureValue + spousePrevRetirementPlanFvDetails.FutureValue +
                                   clientBankCreditUnionFvDetails.FutureValue + spouseBankCreditUnionFvDetails.FutureValue +
                                   clientAnnuityContractFvDetails.FutureValue + spouseAnnuityContractFvDetails.FutureValue +
                                   clientBrokerageFvDetails.FutureValue + spouseBrokerageFvDetails.FutureValue;

            var investmentNeededAtRetirement = GetInvestmentNeededAtRetirement(totalIncomeAtRetirement, totalIncomeAtRetirement);
            var result = new ClientReportSummaryData
            {
                PresentValue = totalPresentValue,
                FutureValue = totalFutureValue,
                ValueOfReturn = totalValueOfReturn,
                PmtValue = totalPmtValue,
                IncomeNeededAtRetirement = totalIncomeAtRetirement,
                InvestmentNeededAtRetirement = Convert.ToInt32(investmentNeededAtRetirement)
            };
            return result;
        }
        public class ClientReportSummaryData
        {
            public int PresentValue { get; set; }
            public int FutureValue { get; set; }
            public int ValueOfReturn { get; set; }
            public int PmtValue { get; set; }
            public int IncomeNeededAtRetirement { get; set; }
            public int InvestmentNeededAtRetirement { get; set; }
        }
        ~ReportService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}