﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Entities.Master;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class CommonService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        private bool disposed;
        public CommonService(LifeArcPlanContext2 context2)
        {
            _context2 = context2;
        }
        public async Task ClearSpouseData(Guid ClientLoginGuid)
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            clientData.DateOfCurrentMarriage = null;
            var spouseData = await _context2.SpouseDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            if (spouseData is not null)
                _context2.SpouseDatas.Remove(spouseData);

            var type = ((int)ClientTypeEnum.Spouse).ToString();

            var spouseHeirData = _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.HeirOf == type);
            if (spouseHeirData is not null)
                _context2.Heirs.RemoveRange(spouseHeirData);

            var spousePropertyData = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Ownership == type);
            if (spousePropertyData is not null)
                _context2.ClientProperties.RemoveRange(spousePropertyData);

            var spouseOccupationData = _context2.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.EmploymentOf == ClientTypeEnum.Spouse);
            if (spouseOccupationData is not null)
                _context2.ClientOccupations.RemoveRange(spouseOccupationData);

            var spouseEspData = _context2.ClientEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.ClientOccupation.EmploymentOf == ClientTypeEnum.Spouse);
            if (spouseEspData is not null)
                _context2.ClientEsps.RemoveRange(spouseEspData);

            var spousePrevEspData = _context2.ClientPrevEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.EmploymentOf == ClientTypeEnum.Spouse);
            if (spousePrevEspData is not null)
                _context2.ClientPrevEsps.RemoveRange(spousePrevEspData);

            var businessinterestdata = _context2.BusinessInterests.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.businessInterestOf == ClientTypeEnum.Spouse);
            if (businessinterestdata is not null)
                _context2.BusinessInterests.RemoveRange(businessinterestdata);

            var otherincomedata = _context2.OtherIncomes.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.businessInterestof == ClientTypeEnum.Spouse);
            if (otherincomedata is not null)
                _context2.OtherIncomes.RemoveRange(otherincomedata);

            var socialSecurityEstimatedata = _context2.SocialSecurities.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.EmploymentOf == ClientTypeEnum.Spouse);
            if (socialSecurityEstimatedata is not null)
                _context2.SocialSecurities.RemoveRange(socialSecurityEstimatedata);

            var personalCaredata = _context2.PersonalCares.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.ClientType == ClientTypeEnum.Spouse);
            if (personalCaredata is not null)
                _context2.PersonalCares.RemoveRange(personalCaredata);

            var RetirementStatusData = _context2.RetirementStatuses.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.employeeOf == ClientTypeEnum.Spouse);
            if (RetirementStatusData is not null)
                _context2.RetirementStatuses.RemoveRange(RetirementStatusData);

            var BankCreditUnionAccounts = _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Owner == type);
            if (BankCreditUnionAccounts is not null)
                _context2.BankCreditUnionAccounts.RemoveRange(BankCreditUnionAccounts);

            var BrokerageAdvisoryAccounts = _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Owner == type);
            if (BrokerageAdvisoryAccounts is not null)
                _context2.BrokerageAdvisoryAccounts.RemoveRange(BrokerageAdvisoryAccounts);

            var LifeAnnuityContracts = _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Owner == type);
            if (LifeAnnuityContracts is not null)
                _context2.LifeAnnuityContracts.RemoveRange(LifeAnnuityContracts);



            var heirs = _context2.Heirs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.HeirOf.Contains(type) && m.HeirOf != type).ToList();
            heirs = heirs.Select(c => { c.HeirOf = c.HeirOf.Replace(type, "").Trim(','); return c; }).ToList();
            _context2.Heirs.UpdateRange(heirs);

            var properties = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Ownership.Contains(type) && m.Ownership != type).ToList();
            properties = properties.Select(c => { c.Ownership = c.Ownership.Replace(type, "").Trim(','); return c; }).ToList();
            _context2.ClientProperties.UpdateRange(properties);

            var businesses = _context2.BusinessInterests.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.FirmOwnershipEnum.Contains(type) && m.FirmOwnershipEnum != type).ToList();
            businesses = businesses.Select(c => { c.FirmOwnershipEnum = c.FirmOwnershipEnum.Replace(type, "").Trim(','); return c; }).ToList();
            _context2.BusinessInterests.UpdateRange(businesses);

            var bankcreditAccounts = _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Owner.Contains(type) && m.Owner != type).ToList();
            bankcreditAccounts = bankcreditAccounts.Select(c => { c.Owner = c.Owner.Replace(type, "").Trim(','); return c; }).ToList();
            _context2.BankCreditUnionAccounts.UpdateRange(bankcreditAccounts);

            var brokerageAccounts = _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Owner.Contains(type) && m.Owner != type).ToList();
            brokerageAccounts = brokerageAccounts.Select(c => { c.Owner = c.Owner.Replace(type, "").Trim(','); return c; }).ToList();
            _context2.BrokerageAdvisoryAccounts.UpdateRange(brokerageAccounts);

            var lifeannuityAccounts = _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.Owner.Contains(type) && m.Owner != type).ToList();
            lifeannuityAccounts = lifeannuityAccounts.Select(c => { c.Owner = c.Owner.Replace(type, "").Trim(','); return c; }).ToList();
            _context2.LifeAnnuityContracts.UpdateRange(lifeannuityAccounts);

            var clientIncomeConcern = _context2.ClientIncomeConcerns.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientIncomeConcern is not null)
            {
                clientIncomeConcern.ToList().ForEach(w => w.radioListforSpouseEnum = RadioListEnum.None);
                _context2.ClientIncomeConcerns.UpdateRange(clientIncomeConcern);
            }
            var ClientExpenseConcerns = _context2.ClientExpenseConcerns.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (ClientExpenseConcerns is not null)
            {
                ClientExpenseConcerns.ToList().ForEach(w => w.radioListforSpouseEnum = RadioListEnum.None);
                _context2.ClientExpenseConcerns.UpdateRange(ClientExpenseConcerns);
            }
            var healthassement = _context2.ClientHealthItems.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (healthassement is not null)
            {
                healthassement.ToList().ForEach(w => w.isSpouseActive = false);
                _context2.ClientHealthItems.UpdateRange(healthassement);
            }
            var Clientexpectation = _context2.ClientExpectations.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (Clientexpectation is not null)
            {
                Clientexpectation.ToList().ForEach(w => w.isSpouseActive = false);
                _context2.ClientExpectations.UpdateRange(Clientexpectation);
            }

        }
        public async Task ClearPlanningData(Guid ClientLoginGuid)
        {
            var clientPlannings = (await GetPlanningItemsAsync(ClientLoginGuid)).Where(m => !m.Active);
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Investment")))
            {
                var investmentLiteData = await _context2.InvestmentLiteDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
                if (investmentLiteData is not null)
                    _context2.InvestmentLiteDatas.Remove(investmentLiteData);

                var bankCreditUnionAccounts = await _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (bankCreditUnionAccounts is not null)
                    _context2.BankCreditUnionAccounts.RemoveRange(bankCreditUnionAccounts);
                var brokerageAdvisoryAccounts = await _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (brokerageAdvisoryAccounts is not null)
                    _context2.BrokerageAdvisoryAccounts.RemoveRange(brokerageAdvisoryAccounts);
                var lifeAnnuityContracts = await _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (lifeAnnuityContracts is not null)
                    _context2.LifeAnnuityContracts.RemoveRange(lifeAnnuityContracts);
            }
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Retirement Income")))
            {
                var retirementStatuses = await _context2.RetirementStatuses.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (retirementStatuses is not null)
                    _context2.RetirementStatuses.RemoveRange(retirementStatuses);
            }
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Healthcare")))
            {
                var clientHealth = await _context2.ClientHealthItems.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
                if (clientHealth is not null)
                    _context2.ClientHealthItems.Remove(clientHealth);
            }
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Personal Care")))
            {
                var personalCares = await _context2.PersonalCares.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (personalCares is not null)
                    _context2.PersonalCares.RemoveRange(personalCares);
            }
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Education")))
            {
                var educationPlannings = await _context2.EducationPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (educationPlannings is not null)
                    _context2.EducationPlannings.RemoveRange(educationPlannings);
            }
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Charitable")))
            {
                var charitablePlanning = await _context2.CharitablePlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
                if (charitablePlanning is not null)
                    _context2.CharitablePlannings.Remove(charitablePlanning);
            }
            if (clientPlannings.Any(m => m.PlanningItem.ItemName.Contains("Estate")))
            {
                var clientEstate = await _context2.ClientEstateItems.Where(m => m.ClientLogin.Guid == ClientLoginGuid).ToListAsync();
                if (clientEstate is not null)
                    _context2.ClientEstateItems.RemoveRange(clientEstate);
            }
            await _context2.SaveChangesAsync();
        }
        public async Task<IQueryable<ClientPlanning>> GetPlanningItemsAsync(Guid ClientLoginGuid)
        {
            var planningitem = _context2.PlanningItems.SelectMany
            (
                clientPlannning => _context2.ClientPlannings.Where(budgetItem => clientPlannning.Id == budgetItem.PlanningItemId && budgetItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty(),
                (planningitem, clientPlannning) => new ClientPlanning
                {
                    Id = clientPlannning != null ? clientPlannning.Id : 0,
                    Guid = clientPlannning != null ? clientPlannning.Guid : Guid.Empty,
                    ClientId = clientPlannning != null ? clientPlannning.ClientId : 0,
                    Importance = clientPlannning != null ? clientPlannning.Importance : new ImportanceEnum(),
                    PlanningItemId = planningitem.Id,
                    Active = clientPlannning != null,
                    PlanningItem = new PlanningItem
                    {
                        Id = planningitem.Id,
                        Guid = planningitem.Guid,
                        ItemName = planningitem.ItemName,
                        SortOrder = planningitem.SortOrder
                    }
                }
            );
            return await Task.FromResult(planningitem.OrderBy(m => m.PlanningItem.SortOrder));
        }
        public async Task<double?> GetYearToRetirementAsync(Guid ClientLoginGuid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).FirstOrDefaultAsync();
            var occupations = client.ClientOccupations;
            var clientOccupations = occupations.Where(m => m.EmploymentOf == ClientTypeEnum.Client && m.EmploymentStatus != EmploymentStatusEnum.HomeMaker);
            var spouseOccupations = occupations.Where(m => m.EmploymentOf == ClientTypeEnum.Spouse && m.EmploymentStatus != EmploymentStatusEnum.HomeMaker);

            int? clientAgeToRetire = clientOccupations.Any() ? clientOccupations.Min(m => m.AgeToRetire) : null;
            int? spouseAgeToRetire = spouseOccupations.Any() ? spouseOccupations.Min(m => m.AgeToRetire) : null;

            double? yearsToRetire = null;

            double? clientYearToRetirement = client.ClientData?.DateOfBirth.GetRemainYearToRetirement(clientAgeToRetire);
            double? spouseYearToRetirement = client.SpouseData?.DateOfBirth.GetRemainYearToRetirement(spouseAgeToRetire);
            yearsToRetire = clientYearToRetirement <= spouseYearToRetirement || spouseYearToRetirement == null ? clientYearToRetirement : spouseYearToRetirement;
            return yearsToRetire;
        }
        public async Task<AgeModel> GetAgeData(Guid ClientLoginGuid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).Include(m => m.SpouseData)
                .Include(m => m.ClientOccupations).FirstOrDefaultAsync();

            var clientData = client.ClientData;
            var spouseData = client.SpouseData;
            var occupations = client.ClientOccupations;
            var clientOccupations = occupations.Where(m => m.EmploymentOf == ClientTypeEnum.Client);
            var spouseOccupations = occupations.Where(m => m.EmploymentOf == ClientTypeEnum.Spouse);

            double spouseAge = 0;
            int? spouseRetirementAge = null, spouseActualRetirementAge = null, spouseYearToRetirement = null;

            double clientAge = clientData.DateOfBirth.CalculateAge();
            double? yearToRetirement = await GetYearToRetirementAsync(ClientLoginGuid);
            int? clientRetirementAge = Convert.ToInt32(clientAge + yearToRetirement);
            int? clientActualRetirementAge = clientOccupations.Any(m => m.EmploymentStatus != EmploymentStatusEnum.HomeMaker) ? clientOccupations.Where(m => m.EmploymentStatus != EmploymentStatusEnum.HomeMaker).Min(m => m.AgeToRetire) : null;
            bool isClientRetired = clientOccupations.Any(m => m.EmploymentStatus == EmploymentStatusEnum.Retired || m.EmploymentStatus == EmploymentStatusEnum.HomeMaker) || clientAge >= clientActualRetirementAge;
            bool isSpouseRetired = false;
            int? clientYearToRetirement = clientData.DateOfBirth.GetRemainYearToRetirement(clientActualRetirementAge);
            if (spouseData != null)
            {
                spouseAge = spouseData.DateOfBirth.CalculateAge();
                spouseRetirementAge = Convert.ToInt32(spouseAge + yearToRetirement);
                spouseActualRetirementAge = spouseOccupations.Any(m => m.EmploymentStatus != EmploymentStatusEnum.HomeMaker) ? spouseOccupations.Where(m => m.EmploymentStatus != EmploymentStatusEnum.HomeMaker).Min(m => m.AgeToRetire) : null;
                isSpouseRetired = !spouseOccupations.Any() || spouseOccupations.Any(m => m.EmploymentStatus == EmploymentStatusEnum.Retired || m.EmploymentStatus == EmploymentStatusEnum.HomeMaker) || spouseAge >= spouseActualRetirementAge;
                spouseYearToRetirement = spouseData.DateOfBirth.GetRemainYearToRetirement(spouseActualRetirementAge);
            }
            return new AgeModel
            {
                YearToRetirement = yearToRetirement,
                Client = new AgeDataModel
                {
                    IsRetired = isClientRetired,
                    Age = clientAge,
                    RetirementAge = clientRetirementAge,
                    ActualRetirementAge = clientActualRetirementAge,
                    YearToRetirement = clientYearToRetirement,
                },
                Spouse = new AgeDataModel
                {
                    IsRetired = isSpouseRetired,
                    Age = spouseAge,
                    RetirementAge = spouseRetirementAge,
                    ActualRetirementAge = spouseActualRetirementAge,
                    YearToRetirement = spouseYearToRetirement,
                }
            };
        }
        public async Task<PrevNextMenuModel> GetPlanningPrevNextAsync(string action, Guid ClientLoginGuid)
        {
            var clientData = _context2.ClientDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var planningItemIds = _context2.ClientPlannings.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Select(m => m.PlanningItemId);
            var menuItems = await MenuDataHelper.GetMenuDataAsync();
            var Ids = menuItems.Where(m => m.ParentId == 4).Select(m => m.Id);
            var items = menuItems.Where(m => Ids.Contains(m.ParentId) && planningItemIds.Contains(m.PlanningItemId)).ToList();
            int index = items.FindIndex(m => m.Action == action);
            var prevAction = index != 0 ? items[index - 1] : new MenuModel { Title = "Budget Info", Action = "budget-info", Controller = "expense" };
            var nextAction = index != (items.AsEnumerable().Count() - 1) ? items[index + 1] : new MenuModel { Title = "Introduction", Action = "life-style-intro", Controller = "LifeStyle" };
            var arrow = "<i class='flaticon2-right-arrow icon-nm'></i>";
            var nextArrow = nextAction.Controller.Contains("Planning") ? arrow : "";
            string[] splitPrevController = Regex.Split(prevAction.Controller, @"(?<!^)(?=[A-Z])").Where(item => item != "Planning").ToArray();
            string[] splitNextController = Regex.Split(nextAction.Controller, @"(?<!^)(?=[A-Z])").Where(item => item != "Planning").ToArray();
            bool isExpress = !clientData.BudgetVersion;
            var budgetText = isExpress ? " Express" : "";
            var budgetAction = isExpress ? "-express" : "";
            var result = new PrevNextMenuModel
            {
                PrevTitle = prevAction.Action != "budget-info" ? $"{string.Join(" ", splitPrevController)} {arrow} {prevAction.Title.Replace("Introduction", "Intro")}" : $"Budget Info{budgetText}",
                PrevAction = $"{prevAction.Controller}/{prevAction.Action}",
                NextTitle = $"{string.Join(" ", splitNextController)} {nextArrow} {nextAction.Title.Replace("Introduction", "Intro")}",
                NextAction = $"{nextAction.Controller}/{nextAction.Action}"
            };
            if (result.PrevAction.Contains("life-annuity-contracts"))
            {
                result.PrevTitle = clientData.InvestmentVersion ? result.PrevTitle : "Investment Planning Express";
                result.PrevAction = clientData.InvestmentVersion ? result.PrevAction : "investmentplanning/investment-planning-express";
            }
            else if (result.NextAction.Contains("investment-planning-intro"))
            {
                result.NextTitle = clientData.InvestmentVersion ? result.NextTitle : "Investment Planning Express";
                result.NextAction = clientData.InvestmentVersion ? result.NextAction : "investmentplanning/investment-planning-express";
            }
            return result;
        }
        public IEnumerable<RiskArcQuestionModel> GetRiskArcQuestionsWithAnswers(Guid ClientLoginGuid, ClientTypeEnum RiskArcClientType)
        {
            var riskArcQuestions = _context2.RiskArcItems;
            var clientRikArcItems = _context2.ClientRiskArcs.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.ClientType == RiskArcClientType);

            var clientRiskArcAnswers = from c in riskArcQuestions
                                       join p in clientRikArcItems
                                       on c.Id equals p.SectionId into ps
                                       from p in ps.DefaultIfEmpty()
                                       select new RiskArcQuestionModel
                                       {
                                           Guid = p != null ? p.Guid : Guid.Empty,
                                           Answer = p != null ? p.Answer : 0,
                                           Id = p != null ? p.Id : 0,
                                           SectionId = c.Id,
                                           Question = c.Question,
                                           Options = c.Options
                                       };
            return clientRiskArcAnswers;
        }
        public async Task<IEnumerable<RegulatoryQuestionModel>> GetRegulatoryQuestionsAsync(Guid UserLoginGuid, Guid Guid = default)
        {
            UserLoginGuid = UserLoginGuid != Guid.Empty ? UserLoginGuid : Guid;
            var regulatoryQuestions = (await MenuDataHelper.GetRegulatoryQuestionDataAsync()).Where(m => m.ParentId > 0);
            var adviserRegulatoryItems = _context2.Regulatories.Where(m => m.UserLogin.Guid == UserLoginGuid).AsNoTracking();

            var regulatoryAnswers = from c in regulatoryQuestions
                                    join p in adviserRegulatoryItems
                                    on c.SectionId equals p.SectionId into ps
                                    from p in ps.DefaultIfEmpty()
                                    select new RegulatoryQuestionModel
                                    {
                                        Guid = p is not null ? p.Guid : Guid.Empty,
                                        Answer = p is not null && p.Answer,
                                        RegulatoryId = p is not null ? p.Id : 0,
                                        SectionId = c.SectionId,
                                        ParentId = c.ParentId,
                                        Question = c.Question
                                    };
            return regulatoryAnswers;
        }
        public bool IsExistUser(Guid guid)
        {
            return _context2.UserLogins.Any(m => m.Guid == guid && m.Active == true);
        }
        public DateTime? GetLastLogin(Guid guid)
        {
            var user = _context2.UserLogins.FirstOrDefault(m => m.Guid == guid && m.Active == true);
            return user?.LastLogin;
        }
        public bool IsExistClient(Guid guid)
        {
            return _context2.ClientLogins.Any(m => m.Guid == guid);
        }
        public async Task UpdateLastAccessPage(Guid ClientLoginGuid, string action)
        {
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefaultAsync();
            clientData.LastAccessPage = $"/{action}";
            await _context2.SaveChangesAsync();
        }
        public async Task<IEnumerable<ClientHealthItem>> GetHealthAsync(Guid ClientLoginGuid)
        {
            var clientHealths = _context2.DescriptionItems.SelectMany
            (
                clientHealth => _context2.ClientHealthItems.Where(healthItem => clientHealth.Id == healthItem.DescriptionItemId && healthItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty(),
                (healthItem, clientHealth) => new ClientHealthItem
                {
                    Id = clientHealth != null ? clientHealth.Id : 0,
                    Guid = clientHealth != null ? clientHealth.Guid : Guid.Empty,
                    ClientId = clientHealth != null ? clientHealth.ClientId : 0,
                    isClientActive = clientHealth != null && clientHealth.isClientActive,
                    isSpouseActive = clientHealth != null && clientHealth.isSpouseActive,
                    DescriptionItemId = healthItem.Id,
                    DescriptionItems = new DescriptionItem
                    {
                        Guid = healthItem.Guid,
                        Id = healthItem.Id,
                        tabletype = healthItem.tabletype,
                        itemDesc = healthItem.itemDesc,
                        sortOrder = healthItem.sortOrder
                    }
                }
            );
            clientHealths = clientHealths.Where(m => m.DescriptionItems.tabletype == DescritpionTableNameEnum.Health);
            return await Task.FromResult(clientHealths.OrderBy(m => m.DescriptionItems.sortOrder));
        }
        public async Task<IEnumerable<ClientExpectation>> GetExpectationItemsAsync(Guid ClientLoginGuid)
        {
            var includeExpectation = new DescritpionTableNameEnum[] {
                DescritpionTableNameEnum.WorkExpectations,DescritpionTableNameEnum.ResidenceExpectations,
                DescritpionTableNameEnum.LifeStyleExpectations,DescritpionTableNameEnum.CommunityExpectations,
                DescritpionTableNameEnum.DependentCare
            };
            var clientExpectations = _context2.DescriptionItems.SelectMany
            (
                clientExpectation => _context2.ClientExpectations.Where(healthItem => clientExpectation.Id == healthItem.DescriptionItemId && healthItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty(),
                (expectationItem, clientExpectation) => new ClientExpectation
                {
                    Id = clientExpectation != null ? clientExpectation.Id : 0,
                    Guid = clientExpectation != null ? clientExpectation.Guid : Guid.Empty,
                    ClientId = clientExpectation == null ? 0 : clientExpectation.ClientId,
                    isClientActive = clientExpectation != null && clientExpectation.isClientActive,
                    isSpouseActive = clientExpectation != null && clientExpectation.isSpouseActive,
                    DescriptionItemId = expectationItem.Id,
                    DescriptionItems = new DescriptionItem
                    {
                        Guid = expectationItem.Guid,
                        Id = expectationItem.Id,
                        tabletype = expectationItem.tabletype,
                        itemDesc = expectationItem.itemDesc,
                        sortOrder = expectationItem.sortOrder
                    }
                }
            );
            clientExpectations = clientExpectations.Where(m => includeExpectation.Contains(m.DescriptionItems.tabletype));
            return await Task.FromResult(clientExpectations.OrderBy(m => m.DescriptionItems.sortOrder));
        }
        public async Task<IEnumerable<ClientIncomeConcern>> GetIncomeConcernAsync(Guid ClientLoginGuid)
        {
            var clientIncomeConcern = _context2.DescriptionItems.SelectMany
            (
                clientIncomeConcern => _context2.ClientIncomeConcerns.Where(healthItem => clientIncomeConcern.Id == healthItem.DescriptionItemId && healthItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty(),
                (incomeConcern, clientIncomeConcern) => new ClientIncomeConcern
                {
                    Id = clientIncomeConcern != null ? clientIncomeConcern.Id : 0,
                    Guid = clientIncomeConcern != null ? clientIncomeConcern.Guid : Guid.Empty,
                    ClientId = clientIncomeConcern != null ? clientIncomeConcern.ClientId : 0,
                    radioListforClientEnum = clientIncomeConcern != null ? clientIncomeConcern.radioListforClientEnum : default,
                    radioListforSpouseEnum = clientIncomeConcern != null ? clientIncomeConcern.radioListforSpouseEnum : default,
                    DescriptionItemId = incomeConcern.Id,
                    DescriptionItems = new DescriptionItem
                    {
                        Guid = incomeConcern.Guid,
                        Id = incomeConcern.Id,
                        tabletype = incomeConcern.tabletype,
                        itemDesc = incomeConcern.itemDesc,
                        sortOrder = incomeConcern.sortOrder
                    }
                }
            );
            clientIncomeConcern = clientIncomeConcern.Where(m => m.DescriptionItems.tabletype == DescritpionTableNameEnum.IncomeConcerns);
            return await Task.FromResult(clientIncomeConcern.OrderBy(m => m.DescriptionItems.sortOrder));
        }
        public async Task<IEnumerable<ClientExpenseConcern>> GetExpenseConcernAsync(Guid ClientLoginGuid)
        {
            var clientExpenseConcern = _context2.DescriptionItems.SelectMany
            (
                clientExpenseConcern => _context2.ClientExpenseConcerns.Where(healthItem => clientExpenseConcern.Id == healthItem.DescriptionItemId && healthItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty(),
                (expenseConcern, clientExpenseConcern) => new ClientExpenseConcern
                {
                    Id = clientExpenseConcern != null ? clientExpenseConcern.Id : 0,
                    Guid = clientExpenseConcern != null ? clientExpenseConcern.Guid : Guid.Empty,
                    ClientId = clientExpenseConcern != null ? clientExpenseConcern.ClientId : 0,
                    radioListforClientEnum = clientExpenseConcern != null ? clientExpenseConcern.radioListforClientEnum : default,
                    radioListforSpouseEnum = clientExpenseConcern != null ? clientExpenseConcern.radioListforSpouseEnum : default,
                    DescriptionItemId = expenseConcern.Id,
                    DescriptionItems = new DescriptionItem
                    {
                        Guid = expenseConcern.Guid,
                        Id = expenseConcern.Id,
                        tabletype = expenseConcern.tabletype,
                        itemDesc = expenseConcern.itemDesc,
                        sortOrder = expenseConcern.sortOrder
                    }
                }
            );
            clientExpenseConcern = clientExpenseConcern.Where(m => m.DescriptionItems.tabletype == DescritpionTableNameEnum.ExpenseConcerns);
            return await Task.FromResult(clientExpenseConcern.OrderBy(m => m.DescriptionItems.sortOrder));
        }
        public async Task<IEnumerable<ClientEstateItem>> GetEstateItemAsync(Guid ClientLoginGuid)
        {
            var clientEstate = _context2.DescriptionItems.SelectMany
            (
                clientEstate => _context2.ClientEstateItems.Where(healthItem => clientEstate.Id == healthItem.DescriptionItemId && healthItem.ClientLogin.Guid == ClientLoginGuid).DefaultIfEmpty(),
                (estate, clientEstate) => new ClientEstateItem
                {
                    Id = clientEstate != null ? clientEstate.Id : 0,
                    Guid = clientEstate != null ? clientEstate.Guid : Guid.Empty,
                    ClientId = clientEstate != null ? clientEstate.ClientId : 0,
                    isClientActive = clientEstate != null && clientEstate.isClientActive,
                    isSpouseActive = clientEstate != null && clientEstate.isSpouseActive,
                    DescriptionItemId = estate.Id,
                    DescriptionItem = new DescriptionItem
                    {
                        Guid = estate.Guid,
                        Id = estate.Id,
                        tabletype = estate.tabletype,
                        itemDesc = estate.itemDesc,
                        sortOrder = estate.sortOrder
                    }
                }
            );
            clientEstate = clientEstate.Where(m => m.DescriptionItem.tabletype == DescritpionTableNameEnum.Estate);
            return await Task.FromResult(clientEstate.OrderBy(m => m.DescriptionItem.sortOrder));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~CommonService()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}