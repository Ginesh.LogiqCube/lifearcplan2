﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class SocialSecurityService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        private bool disposed;

        public SocialSecurityService(LifeArcPlanContext2 context2)
        {
            _context2 = context2;
        }
        public async Task<SocialSecurity> GetSocialSecurityDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType)
        {
            return await _context2.SocialSecurities.Where(m => m.ClientLogin.Guid == ClientLoginGuid && m.EmploymentOf == clientType).FirstOrDefaultAsync();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~SocialSecurityService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}