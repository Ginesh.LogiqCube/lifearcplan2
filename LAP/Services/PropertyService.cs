﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class PropertyService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        private bool disposed;

        public PropertyService(LifeArcPlanContext2 context2)
        {
            _context2 = context2;
        }
        public async Task<IEnumerable<ClientProperty>> GetPropertyDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, PropertyTypeEnum[] excludePropertyTypes = null)
        {
            var clientPropertyData = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
            {
                if (clientType == ClientTypeEnum.Spouse)
                    clientPropertyData = clientPropertyData.Where(m => m.Ownership == ((int)ClientTypeEnum.Spouse).ToString());
                else
                    clientPropertyData = clientPropertyData.Where(m => m.Ownership != ((int)ClientTypeEnum.Spouse).ToString());
            }
            
            if (excludePropertyTypes != null)
                clientPropertyData = clientPropertyData.Where(m => !excludePropertyTypes.Contains(m.Type));
            return await Task.FromResult(clientPropertyData);
        }
        public async Task<int> GetPropertyFutureValueAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, PropertyTypeEnum[] excludePropertyTypes = null)
        {
            var clientPropertyData = (await GetPropertyDataAsync(ClientLoginGuid, clientType, excludePropertyTypes))
                .Select(m => new
                {
                    FutureValue = m.ProjectedFairMarketValueRetirement * (m.PercentageOfOwnership / 100)
                });
            decimal? result = clientPropertyData.Sum(m => m.FutureValue);
            return Convert.ToInt32(result).NagativeToZero();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~PropertyService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
