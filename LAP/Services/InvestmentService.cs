﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class InvestmentService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly CommonService _commonService;
        private bool disposed;
        public InvestmentService(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _commonService = new CommonService(_context2);
        }
        #region Bank Credit Union
        public async Task<IEnumerable<BankCreditUnionAccount>> GetBankCreditUnionDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, BankCreditUnionAccountTypeEnum[] accountTypes = null, TaxClassificationEnum[] taxClassifications = null)
        {
            var clientBankCreditUnionData = _context2.BankCreditUnionAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
            {
                if (clientType == ClientTypeEnum.Spouse)
                    clientBankCreditUnionData = clientBankCreditUnionData.Where(m => m.Owner == ((int)ClientTypeEnum.Spouse).ToString());
                else
                    clientBankCreditUnionData = clientBankCreditUnionData.Where(m => m.Owner != ((int)ClientTypeEnum.Spouse).ToString());
            }
            if (accountTypes is not null)
                clientBankCreditUnionData = clientBankCreditUnionData.Where(m => accountTypes.Contains(m.AccountType));
            if (taxClassifications is not null)
                clientBankCreditUnionData = clientBankCreditUnionData.Where(m => taxClassifications.Contains(m.TaxClassification));

            return await Task.FromResult(clientBankCreditUnionData);
        }
        public async Task<FVTotalModel> GetBankCreditUnionFutureValueDetailsAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, BankCreditUnionAccountTypeEnum[] accountTypes = null, TaxClassificationEnum[] taxClassifications = null)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var clientBankCreditUnionData = (await GetBankCreditUnionDataAsync(ClientLoginGuid, clientType, accountTypes, taxClassifications))
                .Select(m => new FVDetailModel
                {
                    Contribution = Convert.ToInt32(m.AnnualContribtion),
                    PresentValue = m.CurrentBalance,
                    Income = m.AnnualIncome,
                    RateOfInterest = Convert.ToDouble(m.AvgRateReturn),
                    FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.AvgRateReturn), yearToRetire, m.CurrentBalance, m.AnnualIncome, m.AnnualContribtion),
                    Year28FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.AvgRateReturn), null, m.CurrentBalance, m.AnnualIncome, m.AnnualContribtion),
                });
            return new FVTotalModel
            {
                PresentValue = clientBankCreditUnionData.Any() ? clientBankCreditUnionData.Sum(m => m.PresentValue) : 0,
                Income = clientBankCreditUnionData.Any() ? clientBankCreditUnionData.Sum(m => m.Income) : 0,
                PmtValue = clientBankCreditUnionData.Any() ? clientBankCreditUnionData.Sum(m => m.PmtValue) : 0,
                FutureValue = clientBankCreditUnionData.Any() ? clientBankCreditUnionData.Sum(m => m.FutureValue) : 0,
                Year28FutureValue = clientBankCreditUnionData.Any() ? clientBankCreditUnionData.Sum(m => m.Year28FutureValue) : 0,
                ValueOfReturn = clientBankCreditUnionData.Any() ? clientBankCreditUnionData.Sum(m => m.ValueOfReturn) : 0
            };
        }
        public async Task<FVTotalModel> GetBankCreditUnionExpressFutureValueDetailsAsync(Guid ClientLoginGuid)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var investmentData = _context2.InvestmentLiteDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var qualifiedBankCreditUnionValueOfReturn = investmentData?.BankInflationaryInvestmentAmount * (investmentData?.BankInflationaryRateOfReturn / 100) ?? 0;
            var nonqualifiedBankCreditUnionValueOfReturn = investmentData?.BankNonInflationaryInvestmentAmount * (investmentData?.BankNonInflationaryRateOfReturn / 100) ?? 0;
            int qualifiedBankCreditUnionFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BankInflationaryRateOfReturn), yearToRetire, investmentData?.BankInflationaryInvestmentAmount);
            int nonqualifiedBankCreditUnionFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BankNonInflationaryRateOfReturn), yearToRetire, investmentData?.BankNonInflationaryInvestmentAmount);
            int qualifiedBankCreditUnion28FV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BankInflationaryRateOfReturn), null, investmentData?.BankInflationaryInvestmentAmount);
            int nonqualifiedBankCreditUnion28FV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BankNonInflationaryRateOfReturn), null, investmentData?.BankNonInflationaryInvestmentAmount);

            return new FVTotalModel
            {
                PresentValue = Convert.ToInt32(investmentData?.BankInflationaryInvestmentAmount + investmentData?.BankNonInflationaryInvestmentAmount),
                ValueOfReturn = Convert.ToInt32(qualifiedBankCreditUnionValueOfReturn + nonqualifiedBankCreditUnionValueOfReturn),
                FutureValue = Convert.ToInt32(qualifiedBankCreditUnionFV + nonqualifiedBankCreditUnionFV),
                Year28FutureValue = Convert.ToInt32(qualifiedBankCreditUnion28FV + nonqualifiedBankCreditUnion28FV)
            };
        }
        #endregion
        #region Brokerage Advisory
        public async Task<IEnumerable<BrokerageAdvisoryAccount>> GetBrokerageAdvisoryDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, BrokerageAdvisoryAccountTypeEnum[] accountTypes = null, TaxClassificationEnum[] taxClassifications = null)
        {
            var clientBrokerageAdvisoryData = _context2.BrokerageAdvisoryAccounts.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
            {
                if (clientType == ClientTypeEnum.Spouse)
                    clientBrokerageAdvisoryData = clientBrokerageAdvisoryData.Where(m => m.Owner == ((int)ClientTypeEnum.Spouse).ToString());
                else
                    clientBrokerageAdvisoryData = clientBrokerageAdvisoryData.Where(m => m.Owner != ((int)ClientTypeEnum.Spouse).ToString());
            }
            if (accountTypes is not null)
                clientBrokerageAdvisoryData = clientBrokerageAdvisoryData.Where(m => accountTypes.Contains(m.AccountType));

            if (taxClassifications is not null)
                clientBrokerageAdvisoryData = clientBrokerageAdvisoryData.Where(m => taxClassifications.Contains(m.TaxClassification));
            return await Task.FromResult(clientBrokerageAdvisoryData);
        }
        public async Task<FVTotalModel> GetBrokerageAdvisoryFutureValueDetailsAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, BrokerageAdvisoryAccountTypeEnum[] accountTypes = null, TaxClassificationEnum[] taxClassification = null)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var clientBrokerageAdvisoryData = (await GetBrokerageAdvisoryDataAsync(ClientLoginGuid, clientType, accountTypes, taxClassification))
                .Select(m => new FVDetailModel
                {
                    Contribution = Convert.ToInt32(m.AnnualContribtion),
                    PresentValue = m.CurrentBalance,
                    Income = m.AnnualIncome,
                    RateOfInterest = Convert.ToDouble(m.AvgRateReturn),
                    FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.AvgRateReturn), yearToRetire, m.CurrentBalance, m.AnnualIncome, m.AnnualContribtion),
                    Year28FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.AvgRateReturn), null, m.CurrentBalance, m.AnnualIncome, m.AnnualContribtion),
                });
            return new FVTotalModel
            {
                PresentValue = clientBrokerageAdvisoryData.Any() ? clientBrokerageAdvisoryData.Sum(m => m.PresentValue) : 0,
                Income = clientBrokerageAdvisoryData.Any() ? clientBrokerageAdvisoryData.Sum(m => m.Income) : 0,
                PmtValue = clientBrokerageAdvisoryData.Any() ? clientBrokerageAdvisoryData.Sum(m => m.PmtValue) : 0,
                FutureValue = clientBrokerageAdvisoryData.Any() ? clientBrokerageAdvisoryData.Sum(m => m.FutureValue) : 0,
                Year28FutureValue = clientBrokerageAdvisoryData.Any() ? clientBrokerageAdvisoryData.Sum(m => m.Year28FutureValue) : 0,
                ValueOfReturn = clientBrokerageAdvisoryData.Any() ? clientBrokerageAdvisoryData.Sum(m => m.ValueOfReturn) : 0
            };
        }
        public async Task<FVTotalModel> GetBrokerageAdvisoryExpressFutureValueDetailsAsync(Guid ClientLoginGuid)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var investmentData = _context2.InvestmentLiteDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var qualifiedBrokerageAdvisoryValueOfReturn = investmentData?.BrokerageInflationaryInvestmentAmount * (investmentData?.BrokerageInflationaryRateOfReturn / 100) ?? 0;
            var nonqualifiedBrokerageAdvisoryValueOfReturn = investmentData?.BrokerageNonInflationaryInvestmentAmount * (investmentData?.BrokerageNonInflationaryRateOfReturn / 100) ?? 0;
            int qualifiedBrokerageAdvisoryFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BrokerageInflationaryRateOfReturn), yearToRetire, investmentData?.BrokerageInflationaryInvestmentAmount);
            int nonqualifiedBrokerageAdvisoryFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BrokerageNonInflationaryRateOfReturn), yearToRetire, investmentData?.BrokerageNonInflationaryInvestmentAmount);
            int qualifiedBrokerageAdvisory28FV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BrokerageInflationaryRateOfReturn), null, investmentData?.BrokerageInflationaryInvestmentAmount);
            int nonqualifiedBrokerageAdvisory28FV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BrokerageNonInflationaryRateOfReturn), null, investmentData?.BrokerageNonInflationaryInvestmentAmount);
            return new FVTotalModel
            {
                PresentValue = Convert.ToInt32(investmentData?.BrokerageInflationaryInvestmentAmount + investmentData?.BrokerageNonInflationaryInvestmentAmount),
                ValueOfReturn = Convert.ToInt32(qualifiedBrokerageAdvisoryValueOfReturn + nonqualifiedBrokerageAdvisoryValueOfReturn),
                FutureValue = Convert.ToInt32(qualifiedBrokerageAdvisoryFV + nonqualifiedBrokerageAdvisoryFV),
                Year28FutureValue = Convert.ToInt32(qualifiedBrokerageAdvisory28FV + nonqualifiedBrokerageAdvisory28FV)
            };
        }
        #endregion
        #region Life Annuity
        public async Task<IEnumerable<LifeAnnuityContract>> GetLifeAnnuityContractDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, ContractTypeEnum[] contractTypes = null, TaxClassificationEnum[] taxClassifications = null)
        {
            var clientLifeAnnuityContractData = _context2.LifeAnnuityContracts.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
            {
                if (clientType == ClientTypeEnum.Spouse)
                    clientLifeAnnuityContractData = clientLifeAnnuityContractData.Where(m => m.Owner == ((int)ClientTypeEnum.Spouse).ToString());
                else
                    clientLifeAnnuityContractData = clientLifeAnnuityContractData.Where(m => m.Owner != ((int)ClientTypeEnum.Spouse).ToString());
            }
            if (contractTypes is not null)
                clientLifeAnnuityContractData = clientLifeAnnuityContractData.Where(m => contractTypes.Contains(m.ContractType));
            if (taxClassifications is not null)
                clientLifeAnnuityContractData = clientLifeAnnuityContractData.Where(m => taxClassifications.Contains(m.TaxClassification));
            return await Task.FromResult(clientLifeAnnuityContractData);
        }
        public async Task<FVTotalModel> GetLifeAnnuityContractFutureValueDetailsAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default, ContractTypeEnum[] contractTypes = null, TaxClassificationEnum[] taxClassification = null)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var clientLifeAnnuityData = (await GetLifeAnnuityContractDataAsync(ClientLoginGuid, clientType, contractTypes, taxClassification))
                .Select(m => new FVDetailModel
                {
                    Contribution = Convert.ToInt32(m.AnnualContribution),
                    PresentValue = m.ContractType == ContractTypeEnum.Annuity ? m.CurrentBalance : m.ContractFaceAmount,
                    Income = m.AnnualIncome,
                    RateOfInterest = Convert.ToDouble(m.RateOfReturn),
                    FutureValue = m.ContractType == ContractTypeEnum.Annuity ? ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.RateOfReturn), yearToRetire, m.CurrentBalance, m.AnnualIncome, m.AnnualContribution) : m.ContractFaceAmount,
                    Year28FutureValue = m.ContractType == ContractTypeEnum.Annuity ? ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.RateOfReturn), null, m.CurrentBalance, m.AnnualIncome, m.AnnualContribution) : m.ContractFaceAmount,
                });
            return new FVTotalModel
            {
                PresentValue = clientLifeAnnuityData.Any() ? clientLifeAnnuityData.Sum(m => m.PresentValue) : 0,
                Income = clientLifeAnnuityData.Any() ? clientLifeAnnuityData.Sum(m => m.Income) : 0,
                PmtValue = clientLifeAnnuityData.Any() ? clientLifeAnnuityData.Sum(m => m.PmtValue) : 0,
                FutureValue = clientLifeAnnuityData.Any() ? clientLifeAnnuityData.Sum(m => m.FutureValue) : 0,
                Year28FutureValue = clientLifeAnnuityData.Any() ? clientLifeAnnuityData.Sum(m => m.Year28FutureValue) : 0,
                ValueOfReturn = clientLifeAnnuityData.Any() ? clientLifeAnnuityData.Sum(m => m.ValueOfReturn) : 0
            };
        }
        public async Task<FVTotalModel> GetLifeAnnuityContractExpressFutureValueDetailsAsync(Guid ClientLoginGuid)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var investmentData = _context2.InvestmentLiteDatas.FirstOrDefault(m => m.ClientLogin.Guid == ClientLoginGuid);
            var qualifiedLifeAnnuityValueOfReturn = investmentData?.AnnuityInflationaryInvestmentAmount * (investmentData?.AnnuityInflationaryRateOfReturn / 100) ?? 0;
            var nonqualifiedLifeAnnuityValueOfReturn = investmentData?.AnnuityNonInflationaryInvestmentAmount * (investmentData?.AnnuityNonInflationaryRateOfReturn / 100) ?? 0;
            int qualifiedLifeAnnuityFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.AnnuityInflationaryRateOfReturn), yearToRetire, investmentData?.AnnuityInflationaryInvestmentAmount);
            int nonqualifiedLifeAnnuityFV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.AnnuityNonInflationaryRateOfReturn), yearToRetire, investmentData?.AnnuityNonInflationaryInvestmentAmount);
            int qualifiedLifeAnnuity28FV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.AnnuityInflationaryRateOfReturn), null, investmentData?.AnnuityInflationaryInvestmentAmount);
            int nonqualifiedLifeAnnuity28FV = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.AnnuityNonInflationaryRateOfReturn), null, investmentData?.AnnuityNonInflationaryInvestmentAmount);
            return new FVTotalModel
            {
                PresentValue = Convert.ToInt32(investmentData?.AnnuityInflationaryInvestmentAmount + investmentData?.AnnuityNonInflationaryInvestmentAmount),
                ValueOfReturn = Convert.ToInt32(qualifiedLifeAnnuityValueOfReturn + nonqualifiedLifeAnnuityValueOfReturn),
                FutureValue = Convert.ToInt32(qualifiedLifeAnnuityFV + nonqualifiedLifeAnnuityFV),
                Year28FutureValue = Convert.ToInt32(qualifiedLifeAnnuity28FV + nonqualifiedLifeAnnuity28FV)
            };
        }
        #endregion
        public bool HasInvestmentIncome(Guid ClientLoginGuid)
        {
            var clientData = _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).FirstOrDefault();
            var hasBankCreditUnionAccounts = _context2.BankCreditUnionAccounts.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            var hasBrokerageAdvisoryAccounts = _context2.BrokerageAdvisoryAccounts.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            var hasLifeAnnuityContracts = _context2.LifeAnnuityContracts.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
            return clientData.InvestmentVersion && (hasBankCreditUnionAccounts || hasBrokerageAdvisoryAccounts || hasLifeAnnuityContracts);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                }
                disposed = true;
            }
        }
        ~InvestmentService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}