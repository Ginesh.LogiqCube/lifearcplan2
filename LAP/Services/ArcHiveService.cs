﻿using LAP.Areas.Adviser.Controllers;
using LAP.Controllers;
using LAP.DomainModels;
using LAP.DomainModels.BackgroundTask;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Entities.Client;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using System;
using System.IO;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class ArcHiveService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IBackgroundTaskQueue _queue;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public ArcHiveService(IServiceScopeFactory serviceScopeFactory, IBackgroundTaskQueue queue, IHttpContextAccessor httpContextAccessor)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _queue = queue;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task RiskArcSurvey(string token, ClientTypeEnum clientType, int tz, string host)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;

            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var _config = serviceProvider.GetRequiredService<IConfiguration>();
            var _riskArcController = serviceProvider.GetRequiredService<RiskArcController>();
            var _fileHelper = new FileHelper(_config);
            var actionContext = ExtensionMethod.GetActionContext(null, "RiskArc", "RiskArcSurvey", host, serviceProvider);
            var model = await _riskArcController.RiskArcSurveyData(token, clientType, tz);
            var footerAction = $"https://{host}/Home/ReportFooter";
            string customSwitches = string.Format("--footer-html {0} --footer-spacing -10", footerAction);
            var pdfViewAction = new ViewAsPdf(model)
            {
                PageSize = Size.Letter,
                PageMargins = { Left = 0, Bottom = 12, Right = 0, Top = 10 },
                CustomSwitches = customSwitches,
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName
            };
            byte[] pdfByteData = await pdfViewAction.BuildFile(actionContext);
            Stream pdfStream = new MemoryStream(pdfByteData);
            string bucketPath = $"{_config["BucketName"]}/documents/client";
            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(pdfStream, bucketPath, model.FileName);
            string contentType = "application/pdf";
            _context2.ClientDocuments.Add(new ClientDocument
            {
                ClientId = model.ClientId,
                FileName = model.FileName,
                ContentType = contentType,
                VersionId = uploadedFile.VersionId,
                FileSize = pdfByteData.Length,
                CreatedBy = model.ClientId,
                ModifiedBy = model.ClientId,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow
            });
            _context2.SaveChanges();
        }
        public async Task RiskArc(string token, ClientTypeEnum clientType, int tz, string host)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;

            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var _config = serviceProvider.GetRequiredService<IConfiguration>();
            var _reportService = serviceProvider.GetRequiredService<ReportService>();
            var _fileHelper = new FileHelper(_config);
            var actionContext = ExtensionMethod.GetActionContext("Adviser", "Report", "RiskArc", host, serviceProvider);
            var model = await _reportService.RiskArcReportData(token, clientType, tz);
            var viewName = model.RiskArcVersion ? "RiskArcInDepth" : "RiskArcExpress";
            var pdfViewAction = new ViewAsPdf(viewName, model)
            {
                PageMargins = { Left = 10, Bottom = 5, Right = 10, Top = 8 },
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName
            };
            byte[] pdfByteData = await pdfViewAction.BuildFile(actionContext);
            Stream pdfStream = new MemoryStream(pdfByteData);
            string bucketPath = $"{_config["BucketName"]}/documents/client";
            string contentType = "application/pdf";
            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(pdfStream, bucketPath, model.FileName);

            _context2.ClientDocuments.Add(new ClientDocument
            {
                ClientId = model.ClientId,
                FileName = model.FileName,
                ContentType = contentType,
                VersionId = uploadedFile.VersionId,
                FileSize = pdfByteData.Length,
                CreatedBy = model.ClientId,
                ModifiedBy = model.ClientId,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow
            });
            _context2.SaveChanges();
        }
        public async Task ArcOfLife(string token, int tz, string host)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;

            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var _config = serviceProvider.GetRequiredService<IConfiguration>();
            var _reportController = serviceProvider.GetRequiredService<ReportController>();
            var _fileHelper = new FileHelper(_config);
            var actionContext = ExtensionMethod.GetActionContext("Adviser", "Report", "ArcOfLife", host, serviceProvider);
            var model = await _reportController.ArcOfLifeData(token, tz);
            var pdfViewAction = new ViewAsPdf(model)
            {
                PageMargins = { Left = 10, Bottom = 5, Right = 10, Top = 10 },
                PageSize = Size.Letter,
                PageOrientation = Orientation.Landscape,
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName,
            };

            byte[] pdfByteData = await pdfViewAction.BuildFile(actionContext);
            Stream pdfStream = new MemoryStream(pdfByteData);
            string bucketPath = $"{_config["BucketName"]}/documents/client";

            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(pdfStream, bucketPath, model.FileName);
            string contentType = "application/pdf";
            _context2.ClientDocuments.Add(new ClientDocument
            {
                ClientId = model.ClientLogin.Id,
                FileName = model.FileName,
                ContentType = contentType,
                VersionId = uploadedFile.VersionId,
                FileSize = pdfByteData.Length,
                CreatedBy = model.ClientLogin.Id,
                ModifiedBy = model.ClientLogin.Id,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow
            });
            _context2.SaveChanges();
        }
        public async Task ClientSurvey(string token, int tz, string host)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;

            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var _config = serviceProvider.GetRequiredService<IConfiguration>();
            var _homeController = serviceProvider.GetRequiredService<Controllers.HomeController>();
            var _fileHelper = new FileHelper(_config);
            var actionContext = ExtensionMethod.GetActionContext(null, "Home", "ClientSurvey", host, serviceProvider);
            var model = await _homeController.ClientSurveyDataAsync(token, tz);
            var footerAction = $"https://{host}/Home/ReportFooter";
            string customSwitches = string.Format("--footer-html {0} --footer-spacing -10", footerAction);
            var pdfViewAction = new ViewAsPdf("Views/Home/ClientSurvey.cshtml", model)
            {
                PageSize = Size.Letter,
                PageMargins = { Left = 0, Bottom = 13, Right = 0, Top = 10 },
                CustomSwitches = customSwitches,
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName
            };
            byte[] pdfByteData = await pdfViewAction.BuildFile(actionContext);
            Stream pdfStream = new MemoryStream(pdfByteData);
            string bucketPath = $"{_config["BucketName"]}/documents/client";
            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(pdfStream, bucketPath, model.FileName);
            string contentType = $"application/pdf";
            _context2.ClientDocuments.Add(new ClientDocument
            {
                ClientId = model.ClientLoginData.Id,
                FileName = model.FileName,
                ContentType = contentType,
                VersionId = uploadedFile.VersionId,
                FileSize = pdfByteData.Length,
                CreatedBy = model.ClientLoginData.Id,
                ModifiedBy = model.ClientLoginData.Id,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow
            });
            _context2.SaveChanges();
        }
        public async void SaveReports(Guid guid, bool IsMarried, int tz)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;
            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var client = await _context2.ClientLogins.FirstOrDefaultAsync(m => m.Guid == guid);
            string host = _httpContextAccessor.HttpContext.Request.Host.Value;
            _queue.QueueBackgroundWorkItem(async token =>
              {
                  string tokenGuid = await guid.ToString().EncryptAsync();
                  if (client.HasChangesClientRiskArc)
                  {
                      await RiskArcSurvey(tokenGuid, ClientTypeEnum.Client, tz, host);
                      await RiskArc(tokenGuid, ClientTypeEnum.Client, tz, host);
                  }
                  if (client.HasChangesAol)
                      await ArcOfLife(tokenGuid, tz, host);
                  if (client.HasChangesCanvas)
                      await ClientSurvey(tokenGuid, tz, host);
                  if (IsMarried && client.HasChangesSpouseRiskArc)
                  {
                      await RiskArcSurvey(tokenGuid, ClientTypeEnum.Spouse, tz, host);
                      await RiskArc(tokenGuid, ClientTypeEnum.Spouse, tz, host);
                  }
              });
        }
        public async Task ADV_2B(string token, int tz, string host)
        {
            var scope = _serviceScopeFactory.CreateScope();
            var serviceProvider = scope.ServiceProvider;
            var _context2 = serviceProvider.GetRequiredService<LifeArcPlanContext2>();
            var _config = serviceProvider.GetRequiredService<IConfiguration>();
            var _reportController = serviceProvider.GetRequiredService<ReportController>();
            var _fileHelper = new FileHelper(_config);
            var actionContext = ExtensionMethod.GetActionContext("Adviser", "Report", "ADV_2B", host, serviceProvider);

            var model = await _reportController.ADV_2BData(token, tz);
            var pdfViewAction = new ViewAsPdf(model)
            {
                PageMargins = { Left = 10, Bottom = 12, Right = 10, Top = 10 },
                CustomSwitches = "--footer-center \"Page [page]\" --footer-line --footer-font-size \"12\" --footer-font-name \"Segoe UI\"",
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName
            };
            byte[] pdfByteData = await pdfViewAction.BuildFile(actionContext);
            Stream pdfStream = new MemoryStream(pdfByteData);
            string bucketPath = $"{_config["BucketName"]}/documents/compliance";
            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(pdfStream, bucketPath, model.FileName);
            string contentType = "application/pdf";
            _context2.ComplianceDocuments.Add(new ComplianceDocument
            {
                UserId = model.UserLogin.Id,
                FileName = model.FileName,
                ContentType = contentType,
                Status = ComplianceDocumentStatusEnum.Pending,
                ReqiuredChange = false,
                VersionId = uploadedFile.VersionId,
                FileSize = pdfByteData.Length,
                CreatedBy = model.UserLogin.Id,
                ModifiedBy = model.UserLogin.Id,
                UploadedDate = DateTime.UtcNow,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow
            });
            _context2.SaveChanges();
        }
        public void SaveAdviserReports(Guid guid, int tz)
        {
            string host = _httpContextAccessor.HttpContext.Request.Host.Value;
            _queue.QueueBackgroundWorkItem(async token =>
            {
                string tokenGuid = await guid.ToString().EncryptAsync();
                await ADV_2B(tokenGuid, tz, host);
            });
        }
    }
}