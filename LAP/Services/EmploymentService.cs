﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Services
{
    public class EmploymentService : IDisposable
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly CommonService _commonService;
        private bool disposed;

        public EmploymentService(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _commonService = new CommonService(_context2);
        }
        public async Task<IEnumerable<ClientOccupation>> GetEmploymentDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType)
        {
            var employments = _context2.ClientOccupations.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
                employments = employments.Where(m => m.EmploymentOf == clientType);
            return await Task.FromResult(employments);
        }
        public async Task<IEnumerable<ClientEsp>> GetRetirementPlanDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default)
        {
            var clientEspData = _context2.ClientEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
                clientEspData = clientEspData.Where(m => m.ClientOccupation.EmploymentOf == clientType);
            return await Task.FromResult(clientEspData);
        }
        public async Task<IEnumerable<ClientPrevEsp>> GetPreviousRetirementPlanDataAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default)
        {
            var employments = _context2.ClientPrevEsps.Where(m => m.ClientLogin.Guid == ClientLoginGuid);
            if (clientType != default)
                employments = employments.Where(m => m.EmploymentOf == clientType);
            return await Task.FromResult(employments);
        }
        public async Task<FVTotalModel> GetRetirementPlanFutureValueDetailsAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var retirementPlanData = (await GetRetirementPlanDataAsync(ClientLoginGuid, clientType))
                .Select(m => new FVDetailModel
                {
                    Contribution = m.EspContributionCalculated + m.EspEmployerMatchCalculated,
                    PresentValue = m.EspCurrentBalance,
                    RateOfInterest = Convert.ToDouble(m.EspAvgRateReturn),
                    FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.EspAvgRateReturn), yearToRetire, m.EspCurrentBalance, 0, m.EspContributionCalculated + m.EspEmployerMatchCalculated),
                    Year28FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.EspAvgRateReturn), null, m.EspCurrentBalance, 0, m.EspContributionCalculated + m.EspEmployerMatchCalculated),
                });
            return new FVTotalModel
            {
                PresentValue = retirementPlanData.Any() ? retirementPlanData.Sum(m => m.PresentValue) : 0,
                PmtValue = retirementPlanData.Any() ? retirementPlanData.Sum(m => m.PmtValue) : 0,
                FutureValue = retirementPlanData.Any() ? retirementPlanData.Sum(m => m.FutureValue) : 0,
                Year28FutureValue = retirementPlanData.Any() ? retirementPlanData.Sum(m => m.Year28FutureValue) : 0,
                ValueOfReturn = retirementPlanData.Any() ? retirementPlanData.Sum(m => m.ValueOfReturn) : 0
            };
        }
        public async Task<FVTotalModel> GetPreviousRetirementPlanFutureValueDetailsAsync(Guid ClientLoginGuid, ClientTypeEnum clientType = default)
        {
            var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
            var clientPrevEspData = (await GetPreviousRetirementPlanDataAsync(ClientLoginGuid, clientType))
                 .Select(m => new FVDetailModel
                 {
                     PresentValue = m.PreEmpCurrentBalance,
                     FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.PreEmpAvgRateReturn), yearToRetire, m.PreEmpCurrentBalance, m.PreEmpAnnualAmount),
                     Year28FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(m.PreEmpAvgRateReturn), null, m.PreEmpCurrentBalance, m.PreEmpAnnualAmount),
                     RateOfInterest = Convert.ToDouble(m.PreEmpAvgRateReturn),
                     Income = m.PreEmpAnnualAmount
                 });

            return new FVTotalModel
            {
                PresentValue = clientPrevEspData.Any() ? clientPrevEspData.Sum(m => m.PresentValue) : 0,
                PmtValue = clientPrevEspData.Any() ? clientPrevEspData.Sum(m => m.PmtValue) : 0,
                FutureValue = clientPrevEspData.Any() ? clientPrevEspData.Sum(m => m.FutureValue) : 0,
                Year28FutureValue = clientPrevEspData.Any() ? clientPrevEspData.Sum(m => m.Year28FutureValue) : 0,
                ValueOfReturn = clientPrevEspData.Any() ? clientPrevEspData.Sum(m => m.ValueOfReturn) : 0,
                Income = clientPrevEspData.Any() ? clientPrevEspData.Sum(m => m.Income) : 0
            };
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                }
                disposed = true;
            }
        }
        ~EmploymentService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}