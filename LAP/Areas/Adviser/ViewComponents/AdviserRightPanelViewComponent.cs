﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Areas.Adviser.ViewComponents
{
    public class AdviserRightPanelViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly FileHelper _fileHelper;
        protected readonly IConfiguration _config;
        protected readonly Guid UserLoginGuid;
        public AdviserRightPanelViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor, IConfiguration config)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _config = config;
            _fileHelper = new FileHelper(_config);
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var adviser = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.UserData).Include(m => m.Permissions)
                .Include(m => m.AssistantPermissions).Include(m => m.OwnerUser)
                .Include(m => m.OwnerUser.UserData).FirstOrDefaultAsync();
            ViewBag.additionalRoles = string.Join(",", adviser.Permissions.Select(m => m.RoleType.GetDisplayName()));
            ViewBag.assistantPermissions = adviser.AssistantPermissions.Count > 0 ? string.Join(",", adviser.AssistantPermissions.Select(m => m.Permission.GetDisplayName())) : null;
            var OwnerUserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value;
            ViewBag.IsAssistant = adviser.IsAssistant;
            var adviserData = adviser.UserData;
            ViewBag.AdviserName = adviserData.Name;
            ViewBag.OwnerName = adviser.OwnerUser != null ? adviser.OwnerUser.UserData.Name : null;
            string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
            if (!string.IsNullOrEmpty(adviserData.AvatarFileName))
            {
                string extension = Path.GetExtension(adviserData.AvatarFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                ViewBag.avatarImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.AvatarFileName, contentType);
            }
            return await Task.FromResult(View());
        }
    }
}
