﻿using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace LAP.Areas.Adviser.ViewComponents
{
    public class AdviserTopBarViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public AdviserTopBarViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var role = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value;
            Guid UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            var OwnerUserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId").ClaimToInt();
            var adviser = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.UserData).Include(m => m.AdviserContactInfos).FirstOrDefaultAsync();
            var adviserData = adviser.UserData;
            var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
            ViewBag.AdviserName = $"{adviserData.FirstName} {adviserData.LastName}";
            ViewBag.FirmId = adviser.FirmId;
            ViewBag.AdviserId = adviser.Id;
            ViewBag.OwnerUserId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : adviser.Id;
            ViewBag.LoginUrl = "user/login";
            ViewBag.DisplayOnlyAdviser = _httpContextAccessor.HttpContext.Request.Query["flag"] == "true";
            ViewBag.IsAdv2bValid = adviser.UserData.DateOfBirth is not null && adviser.AdviserContactInfos is not null;
            ViewBag.TimeZone = Convert.ToInt32(TimeZone);
            return await Task.FromResult(View());
        }
    }
}