﻿using System.ComponentModel.DataAnnotations;

namespace LAP.Areas.Adviser.Models
{
    public class OmniSessionModel
    {
        public bool success { get; set; }
        public OmniSessionResultModel result { get; set; }
    }
    public class OmniSessionResultModel
    {
        public string token { get; set; }
    }

    public class OmniUserModel
    {
        public bool success { get; set; }
        public OmniUserResultModel result { get; set; }
    }
    public class OmniUserResultModel
    {
        public string userId { get; set; }
        public string sessionName { get; set; }
    }
    public class OmniUserDetailModel
    {
        public string id { get; set; }
        public string email { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        [Display(Name = "Spouse Name")]
        public string cf_1500 { get; set; }
        public string assigned_user_id { get; set; }
    }
}
