﻿using LAP.DomainModels.Entities.Client;
using System;
using System.Collections.Generic;

namespace LAP.Areas.Adviser.Models
{
    public class ArcOfLifeReportModel
    {
        public ClientLogin ClientLogin { get; set; }
        public string BusinessLogoImage { get; set; }
        public int? ClientActualRetirementAge { get; set; }
        public int? SpouseActualRetirementAge { get; set; }
        public DateTime LocalDateTime { get; set; }
        public int TotalHeirs { get; set; }
        public bool SpouseHasOccupation { get; set; }
        public bool HasEstatePlanning { get; set; }
        public int? ClientRetirementAge { get; set; }
        public int? SpouseRetirementAge { get; set; }
        public bool HasHealthCarePlanning { get; set; }
        public int LongTermCareRate { get; set; }
        public bool IsClientRetired { get; set; }
        public bool IsSpouseRetired { get; set; }
        public AccumPrevDistri Accumulation { get; set; }
        public AccumPrevDistri Preservation { get; set; }
        public AccumPrevDistri Distribution { get; set; }
        public ExplicitVue ExplicitVue { get; set; }
        public EstatePlanningModel EstatePlanning { get; set; }
        public List<CashFlowRowModel> CashFlowReportData { get; set; }
        public int HouseHoldIncomeNeededTotal { get; set; }
        public int ClientSalaryTotal { get; set; }
        public int SpouseSalaryTotal { get; set; }
        public int ClientSelfEmpWagesTotal { get; set; }
        public int SpouseSelfEmpWagesTotal { get; set; }
        public int ClientPersonalPropertyTotal { get; set; }
        public int SpousePersonalPropertyTotal { get; set; }
        public int ClientPersonalActivityTotal { get; set; }
        public int SpousePersonalActivityTotal { get; set; }
        public int ClientInvestmentTotal { get; set; }
        public int SpouseInvestmentTotal { get; set; }
        public int ClientPensionTotal { get; set; }
        public int SpousePensionTotal { get; set; }
        public int ClientSocialSecurityTotal { get; set; }
        public int SpouseSocialSecurityTotal { get; set; }
        public int ClientAnnuityTotal { get; set; }
        public int SpouseAnnuityTotal { get; set; }
        public int GrandTotal
        {
            get => ClientSalaryTotal + SpouseSalaryTotal +
                   ClientSelfEmpWagesTotal + SpouseSelfEmpWagesTotal +
                   ClientPersonalPropertyTotal + SpousePersonalPropertyTotal +
                   ClientPersonalActivityTotal + SpousePersonalActivityTotal +
                   ClientInvestmentTotal + SpouseInvestmentTotal +
                   ClientSocialSecurityTotal + SpouseSocialSecurityTotal +
                   ClientPensionTotal + SpousePensionTotal +
                   ClientAnnuityTotal + SpouseAnnuityTotal;
        }
        public int CashFlowGrandTotal
        {
            get => GrandTotal - HouseHoldIncomeNeededTotal;
        }
        public string FileName { get; set; }
    }
    public class CashFlowRowModel
    {
        public int Id { get; set; }
        public int ClientAge { get; set; }
        public int SpouseAge { get; set; }
        public int HouseHoldIncomeNeeded { get; set; }
        public int ClientSalary { get; set; }
        public int SpouseSalary { get; set; }
        public int ClientSelfEmpWages { get; set; }
        public int SpouseSelfEmpWages { get; set; }
        public int ClientPersonalProperty { get; set; }
        public int SpousePersonalProperty { get; set; }
        public int ClientPersonalActivity { get; set; }
        public int SpousePersonalActivity { get; set; }
        public int ClientInvestment { get; set; }
        public int SpouseInvestment { get; set; }
        public int ClientPension { get; set; }
        public int SpousePension { get; set; }
        public int ClientSocialSecurity { get; set; }
        public int SpouseSocialSecurity { get; set; }
        public int ClientAnnuity { get; set; }
        public int SpouseAnnuity { get; set; }

        public int Total
        {
            get => ClientSalary + SpouseSalary +
                   ClientSelfEmpWages + SpouseSelfEmpWages +
                   ClientPersonalProperty + SpousePersonalProperty +
                   ClientPersonalActivity + SpousePersonalActivity +
                   ClientInvestment + SpouseInvestment +
                   ClientSocialSecurity + SpouseSocialSecurity +
                   ClientPension + SpousePension +
                   ClientAnnuity + SpouseAnnuity;
        }
        public int CashFlowTotal
        {
            get => Total - HouseHoldIncomeNeeded;
        }
    }
    public class AccumPrevDistri
    {
        public int Residence { get; set; }
        public int Assets { get; set; }
        public int ClientIncome { get; set; }
        public int ClientRetirement { get; set; }
        public int ClientSocialSecurity { get; set; }
        public int SpouseIncome { get; set; }
        public int SpouseRetirement { get; set; }
        public int SpouseSocialSecurity { get; set; }
        public int TotalHeirs { get; set; }
        public int Savings { get; set; }
        public int Life { get; set; }
        public int Annuities { get; set; }
        public int Investments { get; set; }
        public int Education { get; set; }
        public int ClientAssetTotal { get; set; }
        public int ClientDebtTotal { get; set; }
        public int ClientNetWorthTotal
        {
            get => ClientAssetTotal - ClientDebtTotal;
        }
        public CashFlowAnalysis CashFlowAnalysis { get; set; }
        public AssetAnalysis AssetAnalysis { get; set; }
        public PropertyAnalysis PropertyAnalysis { get; set; }
    }
    public class CashFlowAnalysis
    {
        public int EarnedIncome { get; set; }
        public int EmployerPension { get; set; }
        public int OtherIncome { get; set; }
        public int HouseholdIncome
        {
            get => EarnedIncome + EmployerPension + OtherIncome;
        }
        public int HouseholdExpenses
        {
            get => InflationaryExpenses + NonInflationaryExpenses;
        }
        public int InflationaryExpenses { get; set; }
        public int NonInflationaryExpenses { get; set; }
        public int CashFlowAnalysisTotal
        {
            get => HouseholdIncome - HouseholdExpenses;
        }
    }
    public class AssetAnalysis
    {
        public int EmployerRetirementPlans { get; set; }
        public int BankAssets { get; set; }
        public int PersonalInvestmentAssets { get; set; }
        public int InsuranceAssets { get; set; }
        public int AssetAnalysisTotal
        {
            get => EmployerRetirementPlans + BankAssets + PersonalInvestmentAssets + InsuranceAssets;
        }
    }
    public class PropertyAnalysis
    {
        public int RealProperty { get; set; }
        public int PersonalProperty { get; set; }
        public int PropertyAnalysisTotal
        {
            get => RealProperty + PersonalProperty;
        }
    }
    public class ExplicitVue
    {
        public int TotalInvestment { get; set; }
        public int StructureIncomeAtRetirement { get; set; }
        public int ExpenseAtRetirement { get; set; }
        public long InvestmentAccountNeededAtRetirement { get; set; }
        public int ProjectedValueOfInvestmentAtRetirement { get; set; }
        public double CurrentRoR { get; set; }
        public double RequiredRoR { get; set; }
        public double Fis { get; set; }
    }
    public class EstatePlanningModel
    {
        public bool Will { get; set; }
        public bool Trust { get; set; }
        public bool PowerOfAttorney { get; set; }
    }
}
