﻿using LAP.DomainModels;
using LAP.DomainModels.Entities.Client;
using System;
using System.Collections.Generic;

namespace LAP.Areas.Adviser.Models
{
    public class RiskArcReportModel
    {
        public string RIADisclosure { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double RiskCapacity { get; set; }
        public string RiskProfile { get; set; }
        public DateTime LocalDateTime { get; set; }
        public IEnumerable<RiskArcBitData> BitData { get; set; }
        public string FileName { get; set; }
        public bool RiskArcVersion { get; set; }
        public int ClientId { get; set; }
        public string BaseUrl { get; set; }
    }
    public class RiskArcBitData
    {
        public RiskArcBitEnum BitType { get; set; }
        public int Point { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
