﻿using LAP.DomainModels;
using LAP.DomainModels.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace LAP.Areas.Adviser.Models
{
    public class ClientViewModel
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public ClientInfo SpouseInfo { get; set; }
        [Display(Name = "Email Address", Prompt = "Email Address")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [RegularExpression(ValidationRegEx.Email, ErrorMessage = ValidationMessage.INVALID)]
        [StringLength(50)]
        [Remote("CheckEmailExist", "Home", AdditionalFields = "Guid")]
        public string Email { get; set; }
        public bool IsMarried { get; set; }
        public int UserId { get; set; }
        public string FromView { get; set; }
        public int TimeZone { get; set; }
        public string VerificationCode { get; set; }
    }
}
