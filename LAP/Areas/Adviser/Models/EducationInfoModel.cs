﻿using LAP.DomainModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LAP.Areas.Adviser.Models
{
    public class EducationInfoModel
    {
        [Display(Name = "Professional Degree & Designation")]
        public List<int> DegreeDesignations { get; set; }
        [Display(Name = "Professional License")]
        public List<int> Licenses { get; set; }
        [Display(Name = "CRD #")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int? CRD { get; set; }
        [Display(Name = "NPN #")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int? NPN { get; set; }
        public bool HasPostHighSchoolEducation { get; set; }
    }
}
