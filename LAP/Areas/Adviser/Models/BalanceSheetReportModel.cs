﻿using LAP.DomainModels;
using LAP.DomainModels.Entities.Client;
using System.Collections.Generic;

namespace LAP.Areas.Adviser.Models
{
    public class IncomeModel
    {
        public ClientTypeEnum ClientType { get; set; }
        public string Title { get; set; }
        public string ModuleName { get; set; }
        public int? Amount { get; set; }
        public int? Years { get; set; }
        public string FormatedAmount => string.Format("{0:C0}", Amount);
    }
    public class AssetModel
    {
        public ClientTypeEnum ClientType { get; set; }
        public string Title { get; set; }
        public string ModuleName { get; set; }
        public int? CurrentValue { get; set; }
        public int? FutureValue { get; set; }
        public string CurrentValueFormated => string.Format("{0:C0}", CurrentValue);
        public string FutureValueFormated => string.Format("{0:C0}", FutureValue);
    }
    public class BudgetModel
    {
        public string Title { get; set; }
        public string ModuleName { get; set; }
        public int? CurrentValue { get; set; }
        public int? FutureValue { get; set; }
        public string CurrentValueFormated => string.Format("{0:C0}", CurrentValue);
        public string FutureValueFormated => string.Format("{0:C0}", FutureValue);
    }
    public class BalanceSheetReportModel
    {
        public ClientLogin ClientLoginData { get; set; }
        public string ClientName { get; set; }
        public string SpouseName { get; set; }
        public string BusinessLogoImage { get; set; }
        public string FileName { get; set; }
        public bool BudgetVersion { get; set; }
        public List<IncomeModel> CurrentIncomes { get; set; }
        public List<IncomeModel> FutureIncomes { get; set; }
        public List<AssetModel> Assets { get; set; }
        public List<BudgetModel> Budgets { get; set; }
    }
}