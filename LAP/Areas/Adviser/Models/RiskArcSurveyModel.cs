﻿using LAP.DomainModels.Models;
using System;
using System.Collections.Generic;

namespace LAP.Areas.Adviser.Models
{
    public class RiskArcSurveyModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double RiskCapacity { get; set; }
        public string RiskProfile { get; set; }
        public DateTime LocalDateTime { get; set; }
        public IEnumerable<RiskArcBitData> BitData { get; set; }
        public string FileName { get; set; }
        public bool RiskArcVersion { get; set; }
        public IEnumerable<RiskArcQuestionModel> RiskArcQuestions { get; set; }
        public int ClientId { get; set; }
        public string BusinessLogoImage { get; set; }
        public string RIADisclosure { get; set; }
    }
}
