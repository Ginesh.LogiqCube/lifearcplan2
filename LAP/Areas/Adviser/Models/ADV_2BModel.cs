﻿using LAP.DomainModels.Entities.Admin;
using System;

namespace LAP.Areas.Adviser.Models
{
    public class ADV2BModel
    {
        public UserLogin UserLogin { get; set; }
        public bool HasDisciplinary { get; set; }
        public DateTime LocalDateTime { get; set; }
        public string FileName { get; set; }
    }
}
