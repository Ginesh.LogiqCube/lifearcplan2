﻿using LAP.DomainModels.Entities.Client;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace LAP.Areas.Adviser.Models
{
    public class ClientSurveyModel
    {
        public ClientLogin ClientLoginData { get; set; }
        public string BusinessLogoImage { get; set; }
        public string FileName { get; set; }
        public IEnumerable<SelectListItem> EmploymentOf { get; set; }
        public InvestmentLiteData InvestmentLiteData { get; set; }
        public List<ClientPlanning> PlanningItems { get; set; }
    }
}