﻿using LAP.DomainModels;
using System;
using System.ComponentModel.DataAnnotations;

namespace LAP.Areas.Adviser.Models
{
    public class CFSModel
    {
        public Guid StudentGuid { get; set; }
        public string CFSStudentId { get; set; }
        public string userid { get; set; }
        public string apikey { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string student_state { get; set; }
        [Display(Name = "Years till college")]
        [Range(1, 99, ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int? student_years_until_college { get; set; }
        [Display(Name = "Family Size")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int family_size { get; set; }
        [Display(Name = "Number in College")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int number_of_family_in_college { get; set; }
        [Range(1, 99, ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Age of Oldest Parent")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public int? age_of_oldest_parent { get; set; }
        [Display(Name = "Tax Return Filed")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string tax_return_filed { get; set; }
        [Display(Name = "Parent AGI")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public double parents_agi { get; set; }
        [Display(Name = "College")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string college_cbc { get; set; }
        [Display(Name = "Student Name/Initial")]
        public string student { get; set; }
        [Display(Name = "Student Grade")]
        [Range(int.MinValue, 12, ErrorMessage = ValidationMessage.RANGE_VALIDATION_GENERAL)]
        public int? grade { get; set; }
        [Display(Name = "Student GPA")]
        public double? gpa { get; set; }
        [Display(Name = "SAT (reading + math)")]
        public int? sat { get; set; }
        [Display(Name = "Parents Marital Status")]
        public string parents_marital_status { get; set; }
        [Display(Name = "Parent 1 Wage")]
        public double? parents_w2 { get; set; }
        [Display(Name = "Parent 2 Wage")]
        public double? spouse_w2 { get; set; }
        [Display(Name = "Federal Taxes Paid")]
        public double? parents_federal_taxes_paid { get; set; }
        [Display(Name = "Total Untaxed Income")]
        public double? parents_total_untaxed_income { get; set; }
        [Display(Name = "Liquid Assets")]
        public double? parents_liquid_assets { get; set; }
        [Display(Name = "Residence Equity")]
        public double? parents_residence_equity { get; set; }
        [Display(Name = "Other Real Estate Equity")]
        public double? parents_other_real_estate_equity { get; set; }
        [Display(Name = "Student's Gross Income")]
        public double? student_w2 { get; set; }
        [Display(Name = "AGI")]
        public double? student_agi { get; set; }
        [Display(Name = "Untaxed Income")]
        public double? student_untaxed_income { get; set; }
        [Display(Name = "Federal Tax Paid")]
        public double? student_federal_tax { get; set; }
        [Display(Name = "Liquid Assets")]
        public double? student_cash { get; set; }
        [Display(Name = "Student Sibling Assets")]
        public double? student_sibling_assets { get; set; }
        [Display(Name = "State the College is in")]
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        public string college_state { get; set; }
        [Display(Name = "College Annual Inflation")]
        public double? college_annual_inflation_rate { get; set; }
    }    
}
