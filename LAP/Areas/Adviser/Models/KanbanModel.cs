﻿using LAP.DomainModels;
using System;
using System.Collections.Generic;

namespace LAP.Areas.Adviser.Models
{
    public class KanbanModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Class { get; set; }
        public List<Item> Item { get; set; }
    }
    public class Item
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
    public class BoardItemModel
    {
        public ProspectAppointmentStageEnum Stage { get; set; }
        public bool FallOut { get; set; }
        public DateTime Date { get; set; }
    }
}