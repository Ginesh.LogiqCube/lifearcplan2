﻿using LAP.DomainModels;
using System.ComponentModel.DataAnnotations;

namespace LAP.Areas.Adviser.Models
{
    public class MyBusinessModel
    {
        [Display(Name = "Field Marketing Organization")]
        public int? FieldMarketingOrganization { get; set; }
        [Display(Name = "Broker/Dealer")]
        public int? BrokerDealer { get; set; }

        [Display(Name = "Registered Investment Advisor")]
        public int? RegisteredInvestmentAdvisor { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Disclosure for Broker/Dealer")]
        public string BDDisclosure { get; set; }
        [Display(Name = "Disclosure for Registered Investment Advisor")]
        public string RIADisclosure { get; set; }
        [Required(ErrorMessage = ValidationMessage.REQUIRED_VALIDATION_GENERAL)]
        [Display(Name = "Agent/Adviser Compensation")]
        public CompensationTypeEnum CompensationType { get; set; }
    }
}
