﻿using LAP.DomainModels;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace LAP.Areas.Adviser.Models
{
    public class RedtailUser
    {
        [JsonPropertyName("database_id")]
        public int DatabaseId { get; set; }
        [JsonPropertyName("user_id")]
        public int UserId { get; set; }
        [JsonPropertyName("user_key")]
        public string UserKey { get; set; }
    }
    public class IntegrationData
    {
        public string PartnerName { get; set; }
        public bool Connected { get; set; }
    }
    public enum RedtailStatusEnum
    {
        Prospect = 7,
        SpouseOfClient = 8
    }
    public enum RedtailEmailTypeEnum
    {
        Home = 1,
        Work = 2,
    }
    public enum RedtailAddressTypeEnum
    {
        Home = 1,
        Work = 2,
    }
    public enum RedtailPhoneTypeEnum
    {
        Home = 1,
        Work = 2,
        Mobile = 3,
    }
    public enum RedtailNoteTypeEnum
    {
        Note = 1,
    }
    public enum RedtailCategoryEnum
    {
        GeneralInformation = 2,
    }
    public enum RedtailRelationshipEnum
    {
        Spouse = 1,
    }
    public enum RedtailMaritalStatusEnum
    {
        Single = 1,
        Married = 3,
        Separated = 4,
        Divorced = 5,
        Widowed = 6
    }
    public enum RedtailGenderEnum
    {
        Male = 1,
        Female = 2
    }
    public class RedtailContact
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("type")]
        public string Type { get; set; }
        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }
        [JsonPropertyName("middle_name")]
        public string MiddleName { get; set; }
        [JsonPropertyName("last_name")]
        public string LastName { get; set; }
        [JsonPropertyName("status_id")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public RedtailStatusEnum? StatusId { get; set; }
        [JsonPropertyName("marital_status_id")]
        public RedtailMaritalStatusEnum MaritalStatusId { get; set; }
        [JsonPropertyName("dob")]
        public string DateOfBirth { get; set; }
        [JsonPropertyName("gender_id")]
        public GenderEnum Gender { get; set; }
        [JsonPropertyName("marital_date")]
        public string MarriageDate { get; set; }
        [JsonPropertyName("addresses")]
        public List<RedtailAddress> Addresses { get; set; }
        [JsonPropertyName("emails")]
        public List<RedtailEmail> Emails { get; set; }
        public List<RedtailPhone> Phones { get; set; }
    }
    public class RedtailEmail
    {
        [JsonPropertyName("id")]
        public int? Id { get; set; }
        [JsonPropertyName("email_type")]
        public RedtailEmailTypeEnum EmailType { get; set; }
        [JsonPropertyName("address")]
        public string Address { get; set; }
        [JsonPropertyName("is_primary")]
        public bool IsPrimary { get; set; }
    }
    public class RedtailPhone
    {
        [JsonPropertyName("id")]
        public int? Id { get; set; }
        [JsonPropertyName("phone_type")]
        public RedtailPhoneTypeEnum PhoneType { get; set; }
        [JsonPropertyName("country_code")]
        public int CountryCode { get; set; }
        [JsonPropertyName("number")]
        public string Number { get; set; }
        [JsonPropertyName("is_primary")]
        public bool IsPrimary { get; set; }
    }
    public class RedtailAddress
    {
        [JsonPropertyName("id")]
        public int? Id { get; set; }

        [JsonPropertyName("address_type")]
        public RedtailAddressTypeEnum AddressType { get; set; }
        [JsonPropertyName("street_address")]
        public string StreetAddress { get; set; }
        [JsonPropertyName("secondary_address")]
        public string SuiteApt { get; set; }
        [JsonPropertyName("city")]
        public string City { get; set; }
        [JsonPropertyName("state")]
        public string State { get; set; }
        [JsonPropertyName("zip")]
        public string Zip { get; set; }
        [JsonPropertyName("is_primary")]
        public bool IsPrimary { get; set; }
    }
    public class RedtailNotes
    {
        [JsonPropertyName("category_id")]
        public RedtailCategoryEnum CategoryId { get; set; }
        [JsonPropertyName("note_type")]
        public RedtailNoteTypeEnum NoteType { get; set; }
        [JsonPropertyName("body")]
        public string Body { get; set; }
    }
    public class RedtailFamily
    {
        [JsonPropertyName("id")]
        public int? Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("members")]
        public List<RedtailFamilyMember> Members { get; set; }

    }
    public class RedtailFamilyMember
    {
        [JsonPropertyName("contact_id")]
        public int ContactId { get; set; }
        [JsonPropertyName("hoh")]
        public bool Hoh { get; set; }
        [JsonPropertyName("relationship")]
        public int? Relationship { get; set; }
    }
}