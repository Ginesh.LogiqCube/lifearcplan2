﻿using AutoMapper;
using Google.Authenticator;
using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAP.Areas.Adviser.Controllers
{
    [Area(Roles.Adviser)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class SettingsController : BaseController, IDisposable
    {
        protected readonly Guid UserLoginGuid;
        public SettingsController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
        }
        public async Task<IActionResult> Index()
        {
            var client = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid && m.IsDeleted != true).FirstOrDefaultAsync();
            ViewBag.EmptyLayout = true;
            var twoFactorAuthenticator = new TwoFactorAuthenticator();
            var accountSecretKey = $"{_config["SecretKey"]}-{client.Username}";
            var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], client.Username, Encoding.ASCII.GetBytes(accountSecretKey));
            ViewBag.SetupCode = setupCode;
            ViewBag.HasTwoFactorAuth = client.TwoFactorAuth;
            ViewBag.nextPrev = new PrevNextMenuModel { NextTitle = "Connected Apps", NextAction = "user/connected-apps" };
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SaveTwoFactorAuth(string VerificationCode, bool TwoFactorAuth)
        {
            var admin = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid && m.IsDeleted != true)
                .Include(m => m.UserData).FirstOrDefaultAsync();
            bool isChangedStatus = admin.TwoFactorAuth != TwoFactorAuth;
            bool isAuthenticated = false;
            if (TwoFactorAuth && !string.IsNullOrEmpty(VerificationCode))
            {
                var accountSecretKey = $"{_config["SecretKey"]}-{admin.Username}";
                var twoFactorAuthenticator = new TwoFactorAuthenticator();
                isAuthenticated = twoFactorAuthenticator.ValidateTwoFactorPIN(accountSecretKey, VerificationCode, TimeSpan.FromSeconds(30));
                if (isAuthenticated)
                    admin.TwoFactorAuth = TwoFactorAuth;
            }
            else
                admin.TwoFactorAuth = TwoFactorAuth;
            await _context2.SaveChangesAsync();
            var action = admin.TwoFactorAuth ? "activated" : "deactivated";
            string role = admin.AdminRole == AdminRoleEnum.FirmPrincipal ? "firm-principal" : admin.AdminRole == AdminRoleEnum.FirmAdmin ? "firm-admin" : "adviser";
            var result = new CommonResponse
            {
                Success = isAuthenticated || !TwoFactorAuth || string.IsNullOrEmpty(VerificationCode),
                Message = !isChangedStatus ? string.Empty : isAuthenticated || !TwoFactorAuth || string.IsNullOrEmpty(VerificationCode) ? string.Format(Message.TWO_FACTOR_AUTH, action) : Message.INVALID_VERIFICATION_CODE,
                RedirectUrl = Url.RouteUrl("user/connected-apps"),
                Data = new { admin.TwoFactorAuth },
            };
            return Json(result);
        }
        public async Task<IEnumerable<IntegrationItemModel>> GetIntegrationPartners()
        {
            var integration = await _context2.Integrations.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            var integrationPartnersData = await MenuDataHelper.GeIntegrationPartnerDataAsync();
            var adviserIntegrationData = new List<IntegrationData>
            {
                new IntegrationData
               {
                    PartnerName = "Redtail",
                    Connected = integration?.RedtailUserId > 0
               },
               new IntegrationData
               {
                    PartnerName = "CollegiateFundingSolutions",
                    Connected =!string.IsNullOrEmpty(integration?.CFSUserId)
               },
               new IntegrationData
               {
                    PartnerName = "Omniscient",
                    Connected =!string.IsNullOrEmpty(integration?.OmniUserId)
               },
               new IntegrationData
               {
                    PartnerName = "FusionElements",
                    Connected =!string.IsNullOrEmpty(integration?.FusionUserId)
               },
            };
            var adviserIntegrationPartnersData = from c in integrationPartnersData
                                                 join p in adviserIntegrationData
                                                 on c.PartnerName equals p.PartnerName into ps
                                                 from p in ps.DefaultIfEmpty()
                                                 select new IntegrationItemModel
                                                 {
                                                     PartnerName = c.PartnerName,
                                                     BorderColor = c.BorderColor,
                                                     Description = c.Description,
                                                     Id = c.Id,
                                                     LogoFileName = c.LogoFileName,
                                                     Title = c.Title,
                                                     Connected = p != null && p.Connected
                                                 };
            return adviserIntegrationPartnersData;
        }
        public async Task<IActionResult> ConnectedApps()
        {
            ViewBag.EmptyLayout = true;
            var integrationPartnerData = await GetIntegrationPartners();
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Security Setting", PrevAction = "user/security-setting" };
            return View("ConnectedApps/ConnectedApps", integrationPartnerData);
        }
        public IActionResult IntegrationAuth()
        {
            return View("ConnectedApps/_IntegrationAuth");
        }
    }
}
