﻿using AutoMapper;
using LAP.Areas.Adviser.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using LAP.Model;
using System.IO;
using LAP.Services;
using System.Web;
using LAP.Services.Integration;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Models;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using Newtonsoft.Json.Linq;
using System.Net.Mail;
using System.Collections.Generic;
using Google.Authenticator;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LAP.Areas.Adviser.Controllers
{
    [Area(Roles.Adviser)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class HomeController : BaseController, IDisposable
    {
        protected readonly FileHelper _fileHelper;
        protected readonly Guid UserLoginGuid;
        private readonly int UserId;
        private readonly int OwnerUserId;
        private readonly int FirmId;
        protected readonly CommonService _commonService;
        protected readonly RedtailService _redtailService;
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly CFSService _cfsService;
        protected readonly OmniService _omniService;
        protected readonly EmploymentService _employmentService;
        public HomeController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _fileHelper = new FileHelper(_config);
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            UserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").ClaimToInt();
            OwnerUserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId").ClaimToInt();
            FirmId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "FirmId").ClaimToInt();
            _commonService = new CommonService(_context2);
            _redtailService = new RedtailService(context2, config);
            _omniService = new OmniService(context2, config, env);
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _cfsService = new CFSService(context2, config, env);
            _employmentService = new EmploymentService(_context2, _httpContextAccessor);
        }
        public async Task<IActionResult> WelcomeUser()
        {
            var OwnerUserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value;
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.Permissions).Include(m => m.AssistantPermissions)
                .Include(m => m.Firm).FirstOrDefaultAsync();
            ViewBag.IsAssistant = adviserDataObj.IsAssistant;
            ViewBag.additionalRoles = adviserDataObj.Permissions.Count > 0 ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            ViewBag.assistantPermissions = adviserDataObj.AssistantPermissions.Count > 0 ? string.Join(",", adviserDataObj.AssistantPermissions.Select(m => (int)m.Permission)) : null;

            var firm = adviserDataObj.Firm;
            ViewBag.IsInitialLogin = adviserDataObj.LastLogin == adviserDataObj.SetupCompletedDate;
            ViewBag.UserBoardMessage = !string.IsNullOrEmpty(firm.UserBoardMessage) ? HttpUtility.UrlDecode(firm.UserBoardMessage).Replace("<a", "<a target='_blank'") : string.Empty;
            var releaseNote = await _context2.ReleaseNotes.OrderByDescending(m => m.Id).FirstOrDefaultAsync();
            ViewBag.LastReleaseNote = releaseNote;
            return View();
        }
        public async Task<IActionResult> Clients()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            return View();
        }
        public async Task<IActionResult> AddClient(Guid guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == guid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            ClientViewModel clientViewModel = new();
            if (client is not null)
            {
                clientViewModel = new ClientViewModel()
                {
                    Id = client.Id,
                    Guid = client.Guid,
                    Email = client.Username,
                    ClientInfo = new ClientInfo()
                    {
                        FirstName = client.ClientData.FirstName,
                        MiddleInitial = client.ClientData.MiddleInitial,
                        LastName = client.ClientData.LastName,
                    }
                };
            }
            if (guid != Guid.Empty)
            {
                var twoFactorAuthenticator = new TwoFactorAuthenticator();
                var accountSecretKey = $"{_config["SecretKey"]}-{client.Username}";
                var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], client.Username, Encoding.ASCII.GetBytes(accountSecretKey));
                ViewBag.SetupKey = setupCode.ManualEntryKey;
            }
            return View("_AddClientModal", clientViewModel);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SaveClient(ClientViewModel clientViewModel, bool fromMarketing = false)
        {
            var clientObj = await _context2.ClientLogins.Where(m => m.Guid == clientViewModel.Guid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var clientLogin = new ClientLogin();
            bool veryfied = false;
            string OldEmail = string.Empty;
            string NewEmail = clientViewModel.Email;
            var OwnerUserId = User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value.StringToInt();
            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            if (clientObj is not null)
            {
                clientObj.ClientData.FirstName = clientViewModel.ClientInfo.FirstName;
                clientObj.ClientData.MiddleInitial = clientViewModel.ClientInfo.MiddleInitial;
                clientObj.ClientData.LastName = clientViewModel.ClientInfo.LastName;
                clientObj.ClientData.DateOfBirth = clientViewModel.ClientInfo.DateOfBirth != null ? Convert.ToDateTime(clientViewModel.ClientInfo.DateOfBirth) : clientObj.ClientData.DateOfBirth;
                clientObj.ClientData.MaritalStatus = clientViewModel.IsMarried ? MaritalStatusEnum.Married : MaritalStatusEnum.Unmarried;
                OldEmail = clientObj.Username;

                if (clientViewModel.IsMarried)
                {
                    var spouseData = clientObj.SpouseData ?? new SpouseData();
                    spouseData.FirstName = clientViewModel.SpouseInfo.FirstName;
                    spouseData.MiddleInitial = clientViewModel.SpouseInfo.MiddleInitial;
                    spouseData.LastName = clientViewModel.SpouseInfo.LastName;
                    spouseData.DateOfBirth = clientViewModel.SpouseInfo.DateOfBirth != null ? Convert.ToDateTime(clientViewModel.SpouseInfo.DateOfBirth) : clientObj.SpouseData.DateOfBirth;
                    clientObj.SpouseData = spouseData;
                }
                else
                    await _commonService.ClearSpouseData(clientObj.Guid);
                if (clientObj.Username != clientViewModel.Email)
                {
                    if (string.IsNullOrEmpty(clientViewModel.VerificationCode))
                    {
                        veryfied = true;
                        Random generator = new();
                        string code = generator.Next(0, 9999).ToString("D4");
                        clientObj.VerificationCode = code;
                        var initiator = await _context2.UserDatas.FirstOrDefaultAsync(m => m.UserLogin.Guid == UserLoginGuid);
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", clientObj.ClientData.FirstName);
                        var emailText = @$"This is to notify you that <span style='color:#0073e9'>{initiator.Name}</span> has initiated the email change process.
                                           Please verify by applying the code in the Change Email Address pop-up.<br>
                                           Verification Code: <b style='color:#800000'>{code}</b>";
                        emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        string email = User.FindFirst(ClaimTypes.Email)?.Value;
                        string receiver = _env.EnvironmentName == "Production" ? $"{email},{clientViewModel.Email}" : $"{email},{clientViewModel.Email},{_config["Email:SuperAdminEmail"]}";
                        await emailHelper.SendEmailAsync(receiver, null, _config["Email:BccEmail"], EmailSubject.EmailChangeRequest, emailHtmlBody, _env.WebRootPath);
                    }
                    else
                    {
                        if (clientViewModel.VerificationCode == clientObj.VerificationCode)
                        {
                            clientObj.Username = clientViewModel.Email;
                            veryfied = true;
                            clientObj.VerificationCode = string.Empty;
                        }
                    }
                    await _context2.SaveChangesAsync();
                    if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && veryfied && !string.IsNullOrEmpty(clientViewModel.VerificationCode))
                    {
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", clientObj.ClientData.FirstName);
                        emailTemplateHtml.Replace("@EMAILTEXT", string.Format(EmailNotification.EMAILCHANGE, clientObj.Username));
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        await emailHelper.SendEmailAsync(clientObj.Username, null, _config["Email:BccEmail"], EmailSubject.EmailChangeNotification, emailHtmlBody, _env.WebRootPath);
                    }
                }
                _context2.ClientLogins.Update(clientObj);
            }
            else
            {
                clientLogin = new ClientLogin()
                {
                    Username = clientViewModel.Email,
                    UserId = UId > 0 ? UId : clientViewModel.UserId,
                    ClientData = new ClientData()
                    {
                        FirstName = clientViewModel.ClientInfo.FirstName,
                        MiddleInitial = clientViewModel.ClientInfo.MiddleInitial,
                        LastName = clientViewModel.ClientInfo.LastName,
                        MaritalStatus = clientViewModel.IsMarried ? MaritalStatusEnum.Married : MaritalStatusEnum.Unmarried,
                        AssumedInflationRate = 3
                    },
                    SpouseData = clientViewModel.IsMarried ? new SpouseData()
                    {
                        FirstName = clientViewModel.SpouseInfo.FirstName,
                        MiddleInitial = clientViewModel.SpouseInfo.MiddleInitial,
                        LastName = clientViewModel.SpouseInfo.LastName,
                    } : null
                };
                _context2.ClientLogins.Add(clientLogin);
                if (!fromMarketing)
                    await AddClientWithoutMarketingAsync(clientViewModel, UId);
            }
            await _context2.SaveChangesAsync();
            if (clientObj == null)
                await InviteClient(clientViewModel);
            else
                clientLogin = clientObj;
            var clientDataObj = clientLogin.ClientData;
            var spouseDataObj = clientLogin.SpouseData;
            await _redtailService.UpdateClientContact(clientLogin.Guid, clientViewModel.TimeZone);

            if (clientDataObj.MaritalStatus == MaritalStatusEnum.Married)
                await _redtailService.UpdateSpouseContact(clientLogin.Guid, $"{clientDataObj.LastName} Family", clientDataObj.RedtailContactId);
            var integrationData = await _context2.UserLogins.Where(m => m.Id == clientLogin.UserId)
                .Include(m => m.Integration).FirstOrDefaultAsync();
            if (!string.IsNullOrEmpty(integrationData?.Integration?.OmniUsername))
            {
                var model = new LoginModel
                {
                    Username = integrationData.Integration.OmniUsername,
                    Password = await integrationData.Integration.OmniAccessKey.DecryptAsync()
                };
                await _omniService.UpdateClientContact(model, clientLogin.Guid, clientDataObj.OmniContactId);
            }
            string msg = string.Empty;
            if (OldEmail == NewEmail || string.IsNullOrEmpty(OldEmail))
                msg = clientObj is not null ? Message.SAVE_SUCCESS : string.Format(Message.CLIENTADVISER_ADDED, Roles.Client);
            else
                msg = string.IsNullOrEmpty(clientViewModel.VerificationCode) ? string.Format(Message.VERIFICATION_CODE_SENT, clientViewModel.Email) : veryfied && !string.IsNullOrEmpty(clientViewModel.VerificationCode) ? Message.VARIFIED_SUCCESS : Message.INVALID_VERIFICATION_CODE;

            var result = new CommonResponse
            {
                Success = OldEmail == NewEmail || string.IsNullOrEmpty(OldEmail) || veryfied,
                Message = msg,
                ParentId = clientObj is not null ? clientObj.UserId : clientLogin.UserId,
                ActionType = clientObj is not null ? "update" : "add",
                Data = new { OldEmail, NewEmail },
                RedirectUrl = clientViewModel.FromView == "ClientBasicDetail" ? "client-riskarc" : string.Empty
            };
            return Json(result);
        }
        public async Task AddClientWithoutMarketingAsync(ClientViewModel clientViewModel, int UId)
        {
            var lead = new Lead
            {
                UserId = UId,
                Email = clientViewModel.Email,
                FirstName = clientViewModel.ClientInfo.FirstName,
                LastName = clientViewModel.ClientInfo.LastName
            };
            _context2.Leads.Add(lead);
            await _context2.SaveChangesAsync();

            _context2.ProspectAppointments.Add(new ProspectAppointment
            {
                LeadId = lead.Id,
                Location = string.Empty,
                Subject = string.Empty,
                Stage = ProspectAppointmentStageEnum.PreAppointment,
            });
            _context2.LeadHistories.Add(new LeadHistory
            {
                LeadId = lead.Id,
                Stage = ProspectAppointmentStageEnum.PreAppointment,
                Default = true
            });
        }
        public async Task<IActionResult> SendInviteToLead(Guid guid)
        {
            var lead = await _context2.Leads.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            var client = await _context2.ClientLogins.Where(m => m.Username == lead.Email && m.IsDeleted != true).FirstOrDefaultAsync();
            if (client == null)
            {
                await SaveClient(new ClientViewModel
                {
                    ClientInfo = new ClientInfo
                    {
                        FirstName = lead.FirstName,
                        LastName = lead.LastName
                    },
                    Email = lead.Email
                }, true);
            }
            else
                await ReInvite(client.Guid);
            lead.Active = false;
            _context2.LeadHistories.Add(new LeadHistory
            {
                LeadId = lead.Id,
                Stage = ProspectAppointmentStageEnum.PreAppointment,
                Default = true
            });
            var newAppointment = new ProspectAppointment
            {
                LeadId = lead.Id,
                Location = string.Empty,
                Subject = string.Empty,
                Stage = ProspectAppointmentStageEnum.PreAppointment,
            };
            _context2.ProspectAppointments.Add(newAppointment);

            await _context2.SaveChangesAsync();
            var itemTitle = @$"<div class='d-flex align-items-center'>                                    
                                    <input type='hidden' value='{newAppointment.Guid}' class='guid'/>
                        	        <div class='symbol symbol-light-primary mr-3 initial'>
                        	            <span class='symbol-label'>{lead.Name.GetInitials()}</span>
                        	        </div>
                        	        <div class='d-flex flex-column align-items-start'>
                        	            <span class='text-dark-50 font-weight-bold mb-1'>{lead.Name}</span>
                        	            <button class='btn label label-inline label-outline-primary font-weight-bold status' data-guid='{newAppointment.Guid}' data-has-event-appointment='1'>Invite Sent</button>
                                    </div>
                        	    </div>";
            var result = new CommonResponse
            {
                Success = lead != null,
                Message = lead is not null ? client == null ? Message.PREAPPOINTMENTSENT : Message.RESEND_INVITATION : Message.NO_RECORD_FOUND,
                Data = new { title = itemTitle }
            };
            return Json(result);
        }
        public async Task InviteClient(ClientViewModel clientViewModel)
        {
            var AdviserEmailId = User.FindFirst(ClaimTypes.Email)?.Value;
            string name = clientViewModel.ClientInfo.FirstName;
            string emailToken = await clientViewModel.Email.EncryptAsync();
            var createPasswordLink = Url.RouteUrl("create-password", new { token = emailToken }, Request.Scheme);
            StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
            mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
            var adviser = await _context2.ClientLogins.Where(m => m.Username == clientViewModel.Email && m.IsDeleted != true).Include(m => m.UserLogin.UserData).FirstOrDefaultAsync();
            Guid ownerGuid = adviser.UserLogin.IsAssistant ? adviser.UserLogin.OwnerUser.Guid : adviser.UserLogin.Guid;
            string guidToken = await ownerGuid.ToString().EncryptAsync();
            var adviserLink = Url.RouteUrl("adviser-documents", new { token = guidToken }, Request.Scheme);
            StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/create-password.html")));
            emailTemplateHtml.Replace("@ADVISERNAME", $"{adviser.UserLogin.UserData.FirstName} {adviser.UserLogin.UserData.LastName}");
            emailTemplateHtml.Replace("@ADVISERLINK", adviserLink);
            emailTemplateHtml.Replace("@CREATEPASSWORDLINK", createPasswordLink);
            emailTemplateHtml.Replace("@NAME", name);
            mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
            EmailHelper emailHelper = new(_config);
            string emailHtmlBody = mainEmailTemplateHtml.ToString();
            List<LinkedResource> resources = new()
            {
                new LinkedResource(Path.Combine(_env.WebRootPath, "images/brand_logo.png"))
                {
                    ContentId = "LogoImage"
                }
            };
            await emailHelper.SendEmailAsync(clientViewModel.Email, null, _config["Email:BccEmail"], EmailSubject.CreatePassword, emailHtmlBody, _env.WebRootPath);
            await Task.CompletedTask;
        }
        public async Task<IActionResult> ReInvite(Guid ClientLoginGuid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
            var adviserViewModel = new ClientViewModel
            {
                ClientInfo = new ClientInfo
                {
                    FirstName = client.ClientData.FirstName,
                    LastName = client.ClientData.LastName
                },
                Email = client.Username
            };
            await InviteClient(adviserViewModel);
            var result = new CommonResponse
            {
                Success = client is not null,
                Message = client is not null ? Message.RESEND_INVITATION : string.Format(Message.NO_RECORD_FOUND),
            };
            return Json(result);
        }
        public async Task<IActionResult> Assistants()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions is not null ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            return View();
        }
        public async Task<IActionResult> FetchClientsWithCount(int userId, DatatableMetaModel meta, CommonSearchModel query)
        {
            bool isAdviser = User.IsInRole(AdminRoleEnum.Adviser.GetDisplayName());
            var allClients = _context2.ClientLogins.Where(m => m.UserLogin.FirmId == FirmId && m.IsDeleted != true && (m.UserLogin.AdminRole == AdminRoleEnum.Adviser || m.UserLogin.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)));
            if (isAdviser)
                allClients = allClients.Where(m => m.UserLogin.Id == userId);
            var advisers = (await allClients.Select(m => new
            {
                m.Id,
                m.ClientData.Name,
                ClientData = new ClientData
                {
                    FirstName = m.ClientData.FirstName,
                    LastName = m.ClientData.LastName
                },
                m.Username,
                m.UserId,
                TotalDocument = m.ClientDocuments.Count(n => n.ClientId == m.Id),
            }).ToListAsync()).AsQueryable();
            int rowsTotal = advisers.Count();
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            var result = await advisers.PagedListAsync(meta, null, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> FetchTaxDocumentClientsWithCount(int userId, int year, DatatableMetaModel meta, CommonSearchModel query)
        {
            bool isAdviser = User.IsInRole(AdminRoleEnum.Adviser.GetDisplayName());
            var allClients = _context2.ClientLogins.Where(m => m.UserLogin.FirmId == FirmId && m.IsDeleted != true && (m.UserLogin.AdminRole == AdminRoleEnum.Adviser || m.UserLogin.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)));
            if (isAdviser)
                allClients = allClients.Where(m => m.UserLogin.Id == userId);
            var statuses = _context2.TaxPrepStatuses.Where(m => m.Year == year);
            var advisers = (await allClients.Select(m => new TaxDocumentClientListModel
            {
                Id = m.Id,
                Guid = m.Guid,
                Name = m.ClientData.Name,
                ClientData = new ClientData
                {
                    FirstName = m.ClientData.FirstName,
                    LastName = m.ClientData.LastName
                },
                Username = m.Username,
                UserId = m.UserId,
                TaxPrepStatus = statuses.FirstOrDefault(n => n.ClientId == m.Id) != null ? (int)statuses.FirstOrDefault(n => n.ClientId == m.Id).Status : 0
            }).ToListAsync()).AsQueryable();
            int rowsTotal = advisers.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            var result = await advisers.PagedListAsync(meta, null, sortString, searchQuery);
            var firmUsers = _context2.UserDatas.Where(m => m.UserLogin.FirmId == FirmId);
            result.Data = result.Data.ToList().Select(m =>
                            {
                                m.EncryptedGuid = m.Guid.ToString().EncryptAsync().Result;
                                m.TaxPrepStatusString = statuses.FirstOrDefault(n => n.ClientId == m.Id) != null ? statuses.FirstOrDefault(n => n.ClientId == m.Id).TaxPrepStatusString : string.Empty;
                                m.AdviserName = firmUsers.FirstOrDefault(n => n.UserId == m.UserId).Name;
                                m.TotalTaxDocument = _context2.TaxDocuments.Count(n => n.ClientId == m.Id && n.Year == year);
                                return m;
                            }).AsQueryable();
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateTaxPrepStatus(int clientId, int year, TaxFilingPrepStatusEnum status)
        {
            var taxPrepStatus = await _context2.TaxPrepStatuses.FirstOrDefaultAsync(m => m.ClientId == clientId && m.Year == year);
            bool statusChange = status != taxPrepStatus?.Status;
            if (taxPrepStatus == null)
                _context2.TaxPrepStatuses.Add(new TaxPrepStatus
                {
                    ClientId = clientId,
                    Year = year,
                    Status = status
                });
            else
                taxPrepStatus.Status = status;
            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            _context2.TaxPrepHistories.Add(new TaxPrepHistory
            {
                ActionType = "Update",
                ClientId = clientId,
                Year = year,
                Status = status,
                ActionByUserId = UId,
            });
            await _context2.SaveChangesAsync();
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && statusChange)
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                var adviser = await _context2.UserLogins.Where(m => m.Id == UId).Include(m => m.UserData).FirstOrDefaultAsync();
                var client = await _context2.ClientLogins.Where(m => m.Id == clientId).Include(m => m.ClientData).FirstOrDefaultAsync();
                emailTemplateHtml.Replace("@NAME", client.ClientData.FirstName);
                var emailText = @$"This is to notify you that <b>{adviser.UserData.Name}</b> has updated the Tax Filing status to <span style='color:#0073e9'>{status.GetDisplayName()}</span> for the Year {year}";
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                List<string> ccedEmails = new()
                {
                    adviser.Username
                };
                var assistantWithTaxPrepPermission = _context2.UserLogins.Where(m => m.FirmId == FirmId && m.OwnerUserId == UId && m.IsAssistant && m.AssistantPermissions.Any(m => m.Permission == AssistantPermissionEnum.AssistantTQ)).Select(m => m.Username);
                var adminWithAdviserPermission = _context2.UserLogins.Where(m => m.FirmId == FirmId && !m.IsAssistant && (m.AdminRole == AdminRoleEnum.FirmPrincipal || m.AdminRole == AdminRoleEnum.FirmAdmin) && m.Permissions.Any(m => m.RoleType == AdminRoleEnum.Adviser)).Select(m => m.Username);
                ccedEmails.AddRange(assistantWithTaxPrepPermission);
                ccedEmails.AddRange(adminWithAdviserPermission);
                await emailHelper.SendEmailAsync(client.Username, string.Join(",", ccedEmails), _config["Email:BccEmail"], EmailSubject.TaxPrepStatusUpdate, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = status != default,
                Message = status != default ? Message.TAXPREPSTATUSUPDATE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        public IActionResult ClientDocumentVault()
        {
            return View("DocumentVault");
        }
        public async Task<IActionResult> ComplianceDocuments()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.UserData).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            ViewBag.EncryptedGuid = await UserLoginGuid.ToString().EncryptAsync();
            ViewBag.hasRIA = adviserDataObj.UserData.RIAId > 0;
            return View();
        }
        public async Task<IActionResult> ComplianceDocumentModal(Guid guid)
        {
            var document = await _context2.ComplianceDocuments.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            return View("_ComplianceDocumentModal", document);
        }
        [HttpPost]
        public async Task<IActionResult> ComplianceDocumentUpload(IFormFile file, ComplianceDocument complianceDocument)
        {
            string bucketPath = $"{_config["BucketName"]}/documents/compliance";
            var document = await _context2.ComplianceDocuments.Where(m => m.Guid == complianceDocument.Guid).FirstOrDefaultAsync() ?? new ComplianceDocument();
            string VersionId = string.Empty;
            if (file != null)
            {
                var uploadedFile = await _fileHelper.UploadFileToS3Bucket(file.OpenReadStream(), bucketPath, file.FileName);
                document.VersionId = uploadedFile.VersionId;
                document.FileSize = file.Length;
            }
            VersionId = document.VersionId is not null ? document.VersionId : "N/A";
            document.FileName = file != null ? file.FileName : complianceDocument.FileUrl;
            document.ContentType = file != null ? file.ContentType : "link";
            document.Status = ComplianceDocumentStatusEnum.Pending;
            document.ReqiuredChange = false;
            document.UploadedDate = DateTime.UtcNow;
            document.AdviserNote = complianceDocument.AdviserNote;
            document.UserId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            _context2.ComplianceDocuments.Update(document);
            await _context2.SaveChangesAsync();

            var complianceEmails = _context2.Permissions.Where(m => m.RoleType == AdminRoleEnum.Compliance && m.UserInfo.FirmId == FirmId)
                .Include(m => m.UserInfo).Select(m => m.UserInfo.Username);

            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && complianceEmails.Any())
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/compliance-document-upload.html")));
                var adviser = await _context2.UserLogins.Where(m => m.Id == UserId).Include(m => m.UserData).FirstOrDefaultAsync();
                emailTemplateHtml.Replace("@ADVISERNAME", adviser.UserData.Name);
                var fileName = file != null ? $"<span style='color:#0073e9'>{document.FileName}</span>" : $"a <a href='{complianceDocument.FileUrl}' target='_blank'>Link</a>";
                emailTemplateHtml.Replace("@FILENAME", fileName);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(string.Join(",", complianceEmails), null, _config["Email:BccEmail"], EmailSubject.ComplianceDocumentUpload, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(VersionId),
                Message = !string.IsNullOrEmpty(VersionId) ? Message.UPLOAD_SUCCESSFUL : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        public async Task<IActionResult> FetchComplianceDocuments(DatatableMetaModel meta, DocumentsSearchModel query)
        {
            var documents = _context2.ComplianceDocuments.Where(m => m.User.FirmId == FirmId);
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            var additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            bool hasComplianceRole = !string.IsNullOrEmpty(additionalRoles) && additionalRoles.Contains(AdminRoleEnum.Compliance.ToString());
            var OwnerUserId = User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value.StringToInt();
            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            if (UId > 0 && !hasComplianceRole)
                documents = documents.Where(m => m.UserId == UId);
            int rowsTotal = documents.Count();
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = string.Empty;
            if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(field) && field == "reviewedBy.userData.name")
            {
                var reviewed_doc = documents.Where(m => m.ReviewedById.HasValue).OrderBy($"reviewedBy.userData.firstName {sort},reviewedBy.userData.lastName {sort}");
                var remaining_doc = documents.Except(reviewed_doc);
                if (sort == "asc")
                    documents = remaining_doc.Concat(reviewed_doc);
                else
                    documents = reviewed_doc.Concat(remaining_doc);
            }
            else if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(field))
                sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "ReviewedBy,ReviewedBy.UserData,User,User.UserData";
            var result = await documents.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> InsuranceDocuments()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            return View();
        }
        public async Task<IActionResult> InsuranceDocumentModal(Guid guid)
        {
            var document = await _context2.InsuranceDocuments.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            ViewBag.ClientList = await _dropdownDataHelper.GetAdviserClientsAsync(UId);
            return View("_InsuranceDocumentModal", document);
        }
        [HttpPost]
        public async Task<IActionResult> InsuranceDocumentUpload(IFormFile file, InsuranceDocument insuranceDocument)
        {
            string bucketPath = $"{_config["BucketName"]}/documents/insurance";
            var document = await _context2.InsuranceDocuments.Where(m => m.Guid == insuranceDocument.Guid).Include(m => m.ClientLogin).FirstOrDefaultAsync() ?? new InsuranceDocument();
            string VersionId = string.Empty;
            if (file != null)
            {
                var uploadedFile = await _fileHelper.UploadFileToS3Bucket(file.OpenReadStream(), bucketPath, file.FileName);
                document.VersionId = uploadedFile.VersionId;
                document.FileSize = file.Length;
            }
            VersionId = document.VersionId is not null ? document.VersionId : "N/A";
            document.FileName = file != null ? file.FileName : insuranceDocument.FileUrl;
            document.ContentType = file != null ? file.ContentType : "link";
            document.Status = InsuranceDocumentStatusEnum.Pending;
            document.ReqiuredChange = false;
            document.UploadedDate = DateTime.UtcNow;
            document.AdviserNote = insuranceDocument.AdviserNote;
            document.ClientId = insuranceDocument.ClientId;
            document.InsuranceType = insuranceDocument.InsuranceType;
            document.CreatedBy = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            _context2.InsuranceDocuments.Update(document);
            await _context2.SaveChangesAsync();

            var complianceEmails = _context2.Permissions.Where(m => m.RoleType == AdminRoleEnum.Compliance && m.UserInfo.FirmId == FirmId)
                .Include(m => m.UserInfo).Select(m => m.UserInfo.Username);

            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && complianceEmails.Any())
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/compliance-document-upload.html")));
                var adviser = await _context2.UserLogins.Where(m => m.Id == UserId).Include(m => m.UserData).FirstOrDefaultAsync();
                emailTemplateHtml.Replace("@ADVISERNAME", adviser.UserData.Name);
                var fileName = file != null ? $"<span style='color:#0073e9'>{document.FileName}</span>" : $"a <a href='{insuranceDocument.FileUrl}' target='_blank'>Link</a>";
                emailTemplateHtml.Replace("@FILENAME", fileName);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(string.Join(",", complianceEmails), null, _config["Email:BccEmail"], EmailSubject.ComplianceDocumentUpload, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(VersionId),
                Message = !string.IsNullOrEmpty(VersionId) ? Message.UPLOAD_SUCCESSFUL : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        public async Task<IActionResult> FetchInsuranceDocuments(DatatableMetaModel meta, DocumentsSearchModel query)
        {
            var documents = _context2.InsuranceDocuments.Where(m => m.ClientLogin.UserLogin.Firm.Id == FirmId);

            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            var additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            bool hasComplianceRole = !string.IsNullOrEmpty(additionalRoles) && additionalRoles.Contains(AdminRoleEnum.Compliance.ToString());
            var OwnerUserId = User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value.StringToInt();

            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;

            if (UId > 0 && !hasComplianceRole)
                documents = documents.Where(m => m.CreatedBy == UId);
            int rowsTotal = documents.Count();
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            if (!string.IsNullOrEmpty(searchQuery))
                searchQuery = searchQuery.Replace("ClientData", "ClientLogin.ClientData");
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = string.Empty;
            if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(field) && field == "reviewedBy.userData.name")
            {
                var reviewed_doc = documents.Where(m => m.ReviewedById.HasValue).OrderBy($"reviewedBy.userData.firstName {sort},reviewedBy.userData.lastName {sort}");
                var remaining_doc = documents.Except(reviewed_doc);
                if (sort == "asc")
                    documents = remaining_doc.Concat(reviewed_doc);
                else
                    documents = reviewed_doc.Concat(remaining_doc);
            }
            else if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(field))
                sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "ReviewedBy,ReviewedBy.UserData,ClientLogin,ClientLogin.UserLogin,ClientLogin.UserLogin.UserData,ClientLogin.ClientData";
            var result = await documents.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> TaxDocuments()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            var years = new List<SelectListItem>();
            for (int i = 1; i <= 3; i++)
                years.Add(new SelectListItem
                {
                    Text = DateTime.UtcNow.AddYears(-i).ToString("yyyy"),
                    Value = DateTime.UtcNow.AddYears(-i).ToString("yyyy"),
                    Selected = i == 1
                });
            ViewBag.Years = years;
            return View();
        }

        public async Task<IActionResult> RegulatoryDocuments()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.UserData).Include(m => m.Permissions).Include(m => m.AdviserContactInfos).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            ViewBag.isValidDateOfBirth = adviserDataObj.UserData.DateOfBirth is not null;
            ViewBag.isValidContactInfo = adviserDataObj.AdviserContactInfos is not null;
            ViewBag.hasRIA = adviserDataObj.UserData.RIAId > 0;
            ViewBag.token = await UserLoginGuid.ToString().EncryptAsync();
            ViewBag.EmptyLayout = true;
            return View();
        }
        [AllowAnonymous]
        public async Task<IActionResult> FetchRegulatoryDocuments(DatatableMetaModel meta, int FirmId, DocumentsSearchModel query)
        {
            var documents = _context2.RegulatoryDocuments.Where(m => m.FirmId == FirmId);
            int rowsTotal = documents.Count();
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            var result = await documents.PagedListAsync(meta, null, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> RegulatoryDocumentModal(Guid guid, int firmId)
        {
            var document = await _context2.RegulatoryDocuments.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            ViewBag.firmId = firmId;
            return View("_RegulatoryDocumentModal", document);
        }
        [HttpPost]
        public async Task<IActionResult> RegulatoryDocumentUpload(IFormFile file, RegulatoryDocument regulatoryDocument)
        {
            string bucketPath = $"{_config["BucketName"]}/documents/regulatory";
            var uploadedFile = await _fileHelper.UploadFileToS3Bucket(file.OpenReadStream(), bucketPath, file.FileName);
            var document = _context2.RegulatoryDocuments.FirstOrDefault(m => m.Guid == regulatoryDocument.Guid);
            var mappedDocument = _mapper.Map(regulatoryDocument, document);
            mappedDocument.FileName = HttpUtility.UrlDecode(file.FileName);
            mappedDocument.ContentType = file.ContentType;
            mappedDocument.FileSize = file.Length;
            mappedDocument.UploadedDate = DateTime.UtcNow;
            mappedDocument.FirmId = regulatoryDocument.FirmId;
            _context2.RegulatoryDocuments.Update(mappedDocument);
            await _context2.SaveChangesAsync();
            var ragulatoryCount = _context2.RegulatoryDocuments.Count(m => m.FirmId == regulatoryDocument.FirmId);
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(uploadedFile.VersionId),
                Message = !string.IsNullOrEmpty(uploadedFile.VersionId) ? Message.UPLOAD_SUCCESSFUL : Message.INTERNAL_SERVER_ERROR,
                ParentId = regulatoryDocument.FirmId,
                Data = new { ragulatoryCount }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteRegulatory(Guid Guid)
        {
            var regulatoryDocument = await _context2.RegulatoryDocuments.Where(m => m.Guid == Guid).FirstOrDefaultAsync();
            if (regulatoryDocument is not null)
            {
                _context2.RegulatoryDocuments.Remove(regulatoryDocument);
                await _context2.SaveChangesAsync();
            }
            var ragulatoryCount = _context2.RegulatoryDocuments.Count(m => m.FirmId == regulatoryDocument.FirmId);
            var result = new CommonResponse
            {
                Success = regulatoryDocument is not null,
                Message = regulatoryDocument is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                ParentId = regulatoryDocument.FirmId,
                Data = new { ragulatoryCount }
            };
            return Json(result);
        }
        [AllowAnonymous]
        public async Task<IActionResult> AdviserDocuments(string token)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
            {
                ViewBag.StatusCode = 403;
                ViewBag.Title = "Access Denied";
                ViewBag.Message = "Sorry, You don't have permission to access this page.";
                return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
            }
            Guid adviserGuid = new(token);
            var adviser = await _context2.UserLogins.Where(m => m.Guid == adviserGuid)
                .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.Firm.RegulatoryDocuments).Include(m => m.AdviserContactInfos)
                .Include(m => m.UserData.FMO).Include(m => m.UserData.BD).Include(m => m.UserData.RIA).FirstOrDefaultAsync();
            string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
            ViewBag.EmptyLayout = true;
            ViewBag.isValidContactInfo = adviser.AdviserContactInfos is not null;
            ViewBag.isValidDateOfBirth = adviser.UserData.DateOfBirth is not null;
            ViewBag.hasRIA = adviser.UserData.RIAId > 0;
            if (!string.IsNullOrEmpty(adviser.UserData.BusinessLogoFileName))
            {
                string extension = Path.GetExtension(adviser.UserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                ViewBag.BusinessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviser.UserData.BusinessLogoFileName, contentType);
            }
            return View(adviser);
        }
        public async Task<IActionResult> ClientMessageBoard()
        {
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            user.ClientBoardMessage = !string.IsNullOrEmpty(user.ClientBoardMessage) ? HttpUtility.UrlDecode(user.ClientBoardMessage).Replace("<a", "<a target='_blank'") : string.Empty;
            return View(user);
        }
        public async Task<IActionResult> SaveClientMessageBoard(string ClientBoardMessage)
        {
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            user.ClientBoardMessage = !string.IsNullOrEmpty(ClientBoardMessage) ? HttpUtility.UrlDecode(ClientBoardMessage).Replace("<a", "<a target='_blank'") : string.Empty;
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = user != null,
                Message = user != null ? Message.SAVE_SUCCESS : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        public IActionResult CFSStudents(Guid guid)
        {
            var students = _context2.EducationPlannings.Where(m => m.ClientLogin.Guid == guid);
            return View("_CFSStudentsModal", students);
        }
        public async Task<IActionResult> CFSForm(Guid StudentGuid)
        {
            var student = await _context2.EducationPlannings.Where(m => m.Guid == StudentGuid)
                .Include(m => m.ClientLogin).Include(m => m.ClientLogin.UserLogin).FirstOrDefaultAsync();
            var client = await _context2.ClientLogins.Where(m => m.Guid == student.ClientLogin.Guid).Include(m => m.ClientData).FirstOrDefaultAsync();
            string CFSStudentId = student.CFSStudentId;
            CFSModel CFSStudent = !string.IsNullOrEmpty(CFSStudentId) ? await _cfsService.GetCFSStudentAsync(student.ClientLogin.UserLogin.Guid, CFSStudentId) : new CFSModel();
            CFSStudent.StudentGuid = StudentGuid;
            CFSStudent.student = student.StudentName;
            CFSStudent.student_years_until_college = student.NumberOfUears;
            if (!string.IsNullOrEmpty(student.State))
            {
                var statesJson = JArray.Parse(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "JSON/states_titlecase.json")));
                var stateAbbreviation = statesJson.FirstOrDefault(n => n["name"].Value<string>() == student.State);
                if (stateAbbreviation != null)
                    CFSStudent.student_state = stateAbbreviation["abbreviation"].ToString();
            }
            if (student.LegalGuardian)
            {
                AgeModel ageData = await _commonService.GetAgeData(student.ClientLogin.Guid);
                CFSStudent.age_of_oldest_parent = new int[] { Convert.ToInt32(ageData.Client.Age), Convert.ToInt32(ageData.Spouse.Age) }.Max();
                CFSStudent.tax_return_filed = client.ClientData.TaxFilingType == TaxFilingTypeEnum.Joint ? "Married" : "Individual";
                CFSStudent.parents_marital_status = client.ClientData.MaritalStatus == MaritalStatusEnum.Widowed || client.ClientData.MaritalStatus == MaritalStatusEnum.Unmarried ? "Single" : client.ClientData.MaritalStatus.ToString();
                int clientOccupationIncome = (await _employmentService.GetEmploymentDataAsync(student.ClientLogin.Guid, ClientTypeEnum.Client)).Where(m => m.AgeToRetire > ageData.Client.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
                bool isMarried = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null && Convert.ToDateTime(client.ClientData.DateOfCurrentMarriage).IsNotEmptyDate();
                int spouseOccupationIncome = 0;
                if (isMarried)
                    spouseOccupationIncome = (await _employmentService.GetEmploymentDataAsync(student.ClientLogin.Guid, ClientTypeEnum.Spouse)).Where(m => m.AgeToRetire > ageData.Spouse.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
                CFSStudent.parents_w2 = clientOccupationIncome;
                CFSStudent.spouse_w2 = spouseOccupationIncome;
                var clientProperties = await _context2.ClientProperties.Where(m => m.ClientLogin.Guid == student.ClientLogin.Guid).ToListAsync();
                var totalResidenceProperties = clientProperties.Where(m => m.Type == PropertyTypeEnum.Primary).Sum(m => m.CurrentValue - m.PricePaid);
                var totalOtherProperties = clientProperties.Where(m => m.Type == PropertyTypeEnum.Secondary || m.Type == PropertyTypeEnum.Vacation || m.Type == PropertyTypeEnum.RealEstate).Sum(m => m.CurrentValue - m.PricePaid);
                CFSStudent.parents_residence_equity = totalResidenceProperties > 0 ? totalResidenceProperties : 0;
                CFSStudent.parents_other_real_estate_equity = totalOtherProperties > 0 ? totalOtherProperties : 0;
            }
            return View("_CFSForm", CFSStudent);
        }
        [HttpPost]
        public async Task<IActionResult> SaveCFSForm(CFSModel model)
        {
            var student = await _context2.EducationPlannings.FirstOrDefaultAsync(m => m.Guid == model.StudentGuid);
            await _cfsService.UpdateCFSStudentAsync(UserLoginGuid, model);
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(student.CFSStudentId),
                Message = !string.IsNullOrEmpty(student.CFSStudentId) ? Message.CFS_STUDENT_SAVED : Message.NO_RECORD_FOUND,
                Data = new { student.CFSStudentId }
            };
            return Json(result);
        }
        public IActionResult ReleaseNotes()
        {
            var role = User.FindFirst(ClaimTypes.Role)?.Value;
            var releaseNotes = _context2.ReleaseNotes.OrderByDescending(m => m.Id).ToList();
            ViewBag.EmptyLayout = role != Roles.SuperAdmin;
            return View(releaseNotes);
        }
        public async Task<IActionResult> ActivityLog(string token, int tz)
        {
            token = await token.DecryptAsync();
            if (token != "InvalidToken")
            {
                Guid guid = new(token);
                ViewBag.EmptyLayout = true;
                var client = await _context2.ClientLogins.Where(m => m.Guid == guid)
                    .Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
                var clientName = client.ClientData.FirstName;
                var spouseName = client.ClientData.MaritalStatus == MaritalStatusEnum.Married && client.SpouseData != null ? client.SpouseData.FirstName : null;
                string[] names = { clientName, spouseName };
                ViewBag.Names = names;
                var activities = _context2.ClientActivityLogs.Where(m => m.Owner.Guid == guid && m.ParentId == null)
                    .Include(m => m.Owner).Include(m => m.Owner.ClientData).Include(m => m.CreatedByUser).Include(m => m.CreatedByUser.UserData).OrderByDescending(m => m.TimeStamp);
                ViewBag.TimeZone = tz;
                return View(activities);
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        public async Task<IActionResult> TaxPrepQueueActivityLog(string token, int tz)
        {
            token = await token.DecryptAsync();
            if (token != "InvalidToken")
            {
                Guid guid = new(token);
                ViewBag.EmptyLayout = true;
                var taxPrepHistory = await _context2.TaxPrepHistories.Where(m => m.ClientLogin.Guid == guid)
                .Include(m => m.ClientLogin).Include(m => m.ClientLogin.ClientData).Include(m => m.ActionByUser).Include(m => m.ActionByUser.UserData).OrderByDescending(m => m.CreatedDate).ToListAsync();
                ViewBag.TimeZone = tz;
                return View(taxPrepHistory);
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _fileHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~HomeController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}