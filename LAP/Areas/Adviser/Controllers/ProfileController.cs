﻿using AutoMapper;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.IO;
using LAP.Areas.Adviser.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using LAP.Services;
using LAP.DomainModels.Models;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Admin;

namespace LAP.Areas.Adviser.Controllers
{
    [Area(Roles.Adviser)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class ProfileController : BaseController, IDisposable
    {
        protected readonly CommonService _commonService;
        protected readonly FileHelper _fileHelper;
        protected readonly DropdownDataHelper _dropdownDataHelper;
        protected readonly Guid UserLoginGuid;
        protected readonly int UserId;
        protected readonly Guid AdviserUserLoginGuid;
        protected readonly Guid UserGuid;
        public ProfileController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _commonService = new CommonService(_context2);
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _fileHelper = new FileHelper(_config);
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            UserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").ClaimToInt();
            AdviserUserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "AdviserUserLoginGuid").ClaimToGuid();
            UserGuid = _httpContextAccessor.HttpContext.Request.Query["flag"] == "true" ? AdviserUserLoginGuid : UserLoginGuid;
        }
        public IActionResult Index()
        {
            ViewBag.EmptyLayout = true;
            return View("WizardContainer");
        }
        public async Task<IActionResult> Wizard()
        {
            var wizardItems = await MenuDataHelper.GetAdviserWizardItemDataAsync();
            ViewBag.WizardItems = wizardItems;
            return View("_AdviserWizard");
        }
        public async Task<IActionResult> Info()
        {
            var adviserData = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserGuid).Include(m => m.UserLogin).FirstOrDefaultAsync();
            if (adviserData is not null)
                adviserData.DateOfBirth = Convert.ToDateTime(adviserData.DateOfBirth).IsNotEmptyDate() ? adviserData.DateOfBirth : null;
            ViewBag.nextPrev = new PrevNextMenuModel { NextTitle = "Contact Info", NextAction = "adviser/contact-info" };
            string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
            if (!string.IsNullOrEmpty(adviserData.AvatarFileName))
            {
                string extension = Path.GetExtension(adviserData.AvatarFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                ViewBag.avatarImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.AvatarFileName, contentType);
            }
            if (!string.IsNullOrEmpty(adviserData.BusinessLogoFileName))
            {
                string extension = Path.GetExtension(adviserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                ViewBag.businessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.BusinessLogoFileName, contentType);
            }
            return View("_AdviserInfo", adviserData);
        }
        [HttpPost]
        public async Task<IActionResult> SaveAdviserInfo(UserData userData)
        {
            var adviserDataObj = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            adviserDataObj.Salutation = userData.Salutation;
            adviserDataObj.FirstName = userData.FirstName;
            adviserDataObj.MiddleInitial = userData.MiddleInitial;
            adviserDataObj.LastName = userData.LastName;
            adviserDataObj.DateOfBirth = userData.DateOfBirth;
            adviserDataObj.BusinessName = userData.BusinessName;
            adviserDataObj.RoleTitle = userData.RoleTitle;
            adviserDataObj.Tagline = userData.Tagline;
            string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
            string suffix = $"{adviserDataObj.UserId}_{UserLoginGuid}";

            if (!string.IsNullOrEmpty(userData.AvatarFileBase64String))
            {
                var avatarExtension = Path.GetExtension(userData.AvatarFileName);
                var avatarByteArray = Convert.FromBase64String(userData.AvatarFileBase64String.Replace($"data:image/{avatarExtension.Replace(".", "")};base64,", ""));
                var avatarFileStream = new MemoryStream(avatarByteArray);
                var avatarFile = await _fileHelper.UploadFileToS3Bucket(avatarFileStream, bucketPath, $"avatar_{suffix}{avatarExtension}");
                adviserDataObj.AvatarFileName = avatarFile.Key;
            }
            else
                adviserDataObj.AvatarFileName = !string.IsNullOrEmpty(userData.AvatarFileName) ? !userData.AvatarFileName.Contains(@"C:\") ? userData.AvatarFileName : adviserDataObj.AvatarFileName : null;

            if (userData.BusinessLogoFile is not null)
            {
                var businessLogoFileExtension = Path.GetExtension(userData.BusinessLogoFile.FileName);
                var businessFile = await _fileHelper.UploadFileToS3Bucket(userData.BusinessLogoFile.OpenReadStream(), bucketPath, $"business_logo_{suffix}{businessLogoFileExtension}");
                adviserDataObj.BusinessLogoFileName = businessFile.Key;
            }
            else
                adviserDataObj.BusinessLogoFileName = !string.IsNullOrEmpty(userData.BusinessLogoFileName) ? userData.BusinessLogoFileName : null;
            await _context2.SaveChangesAsync();

            var result = new CommonResponse
            {
                Success = userData is not null,
                Message = userData is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { AdviserFirstName = userData.FirstName, AdviserLastName = userData.LastName, AvatarImageSrc = userData.AvatarFileBase64String }
            };
            return Json(result);
        }
        public async Task<IActionResult> ContactInfo()
        {
            var adviserContactInfos = await _context2.AdviserContactInfos.Where(m => m.UserLogin.Guid == UserGuid).ToListAsync();

            List<ContactInfoViewModel> ContactInfoMap = _mapper.Map<List<ContactInfoViewModel>>(adviserContactInfos);
            if (!adviserContactInfos.Any(m => m.AddressType == AddressTypeEnum.Resident))
            {
                ContactInfoMap.Add(new ContactInfoViewModel
                {
                    AddressType = AddressTypeEnum.Resident,
                });
            }
            if (!adviserContactInfos.Any(m => m.AddressType == AddressTypeEnum.Business))
            {
                ContactInfoMap.Add(new ContactInfoViewModel
                {
                    AddressType = AddressTypeEnum.Business
                });
            }
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Adviser Info", PrevAction = "adviser/info", NextTitle = "Education Info", NextAction = "adviser/education-info" };
            return View("_ContactInfo", ContactInfoMap);
        }
        [HttpPost]
        public async Task<IActionResult> SaveContactInfo(List<ContactInfoViewModel> adviserContacts)
        {
            var contactInfoDatas = await _context2.AdviserContactInfos.Where(m => m.UserLogin.Guid == UserLoginGuid).AsNoTracking().ToListAsync();
            bool hasChanges = contactInfoDatas.Any(x => adviserContacts.Any(y => y.Guid == x.Guid &&
                                            (y.Address != x.Address ||
                                            y.Address2 != x.Address2 ||
                                            y.CellNumber != x.CellNumber ||
                                            y.POBox != x.POBox ||
                                            y.City != x.City ||
                                            y.Phone != x.Phone ||
                                            y.State != x.State ||
                                            y.ZipCode != x.ZipCode)));
            if (hasChanges)
            {
                var adviser = await _context2.UserLogins.FirstOrDefaultAsync(m => m.Guid == UserLoginGuid);
                adviser.HasChanges = true;
            }
            var ContactInfoMap = _mapper.Map(adviserContacts, contactInfoDatas);
            ContactInfoMap = ContactInfoMap.Select(m => { m.UserId = UserId; return m; }).ToList();
            _context2.AdviserContactInfos.UpdateRange(ContactInfoMap);

            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = adviserContacts.Any(),
                Message = adviserContacts.Any() ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        public async Task<IActionResult> DeleteContactInfo(Guid Guid)
        {
            var contactInfo = await _context2.AdviserContactInfos.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (contactInfo is not null)
            {
                _context2.AdviserContactInfos.Remove(contactInfo);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = contactInfo is not null,
                Message = contactInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND
            };
            return Json(result);
        }
        public async Task<IActionResult> EducationInfo()
        {
            var adviserData = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserGuid).Include(m => m.UserLogin).FirstOrDefaultAsync();
            var adviserEducation = new EducationInfoModel
            {
                DegreeDesignations = !string.IsNullOrEmpty(adviserData.DegreeDesignation) ? adviserData.DegreeDesignation.Split(",").Select(int.Parse).ToList() : new List<int>(),
                Licenses = !string.IsNullOrEmpty(adviserData.License) ? adviserData.License.Split(",").Select(int.Parse).ToList() : new List<int>(),
                CRD = adviserData.CRD,
                NPN = adviserData.NPN
            };
            var postHighSchoolEducations = _context2.PostHighSchoolEducations.Where(m => m.UserLogin.Guid == UserGuid);
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Contact Info", PrevAction = "adviser/contact-info", NextTitle = "Work History", NextAction = "adviser/work-history" };
            ViewBag.PostHighSchoolEducations = postHighSchoolEducations;
            return View("Education/_EducationInfo", adviserEducation);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEducationInfo(EducationInfoModel educationInfoModel)
        {
            var adviserData = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            adviserData.DegreeDesignation = (educationInfoModel.DegreeDesignations?.Count) > 0 ? string.Join(",", educationInfoModel.DegreeDesignations) : null;
            adviserData.License = educationInfoModel.Licenses?.Count > 0 ? string.Join(",", educationInfoModel.Licenses) : null;
            adviserData.CRD = educationInfoModel.CRD;
            adviserData.NPN = educationInfoModel.NPN;
            if (!educationInfoModel.HasPostHighSchoolEducation)
            {
                var postHighSchoolEducations = _context2.PostHighSchoolEducations.Where(m => m.UserLogin.Guid == UserLoginGuid);
                _context2.PostHighSchoolEducations.RemoveRange(postHighSchoolEducations);
            }
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = adviserData.UserId > 0,
                Message = adviserData.UserId > 0 ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        public async Task<IActionResult> PostHighSchoolEducation(Guid guid)
        {
            var education = await _context2.PostHighSchoolEducations.Where(m => m.UserLogin.Guid == UserGuid && m.Guid == guid).FirstOrDefaultAsync();
            ViewBag.FieldOfStudy = education is not null ? await DropdownDataHelper.GetFieldOfStudyAsync(education.Degree.ToString()) : null;
            return View("Education/_PostHighSchoolEducationModal", education);
        }
        public async Task<IEnumerable<SelectListItem>> GetFieldOfStudy(string degree)
        {
            return await DropdownDataHelper.GetFieldOfStudyAsync(degree);
        }
        [HttpPost]
        public async Task<IActionResult> SavePostHighSchoolEducation(PostHighSchoolEducation postHighSchoolEducation)
        {
            var postHighSchoolEducationObj = await _context2.PostHighSchoolEducations
                .Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid == postHighSchoolEducation.Guid).FirstOrDefaultAsync();
            var postHighSchoolEducationMap = _mapper.Map(postHighSchoolEducation, postHighSchoolEducationObj);
            postHighSchoolEducationMap.UserId = UserId;
            _context2.PostHighSchoolEducations.Update(postHighSchoolEducationMap);
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = postHighSchoolEducationMap is not null,
                Message = postHighSchoolEducationMap is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { educationInfo = postHighSchoolEducationMap }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteEducation(Guid Guid)
        {
            var educationInfo = await _context2.PostHighSchoolEducations.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (educationInfo is not null)
            {
                _context2.PostHighSchoolEducations.Remove(educationInfo);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = educationInfo is not null,
                Message = educationInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        public IActionResult WorkHistory()
        {
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Education Info", PrevAction = "adviser/education-info", NextTitle = "My Business", NextAction = "adviser/business" };
            var works = _context2.Works.Where(m => m.UserLogin.Guid == UserGuid).OrderByDescending(m => m.IsPresent);
            return View("Work/_WorkHistory", works);
        }
        public async Task<IActionResult> WorkInfo(Guid guid)
        {
            var work = await _context2.Works.Where(m => m.UserLogin.Guid == UserGuid && m.Guid == guid).FirstOrDefaultAsync();
            return View("Work/_WorkModal", work);
        }
        [HttpPost]
        public async Task<IActionResult> SaveWork(Work work)
        {
            var workObj = await _context2.Works.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid == work.Guid).FirstOrDefaultAsync();
            var workMap = _mapper.Map(work, workObj);
            workMap.UserId = UserId;
            _context2.Works.Update(workMap);
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = workMap is not null,
                Message = workMap is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { workInfo = workMap }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteWork(Guid Guid)
        {
            var workInfo = await _context2.Works.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid == Guid).FirstOrDefaultAsync();
            if (workInfo is not null)
            {
                _context2.Works.Remove(workInfo);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = workInfo is not null,
                Message = workInfo is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND
            };
            return Json(result);
        }
        public async Task<IActionResult> Business()
        {
            var adviserData = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserGuid)
                .Include(m => m.FMO).Include(m => m.BD).Include(m => m.RIA).FirstOrDefaultAsync();
            ViewBag.FMO = adviserData.FMO is not null ? adviserData.FMO : new Affiliation();
            ViewBag.BD = adviserData.BD is not null ? adviserData.BD : new Affiliation();
            ViewBag.RIA = adviserData.RIA is not null ? adviserData.RIA : new Affiliation();
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "Work History", PrevAction = "adviser/work-history", NextTitle = "Regulatory", NextAction = "adviser/regulatory" };
            var result = new MyBusinessModel
            {
                FieldMarketingOrganization = Convert.ToInt32(adviserData.FMOId),
                BrokerDealer = Convert.ToInt32(adviserData.BDId),
                RegisteredInvestmentAdvisor = Convert.ToInt32(adviserData.RIAId),
                BDDisclosure = adviserData.BDDisclosure,
                RIADisclosure = adviserData.RIADisclosure,
                CompensationType = adviserData.CompensationType
            };
            return View("Business/_MyBusiness", result);
        }
        public async Task<Select2Model> GetAffiliations(AffiliationTypeEnum type, string term, int page)
        {
            return await _dropdownDataHelper.GetAffiliationsAsync(type, term, page);
        }
        public async Task<IActionResult> Affiliation(Guid guid)
        {
            var affiliation = await _context2.Affiliations.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            return View("Business/_AffiliationModal", affiliation);
        }
        [HttpPost]
        public async Task<IActionResult> SaveAffiliation(Affiliation affiliation)
        {
            var affiliationObj = await _context2.Affiliations.Where(m => m.Guid == affiliation.Guid).FirstOrDefaultAsync();
            var affiliationMapper = _mapper.Map(affiliation, affiliationObj);
            _context2.Affiliations.Update(affiliationMapper);
            await _context2.SaveChangesAsync();
            var adviserCount = await _context2.Affiliations.CountAsync(m => m.Type == affiliationMapper.Type);
            var result = new CommonResponse
            {
                Success = affiliation is not null,
                Message = affiliation is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new
                {
                    affiliationMapper.Id,
                    affiliationMapper.Guid,
                    affiliationMapper.Name,
                    Type = (int)affiliationMapper.Type,
                    TypeString = affiliationMapper.Type.ToString(),
                    AdviserCount = adviserCount
                },
                ActionType = affiliation.Guid != Guid.Empty ? "update" : "add"
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveBusiness(MyBusinessModel myBusiness)
        {
            var adviserData = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            adviserData.FMOId = myBusiness.FieldMarketingOrganization > 0 ? myBusiness.FieldMarketingOrganization : null;
            adviserData.BDId = myBusiness.BrokerDealer > 0 ? myBusiness.BrokerDealer : null;
            adviserData.RIAId = myBusiness.RegisteredInvestmentAdvisor > 0 ? myBusiness.RegisteredInvestmentAdvisor : null;
            adviserData.BDDisclosure = myBusiness.BDDisclosure;
            adviserData.RIADisclosure = myBusiness.RIADisclosure;
            adviserData.CompensationType = myBusiness.CompensationType;
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = adviserData.UserId > 0,
                Message = adviserData.UserId > 0 ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        public async Task<IActionResult> Regulatory()
        {
            ViewBag.RegulatoryQuestions = await _commonService.GetRegulatoryQuestionsAsync(UserGuid);
            ViewBag.nextPrev = new PrevNextMenuModel { PrevTitle = "My Business", PrevAction = "adviser/business", NextTitle = "", NextAction = "" };
            return View("_Regulatory");
        }
        [HttpPost]
        public async Task<IActionResult> SaveRegulatory(IEnumerable<Regulatory> regulatories)
        {
            regulatories = regulatories.Select(m => { m.UserId = UserId; return m; });
            var regulatory = await _commonService.GetRegulatoryQuestionsAsync(UserLoginGuid);
            bool hasChanges = regulatory.Any(x => regulatories.Any(y => y.Guid == x.Guid && y.Answer != x.Answer));
            if (hasChanges)
            {
                var adviser = await _context2.UserLogins.FirstOrDefaultAsync(m => m.Guid == UserLoginGuid);
                adviser.HasChanges = true;
            }
            _context2.Regulatories.UpdateRange(regulatories);
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = regulatories.Any(),
                Message = regulatories.Any() ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }

        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                    _fileHelper.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~ProfileController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}