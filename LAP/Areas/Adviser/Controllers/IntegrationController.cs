﻿using AutoMapper;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System;
using System.Threading.Tasks;
using LAP.Services.Integration;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;

namespace LAP.Areas.Adviser.Controllers
{
    [Area(Roles.Adviser)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class IntegrationController : BaseController, IDisposable
    {
        protected readonly Guid UserLoginGuid;
        private readonly int UserId;
        private readonly RedtailService _redtailService;
        private readonly CFSService _cfsService;
        private readonly OmniService _omniService;
        private readonly FusionService _fusionService;
        public IntegrationController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            UserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").ClaimToInt();
            _redtailService = new RedtailService(_context2, _config);
            _cfsService = new CFSService(_context2, _config, _env);
            _omniService = new OmniService(_context2, _config, _env);
            _fusionService = new FusionService(_context2, _config, _env);
        }
        [HttpPost]
        public async Task<IActionResult> AuthRedtail(LoginModel model, bool active = true)
        {
            bool isAuthenticated = await _redtailService.Authenticate(model, UserLoginGuid, active);
            string parnerName = "Redtail";
            var result = new CommonResponse
            {
                Success = isAuthenticated && active,
                Message = isAuthenticated && active ? string.Format(Message.COFIGURE_INTEGRATION_SUCCESS, parnerName) : active ? Message.INVALID_USERNAME_PASSWORD : string.Format(Message.DEACTIVE_INTEGRATION_SUCCESS, parnerName),
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> AuthCollegiateFundingSolutions(LoginModel model, bool active = true)
        {
            bool isAuthenticated = await _cfsService.Authenticate(model, UserLoginGuid, UserId, active);
            string parnerName = "Collegiate Funding Solutions";
            var result = new CommonResponse
            {
                Success = isAuthenticated && active,
                Message = isAuthenticated && active ? string.Format(Message.COFIGURE_INTEGRATION_SUCCESS, parnerName) : active ? Message.INVALID_USERNAME_PASSWORD : string.Format(Message.DEACTIVE_INTEGRATION_SUCCESS, parnerName),
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> AuthOmniscient(LoginModel model, bool active = true)
        {
            bool isAuthenticated = await _omniService.Authenticate(model, UserLoginGuid, active);
            string parnerName = "Omniscient";
            var result = new CommonResponse
            {
                Success = isAuthenticated && active,
                Message = isAuthenticated && active ? string.Format(Message.COFIGURE_INTEGRATION_SUCCESS, parnerName) : active ? Message.INVALID_USERNAME_PASSWORD : string.Format(Message.DEACTIVE_INTEGRATION_SUCCESS, parnerName),
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> AuthFusionElements(LoginModel model, bool active = true)
        {
            dynamic response = await _fusionService.Authenticate(model, UserLoginGuid, active);
            bool isAuthenticated = response.isAuthenticated;
            string parnerName = "Fusion Elements";
            var result = new CommonResponse
            {
                Success = isAuthenticated && active,
                Message = isAuthenticated && active ? string.Format(Message.COFIGURE_INTEGRATION_SUCCESS, parnerName) : active ? response.msg : string.Format(Message.DEACTIVE_INTEGRATION_SUCCESS, parnerName),
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~IntegrationController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}