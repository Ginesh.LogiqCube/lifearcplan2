﻿using AutoMapper;
using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels.Models;
using LAP.Helpers;
using LAP.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Areas.Adviser.Controllers
{
    [Area(Roles.Adviser)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class DashboardController : BaseController, IDisposable
    {
        protected readonly Guid UserLoginGuid;
        protected readonly int UserId;
        protected readonly int TimeZone;
        private readonly int FirmId;
        private readonly int OwnerUserId;
        public DashboardController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            UserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").ClaimToInt();
            TimeZone = Convert.ToInt32(_httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value);
            FirmId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "FirmId").ClaimToInt();
            OwnerUserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId").ClaimToInt();
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult FetchEventChartData()
        {
            var eventTypes = Enum.GetValues(typeof(EventTypeEnum)).Cast<EventTypeEnum>()
                .Select(r => new { Value = (int)r, Name = r.ToString() }).ToList();
            var eventsData = _context2.Events.Where(m => m.UserLogin.Guid == UserLoginGuid);

            var eventData = eventTypes.SelectMany
            (
                eventType => eventsData.Where(activityEvent => (int)activityEvent.Type == eventType.Value).DefaultIfEmpty(),
                (a, b) => new
                {
                    EventType = a.Value,
                    TotalCount = eventsData.Count(m => (int)m.Type == a.Value),
                }
            ).Distinct().OrderBy(m => m.EventType).ToList();
            return Json(eventData.Select(m => m.TotalCount));
        }
        public IActionResult FetchProspectAppointmentChartData()
        {
            var prospectAppointmentStages = Enum.GetValues(typeof(ClientAppointmentPurposeEnum)).Cast<ClientAppointmentPurposeEnum>()
                .Select(r => new { Value = (int)r, Name = r.ToString() }).Where(m => Convert.ToInt32(m.Value) <= 5).ToList();
            var prospectAppointments = _context2.ProspectAppointments.Where(m => m.Lead.UserLogin.Guid == UserLoginGuid && !m.FallOut);
            var prospectAppointmentData = prospectAppointmentStages.SelectMany
            (
                appointmentStage => prospectAppointments.Where(appointment => (int)appointment.Stage == appointmentStage.Value).DefaultIfEmpty(),
                (a, b) => new
                {
                    Stage = a.Value,
                    TotalCount = prospectAppointments.Count(m => (int)m.Stage == a.Value),
                }
            ).Distinct().OrderBy(m => m.Stage).ToList();
            return Json(prospectAppointmentData.Select(m => m.TotalCount));
        }
        public IActionResult FetchClientAppointmentChartData()
        {
            var clientAppointmentPurposes = Enum.GetValues(typeof(ClientAppointmentPurposeEnum)).Cast<ClientAppointmentPurposeEnum>()
                .Select(r => new { Value = (int)r, Name = r.ToString() });
            var clientAppointments = _context2.ClientAppointments.Where(m => m.UserLogin.Guid == UserLoginGuid);
            var clientAppointmentData = clientAppointmentPurposes.SelectMany
            (
                appointmentPurpose => clientAppointments.Where(appointment => (int)appointment.Purpose == appointmentPurpose.Value).DefaultIfEmpty(),
                (a, b) => new
                {
                    Stage = a.Value,
                    TotalCount = clientAppointments.Count(m => (int)m.Purpose == a.Value),
                }
            ).Distinct().OrderBy(m => m.Stage).ToList();
            return Json(clientAppointmentData.Select(m => m.TotalCount));
        }
        public IActionResult Activities()
        {
            return View();
        }
        public async Task<IActionResult> AddEvent(Guid guid)
        {
            var activityEvent = await _context2.Events.Where(m => m.Guid == guid && m.IsDeleted != true)
                .Include(m => m.Leads).FirstOrDefaultAsync();
            return View("_AddEventModal", activityEvent);
        }
        [HttpPost]
        public async Task<IActionResult> SaveEvent(ActivityEvent activity, List<Lead> leads, bool confirmed = false)
        {
            var activityEvent = await _context2.Events.Include(m => m.Leads).AsNoTracking().Where(m => m.Guid == activity.Guid).FirstOrDefaultAsync();
            var eventDataObj = _mapper.Map(activity, activityEvent);
            eventDataObj.UserId = UserId;
            bool exist = false;
            if (activity.Date != activityEvent?.Date)
                exist = await CheckActivityExistOrHolidayAsync(eventDataObj.Date, null, eventDataObj.Guid, false);
            if (!exist || confirmed)
            {
                if (leads.Any())
                {
                    leads = leads.Select(m =>
                    {
                        m.UserId = UserId;
                        m.Active = m.Guid == Guid.Empty || m.Active;
                        m.LeadHistories = m.Guid == Guid.Empty ? new List<LeadHistory>
                            {
                        new LeadHistory
                        {
                            LeadId = m.Id,
                            Stage = ProspectAppointmentStageEnum.Lead,
                            Default = true
                        }
                            } : null;
                        return m;
                    }).ToList();
                    eventDataObj.Leads = leads;
                }
                _context2.Events.Update(eventDataObj);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = !exist || confirmed,
                Message = exist && !confirmed ? Message.NOTAVAILABLE : activityEvent is not null ? Message.SAVE_SUCCESS : Message.RECORD_ADD
            };
            return Json(result);
        }
        public async Task<IActionResult> AddProspectAppointment(Guid guid)
        {
            var activityEvent = await _context2.ProspectAppointments.Where(m => m.Guid == guid)
                .Include(m => m.Lead).FirstOrDefaultAsync();
            return View("_AddProspectAppointmentModal", activityEvent);
        }
        public async Task<IActionResult> AddClientAppointment(Guid guid)
        {
            var activityEvent = await _context2.ClientAppointments.Where(m => m.Guid == guid)
                .Include(m => m.Leads).FirstOrDefaultAsync();
            if (activityEvent != null)
            {
                activityEvent.Date = activityEvent.StartDateTime.Value;
                activityEvent.Time = activityEvent.StartDateTime.Value;
            }
            return View("_AddClientAppointmentModal", activityEvent);
        }
        [HttpPost]
        public async Task<IActionResult> SaveProspectAppointment(ProspectAppointment prospectAppointment, bool confirmed = false)
        {
            var appointment = await _context2.ProspectAppointments.AsNoTracking().Where(m => m.Guid == prospectAppointment.Guid)
                .Include(m => m.Lead).Include(m => m.Lead.LeadHistories).FirstOrDefaultAsync();
            prospectAppointment.StartDateTime = prospectAppointment.Date.Add(prospectAppointment.StartTime.TimeOfDay);
            prospectAppointment.EndDateTime = prospectAppointment.Date.Add(prospectAppointment.EndTime.TimeOfDay);
            var activityDataObj = _mapper.Map(prospectAppointment, appointment);
            bool exist = false;
            if (prospectAppointment.StartDateTime != appointment?.StartDateTime || prospectAppointment.EndDateTime != appointment?.EndDateTime)
                exist = await CheckActivityExistOrHolidayAsync(Convert.ToDateTime(activityDataObj.StartDateTime), activityDataObj.EndDateTime, activityDataObj.Guid);
            if (!exist || confirmed)
            {
                _context2.ProspectAppointments.Update(activityDataObj);

                if (appointment.Stage == prospectAppointment.Stage)
                {
                    var lastLeadHistory = appointment.Lead.LeadHistories.LastOrDefault();
                    var lastHistory = await _context2.LeadHistories.FirstOrDefaultAsync(m => m.Guid == lastLeadHistory.Guid);
                    lastHistory.Default = false;
                    _context2.LeadHistories.Update(lastHistory);
                }
                _context2.LeadHistories.Add(new LeadHistory
                {
                    StartDateTime = prospectAppointment.StartDateTime,
                    EndDateTime = prospectAppointment.EndDateTime,
                    Stage = prospectAppointment.Stage,
                    LeadId = prospectAppointment.LeadId,
                    Lead = activityDataObj.Lead,
                    Status = prospectAppointment.Status,
                    Default = true
                });
                await _context2.SaveChangesAsync();
            }
            Lead leadDetail = activityDataObj.Lead ?? appointment.Lead;
            var hasEventAppintment = (activityDataObj?.Lead?.EventId > 0 || activityDataObj?.Lead?.ClientAppointmentId > 0) ? "1" : "0";
            var ItemTitle = $@"<div class='d-flex align-items-center'>
                                    <input type='hidden' value='{activityDataObj.Guid}' class='guid'/>
                        	        <div class='symbol symbol-light-primary mr-3 initial'>
                        	            <span class='symbol-label'>{leadDetail.Name.GetInitials()}</span>
                        	        </div>
                        	        <div class='d-flex flex-column align-items-start'>
                        	            <span class='text-dark-50 font-weight-bold mb-1'>{leadDetail.Name}</span>
                        	            <button class='btn label label-inline label-outline-primary font-weight-bold status' data-guid='{activityDataObj.Guid}' data-has-event-appointment='{hasEventAppintment}'>{activityDataObj.Status.GetDisplayName()}</button>
                                    </div>
                                </div>";
            var result = new CommonResponse
            {
                Success = !exist || confirmed,
                Message = exist && !confirmed ? Message.NOTAVAILABLE : appointment is not null ? Message.SAVE_SUCCESS : Message.RECORD_ADD,
                Data = new { TargetBoardId = (int)activityDataObj.Stage, appointment?.Guid, Title = ItemTitle }
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveClientAppointment(ClientAppointment clientAppointment, List<Lead> leads, bool confirmed)
        {
            var appointment = await _context2.ClientAppointments.AsNoTracking().Where(m => m.Guid == clientAppointment.Guid).FirstOrDefaultAsync();
            clientAppointment.StartDateTime = clientAppointment.Date.Add(clientAppointment.Time.TimeOfDay);
            var activityDataObj = _mapper.Map(clientAppointment, appointment);
            bool exist = false;
            if (clientAppointment.StartDateTime != appointment?.StartDateTime)
                exist = await CheckActivityExistOrHolidayAsync(Convert.ToDateTime(activityDataObj.StartDateTime), null, activityDataObj.Guid);
            activityDataObj.UserId = UserId;
            if (!exist || confirmed)
            {
                if (leads.Any())
                {
                    leads = leads.Select(m =>
                    {
                        m.UserId = UserId;
                        m.Active = m.Guid == Guid.Empty || m.Active;
                        m.LeadHistories = m.Guid == Guid.Empty ? new List<LeadHistory>
                        {
                        new LeadHistory
                        {
                            LeadId = m.Id,
                            Stage = ProspectAppointmentStageEnum.Lead,
                            Default = true
                        }
                        } : null;
                        return m;
                    }).ToList();
                    activityDataObj.Leads = leads;
                }
                _context2.ClientAppointments.Update(activityDataObj);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = !exist || confirmed,
                Message = exist && !confirmed ? Message.NOTAVAILABLE : appointment is not null ? Message.SAVE_SUCCESS : Message.RECORD_ADD
            };
            return Json(result);
        }
        public IActionResult GetActivities(DateTime start, DateTime end)
        {
            var events = _context2.Events.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Date >= start && m.Date <= end);
            List<CalenderModel> activities = new();
            foreach (var item in events)
                activities.Add(new CalenderModel
                {
                    Id = item.Guid,
                    Title = item.Title,
                    Start = item.Date,
                    Color = "#26b99a",
                    EventType = ActivityTypeEnum.Event,
                    AllDay = false
                });
            var clientAppointments = _context2.ClientAppointments.Where(m => m.UserLogin.Guid == UserLoginGuid && m.StartDateTime != null).ToList();
            clientAppointments = clientAppointments.Where(m => m.StartDateTime.Value.BetweenDate(start, end)).ToList();
            foreach (var item in clientAppointments)
            {
                activities.Add(new CalenderModel
                {
                    Id = item.Guid,
                    Title = item.Subject,
                    Start = Convert.ToDateTime(item.StartDateTime),
                    Color = "#8950FC",
                    EventType = ActivityTypeEnum.ClientAppointment,
                    AllDay = false
                });
            }

            var prospectAppointments = _context2.ProspectAppointments.Where(m => m.Lead.UserLogin.Guid == UserLoginGuid && m.StartDateTime != null && m.EndDateTime != null && !m.FallOut).ToList();
            prospectAppointments = prospectAppointments.Where(m => m.StartDateTime.Value.BetweenDate(start, end) || m.EndDateTime.Value.BetweenDate(start, end)).ToList();
            foreach (var item in prospectAppointments)
            {
                bool AllDay = Convert.ToDateTime(item.EndDateTime).Subtract(Convert.ToDateTime(item.StartDateTime)).TotalDays >= 1;
                activities.Add(new CalenderModel
                {
                    Id = item.Guid,
                    Title = item.Subject,
                    Start = Convert.ToDateTime(item.StartDateTime),
                    End = AllDay ? Convert.ToDateTime(item.EndDateTime).AddDays(1) : Convert.ToDateTime(item.EndDateTime),
                    Color = "#F64E60",
                    EventType = ActivityTypeEnum.ProspectAppointment,
                    AllDay = AllDay
                });
            }

            var occasions = _context2.OccasionEvents.Where(m => m.UserLogin.Guid == UserLoginGuid && m.StartDateTime != null && m.EndDateTime != null).ToList();
            occasions = occasions.Where(m => m.StartDateTime.Value.BetweenDate(start, end) || m.EndDateTime.Value.BetweenDate(start, end)).ToList();
            foreach (var item in occasions)
            {
                bool AllDay = Convert.ToDateTime(item.EndDateTime).Subtract(Convert.ToDateTime(item.StartDateTime)).TotalDays >= 1;
                activities.Add(new CalenderModel
                {
                    Id = item.Guid,
                    Title = item.Subject,
                    Start = Convert.ToDateTime(item.StartDateTime),
                    End = AllDay ? Convert.ToDateTime(item.EndDateTime).AddDays(1) : Convert.ToDateTime(item.EndDateTime),
                    Color = "#FF9933",
                    EventType = ActivityTypeEnum.Occasion,
                    AllDay = AllDay
                });
            }
            activities = activities.OrderBy(m => m.Start).ToList();
            return Json(activities);
        }
        public async Task<IActionResult> GetEventDetail(Guid guid)
        {
            var activityEvent = await _context2.Events.Where(m => m.Guid == guid && m.IsDeleted != true)
                .Include(m => m.Leads).FirstOrDefaultAsync();
            return View("ActivityDetail/EventDetail", activityEvent);
        }
        public async Task<IActionResult> GetProspectAppointmentDetail(Guid guid, bool fromFilter = false)
        {
            var appointment = await _context2.ProspectAppointments.Where(m => m.Guid == guid && m.IsDeleted != true)
                .Include(m => m.Lead).FirstOrDefaultAsync();
            Lead leadDetail = new();
            if (appointment == null)
                leadDetail = await _context2.Leads.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            else
                leadDetail = appointment.Lead;

            List<BoardItemModel> history = new() { };
            var leadHistory = await _context2.LeadHistories.Where(m => m.LeadId == leadDetail.Id).ToListAsync();

            if (!fromFilter)
                history = leadHistory.Select(m =>
                   new BoardItemModel
                   {
                       Stage = m.Stage,
                       Date = m.CreatedDate.AddMinutes(-TimeZone),
                       FallOut = m.FallOut
                   }).ToList();
            ViewBag.History = history;
            ViewBag.Lead = leadDetail;
            ViewBag.FromFilter = fromFilter;
            return View("ActivityDetail/ProspectAppointmentDetail", appointment);
        }
        public async Task<IActionResult> GetClientAppointmentDetail(Guid guid)
        {
            var appointment = await _context2.ClientAppointments.Where(m => m.Guid == guid && m.IsDeleted != true)
                .Include(m => m.Leads).FirstOrDefaultAsync();
            return View("ActivityDetail/ClientAppointmentDetail", appointment);
        }
        public async Task<IActionResult> GetOccasionDetail(Guid guid)
        {
            var occasion = await _context2.OccasionEvents.Where(m => m.Guid == guid && m.IsDeleted != true).FirstOrDefaultAsync();
            return View("ActivityDetail/OccasionDetail", occasion);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteLead(Guid guid)
        {
            var lead = await _context2.Leads.Where(m => m.Guid == guid).AsNoTracking().FirstOrDefaultAsync();
            if (lead is not null)
            {
                _context2.Leads.Remove(lead);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = lead is not null,
                Message = lead is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteEvent(Guid guid, ActivityTypeEnum activityType)
        {
            var result = new CommonResponse();
            if (activityType == ActivityTypeEnum.Event)
            {
                var activityEvent = await _context2.Events.Where(m => m.Guid == guid).AsNoTracking().FirstOrDefaultAsync();
                if (activityEvent is not null)
                {
                    _context2.Events.Remove(activityEvent);
                    await _context2.SaveChangesAsync();
                }
                result = new CommonResponse
                {
                    Success = activityEvent is not null,
                    Message = activityEvent is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                };
            }
            else if (activityType == ActivityTypeEnum.ClientAppointment)
            {
                var activityEvent = await _context2.ClientAppointments.Where(m => m.Guid == guid).AsNoTracking().FirstOrDefaultAsync();
                if (activityEvent is not null)
                {
                    _context2.ClientAppointments.Remove(activityEvent);
                    await _context2.SaveChangesAsync();
                }
                result = new CommonResponse
                {
                    Success = activityEvent is not null,
                    Message = activityEvent is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                };
            }
            else if (activityType == ActivityTypeEnum.ProspectAppointment)
            {
                var activityEvent = await _context2.ProspectAppointments.Where(m => m.Guid == guid)
                    .Include(m => m.Lead).AsNoTracking().FirstOrDefaultAsync();
                if (activityEvent is not null)
                {
                    var clientToDelete = await _context2.ClientLogins.FirstOrDefaultAsync(m => m.Username == activityEvent.Lead.Email);
                    _context2.ProspectAppointments.Remove(activityEvent);
                    if (clientToDelete is not null)
                        _context2.ClientLogins.Remove(clientToDelete);
                    await _context2.SaveChangesAsync();
                }
                result = new CommonResponse
                {
                    Success = activityEvent is not null,
                    Message = activityEvent is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                };
            }
            else if (activityType == ActivityTypeEnum.Occasion)
            {
                var occasionEvent = await _context2.OccasionEvents.Where(m => m.Guid == guid).AsNoTracking().FirstOrDefaultAsync();
                if (occasionEvent is not null)
                {
                    _context2.OccasionEvents.Remove(occasionEvent);
                    await _context2.SaveChangesAsync();
                }
                result = new CommonResponse
                {
                    Success = occasionEvent is not null,
                    Message = occasionEvent is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
                };
            }
            return Json(result);
        }
        public async Task<IActionResult> AddOccasion(Guid guid)
        {
            var occasionEvent = await _context2.OccasionEvents.Where(m => m.Guid == guid && m.IsDeleted != true).FirstOrDefaultAsync();
            if (occasionEvent != null)
            {
                occasionEvent.StartDate = occasionEvent.StartDateTime.Value;
                occasionEvent.StartTime = occasionEvent.StartDateTime.Value;
                occasionEvent.EndDate = occasionEvent.EndDateTime.Value;
                occasionEvent.EndTime = occasionEvent.EndDateTime.Value;
            }
            return View("_AddOccasionModal", occasionEvent);
        }
        [HttpPost]
        public async Task<IActionResult> SaveOccasion(OccasionEvent occasion, bool confirmed)
        {
            var occasionEvent = await _context2.OccasionEvents.AsNoTracking().Where(m => m.Guid == occasion.Guid).FirstOrDefaultAsync();
            occasion.StartDateTime = occasion.StartDate.Add(occasion.StartTime.TimeOfDay);
            occasion.EndDateTime = occasion.EndDate.Add(occasion.EndTime.TimeOfDay);
            var occasionDataObj = _mapper.Map(occasion, occasionEvent);
            bool exist = false;
            if (occasionDataObj.OccasionType != OccasionTypeEnum.HolidayVacation && occasionDataObj.OccasionType != OccasionTypeEnum.PersonalTimeOutOfOffice && (occasion.StartDateTime != occasionEvent?.StartDateTime || occasion.EndDateTime != occasionEvent?.EndDateTime))
                exist = await CheckActivityExistOrHolidayAsync(Convert.ToDateTime(occasionDataObj.StartDateTime), occasionDataObj.EndDateTime, occasionDataObj.Guid);
            occasionDataObj.UserId = UserId;
            if (!exist || confirmed)
            {
                _context2.OccasionEvents.Update(occasionDataObj);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = !exist || confirmed,
                Message = exist && !confirmed ? Message.NOTAVAILABLE : occasionEvent is not null ? Message.SAVE_SUCCESS : Message.RECORD_ADD
            };
            return Json(result);
        }
        public IActionResult Marketing()
        {
            return View();
        }
        public IActionResult GetBoardData()
        {
            var itemTemplate = @"<div class='d-flex align-items-center'>
                                    <input type='hidden' value='{0}' class='guid'/>
                        	        <div class='symbol symbol-{1} mr-3 initial'>
                        	            <span class='symbol-label'>{2}</span>
                        	        </div>
                        	        <div class='d-flex flex-column align-items-start'>
                        	            <span class='text-dark-50 font-weight-bold mb-1'>{3}</span>
                        	            <button class='btn label label-inline label-outline-{4} font-weight-bold status' data-guid='{0}' data-has-event-appointment='{6}'>{5}</button>
                        	        </div>
                        	    </div>";
            var boardItems = _context2.ProspectAppointments.Where(m => m.Lead.UserLogin.Guid == UserLoginGuid);
            var boards = new List<KanbanModel>
            {
                new KanbanModel
                {
                    Id="6",
                    Title="Leads",
                    Class = "light-primary",
                    Item  = _context2.Leads.Where(m=>(m.ActivityEvent.UserLogin.Guid == UserLoginGuid || m.ClientAppointment.UserLogin.Guid == UserLoginGuid) && m.Active).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-primary",m.Name.GetInitials(),m.Name,"primary","Send Invite","1").Replace("outline-","")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id="7",
                    Title="Pre-Appointment",
                    Class = "light-primary",
                    Item  = boardItems.Where(m=>m.Stage==ProspectAppointmentStageEnum.PreAppointment).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary","Invite Sent","1")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "1",
                    Title = "1<sup>st</sup> Appointment",
                    Class = "light-primary",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.FirstAppointment && ! m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "2",
                    Title = "2<sup>nd</sup> Appointment",
                    Class = "light-primary",
                    Item  = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.SecondAppointment && ! m.FallOut).Select(m=>
                    new Item{
                        Id = m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "3",
                    Title = "3<sup>rd</sup> Appointment",
                    Class = "light-primary",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.ThirdAppointment && ! m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "4",
                    Title = "New Client",
                    Class = "light-primary",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.NewClient).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "5",
                    Title = "Referral",
                    Class = "light-warning",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.Referral).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-warning",m.Lead.Name.GetInitials(),m.Lead.Name,"warning",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "0",
                    Class = "light-warning",
                },
                new KanbanModel
                {
                    Id = "0",
                    Class = "light-warning",
                },
                new KanbanModel
                {
                    Id = "8",
                    Title = "Fallout",
                    Class = "light-danger",
                    Item = boardItems.Where(m => m.Stage == ProspectAppointmentStageEnum.FirstAppointment && m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-danger",m.Lead.Name.GetInitials(),m.Lead.Name,"danger",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "9",
                    Title = "Fallout",
                    Class = "light-danger",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.SecondAppointment && m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-danger",m.Lead.Name.GetInitials(),m.Lead.Name,"danger",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "10",
                    Title = "Fallout",
                    Class = "light-danger",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.ThirdAppointment && m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Guid,"light-danger",m.Lead.Name.GetInitials(),m.Lead.Name,"danger",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0")
                    }).ToList()
                }
            };
            return Json(boards);
        }
        public IActionResult GetFilterBoardData(DateTime startDate, DateTime endDate)
        {
            var itemTemplate = @"<div class='d-flex align-items-center k-items' data-lead-guid='{8}'>
                                    <input type='hidden' value='{0}' class='guid'/>
                        	        <div class='symbol symbol-{1} mr-3 initial'>
                        	            <span class='symbol-label'>{2}</span>
                        	        </div>
                        	        <div class='d-flex flex-column align-items-start'>
                        	            <span class='text-dark-50 font-weight-bold mb-1'>{3}</span>
                        	            <button class='btn label label-inline label-outline-{4} font-weight-bold status' data-guid='{0}' data-has-event-appointment='{6}'>{5}</button>                                                               	        
                                    </div>
                        	    </div>
                                <div class='text-maroon mt-2 font-italic' style='font-size:10px;'>{7}</div>";
            var boardItems = _context2.LeadHistories.Where(m => m.Lead.UserLogin.Guid == UserLoginGuid && m.Default && m.CreatedDate.AddMinutes(-TimeZone).Date >= startDate && m.CreatedDate.AddMinutes(-TimeZone).Date <= endDate)
                .Include(m => m.Lead).Include(m => m.Lead.ProspectAppointment);

            var boards = new List<KanbanModel>
            {
                new KanbanModel
                {
                    Id="6",
                    Title="Leads",
                    Class = "light-primary",
                    Item  = boardItems.Where(m=>m.Stage==ProspectAppointmentStageEnum.Lead).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary","Send Invite","1",m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid).Replace("outline-","")
                    }).ToList()
                },
                new KanbanModel
                {
                    Id="7",
                    Title="Pre-Appointment",
                    Class = "light-primary",
                    Item  = boardItems.Where(m=>m.Stage==ProspectAppointmentStageEnum.PreAppointment).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary","Invite Sent","1",m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "1",
                    Title = "1<sup>st</sup> Appointment",
                    Class = "light-primary",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.FirstAppointment && ! m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0",m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "2",
                    Title = "2<sup>nd</sup> Appointment",
                    Class = "light-primary",
                    Item  = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.SecondAppointment && ! m.FallOut).Select(m=>
                    new Item{
                        Id = m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0", m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "3",
                    Title = "3<sup>rd</sup> Appointment",
                    Class = "light-primary",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.ThirdAppointment && ! m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0", m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "4",
                    Title = "New Client",
                    Class = "light-primary",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.NewClient).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-primary",m.Lead.Name.GetInitials(),m.Lead.Name,"primary",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0", m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "5",
                    Title = "Referral",
                    Class = "light-warning",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.Referral).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-warning",m.Lead.Name.GetInitials(),m.Lead.Name,"warning",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0",m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "0",
                    Class = "light-warning",
                },
                new KanbanModel
                {
                    Id = "0",
                    Class = "light-warning",
                },
                new KanbanModel
                {
                    Id = "8",
                    Title = "Fallout",
                    Class = "light-danger",
                    Item = boardItems.Where(m => m.Stage == ProspectAppointmentStageEnum.FirstAppointment && m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-danger",m.Lead.Name.GetInitials(),m.Lead.Name,"danger",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0", m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "9",
                    Title = "Fallout",
                    Class = "light-danger",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.SecondAppointment && m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-danger",m.Lead.Name.GetInitials(),m.Lead.Name,"danger",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0", m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                },
                new KanbanModel
                {
                    Id = "10",
                    Title = "Fallout",
                    Class = "light-danger",
                    Item = boardItems.Where(m=>m.Stage == ProspectAppointmentStageEnum.ThirdAppointment && m.FallOut).Select(m=>
                    new Item{
                        Id=m.Guid,
                        Title =string.Format(itemTemplate,m.Lead.ProspectAppointment.Guid,"light-danger",m.Lead.Name.GetInitials(),m.Lead.Name,"danger",m.Status.GetDisplayName(),(m.Lead.EventId > 0 || m.Lead.ClientAppointmentId > 0)? "1" :"0", m.CreatedDate.AddMinutes(-TimeZone).ToString("MMM dd, yyyy hh:mm tt"),m.Lead.Guid)
                    }).ToList()
                }
            };
            return Json(boards);
        }
        public async Task<IActionResult> UpdateKanbanItemStatus(Guid guid, ProspectAppointmentStageEnum status, int falloutStatus)
        {
            var appointment = await _context2.ProspectAppointments.Where(m => m.Guid == guid)
                .Include(m => m.Lead).Include(m => m.Lead.LeadHistories).FirstOrDefaultAsync();
            string TargetClass = status != default ? "primary" : "danger";
            string SourceClass = status != default ? "danger" : appointment.Stage.GetDescription();
            appointment.FallOut = status == default;
            appointment.Stage = status != default ? status : EnumHelper.ToEnum<ProspectAppointmentStageEnum>(falloutStatus.ToString());
            _context2.ProspectAppointments.Update(appointment);
            if (appointment.FallOut)
            {
                var lastLeadHistory = appointment.Lead.LeadHistories.LastOrDefault();
                lastLeadHistory.Default = false;
            }
            _context2.LeadHistories.Add(new LeadHistory
            {
                LeadId = appointment.LeadId,
                FallOut = appointment.FallOut,
                Stage = appointment.Stage,
                Status = appointment.Status,
                Default = true
            });
            await _context2.SaveChangesAsync();
            var StatusName = status == ProspectAppointmentStageEnum.PreAppointment ? "Invite Sent" : appointment.Status.GetDisplayName();
            var result = new CommonResponse
            {
                Success = true,
                Message = Message.SAVE_SUCCESS,
                Data = new { SourceClass, TargetClass, StatusName, appointment.Guid }
            };
            return Json(result);
        }
        public ActionResult CalendarSetting()
        {
            return View("_CalendarSettingModal");
        }
        [HttpPost]
        public async Task<IActionResult> SaveCalendarSetting(List<WorkingHourModel> workingHour)
        {
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            user.WorkingHourConfig = workingHour;
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = workingHour is not null,
                Message = workingHour is not null ? Message.SAVE_SUCCESS : Message.RECORD_ADD,
            };
            return Json(result);
        }
        public async Task<IActionResult> GetBusinessHours()
        {
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            var defaultWorkingHours = (await MenuDataHelper.GetDefaultWorkingHourDataAsync()).Where(m => m.Active);
            var workingHours = user.WorkingHourConfig != null ? user.WorkingHourConfig.Where(m => m.Active) : defaultWorkingHours;
            var result = new CommonResponse
            {
                Data = workingHours
            };
            return Json(result);
        }
        public async Task<bool> CheckActivityExistOrHolidayAsync(DateTime start, DateTime? end, Guid guid, bool checkWorkingHour = true)
        {
            end = end != null ? Convert.ToDateTime(end) : start;
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            var defaultWorkingHours = (await MenuDataHelper.GetDefaultWorkingHourDataAsync()).Where(m => m.Active);
            var workingHoursConfig = user.WorkingHourConfig != null ? user.WorkingHourConfig.Where(m => m.Active) : defaultWorkingHours;
            bool nonWorkingDayHourActivity = false;
            bool existActivity = false;
            var prospectAppointments = _context2.ProspectAppointments.Where(m => m.Lead.UserLogin.Guid == UserLoginGuid && m.Guid != guid && m.StartDateTime != null && m.EndDateTime != null).AsNoTracking().ToList();
            var occasions = _context2.OccasionEvents.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid != guid && m.StartDateTime != null && m.EndDateTime != null).AsNoTracking().ToList();
            for (DateTime counter = Convert.ToDateTime(start); counter <= end; counter = counter.AddDays(1))
            {
                var curruentWorkingDayConfig = workingHoursConfig.FirstOrDefault(m => m.Day == counter.ToString("dddd"));
                if (curruentWorkingDayConfig == null && !nonWorkingDayHourActivity)
                    nonWorkingDayHourActivity = true;
                if (!nonWorkingDayHourActivity)
                {
                    if (curruentWorkingDayConfig != null && checkWorkingHour)
                    {
                        var startTime = Convert.ToDateTime(curruentWorkingDayConfig.StartTime).TimeOfDay;
                        var endTime = Convert.ToDateTime(curruentWorkingDayConfig.EndTime).TimeOfDay;
                        if (!nonWorkingDayHourActivity && !(counter.IsBetweenTime(startTime, endTime) && end.Value.IsBetweenTime(startTime, endTime)))
                            nonWorkingDayHourActivity = true;
                    }
                    existActivity = prospectAppointments.Any(m => counter.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value) || end.Value.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value));
                    if (!existActivity)
                        existActivity = occasions.Any(m => counter.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value) || end.Value.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value));
                }
                if (nonWorkingDayHourActivity || existActivity) break;
            }
            return nonWorkingDayHourActivity || existActivity;
        }
        public IActionResult OverlappedActivity(DateTime start, DateTime? end, Guid guid)
        {
            end = end != null ? Convert.ToDateTime(end) : start;
            List<string> RepeatedActivityList = new();
            var prospectAppointments = _context2.ProspectAppointments.Where(m => m.Lead.UserLogin.Guid == UserLoginGuid && m.Guid != guid && m.StartDateTime != null && m.EndDateTime != null).AsNoTracking().ToList();
            var occasions = _context2.OccasionEvents.Where(m => m.UserLogin.Guid == UserLoginGuid && m.Guid != guid && m.StartDateTime != null && m.EndDateTime != null).AsNoTracking().ToList();
            for (DateTime counter = Convert.ToDateTime(start); counter <= end; counter = counter.AddDays(1))
            {
                prospectAppointments = prospectAppointments.Where(m => counter.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value) || end.Value.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value)).ToList();
                RepeatedActivityList.AddRange(prospectAppointments.Select(m => m.Subject));

                occasions = occasions.Where(m => counter.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value) || end.Value.BetweenDate(m.StartDateTime.Value, m.EndDateTime.Value)).ToList();
                RepeatedActivityList.AddRange(occasions.Select(m => m.Subject));
            }
            ViewBag.RepeatedActivityList = RepeatedActivityList;
            return View();
        }
        public async Task<IActionResult> FetchTaxPrepQueueAcitvities(int page)
        {
            var teaxPrepHistory = _context2.TaxPrepHistories.Where(m => m.ClientLogin.UserLogin.FirmId == FirmId);
            var AdminRole = (await _context2.UserLogins.FirstOrDefaultAsync(m => m.Id == UserId)).AdminRole;
            var skip = page > 0 ? (page - 1) * 10 : 0;
            var meta = new DatatableMetaModel
            {
                pagination = new Pagination
                {
                    perpage = 15,
                    page = page
                }
            };
            string sortString = DataTableHelper.GetSortString("CreatedDate", "desc");
            string includeProperties = "ClientLogin,ClientLogin.ClientData,ActionByUser,ActionByUser.UserData";
            int UId = OwnerUserId > 0 ? Convert.ToInt32(OwnerUserId) : UserId;
            string searchQuery = AdminRole == AdminRoleEnum.Adviser ? $"ClientLogin.UserId == {UId}" : string.Empty;
            var result = await teaxPrepHistory.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            return Json(result);
        }
        public async Task<IActionResult> FetchDashboardConfig()
        {
            var user = await _context2.UserDatas.FirstOrDefaultAsync(m => m.UserId == UserId);
            return Json(user.DashboardConfig);
        }
        public async Task<IActionResult> UpdateDashboardConfig(List<DashboardConfigModel> config)
        {
            var user = await _context2.UserDatas.FirstOrDefaultAsync(m => m.UserId == UserId);
            user.DashboardConfig = config;
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = true
            };
            return Json(result);
        }
    }
}