﻿using AutoMapper;
using LAP.Areas.Admin.Model;
using LAP.Areas.Adviser.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.Extension;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NuGet.ContentModel;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace LAP.Areas.Adviser.Controllers
{
    [Area(Roles.Adviser)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class ReportController : BaseController, IDisposable
    {
        protected readonly FileHelper _fileHelper;
        protected readonly CommonService _commonService;
        protected readonly BudgetService _budgetService;
        protected readonly EmploymentService _employmentService;
        protected readonly InvestmentService _investmentService;
        protected readonly SocialSecurityService _socialSecurityService;
        protected readonly OtherIncomeService _otherIncomeService;
        protected readonly ReportService _reportService;
        protected readonly PropertyService _propertyService;
        public ReportController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _commonService = new CommonService(_context2);
            _fileHelper = new FileHelper(_config);
            _budgetService = new BudgetService(_context2, _httpContextAccessor);
            _employmentService = new EmploymentService(_context2, _httpContextAccessor);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
            _socialSecurityService = new SocialSecurityService(_context2);
            _reportService = new ReportService(_context2, _httpContextAccessor);
            _otherIncomeService = new OtherIncomeService(_context2);
            _propertyService = new PropertyService(_context2);
        }
        public async Task<ADV2BModel> ADV_2BData(string token, int tz)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid UserLoginGuid = new(token);
            bool hasDisciplinary = (await _commonService.GetRegulatoryQuestionsAsync(default, UserLoginGuid)).Any(m => m.Answer);
            var adviser = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.UserData).Include(m => m.AdviserContactInfos).Include(m => m.PostHighSchoolEducations).Include(m => m.Works)
                .Include(m => m.UserData.RIA).FirstOrDefaultAsync();
            var degree = Enum.GetValues(typeof(DegreeDesinationEnum)).Cast<DegreeDesinationEnum>();
            List<string> designations = new();
            if (!string.IsNullOrEmpty(adviser.UserData?.DegreeDesignation))
            {
                foreach (var item in adviser.UserData.DegreeDesignation?.Split(","))
                {
                    DegreeDesinationEnum designation = degree.FirstOrDefault(m => Convert.ToInt32(m) == Convert.ToInt32(item));
                    designations.Add(designation.ToString());
                }
                adviser.UserData.DegreeDesignationWithData = string.Join(", ", designations);
            }
            var fileName = $"{adviser.UserData.FirstName}'s ADV 2B {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
            var model = new ADV2BModel
            {
                UserLogin = adviser,
                HasDisciplinary = hasDisciplinary,
                LocalDateTime = DateTime.UtcNow.AddMinutes(-tz),
                FileName = fileName
            };
            return model;
        }
        [AllowAnonymous]
        public async Task<IActionResult> ADV_2B(string token, int tz, bool html)
        {
            var model = await ADV_2BData(token, tz);
            if (html)
                return View(model);
            return new ViewAsPdf(model)
            {
                PageMargins = { Left = 10, Bottom = 12, Right = 10, Top = 10 },
                CustomSwitches = "--footer-center \"Page [page]\" --footer-line --footer-font-size \"12\" --footer-font-name \"Segoe UI\"",
                ContentDisposition = ContentDisposition.Inline,
                FileName = model.FileName
            };
        }

        [AllowAnonymous]
        public async Task<IActionResult> RiskArc(string token, ClientTypeEnum clientType, int tz)
        {
            var model = await _reportService.RiskArcReportData(token, clientType, tz);
            if (model != null)
            {
                var viewName = model.RiskArcVersion ? "RiskArcInDepth" : "RiskArcExpress";
                var pdfViewAction = new ViewAsPdf(viewName, model)
                {
                    PageMargins = { Left = 10, Bottom = 5, Right = 10, Top = 8 },
                    ContentDisposition = ContentDisposition.Inline,
                    FileName = model.FileName
                };
                return pdfViewAction;
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }

        [AllowAnonymous]
        public async Task<ArcOfLifeReportModel> ArcOfLifeData(string token, int tz)
        {
            int year28 = 28;
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid guid = new(token);
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == guid && m.IsDeleted != true)
                .Include(m => m.UserLogin).Include(m => m.ClientData).Include(m => m.SpouseData).FirstOrDefaultAsync();
            var userLogin = await _context2.UserLogins.Where(m => m.Guid == clientLogin.UserLogin.Guid)
                .Include(m => m.UserData).Include(m => m.AdviserContactInfos).Include(m => m.AdviserContactInfos).FirstOrDefaultAsync();
            int totalHeirs = await _context2.Heirs.CountAsync(m => m.ClientLogin.Guid == guid);
            var adviserData = userLogin.UserData;
            var clientData = clientLogin.ClientData;
            var spouseData = clientLogin.SpouseData;
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();

            var planningITems = (await _commonService.GetPlanningItemsAsync(guid)).Where(m => m.Active);
            bool hasInvestmentPlanning = await planningITems.AnyAsync(m => m.PlanningItem.ItemName.Contains("Investment"));
            bool hasEstatePlanning = await planningITems.AnyAsync(m => m.PlanningItem.ItemName.Contains("Estate"));
            bool hasHealthCarePlanning = await planningITems.AnyAsync(m => m.PlanningItem.ItemName.Contains("Healthcare"));
            string businessLogoImage = string.Empty;
            if (!string.IsNullOrEmpty(adviserData.BusinessLogoFileName))
            {
                string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
                string extension = Path.GetExtension(adviserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                businessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.BusinessLogoFileName, contentType);
            }

            var properties = _context2.ClientProperties.Where(m => m.ClientLogin.Guid == guid);

            var primaryResidence = properties.Where(m => m.Type == PropertyTypeEnum.Primary).Sum(m => m.CurrentValue);
            var assetsPv = properties.Where(m => m.Type != PropertyTypeEnum.Primary)?.Sum(m => m.CurrentValue) ?? 0;

            var primaryResidenceFv = properties.Where(m => m.Type == PropertyTypeEnum.Primary)?.Sum(m => m.ProjectedFairMarketValueRetirement) ?? 0;
            var assetsFv = properties.Where(m => m.Type != PropertyTypeEnum.Primary)?.Sum(m => m.ProjectedFairMarketValueRetirement) ?? 0;

            var includeRealProperty = new PropertyTypeEnum[] { PropertyTypeEnum.Primary, PropertyTypeEnum.Secondary, PropertyTypeEnum.Vacation, PropertyTypeEnum.RealEstate, PropertyTypeEnum.Land };

            var realProperties = properties.Where(m => includeRealProperty.Contains(m.Type));
            var personalProperties = properties.Where(m => !includeRealProperty.Contains(m.Type));

            var realPropertyPv = realProperties.Sum(m => m.CurrentValue);
            var personalPropertyPv = personalProperties.Sum(m => m.CurrentValue);

            var realPropertyProjected = await realProperties?.SumAsync(m => m.ProjectedFairMarketValueRetirement) ?? 0;
            var personalPropertyProjected = await personalProperties?.SumAsync(m => m.ProjectedFairMarketValueRetirement) ?? 0;

            var clientPersonalProperties = properties.Where(m => m.Ownership != ((int)ClientTypeEnum.Spouse).ToString());
            var spousePersonalProperties = properties.Where(m => m.Ownership == ((int)ClientTypeEnum.Spouse).ToString());

            int clientOtherIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(guid, ClientTypeEnum.Client))?.Sum(m => m.AnnualIncome) ?? 0;
            PropertyTypeEnum[] excludePropertyIncome = new PropertyTypeEnum[] { PropertyTypeEnum.Metals, PropertyTypeEnum.Automobile, PropertyTypeEnum.Jewelry, PropertyTypeEnum.Collectibles };
            int clientExtraOtherIncome = (await _propertyService.GetPropertyDataAsync(guid, ClientTypeEnum.Client, excludePropertyIncome))?.Sum(m => m.AnnualIncome) ?? 0;

            AgeModel ageData = await _commonService.GetAgeData(guid);
            FVTotalModel clientRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(guid, ClientTypeEnum.Client);
            FVTotalModel clientPrevRetirementPlanFvDetails = await _employmentService.GetPreviousRetirementPlanFutureValueDetailsAsync(guid, ClientTypeEnum.Client);
            FVTotalModel clientBankCreditUnionFvDetails = new();
            FVTotalModel clientLifeFvDetails = new();
            FVTotalModel clientAnnuityContractFvDetails = new();
            FVTotalModel clientBrokerageFvDetails = new();
            FVTotalModel clientEducationFvDetails = new();
            FVTotalModel spouseRetirementPlanFvDetails = new();
            FVTotalModel spousePrevRetirementPlanFvDetails = new();
            FVTotalModel spouseBankCreditUnionFvDetails = new();
            FVTotalModel spouseLifeFvDetails = new();
            FVTotalModel spouseAnnuityContractFvDetails = new();
            FVTotalModel spouseBrokerageFvDetails = new();
            FVTotalModel spouseEducationFvDetails = new();

            var clientOccupations = await _employmentService.GetEmploymentDataAsync(guid, ClientTypeEnum.Client);
            int clientOccupationIncome = clientOccupations.Where(m => m.AgeToRetire > ageData.Client.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
            int clientOccupationIncomeAtRetirement = clientOccupations.Where(m => m.AgeToRetire > (ageData.Client.RetirementAge))?.Sum(m => m.AnnualEarnings) ?? 0;
            int clientOccupationIncomeAt28YearRetirement = clientOccupations.Where(m => m.AgeToRetire > ageData.Client.Age28YearAtRetirement)?.Sum(m => m.AnnualEarnings) ?? 0;
            var clientPensionData = await _employmentService.GetRetirementPlanDataAsync(guid, ClientTypeEnum.Client);
            int clientPensionIncomeAmount = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
            int clientPensionIncomeAtRetirement = clientPensionData.Where(m => m.PensionExpectedAge <= ageData.Client.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;

            var clientEmployments = clientOccupations.Where(m => m.EmploymentStatus == EmploymentStatusEnum.FullTimeEmployee || m.EmploymentStatus == EmploymentStatusEnum.PartTimeEmployee);
            var clientSelfEmployments = clientOccupations.Where(m => m.EmploymentStatus == EmploymentStatusEnum.SelfEmployed);
            var clientPersonalActivities = await _otherIncomeService.GetOtherIncomeDataAsync(guid, ClientTypeEnum.Client);

            IEnumerable<ClientOccupation> spouseOccupations = new List<ClientOccupation>(), spouseEmployments = new List<ClientOccupation>(), spouseSelfEmployments = new List<ClientOccupation>();
            IEnumerable<OtherIncome> spousePersonalActivities = new List<OtherIncome>();
            int spouseOccupationIncome = 0, spouseOccupationIncomeAtRetirement = 0, spouseOccupationIncomeAt28YearRetirement = 0, spouseOtherIncome = 0, spouseExtraOtherIncome = 0,
                spousePensionIncomeAmount = 0, spousePensionIncomeAtRetirement = 0;
            IEnumerable<ClientEsp> spousePensionData = new List<ClientEsp>();
            if (isMarried)
            {
                spouseOccupations = await _employmentService.GetEmploymentDataAsync(guid, ClientTypeEnum.Spouse);
                spouseOccupationIncome = spouseOccupations.Where(m => m.AgeToRetire > ageData.Spouse.Age)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseOccupationIncomeAtRetirement = spouseOccupations.Where(m => m.AgeToRetire > (ageData.Spouse.RetirementAge))?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseOccupationIncomeAt28YearRetirement = spouseOccupations.Where(m => m.AgeToRetire > ageData.Spouse.Age28YearAtRetirement)?.Sum(m => m.AnnualEarnings) ?? 0;

                spousePensionData = await _employmentService.GetRetirementPlanDataAsync(guid, ClientTypeEnum.Spouse);
                spousePensionIncomeAmount = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.Age)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
                spousePensionIncomeAtRetirement = spousePensionData.Where(m => m.PensionExpectedAge <= ageData.Spouse.RetirementAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
                spouseOtherIncome = (await _otherIncomeService.GetOtherIncomeDataAsync(guid, ClientTypeEnum.Spouse))?.Sum(m => m.AnnualIncome) ?? 0;
                spouseExtraOtherIncome = (await _propertyService.GetPropertyDataAsync(guid, ClientTypeEnum.Spouse, excludePropertyIncome))?.Sum(m => m.AnnualIncome) ?? 0;

                spouseRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse);
                spousePrevRetirementPlanFvDetails = await _employmentService.GetRetirementPlanFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse);

                spouseEmployments = spouseOccupations.Where(m => m.EmploymentStatus == EmploymentStatusEnum.FullTimeEmployee || m.EmploymentStatus == EmploymentStatusEnum.PartTimeEmployee);
                spouseSelfEmployments = spouseOccupations.Where(m => m.EmploymentStatus == EmploymentStatusEnum.SelfEmployed);
                spousePersonalActivities = await _otherIncomeService.GetOtherIncomeDataAsync(guid, ClientTypeEnum.Spouse);
            }

            IEnumerable<LifeAnnuityContract> clientAnnuities = new List<LifeAnnuityContract>(), spouseAnnuities = new List<LifeAnnuityContract>();
            IEnumerable<BrokerageAdvisoryAccount> clientBrokerageAdvisoryAccounts = new List<BrokerageAdvisoryAccount>(), spouseBrokerageAdvisoryAccounts = new List<BrokerageAdvisoryAccount>();
            IEnumerable<BankCreditUnionAccount> clientBankCreditUnionAccounts = new List<BankCreditUnionAccount>(), spouseBankCreditUnionAccounts = new List<BankCreditUnionAccount>();
            int clientAnnuityIncome = 0, spouseAnnuityIncome = 0, clientBrokerageIncome = 0, spouseBrokerageIncome = 0, clientBankCreditUnionIncome = 0, spouseBankCreditUnionIncome = 0;

            if (hasInvestmentPlanning)
            {
                var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                if (clientData.InvestmentVersion)
                {
                    clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(guid, ClientTypeEnum.Client, bankingAccountTypes);
                    clientLifeFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(guid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Life });
                    clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(guid, ClientTypeEnum.Client, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                    clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(guid, ClientTypeEnum.Client, new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse });
                    clientEducationFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(guid, ClientTypeEnum.Client, new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Education });

                    clientAnnuities = await _investmentService.GetLifeAnnuityContractDataAsync(guid, ClientTypeEnum.Client);
                    clientBrokerageAdvisoryAccounts = await _investmentService.GetBrokerageAdvisoryDataAsync(guid, ClientTypeEnum.Client);
                    clientBankCreditUnionAccounts = await _investmentService.GetBankCreditUnionDataAsync(guid, ClientTypeEnum.Client);

                    clientAnnuityIncome = clientAnnuities?.Sum(m => m.AnnualIncome) ?? 0;
                    clientBrokerageIncome = clientBrokerageAdvisoryAccounts?.Sum(m => m.AnnualIncome) ?? 0;
                    clientBankCreditUnionIncome = clientBankCreditUnionAccounts?.Sum(m => m.AnnualIncome) ?? 0;
                    if (isMarried)
                    {
                        spouseBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse, bankingAccountTypes);
                        spouseLifeFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Life });
                        spouseAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                        spouseBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse, new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse });
                        spouseEducationFvDetails = await _investmentService.GetBrokerageAdvisoryFutureValueDetailsAsync(guid, ClientTypeEnum.Spouse, new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Education });

                        spouseAnnuities = await _investmentService.GetLifeAnnuityContractDataAsync(guid, ClientTypeEnum.Spouse);
                        spouseBrokerageAdvisoryAccounts = await _investmentService.GetBrokerageAdvisoryDataAsync(guid, ClientTypeEnum.Spouse);
                        spouseBankCreditUnionAccounts = await _investmentService.GetBankCreditUnionDataAsync(guid, ClientTypeEnum.Spouse);

                        spouseBankCreditUnionIncome = spouseBankCreditUnionAccounts?.Sum(m => m.AnnualIncome) ?? 0;
                        spouseAnnuityIncome = spouseAnnuities?.Sum(m => m.AnnualIncome) ?? 0;
                        spouseBrokerageIncome = spouseBrokerageAdvisoryAccounts?.Sum(m => m.AnnualIncome) ?? 0;
                    }
                }
                else
                {
                    clientBankCreditUnionFvDetails = await _investmentService.GetBankCreditUnionExpressFutureValueDetailsAsync(guid);
                    clientAnnuityContractFvDetails = await _investmentService.GetLifeAnnuityContractExpressFutureValueDetailsAsync(guid);
                    clientBrokerageFvDetails = await _investmentService.GetBrokerageAdvisoryExpressFutureValueDetailsAsync(guid);
                }
            }

            int LongTermCareRate = 0;
            if (hasHealthCarePlanning)
            {
                var healthCarePlanning = _context2.HealthCarePlannings.FirstOrDefault(m => m.ClientLogin.Guid == guid);
                if (healthCarePlanning != null)
                {
                    LongTermCareRate = healthCarePlanning.LongTermCareRate;
                    hasHealthCarePlanning = hasHealthCarePlanning && healthCarePlanning.longTermCare;
                }
            }
            int clientSocialSecurityIncome = 0, spouseSocialSecurityIncome = 0, clientSocialSecurityIncomeAtRetirement = 0, spouseSocialSecurityIncomeAtRetirement = 0;
            int clientSocialSecurityAge = 0, spouseSocialSecurityAge = 0;
            SocialSecurity clientSocialSecurity = new(), spouseSocialSecurity = new();
            clientSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(guid, ClientTypeEnum.Client);

            if (clientSocialSecurity != null)
            {
                clientSocialSecurityAge = clientSocialSecurity.sSageBenifitsEnum;
                if (clientSocialSecurity.ssreceiving)
                {
                    clientSocialSecurityIncome = clientSocialSecurity.ssmonthly * 12;
                    clientSocialSecurityIncomeAtRetirement = clientSocialSecurity.ssmonthly * 12;
                }
                else
                {
                    clientSocialSecurityIncome = ageData.Client.Age >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                    clientSocialSecurityIncomeAtRetirement = ageData.Client.RetirementAge >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                }
            }

            if (isMarried)
            {
                spouseSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(guid, ClientTypeEnum.Spouse);
                if (spouseSocialSecurity != null)
                {
                    spouseSocialSecurityAge = spouseSocialSecurity.sSageBenifitsEnum;
                    if (spouseSocialSecurity.ssreceiving)
                    {
                        spouseSocialSecurityIncome = spouseSocialSecurity.ssmonthly * 12;
                        spouseSocialSecurityIncomeAtRetirement = spouseSocialSecurity.ssmonthly * 12;
                    }
                    else
                    {
                        spouseSocialSecurityIncome = ageData.Spouse.Age >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                        spouseSocialSecurityIncomeAtRetirement = ageData.Spouse.RetirementAge >= spouseSocialSecurity.sSageBenifitsEnum ? spouseSocialSecurity.ssexpexting * 12 : 0;
                    }
                }
            }
            var totalPmtValue = clientRetirementPlanFvDetails.PmtValue + spouseRetirementPlanFvDetails.PmtValue +
                                clientBankCreditUnionFvDetails.PmtValue + spouseBankCreditUnionFvDetails.PmtValue +
                                clientBrokerageFvDetails.PmtValue + spouseBrokerageFvDetails.PmtValue +
                                clientAnnuityContractFvDetails.PmtValue + spouseAnnuityContractFvDetails.PmtValue;

            var budgetItems = await _budgetService.GetBudgetItemsAsync(guid);
            FVTotalModel inflationaryFvDetails = clientData.BudgetVersion ? await _budgetService.GetInflationaryBudgetFutureValue(guid) : await _budgetService.GetInfaltionaryBudgetFutureValueExpressAsync(guid);
            var inflationary = clientData.BudgetVersion ? inflationaryFvDetails.PresentValue : clientData.InflationaryBudget;
            var nonInflationary = clientData.BudgetVersion ? budgetItems.Where(m => !m.Inflationary && (m.YearsPayOff > ageData.YearToRetirement || !m.BudgetItem.IsRevolvingDebt))?.Sum(m => m.Amount) ?? 0 : clientData.NonInflationaryBudget;

            var totalPropertyDebt = await properties.Where(m => m.Type != PropertyTypeEnum.Primary)?.SumAsync(m => m.AmountOwed) ?? 0;
            var totalBudgetDebt = budgetItems?.Sum(m => m.BalanceOwn) ?? 0;

            var totalPropertyDebtAtRetirement = await properties.Where(m => m.Type != PropertyTypeEnum.Primary && m.YearsPayOff > ageData.YearToRetirement)?.SumAsync(m => m.AmountOwed) ?? 0;
            var totalBudgetDebtAtRetirement = budgetItems.Where(m => m.YearsPayOff > ageData.YearToRetirement)?.Sum(m => m.BalanceOwn) ?? 0;

            var totalPropertyDebtAt28YearRetirement = await properties.Where(m => m.Type != PropertyTypeEnum.Primary && m.YearsPayOff >= 28)?.SumAsync(m => m.AmountOwed) ?? 0;
            var totalBudgetDebtAtAt28YearRetirement = budgetItems.Where(m => m.YearsPayOff >= 28)?.Sum(m => m.BalanceOwn) ?? 0;

            clientOtherIncome += clientExtraOtherIncome;
            spouseOtherIncome += spouseExtraOtherIncome;
            int clientInvestmentIncome = clientBankCreditUnionFvDetails.Income + clientBrokerageFvDetails.Income + clientAnnuityContractFvDetails.Income;
            int spouseInvestmentIncome = spouseBankCreditUnionFvDetails.Income + spouseBrokerageFvDetails.Income + spouseAnnuityContractFvDetails.Income;

            var totalIncomeAtRetirement = clientOccupationIncomeAtRetirement + spouseOccupationIncomeAtRetirement +
                                          clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                          clientOtherIncome + spouseOtherIncome +
                                          clientInvestmentIncome + spouseInvestmentIncome +
                                          clientSocialSecurityIncomeAtRetirement + spouseSocialSecurityIncomeAtRetirement +
                                          clientPensionIncomeAtRetirement + spousePensionIncomeAtRetirement;

            var incomeNeededAtRetirement = inflationaryFvDetails.FutureValue + nonInflationary;
            var investmentNeededAtRetirement = _reportService.GetInvestmentNeededAtRetirement(incomeNeededAtRetirement, totalIncomeAtRetirement);

            var Estate = _context2.ClientEstateItems.Where(m => m.ClientLogin.Guid == guid);

            int totalPv = clientRetirementPlanFvDetails.PresentValue + spouseRetirementPlanFvDetails.PresentValue +
                                  clientPrevRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue +
                                  clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue +
                                  clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue +
                                  clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue;

            int totalValueOfReturn = clientRetirementPlanFvDetails.ValueOfReturn + spouseRetirementPlanFvDetails.ValueOfReturn
                + clientPrevRetirementPlanFvDetails.ValueOfReturn + spousePrevRetirementPlanFvDetails.ValueOfReturn
                + clientBankCreditUnionFvDetails.ValueOfReturn + spouseBankCreditUnionFvDetails.ValueOfReturn
                + clientBrokerageFvDetails.ValueOfReturn + spouseBrokerageFvDetails.ValueOfReturn
                + clientAnnuityContractFvDetails.ValueOfReturn + spouseAnnuityContractFvDetails.ValueOfReturn;

            double currentRoR = _reportService.GetCurrentRoR(totalPv, totalValueOfReturn);
            double requiredRoR = ExtensionMethod.CalculateRate(ageData.YearToRetirement, totalPmtValue, totalPv, investmentNeededAtRetirement);
            double Fis = _reportService.GetFIS(totalPv, investmentNeededAtRetirement);
            string fileName = $"Arc of Life Report {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
            var model = new ArcOfLifeReportModel
            {
                ClientLogin = clientLogin,
                BusinessLogoImage = businessLogoImage,
                ClientActualRetirementAge = ageData.Client.ActualRetirementAge,
                SpouseActualRetirementAge = ageData.Spouse.ActualRetirementAge,
                ClientRetirementAge = ageData.Client.RetirementAge,
                SpouseRetirementAge = ageData.Spouse.RetirementAge,
                LocalDateTime = DateTime.UtcNow.AddMinutes(-tz),
                TotalHeirs = totalHeirs,
                SpouseHasOccupation = spouseOccupations.Any(),
                HasEstatePlanning = hasEstatePlanning,
                HasHealthCarePlanning = hasHealthCarePlanning,
                LongTermCareRate = LongTermCareRate,
                IsClientRetired = ageData.Client.IsRetired,
                IsSpouseRetired = ageData.Spouse.IsRetired,
                Accumulation = new AccumPrevDistri
                {
                    Residence = primaryResidence,
                    Assets = assetsPv,
                    ClientIncome = clientOccupationIncome + clientPrevRetirementPlanFvDetails.Income + clientOtherIncome + clientInvestmentIncome,
                    SpouseIncome = spouseOccupationIncome + spousePrevRetirementPlanFvDetails.Income + spouseOtherIncome + spouseInvestmentIncome,
                    ClientRetirement = clientRetirementPlanFvDetails.PresentValue + clientPrevRetirementPlanFvDetails.PresentValue,
                    SpouseRetirement = spouseRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue,
                    ClientSocialSecurity = clientSocialSecurityIncome,
                    SpouseSocialSecurity = spouseSocialSecurityIncome,
                    Savings = clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue,
                    Life = clientLifeFvDetails.PresentValue + spouseLifeFvDetails.PresentValue,
                    Annuities = clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue,
                    Investments = clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue,
                    Education = clientEducationFvDetails.PresentValue + spouseEducationFvDetails.PresentValue,
                    CashFlowAnalysis = new CashFlowAnalysis
                    {
                        EarnedIncome = clientOccupationIncome + spouseOccupationIncome,
                        OtherIncome = clientOtherIncome + spouseOtherIncome +
                                      clientInvestmentIncome + spouseInvestmentIncome +
                                      clientSocialSecurityIncome + spouseSocialSecurityIncome,
                        EmployerPension = clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                          clientPensionIncomeAmount + spousePensionIncomeAmount,
                        InflationaryExpenses = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), 1, inflationary),
                        NonInflationaryExpenses = nonInflationary
                    },
                    AssetAnalysis = new AssetAnalysis
                    {
                        EmployerRetirementPlans = clientRetirementPlanFvDetails.PresentValue + spouseRetirementPlanFvDetails.PresentValue + clientPrevRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.PresentValue,
                        BankAssets = clientBankCreditUnionFvDetails.PresentValue + spouseBankCreditUnionFvDetails.PresentValue,
                        PersonalInvestmentAssets = clientBrokerageFvDetails.PresentValue + spouseBrokerageFvDetails.PresentValue,
                        InsuranceAssets = clientAnnuityContractFvDetails.PresentValue + spouseAnnuityContractFvDetails.PresentValue,
                    },
                    PropertyAnalysis = new PropertyAnalysis
                    {
                        RealProperty = realPropertyPv,
                        PersonalProperty = personalPropertyPv
                    },
                },
                Preservation = new AccumPrevDistri
                {
                    Residence = primaryResidenceFv,
                    Assets = assetsFv,
                    ClientIncome = clientOccupationIncomeAtRetirement + clientPrevRetirementPlanFvDetails.Income + clientOtherIncome + clientInvestmentIncome,
                    SpouseIncome = spouseOccupationIncomeAtRetirement + spousePrevRetirementPlanFvDetails.Income + spouseOtherIncome + spouseInvestmentIncome,
                    ClientRetirement = clientRetirementPlanFvDetails.FutureValue + clientPrevRetirementPlanFvDetails.PresentValue,
                    SpouseRetirement = spouseRetirementPlanFvDetails.FutureValue + spousePrevRetirementPlanFvDetails.FutureValue,
                    ClientSocialSecurity = clientSocialSecurityIncomeAtRetirement,
                    SpouseSocialSecurity = spouseSocialSecurityIncomeAtRetirement,
                    Savings = clientBankCreditUnionFvDetails.FutureValue + spouseBankCreditUnionFvDetails.FutureValue,
                    Life = clientLifeFvDetails.FutureValue + spouseLifeFvDetails.FutureValue,
                    Annuities = clientAnnuityContractFvDetails.FutureValue + spouseAnnuityContractFvDetails.FutureValue,
                    Investments = clientBrokerageFvDetails.FutureValue + spouseBrokerageFvDetails.FutureValue,
                    Education = clientEducationFvDetails.FutureValue + spouseEducationFvDetails.FutureValue,
                    CashFlowAnalysis = new CashFlowAnalysis
                    {
                        EarnedIncome = clientOccupationIncomeAtRetirement + spouseOccupationIncomeAtRetirement,
                        EmployerPension = clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                          clientPensionIncomeAtRetirement + spousePensionIncomeAtRetirement,
                        OtherIncome = clientOtherIncome + spouseOtherIncome +
                                      clientInvestmentIncome + spouseInvestmentIncome +
                                      clientSocialSecurityIncomeAtRetirement + spouseSocialSecurityIncomeAtRetirement,
                        InflationaryExpenses = inflationaryFvDetails.FutureValue,
                        NonInflationaryExpenses = nonInflationary
                    },
                    AssetAnalysis = new AssetAnalysis
                    {
                        EmployerRetirementPlans = clientRetirementPlanFvDetails.FutureValue + spouseRetirementPlanFvDetails.FutureValue + clientPrevRetirementPlanFvDetails.FutureValue + spousePrevRetirementPlanFvDetails.FutureValue,

                        BankAssets = clientBankCreditUnionFvDetails.FutureValue + spouseBankCreditUnionFvDetails.FutureValue,
                        PersonalInvestmentAssets = clientBrokerageFvDetails.FutureValue + spouseBrokerageFvDetails.FutureValue,
                        InsuranceAssets = clientAnnuityContractFvDetails.FutureValue + spouseAnnuityContractFvDetails.FutureValue
                    },
                    PropertyAnalysis = new PropertyAnalysis
                    {
                        RealProperty = realPropertyProjected,
                        PersonalProperty = personalPropertyProjected
                    }
                },
                Distribution = new AccumPrevDistri
                {
                    Residence = primaryResidenceFv,
                    Assets = assetsFv,
                    ClientIncome = clientOccupationIncomeAt28YearRetirement + spouseOccupationIncomeAtRetirement +
                                   clientPrevRetirementPlanFvDetails.Income + spousePrevRetirementPlanFvDetails.Income +
                                   clientOtherIncome + spouseOtherIncome +
                                   clientInvestmentIncome + spouseInvestmentIncome,
                    Savings = clientBankCreditUnionFvDetails.Year28FutureValue + spouseBankCreditUnionFvDetails.Year28FutureValue,
                    Life = clientLifeFvDetails.Year28FutureValue + spouseLifeFvDetails.Year28FutureValue,
                    Annuities = clientAnnuityContractFvDetails.Year28FutureValue + spouseAnnuityContractFvDetails.Year28FutureValue,
                    Investments = clientBrokerageFvDetails.Year28FutureValue + spouseBrokerageFvDetails.Year28FutureValue,
                    AssetAnalysis = new AssetAnalysis
                    {
                        EmployerRetirementPlans = clientRetirementPlanFvDetails.Year28FutureValue + spouseRetirementPlanFvDetails.Year28FutureValue +
                                                  clientPrevRetirementPlanFvDetails.Year28FutureValue + spousePrevRetirementPlanFvDetails.Year28FutureValue,
                        BankAssets = clientBankCreditUnionFvDetails.Year28FutureValue + spouseBankCreditUnionFvDetails.Year28FutureValue,
                        PersonalInvestmentAssets = clientBrokerageFvDetails.Year28FutureValue + spouseBrokerageFvDetails.Year28FutureValue,
                        InsuranceAssets = clientAnnuityContractFvDetails.Year28FutureValue + spouseAnnuityContractFvDetails.Year28FutureValue
                    },
                    PropertyAnalysis = new PropertyAnalysis
                    {
                        RealProperty = realPropertyProjected,
                        PersonalProperty = personalPropertyProjected
                    }
                },
                ExplicitVue = new ExplicitVue
                {
                    TotalInvestment = totalPv,
                    StructureIncomeAtRetirement = totalIncomeAtRetirement,
                    ExpenseAtRetirement = incomeNeededAtRetirement,
                    InvestmentAccountNeededAtRetirement = investmentNeededAtRetirement,
                    ProjectedValueOfInvestmentAtRetirement = clientRetirementPlanFvDetails.FutureValue + spouseRetirementPlanFvDetails.FutureValue +
                                                             clientPrevRetirementPlanFvDetails.PresentValue + spousePrevRetirementPlanFvDetails.FutureValue +
                                                             clientBankCreditUnionFvDetails.FutureValue + spouseBankCreditUnionFvDetails.FutureValue +
                                                             clientAnnuityContractFvDetails.FutureValue + spouseAnnuityContractFvDetails.FutureValue +
                                                             clientBrokerageFvDetails.FutureValue + spouseBrokerageFvDetails.FutureValue,
                    CurrentRoR = currentRoR,
                    RequiredRoR = requiredRoR,
                    Fis = Fis
                },
                EstatePlanning = new EstatePlanningModel
                {
                    Will = Estate.Any() && (Estate.FirstOrDefault(m => m.DescriptionItemId == 57).isClientActive || Estate.FirstOrDefault(m => m.DescriptionItemId == 57).isSpouseActive),
                    Trust = Estate.Any() && (Estate.FirstOrDefault(m => m.DescriptionItemId == 58 || m.DescriptionItemId == 59).isClientActive || Estate.FirstOrDefault(m => m.DescriptionItemId == 58 || m.DescriptionItemId == 59).isSpouseActive),
                    PowerOfAttorney = Estate.Any() && (Estate.FirstOrDefault(m => m.DescriptionItemId == 61 || m.DescriptionItemId == 62).isClientActive || Estate.FirstOrDefault(m => m.DescriptionItemId == 61 || m.DescriptionItemId == 62).isSpouseActive)
                },
                FileName = fileName
            };
            model.Accumulation.ClientAssetTotal = model.Accumulation.AssetAnalysis.AssetAnalysisTotal + model.Accumulation.PropertyAnalysis.PropertyAnalysisTotal;
            model.Preservation.ClientAssetTotal = model.Preservation.AssetAnalysis.AssetAnalysisTotal + model.Preservation.PropertyAnalysis.PropertyAnalysisTotal;
            model.Distribution.ClientAssetTotal = model.Distribution.AssetAnalysis.AssetAnalysisTotal + model.Distribution.PropertyAnalysis.PropertyAnalysisTotal;
            model.Accumulation.ClientDebtTotal = totalPropertyDebt + totalBudgetDebt;
            model.Preservation.ClientDebtTotal = totalPropertyDebtAtRetirement + totalBudgetDebtAtRetirement;
            model.Distribution.ClientDebtTotal = totalPropertyDebtAt28YearRetirement + totalBudgetDebtAtAt28YearRetirement;

            model.CashFlowReportData = new List<CashFlowRowModel>();
            int clientAge = 0, spouseAge = 0, inflationaryIncome = 0, nonInflationaryIncome = 0, inflationaryIncomeFv = 0,
                clientSalary = 0, spouseSalary = 0, clientSelfEmployedIncome = 0, spouseSelfEmployedIncome = 0,
                clientPersonalPropertyIncome = 0, spousePersonalPropertyIncome = 0, clientPersonalActivityIncome = 0, spousePersonalActivityIncome = 0,
                clientPensionIncome = 0, spousePensionIncome = 0, clientSocialSecurityIncomes = 0, spouseSocialSecurityIncomes = 0;

            for (int i = 1; i <= year28; i++)
            {
                clientAge = Convert.ToInt32(ageData.Client.Age + i);
                spouseAge = Convert.ToInt32(ageData.Spouse.Age + i);
                var infationaryBudgetData = (await _budgetService.GetBudgetItemsAsync(guid)).Where(m => m.Inflationary).Select(m => new
                {
                    m.BudgetItem.IsRevolvingDebt,
                    m.Inflationary,
                    PresentValue = m.Amount,
                    m.YearsPayOff,
                    FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), i, m.Amount)
                });

                inflationaryIncome = clientData.BudgetVersion ? infationaryBudgetData.Where(m => m.YearsPayOff >= i || !m.IsRevolvingDebt).Sum(m => m.PresentValue) : inflationary;
                inflationaryIncomeFv = clientData.BudgetVersion ? infationaryBudgetData.Where(m => m.YearsPayOff >= i || !m.IsRevolvingDebt).Sum(m => m.FutureValue) : ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), i, inflationary);
                nonInflationaryIncome = clientData.BudgetVersion ? budgetItems.Where(m => !m.Inflationary && (m.YearsPayOff >= i || !m.BudgetItem.IsRevolvingDebt))?.Sum(m => m.Amount) ?? 0 : nonInflationary;

                clientSalary = clientEmployments.Where(m => m.AgeToRetire > clientAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                clientSelfEmployedIncome = clientSelfEmployments.Where(m => m.AgeToRetire > clientAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseSalary = spouseEmployments.Where(m => m.AgeToRetire > spouseAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                spouseSelfEmployedIncome = spouseSelfEmployments.Where(m => m.AgeToRetire > spouseAge)?.Sum(m => m.AnnualEarnings) ?? 0;
                clientPersonalPropertyIncome = clientPersonalProperties.Where(m => m.Years >= i)?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalPropertyIncome = spousePersonalProperties.Where(m => m.Years >= i)?.Sum(m => m.AnnualIncome) ?? 0;
                clientPersonalActivityIncome = clientPersonalActivities.Where(m => m.Years >= i)?.Sum(m => m.AnnualIncome) ?? 0;
                spousePersonalActivityIncome = spousePersonalActivities.Where(m => m.Years >= i)?.Sum(m => m.AnnualIncome) ?? 0;
                clientPensionIncome = clientPensionData.Where(m => m.PensionExpectedAge <= clientAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;
                spousePensionIncome = spousePensionData.Where(m => m.PensionExpectedAge <= spouseAge)?.Sum(m => m.PensionBenefit) * 12 ?? 0;

                if (clientSocialSecurity != null)
                    clientSocialSecurityIncomes = clientSocialSecurity.ssreceiving ? clientSocialSecurity.ssmonthly * 12 : clientAge >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                if (spouseSocialSecurity != null)
                    spouseSocialSecurityIncomes = spouseSocialSecurity.ssreceiving ? spouseSocialSecurity.ssmonthly * 12 : spouseAge >= spouseSocialSecurityAge ? spouseSocialSecurity.ssexpexting * 12 : 0;

                model.CashFlowReportData.Add(new CashFlowRowModel()
                {
                    Id = i,
                    ClientAge = clientAge,
                    SpouseAge = spouseAge,
                    HouseHoldIncomeNeeded = inflationaryIncomeFv + nonInflationaryIncome,
                    ClientSalary = clientSalary,
                    ClientSelfEmpWages = clientSelfEmployedIncome,
                    SpouseSalary = spouseSalary,
                    SpouseSelfEmpWages = spouseSelfEmployedIncome,
                    ClientInvestment = clientBrokerageIncome + clientBankCreditUnionIncome,
                    SpouseInvestment = spouseBrokerageIncome + spouseBankCreditUnionIncome,
                    ClientPersonalProperty = clientPersonalPropertyIncome,
                    SpousePersonalProperty = spousePersonalPropertyIncome,
                    ClientPersonalActivity = clientPersonalActivityIncome,
                    SpousePersonalActivity = spousePersonalActivityIncome,
                    ClientPension = clientPensionIncome + clientPrevRetirementPlanFvDetails.Income,
                    SpousePension = spousePensionIncome + spousePrevRetirementPlanFvDetails.Income,
                    ClientSocialSecurity = clientSocialSecurityIncomes,
                    SpouseSocialSecurity = spouseSocialSecurityIncomes,
                    ClientAnnuity = clientAnnuityIncome,
                    SpouseAnnuity = spouseAnnuityIncome,
                });
            }

            model.HouseHoldIncomeNeededTotal = model.CashFlowReportData.Sum(m => m.HouseHoldIncomeNeeded);
            model.ClientSalaryTotal = model.CashFlowReportData.Sum(m => m.ClientSalary);
            model.ClientSelfEmpWagesTotal = model.CashFlowReportData.Sum(m => m.ClientSelfEmpWages);
            model.SpouseSalaryTotal = model.CashFlowReportData.Sum(m => m.SpouseSalary);
            model.SpouseSelfEmpWagesTotal = model.CashFlowReportData.Sum(m => m.SpouseSelfEmpWages);
            model.ClientInvestmentTotal = model.CashFlowReportData.Sum(m => m.ClientInvestment);
            model.SpouseInvestmentTotal = model.CashFlowReportData.Sum(m => m.SpouseInvestment);
            model.ClientPersonalPropertyTotal = model.CashFlowReportData.Sum(m => m.ClientPersonalProperty);
            model.SpousePersonalPropertyTotal = model.CashFlowReportData.Sum(m => m.SpousePersonalProperty);
            model.ClientPersonalActivityTotal = model.CashFlowReportData.Sum(m => m.ClientPersonalActivity);
            model.SpousePersonalActivityTotal = model.CashFlowReportData.Sum(m => m.SpousePersonalActivity);
            model.ClientPensionTotal = model.CashFlowReportData.Sum(m => m.ClientPension);
            model.SpousePensionTotal = model.CashFlowReportData.Sum(m => m.SpousePension);
            model.ClientSocialSecurityTotal = model.CashFlowReportData.Sum(m => m.ClientSocialSecurity);
            model.SpouseSocialSecurityTotal = model.CashFlowReportData.Sum(m => m.SpouseSocialSecurity);
            model.ClientAnnuityTotal = model.CashFlowReportData.Sum(m => m.ClientAnnuity);
            model.SpouseAnnuityTotal = model.CashFlowReportData.Sum(m => m.SpouseAnnuity);
            return model;
        }
        [AllowAnonymous]
        public async Task<IActionResult> ArcOfLife(string token, int tz)
        {
            var model = await ArcOfLifeData(token, tz);
            if (model != null)
            {
                var pdfViewAction = new ViewAsPdf(model)
                {
                    PageMargins = { Left = 10, Bottom = 7, Right = 10, Top = 7 },
                    PageSize = Size.Letter,
                    PageOrientation = Orientation.Landscape,
                    ContentDisposition = ContentDisposition.Inline,
                    FileName = model.FileName
                };
                return pdfViewAction;
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        public async Task<IActionResult> EngagementLetter(string token, int tz)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
            {
                ViewBag.StatusCode = 403;
                ViewBag.Title = "Access Denied";
                ViewBag.Message = "Sorry, You don't have permission to access this page.";
                return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
            }
            Guid ClientLoginGuid = new(token);
            var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.UserLogin)
                .Include(m => m.UserLogin.UserData).Include(m => m.UserLogin.UserData.RIA)
                .Include(m => m.UserLogin.UserData.FMO).Include(m => m.UserLogin.UserData.BD).FirstOrDefaultAsync();
            var adviserData = client.UserLogin.UserData;
            var planningItems = await _commonService.GetPlanningItemsAsync(ClientLoginGuid);
            string businessLogoImage = string.Empty;
            if (!string.IsNullOrEmpty(adviserData.BusinessLogoFileName))
            {
                string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
                string extension = Path.GetExtension(adviserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                businessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.BusinessLogoFileName, contentType);
            }
            var fileName = $"{client.ClientData.FirstName}'s Engagement Letter {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
            var model = new ClientSurveyModel
            {
                ClientLoginData = client,
                PlanningItems = await planningItems.ToListAsync(),
                BusinessLogoImage = businessLogoImage,
                FileName = fileName
            };
            var company = adviserData.BusinessName;
            string CustomSwitches = $"--footer-left \"{company}\" --footer-right \"Page [page] of [topage]\" --footer-line --footer-font-size \"12\" --footer-font-name \"Lato\"";
            return new ViewAsPdf(model)
            {
                PageMargins = { Left = 15, Bottom = 15, Right = 15, Top = 15 },
                CustomSwitches = CustomSwitches,
                ContentDisposition = ContentDisposition.Inline,
                FileName = fileName
            };
        }
        [Route("report/cfs/{cfsid}")]
        public IActionResult CFSReport(string cfsid)
        {
            string url = $"{_config["Integration:CFS:Url"]}/cocoon/cfs/api-{cfsid}.html";
            return new UrlAsPdf(url);
        }
        public async Task<BalanceSheetReportModel> BalanceSheetReportData(string token)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid ClientLoginGuid = new(token);
            var clientLogin = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.InvestmentLiteData)
                .Include(m => m.UserLogin).Include(m => m.UserLogin.UserData).Include(m => m.UserLogin.AdviserContactInfos)
                .Include(m => m.ClientContactInfo).FirstOrDefaultAsync();
            var clientData = clientLogin.ClientData;
            var adviserData = clientLogin.UserLogin.UserData;
            var spouseData = clientLogin.SpouseData;
            var planningItems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).Where(m => m.Active);
            bool hasInvestmentPlanning = planningItems.Any(m => m.PlanningItem.ItemName.Contains("Investment"));
            bool isMarried = clientData.MaritalStatus == MaritalStatusEnum.Married && spouseData != null && Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();

            AgeModel ageData = await _commonService.GetAgeData(ClientLoginGuid);
            List<IncomeModel> AllIncomes = new();
            List<IncomeModel> AllFutureIncomes = new();
            List<AssetModel> AllAssets = new();
            List<BudgetModel> AllBudgets = new();

            var EmploymentData = await _employmentService.GetEmploymentDataAsync(ClientLoginGuid, default);
            var EmploymentDataAtRetirement = EmploymentData.Where(m => m.AgeToRetire > ageData.Client.RetirementAge);
            foreach (var income in EmploymentData)
                AllIncomes.Add(new IncomeModel
                {
                    ClientType = income.EmploymentOf,
                    Title = income.Employer,
                    Amount = income.AnnualEarnings,
                    ModuleName = $"{income.EmploymentStatusString} Employment".Replace("Employee ", ""),
                    Years = income.EmploymentOf == ClientTypeEnum.Client ? clientData.DateOfBirth.GetRemainYearToRetirement(income.AgeToRetire) : spouseData.DateOfBirth.GetRemainYearToRetirement(income.AgeToRetire)
                });
            foreach (var income in EmploymentDataAtRetirement)
                AllFutureIncomes.Add(new IncomeModel
                {
                    ClientType = income.EmploymentOf,
                    Title = income.Employer,
                    Amount = income.AnnualEarnings,
                    ModuleName = $"{income.EmploymentStatusString} Employment".Replace("Employee ", ""),
                    Years = income.EmploymentOf == ClientTypeEnum.Client ? clientData.DateOfBirth.GetRemainYearToRetirement(income.AgeToRetire) : spouseData.DateOfBirth.GetRemainYearToRetirement(income.AgeToRetire)
                });

            var PrevRetirementPlanData = await _employmentService.GetPreviousRetirementPlanDataAsync(ClientLoginGuid, default);
            foreach (var income in PrevRetirementPlanData)
            {
                AllIncomes.Add(new IncomeModel
                {
                    ClientType = income.EmploymentOf,
                    Title = income.EmployerName,
                    Amount = income.PreEmpAnnualAmount,
                    ModuleName = "Previous Retirement"
                });
                AllFutureIncomes.Add(new IncomeModel
                {
                    ClientType = income.EmploymentOf,
                    Title = income.EmployerName,
                    Amount = income.PreEmpAnnualAmount,
                    ModuleName = "Previous Retirement"
                });
            }
            var RetirementPlanDataAtRetirement = (await _employmentService.GetRetirementPlanDataAsync(ClientLoginGuid, default)).Where(m => m.PensionExpectedAge <= ageData.Client.RetirementAge);
            foreach (var income in RetirementPlanDataAtRetirement)
                AllFutureIncomes.Add(new IncomeModel
                {
                    ClientType = income.ClientOccupation.EmploymentOf,
                    Title = income.ClientOccupation.Employer,
                    Amount = income.PensionBenefit * 12,
                    ModuleName = "Pension"
                });

            var PersonalActivityIncome = await _otherIncomeService.GetOtherIncomeDataAsync(ClientLoginGuid, default);
            var PersonalActivityIncomeAtRetirement = PersonalActivityIncome.Where(m => m.Years >= ageData.YearToRetirement);
            foreach (var income in PersonalActivityIncome)
                AllIncomes.Add(new IncomeModel
                {
                    ClientType = income.businessInterestof,
                    Title = income.sourceofIncomeString,
                    Amount = income.AnnualIncome,
                    Years = income.Years,
                    ModuleName = "Personal Activity"
                });
            foreach (var income in PersonalActivityIncomeAtRetirement)
                AllFutureIncomes.Add(new IncomeModel
                {
                    ClientType = income.businessInterestof,
                    Title = income.sourceofIncomeString,
                    Amount = income.AnnualIncome,
                    ModuleName = "Personal Activity",
                    Years = income.Years,
                });

            var PersonalAssetsIncomes = await _propertyService.GetPropertyDataAsync(ClientLoginGuid, default);
            var PersonalAssetsIncomesAtRetirement = PersonalAssetsIncomes.Where(m => m.Years >= ageData.YearToRetirement);
            foreach (var income in PersonalAssetsIncomes)
            {
                string title = !string.IsNullOrEmpty(income.Nickname) ? $"{income.PropertyTypeString}-{income.Nickname}" : income.PropertyTypeString;
                AllIncomes.Add(new IncomeModel
                {
                    ClientType = income.Ownership == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                             income.Ownership == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                    Title = title,
                    Amount = income.AnnualIncome,
                    Years = income.Years,
                    ModuleName = "Personal Asset"
                });
            }
            foreach (var income in PersonalAssetsIncomesAtRetirement)
            {
                string title = !string.IsNullOrEmpty(income.Nickname) ? $"{income.PropertyTypeString}-{income.Nickname}" : income.PropertyTypeString;
                AllFutureIncomes.Add(new IncomeModel
                {
                    ClientType = income.Ownership == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                 income.Ownership == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                    Title = title,
                    Amount = income.AnnualIncome,
                    Years = income.Years,
                    ModuleName = "Personal Asset"
                });
            }
            foreach (var asset in RetirementPlanDataAtRetirement)
                AllAssets.Add(new AssetModel
                {
                    ClientType = asset.ClientOccupation.EmploymentOf,
                    ModuleName = "Retirement",
                    Title = asset.ClientOccupation.Employer,
                    CurrentValue = asset.EspCurrentBalance,
                    FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(asset.EspAvgRateReturn), ageData.YearToRetirement, asset.EspCurrentBalance, 0, asset.EspContributionCalculated + asset.EspEmployerMatchCalculated),
                });

            foreach (var asset in PersonalAssetsIncomes)
            {
                string title = !string.IsNullOrEmpty(asset.Nickname) ? $"{asset.PropertyTypeString}-{asset.Nickname}" : asset.PropertyTypeString;
                AllAssets.Add(new AssetModel
                {
                    ClientType = asset.Ownership == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                 asset.Ownership == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                    ModuleName = "Personal Property",
                    Title = asset.PropertyTypeString,
                    CurrentValue = asset.CurrentValue,
                    FutureValue = asset.ProjectedFairMarketValueRetirement
                });
            }
            if (hasInvestmentPlanning)
            {
                if (clientData.InvestmentVersion)
                {
                    var bankingAccountTypes = new BankCreditUnionAccountTypeEnum[] { BankCreditUnionAccountTypeEnum.Savings, BankCreditUnionAccountTypeEnum.CertificateOfDeposit, BankCreditUnionAccountTypeEnum.MoneyMarket, BankCreditUnionAccountTypeEnum.PersonalChecking };
                    var BankCreditUnionData = await _investmentService.GetBankCreditUnionDataAsync(ClientLoginGuid, default, bankingAccountTypes);
                    var BankCreditUnionDataAtRetirement = BankCreditUnionData.Where(m => m.Years > ageData.Client.RetirementAge);
                    foreach (var income in BankCreditUnionData)
                        AllIncomes.Add(new IncomeModel
                        {
                            ClientType = income.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         income.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            Title = income.AccountTypeString,
                            Amount = income.AnnualIncome,
                            Years = income.Years,
                            ModuleName = "Banking"
                        });
                    foreach (var income in BankCreditUnionDataAtRetirement)
                        AllFutureIncomes.Add(new IncomeModel
                        {
                            ClientType = income.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         income.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            Title = income.AccountTypeString,
                            Amount = income.AnnualIncome,
                            Years = income.Years,
                            ModuleName = "Banking"
                        });

                    var brokerageAccountTypes = new BrokerageAdvisoryAccountTypeEnum[] { BrokerageAdvisoryAccountTypeEnum.Retirement, BrokerageAdvisoryAccountTypeEnum.SingleUse };
                    var BrokerageAdvisoryData = await _investmentService.GetBrokerageAdvisoryDataAsync(ClientLoginGuid, default, brokerageAccountTypes);
                    var BrokerageAdvisoryDataAtRetirement = BrokerageAdvisoryData.Where(m => m.Years > ageData.Client.RetirementAge);
                    foreach (var income in BrokerageAdvisoryData)
                        AllIncomes.Add(new IncomeModel
                        {
                            ClientType = income.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         income.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            Title = income.AccountTypeString,
                            Amount = income.AnnualIncome,
                            Years = income.Years,
                            ModuleName = "Investment"
                        });
                    foreach (var income in BrokerageAdvisoryDataAtRetirement)
                        AllFutureIncomes.Add(new IncomeModel
                        {
                            ClientType = income.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         income.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            Title = income.AccountTypeString,
                            Amount = income.AnnualIncome,
                            Years = income.Years,
                            ModuleName = "Investment"
                        });

                    var LifeAnnuityData = await _investmentService.GetLifeAnnuityContractDataAsync(ClientLoginGuid, default, new ContractTypeEnum[] { ContractTypeEnum.Annuity });
                    foreach (var income in LifeAnnuityData)
                        AllIncomes.Add(new IncomeModel
                        {
                            ClientType = income.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         income.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            Title = income.AccountName,
                            Amount = income.AnnualIncome,
                            Years = income.Years,
                            ModuleName = "Insurance"
                        });
                    foreach (var income in LifeAnnuityData)
                        AllFutureIncomes.Add(new IncomeModel
                        {
                            ClientType = income.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         income.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            Title = income.AccountName,
                            Amount = income.AnnualIncome,
                            Years = income.Years,
                            ModuleName = "Insurance"
                        });

                    foreach (var asset in BankCreditUnionData)
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = asset.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         asset.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            ModuleName = "Banking",
                            Title = asset.AccountTypeString,
                            CurrentValue = asset.CurrentBalance,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(asset.AvgRateReturn), ageData.YearToRetirement, asset.CurrentBalance, asset.AnnualIncome, asset.AnnualContribtion),
                        });
                    foreach (var asset in BrokerageAdvisoryData)
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = asset.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         asset.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            ModuleName = "Investment",
                            Title = asset.AccountTypeString,
                            CurrentValue = asset.CurrentBalance,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(asset.AvgRateReturn), ageData.YearToRetirement, asset.CurrentBalance, asset.AnnualIncome, asset.AnnualContribtion),
                        });
                    foreach (var asset in LifeAnnuityData)
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = asset.Owner == ((int)ClientTypeEnum.Client).ToString() ? ClientTypeEnum.Client :
                                         asset.Owner == ((int)ClientTypeEnum.Spouse).ToString() ? ClientTypeEnum.Spouse : ClientTypeEnum.Other,
                            ModuleName = "Insurance",
                            Title = asset.TypeString,
                            CurrentValue = asset.CurrentBalance,
                            FutureValue = asset.ContractType == ContractTypeEnum.Annuity ? ExtensionMethod.CalculateFutureValue(Convert.ToDouble(asset.RateOfReturn), ageData.YearToRetirement, asset.CurrentBalance, asset.AnnualIncome, asset.AnnualContribution) : asset.ContractFaceAmount,
                        });
                }
                else
                {
                    var investmentData = clientLogin.InvestmentLiteData;
                    if (investmentData != null)
                    {
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = ClientTypeEnum.Client,
                            ModuleName = "Banking",
                            Title = "Qualified Accounts",
                            CurrentValue = investmentData.BankInflationaryInvestmentAmount,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BankInflationaryRateOfReturn), ageData.YearToRetirement, investmentData?.BankInflationaryInvestmentAmount)
                        });
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = ClientTypeEnum.Client,
                            ModuleName = "Banking",
                            Title = "Non-Qualified Accounts",
                            CurrentValue = investmentData.BankNonInflationaryInvestmentAmount,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BankNonInflationaryRateOfReturn), ageData.YearToRetirement, investmentData?.BankNonInflationaryInvestmentAmount)
                        });
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = ClientTypeEnum.Client,
                            ModuleName = "Investment",
                            Title = "Qualified Accounts",
                            CurrentValue = investmentData.BrokerageInflationaryInvestmentAmount,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BrokerageInflationaryRateOfReturn), ageData.YearToRetirement, investmentData?.BrokerageInflationaryInvestmentAmount)
                        });
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = ClientTypeEnum.Client,
                            ModuleName = "Investment",
                            Title = "Non-Qualified Accounts",
                            CurrentValue = investmentData.BrokerageNonInflationaryInvestmentAmount,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.BrokerageNonInflationaryRateOfReturn), ageData.YearToRetirement, investmentData?.BrokerageNonInflationaryInvestmentAmount)
                        });
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = ClientTypeEnum.Client,
                            ModuleName = "Insurance",
                            Title = "Qualified Accounts",
                            CurrentValue = investmentData.AnnuityInflationaryInvestmentAmount,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.AnnuityInflationaryRateOfReturn), ageData.YearToRetirement, investmentData?.AnnuityInflationaryInvestmentAmount)
                        });
                        AllAssets.Add(new AssetModel
                        {
                            ClientType = ClientTypeEnum.Client,
                            ModuleName = "Insurance",
                            Title = "Non-Qualified Accounts",
                            CurrentValue = investmentData.AnnuityNonInflationaryInvestmentAmount,
                            FutureValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(investmentData?.AnnuityNonInflationaryRateOfReturn), ageData.YearToRetirement, investmentData?.AnnuityNonInflationaryInvestmentAmount)
                        });
                    }
                }
            }

            int clientSocialSecurityAge = 0, spouseSocialSecurityAge = 0;
            var clientSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Client);
            int clientSocialSecurityIncome = 0, clientSocialSecurityIncomeAtRetirement = 0;
            int spouseSocialSecurityIncome = 0, spouseSocialSecurityIncomeAtRetirement = 0;
            if (clientSocialSecurity != null)
            {
                clientSocialSecurityAge = clientSocialSecurity.sSageBenifitsEnum;
                if (clientSocialSecurity.ssreceiving)
                {
                    clientSocialSecurityIncome = clientSocialSecurity.ssmonthly * 12;
                    clientSocialSecurityIncomeAtRetirement = clientSocialSecurity.ssmonthly * 12;
                }
                else
                {
                    clientSocialSecurityIncome = ageData.Client.Age >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                    clientSocialSecurityIncomeAtRetirement = ageData.Client.RetirementAge >= clientSocialSecurityAge ? clientSocialSecurity.ssexpexting * 12 : 0;
                }
                AllIncomes.Add(new IncomeModel
                {
                    ClientType = clientSocialSecurity.EmploymentOf,
                    Title = "Social Security",
                    Amount = clientSocialSecurityIncome
                });
                AllFutureIncomes.Add(new IncomeModel
                {
                    ClientType = clientSocialSecurity.EmploymentOf,
                    Title = "Social Security",
                    Amount = clientSocialSecurityIncomeAtRetirement
                });
            }
            if (isMarried)
            {
                var spouseSocialSecurity = await _socialSecurityService.GetSocialSecurityDataAsync(ClientLoginGuid, ClientTypeEnum.Spouse);
                if (spouseSocialSecurity != null)
                {
                    spouseSocialSecurityAge = spouseSocialSecurity.sSageBenifitsEnum;
                    if (spouseSocialSecurity.ssreceiving)
                    {
                        spouseSocialSecurityIncome = spouseSocialSecurity.ssmonthly * 12;
                        spouseSocialSecurityIncomeAtRetirement = spouseSocialSecurity.ssmonthly * 12;
                    }
                    else
                    {
                        spouseSocialSecurityIncome = ageData.Client.Age >= spouseSocialSecurityAge ? spouseSocialSecurity.ssexpexting * 12 : 0;
                        spouseSocialSecurityIncomeAtRetirement = ageData.Spouse.RetirementAge >= spouseSocialSecurityAge ? spouseSocialSecurity.ssexpexting * 12 : 0;
                    }
                    AllIncomes.Add(new IncomeModel
                    {
                        ClientType = spouseSocialSecurity.EmploymentOf,
                        Title = "Social Security",
                        Amount = spouseSocialSecurityIncome
                    });
                    AllFutureIncomes.Add(new IncomeModel
                    {
                        ClientType = spouseSocialSecurity.EmploymentOf,
                        Title = "Social Security",
                        Amount = spouseSocialSecurityIncomeAtRetirement
                    });
                }
            }
            if (clientData.BudgetVersion)
            {
                var Budgets = (await _budgetService.GetBudgetItemsAsync(ClientLoginGuid)).OrderBy(m => m.BudgetItem.SortOrder);
                foreach (var item in Budgets)
                    AllBudgets.Add(new BudgetModel
                    {
                        Title = item.BudgetItem.ItemName,
                        CurrentValue = item.Inflationary ? ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), 1, item.Amount) : item.Amount,
                        FutureValue = item.Inflationary ? ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), ageData.YearToRetirement, item.Amount) : item.Amount,
                        ModuleName = item.BudgetItem.Parent.ItemName
                    });
            }
            else
            {
                var budgetExpressData = await _budgetService.GetInfaltionaryBudgetFutureValueExpressAsync(ClientLoginGuid);
                AllBudgets.Add(new BudgetModel
                {
                    Title = "Inflationary",
                    CurrentValue = ExtensionMethod.CalculateFutureValue(Convert.ToDouble(clientData.AssumedInflationRate), 1, budgetExpressData.PresentValue),
                    FutureValue = budgetExpressData.FutureValue,
                    ModuleName = "Express"
                });
                AllBudgets.Add(new BudgetModel
                {
                    Title = "Non-Inflationary",
                    CurrentValue = clientData.NonInflationaryBudget,
                    FutureValue = clientData.NonInflationaryBudget,
                    ModuleName = "Express"
                });
            }
            string businessLogoImage = string.Empty;
            if (!string.IsNullOrEmpty(adviserData.BusinessLogoFileName))
            {
                string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
                string extension = Path.GetExtension(adviserData.BusinessLogoFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                businessLogoImage = await _fileHelper.GetS3FileBase64String(bucketPath, adviserData.BusinessLogoFileName, contentType);
            }
            var model = new BalanceSheetReportModel()
            {
                ClientLoginData = clientLogin,
                ClientName = clientData.FirstName,
                SpouseName = isMarried ? spouseData.FirstName : string.Empty,
                BusinessLogoImage = businessLogoImage,
                FileName = "Balance Sheet Report.pdf",
                BudgetVersion = clientData.BudgetVersion,
                CurrentIncomes = AllIncomes.OrderBy(m => m.ClientType).Where(m => m.Amount > 0).ToList(),
                FutureIncomes = AllFutureIncomes.OrderBy(m => m.ClientType).Where(m => m.Amount > 0).ToList(),
                Assets = AllAssets.OrderBy(m => m.ClientType).Where(m => m.CurrentValue > 0).ToList(),
                Budgets = AllBudgets.Where(m => m.CurrentValue > 0).ToList(),
            };
            return model;
        }
        public async Task<IActionResult> BalanceSheetReport(string token, bool html = false)
        {
            var model = await BalanceSheetReportData(token);
            if (model != null)
            {
                var footerAction = _httpContextAccessor.HttpContext.GetAbsoluteUrl("Home/ReportFooter");
                string customSwitches = string.Format("--footer-html {0} --footer-spacing -10", footerAction);
                var pdfViewAction = new ViewAsPdf("BalanceSheet", model)
                {
                    PageSize = Size.Letter,
                    PageMargins = { Left = 0, Bottom = 13, Right = 0, Top = 10 },
                    CustomSwitches = customSwitches,
                    ContentDisposition = ContentDisposition.Inline,
                    FileName = model.FileName
                };
                if (html)
                    return View("BalanceSheet", model);
                return pdfViewAction;
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        public async Task<IActionResult> FirmAdv2b(string token, int tz)
        {
            var model = await FirmAdv2bDataAsync(token, tz);
            if (model != null)
            {
                var currentDateTime = DateTime.UtcNow.AddMinutes(-tz).ToString("MMM dd, yyyy hh:mm tt");
                string customSwitches = $"--footer-left \"{currentDateTime}\" --footer-right \"Page [page]\" --footer-line --footer-font-size \"10\" --footer-font-name \"Lato\"";
                var pdfViewAction = new ViewAsPdf(model)
                {
                    PageSize = Size.Letter,
                    PageMargins = { Left = 10, Bottom = 15, Right = 10, Top = 15 },
                    CustomSwitches = customSwitches,
                    ContentDisposition = ContentDisposition.Inline,
                    FileName = model.FileName
                };
                return pdfViewAction;
            }
            ViewBag.StatusCode = 403;
            ViewBag.Title = "Access Denied";
            ViewBag.Message = "Sorry, You don't have permission to access this page.";
            return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
        }
        public async Task<FirmAdv2bModel> FirmAdv2bDataAsync(string token, int tz)
        {
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
                return null;
            Guid UserLoginGuid = new(token);
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid)
                .Include(m => m.UserLogin).Include(m => m.RIA).FirstOrDefaultAsync();
            var model = new FirmAdv2bModel();
            if (user.RIAId > 0)
            {
                var users = _context2.UserLogins.Where(m => m.FirmId == user.UserLogin.FirmId && m.UserData.RIAId == user.RIA.Id && m.IsDeleted != true && (m.AdminRole == AdminRoleEnum.Adviser || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)))
               .Include(m => m.UserData);
                string fileName = $"{user.RIA.Name} ADV 2B {DateTime.UtcNow.AddMinutes(-tz):MMM dd, yyyy hh:mm tt}.pdf";
                var firmId = user.UserLogin.FirmId;
                var userAdv2bDocs = _context2.ComplianceDocuments.Where(m => users.Select(n => n.Id).Contains(m.UserId) && m.FileName.Contains("ADV 2B"))
                    .OrderByDescending(m => m.UploadedDate);
                model = new FirmAdv2bModel
                {
                    FirmName = user.RIA.Name,
                    FileName = fileName,
                    Members = users.Select(m => new FirmAdv2bMemberModel
                    {
                        Name = m.UserData.Name,
                        Email = m.Username,
                        Role = m.AdminRole,
                        IsAssistant = m.IsAssistant,
                        Active = m.Active,
                        Document = userAdv2bDocs.FirstOrDefault(n => n.UserId == m.Id)
                    })
                };
            }
            return model;
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _commonService.Dispose();
                }
                disposed = true;
            }
        }
        ~ReportController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}