﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Areas.Admin.ViewComponents
{
    public class AdminLeftNavViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly Guid UserLoginGuid;
        public AdminLeftNavViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var OwnerUserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "OwnerUserId")?.Value;

            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.Permissions).Include(m => m.AssistantPermissions).FirstOrDefaultAsync();
            ViewBag.IsAssistant = adviserDataObj.IsAssistant;
            ViewBag.additionalRoles = adviserDataObj.Permissions.Count > 0 ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            ViewBag.assistantPermissions = adviserDataObj.AssistantPermissions.Count > 0 ? string.Join(",", adviserDataObj.AssistantPermissions.Select(m => (int)m.Permission)) : null;
            return await Task.FromResult(View("~/Areas/Admin/Views/Shared/Components/AdminLeftNav/Default.cshtml"));
        }
    }
}
