﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.Areas.Admin.ViewComponents
{
    public class AdminRightPanelViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly FileHelper _fileHelper;
        protected readonly IConfiguration _config;
        protected readonly Guid UserLoginGuid;
        public AdminRightPanelViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor, IConfiguration config)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _config = config;
            _fileHelper = new FileHelper(_config);
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var admin = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.UserData).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = admin.Permissions.Count > 0 ? string.Join(",", admin.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            ViewBag.AdminName = $"{admin.UserData.FirstName} {admin.UserData.LastName}";
            var userData = admin.UserData;
            string bucketPath = $"{_config["BucketName"]}/avatars/adviser";
            if (!string.IsNullOrEmpty(userData.AvatarFileName))
            {
                string extension = Path.GetExtension(userData.AvatarFileName).Replace(".", "");
                string contentType = $"image/{extension}";
                ViewBag.avatarImage = await _fileHelper.GetS3FileBase64String(bucketPath, userData.AvatarFileName, contentType);
            }

            return await Task.FromResult(View("~/Areas/Admin/Views/Shared/Components/AdminRightPanel/Default.cshtml"));
        }
    }
}
