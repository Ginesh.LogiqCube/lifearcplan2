﻿using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LAP.Areas.Admin.ViewComponents
{
    public class AdminTopBarViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly Guid UserLoginGuid;
        protected readonly Guid AdviserUserLoginGuid;
        protected readonly Guid UserGuid;
        public AdminTopBarViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            AdviserUserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "AdviserUserLoginGuid").ClaimToGuid();
            UserGuid = _httpContextAccessor.HttpContext.Request.Query["flag"] == "true" ? AdviserUserLoginGuid : UserLoginGuid;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var role = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Role)?.Value;
            var admin = await _context2.UserLogins.Where(m => m.Guid == UserGuid).Include(m => m.UserData).FirstOrDefaultAsync();
            var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
            ViewBag.AdminName = $"{admin.UserData.FirstName} {admin.UserData.LastName}";
            ViewBag.FirmId = admin.FirmId;
            ViewBag.AdminId = admin.Id;
            ViewBag.LoginUrl = admin.AdminRole == AdminRoleEnum.SuperAdmin ? "super-admin/login" : "user/login";
            ViewBag.DisplayOnlyAdviser = _httpContextAccessor.HttpContext.Request.Query["flag"] == "true";
            ViewBag.IsAdv2bValid = admin.UserData.DateOfBirth is not null && admin.AdviserContactInfos is not null;
            ViewBag.TimeZone = Convert.ToInt32(TimeZone);
            return await Task.FromResult(View("~/Areas/Admin/Views/Shared/Components/AdminTopBar/Default.cshtml"));
        }
    }
}