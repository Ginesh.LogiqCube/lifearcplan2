﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Linq.Dynamic.Core;
using LAP.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using LAP.Model;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using System.Net.Mail;
using System.IO;

namespace LAP.Areas.Admin.Controllers
{
    [Area(Roles.Admin)]
    [Authorize(Roles = Roles.SuperAdmin, AuthenticationSchemes = AuthenticationSchemes.SuperAdminAuth)]
    public class SuperAdminController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        public SuperAdminController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
        }
        public async Task<IActionResult> FirmPrincipalList()
        {
            ViewBag.PermissionTypes = await _dropdownDataHelper.GetPermissionTypesAsync((int)AdminRoleEnum.SuperAdmin);
            return View();
        }
        public IActionResult FirmAdminList()
        {
            return View();
        }
        public async Task<IActionResult> AdviserList()
        {
            ViewBag.PermissionTypes = await _dropdownDataHelper.GetPermissionTypesAsync((int)AdminRoleEnum.SuperAdmin);
            return View();
        }
        public IActionResult ClientList()
        {
            return View();
        }
        public async Task<IActionResult> FetchFirms(DatatableMetaModel meta, CommonSearchModel query)
        {
            var firms = _context2.Firms.Select(m => new
            {
                m.Id,
                FirmName = m.Name,
                TotalAdviser = m.UserLogins.Count(m => m.AdminRole == AdminRoleEnum.Adviser || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)),
                TotalFirmAdmin = m.UserLogins.Count(m => m.AdminRole == AdminRoleEnum.FirmAdmin || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.FirmAdmin)),
                TotalFirmPrincipal = m.UserLogins.Count(m => m.AdminRole == AdminRoleEnum.FirmPrincipal),
                TotalRegulatoryDocument = m.RegulatoryDocuments.AsEnumerable().Count()
            });
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                firms = firms.Where(m => m.FirmName != "LQC");
            int rowsTotal = firms.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            var result = await firms.PagedListAsync(meta, null, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> FetchAdmins(int? FirmId, int? role, DatatableMetaModel meta, CommonSearchModel query)
        {
            FirmId = FirmId > 0 ? FirmId : query.FirmId;
            IQueryable<UserLogin> firmPrincipals = Enumerable.Empty<UserLogin>().AsQueryable();
            if (FirmId > 0)
                firmPrincipals = _context2.UserLogins.Where(m => m.FirmId == FirmId && (m.AdminRole == EnumHelper.ToEnum<AdminRoleEnum>(role.ToString()) || m.Permissions.Any(n => n.RoleType == EnumHelper.ToEnum<AdminRoleEnum>(role.ToString()))));
            else
                firmPrincipals = _context2.UserLogins.Where(m => m.AdminRole == EnumHelper.ToEnum<AdminRoleEnum>(role.ToString()) || m.Permissions.Any(n => n.RoleType == EnumHelper.ToEnum<AdminRoleEnum>(role.ToString())));

            int rowsTotal = firmPrincipals.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);

            if (query.RolesList != null && query.RolesList.Count > 0)
                firmPrincipals = firmPrincipals.Where(m => query.RolesList.Contains((int)m.AdminRole) || m.Permissions.Any(n => query.RolesList.Contains((int)n.RoleType)));

            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "UserData,Firm,Permissions";
            var result = await firmPrincipals.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> FetchAdvisers(int? FirmId, DatatableMetaModel meta, CommonSearchModel query)
        {
            FirmId = FirmId > 0 ? FirmId : query.FirmId;
            IQueryable<UserLogin> advisers = Enumerable.Empty<UserLogin>().AsQueryable();
            if (FirmId > 0)
                advisers = _context2.UserLogins.Where(m => m.FirmId == FirmId && (m.AdminRole == AdminRoleEnum.Adviser || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)))
                    .OrderByDescending(m => m.Active);
            else
                advisers = _context2.UserLogins.Where(m => m.AdminRole == AdminRoleEnum.Adviser || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser))
                    .OrderByDescending(m => m.Active);

            int rowsTotal = advisers.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);

            if (query.RolesList != null && query.RolesList.Count > 0)
                advisers = advisers.Where(m => query.RolesList.Contains((int)m.AdminRole) || m.Permissions.Any(n => query.RolesList.Contains((int)n.RoleType)));

            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "AdviserContactInfos,UserData,Permissions";
            var result = await advisers.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Data = result.Data.ToList().Select(m =>
            {
                m.EncryptedGuid = m.Guid.ToString().EncryptAsync().Result;
                return m;
            }).AsQueryable();
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public IActionResult ChangeEmailModal(Guid guid)
        {
            ViewBag.ClientLoginGuid = guid;
            return View("_ChangeEmailModal");
        }
        public IActionResult Affiliations()
        {
            return View();
        }
        public IActionResult FetchAffiliationTypes()
        {
            var affiliationsTypes = Enum.GetValues(typeof(AffiliationTypeEnum)).Cast<AffiliationTypeEnum>();
            List<object> affiliations = new();
            var allFirms = _context2.Affiliations;
            foreach (AffiliationTypeEnum item in affiliationsTypes)
                affiliations.Add(new
                {
                    Id = (int)item,
                    Type = item.ToString(),
                    TypeString = item.GetDisplayName(),
                    TotalFirm = allFirms.Count(m => m.Type == item)
                });
            return Json(affiliations);
        }
        public async Task<IActionResult> FetchAffiliations(AffiliationTypeEnum type, DatatableMetaModel meta, CommonSearchModel query)
        {
            var affilations = _context2.Affiliations.Where(m => m.Type == type);
            int rowsTotal = affilations.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            searchQuery = searchQuery.Replace("AffliationName", "Name");
            if (!string.IsNullOrEmpty(query.ComplianceName))
                affilations = affilations.Where(m => m.Email.Contains(query.ComplianceName) || m.ComplianceOfficerName.Contains(query.ComplianceName));

            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            var result = await affilations.PagedListAsync(meta,null, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteAffiliation(Guid Guid)
        {
            var affilation = await _context2.Affiliations.Where(m => m.Guid == Guid).FirstOrDefaultAsync();
            bool isExist = _context2.UserDatas.Any(m => m.FMOId == affilation.Id || m.BDId == affilation.Id || m.RIAId == affilation.Id);
            if (affilation is not null && !isExist)
            {
                _context2.Affiliations.Remove(affilation);
                await _context2.SaveChangesAsync();
            }
            var adviserCount = await _context2.Affiliations.CountAsync(m => m.Type == affilation.Type);
            var result = new CommonResponse
            {
                Success = affilation is not null && !isExist,
                Message = affilation is not null && !isExist ? Message.RECORD_DELETE : string.Format(Message.ALREADY_ASSOCIATED, "Firm", "an adviser"),
                ParentId = (int)affilation.Type,
                Data = new { AdviserCount = adviserCount }
            };
            return Json(result);
        }
        public IActionResult RegulatoryDocuments()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> MigrateClient(string clientIds, int userId, int sourceUserId, bool isAllChecked)
        {
            var allClients = _context2.ClientLogins.Where(m => m.UserId == sourceUserId && m.IsDeleted != true).Include(m => m.UserLogin).Include(m => m.UserLogin.UserData);
            clientIds = isAllChecked ? string.Join(",", allClients.Select(m => m.Id)) : clientIds;
            var query = $"update client.clientlogin set userid={userId} where id in ({clientIds})";
            _context2.Database.ExecuteSqlRaw(query);
            var newAdviser = await _context2.UserLogins.Where(m => m.Id == userId).Include(m => m.UserData).FirstOrDefaultAsync();
            var newAdviserName = newAdviser.UserData.Name;
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
            {
                EmailHelper emailHelper = new(_config);
                string mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml = mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());

                string contentEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));

                var ids = clientIds.Split(",").ToList();
                var clients = _context2.ClientDatas.Where(m => ids.Contains(m.ClientLogin.Id.ToString())).Include(m => m.ClientLogin);
                var oldAdviser = await _context2.UserLogins.Where(m => m.Id == sourceUserId).Include(m => m.UserData).FirstOrDefaultAsync();
                var oldAdviserName = oldAdviser.UserData.Name;
                var admins = _context2.UserLogins.Where(m => m.FirmId == newAdviser.FirmId && m.AdminRole == AdminRoleEnum.FirmPrincipal && m.AdminRole == AdminRoleEnum.FirmAdmin).Select(m => m.Username);
                string guidToken = await newAdviser.Guid.ToString().EncryptAsync();
                var adviserLink = Url.RouteUrl("adviser-documents", new { token = guidToken }, Request.Scheme);
                var rowsHtml = string.Empty;

                foreach (var item in clients)
                {
                    StringBuilder clientmainEmailTemplateHtml = new(mainEmailTemplateHtml);
                    StringBuilder clientContentEmailTemplateHtml = new(contentEmailTemplateHtml);
                    clientContentEmailTemplateHtml = clientContentEmailTemplateHtml.Replace("@NAME", item.Name);
                    clientContentEmailTemplateHtml.Replace("@EMAILTEXT", $@"<p style='margin-bottom:10px;'>This is to inform you that you have been assigned with new Adviser, Please  contact Adviser {newAdviserName} for futher assistance.</p>
                                                                        <p style='margin-bottom:10px;'>To learn more about your adviser, Click <a href ='{adviserLink}'>Here</a>!</p>");
                    clientmainEmailTemplateHtml.Replace("@CONTENT", clientContentEmailTemplateHtml.ToString());

                    await emailHelper.SendEmailAsync(item.ClientLogin.Username, string.Join(",", admins), _config["Email:BccEmail"], EmailSubject.NewAdviserAssign, clientmainEmailTemplateHtml.ToString(), _env.WebRootPath);
                    rowsHtml += $@"<tr style='font-style: italic;'>
                                        <td style='margin:0;padding-right:10px'>{item.ClientId}</td>
                                        <td style='padding: 0 10px;margin:0'>{item.Name}</td>
                                        <td style='padding: 0 10px;margin:0'>{item.ClientLogin.Username}</td>
                                   </tr>";
                }

                var clientTableHtml = $@"<table style='border: none; border-collapse: collapse;font-family: sans-serif;font-size: 14px;margin-left:20px;' role='presentation' cellpadding='0' cellspacing='0'>
                                            <tr>
                                                <td style='color: #3699FF;padding-right:10px;margin:0'>Id</td>
                                                <td style='padding: 0 10px;color: #3699FF;margin:0'>Name</td>
                                                <td style='padding: 0 10px;color: #3699FF;margin:0'>Email</td>
                                            </tr>
                                            {rowsHtml}                            
                                        </table>";
                StringBuilder oldAdvisermainEmailTemplateHtml = new(mainEmailTemplateHtml);
                StringBuilder oldAdviserContentEmailTemplateHtml = new(contentEmailTemplateHtml);
                oldAdviserContentEmailTemplateHtml.Replace("@NAME", oldAdviser.UserData.Name);
                oldAdviserContentEmailTemplateHtml.Replace("@EMAILTEXT", string.Format(EmailNotification.MIGRATEDCLIENT, "moved", clientTableHtml));
                oldAdvisermainEmailTemplateHtml.Replace("@CONTENT", oldAdviserContentEmailTemplateHtml.ToString());
                await emailHelper.SendEmailAsync(oldAdviser.Username, string.Join(",", admins), _config["Email:BccEmail"], EmailSubject.ClientRevoke, oldAdvisermainEmailTemplateHtml.ToString(), _env.WebRootPath);

                StringBuilder newAdvisermainEmailTemplateHtml = new(mainEmailTemplateHtml);
                StringBuilder newAdviserContentEmailTemplateHtml = new(contentEmailTemplateHtml);
                newAdviserContentEmailTemplateHtml.Replace("@NAME", newAdviser.UserData.Name);
                newAdviserContentEmailTemplateHtml.Replace("@EMAILTEXT", string.Format(EmailNotification.MIGRATEDCLIENT, "added", clientTableHtml));
                newAdvisermainEmailTemplateHtml.Replace("@CONTENT", newAdviserContentEmailTemplateHtml.ToString());
                await emailHelper.SendEmailAsync(newAdviser.Username, string.Join(",", admins), _config["Email:BccEmail"], EmailSubject.NewClientAdded, newAdvisermainEmailTemplateHtml.ToString(), _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = true,
                Message = $"{clientIds.Split(",").Length} client(s) moved to {newAdviserName} successfully"
            };
            return Json(result);
        }

        public async Task<IActionResult> AddReleaseNote(Guid guid)
        {
            var releaseNote = await _context2.ReleaseNotes.FirstOrDefaultAsync(m => m.Guid == guid);
            return View("_AddReleaseNoteModal", releaseNote);
        }
        [HttpPost]
        public async Task<IActionResult> SaveReleaseNote(ReleaseNote releaseNote)
        {
            var releaseNoteObj = await _context2.ReleaseNotes.FirstOrDefaultAsync(m => m.Guid == releaseNote.Guid);
            var releaseNoteMapper = _mapper.Map(releaseNote, releaseNoteObj);
            _context2.ReleaseNotes.Update(releaseNoteMapper);
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = releaseNote is not null,
                Message = releaseNote is not null ? Message.SAVE_SUCCESS : Message.NO_RECORD_FOUND,
                Data = new { releaseNote = releaseNoteMapper }
            };
            return Json(result);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteReleaseNote(Guid Guid)
        {
            var releaseNote = await _context2.ReleaseNotes.Where(m => m.Guid == Guid).FirstOrDefaultAsync();

            if (releaseNote is not null)
            {
                _context2.ReleaseNotes.Remove(releaseNote);
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = releaseNote is not null,
                Message = releaseNote is not null ? Message.RECORD_DELETE : Message.NO_RECORD_FOUND,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~SuperAdminController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}