﻿using AutoMapper;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Models;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using System.Net.Mail;
using System.IO;
using System.Web;
using Google.Authenticator;
using LAP.Services;
using LAP.Model;

namespace LAP.Areas.Admin.Controllers
{
    [Area(Roles.Admin)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class FirmAdminController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        private readonly int FirmId;
        public FirmAdminController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            FirmId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "FirmId").ClaimToInt();
        }
        public IActionResult FirmAdmins()
        {
            return View();
        }
        public async Task<IActionResult> FetchFirmAdmins(DatatableMetaModel meta, CommonSearchModel query)
        {
            var firmAdmins = _context2.UserLogins.Where(m => m.FirmId == FirmId && (m.AdminRole == AdminRoleEnum.FirmAdmin || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.FirmAdmin)));

            int rowsTotal = firmAdmins.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            if (query.RolesList != null && query.RolesList.Count > 0)
                firmAdmins = firmAdmins.Where(m => query.RolesList.Contains((int)m.AdminRole) || m.Permissions.Any(n => query.RolesList.Contains((int)n.RoleType)));

            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "UserData,Permissions";
            var result = await firmAdmins.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> AddFirmAdmin(Guid guid)
        {
            var firmAdmin = await _context2.UserLogins.Where(m => m.Guid == guid)
                .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.Permissions).FirstOrDefaultAsync();
            var role = firmAdmin is not null ? firmAdmin.AdminRole : AdminRoleEnum.FirmAdmin;
            ViewBag.PermissionTypes = await _dropdownDataHelper.GetPermissionTypesAsync((int)role);
            if (guid != Guid.Empty)
            {
                var twoFactorAuthenticator = new TwoFactorAuthenticator();
                var accountSecretKey = $"{_config["SecretKey"]}-{firmAdmin.Username}";
                var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], firmAdmin.Username, Encoding.ASCII.GetBytes(accountSecretKey));
                ViewBag.SetupKey = setupCode.ManualEntryKey;
            }
            return View("_AddFirmAdminModal", firmAdmin);
        }
        public async Task InviteFirmAdmin(UserLogin adminLogin)
        {
            string token = await adminLogin.Username.EncryptAsync();
            var createPasswordLink = Url.RouteUrl("user/create-password", new { token }, Request.Scheme);
            StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
            mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
            StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/create-password-user.html")));
            emailTemplateHtml.Replace("@EMAILTO", adminLogin.Username);
            emailTemplateHtml.Replace("@CREATEPASSWORDLINK", createPasswordLink);
            emailTemplateHtml.Replace("@NAME", adminLogin.UserData.FirstName);
            emailTemplateHtml.Replace("@ROLE", adminLogin.AdminRole.GetDisplayName());
            emailTemplateHtml.Replace("@AORAN", adminLogin.AdminRole == AdminRoleEnum.Adviser ? "an" : "a");
            var permissionsString = "";
            if (adminLogin.Permissions.Any())
            {
                permissionsString = "<ul>";
                foreach (var item in adminLogin.Permissions)
                    permissionsString += $"<li>{item.RoleTypeString}</li>";
                permissionsString += "</ul>";
            }
            emailTemplateHtml.Replace("@PERMISSIONS", permissionsString);
            mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
            EmailHelper emailHelper = new(_config);
            string emailHtmlBody = mainEmailTemplateHtml.ToString();
            await emailHelper.SendEmailAsync(adminLogin.Username, null, _config["Email:BccEmail"], EmailSubject.CreatePassword, emailHtmlBody, _env.WebRootPath);
            await Task.CompletedTask;
        }
        public async Task<IActionResult> ReInvite(Guid UserLoginGuid)
        {
            var admin = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.UserData)
                .Include(m => m.Permissions).FirstOrDefaultAsync();
            await InviteFirmAdmin(admin);
            var result = new CommonResponse
            {
                Success = admin is not null,
                Message = admin is not null ? Message.RESEND_INVITATION : string.Format(Message.NO_RECORD_FOUND),
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveFirmAdmin(UserLogin adminLogin)
        {
            var adminObj = await _context2.UserLogins.Where(m => m.Guid == adminLogin.Guid)
                .Include(m => m.UserData).Include(m => m.Permissions).FirstOrDefaultAsync();

            var complianceRole = AdminRoleEnum.Compliance;
            bool hasCompliancePermission = adminObj is not null && adminObj.Permissions.Any(m => m.RoleType == complianceRole);
            bool hasCompliancePermissionUpdated = adminLogin.Permission is not null && adminLogin.Permission.Contains((int)complianceRole);
            var permissionType = !hasCompliancePermission && hasCompliancePermissionUpdated ? PermissionTypeEnum.Grant : hasCompliancePermission && !hasCompliancePermissionUpdated ? PermissionTypeEnum.Revoke : 0;
            if (adminLogin.Permission is not null)
            {
                var permissions = new List<Permission>();
                foreach (var item in adminLogin.Permission)
                {
                    permissions.Add(new Permission
                    {
                        RoleType = EnumHelper.ToEnum<AdminRoleEnum>(item.ToString())
                    });
                }
                adminLogin.Permissions = permissions;
            }
            if (adminLogin.Guid == Guid.Empty)
            {
                var adviserLoginObj = new UserLogin
                {
                    Username = adminLogin.Username,
                    FirmId = FirmId,
                    AdminRole = AdminRoleEnum.FirmAdmin,
                    UserData = new UserData
                    {
                        FirstName = adminLogin.UserData.FirstName,
                        MiddleInitial = adminLogin.UserData.MiddleInitial,
                        LastName = adminLogin.UserData.LastName,
                    },
                    Permissions = adminLogin.Permissions,
                    Active = true
                };
                _context2.UserLogins.Add(adviserLoginObj);
                adminLogin.AdminRole = adviserLoginObj.AdminRole;
            }
            else
            {
                var adviserDataObj = adminObj.UserData;
                adviserDataObj.FirstName = adminLogin.UserData.FirstName;
                adviserDataObj.MiddleInitial = adminLogin.UserData.MiddleInitial;
                adviserDataObj.LastName = adminLogin.UserData.LastName;
                adviserDataObj.UserLogin.Permissions = adminLogin.Permissions;
                _context2.UserDatas.Update(adviserDataObj);
            }

            await _context2.SaveChangesAsync();

            if (adminLogin.Guid == Guid.Empty)
                await InviteFirmAdmin(adminLogin);

            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && permissionType != 0)
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/compliance-permission.html")));
                emailTemplateHtml.Replace("@NAME", adminLogin.UserData.FirstName);
                var emailText = permissionType == PermissionTypeEnum.Grant ? "This email is to notify you that you have been given <span style='color:#0073e9'>Compliance</span> Permission for" : "This email is to notify you that  <span style='color:#0073e9'>Compliance</span> Permission have been removed from";
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                var firm = _context2.Firms.FirstOrDefault(m => m.Id == FirmId);
                emailTemplateHtml.Replace("@FIRMNAME", firm.Name);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                List<LinkedResource> resources = new()
                {
                    new LinkedResource(Path.Combine(_env.WebRootPath, "images/brand_logo.png"))
                    {
                        ContentId = "LogoImage"
                    }
                };
                await emailHelper.SendEmailAsync(adminLogin.Username, null, _config["Email:BccEmail"], EmailSubject.ComplianceNotification, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = adminLogin is not null,
                Message = adminObj is not null ? Message.SAVE_SUCCESS : string.Format(Message.CLIENTADVISER_ADDED, Roles.FirmAdmin),
                ParentId = FirmId,
                ActionType = adminObj is not null ? "update" : "add"
            };
            return Json(result);
        }
        public async Task<IActionResult> UserMessageBoard()
        {
            var firm = await _context2.Firms.Where(m => m.Id == FirmId).FirstOrDefaultAsync();
            firm.UserBoardMessage = !string.IsNullOrEmpty(firm.UserBoardMessage) ? HttpUtility.UrlDecode(firm.UserBoardMessage).Replace("<a", "<a target='_blank'") : string.Empty;
            return View(firm);
        }
        public async Task<IActionResult> SaveUserMessageBoard(string UserBoardMessage)
        {
            var firm = await _context2.Firms.Where(m => m.Id == FirmId).FirstOrDefaultAsync();
            firm.UserBoardMessage = !string.IsNullOrEmpty(UserBoardMessage) ? HttpUtility.UrlDecode(UserBoardMessage).Replace("<a", "<a target='_blank'") : string.Empty;
            await _context2.SaveChangesAsync();
            var result = new CommonResponse
            {
                Success = firm != null,
                Message = firm != null ? Message.SAVE_SUCCESS : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~FirmAdminController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}