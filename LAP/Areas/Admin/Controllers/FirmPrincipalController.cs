﻿using AutoMapper;
using Google.Authenticator;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Models;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAP.Areas.Admin.Controllers
{
    [Area(Roles.Admin)]
    [Authorize(Roles = Roles.SuperAdmin, AuthenticationSchemes = AuthenticationSchemes.SuperAdminAuth)]
    public class FirmPrincipalController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        private readonly int FirmId;
        public FirmPrincipalController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            FirmId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "FirmId").ClaimToInt();
        }
        public IActionResult FirmPrincipals()
        {
            var firmPrincipals = _context2.UserLogins.Where(m => m.AdminRole == AdminRoleEnum.FirmPrincipal).Include(m => m.Firm).AsQueryable();
            if (FirmId > 0)
                firmPrincipals = firmPrincipals.Where(m => m.FirmId == FirmId);
            return View(firmPrincipals);
        }
        public async Task<IActionResult> AddFirmPrincipal(Guid guid)
        {
            ViewBag.PermissionTypes = await _dropdownDataHelper.GetPermissionTypesAsync((int)AdminRoleEnum.FirmPrincipal);
            var firmPrincipal = await _context2.UserLogins.Where(m => m.Guid == guid)
                .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.Permissions).FirstOrDefaultAsync();
            if (guid != Guid.Empty)
            {
                var twoFactorAuthenticator = new TwoFactorAuthenticator();
                var accountSecretKey = $"{_config["SecretKey"]}-{firmPrincipal.Username}";
                var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], firmPrincipal.Username, Encoding.ASCII.GetBytes(accountSecretKey));
                ViewBag.SetupKey = setupCode.ManualEntryKey;
            }
            return View("_AddFirmPrincipalModal", firmPrincipal);
        }
        public async Task InviteFirmPrincipal(UserLogin adminLogin)
        {
            string token = await adminLogin.Username.EncryptAsync();
            var createPasswordLink = Url.RouteUrl("user/create-password", new { token }, Request.Scheme);
            StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
            mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
            StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/create-password-user.html")));
            emailTemplateHtml.Replace("@EMAILTO", adminLogin.Username);
            emailTemplateHtml.Replace("@CREATEPASSWORDLINK", createPasswordLink);
            emailTemplateHtml.Replace("@NAME", adminLogin.UserData.FirstName);
            emailTemplateHtml.Replace("@ROLE", adminLogin.AdminRoleString);
            emailTemplateHtml.Replace("@AORAN", adminLogin.AdminRole == AdminRoleEnum.Adviser ? "an" : "a");
            var permissionsString = "";
            if (adminLogin.Permissions.Any())
            {
                permissionsString = "<ul>";
                foreach (var item in adminLogin.Permissions)
                    permissionsString += $"<li>{item.RoleTypeString}</li>";
                permissionsString += "</ul>";                
            }
            emailTemplateHtml.Replace("@PERMISSIONS", permissionsString);
            mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
            EmailHelper emailHelper = new(_config);
            string emailHtmlBody = mainEmailTemplateHtml.ToString();
            await emailHelper.SendEmailAsync(adminLogin.Username, null, _config["Email:BccEmail"], EmailSubject.CreatePassword, emailHtmlBody, _env.WebRootPath);
            await Task.CompletedTask;
        }
        public async Task<IActionResult> ReInvite(Guid UserLoginGuid)
        {
            var admin = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.UserData)
                .Include(m => m.Permissions).FirstOrDefaultAsync();
            await InviteFirmPrincipal(admin);
            var result = new CommonResponse
            {
                Success = admin is not null,
                Message = admin is not null ? Message.RESEND_INVITATION : string.Format(Message.NO_RECORD_FOUND),
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveFirmPrincipal(UserLogin adminLogin)
        {
            var adminObj = await _context2.UserLogins.Where(m => m.Guid == adminLogin.Guid)
                .Include(m => m.UserData).Include(m => m.Permissions).Include(m => m.Firm).FirstOrDefaultAsync();
            var complianceRole = AdminRoleEnum.Compliance;
            bool hasCompliancePermission = adminObj is not null && adminObj.Permissions.Any(m => m.RoleType == complianceRole);
            bool hasCompliancePermissionUpdated = adminLogin.Permission is not null && adminLogin.Permission.Contains((int)complianceRole);
            var permissionType = !hasCompliancePermission && hasCompliancePermissionUpdated ? PermissionTypeEnum.Grant : hasCompliancePermission && !hasCompliancePermissionUpdated ? PermissionTypeEnum.Revoke : 0;
            if (adminLogin.Permission is not null)
            {
                var permissions = new List<Permission>();
                foreach (var item in adminLogin.Permission)
                {
                    permissions.Add(new Permission
                    {
                        RoleType = EnumHelper.ToEnum<AdminRoleEnum>(item.ToString())
                    });
                }
                adminLogin.Permissions = permissions;
            }
            if (adminLogin.Guid == Guid.Empty)
            {
                var adviserLoginObj = new UserLogin
                {
                    Username = adminLogin.Username,
                    AdminRole = AdminRoleEnum.FirmPrincipal,
                    UserData = new UserData
                    {
                        FirstName = adminLogin.UserData.FirstName,
                        MiddleInitial = adminLogin.UserData.MiddleInitial,
                        LastName = adminLogin.UserData.LastName,
                    },
                    Permissions = adminLogin.Permissions,
                    Active = true
                };
                if (adminLogin.FirmId > 0)
                    adviserLoginObj.FirmId = adminLogin.FirmId;
                else
                    adviserLoginObj.Firm = adminLogin.Firm;
                _context2.UserLogins.Add(adviserLoginObj);
                adminLogin.AdminRole = adviserLoginObj.AdminRole;
            }
            else
            {
                var adviserDataObj = adminObj.UserData;
                adviserDataObj.FirstName = adminLogin.UserData.FirstName;
                adviserDataObj.MiddleInitial = adminLogin.UserData.MiddleInitial;
                adviserDataObj.LastName = adminLogin.UserData.LastName;
                adviserDataObj.UserLogin.Permissions = adminLogin.Permissions;
                _context2.UserDatas.Update(adviserDataObj);
            }
            await _context2.SaveChangesAsync();
            if (adminLogin.Guid == Guid.Empty)
                await InviteFirmPrincipal(adminLogin);
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && permissionType != 0)
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/compliance-permission.html")));
                emailTemplateHtml.Replace("@NAME", adminLogin.UserData.FirstName);
                var emailText = permissionType == PermissionTypeEnum.Grant ? "This email is to notify you that you have been given <span style='color:#0073e9'>Compliance</span> Permission for" : "This email is to notify you that <span style='color:#0073e9'>Compliance</span> Permission have been removed from";
                emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                emailTemplateHtml.Replace("@FIRMNAME", adminLogin.Firm.Name);
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(adminLogin.Username, null, _config["Email:BccEmail"], EmailSubject.ComplianceNotification, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = adminLogin is not null,
                Message = adminObj is not null ? Message.SAVE_SUCCESS : string.Format(Message.CLIENTADVISER_ADDED, Roles.FirmPrincipal),
                ParentId = adminLogin.FirmId,
                ActionType = adminObj is not null ? "update" : "add"
            };
            return Json(result);
        }
        public async Task<Select2Model> GetFirms(string term, int page)
        {
            return await _dropdownDataHelper.GetFirmsAsync(term, page);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~FirmPrincipalController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}