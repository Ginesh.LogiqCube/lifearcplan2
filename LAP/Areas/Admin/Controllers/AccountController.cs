﻿using AutoMapper;
using Google.Authenticator;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using LAP.DomainModels.Models;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
namespace LAP.Areas.Admin.Controllers
{
    [Area(Roles.Admin)]
    public class AccountController : BaseController, IDisposable
    {
        private readonly ArcHiveService _arcHiveController;
        private readonly TwoFactorAuthenticator twoFactorAuthenticator;
        public AccountController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper, ArcHiveService arcHiveController) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _arcHiveController = arcHiveController;
            twoFactorAuthenticator = new TwoFactorAuthenticator();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel login, string returnUrl)
        {
            bool isFromSuperAdmin = _httpContextAccessor.HttpContext.Request.Path.Value.Contains("super");
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            login.Password = await login.Password.EncryptAsync();

            var admin = await _context2.UserLogins.Where(m => m.Username == login.Username && m.Password == login.Password)
                .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.AdviserContactInfos)
                .Include(m => m.Permissions).Include(m => m.AssistantPermissions).FirstOrDefaultAsync();
            if ((!isFromSuperAdmin && admin?.AdminRole == AdminRoleEnum.SuperAdmin) || (isFromSuperAdmin && admin?.AdminRole != AdminRoleEnum.SuperAdmin)) admin = null;
            string url = string.Empty;
            bool isActive = false;
            if (admin is not null)
            {
                isActive = Convert.ToBoolean(admin?.Active);
                if (isActive)
                {
                    if (!admin.TwoFactorAuth || _httpContextAccessor.HttpContext.IsLocalOrSupport())
                    {
                        if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                        {
                            admin.SetupCompletedDate = Convert.ToDateTime(admin.SetupCompletedDate).IsNotEmptyDate() ? admin.SetupCompletedDate : admin.LastLogin;
                            admin.LastLogin = DateTime.UtcNow;
                            bool isValidAdv2B = admin.UserData.DateOfBirth is not null && admin.AdviserContactInfos is not null && admin.UserData.RIAId > 0;
                            await _context2.SaveChangesAsync();
                        }
                        var adminRole = EnumHelper.ToEnum<AdminRoleEnum>(admin.AdminRole.ToString());
                        var claims = new List<Claim>()
                            {
                                new Claim(ClaimTypes.Email, admin.Username),
                                new Claim(ClaimTypes.Role,adminRole.GetDisplayName()),
                                new Claim("UserId", admin.Id.ToString()),
                                new Claim("UserLoginGuid", admin.Guid.ToString()),
                                new Claim("TimeZone", login.TimeZone.ToString()),
                        };
                        if (admin.IsAssistant)
                            claims.Add(new Claim("OwnerUserId", admin.OwnerUserId.ToString()));
                        if (admin.FirmId > 0)
                        {
                            claims.Add(new Claim("FirmId", admin.FirmId.ToString()));
                            claims.Add(new Claim("FirmGuid", admin?.Firm?.Guid.ToString()));
                        }
                        bool isSuperAdmin = admin.AdminRole == AdminRoleEnum.SuperAdmin;
                        var authenticationScheme = isSuperAdmin ? AuthenticationSchemes.SuperAdminAuth : AuthenticationSchemes.UserAuth;
                        var claimsIdentity = new ClaimsIdentity(claims, authenticationScheme);
                        var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                        var prop = new AuthenticationProperties();

                        if (login.RememberMe)
                        {
                            prop.IsPersistent = login.RememberMe;
                            prop.ExpiresUtc = DateTime.UtcNow.AddMonths(1);
                            prop.RedirectUri = isSuperAdmin ? "super-admin/login" : "user/login";
                        }
                        await _httpContextAccessor.HttpContext.SignInAsync(authenticationScheme, claimsPrincipal, prop);
                        string area = admin.AdminRole == AdminRoleEnum.FirmPrincipal ? "firm-principal" : admin.AdminRole == AdminRoleEnum.FirmAdmin ? "firm-admin" : "adviser";
                        string redirectUrl = isSuperAdmin ? "super-admin/firm-principal-list" : "welcome-user";
                        url = !string.IsNullOrEmpty(returnUrl) ? returnUrl : redirectUrl;
                    }
                }
            }
            var result = new CommonResponse
            {
                Success = admin is not null && isActive,
                Message = admin is not null && isActive ? Message.LOGIN_SUCCESS : admin is not null && !isActive ? Message.DEACTIVE_USER_ACCOUNT : Message.INVALID_USERNAME_PASSWORD,
                RedirectUrl = admin is not null && (!admin.TwoFactorAuth || _httpContextAccessor.HttpContext.IsLocalOrSupport()) ? url : null,
                Data = admin is not null ? new { login, firstname = admin.UserData.FirstName } : null
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> CodeVerification(LoginModel model, string returnUrl, int EmailSent)
        {
            var admin = await _context2.UserLogins.Where(m => m.Username == model.Username)
                .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.Permissions).FirstOrDefaultAsync();
            string url = string.Empty;
            bool isAuthenticated = false;
            if (admin is not null)
            {
                var accountSecretKey = $"{_config["SecretKey"]}-{admin.Username}";
                var CodeValidity = EmailSent == 1 ? TimeSpan.FromMinutes(5) : TimeSpan.FromSeconds(30);
                isAuthenticated = twoFactorAuthenticator.ValidateTwoFactorPIN(accountSecretKey, model.VerificationCode, CodeValidity);
                if (isAuthenticated)
                {
                    if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                    {
                        admin.SetupCompletedDate = Convert.ToDateTime(admin.SetupCompletedDate).IsNotEmptyDate() ? admin.SetupCompletedDate : admin.LastLogin;
                        admin.LastLogin = DateTime.UtcNow;
                        await _context2.SaveChangesAsync();
                    }
                    var adminRole = EnumHelper.ToEnum<AdminRoleEnum>(admin.AdminRole.ToString());
                    var claims = new List<Claim>()
                            {
                                new Claim(ClaimTypes.Email, admin.Username),
                                new Claim(ClaimTypes.Role,adminRole.GetDisplayName()),
                                new Claim("UserId", admin.Id.ToString()),
                                new Claim("UserLoginGuid", admin.Guid.ToString()),
                                new Claim("TimeZone", model.TimeZone.ToString())
                            };
                    if (admin.IsAssistant)
                        claims.Add(new Claim("OwnerUserId", admin.OwnerUserId.ToString()));
                    if (admin.FirmId > 0)
                    {
                        claims.Add(new Claim("FirmId", admin.FirmId.ToString()));
                        claims.Add(new Claim("FirmGuid", admin?.Firm?.Guid.ToString()));
                    }
                    bool isSuperAdmin = admin.AdminRole == AdminRoleEnum.SuperAdmin;
                    var authenticationScheme = isSuperAdmin ? AuthenticationSchemes.SuperAdminAuth : AuthenticationSchemes.UserAuth;
                    var claimsIdentity = new ClaimsIdentity(claims, authenticationScheme);
                    var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                    var prop = new AuthenticationProperties();

                    if (model.RememberMe)
                    {
                        prop.IsPersistent = model.RememberMe;
                        prop.ExpiresUtc = DateTime.UtcNow.AddMonths(1);
                        prop.RedirectUri = isSuperAdmin ? "super-admin/login" : "user/login";
                    }
                    await _httpContextAccessor.HttpContext.SignInAsync(authenticationScheme, claimsPrincipal, prop);
                    string redirectUrl = isSuperAdmin ? "super-admin/firm-principal-list" : "welcome-user";
                    url = !string.IsNullOrEmpty(returnUrl) ? returnUrl : Url.RouteUrl(redirectUrl);
                }
            }
            var result = new CommonResponse
            {
                Success = admin is not null && isAuthenticated,
                Message = admin is not null && isAuthenticated ? Message.LOGIN_SUCCESS : Message.INVALID_VERIFICATION_CODE,
                RedirectUrl = admin is not null && isAuthenticated ? url : null,
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> Send2FACode(string email)
        {
            var user = await _context2.UserLogins.Where(m => m.Username == email && m.IsDeleted != true)
                .Include(m => m.UserData).FirstOrDefaultAsync();
            var accountSecretKey = $"{_config["SecretKey"]}-{user.Username}";
            var code = twoFactorAuthenticator.GetCurrentPIN(accountSecretKey);
            StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
            mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
            StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
            emailTemplateHtml.Replace("@NAME", user.UserData.FirstName);
            var emailText = @$"Please find the verification code mentioned below as requested by you.<br>
                                   Verification Code: <b style='color:#800000'>{code}</b><br><br>
                                   <b>Note:</b> Verification code will be valid for 5 minutes only";
            emailTemplateHtml.Replace("@EMAILTEXT", emailText);
            mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
            EmailHelper emailHelper = new(_config);
            string emailHtmlBody = mainEmailTemplateHtml.ToString();
            await emailHelper.SendEmailAsync(user.Username, null, _config["Email:BccEmail"], EmailSubject.TwoFactorAuthCodeRequest, emailHtmlBody, _env.WebRootPath);

            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(code),
                Message = !string.IsNullOrEmpty(code) ? string.Format(Message.VERIFICATION_CODE_SENT, user.Username) : Message.INVALID_VERIFICATION_CODE,
            };
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> SendResetPasswordLink(ForgotPasswordModel model)
        {
            var adviser = await _context2.UserLogins.Where(m => m.Username == model.Email).Include(m => m.UserData).FirstOrDefaultAsync();
            string resetPasswordLink = string.Empty;
            if (adviser is not null)
            {
                string name = $"{adviser.UserData.FirstName} {adviser.UserData.LastName}";
                string token = await ($"{adviser.Username}#{DateTime.UtcNow.AddHours(1)}").EncryptAsync();
                resetPasswordLink = Url.RouteUrl("user/reset-password", new { token }, Request.Scheme);
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());

                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/reset-password.html")));
                emailTemplateHtml.Replace("@RESETPASSWORDLINK", resetPasswordLink);
                emailTemplateHtml.Replace("@NAME", name);

                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                await emailHelper.SendEmailAsync(adviser.Username, null, _config["Email:BccEmail"], EmailSubject.ResetPasswordRequest, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = adviser is not null,
                Message = adviser is not null ? Message.RESET_PASSWORDLINK_SENT : string.Format(ValidationMessage.INVALID, "Email address"),
            };
            return Json(result);
        }
        public async Task<IActionResult> ResetPassword(string token)
        {
            ResetPasswordModel model = new();
            token = await token.DecryptAsync();
            if (token == "InvalidToken")
            {
                ViewBag.StatusCode = 403;
                ViewBag.Title = "Access Denied";
                ViewBag.Message = "Sorry, You don't have permission to access this page.";
                return View("/Views/Error/ErrorPage.cshtml", new { statusCode = 403 });
            }
            var sp = token.Split("#");
            model.Email = sp[0];
            model.Token = token;
            if (sp.Length > 1 && Convert.ToDateTime(sp[1]) < DateTime.UtcNow)
                ViewData["msg"] = Message.LINK_EXPIRED;
            return View("ResetPassword", model);
        }
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            string token = await model.Token.DecryptAsync();
            var sp = token.Split("#");
            model.Email = sp[0];
            var admin = await _context2.UserLogins.Where(m => m.Username == model.Email)
                .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.Permissions).FirstOrDefaultAsync();
            var msg = string.Empty;
            string redirectUrl = string.Empty;
            if (admin is not null)
            {
                if (Convert.ToBoolean(admin.Active))
                {
                    admin.Password = await model.Password.EncryptAsync();
                    admin.SetupCompletedDate = DateTime.UtcNow;
                    admin.LastLogin = DateTime.UtcNow;
                    await _context2.SaveChangesAsync();
                    var adminRole = EnumHelper.ToEnum<AdminRoleEnum>(admin.AdminRole.ToString());
                    var claims = new List<Claim>()
                            {
                                new Claim(ClaimTypes.Email, admin.Username),
                                new Claim(ClaimTypes.Role,adminRole.GetDisplayName()),
                                new Claim("UserId", admin.Id.ToString()),
                                new Claim("UserLoginGuid", admin.Guid.ToString()),
                                new Claim("TimeZone", model.TimeZone.ToString()),
                            };
                    if (admin.IsAssistant)
                        claims.Add(new Claim("OwnerUserId", admin.OwnerUserId.ToString()));
                    if (admin.FirmId > 0)
                    {
                        claims.Add(new Claim("FirmId", admin.FirmId.ToString()));
                        claims.Add(new Claim("FirmGuid", admin.Firm.Guid.ToString()));
                    }
                    bool isSuperAdmin = admin.AdminRole == AdminRoleEnum.SuperAdmin;
                    var authenticationScheme = admin.AdminRole == AdminRoleEnum.SuperAdmin ? AuthenticationSchemes.SuperAdminAuth : AuthenticationSchemes.UserAuth;
                    var claimsIdentity = new ClaimsIdentity(claims, authenticationScheme);
                    var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                    var prop = new AuthenticationProperties
                    {
                        RedirectUri = isSuperAdmin ? "super-admin/login" : "user/login"
                    };
                    await _httpContextAccessor.HttpContext.SignInAsync(authenticationScheme, claimsPrincipal, prop);

                    if (!isSuperAdmin)
                    {
                        string role = admin.AdminRole == AdminRoleEnum.FirmPrincipal ? "firm-principal" : admin.AdminRole == AdminRoleEnum.FirmAdmin ? "firm-admin" : "adviser";
                        redirectUrl = !admin.TwoFactorAuth ? $"user/security-setting?back=true" : "welcome-user";
                    }
                    else
                        redirectUrl = "super-admin/firm-principal-list";
                    msg = sp.Length > 1 ? Message.RESET_PASSWORD_SUCCESSFUL : Message.CREATE_PASSWORD_SUCCESSFUL;
                    if (!_httpContextAccessor.HttpContext.IsLocalOrSupport())
                    {
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", admin.UserData.FirstName);
                        var action = sp.Length > 1 ? "reset" : "created";
                        var subject = sp.Length > 1 ? EmailSubject.ResetPasswordAcknowledgement : EmailSubject.CreatePassword;
                        var emailText = $@"<p>Your password {action} request has been processed.</p>
                                           <p>In the event you did not request to have your password reset, please notify LifeArcPlan's help desk staff immediately at <a href='mailto:info@lifearcplan.com' style='color:#0073e9'>info@lifearcplan.com</a>.</p>";
                        emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        await emailHelper.SendEmailAsync(admin.Username, null, _config["Email:BccEmail"], subject, emailHtmlBody, _env.WebRootPath);
                    }
                }
                else
                    msg = Message.DEACTIVE_USER_ACCOUNT;
            }
            bool isActive = Convert.ToBoolean(admin?.Active);
            var result = new CommonResponse
            {
                Success = admin is not null && isActive,
                Message = admin is not null && isActive ? msg : admin is not null && !isActive ? msg : string.Format(ValidationMessage.INVALID, "Email address"),
                RedirectUrl = redirectUrl
            };
            return Json(result);
        }
        [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
        public async Task<IActionResult> ClientLogin(Guid guid)
        {
            var client = await _context2.ClientLogins.Where(m => m.Guid == guid && m.IsDeleted != true)
                .Include(m => m.ClientData).Include(m => m.UserLogin).FirstOrDefaultAsync();
            if (client is not null)
            {
                Guid UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
                var user = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
                var additionalRoles = user.Permissions is not null ? string.Join(",", user.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;

                bool hasAdviserRole = !string.IsNullOrEmpty(additionalRoles) && additionalRoles.Contains(Roles.Adviser);
                bool IsAdviser = user.AdminRole == AdminRoleEnum.Adviser || hasAdviserRole;
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Email, client.Username),
                    new Claim(ClaimTypes.Role,Roles.Client),
                    new Claim("ClientId", client.Id.ToString()),
                    new Claim("ClientLoginGuid", client.Guid.ToString()),
                    new Claim("DisabledRightPanel", "true"),
                    new Claim("CreatedByUserId", user.Id.ToString()),
                };
                if (!IsAdviser)
                    claims.Add(new Claim("DisplayOnlyClient", "true"));
                var claimsIdentity = new ClaimsIdentity(claims, AuthenticationSchemes.ClientAuth);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                await _httpContextAccessor.HttpContext.SignInAsync(claimsPrincipal);
            }
            return RedirectToRoute("client-info");
        }
        [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
        public async Task<IActionResult> AdviserLogin(Guid guid)
        {
            var adviser = await _context2.UserLogins.Where(m => m.Guid == guid && m.IsDeleted != true).Include(m => m.UserData).FirstOrDefaultAsync();
            if (adviser is not null)
            {
                var claims = new List<Claim>()
                {
                    new Claim($"{ClaimTypes.Email}Adviser", adviser.Username),
                    new Claim($"{ClaimTypes.Role}Adviser",Roles.Adviser),
                    new Claim("AdviserUserId", adviser.Id.ToString()),
                    new Claim("AdviserUserLoginGuid", adviser.Guid.ToString())
                };
                var claimsIdentity = new ClaimsIdentity(claims, AuthenticationSchemes.AdviserAuth);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                await _httpContextAccessor.HttpContext.SignInAsync(AuthenticationSchemes.AdviserAuth, claimsPrincipal);
            }
            return RedirectToRoute("adviser/info", new { flag = "true" });
        }
        public async Task<IActionResult> SuperAdminLogOut()
        {
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            return RedirectToRoute("super-admin/login");
        }
        public async Task<IActionResult> UserLogOut()
        {
            await _httpContextAccessor.HttpContext.LogoutEveryone();
            return RedirectToRoute("user/login");
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                }
                disposed = true;
            }
        }
        ~AccountController()
        {
            Dispose(false);
        }

        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}