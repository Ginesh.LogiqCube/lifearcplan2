﻿using AutoMapper;
using LAP.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LAP.Model;
using LAP.DomainModels.Entities.Client;
using LAP.DomainModels.Models;
using LAP.DomainModels.Entities.Admin;
using LAP.DomainModels.Entities.Adviser;
using LAP.DomainModels;
using LAP.DomainModels.Entities;
using System.IO;
using System.Collections.Generic;
using Google.Authenticator;
using LAP.Services;

namespace LAP.Areas.Admin.Controllers
{
    [Area(Roles.Admin)]
    [Authorize(Roles = Roles.SuperAdminFirmUser, AuthenticationSchemes = AuthenticationSchemes.SuperAdminFirmUserAuth)]
    public class AdviserController : BaseController, IDisposable
    {
        protected readonly DropdownDataHelper _dropdownDataHelper;
        private readonly ArcHiveService _arcHiveController;
        private readonly Guid UserLoginGuid;
        private readonly int FirmId;
        private readonly int UserId;
        public AdviserController(LifeArcPlanContext2 context2, IWebHostEnvironment env, IConfiguration config, IHttpContextAccessor httpContextAccessor, IMapper mapper, ArcHiveService arcHiveController) : base(context2, env, config, httpContextAccessor, mapper)
        {
            _dropdownDataHelper = new DropdownDataHelper(_context2, _httpContextAccessor);
            _arcHiveController = arcHiveController;
            UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            FirmId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "FirmId").ClaimToInt();
            UserId = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserId").ClaimToInt();
        }
        public async Task<IActionResult> Advisers()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions is not null ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            return View();
        }
        public async Task<IActionResult> FetchAdvisers(DatatableMetaModel meta, CommonSearchModel query, bool isAssistant = false)
        {
            var advisers = _context2.UserLogins.Where(m => m.FirmId == FirmId && m.IsAssistant == isAssistant && (m.AdminRole == AdminRoleEnum.Adviser || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)));
            advisers = advisers.OrderByDescending(m => m.Active);
            if (isAssistant)
                advisers = advisers.Where(m => m.OwnerUserId == UserId);
            int rowsTotal = advisers.Count();
            if (query.RolesList != null && query.RolesList.Count > 0)
                advisers = advisers.Where(m => query.RolesList.Contains((int)m.AdminRole) || m.Permissions.Any(n => query.RolesList.Contains((int)n.RoleType)));
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "UserData,Permissions,AdviserContactInfos,AssistantPermissions";
            var result = await advisers.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Data = result.Data.ToList().Select(m =>
            {
                m.EncryptedGuid = m.Guid.ToString().EncryptAsync().Result;
                return m;
            }).AsQueryable();
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> AddAdviser(Guid guid, bool isAssistant)
        {
            var adviser = await _context2.UserLogins.Where(m => m.Guid == guid)
                .Include(m => m.UserData).Include(m => m.AssistantPermissions).FirstOrDefaultAsync();
            ViewBag.isOwner = UserLoginGuid == adviser?.Guid;
            ViewBag.isAssistant = isAssistant;
            if (guid != Guid.Empty)
            {
                var twoFactorAuthenticator = new TwoFactorAuthenticator();
                var accountSecretKey = $"{_config["SecretKey"]}-{adviser.Username}";
                var setupCode = twoFactorAuthenticator.GenerateSetupCode(_config["AppName"], adviser.Username, Encoding.ASCII.GetBytes(accountSecretKey));
                ViewBag.SetupKey = setupCode.ManualEntryKey;
            }
            return View("_AddAdviserModal", adviser);
        }
        [HttpPost]
        public async Task<IActionResult> SaveAdviser(UserLogin adviserLogin)
        {
            var role = User.FindFirst(ClaimTypes.Role)?.Value;
            bool veryfied = false;
            string OldEmail = string.Empty;
            string NewEmail = adviserLogin.Username;
            if (adviserLogin.AssistantPermission is not null)
            {
                var assistantPermissions = new List<AssistantPermission>();
                foreach (var item in adviserLogin.AssistantPermission)
                {
                    assistantPermissions.Add(new AssistantPermission
                    {
                        Permission = EnumHelper.ToEnum<AssistantPermissionEnum>(item.ToString())
                    });
                }
                adviserLogin.AssistantPermissions = assistantPermissions;
            }
            if (adviserLogin.Guid == Guid.Empty)
            {
                var adviserLoginObj = new UserLogin
                {
                    Username = adviserLogin.Username,
                    FirmId = FirmId,
                    AdminRole = AdminRoleEnum.Adviser,
                    IsAssistant = adviserLogin.IsAssistant,
                    OwnerUserId = adviserLogin.IsAssistant ? UserId : null,
                    UserData = new UserData
                    {
                        UserId = UserId,
                        FirstName = adviserLogin.UserData.FirstName,
                        MiddleInitial = adviserLogin.UserData.MiddleInitial,
                        LastName = adviserLogin.UserData.LastName,
                    },
                    AssistantPermissions = adviserLogin.AssistantPermissions,
                    Active = true
                };
                _context2.UserLogins.Add(adviserLoginObj);
                adviserLogin.AdminRole = adviserLoginObj.AdminRole;
                veryfied = true;
            }
            else
            {
                var adviserObj = await _context2.UserLogins.Where(m => m.Guid == adviserLogin.Guid)
               .Include(m => m.UserData).Include(m => m.AssistantPermissions).FirstOrDefaultAsync();
                var adviserDataObj = adviserObj.UserData;
                adviserDataObj.FirstName = adviserLogin.UserData.FirstName;
                adviserDataObj.MiddleInitial = adviserLogin.UserData.MiddleInitial;
                adviserDataObj.LastName = adviserLogin.UserData.LastName;

                adviserDataObj.UserLogin.Active = adviserLogin.Active;
                adviserDataObj.UserLogin.AssistantPermissions = adviserLogin.AssistantPermissions;
                OldEmail = adviserDataObj.UserLogin.Username;

                if (adviserDataObj.UserLogin.Username != adviserLogin.Username)
                {
                    if (string.IsNullOrEmpty(adviserLogin.VerificationCode))
                    {
                        veryfied = true;
                        Random generator = new();
                        string code = generator.Next(0, 9999).ToString("D4");
                        adviserDataObj.UserLogin.VerificationCode = code;
                        var initiator = await _context2.UserDatas.FirstOrDefaultAsync(m => m.UserLogin.Guid == UserLoginGuid);
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", adviserDataObj.FirstName);
                        var emailText = @$"This is to notify you that <span style='color:#0073e9'>{initiator.Name}</span> has initiated the email change process.
                                           Please verify by applying the code in the Change Email Address pop-up.<br>
                                           Verification Code: <b style='color:#800000'>{code}</b>";
                        emailTemplateHtml.Replace("@EMAILTEXT", emailText);
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        string email = User.FindFirst(ClaimTypes.Email)?.Value;
                        string receiver = _env.EnvironmentName == "Production" ? $"{email},{adviserLogin.Username}" : $"{email},{adviserLogin.Username},{_config["Email:SuperAdminEmail"]}";
                        await emailHelper.SendEmailAsync(receiver, null, _config["Email:BccEmail"], EmailSubject.EmailChangeRequest, emailHtmlBody, _env.WebRootPath);
                    }
                    else
                    {
                        if (adviserLogin.VerificationCode == adviserDataObj.UserLogin.VerificationCode)
                        {
                            adviserDataObj.UserLogin.Username = adviserLogin.Username;
                            veryfied = true;
                            adviserDataObj.UserLogin.VerificationCode = string.Empty;
                        }
                    }
                    await _context2.SaveChangesAsync();
                    if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && veryfied && !string.IsNullOrEmpty(adviserLogin.VerificationCode))
                    {
                        StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                        mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                        StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/common-template.html")));
                        emailTemplateHtml.Replace("@NAME", adviserDataObj.FirstName);
                        emailTemplateHtml.Replace("@EMAILTEXT", string.Format(EmailNotification.EMAILCHANGE, adviserDataObj.UserLogin.Username));
                        mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                        EmailHelper emailHelper = new(_config);
                        string emailHtmlBody = mainEmailTemplateHtml.ToString();
                        await emailHelper.SendEmailAsync(adviserDataObj.UserLogin.Username, null, _config["Email:BccEmail"], EmailSubject.EmailChangeNotification, emailHtmlBody, _env.WebRootPath);
                    }
                }
                _context2.UserDatas.Update(adviserDataObj);
            }
            await _context2.SaveChangesAsync();
            if (adviserLogin.Guid == Guid.Empty)
                await InviteAdviser(adviserLogin);
            string msg = string.Empty;
            if (OldEmail == NewEmail)
                msg = adviserLogin.Guid != Guid.Empty ? Message.SAVE_SUCCESS : string.Format(Message.CLIENTADVISER_ADDED, role == Roles.Adviser ? "Assistant" : Roles.Adviser);
            else
                msg = string.IsNullOrEmpty(adviserLogin.VerificationCode) ? string.Format(Message.VERIFICATION_CODE_SENT, adviserLogin.Username) : veryfied && !string.IsNullOrEmpty(adviserLogin.VerificationCode) ? Message.VARIFIED_SUCCESS : Message.INVALID_VERIFICATION_CODE;
            var result = new CommonResponse
            {
                Success = OldEmail == NewEmail || veryfied,
                Message = msg,
                ParentId = FirmId,
                ActionType = adviserLogin.Guid != Guid.Empty ? "update" : "add",
                Data = new { OldEmail, NewEmail }
            };
            return Json(result);
        }
        public async Task InviteAdviser(UserLogin adviserLogin)
        {
            string name = adviserLogin.UserData.FirstName;
            string token = await adviserLogin.Username.EncryptAsync();
            var createPasswordLink = Url.RouteUrl("user/create-password", new { token }, Request.Scheme);
            StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
            mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
            StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/create-password-user.html")));
            emailTemplateHtml.Replace("@EMAILTO", adviserLogin.Username);
            emailTemplateHtml.Replace("@CREATEPASSWORDLINK", createPasswordLink);
            emailTemplateHtml.Replace(oldValue: "@NAME", name);
            emailTemplateHtml.Replace("@ROLE", adviserLogin.IsAssistant ? Roles.Assistant : adviserLogin.AdminRole.GetDisplayName());
            emailTemplateHtml.Replace("@AORAN", adviserLogin.AdminRole == AdminRoleEnum.Adviser ? "an" : "a");
            emailTemplateHtml.Replace("@ADDITIONALTEXT", adviserLogin.IsAssistant ? Roles.Adviser : "firm manage your firm's operations, financial professionals, and their clients");
            emailTemplateHtml.Replace("@PERMISSIONS", "");
            mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
            EmailHelper emailHelper = new(_config);
            string emailHtmlBody = mainEmailTemplateHtml.ToString();
            await emailHelper.SendEmailAsync(adviserLogin.Username, null, _config["Email:BccEmail"], EmailSubject.CreatePassword, emailHtmlBody, _env.WebRootPath);
            await Task.CompletedTask;
        }
        public async Task<IActionResult> ReInvite(Guid userLoginGuid)
        {
            var adviser = await _context2.UserLogins.Where(m => m.Guid == userLoginGuid).Include(m => m.UserData).FirstOrDefaultAsync();
            await InviteAdviser(adviser);
            var result = new CommonResponse
            {
                Success = adviser is not null,
                Message = adviser is not null ? Message.RESEND_INVITATION : string.Format(Message.NO_RECORD_FOUND),
            };
            return Json(result);
        }
        public async Task<IActionResult> FetchAdvisersWithCount(int firmId, DatatableMetaModel meta, CommonSearchModel query)
        {
            var advisers = (await _context2.UserLogins.Where(m => m.FirmId == firmId && (m.AdminRole == AdminRoleEnum.Adviser || m.Permissions.Any(n => n.RoleType == AdminRoleEnum.Adviser)))
           .Select(m => new
           {
               m.Id,
               m.Username,
               m.UserData.Name,
               UserData = new UserData
               {
                   FirstName = m.UserData.FirstName,
                   LastName = m.UserData.LastName
               },
               m.Active,
               TotalClient = m.ClientLogins.Count(n => n.UserId == m.Id && n.IsDeleted != true)
           }).OrderByDescending(m => m.Active).ToListAsync()).AsQueryable();
            int rowsTotal = advisers.Count();

            query.FirmId = null;
            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            string sortString = DataTableHelper.GetSortString(field, sort);
            var result = await advisers.PagedListAsync(meta, null, sortString, searchQuery);
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }
        public async Task<IActionResult> FetchClients(int userId, DatatableMetaModel meta, CommonSearchModel query)
        {
            query.FirmId = null;
            var FID = FirmId > 0 ? FirmId : (await _context2.UserLogins.FirstOrDefaultAsync(m => m.Id == userId)).FirmId;
            var clients = _context2.ClientLogins.Where(m => m.UserLogin.FirmId == FID && m.IsDeleted != true);

            if (userId > 0)
                clients = clients.Where(m => m.UserId == userId);
            string sort = meta.sort.sort;
            string field = meta.sort.field;
            int rowsTotal = clients.Count();

            string searchQuery = DataTableHelper.GetSearchQueryFromSearchModel(query);
            string sortString = DataTableHelper.GetSortString(field, sort);
            string includeProperties = "ClientData,SpouseData,ClientOccupations,EducationPlannings,UserLogin,UserLogin.Integration";
            var result = await clients.PagedListAsync(meta, includeProperties, sortString, searchQuery);
            result.Data = result.Data.ToList().Select(m =>
              {
                  m.EncryptedGuid = m.Guid.ToString().EncryptAsync().Result;
                  return m;
              }).AsQueryable();
            result.Meta.Sort = sort;
            result.Meta.Field = field;
            result.Meta.RowsTotal = rowsTotal;
            return Json(result);
        }

        public async Task<Select2Model> GetAdvisers(string term, int page, int firmId, int ownerId)
        {
            firmId = FirmId > 0 ? FirmId : firmId;
            return await _dropdownDataHelper.GetAdvisersAsync(term, page, firmId, ownerId);
        }
        public async Task<IActionResult> ComplianceDocuments()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
                .Include(m => m.UserData).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            ViewBag.EncryptedGuid = await UserLoginGuid.ToString().EncryptAsync();
            ViewBag.hasRIA = adviserDataObj.UserData.RIAId > 0;
            return View();
        }
        public async Task<IActionResult> ComplianceDocumentReviewModal(Guid guid)
        {
            var document = await _context2.ComplianceDocuments.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            return View("_ComplianceDocumentReviewModal", document);
        }
        [HttpPost]
        public async Task<IActionResult> ComplianceDocumentReview(ComplianceDocument complianceDocument)
        {
            var document = await _context2.ComplianceDocuments.Where(m => m.Guid == complianceDocument.Guid)
                .Include(m => m.User).Include(m => m.User.UserData).Include(m => m.ReviewedBy).FirstOrDefaultAsync();
            bool isStatusUpdated = complianceDocument.Status != document.Status;
            document.Status = complianceDocument.Status;
            document.ReviewedById = UserId;
            document.ReviewedDate = DateTime.UtcNow;
            document.ComplianceNote = complianceDocument.ComplianceNote;
            document.Allegation = complianceDocument.Allegation;
            document.TerminationType = complianceDocument.TerminationType;
            document.ReqiuredChange = complianceDocument.ReqiuredChange;
            await _context2.SaveChangesAsync();
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && isStatusUpdated)
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(Path.Combine(_env.WebRootPath, "EmailTemplates/compliance-document-review.html")));
                emailTemplateHtml.Replace("@NAME", document.User.UserData.Name);
                var reviewBy = await _context2.UserLogins.Where(m => m.Id == document.ReviewedById).Include(m => m.UserData).FirstOrDefaultAsync();
                emailTemplateHtml.Replace("@COMPLIANCENAME", reviewBy.UserData.Name);
                emailTemplateHtml.Replace("@EXTRATEXT", document.Status == ComplianceDocumentStatusEnum.Approved && document.ReqiuredChange ? " with suggested changes" : "");
                emailTemplateHtml.Replace("@FILENAME", document.FileName);
                emailTemplateHtml.Replace("@COLOR", document.Status == ComplianceDocumentStatusEnum.Approved ? "#009688" : "#f64e60");
                emailTemplateHtml.Replace("@STATUS", document.Status.ToString());
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                var complianceEmails = _context2.Permissions.Where(m => m.RoleType == AdminRoleEnum.Compliance && m.UserInfo.FirmId == FirmId)
                    .Include(m => m.UserInfo).Select(m => m.UserInfo.Username);
                await emailHelper.SendEmailAsync(document.User.Username, string.Join(",", complianceEmails), _config["Email:BccEmail"], EmailSubject.ComplianceDocumentReview, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(document.VersionId) || document.FileSize == 0,
                Message = !string.IsNullOrEmpty(document.VersionId) || document.FileSize == 0 ? Message.SAVE_SUCCESS : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        public async Task<IActionResult> InsuranceDocuments()
        {
            var adviserDataObj = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.Permissions).FirstOrDefaultAsync();
            ViewBag.additionalRoles = adviserDataObj.Permissions.Any() ? string.Join(",", adviserDataObj.Permissions.Select(m => m.RoleType.GetDisplayName())) : null;
            return View();
        }
        public async Task<IActionResult> InsuranceDocumentReviewModal(Guid guid)
        {
            var document = await _context2.InsuranceDocuments.Where(m => m.Guid == guid).FirstOrDefaultAsync();
            return View("_InsuranceDocumentReviewModal", document);
        }
        [HttpPost]
        public async Task<IActionResult> InsuranceDocumentReview(InsuranceDocument insuranceDocument)
        {
            var document = await _context2.InsuranceDocuments.Where(m => m.Guid == insuranceDocument.Guid)
                .Include(m => m.ClientLogin).Include(m => m.ClientLogin.UserLogin).Include(m => m.ClientLogin.UserLogin.UserData).Include(m => m.ClientLogin.ClientData).Include(m => m.ReviewedBy).FirstOrDefaultAsync();
            bool isStatusUpdated = insuranceDocument.Status != document.Status;
            document.Status = insuranceDocument.Status;
            document.ReviewedById = UserId;
            document.ReviewedDate = DateTime.UtcNow;
            document.ReviewerNote = insuranceDocument.ReviewerNote;
            document.ReqiuredChange = insuranceDocument.ReqiuredChange;
            await _context2.SaveChangesAsync();
            if (!_httpContextAccessor.HttpContext.IsLocalOrSupport() && isStatusUpdated)
            {
                StringBuilder mainEmailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/main-template.html")));
                mainEmailTemplateHtml.Replace("@YEAR", DateTime.UtcNow.Year.ToString());
                StringBuilder emailTemplateHtml = new(System.IO.File.ReadAllText(System.IO.Path.Combine(_env.WebRootPath, "EmailTemplates/compliance-document-review.html")));

                emailTemplateHtml.Replace("@NAME", document.ClientLogin.UserLogin.UserData.Name);
                var reviewBy = await _context2.UserLogins.Where(m => m.Id == document.ReviewedById).Include(m => m.UserData).FirstOrDefaultAsync();
                emailTemplateHtml.Replace("@COMPLIANCENAME", reviewBy.UserData.Name);
                emailTemplateHtml.Replace("@EXTRATEXT", document.Status == InsuranceDocumentStatusEnum.Approved && document.ReqiuredChange ? " with suggested changes" : "");
                emailTemplateHtml.Replace("@FILENAME", document.FileName);
                emailTemplateHtml.Replace("@COLOR", document.Status == InsuranceDocumentStatusEnum.Approved ? "#009688" : document.Status == InsuranceDocumentStatusEnum.Issued ? "#8950FC" : "#f64e60");
                emailTemplateHtml.Replace("@STATUS", document.Status.ToString());
                mainEmailTemplateHtml.Replace("@CONTENT", emailTemplateHtml.ToString());
                EmailHelper emailHelper = new(_config);
                string emailHtmlBody = mainEmailTemplateHtml.ToString();
                var complianceEmails = _context2.Permissions.Where(m => m.RoleType == AdminRoleEnum.Compliance && m.UserInfo.FirmId == FirmId)
                    .Include(m => m.UserInfo).Select(m => m.UserInfo.Username);
                await emailHelper.SendEmailAsync(document.ClientLogin.UserLogin.Username, string.Join(",", complianceEmails), _config["Email:BccEmail"], EmailSubject.ComplianceDocumentReview, emailHtmlBody, _env.WebRootPath);
            }
            var result = new CommonResponse
            {
                Success = !string.IsNullOrEmpty(document.VersionId) || document.FileSize == 0,
                Message = !string.IsNullOrEmpty(document.VersionId) || document.FileSize == 0 ? Message.SAVE_SUCCESS : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        public async Task<Select2Model> GetComplianceUsers(string term, int page)
        {
            return await _dropdownDataHelper.GetComplianceUserAsync(term, FirmId, page);
        }
        public async Task<IActionResult> IsAdv2bValid()
        {
            var admin = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid)
               .Include(m => m.UserData).Include(m => m.Firm).Include(m => m.AdviserContactInfos).FirstOrDefaultAsync();
            bool isValidAdv2B = admin.UserData.DateOfBirth is not null && admin.AdviserContactInfos is not null;
            var result = new CommonResponse
            {
                Success = admin != null,
                Data = new { isValidAdv2B }
            };
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> SaveAdviserReports()
        {
            var adviser = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).FirstOrDefaultAsync();
            if (adviser != null && adviser.HasChanges)
            {
                var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
                _arcHiveController.SaveAdviserReports(UserLoginGuid, Convert.ToInt32(TimeZone));
                adviser.HasChanges = false;
                await _context2.SaveChangesAsync();
            }
            var result = new CommonResponse
            {
                Success = adviser != null,
                Message = adviser != null ? Message.ADV2BSAVED : Message.INTERNAL_SERVER_ERROR,
            };
            return Json(result);
        }
        private bool disposed;
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context2.Dispose();
                    _dropdownDataHelper.Dispose();
                }
                disposed = true;
            }
        }
        ~AdviserController()
        {
            Dispose(false);
        }
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}