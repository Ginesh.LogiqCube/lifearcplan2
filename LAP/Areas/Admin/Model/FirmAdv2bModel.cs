﻿using LAP.DomainModels;
using LAP.DomainModels.Entities.Adviser;
using System;
using System.Collections.Generic;

namespace LAP.Areas.Admin.Model
{
    public class FirmAdv2bModel
    {
        public string FirmName { get; set; }
        public string FileName { get; set; }
        public IEnumerable<FirmAdv2bMemberModel> Members { get; set; }
    }
    public class FirmAdv2bMemberModel
    {
        public AdminRoleEnum Role { get; set; }
        public bool IsAssistant { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public ComplianceDocument Document { get; set; }
    }
}
