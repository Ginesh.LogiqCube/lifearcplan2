﻿using Microsoft.AspNetCore.Builder;

namespace LAP
{
    public static class ServiceExtensions
    {
        public static void RouteConfigution(this IApplicationBuilder app)
        {
            app.UseEndpoints(endpoints =>
            {
                #region Clients Route
                endpoints.MapControllerRoute(
                    name: "welcome",
                    pattern: "welcome",
                    defaults: new { controller = "home", action = "welcome" });

                endpoints.MapControllerRoute(
                    name: "settings",
                    pattern: "settings",
                    defaults: new { controller = "home", action = "settings" });

                endpoints.MapControllerRoute(
                    name: "intro",
                    pattern: "intro",
                    defaults: new { controller = "home", action = "intro" });

                endpoints.MapControllerRoute(
                    name: "go-to-survey-last-action",
                    pattern: "go-to-survey-last-action",
                    defaults: new { controller = "home", action = "surveylastaction" });

                endpoints.MapControllerRoute(
                    name: "client/login",
                    pattern: "client/login",
                    defaults: new { controller = "account", action = "login" });

                endpoints.MapControllerRoute(
                    name: "settings",
                    pattern: "settings",
                    defaults: new { controller = "home", action = "settings" });

                endpoints.MapControllerRoute(
                    name: "dashboard",
                    pattern: "dashboard",
                    defaults: new { controller = "home", action = "dashboard" });

                endpoints.MapControllerRoute(
                    name: "client-info",
                    pattern: "client-info",
                    defaults: new { controller = "personal", action = "index" });

                endpoints.MapControllerRoute(
                    name: "marital-info",
                    pattern: "marital-info",
                    defaults: new { controller = "personal", action = "index" });

                endpoints.MapControllerRoute(
                    name: "spouse-info",
                    pattern: "spouse-info",
                    defaults: new { controller = "personal", action = "index" });

                endpoints.MapControllerRoute(
                    name: "contact-info",
                    pattern: "contact-info",
                    defaults: new { controller = "personal", action = "index" });

                endpoints.MapControllerRoute(
                    name: "family-info",
                    pattern: "family-info",
                    defaults: new { controller = "personal", action = "index" });

                endpoints.MapControllerRoute(
                    name: "personal-property",
                    pattern: "personal-property",
                    defaults: new { controller = "personal", action = "index" });

                endpoints.MapControllerRoute(
                    name: "reset-password",
                    pattern: "reset-password",
                    defaults: new { controller = "account", action = "resetpassword" });

                endpoints.MapControllerRoute(
                    name: "create-password",
                    pattern: "create-password",
                    defaults: new { controller = "account", action = "resetpassword" });

                endpoints.MapControllerRoute(
                    name: "employment-info",
                    pattern: "employment-info",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "retirement-plan",
                    pattern: "retirement-plan",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "business-interests",
                    pattern: "business-interests",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "personal-activities",
                    pattern: "personal-activities",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "personal-assets",
                    pattern: "personal-assets",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                   name: "investment-income",
                   pattern: "investment-income",
                   defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "budget-info",
                    pattern: "budget-info",
                    defaults: new { controller = "expense", action = "index" });

                endpoints.MapControllerRoute(
                    name: "budget-info-express",
                    pattern: "budget-info-express",
                    defaults: new { controller = "expense", action = "index" });

                endpoints.MapControllerRoute(
                    name: "social-security-intro",
                    pattern: "social-security-intro",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "social-security-estimate",
                    pattern: "social-security-estimate",
                    defaults: new { controller = "income", action = "index" });

                endpoints.MapControllerRoute(
                    name: "investment-planning-express",
                    pattern: "investment-planning-express",
                    defaults: new { controller = "investmentplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "investment-planning-intro",
                    pattern: "investment-planning-intro",
                    defaults: new { controller = "investmentplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "bank-credit-union-accounts",
                    pattern: "bank-credit-union-accounts",
                    defaults: new { controller = "investmentplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "brokerage-advisory-accounts",
                    pattern: "brokerage-advisory-accounts",
                    defaults: new { controller = "investmentplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "life-annuity-contracts",
                    pattern: "life-annuity-contracts",
                    defaults: new { controller = "investmentplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "estate-planning-intro",
                    pattern: "estate-planning-intro",
                    defaults: new { controller = "estateplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "estate-planning-assessment",
                    pattern: "estate-planning-assessment",
                    defaults: new { controller = "estateplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "charitable-planning-intro",
                    pattern: "charitable-planning-intro",
                    defaults: new { controller = "charitableplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "charitable-planning-ambitions",
                    pattern: "charitable-planning-ambitions",
                    defaults: new { controller = "charitableplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "client-riskarc-indepth",
                    pattern: "client-riskarc-indepth",
                    defaults: new { controller = "riskarc", action = "clientriskarc", isIndepth = true });

                endpoints.MapControllerRoute(
                    name: "client-riskarc-express",
                    pattern: "client-riskarc-express",
                    defaults: new { controller = "riskarc", action = "clientriskarc", isIndepth = false });

                endpoints.MapControllerRoute(
                    name: "spouse-riskarc-indepth",
                    pattern: "spouse-riskarc-indepth",
                    defaults: new { controller = "riskarc", action = "spouseriskarc", isIndepth = true });

                endpoints.MapControllerRoute(
                    name: "spouse-riskarc-express",
                    pattern: "spouse-riskarc-express",
                    defaults: new { controller = "riskarc", action = "spouseriskarc", isIndepth = false });

                endpoints.MapControllerRoute(
                    name: "personal-care-planning-intro",
                    pattern: "personal-care-planning-intro",
                    defaults: new { controller = "personalcareplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "personal-care-planning-plan",
                    pattern: "personal-care-planning-plan",
                    defaults: new { controller = "personalcareplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "education-planning-intro",
                    pattern: "education-planning-intro",
                    defaults: new { controller = "educationplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "education-planning-assessment",
                    pattern: "education-planning-assessment",
                    defaults: new { controller = "educationplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "healthcare-planning-intro",
                    pattern: "healthcare-planning-intro",
                    defaults: new { controller = "healthcareplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "healthcare-planning-assessment",
                    pattern: "healthcare-planning-assessment",
                    defaults: new { controller = "healthcareplanning", action = "index" });

                endpoints.MapControllerRoute(
                  name: "final-thought",
                  pattern: "final-thought",
                  defaults: new { controller = "home", action = "finalthought" });

                endpoints.MapControllerRoute(
                  name: "final-completion",
                  pattern: "final-completion",
                  defaults: new { controller = "home", action = "finalcompletion" });

                endpoints.MapControllerRoute(
                   name: "life-style-intro",
                   pattern: "life-style-intro",
                   defaults: new { controller = "lifestyle", action = "index" });

                endpoints.MapControllerRoute(
                   name: "health-assessment",
                   pattern: "health-assessment",
                   defaults: new { controller = "lifestyle", action = "index" });

                endpoints.MapControllerRoute(
                   name: "health-expectations",
                   pattern: "health-expectations",
                   defaults: new { controller = "lifestyle", action = "index" });

                endpoints.MapControllerRoute(
                   name: "retirement-income-planning-intro",
                   pattern: "retirement-income-planning-intro",
                   defaults: new { controller = "retirementincomeplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "retirement-income-planning-status",
                    pattern: "retirement-income-planning-status",
                    defaults: new { controller = "retirementincomeplanning", action = "index" });

                endpoints.MapControllerRoute(
                    name: "retirement-income-planning-income-concern",
                    pattern: "retirement-income-planning-income-concern",
                    defaults: new { controller = "retirementincomeplanning", action = "index" });

                endpoints.MapControllerRoute(
                  name: "retirement-income-planning-expense-concern",
                  pattern: "retirement-income-planning-expense-concern",
                  defaults: new { controller = "retirementincomeplanning", action = "index" });

                endpoints.MapControllerRoute(
                 name: "arc-hive",
                 pattern: "arc-hive",
                 defaults: new { controller = "home", action = "documentvault" });

                endpoints.MapControllerRoute(
                 name: "tax-documents",
                 pattern: "tax-documents",
                 defaults: new { controller = "home", action = "taxdocuments" });

                endpoints.MapControllerRoute(
                   name: "activity-log",
                   pattern: "activity-log",
                   defaults: new { controller = "home", action = "activitylog" });

                endpoints.MapControllerRoute(
                    name: "adviser-info",
                    pattern: "adviser-info",
                    defaults: new { controller = "home", action = "adviserinfo" });

                endpoints.MapControllerRoute(
                    name: "welcome-user",
                    pattern: "welcome-user",
                    defaults: new { area = "adviser", controller = "home", action = "welcomeuser" });

                endpoints.MapControllerRoute(
                 name: "user/client-arc-hive",
                 pattern: "user/client-arc-hive",
                 defaults: new { area = "adviser", controller = "home", action = "clientdocumentvault" });

                endpoints.MapControllerRoute(
                    name: "user/dashboard",
                    pattern: "user/dashboard",
                    defaults: new { area = "adviser", controller = "dashboard", action = "index" });

                endpoints.MapControllerRoute(
                    name: "user/activities",
                    pattern: "user/activities",
                    defaults: new { area = "adviser", controller = "dashboard", action = "activities" });

                endpoints.MapControllerRoute(
                    name: "user/client-acquisition-pipeline",
                    pattern: "user/client-acquisition-pipeline",
                    defaults: new { area = "adviser", controller = "dashboard", action = "marketing" });

                endpoints.MapControllerRoute(
                    name: "firm-principal/client-list",
                    pattern: "firm-principal/client-list",
                    defaults: new { area = "adviser", controller = "home", action = "clients" });

                endpoints.MapControllerRoute(
                   name: "firm-admin/client-list",
                   pattern: "firm-admin/client-list",
                   defaults: new { area = "adviser", controller = "home", action = "clients" });

                endpoints.MapControllerRoute(
                   name: "adviser/client-list",
                   pattern: "adviser/client-list",
                   defaults: new { area = "adviser", controller = "home", action = "clients" });

                endpoints.MapControllerRoute(
                    name: "firm-principal/assistant-list",
                    pattern: "firm-principal/assistant-list",
                    defaults: new { area = "adviser", controller = "home", action = "assistants" });

                endpoints.MapControllerRoute(
                    name: "firm-admin/assistant-list",
                    pattern: "firm-admin/assistant-list",
                    defaults: new { area = "adviser", controller = "home", action = "assistants" });

                endpoints.MapControllerRoute(
                    name: "adviser/assistant-list",
                    pattern: "adviser/assistant-list",
                    defaults: new { area = "adviser", controller = "home", action = "assistants" });
                #endregion

                #region Adviser route
                endpoints.MapControllerRoute(
                   name: "firm-principal/adviser-list",
                   pattern: "firm-principal/adviser-list",
                   defaults: new { area = "admin", controller = "adviser", action = "advisers" });

                endpoints.MapControllerRoute(
                   name: "firm-admin/adviser-list",
                   pattern: "firm-admin/adviser-list",
                   defaults: new { area = "admin", controller = "adviser", action = "advisers" });

                endpoints.MapControllerRoute(
                   name: "adviser/compliance-documents",
                   pattern: "adviser/compliance-documents",
                   defaults: new { area = "adviser", controller = "home", action = "compliancedocuments" });

                endpoints.MapControllerRoute(
                   name: "adviser/insurance-documents",
                   pattern: "adviser/insurance-documents",
                   defaults: new { area = "adviser", controller = "home", action = "insurancedocuments" });

                endpoints.MapControllerRoute(
                   name: "user/tax-documents",
                   pattern: "user/tax-documents",
                   defaults: new { area = "adviser", controller = "home", action = "taxdocuments" });

                endpoints.MapControllerRoute(
                    name: "adviser/info",
                    pattern: "adviser/info",
                    defaults: new { area = "adviser", controller = "profile", action = "index" });

                endpoints.MapControllerRoute(
                   name: "adviser/contact-info",
                   pattern: "adviser/contact-info",
                   defaults: new { area = "adviser", controller = "profile", action = "index" });

                endpoints.MapControllerRoute(
                   name: "adviser/education-info",
                   pattern: "adviser/education-info",
                   defaults: new { area = "adviser", controller = "profile", action = "index" });

                endpoints.MapControllerRoute(
                   name: "adviser/work-history",
                   pattern: "adviser/work-history",
                   defaults: new { area = "adviser", controller = "profile", action = "index" });

                endpoints.MapControllerRoute(
                   name: "adviser/business",
                   pattern: "adviser/business",
                   defaults: new { area = "adviser", controller = "profile", action = "index" });

                endpoints.MapControllerRoute(
                   name: "adviser/regulatory",
                   pattern: "adviser/regulatory",
                   defaults: new { area = "adviser", controller = "profile", action = "index" });


                endpoints.MapControllerRoute(
                   name: "regulatory-documents",
                   pattern: "regulatory-documents",
                   defaults: new { controller = "home", action = "adviserregulatorydocuments" });

                endpoints.MapControllerRoute(
                   name: "adviser/regulatory-documents",
                   pattern: "adviser/regulatory-documents",
                   defaults: new { area = "adviser", controller = "home", action = "regulatorydocuments" });

                endpoints.MapControllerRoute(
                  name: "firm-admin/regulatory-documents",
                  pattern: "firm-admin/regulatory-documents",
                  defaults: new { area = "adviser", controller = "home", action = "regulatorydocuments" });

                endpoints.MapControllerRoute(
                  name: "firm-principal/regulatory-documents",
                  pattern: "firm-principal/regulatory-documents",
                  defaults: new { area = "adviser", controller = "home", action = "regulatorydocuments" });

                endpoints.MapControllerRoute(
                   name: "user/security-setting",
                   pattern: "user/security-setting",
                   defaults: new { area = "adviser", controller = "settings", action = "index" });

                endpoints.MapControllerRoute(
                     name: "user/connected-apps",
                     pattern: "user/connected-apps",
                     defaults: new { area = "adviser", controller = "settings", action = "connectedapps" });

                endpoints.MapControllerRoute(
                   name: "firm-principal/user-message-board",
                   pattern: "firm-principal/user-message-board",
                   defaults: new { area = "admin", controller = "firmadmin", action = "usermessageboard" });

                endpoints.MapControllerRoute(
                   name: "firm-admin/user-message-board",
                   pattern: "firm-admin/user-message-board",
                   defaults: new { area = "admin", controller = "firmadmin", action = "usermessageboard" });

                endpoints.MapControllerRoute(
                  name: "adviser/client-message-board",
                  pattern: "adviser/client-message-board",
                  defaults: new { area = "adviser", controller = "home", action = "clientmessageboard" });

                endpoints.MapControllerRoute(
                   name: "firm-principal/client-message-board",
                   pattern: "firm-principal/client-message-board",
                   defaults: new { area = "adviser", controller = "home", action = "clientmessageboard" });

                endpoints.MapControllerRoute(
                   name: "firm-admin/client-message-board",
                   pattern: "firm-admin/client-message-board",
                   defaults: new { area = "adviser", controller = "home", action = "clientmessageboard" });

                endpoints.MapControllerRoute(
                   name: "adviser/adv-2b",
                   pattern: "adviser/adv-2b",
                   defaults: new { area = "adviser", controller = "report", action = "adv_2b" });

                endpoints.MapControllerRoute(
                   name: "firm/adv-2b",
                   pattern: "firm/adv-2b",
                   defaults: new { area = "adviser", controller = "report", action = "firmadv2b" });

                endpoints.MapControllerRoute(
                   name: "client/activity-log",
                   pattern: "client/activity-log",
                   defaults: new { area = "adviser", controller = "home", action = "activitylog" });

                endpoints.MapControllerRoute(
                   name: "client/tax-prep-queue-activity-log",
                   pattern: "client/tax-prep-queue-activity-log",
                   defaults: new { area = "adviser", controller = "home", action = "taxprepqueueactivitylog" });

                endpoints.MapControllerRoute(
                   name: "engagement-letter",
                   pattern: "engagement-letter",
                   defaults: new { area = "adviser", controller = "report", action = "engagementletter" });

                endpoints.MapControllerRoute(
                 name: "report/personal-canvas",
                 pattern: "report/personal-canvas",
                 defaults: new { controller = "home", action = "clientsurvey" });

                endpoints.MapControllerRoute(
                   name: "report/client-risk-arc",
                   pattern: "report/client-risk-arc",
                   defaults: new { area = "adviser", controller = "report", action = "riskarc" });

                endpoints.MapControllerRoute(
                   name: "report/arc-of-life",
                   pattern: "report/arc-of-life",
                   defaults: new { area = "adviser", controller = "report", action = "arcoflife" });

                endpoints.MapControllerRoute(
                   name: "report/risk-arc",
                   pattern: "report/risk-arc",
                   defaults: new { area = "adviser", controller = "report", action = "riskarc" });

                endpoints.MapControllerRoute(
                  name: "report/balance-sheet",
                  pattern: "report/balance-sheet",
                  defaults: new { area = "adviser", controller = "report", action = "balancesheetreport" });

                endpoints.MapControllerRoute(
                   name: "report/risk-arc-survey",
                   pattern: "report/risk-arc-survey",
                   defaults: new { controller = "riskarc", action = "riskarcsurvey" });

                endpoints.MapControllerRoute(
                  name: "adviser-documents",
                  pattern: "adviser-documents",
                  defaults: new { area = "adviser", controller = "home", action = "adviserdocuments" });
                #endregion

                #region Admin route
                endpoints.MapControllerRoute(
                   name: "user/login",
                   pattern: "user/login",
                   defaults: new { area = "admin", controller = "account", action = "login" });

                endpoints.MapControllerRoute(
                    name: "user/create-password",
                    pattern: "user/create-password",
                    defaults: new { area = "admin", controller = "account", action = "resetpassword" });

                endpoints.MapControllerRoute(
                    name: "user/reset-password",
                    pattern: "user/reset-password",
                    defaults: new { area = "admin", controller = "account", action = "resetpassword" });

                endpoints.MapControllerRoute(
                   name: "user/release-notes",
                   pattern: "user/release-notes",
                   defaults: new { area = "adviser", controller = "home", action = "releasenotes" });

                #region super admin
                endpoints.MapControllerRoute(
                    name: "super-admin/login",
                    pattern: "super-admin/login",
                    defaults: new { area = "admin", controller = "account", action = "login" });

                endpoints.MapControllerRoute(
                    name: "super-admin/firm-principal-list",
                    pattern: "super-admin/firm-principal-list",
                    defaults: new { area = "admin", controller = "superadmin", action = "firmprincipallist" });

                endpoints.MapControllerRoute(
                    name: "super-admin/firm-admin-list",
                    pattern: "super-admin/firm-admin-list",
                    defaults: new { area = "admin", controller = "superadmin", action = "firmadminlist" });

                endpoints.MapControllerRoute(
                    name: "super-admin/adviser-list",
                    pattern: "super-admin/adviser-list",
                    defaults: new { area = "admin", controller = "superadmin", action = "adviserlist" });

                endpoints.MapControllerRoute(
                    name: "super-admin/client-list",
                    pattern: "super-admin/client-list",
                    defaults: new { area = "admin", controller = "superadmin", action = "clientlist" });

                endpoints.MapControllerRoute(
                   name: "super-admin/firm-affiliations",
                   pattern: "super-admin/firm-affiliations",
                   defaults: new { area = "admin", controller = "superadmin", action = "affiliations" });

                endpoints.MapControllerRoute(
                   name: "super-admin/regulatory-documents",
                   pattern: "super-admin/regulatory-documents",
                   defaults: new { area = "admin", controller = "superadmin", action = "regulatorydocuments" });

                endpoints.MapControllerRoute(
                   name: "super-admin/release-notes",
                   pattern: "super-admin/release-notes",
                   defaults: new { area = "adviser", controller = "home", action = "releasenotes" });
                #endregion

                #region firm admin
                endpoints.MapControllerRoute(
                    name: "firm-principal/firm-admin-list",
                    pattern: "firm-principal/firm-admin-list",
                    defaults: new { area = "admin", controller = "firmadmin", action = "firmadmins" });

                endpoints.MapControllerRoute(
                   name: "firm-admin/compliance-documents",
                   pattern: "firm-admin/compliance-documents",
                   defaults: new { area = "admin", controller = "adviser", action = "compliancedocuments" });

                endpoints.MapControllerRoute(
                  name: "firm-admin/insurance-documents",
                  pattern: "firm-admin/insurance-documents",
                  defaults: new { area = "admin", controller = "adviser", action = "insurancedocuments" });

                #endregion

                #region firm principal

                endpoints.MapControllerRoute(
                   name: "firm-principal/compliance-documents",
                   pattern: "firm-principal/compliance-documents",
                   defaults: new { area = "admin", controller = "adviser", action = "compliancedocuments" });

                endpoints.MapControllerRoute(
                  name: "firm-principal/insurance-documents",
                  pattern: "firm-principal/insurance-documents",
                  defaults: new { area = "admin", controller = "adviser", action = "insurancedocuments" });

                #endregion
                #endregion
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=home}/{action=index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "areas",
                    pattern: "{area:exists}/{controller=home}/{action=index}/{id?}");
            });
        }
    }
}