﻿using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.ViewComponents
{
    public class RightPanelViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public RightPanelViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            Guid ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            var clientData = await _context2.ClientDatas.Where(m => m.ClientLogin.Guid == ClientLoginGuid).Include(m => m.ClientLogin).FirstOrDefaultAsync();
            ViewBag.ClientName = $"{clientData.FirstName} {clientData.LastName}";
            ViewBag.Email = clientData.ClientLogin.Username;
            ViewBag.ProfileCompleted = clientData.ClientLogin.InitialProfileSetupCompleted && clientData.ClientLogin.SetupCompletedDate is not null;
            return await Task.FromResult(View());
        }
    }
}