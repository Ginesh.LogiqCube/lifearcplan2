﻿using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.ViewComponents
{
    public class TopBarMobileUserToggleViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public TopBarMobileUserToggleViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            Guid ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            if (ClientLoginGuid != Guid.Empty)
            {
                var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.ClientData).FirstOrDefaultAsync();
                var clientData = client.ClientData;
                ViewBag.ClientName = clientData.FirstName;
            }
            Guid UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            if (UserLoginGuid != Guid.Empty)
            {
                var adviser = await _context2.UserLogins.Where(m => m.Guid == UserLoginGuid).Include(m => m.UserData).FirstOrDefaultAsync();
                var adviserData = adviser.UserData;
                ViewBag.AdviserName = adviserData.FirstName;
            }
            return await Task.FromResult(View());
        }
    }
}