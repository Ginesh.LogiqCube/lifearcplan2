﻿using LAP.DomainModels.Entities;
using LAP.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.ViewComponents
{
    public class WorkingHourViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        public WorkingHourViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            Guid UserLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            var user = await _context2.UserDatas.Where(m => m.UserLogin.Guid == UserLoginGuid).FirstOrDefaultAsync();
            var defaultWorkingHours = await MenuDataHelper.GetDefaultWorkingHourDataAsync();
            ViewBag.WorkingHours = user.WorkingHourConfig != null ? user.WorkingHourConfig : defaultWorkingHours;
            return await Task.FromResult(View());
        }
    }
}