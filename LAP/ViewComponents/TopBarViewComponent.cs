﻿using LAP.DomainModels.Entities;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP.ViewComponents
{
    public class TopBarViewComponent : ViewComponent
    {
        protected readonly LifeArcPlanContext2 _context2;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly CommonService _commonService;
        protected readonly InvestmentService _investmentService;
        public TopBarViewComponent(LifeArcPlanContext2 context2, IHttpContextAccessor httpContextAccessor)
        {
            _context2 = context2;
            _httpContextAccessor = httpContextAccessor;
            _commonService = new CommonService(_context2);
            _investmentService = new InvestmentService(_context2, _httpContextAccessor);
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            Guid ClientLoginGuid = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            if (ClientLoginGuid != Guid.Empty)
            {
                var yearToRetire = await _commonService.GetYearToRetirementAsync(ClientLoginGuid);
                var client = await _context2.ClientLogins.Where(m => m.Guid == ClientLoginGuid && m.IsDeleted != true).Include(m => m.UserLogin)
                    .Include(m => m.ClientData).Include(m => m.SpouseData).Include(m => m.ClientOccupations).FirstOrDefaultAsync();
                var clientData = client.ClientData;
                var spouseData = client.SpouseData;
                var TimeZone = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "TimeZone")?.Value;
                ViewBag.FirmId = client.UserLogin.FirmId;
                ViewBag.ClientId = client.Id;
                ViewBag.ClientMaritalStatus = (int)clientData.MaritalStatus;
                ViewBag.IsMarried = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate();
                ViewBag.ClientDOB = clientData.DateOfBirth.ToString("MMM dd, yyyy");
                ViewBag.ClientName = clientData.FirstName;
                ViewBag.ClientFullName = $"{clientData.FirstName} {clientData.LastName}";
                ViewBag.SpouseName = Convert.ToDateTime(clientData.DateOfCurrentMarriage).IsNotEmptyDate() ? spouseData?.FirstName : null;
                ViewBag.BudgetVersion = clientData.BudgetVersion;
                ViewBag.InvestmentVersion = clientData.InvestmentVersion;
                ViewBag.RiskArcVersion = clientData.RiskArcVersion;
                ViewBag.ClientSpouseOccupations = string.Join(",", client.ClientOccupations.Select(m => (int)m.EmploymentStatus));
                ViewBag.ClientYearToRetire = yearToRetire;
                ViewBag.InitialProfileSetupCompleted = client.InitialProfileSetupCompleted;
                ViewBag.PlanningItems = (await _commonService.GetPlanningItemsAsync(ClientLoginGuid)).ToList();
                ViewBag.HasPersonalAssets = _context2.ClientProperties.Any(m => m.ClientLogin.Guid == ClientLoginGuid && m.AnnualIncome > 0);
                ViewBag.HasInvestmentIncome = _investmentService.HasInvestmentIncome(ClientLoginGuid);
                ViewBag.TimeZone = Convert.ToInt32(TimeZone);
            }
            ViewBag.DisplayOnlyClient = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "DisplayOnlyClient")?.Value;
            ViewBag.DisabledRightPanel = _httpContextAccessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "DisabledRightPanel")?.Value;
            ViewBag.LoginUrl = "client/login";
            return await Task.FromResult(View());
        }
    }
}