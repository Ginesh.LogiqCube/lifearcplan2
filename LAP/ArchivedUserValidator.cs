﻿using LAP.DomainModels;
using LAP.Helpers;
using LAP.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LAP
{
    public class ArchivedUserValidator
    {
        public static async Task ValidateClientAsync(CookieValidatePrincipalContext context)
        {
            Guid ClientLoginGuid = context.Principal.Claims.FirstOrDefault(c => c.Type == "ClientLoginGuid").ClaimToGuid();
            var commonService = context.HttpContext.RequestServices.GetRequiredService<CommonService>();
            bool isExist = commonService.IsExistClient(ClientLoginGuid);
            if (!isExist)
            {
                context.RejectPrincipal();
                await context.HttpContext.SignOutAsync(AuthenticationSchemes.ClientAuth);
            }
        }
        public static async Task ValidateUserAsync(CookieValidatePrincipalContext context)
        {
            Guid UserLoginGuid = context.Principal.Claims.FirstOrDefault(c => c.Type == "UserLoginGuid").ClaimToGuid();
            var commonService = context.HttpContext.RequestServices.GetRequiredService<CommonService>();
            DateTime? lastLoginDate = commonService.GetLastLogin(UserLoginGuid);
            bool isExist = commonService.IsExistUser(UserLoginGuid);
            if (!isExist)
            {
                context.RejectPrincipal();
                await context.HttpContext.SignOutAsync(AuthenticationSchemes.UserAuth);
            }
        }
    }
}