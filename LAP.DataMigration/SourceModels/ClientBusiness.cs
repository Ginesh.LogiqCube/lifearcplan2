﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientBusiness
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string EntityForm { get; set; }
        public string Ownership { get; set; }
        public string Fmv { get; set; }
        public string FormOwnership { get; set; }
        public string BuySell { get; set; }
        public string LastReview { get; set; }
        public string LifeIns { get; set; }
        public string RetirementPlan { get; set; }
        public string PlanType { get; set; }
        public string Contribution { get; set; }
        public string Participants { get; set; }
        public string PlanDetails { get; set; }
        public string WealthTransfer { get; set; }
        public DateTime DateCreated { get; set; }
        public string EnteredBy { get; set; }
        public string BusinessRetirementPlan { get; set; }
        public string ProjectedFairMarketValueOfBusiness { get; set; }
    }
}
