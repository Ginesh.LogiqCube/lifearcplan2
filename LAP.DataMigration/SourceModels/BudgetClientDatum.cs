﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class BudgetClientDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? ItemId { get; set; }
        public string Period { get; set; }
        public string Amount { get; set; }
        public string Increase { get; set; }
        public string Balance { get; set; }
        public string Duration { get; set; }
        public string DurationType { get; set; }
    }
}
