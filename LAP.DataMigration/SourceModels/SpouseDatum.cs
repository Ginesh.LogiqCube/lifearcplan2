﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class SpouseDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public bool? PreviousDivorce { get; set; }
        public bool? PreviousWidow { get; set; }
        public bool? MarriedTenYears { get; set; }
        public DateTime? DateOfDivorce { get; set; }
        public DateTime? DateSpouseDeath { get; set; }
        public bool? CareForSurvivingSpouse { get; set; }
        public bool? CreateInheritance { get; set; }
        public bool? DontDisinherit { get; set; }
        public DateTime DateCreated { get; set; }
        public bool? IsCitizen { get; set; }
        public bool? IsMilitary { get; set; }
        public bool? ActiveService { get; set; }
        public bool? Discharge { get; set; }
        public bool? HasBusiness { get; set; }
        public bool? HasOtherIncome { get; set; }
        public bool? IsXspouse { get; set; }
    }
}
