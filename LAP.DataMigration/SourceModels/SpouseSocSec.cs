﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class SpouseSocSec
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? Eligible { get; set; }
        public bool? CurrentlyReceiving { get; set; }
        public string AgeToStart { get; set; }
        public string Estimate { get; set; }
        public bool? HavePension { get; set; }
        public string SurvivorChildAmt { get; set; }
        public bool? Exempt { get; set; }
        public string ExemptionOption { get; set; }
        public string ExemptionDescription { get; set; }
        public DateTime DateCreated { get; set; }
        public bool? IsXspouse { get; set; }
    }
}
