﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class Company
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
