﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorCompFile
    {
        public int FileId { get; set; }
        public int AdvisorId { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public byte[] FileData { get; set; }
        public string Status { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime DateUploaded { get; set; }
        public string Notes { get; set; }
        public int? UpdatedBy { get; set; }
        public string AdvisorNotes { get; set; }
        public int? CreatedBy { get; set; }
    }
}
