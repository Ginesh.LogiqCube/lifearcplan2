﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ImportantConcernsSpouseDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? ItemId { get; set; }
        public int? TypeId { get; set; }
        public bool? PreRetirement { get; set; }
        public string PreImportance { get; set; }
        public bool? Retirement { get; set; }
        public string RetImportance { get; set; }
    }
}
