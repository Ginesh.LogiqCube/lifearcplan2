﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class Notification
    {
        public int Id { get; set; }
        public int? AdvisorId { get; set; }
        public int? ClientId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateInactivated { get; set; }
    }
}
