﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class SpouseRiskTolerance
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int SectionId { get; set; }
        public string ItemName { get; set; }
        public int? Value { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
