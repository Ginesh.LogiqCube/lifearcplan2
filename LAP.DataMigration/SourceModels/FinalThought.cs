﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class FinalThought
    {
        public int Id { get; set; }
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public string Client { get; set; }
        public string Spouse { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
    }
}
