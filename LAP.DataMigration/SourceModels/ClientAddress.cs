﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientAddress
    {
        public int AddressId { get; set; }
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Pobox { get; set; }
        public string HomePhone { get; set; }
        public string CellOne { get; set; }
        public string CellTwo { get; set; }
        public string EmailOne { get; set; }
        public string EmailTwo { get; set; }
        public DateTime DateCreated { get; set; }
        public string Country { get; set; }
        public string Address2 { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
    }
}
