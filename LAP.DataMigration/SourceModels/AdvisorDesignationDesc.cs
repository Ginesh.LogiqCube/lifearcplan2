﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorDesignationDesc
    {
        public int Id { get; set; }
        public string DesignationId { get; set; }
        public string DesignationFullName { get; set; }
        public string Desription { get; set; }
    }
}
