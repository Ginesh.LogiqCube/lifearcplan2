﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorWork
    {
        public int Id { get; set; }
        public int AdvisorId { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string FromYear { get; set; }
        public string ToYear { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
