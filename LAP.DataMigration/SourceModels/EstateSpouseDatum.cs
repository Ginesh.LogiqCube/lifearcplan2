﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class EstateSpouseDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? EstateItemId { get; set; }
    }
}
