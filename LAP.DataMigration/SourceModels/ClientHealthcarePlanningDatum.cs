﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientHealthcarePlanningDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? HealthEmpPlan { get; set; }
        public bool? HealthExchangePlan { get; set; }
        public bool? HealthUninsured { get; set; }
        public bool? HealthMedicare { get; set; }
        public bool? HealthMedicareAdv { get; set; }
        public bool? HealthExchangeOver { get; set; }
        public bool? HealthEmployerPlanOver { get; set; }
        public bool? HealthUninsuredOver { get; set; }
        public bool? HealthHaveMedigap { get; set; }
        public string MedigapLetter { get; set; }
        public string MedigapCost { get; set; }
        public string MedigapCostPeriod { get; set; }
        public bool? HaveLongTermCare { get; set; }
        public string LongTermCareAt { get; set; }
        public string LongTermBeginPay { get; set; }
        public string HaveElimPeriod { get; set; }
        public string ElimValue { get; set; }
        public string HowLongLtcilastsQuestion { get; set; }
        public string BenefitsDuration { get; set; }
        public string LtcipayoutWhen { get; set; }
        public string HaveInflationProtection { get; set; }
        public string InflationProvision { get; set; }
        public string Ltcibenefits { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
