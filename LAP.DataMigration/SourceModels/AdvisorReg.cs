﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorReg
    {
        public int Id { get; set; }
        [ForeignKey("AdvisorLogin")]
        public int AdvisorId { get; set; }
        public string RegItem { get; set; }
        public string Answer { get; set; }
        public DateTime DateCreated { get; set; }
        public AdvisorLogin AdvisorLogin { get; set; }
    }
}
