﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ChatDatum
    {
        public int Id { get; set; }
        public int? ClientId { get; set; }
        public int? AdvisorId { get; set; }
        public string Message { get; set; }
        public string Direction { get; set; }
        public DateTime? MessageDateTime { get; set; }
        public bool? HasBeenSeenClient { get; set; }
        public bool? HasBeenSeenAdviser { get; set; }
    }
}
