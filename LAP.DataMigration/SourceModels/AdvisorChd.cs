﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorChd
    {
        public int Id { get; set; }
        public int AdvisorId { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
