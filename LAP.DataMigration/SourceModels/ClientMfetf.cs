﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientMfetf
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Type { get; set; }
        public string Owner { get; set; }
        public string TaxClass { get; set; }
        public string Symbol { get; set; }
        public string Balance { get; set; }
        public string Owned { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
