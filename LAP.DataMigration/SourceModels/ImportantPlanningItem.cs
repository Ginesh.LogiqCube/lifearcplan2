﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ImportantPlanningItem
    {
        public int ItemId { get; set; }
        public string ItemDescription { get; set; }
        public string StartView { get; set; }
        public string EndView { get; set; }
        public int? SortOrder { get; set; }
        public bool? IsActive { get; set; }
    }
}
