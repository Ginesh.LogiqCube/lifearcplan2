﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class HealthAssessmentSpouseDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? HealthAssessmentItemId { get; set; }
    }
}
