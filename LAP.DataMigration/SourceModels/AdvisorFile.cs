﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorFile
    {
        public int FileId { get; set; }
        public int AdvisorId { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public byte[] FileData { get; set; }
        public DateTime DateUploaded { get; set; }
    }
}
