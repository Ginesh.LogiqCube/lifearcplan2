﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientTaxDatum
    {
        public int Id { get; set; }
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public string Status { get; set; }
        public bool? SpouseAnswer { get; set; }
        public string SpouseYear { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
    }
}
