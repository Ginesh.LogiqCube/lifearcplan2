﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientAnn
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? ItemNumber { get; set; }
        public string Type { get; set; }
        public string Owner { get; set; }
        public string Annuitant { get; set; }
        public string TaxClass { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string Rate { get; set; }
        public string IssueDate { get; set; }
        public string Duration { get; set; }
        public string DurationType { get; set; }
        public bool? Income { get; set; }
        public string IncomeAmount { get; set; }
        public DateTime DateCreated { get; set; }
        public string AnnualContribution { get; set; }
        public string AccountName { get; set; }
    }
}
