﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorAct
    {
        public int Id { get; set; }
        public int AdvisorId { get; set; }
        public string ActivityName { get; set; }
        public string Description { get; set; }
        public bool? InvRelated { get; set; }
        public string DateStarted { get; set; }
        public bool? Active { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal? HoursSpent { get; set; }
    }
}
