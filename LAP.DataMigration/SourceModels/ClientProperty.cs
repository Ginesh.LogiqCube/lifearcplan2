﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientProperty
    {
        public int PropertyId { get; set; }
        public int ClientId { get; set; }
        public string PropertyType { get; set; }
        public int? PricePaid { get; set; }
        public int? Value { get; set; }
        public int? AmountOwed { get; set; }
        public string Owners { get; set; }
        public int? IncomeGenerated { get; set; }
        public string Plans { get; set; }
        public DateTime DateCreated { get; set; }
        public int? ProjectedFairMarketValueRetirement { get; set; }
        public int? AmountOfOwnership { get; set; }
    }
}
