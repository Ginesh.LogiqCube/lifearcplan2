﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientFile
    {
        public int ClientFileId { get; set; }
        public int ClientId { get; set; }
        public string FileType { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public byte[] FileData { get; set; }
        public DateTime DateUploaded { get; set; }
    }
}
