﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientLife
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? ItemNumber { get; set; }
        public string Type { get; set; }
        public string DateOfIssue { get; set; }
        public string Owner { get; set; }
        public string Insured { get; set; }
        public string FaceAmount { get; set; }
        public string Premium { get; set; }
        public string CashValue { get; set; }
        public string Duration { get; set; }
        public string DurationType { get; set; }
        public bool? Income { get; set; }
        public string IncomeAmount { get; set; }
        public DateTime DateCreated { get; set; }
        public string AccountName { get; set; }
    }
}
