﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ImportantPlanningItemsClientDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? ItemId { get; set; }
        public bool? Selected { get; set; }
        public string Importance { get; set; }
    }
}
