﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class SpouseOccupation
    {
        public int OccupationId { get; set; }
        public int ClientId { get; set; }
        public int? OccupationNbr { get; set; }
        public string Status { get; set; }
        public string Occupation { get; set; }
        public string Employer { get; set; }
        public string AnnualEarnings { get; set; }
        public bool? CompanyRetirementPlan { get; set; }
        public bool? SelfRetirementPlan { get; set; }
        public string Kcontribution { get; set; }
        public string KcontributionPeriod { get; set; }
        public string KMatch { get; set; }
        public string KmatchPeriod { get; set; }
        public string Kbalance { get; set; }
        public bool? CompanyPensionPlan { get; set; }
        public string PensionAge { get; set; }
        public string PensionAmt { get; set; }
        public string PensionSurvivor { get; set; }
        public DateTime DateCreated { get; set; }
        public string AgeToRetire { get; set; }
        public bool? HasSecondOccupation { get; set; }
        public string EmpType { get; set; }
        public string Industry { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string K1earnings { get; set; }
        public string SchCearnings { get; set; }
        public string TenNineNineEarnings { get; set; }
        public string TenYearExpectedAnnualIncome { get; set; }
        public string CurrentBalance { get; set; }
        public string AvgRateOfReturn { get; set; }
        public bool? HasPreEmpPlan { get; set; }
        public string Amount { get; set; }
        public string Frequency { get; set; }
        public bool? IsReceiveIncome { get; set; }
        public string SepcurrentBalance { get; set; }
        public string SeprateOfReturn { get; set; }
        public string Sepcontribution { get; set; }
        public string RetiredAge { get; set; }
        public string KavgRateOfReturn { get; set; }
    }
}
