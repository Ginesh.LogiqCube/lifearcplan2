﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class BudgetItem
    {
        public int BudgetItemId { get; set; }
        public string BudgetItemDescription { get; set; }
        public bool? IsActive { get; set; }
        public string SectionHeader { get; set; }
        public int? SortOrder { get; set; }
    }
}
