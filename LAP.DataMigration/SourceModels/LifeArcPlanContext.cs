﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class LifeArcPlanContext : DbContext
    {
        public LifeArcPlanContext()
        {
        }

        public LifeArcPlanContext(DbContextOptions<LifeArcPlanContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AdminLogin> AdminLogins { get; set; }
        public virtual DbSet<AdvisorAct> AdvisorActs { get; set; }
        public virtual DbSet<AdvisorChd> AdvisorChds { get; set; }
        public virtual DbSet<AdvisorCivic> AdvisorCivics { get; set; }
        public virtual DbSet<AdvisorComp> AdvisorComps { get; set; }
        public virtual DbSet<AdvisorCompFile> AdvisorCompFiles { get; set; }
        public virtual DbSet<AdvisorDatum> AdvisorData { get; set; }
        public virtual DbSet<AdvisorDesignation> AdvisorDesignations { get; set; }
        public virtual DbSet<AdvisorDesignationDesc> AdvisorDesignationDescs { get; set; }
        public virtual DbSet<AdvisorEdu> AdvisorEdus { get; set; }
        public virtual DbSet<AdvisorFile> AdvisorFiles { get; set; }
        public virtual DbSet<AdvisorLocation> AdvisorLocations { get; set; }
        public virtual DbSet<AdvisorLogin> AdvisorLogins { get; set; }
        public virtual DbSet<AdvisorReg> AdvisorRegs { get; set; }
        public virtual DbSet<AdvisorWork> AdvisorWorks { get; set; }
        public virtual DbSet<BudgetClientDatum> BudgetClientData { get; set; }
        public virtual DbSet<BudgetItem> BudgetItems { get; set; }
        public virtual DbSet<CharitableClientDatum> CharitableClientData { get; set; }
        public virtual DbSet<ChatDatum> ChatData { get; set; }
        public virtual DbSet<ClientAcct1> ClientAcct1s { get; set; }
        public virtual DbSet<ClientAdditionalAnnuityDatum> ClientAdditionalAnnuityData { get; set; }
        public virtual DbSet<ClientAdditionalLifeDatum> ClientAdditionalLifeData { get; set; }
        public virtual DbSet<ClientAddress> ClientAddresses { get; set; }
        public virtual DbSet<ClientAnn> ClientAnns { get; set; }
        public virtual DbSet<ClientBrokerageAcct> ClientBrokerageAccts { get; set; }
        public virtual DbSet<ClientBudgetItem> ClientBudgetItems { get; set; }
        public virtual DbSet<ClientBusiness> ClientBusinesses { get; set; }
        public virtual DbSet<ClientBusinessHeir> ClientBusinessHeirs { get; set; }
        public virtual DbSet<ClientChild> ClientChildren { get; set; }
        public virtual DbSet<ClientDatum> ClientData { get; set; }
        public virtual DbSet<ClientEducationalPlanningDatum> ClientEducationalPlanningData { get; set; }
        public virtual DbSet<ClientFile> ClientFiles { get; set; }
        public virtual DbSet<ClientFinancialPlanningDatum> ClientFinancialPlanningData { get; set; }
        public virtual DbSet<ClientHasAcctDatum> ClientHasAcctData { get; set; }
        public virtual DbSet<ClientHealthcarePlanningDatum> ClientHealthcarePlanningData { get; set; }
        public virtual DbSet<ClientImportanceItem> ClientImportanceItems { get; set; }
        public virtual DbSet<ClientImportantConcernItem> ClientImportantConcernItems { get; set; }
        public virtual DbSet<ClientLife> ClientLives { get; set; }
        public virtual DbSet<ClientLogin> ClientLogins { get; set; }
        public virtual DbSet<ClientMfetf> ClientMfetfs { get; set; }
        public virtual DbSet<ClientOccupation> ClientOccupations { get; set; }
        public virtual DbSet<ClientOccupationCorm> ClientOccupationCorms { get; set; }
        public virtual DbSet<ClientPersonalCareDatum> ClientPersonalCareData { get; set; }
        public virtual DbSet<ClientProperty> ClientProperties { get; set; }
        public virtual DbSet<ClientPropertyQuestion> ClientPropertyQuestions { get; set; }
        public virtual DbSet<ClientRiskTolerance> ClientRiskTolerances { get; set; }
        public virtual DbSet<ClientSocSec> ClientSocSecs { get; set; }
        public virtual DbSet<ClientTaxDatum> ClientTaxData { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<EduDatum> EduData { get; set; }
        public virtual DbSet<EstateClientDatum> EstateClientData { get; set; }
        public virtual DbSet<EstateItem> EstateItems { get; set; }
        public virtual DbSet<EstateSpouseDatum> EstateSpouseData { get; set; }
        public virtual DbSet<FinalThought> FinalThoughts { get; set; }
        public virtual DbSet<HealthAssessmentClientDatum> HealthAssessmentClientData { get; set; }
        public virtual DbSet<HealthAssessmentItem> HealthAssessmentItems { get; set; }
        public virtual DbSet<HealthAssessmentSpouseDatum> HealthAssessmentSpouseData { get; set; }
        public virtual DbSet<ImportantConcernsClientDatum> ImportantConcernsClientData { get; set; }
        public virtual DbSet<ImportantConcernsSpouseDatum> ImportantConcernsSpouseData { get; set; }
        public virtual DbSet<ImportantItemsClientDatum> ImportantItemsClientData { get; set; }
        public virtual DbSet<ImportantItemsSpouseDatum> ImportantItemsSpouseData { get; set; }
        public virtual DbSet<ImportantPlanningItem> ImportantPlanningItems { get; set; }
        public virtual DbSet<ImportantPlanningItemsClientDatum> ImportantPlanningItemsClientData { get; set; }
        public virtual DbSet<InvestLite> InvestLites { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<OtherIncome> OtherIncomes { get; set; }
        public virtual DbSet<RetirementExpectation> RetirementExpectations { get; set; }
        public virtual DbSet<SpouseDatum> SpouseData { get; set; }
        public virtual DbSet<SpouseFinancialPlanningDatum> SpouseFinancialPlanningData { get; set; }
        public virtual DbSet<SpouseOccupation> SpouseOccupations { get; set; }
        public virtual DbSet<SpouseOtherIncome> SpouseOtherIncomes { get; set; }
        public virtual DbSet<SpouseRiskTolerance> SpouseRiskTolerances { get; set; }
        public virtual DbSet<SpouseSocSec> SpouseSocSecs { get; set; }
        public virtual DbSet<State> States { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=lifearcproduction.cg93hfajhduh.us-east-1.rds.amazonaws.com;Initial Catalog=LifeArcPlan;User ID=lifearc;Password=$RDS#2021!;MultipleActiveResultSets=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AdminLogin>(entity =>
            {
                entity.ToTable("AdminLogin");

                entity.Property(e => e.AdminLoginId).HasColumnName("AdminLoginID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastLoginDateTime).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleInitial)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorAct>(entity =>
            {
                entity.ToTable("AdvisorAct");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActivityName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateStarted)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoursSpent).HasColumnType("decimal(4, 1)");
            });

            modelBuilder.Entity<AdvisorChd>(entity =>
            {
                entity.ToTable("AdvisorChd");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorCivic>(entity =>
            {
                entity.ToTable("AdvisorCivic");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActivityName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorComp>(entity =>
            {
                entity.ToTable("AdvisorComp");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CompType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ContactName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiteUrl)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("SiteURL");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorCompFile>(entity =>
            {
                entity.HasKey(e => e.FileId);

                entity.ToTable("AdvisorCompFile");

                entity.Property(e => e.FileId).HasColumnName("FileID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.AdvisorNotes)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DateApproved).HasColumnType("datetime");

                entity.Property(e => e.DateUploaded).HasColumnType("datetime");

                entity.Property(e => e.FileName)
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.FileType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MimeType)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorDatum>(entity =>
            {
                entity.HasKey(e => e.AdvisorId);

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.Address)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AdvisorLoginId).HasColumnName("AdvisorLoginID");

                entity.Property(e => e.AptNbr)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Bdid).HasColumnName("BDID");

                entity.Property(e => e.Bio1).IsUnicode(false);

                entity.Property(e => e.BusinessEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CardNbr)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CellPhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Designations)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Dob)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("DOB");

                entity.Property(e => e.Fedatabase)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FEDatabase");

                entity.Property(e => e.FeeType)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Fmoid).HasColumnName("FMOID");

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Licenses)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleInitial)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Pobox)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("POBox");

                entity.Property(e => e.PracticeName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RecordId)
                    .HasMaxLength(128)
                    .HasColumnName("RecordID");

                entity.Property(e => e.RedTailApikey)
                    .IsUnicode(false)
                    .HasColumnName("RedTailAPIKey");

                entity.Property(e => e.RedTailDb)
                    .IsUnicode(false)
                    .HasColumnName("RedTailDB");

                entity.Property(e => e.Ria)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("RIA");

                entity.Property(e => e.Riaid).HasColumnName("RIAID");

                entity.Property(e => e.Role).IsUnicode(false);

                entity.Property(e => e.RoleOther).IsUnicode(false);

                entity.Property(e => e.Salutation)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.SpouseFname)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SpouseFName");

                entity.Property(e => e.SpouseLname)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SpouseLName");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Wbapikey)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("WBAPIKey");

                entity.Property(e => e.Zip)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorDesignation>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.Designation)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorDesignationDesc>(entity =>
            {
                entity.ToTable("AdvisorDesignationDesc");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DesignationFullName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DesignationId)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("DesignationID");

                entity.Property(e => e.Desription).IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorEdu>(entity =>
            {
                entity.ToTable("AdvisorEdu");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Degree)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Field)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FromYear)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ToYear)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorFile>(entity =>
            {
                entity.HasKey(e => e.FileId);

                entity.ToTable("AdvisorFile");

                entity.Property(e => e.FileId).HasColumnName("FileID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.DateUploaded).HasColumnType("datetime");

                entity.Property(e => e.FileData).IsRequired();

                entity.Property(e => e.FileName)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.FileType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MimeType)
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorLocation>(entity =>
            {
                entity.HasKey(e => e.LocationId);

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Address1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Phone1).HasMaxLength(20);

                entity.Property(e => e.Phone2).HasMaxLength(20);

                entity.Property(e => e.Phone3).HasMaxLength(20);

                entity.Property(e => e.State).HasMaxLength(2);

                entity.Property(e => e.Zip).HasMaxLength(10);
            });

            modelBuilder.Entity<AdvisorLogin>(entity =>
            {
                entity.ToTable("AdvisorLogin");

                entity.Property(e => e.AdvisorLoginId).HasColumnName("AdvisorLoginID");

                entity.Property(e => e.AdvisorMasterId).HasColumnName("AdvisorMasterID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastLoginDateTime).HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleInitial)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.RecordIndicator)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(120)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorReg>(entity =>
            {
                entity.ToTable("AdvisorReg");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.Answer)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.RegItem)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvisorWork>(entity =>
            {
                entity.ToTable("AdvisorWork");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.FromYear)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ToYear)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BudgetClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Balance)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Duration)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.DurationType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Increase)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.Period)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BudgetItem>(entity =>
            {
                entity.Property(e => e.BudgetItemId).HasColumnName("BudgetItemID");

                entity.Property(e => e.BudgetItemDescription)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SectionHeader)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CharitableClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ChatDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Direction)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Message)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.MessageDateTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientAcct1>(entity =>
            {
                entity.ToTable("ClientAcct1");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientAdditionalAnnuityDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientAdditionalLifeDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientAddress>(entity =>
            {
                entity.HasKey(e => e.AddressId)
                    .HasName("PK_Address");

                entity.ToTable("ClientAddress");

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Country).IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Pobox).HasColumnName("POBox");
            });

            modelBuilder.Entity<ClientAnn>(entity =>
            {
                entity.ToTable("ClientAnn");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientBrokerageAcct>(entity =>
            {
                entity.ToTable("ClientBrokerageAcct");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientBudgetItem>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BudgetItemId).HasColumnName("BudgetItemID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Inflation)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientBusiness>(entity =>
            {
                entity.ToTable("ClientBusiness");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.EnteredBy)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Fmv).HasColumnName("FMV");
            });

            modelBuilder.Entity<ClientBusinessHeir>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BusinessId).HasColumnName("BusinessID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientChild>(entity =>
            {
                entity.HasKey(e => e.ChildId);

                entity.Property(e => e.ChildId).HasColumnName("ChildID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.HeirDob).HasColumnName("HeirDOB");
            });

            modelBuilder.Entity<ClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cfid)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("CFID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateOfCurrentMarriage).HasColumnType("datetime");

                entity.Property(e => e.DateOfDivorce).HasColumnType("datetime");

                entity.Property(e => e.DateSpouseDeath).HasColumnType("datetime");

                entity.Property(e => e.Financially).HasColumnName("financially");

                entity.Property(e => e.Rtid).HasColumnName("RTID");

                entity.Property(e => e.SendToCf).HasColumnName("SendToCF");

                entity.Property(e => e.SendToFe).HasColumnName("SendToFE");

                entity.Property(e => e.SendToRt).HasColumnName("SendToRT");

                entity.Property(e => e.SendToSf).HasColumnName("SendToSF");

                entity.Property(e => e.SendToWb).HasColumnName("SendToWB");

                entity.Property(e => e.SentToRt).HasColumnName("SentToRT");

                entity.Property(e => e.Sfid)
                    .HasMaxLength(50)
                    .HasColumnName("SFID");

                entity.Property(e => e.Wbid).HasColumnName("WBID");
            });

            modelBuilder.Entity<ClientEducationalPlanningDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Age)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Cost)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Duration)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SavingType)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Years)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientFile>(entity =>
            {
                entity.ToTable("ClientFile");

                entity.Property(e => e.ClientFileId).HasColumnName("ClientFileID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateUploaded).HasColumnType("datetime");

                entity.Property(e => e.FileData).IsRequired();

                entity.Property(e => e.FileName)
                    .HasMaxLength(128)
                    .IsUnicode(false);

                entity.Property(e => e.FileType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MimeType)
                    .HasMaxLength(128)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientFinancialPlanningDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Age)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Bene)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.BucketList)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Income)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientHasAcctDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientHealthcarePlanningDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BenefitsDuration)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ElimValue)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.HaveElimPeriod)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HaveInflationProtection)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongLtcilastsQuestion)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("HowLongLTCILastsQuestion");

                entity.Property(e => e.InflationProvision)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.LongTermBeginPay)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LongTermCareAt)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ltcibenefits)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("LTCIBenefits");

                entity.Property(e => e.LtcipayoutWhen)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("LTCIPayoutWhen");

                entity.Property(e => e.MedigapCost)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MedigapCostPeriod)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MedigapLetter)
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientImportanceItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<ClientImportantConcernItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<ClientLife>(entity =>
            {
                entity.ToTable("ClientLife");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientLogin>(entity =>
            {
                entity.ToTable("ClientLogin");

                entity.Property(e => e.ClientLoginId).HasColumnName("ClientLoginID");

                entity.Property(e => e.AdvisorLoginId).HasColumnName("AdvisorLoginID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.LastLoginDateTime).HasColumnType("datetime");

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.RecordIndicator)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(120)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientMfetf>(entity =>
            {
                entity.ToTable("ClientMFETF");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientOccupation>(entity =>
            {
                entity.HasKey(e => e.OccupationId);

                entity.ToTable("ClientOccupation");

                entity.Property(e => e.OccupationId).HasColumnName("OccupationID");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AgeToRetire)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Amount)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AnnualEarnings)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AvgRateOfReturn)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CurrentBalance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.EmpType)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Employer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Frequency)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Industry)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.K1earnings)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("K1Earnings");

                entity.Property(e => e.KMatch)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("kMatch");

                entity.Property(e => e.KavgRateOfReturn)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KAvgRateOfReturn");

                entity.Property(e => e.Kbalance)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KBalance");

                entity.Property(e => e.Kcontribution)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KContribution");

                entity.Property(e => e.KcontributionPeriod)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KContributionPeriod");

                entity.Property(e => e.KmatchPeriod)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KMatchPeriod");

                entity.Property(e => e.Occupation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PensionAge)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PensionAmt)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PensionSurvivor)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RetiredAge)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SchCearnings)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("SchCEarnings");

                entity.Property(e => e.Sepcontribution)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SEPContribution");

                entity.Property(e => e.SepcurrentBalance)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SEPCurrentBalance");

                entity.Property(e => e.SeprateOfReturn)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SEPRateOfReturn");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TenNineNineEarnings)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TenYearExpectedAnnualIncome).HasMaxLength(50);

                entity.Property(e => e.Zip)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientOccupationCorm>(entity =>
            {
                entity.ToTable("ClientOccupationCORM");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ChurchAllowance)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ChurchLivingArrangements)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.MilitaryAllowanceAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MilitaryLivingArrangements)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientPersonalCareDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CareOption)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Caregiver)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Caregivername)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Relationship)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientProperty>(entity =>
            {
                entity.HasKey(e => e.PropertyId);

                entity.ToTable("ClientProperty");

                entity.Property(e => e.PropertyId).HasColumnName("PropertyID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Owners)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Plans)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PropertyType)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientPropertyQuestion>(entity =>
            {
                entity.HasKey(e => e.PropertyQuestionId);

                entity.Property(e => e.PropertyQuestionId).HasColumnName("PropertyQuestionID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.PropertyInPortfolio)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PropertyPlans)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientRiskTolerance>(entity =>
            {
                entity.ToTable("ClientRiskTolerance");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ItemName).IsUnicode(false);

                entity.Property(e => e.SectionId).HasColumnName("SectionID");
            });

            modelBuilder.Entity<ClientSocSec>(entity =>
            {
                entity.ToTable("ClientSocSec");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AgeToStart)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Estimate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExemptionDescription)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExemptionOption)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SurvivorChildAmt)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientTaxDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.ToTable("Company");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EduDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DegreeDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DegreeId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("DegreeID");

                entity.Property(e => e.FieldDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FieldId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("FieldID");
            });

            modelBuilder.Entity<EstateClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EstateItemId).HasColumnName("EstateItemID");
            });

            modelBuilder.Entity<EstateItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EstateSpouseDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.EstateItemId).HasColumnName("EstateItemID");
            });

            modelBuilder.Entity<FinalThought>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Client)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Spouse)
                    .HasMaxLength(2000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthAssessmentClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.HealthAssessmentItemId).HasColumnName("HealthAssessmentItemID");
            });

            modelBuilder.Entity<HealthAssessmentItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HealthAssessmentSpouseDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.HealthAssessmentItemId).HasColumnName("HealthAssessmentItemID");
            });

            modelBuilder.Entity<ImportantConcernsClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.PreImportance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RetImportance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<ImportantConcernsSpouseDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.PreImportance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RetImportance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<ImportantItemsClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<ImportantItemsSpouseDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.TypeId).HasColumnName("TypeID");
            });

            modelBuilder.Entity<ImportantPlanningItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.EndView)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.StartView)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ImportantPlanningItemsClientDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Importance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");
            });

            modelBuilder.Entity<InvestLite>(entity =>
            {
                entity.ToTable("InvestLite");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.EnteredBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Message1)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("Message");

                entity.Property(e => e.Riaid).HasColumnName("RIAID");

                entity.Property(e => e.ShowUntil).HasColumnType("datetime");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdvisorId).HasColumnName("AdvisorID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateInactivated).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OtherIncome>(entity =>
            {
                entity.ToTable("OtherIncome");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasMaxLength(10);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DurationType).HasMaxLength(10);

                entity.Property(e => e.OtherDescription).HasMaxLength(100);

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<RetirementExpectation>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemDescription)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SpouseDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateOfDivorce).HasColumnType("datetime");

                entity.Property(e => e.DateSpouseDeath).HasColumnType("datetime");

                entity.Property(e => e.IsXspouse).HasColumnName("isXSpouse");
            });

            modelBuilder.Entity<SpouseFinancialPlanningDatum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Age)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Bene)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Income)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SpouseOccupation>(entity =>
            {
                entity.HasKey(e => e.OccupationId);

                entity.ToTable("SpouseOccupation");

                entity.Property(e => e.OccupationId).HasColumnName("OccupationID");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AgeToRetire)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Amount)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AnnualEarnings)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AvgRateOfReturn)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CurrentBalance)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.EmpType)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Employer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Frequency)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Industry)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.K1earnings)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("K1Earnings");

                entity.Property(e => e.KMatch)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("kMatch");

                entity.Property(e => e.KavgRateOfReturn)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KAvgRateOfReturn");

                entity.Property(e => e.Kbalance)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KBalance");

                entity.Property(e => e.Kcontribution)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KContribution");

                entity.Property(e => e.KcontributionPeriod)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KContributionPeriod");

                entity.Property(e => e.KmatchPeriod)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("KMatchPeriod");

                entity.Property(e => e.Occupation)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PensionAge)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PensionAmt)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PensionSurvivor)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RetiredAge)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SchCearnings)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("SchCEarnings");

                entity.Property(e => e.Sepcontribution)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SEPContribution");

                entity.Property(e => e.SepcurrentBalance)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SEPCurrentBalance");

                entity.Property(e => e.SeprateOfReturn)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("SEPRateOfReturn");

                entity.Property(e => e.State)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TenNineNineEarnings)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TenYearExpectedAnnualIncome).HasMaxLength(50);

                entity.Property(e => e.Zip)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SpouseOtherIncome>(entity =>
            {
                entity.ToTable("SpouseOtherIncome");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Amount).HasMaxLength(10);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DurationType).HasMaxLength(10);

                entity.Property(e => e.OtherDescription).HasMaxLength(100);

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<SpouseRiskTolerance>(entity =>
            {
                entity.ToTable("SpouseRiskTolerance");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.ItemName).IsUnicode(false);

                entity.Property(e => e.SectionId).HasColumnName("SectionID");
            });

            modelBuilder.Entity<SpouseSocSec>(entity =>
            {
                entity.ToTable("SpouseSocSec");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AgeToStart)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Estimate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ExemptionDescription)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExemptionOption)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsXspouse).HasColumnName("isXSpouse");

                entity.Property(e => e.SurvivorChildAmt)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("State");

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.Abbreviation)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
