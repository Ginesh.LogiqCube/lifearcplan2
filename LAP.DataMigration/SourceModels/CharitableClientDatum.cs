﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class CharitableClientDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? Item1 { get; set; }
        public bool? Item2 { get; set; }
        public bool? Item3 { get; set; }
        public bool? Item4 { get; set; }
        public bool? Item5 { get; set; }
        public bool? Item6 { get; set; }
        public bool? Item7 { get; set; }
        public bool? Item8 { get; set; }
        public bool? Item9 { get; set; }
        public bool? Item10 { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
