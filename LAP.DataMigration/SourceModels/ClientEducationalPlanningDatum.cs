﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientEducationalPlanningDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? Selected { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }
        public string Duration { get; set; }
        public string Type { get; set; }
        public string Years { get; set; }
        public string SavingType { get; set; }
        public string Cost { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
