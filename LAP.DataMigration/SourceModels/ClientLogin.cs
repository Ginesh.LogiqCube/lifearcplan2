﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientLogin
    {
        public int ClientLoginId { get; set; }
        public int AdvisorLoginId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? LastLoginDateTime { get; set; }
        public bool? InitialLoginComplete { get; set; }
        public string RecordIndicator { get; set; }
        public int? CreatedBy { get; set; }
        public virtual ClientDatum ClientDatum { get; set; }
        public virtual ClientTaxDatum ClientTaxDatum { get; set; }
        public virtual FinalThought FinalThought { get; set; }
        public virtual ClientAddress ClientAddress { get; set; }
    }
}
