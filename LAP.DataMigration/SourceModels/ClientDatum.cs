﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientDatum
    {
        public int Id { get; set; }
        [ForeignKey("ClientLogin")]
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime? DateOfCurrentMarriage { get; set; }
        public bool? Prenup { get; set; }
        public bool? SeparatedInState { get; set; }
        public bool? PreviousDivorce { get; set; }
        public bool? PreviousWidow { get; set; }
        public bool? MarriedTenYears { get; set; }
        public DateTime? DateOfDivorce { get; set; }
        public DateTime? DateSpouseDeath { get; set; }
        public bool? CareForSurvivingSpouse { get; set; }
        public bool? CreateInheritance { get; set; }
        public bool? DontDisinherit { get; set; }
        public bool? SpecialNeedsChild { get; set; }
        public bool? SpecialNeedsTrust { get; set; }
        public bool? ChildSupportOrder { get; set; }
        public DateTime DateCreated { get; set; }
        public bool? HasOtherIncome { get; set; }
        public bool? IsCitizen { get; set; }
        public bool? CurrentMarriedTenYears { get; set; }
        public bool? IsMilitary { get; set; }
        public bool? ActiveService { get; set; }
        public bool? Discharge { get; set; }
        public bool? IsAdvisorAssisted { get; set; }
        public bool? SentToRt { get; set; }
        public bool? SendToRt { get; set; }
        public bool? SendToFe { get; set; }
        public int? Income { get; set; }
        public int? Expenses { get; set; }
        public bool? FullVersion { get; set; }
        public bool? HasBusiness { get; set; }
        public int? Rtid { get; set; }
        public bool? SendToWb { get; set; }
        public int? Wbid { get; set; }
        public bool? SendToSf { get; set; }
        public string Sfid { get; set; }
        public bool? Financially { get; set; }
        public bool? SendToCf { get; set; }
        public string Cfid { get; set; }
        public virtual ClientLogin ClientLogin { get; set; }
    }
}
