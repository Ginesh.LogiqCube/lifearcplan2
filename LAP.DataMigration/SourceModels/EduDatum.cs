﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class EduDatum
    {
        public int Id { get; set; }
        public string DegreeId { get; set; }
        public string DegreeDesc { get; set; }
        public string FieldId { get; set; }
        public string FieldDesc { get; set; }
    }
}
