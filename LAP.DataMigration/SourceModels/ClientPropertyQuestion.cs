﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientPropertyQuestion
    {
        public int PropertyQuestionId { get; set; }
        public int ClientId { get; set; }
        public string PropertyPlans { get; set; }
        public string PropertyInPortfolio { get; set; }
        public bool? HasInvestmentProperty { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
