﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientChild
    {
        public int ChildId { get; set; }
        public int ClientId { get; set; }
        public string Name { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string ChildOf { get; set; }
        public string Relationship { get; set; }
        public string Inheritance { get; set; }
        public bool? SpecialNeeds { get; set; }
        public DateTime DateCreated { get; set; }
        public string HeirDob { get; set; }
    }
}
