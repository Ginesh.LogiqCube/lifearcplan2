﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientAdditionalLifeDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? ItemNumber { get; set; }
        public string Company { get; set; }
        public string Inforce { get; set; }
        public string Loan { get; set; }
        public string LoanAmount { get; set; }
        public string BeneList { get; set; }
        public string Purpose { get; set; }
        public bool? PurposeChanged { get; set; }
        public string PurposeDescription { get; set; }
        public bool? Dissatisfied { get; set; }
        public string DissatisfiedDescription { get; set; }
        public bool? Understand { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
