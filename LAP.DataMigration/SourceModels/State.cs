﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class State
    {
        public int StateId { get; set; }
        public string Abbreviation { get; set; }
        public string FullName { get; set; }
    }
}
