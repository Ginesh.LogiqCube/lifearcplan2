﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class SpouseFinancialPlanningDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? OnTarget { get; set; }
        public string Income { get; set; }
        public string Bene { get; set; }
        public bool? SocSec { get; set; }
        public string Age { get; set; }
        public bool? Maximized { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
