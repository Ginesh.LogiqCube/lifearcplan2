﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientOccupationCorm
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? Clergy { get; set; }
        public string ChurchLivingArrangements { get; set; }
        public string ChurchAllowance { get; set; }
        public bool? Military { get; set; }
        public bool? MilitaryAllowance { get; set; }
        public string MilitaryAllowanceAmount { get; set; }
        public string MilitaryLivingArrangements { get; set; }
        public bool? MaintainHousing { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
