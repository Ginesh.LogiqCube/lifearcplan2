﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class Message
    {
        public int Id { get; set; }
        public int? Riaid { get; set; }
        public bool? AllUsers { get; set; }
        public string Message1 { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? ShowUntil { get; set; }
        public bool? IsActive { get; set; }
        public string EnteredBy { get; set; }
    }
}
