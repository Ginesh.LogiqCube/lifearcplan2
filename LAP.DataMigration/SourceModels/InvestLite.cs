﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class InvestLite
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? Amt1 { get; set; }
        public int? Amt2 { get; set; }
        public int? Amt3 { get; set; }
        public int? Amt4 { get; set; }
        public int? Amt5 { get; set; }
        public int? Amt6 { get; set; }
    }
}
