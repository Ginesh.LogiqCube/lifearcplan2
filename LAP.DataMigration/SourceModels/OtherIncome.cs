﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class OtherIncome
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Type { get; set; }
        public string OtherDescription { get; set; }
        public string Amount { get; set; }
        public int? Duration { get; set; }
        public string DurationType { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
