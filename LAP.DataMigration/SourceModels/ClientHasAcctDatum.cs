﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientHasAcctDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool? HasAcctType1 { get; set; }
        public bool? HasAcctType2 { get; set; }
        public bool? HasAcctType3 { get; set; }
        public bool? HasAcctType4 { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
