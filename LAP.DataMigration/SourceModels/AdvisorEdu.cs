﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorEdu
    {
        public int Id { get; set; }
        [ForeignKey("AdvisorLogin")]
        public int AdvisorId { get; set; }
        public string SchoolName { get; set; }
        public string Degree { get; set; }
        public string Field { get; set; }
        public string FromYear { get; set; }
        public string ToYear { get; set; }
        public DateTime DateCreated { get; set; }
        public AdvisorLogin AdvisorLogin { get; set; }
    }
}
