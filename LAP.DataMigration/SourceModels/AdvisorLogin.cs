﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorLogin
    {
        public int AdvisorLoginId { get; set; }
        public int CompanyId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? LastLoginDateTime { get; set; }
        public string RecordIndicator { get; set; }
        public bool? InitialLoginComplete { get; set; }
        public int? AdvisorMasterId { get; set; }
        public bool? IsAssistant { get; set; }
        public virtual AdvisorDatum AdvisorDatum { get; set; }
        public virtual List<AdvisorLocation> AdvisorLocations { get; set; }
        public virtual List<AdvisorEdu> AdvisorEdus { get; set; }
    }
}
