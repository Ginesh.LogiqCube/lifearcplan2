﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorDesignation
    {
        public int Id { get; set; }
        public int AdvisorId { get; set; }
        public string Designation { get; set; }
    }
}
