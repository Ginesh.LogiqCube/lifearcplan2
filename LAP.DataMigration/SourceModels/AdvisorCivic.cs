﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorCivic
    {
        public int Id { get; set; }
        public int AdvisorId { get; set; }
        public string ActivityName { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
