﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientBusinessHeir
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int BusinessId { get; set; }
        public string Name { get; set; }
        public string Percent { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
