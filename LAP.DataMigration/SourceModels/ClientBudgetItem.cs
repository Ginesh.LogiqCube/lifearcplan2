﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientBudgetItem
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int? BudgetItemId { get; set; }
        public int? MonthlyAmt { get; set; }
        public string Inflation { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
