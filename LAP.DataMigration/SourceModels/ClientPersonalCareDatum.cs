﻿using System;
using System.Collections.Generic;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class ClientPersonalCareDatum
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string CareOption { get; set; }
        public string Caregiver { get; set; }
        public string Caregivername { get; set; }
        public string Relationship { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
