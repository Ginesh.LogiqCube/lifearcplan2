﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace LAP.DataMigration.SourceModels
{
    public partial class AdvisorDatum
    {
        public int AdvisorId { get; set; }
        public int CompanyId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public string Pobox { get; set; }
        public string AptNbr { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PracticeName { get; set; }
        public string BusinessEmail { get; set; }
        public string FeeType { get; set; }
        public string Designations { get; set; }
        public string Licenses { get; set; }
        public string Bio1 { get; set; }
        public string RedTailApikey { get; set; }
        public string RedTailDb { get; set; }
        public string Role { get; set; }
        public string RoleOther { get; set; }
        public string Salutation { get; set; }
        public int? SubscriptionLevel { get; set; }
        public string Ria { get; set; }
        public bool? IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        [ForeignKey("AdvisorLogin")]
        public int AdvisorLoginId { get; set; }
        public string Dob { get; set; }
        public bool? IsMarried { get; set; }
        public string SpouseFname { get; set; }
        public string SpouseLname { get; set; }
        public bool? HaveChildren { get; set; }
        public int? Fmoid { get; set; }
        public int? Bdid { get; set; }
        public int? Riaid { get; set; }
        public string CardNbr { get; set; }
        public bool? ProfileCompleted { get; set; }
        public string Gender { get; set; }
        public string RecordId { get; set; }
        public bool? IsCompliance { get; set; }
        public string Wbapikey { get; set; }
        public string Fedatabase { get; set; }
    }
}
