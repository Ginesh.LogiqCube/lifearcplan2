﻿//using System;
//using System.Windows.Forms;
//using System.Data;
//using Newtonsoft.Json;
//using LAP.Helper;
//using Microsoft.Data.SqlClient;
//using System.Collections.Generic;
//using LAP;
//using System.IO;
//using System.Globalization;
//using System.Linq;

//namespace DataMigrationApp
//{
//    public class MigrationFun
//    {
//        string SourceConStr = @"Server=lifearcplan.cg93hfajhduh.us-east-1.rds.amazonaws.com;Initial Catalog=LifeArcPlan;User ID=admin;Password=AWSdb0609$;MultipleActiveResultSets=True";
//        //string SourceConStr = @"Server=lifearcproduction.cg93hfajhduh.us-east-1.rds.amazonaws.com;Initial Catalog=LifeArcPlan;User ID=lifearc;Password=AWSdb0609$;MultipleActiveResultSets=True";
//        string DestConStr = @"Server=lifearcplan2.cg93hfajhduh.us-east-1.rds.amazonaws.com;Initial Catalog=lap2-dev;User ID=admin;Password=AWSdb0609$;MultipleActiveResultSets=True";     
//        string getColumnsQuery = "select COLUMN_NAME, DATA_TYPE ,CHARACTER_MAXIMUM_LENGTH from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '{0}'";
//        string selectQuery = "select * from {0}";
//        readonly string selectQuery2 = "select * from {0} where clientid in (select clientloginid from clientlogin)";
//        string identitySetQuery = "SET IDENTITY_INSERT {0} {1};";
//        string mergeQuery = @"MERGE INTO {0} AS A
//                              USING (SELECT * FROM OPENJSON('{1}') WITH ({2})) B
//                              ON (A.Id = B.Id)
//                              WHEN MATCHED THEN
//                              UPDATE SET {3}
//                              WHEN NOT MATCHED THEN
//                              INSERT ({4}) VALUES({5});";
//        CultureInfo cultureUS = new CultureInfo("en-US");
//        CultureInfo cultureGB = new CultureInfo("en-GB");
//        public DataTable GetDataTable(string conStr, string sql)
//        {
//            var resultTable = new DataTable();
//            using (var conn = new SqlConnection(conStr))
//            {
//                using (var cmd = new SqlCommand(sql, conn))
//                {
//                    cmd.CommandTimeout = 600 * 600;
//                    using (var adapter = new SqlDataAdapter(cmd))
//                    {
//                        adapter.Fill(resultTable);
//                    }
//                }
//            }
//            return resultTable;
//        }
//        public string GetColumns(string tableName, bool includetype = false)
//        {
//            DataTable table = GetDataTable(DestConStr, string.Format(getColumnsQuery, tableName));
//            string columns = string.Empty;

//            foreach (DataRow row in table.Rows)
//            {
//                var type = includetype ? $" {row[1]}" : string.Empty;
//                var max = (type.Contains("varchar") && (int)row[2] == -1) ? $"(MAX)" : string.Empty;
//                columns += $"{row[0]}{type}{max},";
//            }

//            columns = columns.TrimEnd(',');
//            return columns;
//        }
//        public string GetColumnUpdateString(string tableName)
//        {
//            DataTable table = GetDataTable(DestConStr, string.Format(getColumnsQuery, tableName));
//            string columns = string.Empty;

//            foreach (DataRow row in table.Rows)
//                if (row[0].ToString() != "Id")
//                    columns += $"A.{row[0]}=B.{row[0]},";
//            columns = columns.TrimEnd(',');
//            return columns;
//        }
//        public string GetColumnValueString(string tableName)
//        {
//            DataTable table = GetDataTable(DestConStr, string.Format(getColumnsQuery, tableName));
//            string columns = string.Empty;

//            foreach (DataRow row in table.Rows)
//                columns += $"B.{row[0]},";
//            columns = columns.TrimEnd(',');
//            return columns;
//        }
//        public void ExecuteSql(string sql)
//        {
//            using (var conn = new SqlConnection(DestConStr))
//            {
//                using (var cmd = new SqlCommand(sql, conn))
//                {
//                    conn.Open();
//                    cmd.ExecuteNonQuery();
//                }
//            }
//        }
//        public void SetColumnsOrder(DataTable table, string columnNames)
//        {
//            int columnIndex = 0;
//            foreach (var columnName in columnNames.Split(','))
//            {
//                table.Columns[columnName].SetOrdinal(columnIndex);
//                columnIndex++;
//            }
//        }
//        public void SaveFileToJson(string content, string filename)
//        {
//            string dirParameter = Path.GetDirectoryName(Application.ExecutablePath) + $@"\{filename}.json";
//            FileStream fParameter = new FileStream(dirParameter, FileMode.Create, FileAccess.Write);
//            StreamWriter m_WriterParameter = new StreamWriter(fParameter);
//            m_WriterParameter.BaseStream.Seek(0, SeekOrigin.End);
//            m_WriterParameter.Write(content);
//            m_WriterParameter.Flush();
//            m_WriterParameter.Close();
//        }
//        public int GetProgress(int current, int total)
//        {
//            var percentage = current * 100 / total;
//            return percentage;
//        }
//        public void Migration()
//        {
//            int total = 6;
//            int cnt = 1;
//            ClientLogin();
//            cnt++;
//            ClientData();
//            BudgetItem();
//            PlanningItem();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //SpouseData();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //XSpouseData();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //ClientAddress();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //Heir();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;

//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //ClientBudgetItem();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;

//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //ClientPlanning();
//            //SetFormLableControl("label4", $"{GetProgress(cnt, total)}% Summary : {cnt}/{total}");
//            //cnt++;
//            //ClientProperty();

//        }
//        public void ClientLogin()
//        {
//            string tableName = "ClientLogin";
//            string sql = string.Format(selectQuery, tableName);
//            var table = GetDataTable(SourceConStr, sql);
//            table.Columns["ClientLoginId"].ColumnName = "Id";
//            table.Columns["AdvisorLoginID"].ColumnName = "AdvisorLoginId";
//            table.Columns["RecordIndicator"].ColumnName = "Guid";
//            table.Columns["LastLoginDateTime"].ColumnName = "LastLogin";
//            table.Columns["InitialLoginComplete"].ColumnName = "InitialProcessComplete";
//            table.Columns.Add("ModifiedBy");
//            table.Columns["DateCreated"].ColumnName = "CreatedDate";
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");
//            foreach (DataRow row in table.Rows)
//            {
//                row["Username"] = Convert.ToInt32(row["Id"]) == 29 ? "client1@yopmail.com" : row["Username"];
//                row["Password"] = Convert.ToInt32(row["Id"]) == 29 ? ("Lifearcplan@2020").Encrypt() : row["Password"];
//                row["Guid"] = row["Guid"] != DBNull.Value ? row["GUid"] : Guid.NewGuid();
//                row["ModifiedBy"] = row["CreatedBy"];
//                row["ModifiedDate"] = row["CreatedDate"];
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table);
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;
//            ExecuteSql(query);
//        }
//        public void ClientData()
//        {
//            string tableName = "ClientData";
//            string sql = string.Format(selectQuery2, tableName);
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["ID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns.Add("ProjectedAnnualBudget");
//            table.Columns.Add("InflationaryBudget");
//            table.Columns.Add("NonInflationaryBudget");
//            table.Columns.Add("HasPrevSpouse");
//            table.Columns.Add("Guid");
//            table.Columns.Add("TaxFilingType");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns["DateCreated"].ColumnName = "CreatedDate";
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");
//            table.Columns.Add("LastAccessMenuId");
//            table.Columns.Add("IsPermanantCitizen");
//            table.Columns.Add("BankInflationaryInvestmentAmount");
//            table.Columns.Add("BankNonInflationaryInvestmentAmount");
//            table.Columns.Add("BrokerageInflationaryInvestmentAmount");
//            table.Columns.Add("BrokerageNonInflationaryInvestmentAmount");
//            table.Columns.Add("AnnuityInflationaryInvestmentAmount");
//            table.Columns.Add("AnnuityNonInflationaryInvestmentAmount");

//            foreach (DataRow row in table.Rows)
//            {
//                row["ProjectedAnnualBudget"] = 0;
//                row["InflationaryBudget"] = 0;
//                row["NonInflationaryBudget"] = 0;
//                row["BankInflationaryInvestmentAmount"] = 0;
//                row["BankNonInflationaryInvestmentAmount"] = 0;
//                row["BrokerageInflationaryInvestmentAmount"] = 0;
//                row["BrokerageNonInflationaryInvestmentAmount"] = 0;
//                row["AnnuityInflationaryInvestmentAmount"] = 0;
//                row["AnnuityNonInflationaryInvestmentAmount"] = 0;
//                row["IsPermanantCitizen"] = false;
//                row["LastAccessMenuId"] = 0;
//                row["ModifiedDate"] = false;
//                row["Guid"] = Guid.NewGuid();
//                row["ModifiedDate"] = row["CreatedDate"];
//                row["FirstName"] = row["FirstName"].ToString().Decrypt();
//                row["MiddleInitial"] = row["MiddleInitial"].ToString().Decrypt();
//                row["LastName"] = row["LastName"].ToString().Decrypt();
//                row["DateOfBirth"] = row["DateOfBirth"].ToString().Decrypt();
//                row["Gender"] = ((int)EnumHelper.GetValueFromName<GenderEnum>(row["Gender"].ToString().Decrypt())).ToString();
//                row["MaritalStatus"] = ((int)EnumHelper.GetValueFromName<MaritalStatusEnum>(row["MaritalStatus"].ToString().Decrypt())).ToString();
//                row["IsCitizen"] = row["IsCitizen"] != DBNull.Value ? Convert.ToBoolean(row["IsCitizen"]) : false;
//                row["ActiveService"] = row["ActiveService"] != DBNull.Value ? Convert.ToBoolean(row["ActiveService"]) : false;
//                row["Discharge"] = row["Discharge"] != DBNull.Value ? Convert.ToBoolean(row["Discharge"]) : false;
//                row["Prenup"] = row["Prenup"] != DBNull.Value ? Convert.ToBoolean(row["Prenup"]) : false;
//                row["CareForSurvivingSpouse"] = row["CareForSurvivingSpouse"] != DBNull.Value ? Convert.ToBoolean(row["CareForSurvivingSpouse"]) : false;
//                row["IsAdvisorAssisted"] = row["IsAdvisorAssisted"] != DBNull.Value ? Convert.ToBoolean(row["IsAdvisorAssisted"]) : false;
//                row["IsMilitary"] = row["IsMilitary"] != DBNull.Value ? Convert.ToBoolean(row["IsMilitary"]) : false;
//                row["SpecialNeedsTrust"] = row["SpecialNeedsTrust"] != DBNull.Value ? Convert.ToBoolean(row["SpecialNeedsTrust"]) : false;
//                row["MarriedTenYears"] = row["MarriedTenYears"] != DBNull.Value ? Convert.ToBoolean(row["MarriedTenYears"]) : false;
//                //var str = "select top 1 Status from ClientTaxData";
//                //var taxFilingType = GetDataTable(SourceConStr, str).Rows[0].ItemArray[0].ToString();
//                row["TaxFilingType"] =  0;
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//        public void SpouseData()
//        {
//            string tableName = "SpouseData";
//            string sql = string.Format(selectQuery2, tableName) + "and (isxspouse is null or isxspouse = 0)";
//            var table = GetDataTable(SourceConStr, sql);
//            table.Columns["ID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns["DateCreated"].ColumnName = "CreatedDate";
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");
//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["ModifiedDate"] = row["CreatedDate"];
//                row["FirstName"] = row["FirstName"].ToString().Decrypt();
//                row["MiddleInitial"] = row["MiddleInitial"].ToString().Decrypt();
//                row["LastName"] = row["LastName"].ToString().Decrypt();
//                row["DateOfBirth"] = row["DateOfBirth"].ToString().Decrypt();
//                row["Gender"] = ((int)EnumHelper.GetValueFromName<GenderEnum>(row["Gender"].ToString().Decrypt())).ToString();
//                row["IsCitizen"] = row["IsCitizen"] != DBNull.Value ? Convert.ToBoolean(row["IsCitizen"]) : false;
//                row["ActiveService"] = row["ActiveService"] != DBNull.Value ? Convert.ToBoolean(row["ActiveService"]) : false;
//                row["Discharge"] = row["Discharge"] != DBNull.Value ? Convert.ToBoolean(row["Discharge"]) : false;
//                row["IsMilitary"] = row["IsMilitary"] != DBNull.Value ? Convert.ToBoolean(row["IsMilitary"]) : false;
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;
//            ExecuteSql(query);
//        }
//        public void XSpouseData()
//        {
//            string tableNameOld = "SpouseData";
//            string tableName = "XSpouseData";
//            string sql = string.Format(selectQuery2, tableNameOld) + "and isxspouse = 1";
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["ID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns["DateCreated"].ColumnName = "CreatedDate";
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");

//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["ModifiedDate"] = row["CreatedDate"];
//                row["FirstName"] = row["FirstName"].ToString().Decrypt();
//                row["MiddleInitial"] = row["MiddleInitial"].ToString().Decrypt();
//                row["LastName"] = row["LastName"].ToString().Decrypt();
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//        public void ClientAddress()
//        {
//            string tableName = "ClientAddress";
//            string sql = string.Format(selectQuery2, tableName);
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["AddressID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns["Zip"].ColumnName = "ZipCode";
//            table.Columns["CellOne"].ColumnName = "CellNumber";
//            table.Columns["CellTwo"].ColumnName = "SpouseCellNumber";
//            table.Columns["EmailOne"].ColumnName = "Email";
//            table.Columns["EmailTwo"].ColumnName = "SpouseEmail";
//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns["DateCreated"].ColumnName = "CreatedDate";
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");

//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["ModifiedDate"] = row["CreatedDate"];
//                row["Address"] = row["Address"].ToString().Decrypt();
//                row["Address2"] = row["Address2"].ToString().Decrypt();
//                row["City"] = row["City"].ToString().Decrypt();
//                row["State"] = row["State"].ToString().Decrypt();
//                row["ZipCode"] = row["ZipCode"].ToString().Decrypt();
//                row["POBox"] = row["POBox"].ToString().Decrypt();
//                row["HomePhone"] = row["HomePhone"].ToString().Decrypt();
//                row["CellNumber"] = row["CellNumber"].ToString().Decrypt();
//                row["SpouseCellNumber"] = row["SpouseCellNumber"].ToString().Decrypt();
//                row["Email"] = row["Email"].ToString().Decrypt();
//                row["SpouseEmail"] = row["SpouseEmail"].ToString().Decrypt();
//                var country = row["Country"].ToString().Decrypt();

//                row["Country"] = country == "usa" ? "US" : country;

//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//        public void Heir()
//        {
//            string tableNameOld = "ClientChildren";
//            string tableName = "Heir";
//            string sql = string.Format(selectQuery2, tableNameOld);
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["ChildID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns["DateCreated"].ColumnName = "CreatedDate";
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");
//            table.Columns.Add("FirstName");
//            table.Columns.Add("MiddleInitial");
//            table.Columns.Add("LastName");
//            table.Columns["ChildOf"].ColumnName = "HeirOf";
//            table.Columns["HeirDOB"].ColumnName = "DateOfBirth";
//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["ModifiedDate"] = row["CreatedDate"];
//                var fullname = row["Name"].ToString().Decrypt();
//                var splitFullname = fullname.Split(" ");
//                row["FirstName"] = splitFullname[0];
//                row["MiddleInitial"] = splitFullname.Length > 2 ? splitFullname[1].Replace(".", "") : string.Empty;
//                row["LastName"] = splitFullname.Length > 2 ? splitFullname[2] : splitFullname.Length > 1 ? splitFullname[1] : string.Empty;
//                var heirof = row["HeirOf"].ToString().Decrypt();
//                row["HeirOf"] = heirof != "both" ? ((int)EnumHelper.GetValueFromName<ClientTypeEnum>(heirof)).ToString() : "1,2";
//                row["Relationship"] = ((int)EnumHelper.GetValueFromName<RelationshipEnum>(row["Relationship"].ToString().Decrypt())).ToString();
//                var Inheritance = row["Inheritance"].ToString().Decrypt();
//                row["Inheritance"] = Inheritance == "Yes" ? true : false;
//                var dob = row["DateOfBirth"].ToString().Decrypt();
//                int day = !string.IsNullOrEmpty(dob) ? Convert.ToInt32(dob.Substring(0, 2)) : 0;
//                var culture = day > 12 ? cultureGB : cultureUS;
//                row["DateOfBirth"] = day > 0 ? Convert.ToDateTime(dob, culture) : DateTime.MinValue;
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//        public void BudgetItem()
//        {
//            string json = File.ReadAllText("budgetItem.json");
//            string tableName = "BudgetItem";
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//               GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;
//            ExecuteSql(query);
//        }
//        public void ClientBudgetItem()
//        {
//            string tableNameOld = "BudgetClientData";
//            string tableName = "ClientBudget";
//            string sql = string.Format(selectQuery2, tableNameOld);
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["ID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns["ItemID"].ColumnName = "BudgetItemId";
//            table.Columns["Increase"].ColumnName = "Inflationary";
//            table.Columns["Balance"].ColumnName = "BalanceOwn";
//            table.Columns["Duration"].ColumnName = "YearsPayOff";

//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns.Add("CreatedDate");
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");

//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["ModifiedDate"] = row["CreatedDate"];
//                row["Inflationary"] = row["Inflationary"].ToString() == "y" ? true : false;
//                row["BalanceOwn"] = row["BalanceOwn"] != DBNull.Value ? row["BalanceOwn"] : 0;
//                row["YearsPayOff"] = row["YearsPayOff"] != DBNull.Value ? row["YearsPayOff"] : 0;
//                row["CreatedDate"] = DateTime.UtcNow;
//                row["ModifiedDate"] = DateTime.UtcNow;
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//        public void PlanningItem()
//        {
//            string json = File.ReadAllText("planningItems.json");
//            string tableName = "PlanningItem";
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//               GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;
//            ExecuteSql(query);
//        }
//        public void descriptionItem()
//        {
//            string json = File.ReadAllText("descriptionItems.json");
//            string tableName = "DescriptionItems";
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//               GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;
//            ExecuteSql(query);
//        }
//        public void ClientPlanning()
//        {
//            string tableNameOld = "ImportantPlanningItemsClientData";
//            string tableName = "ClientPlanning";
//            string sql = string.Format(selectQuery2 + " and selected=1 and importance > 0", tableNameOld);
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["ID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns["ItemID"].ColumnName = "PlanningItemId";
//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns.Add("CreatedDate");
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");

//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["CreatedDate"] = DateTime.UtcNow;
//                row["ModifiedDate"] = DateTime.UtcNow;
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//        public void ClientProperty()
//        {
//            string tableNameOld = "ClientProperty";
//            string tableName = "ClientProperty";
//            string sql = string.Format(selectQuery2, tableNameOld);
//            var table = GetDataTable(SourceConStr, sql);

//            table.Columns["PropertyID"].ColumnName = "Id";
//            table.Columns["ClientID"].ColumnName = "ClientId";
//            table.Columns["PropertyType"].ColumnName = "Type";
//            table.Columns["Value"].ColumnName = "CurrentValue";
//            table.Columns["Owners"].ColumnName = "Ownership";
//            table.Columns["Plans"].ColumnName = "PropertyPlan";
//            table.Columns["AmountOfOwnerShip"].ColumnName = "PercentageOfOwnership";
//            table.Columns.Add("Guid");
//            table.Columns.Add("CreatedBy");
//            table.Columns.Add("ModifiedBy");
//            table.Columns.Add("CreatedDate");
//            table.Columns.Add("ModifiedDate");
//            table.Columns.Add("IsDeleted");

//            foreach (DataRow row in table.Rows)
//            {
//                row["Guid"] = Guid.NewGuid();
//                row["CreatedDate"] = DateTime.UtcNow;
//                row["ModifiedDate"] = DateTime.UtcNow;
//                row["PercentageOfOwnership"] = row["PercentageOfOwnership"] != DBNull.Value ? row["PercentageOfOwnership"] : 0;
//                row["ProjectedFairMarketValueRetirement"] = row["ProjectedFairMarketValueRetirement"] != DBNull.Value ? row["ProjectedFairMarketValueRetirement"] : 0;
//                row["Type"] = ((int)EnumHelper.GetValueFromName<PropertyTypeEnum>(row["Type"].ToString())).ToString();
//                row["Ownership"] = row["Ownership"].ToString() != "both" ? ((int)EnumHelper.GetValueFromName<ClientTypeEnum>(row["Ownership"].ToString())).ToString() : "1,2";
//                row["PropertyPlan"] = ((int)EnumHelper.GetValueFromName<ProprtyPlanEnum>(row["PropertyPlan"].ToString())).ToString();
//                int current = Convert.ToInt32(row.Table.Rows.IndexOf(row)) + 1;
//                SetFormLableControl("label3", $"{tableName} : {GetProgress(current, table.Rows.Count)}% Summary : {current}/{table.Rows.Count}");
//            }
//            SetColumnsOrder(table, GetColumns(tableName));
//            var json = JsonConvert.SerializeObject(table).Replace("'", "''");
//            string identityOn = string.Format(identitySetQuery, tableName, "ON");
//            string identiyOff = string.Format(identitySetQuery, tableName, "OFF");
//            string query = string.Format(mergeQuery, tableName, json, GetColumns(tableName, true),
//                GetColumnUpdateString(tableName), GetColumns(tableName), GetColumnValueString(tableName));
//            query = identityOn + Environment.NewLine + query + Environment.NewLine + identiyOff;

//            ExecuteSql(query);
//        }
//    }
//}