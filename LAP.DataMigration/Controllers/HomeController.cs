﻿using LAP.DataMigration.SourceModels;
using LAP.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace LAP.DataMigration.Controllers
{
    public class HomeController : Controller
    {
        protected readonly LifeArcPlanContext _context;
        protected readonly FileHelper _fileHelper;
        protected readonly int[] adviserIds = {
                13, 14, 16, 19, 20, 21, 23, 24, 26, 27, 28, 30, 31, 34, 36, 38, 43, 44, 46, 48, 51, 61, 62, 63, 65, 68, 70, 74, 75, 78, 84, 91, 94,
                96, 97, 98, 100, 101, 102, 103, 109, 115, 116, 117, 118, 120, 121, 122, 123, 126, 127, 128, 129, 131, 133, 136, 137, 139, 143, 145, 147, 148, 149, 150,
                151, 152, 153, 154, 156, 157, 158, 160, 161, 163, 165, 167, 168, 169, 170, 171, 173, 174, 177, 178, 180, 181, 182, 183, 184, 186, 190, 191, 192, 193,
                194, 195, 196, 197, 198, 200, 201, 202, 205, 209, 212, 213, 215, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 237, 238, 240,
                241, 243, 246, 256, 259, 261, 262, 263, 266, 268, 275, 276, 277, 278, 283, 285, 287, 288, 289, 294, 295, 296, 297, 298, 301, 302, 303, 304 };
        protected readonly int[] affiliationIds = { 2, 4, 7, 11, 12, 13, 17, 19, 22, 23, 25, 26, 27, 32, 33, 37, 39, 41, 42, 43, 45, 46, 48, 49, 50, 51, 52, 53 };
        protected readonly string[] fullMonths = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        public HomeController(LifeArcPlanContext context, IConfiguration config)
        {
            _context = context;
            _fileHelper = new FileHelper(config);
        }

        public async Task<IActionResult> Migration()
        {
            //var affiliation = AffiliationScript();
            //var users = UserScript();
            //ClientScript();
            //AdviserContactInfo();
            //AdviserRegulatory();
            //AdviserEduInfo();
            //AdviserWork();
            //await AdviserComplianceFile();
            await AdviserComplianceLink();
            return View("Index");
        }
        public void AffiliationScript()
        {
            var query = "SET IDENTITY_INSERT [Adviser].[Affiliation] ON" + Environment.NewLine;
            int ownerId = 1;
            var affiliations = _context.AdvisorComps.Where(m => affiliationIds.Contains(m.Id));
            int type;
            foreach (var item in affiliations)
            {
                type = ((int)EnumHelper.GetValueFromName<AffiliationTypeEnum>(item.CompType));
                query += $@"INSERT INTO [Adviser].[Affiliation]([Id],[Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[Name],[Type],[Address],[Address2],[City],[ZipCode],[State],[ComplianceOfficerName],[Email],[Phone],[WebsiteUrl]) VALUES({item.Id},'{Guid.NewGuid()}',{ownerId},{ownerId},'{item.DateCreated}','{item.DateCreated}',0,'{item.CompName.Replace("'", "''")}',{type},'{item.Address}','{item.Address2}','{item.City}','{item.Zip}','{item.State}','{item.ContactName}','{item.Email}','{item.Phone}','{item.SiteUrl}')" + Environment.NewLine;
            }
            query += "SET IDENTITY_INSERT [Adviser].[Affiliation] OFF" + Environment.NewLine;
        }
        public void UserScript()
        {
            var query = "SET IDENTITY_INSERT [Admin].[UserLogin] ON" + Environment.NewLine;
            int firmId = 14;
            int ownerId = 1;
            var users = _context.AdvisorLogins.Include(m => m.AdvisorDatum).Where(m => adviserIds.Contains(m.AdvisorLoginId));
            int salutation = 0;
            int role = 0;
            object fmoid, bdid, riaid, crd = null;
            string degrees, licences = string.Empty;
            foreach (var item in users)
                query += $@"INSERT INTO [Admin].[UserLogin]([Id],[Username],[Password],[Active],[IsAssistant],[AdminRole],[LastLogin],[SetupCompletedDate],[FirmId],[Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted]) VALUES({item.AdvisorLoginId + 400},'{item.Username}','{item.Password}',1,0,{(int)AdminRoleEnum.Adviser},'{item.LastLoginDateTime}','{item.LastLoginDateTime}',{firmId},'{Guid.NewGuid()}',{ownerId},{ownerId},'{item.DateCreated}','{item.DateCreated}',0)" + Environment.NewLine;
            query += "SET IDENTITY_INSERT [Admin].[UserLogin] OFF" + Environment.NewLine;
            foreach (var item in users)
            {
                degrees = string.Empty;
                licences = string.Empty;
                salutation = (int)EnumHelper.GetValueFromName<SalutationEnum>(item.AdvisorDatum.Salutation);
                role = (int)EnumHelper.GetValueFromName<AdviserTitleEnum>(item.AdvisorDatum.Role);
                fmoid = item.AdvisorDatum.Fmoid > 0 && affiliationIds.ToList().Contains(Convert.ToInt32(item.AdvisorDatum.Fmoid)) ? item.AdvisorDatum.Fmoid : "null";
                bdid = item.AdvisorDatum.Bdid > 0 && affiliationIds.ToList().Contains(Convert.ToInt32(item.AdvisorDatum.Bdid)) ? item.AdvisorDatum.Bdid : "null";
                riaid = item.AdvisorDatum.Riaid > 0 && affiliationIds.ToList().Contains(Convert.ToInt32(item.AdvisorDatum.Riaid)) ? item.AdvisorDatum.Riaid : "null";
                crd = !string.IsNullOrEmpty(item.AdvisorDatum.CardNbr) ? item.AdvisorDatum.CardNbr : "null";
                if (!string.IsNullOrEmpty(item.AdvisorDatum.Designations))
                {
                    foreach (var sp in item.AdvisorDatum.Designations.Split(",").Where(m => m.Length > 0))
                    {
                        var enumVal = (int)EnumHelper.GetValueFromName<DegreeDesinationEnum>(sp);
                        degrees += $"{enumVal},";
                    }
                }
                if (!string.IsNullOrEmpty(item.AdvisorDatum.Licenses))
                {
                    foreach (var sp in item.AdvisorDatum.Licenses.Split(",").Where(m => m.Length > 0))
                    {
                        var enumVal = (int)EnumHelper.GetValueFromName<LicenseEnum>(sp);
                        licences += $"{enumVal},";
                    }
                }
                query += $@"INSERT INTO [Admin].[UserData]([UserId],[FirstName],[MiddleInitial],[LastName],[Salutation],[DateOfBirth],[RoleTitle],[BusinessName],[AvatarFileName],[BusinessLogoFileName],[DegreeDesignation],[License],[CRD],[FMOId],[BDId],[RIAId],[Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted]) VALUES({item.AdvisorLoginId + 400},'{item.AdvisorDatum.FirstName.Replace("'", "''")}','{item.AdvisorDatum.MiddleInitial}','{item.AdvisorDatum.LastName.Replace("'", "''")}',{salutation},'{item.AdvisorDatum.Dob}',{role},'{item.AdvisorDatum.PracticeName}','','','{degrees.TrimEnd(',')}','{licences.TrimEnd(',')}',{crd},{fmoid},{bdid},{riaid},'{Guid.NewGuid()}',{ownerId},{ownerId},'{item.DateCreated}','{item.DateCreated}',0)" + Environment.NewLine;
            }
        }
        public void ClientScript()
        {
            var query = "SET IDENTITY_INSERT [Client].[ClientLogin] ON" + Environment.NewLine;
            int ownerId = 1;
            var clients = _context.ClientLogins.Include(m => m.ClientDatum).Include(m => m.ClientTaxDatum).Include(m => m.FinalThought).Include(m => m.ClientAddress)
                .Where(m => adviserIds.Contains(m.AdvisorLoginId));
            int? newAdviserId = 0;
            string Username = string.Empty;
            foreach (var item in clients)
            {
                newAdviserId = GetNewAdviserId(item.AdvisorLoginId);
                int profileCompleted = item.LastLoginDateTime != null ? 1 : 0;
                Username = item.Username.Contains("@") ? item.Username : item.ClientAddress.EmailOne.DecryptAsync().Result;
                query += $@"INSERT INTO [Client].[ClientLogin]([Id],[Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[Username],[Password],[Active],[LastLogin],[SetupCompletedDate],[InitialProfileSetupCompleted]) VALUES({item.ClientLoginId - 1000 + 400}, '{Guid.NewGuid()}',{ownerId},{ownerId},'{item.DateCreated}','{item.DateCreated}',0,{newAdviserId},'{Username.ToLower()}','{item.Password}',1,'{item.LastLoginDateTime}','{item.DateCreated.Value.AddMinutes(10)}',{profileCompleted})" + Environment.NewLine;
            }
            query += "SET IDENTITY_INSERT [Client].[ClientLogin] OFF" + Environment.NewLine;
            int gender, maritalStatus, taxFilingType, Prenup, CareForSurvivingSpouse, IsCitizen, MarriedTenYears, IsMilitary, ActiveService, Discharge, IsAdvisorAssisted, SpecialNeedsTrust, HasPrevSpouse = 0;
            foreach (var item in clients)
            {
                gender = (int)EnumHelper.GetValueFromName<GenderEnum>(item.ClientDatum.Gender.DecryptAsync().Result);
                maritalStatus = (int)EnumHelper.GetValueFromName<MaritalStatusEnum>(item.ClientDatum.MaritalStatus.DecryptAsync().Result);
                taxFilingType = (int)EnumHelper.GetValueFromName<TaxFilingTypeEnum>(item.ClientTaxDatum?.Status);
                Prenup = Convert.ToBoolean(item.ClientDatum.Prenup) ? 1 : 0;
                CareForSurvivingSpouse = Convert.ToBoolean(item.ClientDatum.CareForSurvivingSpouse) ? 1 : 0;
                IsCitizen = Convert.ToBoolean(item.ClientDatum.IsCitizen) ? 1 : 0;
                MarriedTenYears = Convert.ToBoolean(item.ClientDatum.MarriedTenYears) ? 1 : 0;
                IsMilitary = Convert.ToBoolean(item.ClientDatum.IsMilitary) ? 1 : 0;
                ActiveService = Convert.ToBoolean(item.ClientDatum.ActiveService) ? 1 : 0;
                Discharge = Convert.ToBoolean(item.ClientDatum.Discharge) ? 1 : 0;
                IsAdvisorAssisted = Convert.ToBoolean(item.ClientDatum.IsAdvisorAssisted) ? 1 : 0;
                SpecialNeedsTrust = Convert.ToBoolean(item.ClientDatum.SpecialNeedsTrust) ? 1 : 0;
                HasPrevSpouse = Convert.ToBoolean(item.ClientDatum.PreviousDivorce) || Convert.ToBoolean(item.ClientDatum.PreviousWidow) ? 1 : 0;
                query += $@"INSERT INTO [Client].[ClientData]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[ClientId],[FirstName],[MiddleInitial],[LastName],[Gender],[DateOfBirth],[MaritalStatus],[DateOfCurrentMarriage],[TaxFilingType],[Prenup],[CareForSurvivingSpouse],[IsCitizen],[IsPermanantCitizen],[MarriedTenYears],[IsMilitary],[ActiveService],[Discharge],[IsAdviserAssisted],[SpecialNeedsTrust],[ProjectedAnnualBudget],[InflationaryBudget],[NonInflationaryBudget],[HasPrevSpouse],[BudgetVersion],[InvestmentVersion],[RiskArcVersion],[FinalThought]) VALUES('{Guid.NewGuid()}',{ownerId},{ownerId},'{item.DateCreated}','{item.DateCreated}',0,{item.ClientLoginId - 1000 + 400},'{item.ClientDatum.FirstName.DecryptAsync().Result}','{item.ClientDatum.MiddleInitial.DecryptAsync().Result}','{item.ClientDatum.LastName.DecryptAsync().Result.Replace("'", "''")}',{gender},'{item.ClientDatum.DateOfBirth.DecryptAsync().Result}',{maritalStatus},'{item.ClientDatum.DateOfCurrentMarriage}',{taxFilingType},{Prenup},{CareForSurvivingSpouse},{IsCitizen},1,{MarriedTenYears},{IsMilitary},{ActiveService},{Discharge},{IsAdvisorAssisted},{SpecialNeedsTrust},0,0,0,{HasPrevSpouse},1,1,1,'{item.FinalThought?.Client.Replace("'", "''")}')" + Environment.NewLine;
            }
        }
        public void AdviserContactInfo()
        {
            var query = string.Empty;
            var users = _context.AdvisorLogins.Include(m => m.AdvisorDatum).Include(m => m.AdvisorLocations).Where(m => adviserIds.Contains(m.AdvisorLoginId));
            int? newAdviserId = 0;
            foreach (var item in users)
            {
                newAdviserId = GetNewAdviserId(item.AdvisorLoginId);
                var business = item.AdvisorLocations.Count > 0 ? item.AdvisorLocations.FirstOrDefault() : new AdvisorLocation();
                query += $@"INSERT INTO [Adviser].[AdviserContactInfo]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[Address],[Address2],[City],[State],[ZipCode],[POBox],[Phone],[CellNumber],[BusinessAddress],[BusinessAddress2],[BusinessCellNumber],[BusinessCity],[BusinessPOBox],[BusinessPhone],[BusinessState],[BusinessZipCode]) VALUES('{Guid.NewGuid()}',{newAdviserId},{newAdviserId},'{item.AdvisorDatum.DateCreated}','{item.AdvisorDatum.DateCreated}',0,{newAdviserId},'{item.AdvisorDatum.Address?.Replace("'", "''")}','{item.AdvisorDatum.AptNbr}','{item.AdvisorDatum.City}','{item.AdvisorDatum.State}','{item.AdvisorDatum.Zip}','{item.AdvisorDatum.Pobox}','{item.AdvisorDatum.HomePhone}','{item.AdvisorDatum.CellPhone}','{business.Address1}','{business.Address2}','{business.Phone3}','{business.City}','','{business.Phone1}','{business.State}','{business.Zip}')" + Environment.NewLine;
            }
        }
        public void AdviserEduInfo()
        {
            var query = string.Empty;
            var users = _context.AdvisorEdus.Where(m => adviserIds.Contains(m.AdvisorId) && m.Degree != "none" && m.Field != "none");
            int? newAdviserId = 0;
            int degree, field = 0;
            DateTime fromYear = default;
            DateTime toYear = default;
            string from = string.Empty;
            string to = string.Empty;
            foreach (var item in users)
            {
                var sp = item.FromYear.Split(new char[] { '-', '/', ' ' });
                var sp2 = item.ToYear.Split(new char[] { '-', '/', ' ' });
                if ((item.FromYear.Length == 6 || item.FromYear.Length == 7) && (item.ToYear.Length == 6 || item.ToYear.Length == 7))
                {
                    from = item.FromYear.Length == 6 ? $"0{item.FromYear}" : item.FromYear;
                    to = item.ToYear.Length == 6 ? $"0{item.ToYear}" : item.ToYear;
                }
                else
                {
                    if (sp[0].Length > 1 && !Regex.IsMatch(sp[0], @"^\d+$") && sp2.Length > 1 && !Regex.IsMatch(sp2[0], @"^\d+$"))
                    {
                        var m = Array.FindIndex(fullMonths, row => row.Contains(sp[0]));
                        var m2 = Array.FindIndex(fullMonths, row => row.Contains(sp2[0]));
                        fromYear = new DateTime(Convert.ToInt32(sp[1]), m, 1);
                        toYear = new DateTime(Convert.ToInt32(sp2[1]), m2, 1);
                        from = string.Format("{0:MM/yyyy}", fromYear);
                        to = string.Format("{0:MM/yyyy}", to);
                    }
                    else
                    {
                        from = string.Empty;
                        to = string.Empty;
                    }
                }
                newAdviserId = GetNewAdviserId(item.AdvisorId);
                degree = (int)EnumHelper.GetValueFromName<DegreeTypeEnum>(item.Degree);
                field = (int)EnumHelper.GetValueFromName<DegreeEnum>(item.Field);
                query += $@"INSERT INTO[Adviser].[PostHighSchoolEducation]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[School],[Degree],[Field],[FromYear],[ToYear]) VALUES('{Guid.NewGuid()}',{newAdviserId},{newAdviserId},'{item.DateCreated}','{item.DateCreated}',0,{newAdviserId},'{item.SchoolName.Replace("'", "''")}',{degree},{field},'{from}','{to}')" + Environment.NewLine;
            }
        }
        public void AdviserWork()
        {
            var query = string.Empty;
            var users = _context.AdvisorWorks.Where(m => adviserIds.Contains(m.AdvisorId));
            int? newAdviserId = 0;
            DateTime fromYear = default;
            DateTime toYear = default;
            string from = string.Empty;
            string to = string.Empty;
            string state = string.Empty;
            foreach (var item in users)
            {
                var sp = item.FromYear.Split(new char[] { '-', '/', ' ' });
                var sp2 = item.ToYear.Split(new char[] { '-', '/', ' ' });
                if ((item.FromYear.Length == 6 || item.FromYear.Length == 7) && (item.ToYear.Length == 6 || item.ToYear.Length == 7))
                {
                    from = item.FromYear.Length == 6 ? $"0{item.FromYear}" : item.FromYear;
                    to = item.ToYear.Length == 6 ? $"0{item.ToYear}" : item.ToYear;
                }
                else
                {
                    if (sp[0].Length > 1 && !Regex.IsMatch(sp[0], @"^\d+$") && sp2.Length > 1 && !Regex.IsMatch(sp2[0], @"^\d+$"))
                    {
                        var m = Array.FindIndex(fullMonths, row => row.Contains(sp[0]));
                        var m2 = Array.FindIndex(fullMonths, row => row.Contains(sp2[0]));
                        fromYear = new DateTime(Convert.ToInt32(sp[1]), m, 1);
                        toYear = new DateTime(Convert.ToInt32(sp2[1]), m2, 1);
                        from = string.Format("{0:MM/yyyy}", fromYear);
                        to = string.Format("{0:MM/yyyy}", to);
                    }
                    else
                    {
                        from = string.Empty;
                        to = string.Empty;
                    }
                }
                newAdviserId = GetNewAdviserId(item.AdvisorId);
                int isPresent = item.ToYear.Equals("Present", StringComparison.OrdinalIgnoreCase) || item.ToYear.Equals("Current", StringComparison.OrdinalIgnoreCase) ? 1 : 0;
                state = FetchState().FirstOrDefault(m => m.Abbreviation == item.State)?.Name;
                query += $@"INSERT INTO [Adviser].[Work]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[Company],[Position],[State],[City],[FromYear],[ToYear],[IsPresent]) VALUES('{Guid.NewGuid()}',{newAdviserId},{newAdviserId},'{item.DateCreated}','{item.DateCreated}',0,{newAdviserId},'{item.CompanyName}','{item.Position}','{state}','{item.City}','{from}','{to}',{isPresent})" + Environment.NewLine;
            }
        }
        public void AdviserRegulatory()
        {
            var query = string.Empty;
            var users = _context.AdvisorRegs.Where(m => adviserIds.Contains(m.AdvisorId));
            string sectionId;
            int answer;
            int? newAdviserId = 0;
            foreach (var item in users)
            {
                sectionId = item.RegItem.Replace("r", string.Empty);
                answer = item.Answer == "y" ? 1 : 0;
                newAdviserId = GetNewAdviserId(item.AdvisorId);
                query += $@"INSERT INTO [Adviser].[Regulatory]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[SectionId],[Answer]) VALUES('{Guid.NewGuid()}',{newAdviserId},{newAdviserId},'{item.DateCreated}','{item.DateCreated}',0,{newAdviserId},{Convert.ToInt32(sectionId) + 16},{answer})" + Environment.NewLine;
            }
        }
        public class GroupedFiles
        {
            public string FileName { get; set; }
            public int Count { get; set; }
        }
        public async Task AdviserComplianceFile()
        {
            _context.Database.SetCommandTimeout(36000);
            var files = _context.AdvisorCompFiles.Where(m => adviserIds.Contains(m.AdvisorId) && m.FileData.Length > 0);

            var group = files.GroupBy(x => x.FileName).Select(m => new GroupedFiles
            {
                FileName = m.Key,
                Count = m.Count()
            });
            int count = 0;
            string fileName = string.Empty;
            var query = string.Empty;
            int? newAdviserId = 0;
            object newReviewId = 0;
            int status = 0;
            string versionId = string.Empty;
            foreach (var item in group)
            {
                count = 1;
                query = string.Empty;
                foreach (var item2 in files.Where(m => m.FileName == item.FileName))
                {
                    item2.FileName = item2.FileName.Replace("\"", "'").Replace("?", "");
                    item2.FileName = Regex.Replace(item2.FileName, @"(?<=^\s*)\s|\s(?=\s*$)", "_");
                    if (item.Count > 1)
                        item2.FileName = $"{System.IO.Path.GetFileNameWithoutExtension(item2.FileName)} ({count++}){System.IO.Path.GetExtension(item2.FileName)}";
                    if (System.IO.File.Exists(@$"E:\CompFile\{item2.FileName}"))
                        item2.FileName = item2.FileName.Replace("(", "(0");
                    //await System.IO.File.WriteAllBytesAsync(@$"E:\CompFile2\{item2.FileName}", item2.FileData);                    
                    newAdviserId = GetNewAdviserId(item2.AdvisorId);
                    newReviewId = item2.UpdatedBy > 0 ? GetNewAdviserId(Convert.ToInt32(item2.UpdatedBy)) : "null";
                    status = item2.Status == "y" ? (int)ComplianceDocumentStatusEnum.Approved : item2.Status == "n" ? (int)ComplianceDocumentStatusEnum.Rejected : (int)ComplianceDocumentStatusEnum.Pending;
                    versionId = await _fileHelper.GetLatestVersionId("lifearcplan-production-bucket/documents/compliance", item2.FileName);
                    query += $@"INSERT INTO [Adviser].[ComplianceDocument]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[FileName],[ContentType],[FileSize],[VersionId],[UploadedDate],[AdviserNote],[ComplianceNote],[Status],[ReviewedDate],[ReviewedById],[IsFromV1]) VALUES('{Guid.NewGuid()}',{newAdviserId},{newAdviserId},'{item2.DateUploaded}','{item2.DateUploaded}',0,{newAdviserId},'{item2.FileName?.Replace("'", "''")}','{item2.MimeType}',{item2.FileData.Length},'{versionId}','{item2.DateUploaded}','{item2.AdvisorNotes?.Replace("'", "''")}','{item2.Notes?.Replace("'", "''")}',{status},'{item2.DateApproved}',{newReviewId},1)" + Environment.NewLine;
                }
                await System.IO.File.AppendAllTextAsync(@"C:\Users\GINESH\Desktop\compliance-files.sql", query);
            }
        }

        public async Task AdviserComplianceLink()
        {
            var files = _context.AdvisorCompFiles.Where(m => adviserIds.Contains(m.AdvisorId) && m.FileData == null && !m.FileName.Contains(@"C:"));
            string fileName = string.Empty;
            var query = string.Empty;
            int? newAdviserId = 0;
            object newReviewId = 0;
            int status = 0;
            foreach (var item in files)
            {
                newAdviserId = GetNewAdviserId(item.AdvisorId);
                newReviewId = item.UpdatedBy > 0 ? GetNewAdviserId(Convert.ToInt32(item.UpdatedBy)) : "null";
                status = item.Status == "y" ? (int)ComplianceDocumentStatusEnum.Approved : item.Status == "n" ? (int)ComplianceDocumentStatusEnum.Rejected : (int)ComplianceDocumentStatusEnum.Pending;
                query += $@"INSERT INTO [Adviser].[ComplianceDocument]([Guid],[CreatedBy],[ModifiedBy],[CreatedDate],[ModifiedDate],[IsDeleted],[UserId],[FileName],[UploadedDate],[AdviserNote],[ComplianceNote],[Status],[ReviewedDate],[ReviewedById],[IsFromV1],[FileSize],[ContentType]) VALUES('{Guid.NewGuid()}',{newAdviserId},{newAdviserId},'{item.DateUploaded}','{item.DateUploaded}',0,{newAdviserId},'{item.FileName?.Replace("'", "''").Trim()}','{item.DateUploaded}','{item.AdvisorNotes?.Replace("'", "''")}','{item.Notes?.Replace("'", "''")}',{status},'{item.DateApproved}',{newReviewId},1,0,'link')" + Environment.NewLine;
            }
            await System.IO.File.WriteAllTextAsync(@"C:\Users\GINESH\Desktop\compliance-link.sql", query);
        }
        public int GetNewAdviserId(int OldAdviserId)
        {
            var mapping = FetchNewOldId();
            int? newAdviserId = 0;
            newAdviserId = mapping.FirstOrDefault(m => m.OldId == OldAdviserId)?.NewId;
            newAdviserId = newAdviserId > 0 ? newAdviserId : OldAdviserId + 400;
            return Convert.ToInt32(newAdviserId);
        }
        public List<NewOldModel> FetchNewOldId()
        {
            var data = new List<NewOldModel>
            {
                new NewOldModel { OldId = 294, NewId = 189 },
                new NewOldModel { OldId = 256, NewId = 114 },
                new NewOldModel { OldId = 121, NewId = 134 },
                new NewOldModel { OldId = 259, NewId = 125 },
                new NewOldModel { OldId = 38, NewId = 10 },
                new NewOldModel { OldId = 75, NewId = 226 },
                new NewOldModel { OldId = 285, NewId = 145 },
                new NewOldModel { OldId = 302, NewId = 235 },
                new NewOldModel { OldId = 190, NewId = 11 },
                new NewOldModel { OldId = 278, NewId = 141 },
                new NewOldModel { OldId = 120, NewId = 119 },
                new NewOldModel { OldId = 303, NewId = 223 },
                new NewOldModel { OldId = 297, NewId = 221 },
                new NewOldModel { OldId = 261, NewId = 148 },
                new NewOldModel { OldId = 61, NewId = 149 },
                new NewOldModel { OldId = 275, NewId = 144 },
                new NewOldModel { OldId = 131, NewId = 129 },
                new NewOldModel { OldId = 70, NewId = 86 },
                new NewOldModel { OldId = 145, NewId = 142 },
                new NewOldModel { OldId = 97, NewId = 140 },
                new NewOldModel { OldId = 65, NewId = 137 },
                new NewOldModel { OldId = 118, NewId = 121 },
                new NewOldModel { OldId = 277, NewId = 128 },
                new NewOldModel { OldId = 298, NewId = 222 },
                new NewOldModel { OldId = 215, NewId = 127 },
                new NewOldModel { OldId = 127, NewId = 120 },
                new NewOldModel { OldId = 169, NewId = 147 },
                new NewOldModel { OldId = 23, NewId = 153 },
                new NewOldModel { OldId = 288, NewId = 115 },
                new NewOldModel { OldId = 126, NewId = 126 },
                new NewOldModel { OldId = 243, NewId = 197 },
                new NewOldModel { OldId = 116, NewId = 117 },
                new NewOldModel { OldId = 287, NewId = 13 },
                new NewOldModel { OldId = 223, NewId = 227 },
                new NewOldModel { OldId = 246, NewId = 9 },
                new NewOldModel { OldId = 26, NewId = 139 },
                new NewOldModel { OldId = 44, NewId = 155 },
                new NewOldModel { OldId = 229, NewId = 133 },
                new NewOldModel { OldId = 163, NewId = 176 },
                new NewOldModel { OldId = 51, NewId = 122 },
                new NewOldModel { OldId = 289, NewId = 14 },
                new NewOldModel { OldId = 225, NewId = 118 },
                new NewOldModel { OldId = 154, NewId = 430 },
                new NewOldModel { OldId = 13, NewId = 9 },
                new NewOldModel { OldId = 67, NewId = 27 },
                new NewOldModel { OldId = 237, NewId = 596 },
            };
            return data;
        }
        public List<State> FetchState()
        {
            List<State> items = new();
            using (WebClient webClient = new())
            {
                var json = webClient.DownloadString("https://gist.githubusercontent.com/mshafrir/2646763/raw/8b0dbb93521f5d6889502305335104218454c2bf/states_titlecase.json");
                items = JsonConvert.DeserializeObject<List<State>>(json);
            }
            return items;
        }
        public class State
        {
            public string Name { get; set; }
            public string Abbreviation { get; set; }
        }
        public class NewOldModel
        {
            public int OldId { get; set; }
            public int NewId { get; set; }
        }
    }
}